# Walkthrough

The following walkthrough guides you through the use of INCLINE along the running example used in the paper.

## Prerequisite

Locate the files mainline.cpp and fork.cpp in /home/incline/InclineExample

## 1. Prepare the integrated model

Execute the following command to create an integrated model from mainline.cpp and fork.cpp and save it as integrated.cpp

```
java -jar \
/home/incline/git/cpponmps/ppmerge/astmerge/target/astmerge-1.0-SNAPSHOT-jar-with-dependencies.jar \
/home/incline/InclineExample/mainline.cpp \
/home/incline/InclineExample/fork.cpp \
/home/incline/InclineExample/integrated.cpp
```

## 2. Start INCLINE
Start INCLINE using the MPS shortcut on the left. The project should be loaded, and you should see a screen as in the following screenshot:
![01_Incline_Start](Screenshots/01_Incline_Start.PNG)

## 3. Import
Select Tools > Import CPP
![01_Menu_Import.PNG](Screenshots/01_Menu_Import.PNG)

And select the integrated.cpp created in Step 1.

![03_Menu_ImportSelect.PNG](Screenshots/03_Menu_ImportSelect.PNG)

Open the import integrated.cpp in the hierarchy as shown in the screenshot.

![02_SelectIntegrated.PNG](Screenshots/02_SelectIntegrated.PNG)

The file is shown in a single default view:

![Default View](Screenshots/04_DefaultView.PNG)


## 4. Views

Use Tools -> Arrange View to create the four different view described in the paper.
![Arrange View](Screenshots/05_MenuArrangeViews.PNG)

## 5. Intentions

In the fork view (top right), select the node ```int8_t encoderDiff;```. Then, select **Keep as Feature** in the Tools menu.

![Keep As Feature](Screenshots/06_NodeSelectAndMenuKeepAsFeature.PNG)

Provide the feature name ```NEWPANEL```.

![Feature Name](Screenshots/07_KeepAsFeature.PNG)

Note the change in the preview window.

![Preview](Screenshots/08_Preview.PNG)

Select the node ```#if PIN_EXISTS ...``` in the **Mainline view** as shown in the screenshot. Using the keyboard the complete node (spanning 3 lines) can be selected. But it is also possible to just select part of the node (e.g. the ```#if```) using the mouse.

![Select Node](Screenshots/09_SelectNode.PNG)

Then, declare the Keep intention
![Keep](Screenshots/10_MenuKeep.PNG)

Select the node ```bool lcd_oldcardstatus``` in the **Fork view** and declare a KeepAsFeature intention with the feature ```NEWPANEL```.

![Keep As Feature](Screenshots/11_NodeSelectAndMenuKeepFeature.PNG)

Confirm the dialog to convert the intention to an Exclusive intention:

![Exclusive](Screenshots/12_Exclusive.PNG)

For the last two intentions, select ```bool ignor_click = false;``` in the **Mainline view** and declare a Remove intention. Confirm the dialog to declare a corresponding Keep intention.

## Commit
Commit all declared intentions

![Commit](Screenshots/13_Commit.PNG)

## Export

Select **Export C++ File** from the menu and export the result file as **result.cpp**

![Export](Screenshots/14_Export.PNG)

![Save As](Screenshots/15_SaveAs.PNG)








