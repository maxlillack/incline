<?xml version="1.0" encoding="UTF-8"?>
<solution name="Examples" uuid="b6e2d65d-7c0f-48af-b873-a5af2be37b86" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language id="6150e321-6a06-49cb-8a7d-9facbb581dc2" fqName="CPPLanguage" version="0" />
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" fqName="jetbrains.mps.lang.core" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="b6e2d65d-7c0f-48af-b873-a5af2be37b86(Examples)" version="0" />
  </dependencyVersions>
</solution>

