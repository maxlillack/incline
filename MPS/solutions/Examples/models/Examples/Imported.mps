<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b14738f8-2657-42b4-b774-65b9cc12df4f(Examples.Imported)">
  <persistence version="9" />
  <languages>
    <use id="6150e321-6a06-49cb-8a7d-9facbb581dc2" name="CPPLanguage" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="6150e321-6a06-49cb-8a7d-9facbb581dc2" name="CPPLanguage">
      <concept id="4535931673106194951" name="CPPLanguage.structure.CPP" flags="ng" index="U0JZk">
        <property id="8244226309567820609" name="constraints" index="1BOldB" />
        <child id="43933878589130773" name="statements" index="UthQO" />
      </concept>
      <concept id="43933878589532418" name="CPPLanguage.structure.MacroIf" flags="ng" index="UsNMz">
        <property id="3736316424166330986" name="condition" index="3tf_Qn" />
        <child id="43933878589697058" name="trueBranch" index="UrrA3" />
      </concept>
      <concept id="43933878589130779" name="CPPLanguage.structure.TextContent" flags="ng" index="UthQU">
        <property id="43933878589190074" name="text" index="UtvCr" />
      </concept>
      <concept id="43933878589130778" name="CPPLanguage.structure.ICPPElement" flags="ng" index="UthQV">
        <property id="5567135910968350201" name="hidden" index="SPal$" />
        <property id="43933878590180824" name="location" index="UphxT" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="U0JZk" id="3VMRtc4Xu3D">
    <property role="TrG5h" value="test" />
    <property role="1BOldB" value="(&lt;= EXTRUDERS 3)" />
    <node concept="UsNMz" id="4P2s9PPAewZ" role="UthQO">
      <property role="3tf_Qn" value="EXTRUDERS2" />
      <property role="SPal$" value="false" />
      <node concept="UthQU" id="4P2s9PPAex4" role="UrrA3">
        <property role="UtvCr" value="test" />
        <property role="SPal$" value="false" />
      </node>
    </node>
    <node concept="UsNMz" id="71Cofnsp6zv" role="UthQO">
      <property role="UphxT" value="/home/max/CPPonMPS/PPTestServer/helloServer.cpp:471:10" />
      <property role="3tf_Qn" value="Y_MIN_PIN" />
      <property role="SPal$" value="false" />
      <node concept="UsNMz" id="71Cofnsp6zw" role="UrrA3">
        <property role="UphxT" value="/home/max/CPPonMPS/PPTestServer/helloServer.cpp:472:10" />
        <property role="3tf_Qn" value="VERSION" />
        <property role="SPal$" value="false" />
        <node concept="UthQU" id="71Cofnsp6zx" role="UrrA3">
          <property role="UtvCr" value="          bool y_min_endstop=(READ(Y_MIN_PIN) != Y_ENDSTOPS_INVERTING);" />
          <property role="SPal$" value="false" />
        </node>
        <node concept="UthQU" id="71Cofnsp6zy" role="UrrA3">
          <property role="UtvCr" value="          bool y_min_endstop=(READ(Y_MIN_PIN) != Y_MIN_ENDSTOP_INVERTING);" />
          <property role="SPal$" value="false" />
        </node>
      </node>
      <node concept="UthQU" id="71Cofnsp6zz" role="UrrA3">
        <property role="UtvCr" value="          if(y_min_endstop &amp;&amp; old_y_min_endstop &amp;&amp; (current_block-&gt;steps_y &gt; 0)) {" />
        <property role="SPal$" value="false" />
      </node>
      <node concept="UthQU" id="71Cofnsp6z$" role="UrrA3">
        <property role="UtvCr" value="            endstops_trigsteps[Y_AXIS] = count_position[Y_AXIS];" />
        <property role="SPal$" value="false" />
      </node>
      <node concept="UthQU" id="71Cofnsp6z_" role="UrrA3">
        <property role="UtvCr" value="            endstop_y_hit=true;" />
        <property role="SPal$" value="false" />
      </node>
      <node concept="UthQU" id="71Cofnsp6zA" role="UrrA3">
        <property role="UtvCr" value="            step_events_completed = current_block-&gt;step_event_count;" />
        <property role="SPal$" value="false" />
      </node>
      <node concept="UthQU" id="71Cofnsp6zB" role="UrrA3">
        <property role="UtvCr" value="          }" />
        <property role="SPal$" value="false" />
      </node>
      <node concept="UthQU" id="71Cofnsp6zC" role="UrrA3">
        <property role="UtvCr" value="          old_y_min_endstop = y_min_endstop;" />
        <property role="SPal$" value="false" />
      </node>
    </node>
  </node>
</model>

