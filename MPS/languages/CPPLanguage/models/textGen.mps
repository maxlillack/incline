<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b99574b8-54bd-4c72-abe6-234087b73845(CPPLanguage.textGen)">
  <persistence version="9" />
  <languages>
    <use id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen" version="0" />
    <use id="daafa647-f1f7-4b0b-b096-69cd7c8408c0" name="jetbrains.mps.baseLanguage.regexp" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="4s7a" ref="r:c67970af-4844-47d5-9471-54e2bda5945e(CPPLanguage.structure)" implicit="true" />
    <import index="tpfp" ref="r:00000000-0000-4000-0000-011c89590519(jetbrains.mps.baseLanguage.regexp.jetbrains.mps.regexp.accessory)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
        <child id="1206060520071" name="elsifClauses" index="3eNLev" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1206060495898" name="jetbrains.mps.baseLanguage.structure.ElsifClause" flags="ng" index="3eNFk2">
        <child id="1206060619838" name="condition" index="3eO9$A" />
        <child id="1206060644605" name="statementList" index="3eOfB_" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
    </language>
    <language id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen">
      <concept id="1237305208784" name="jetbrains.mps.lang.textGen.structure.NewLineAppendPart" flags="ng" index="l8MVK" />
      <concept id="1237305334312" name="jetbrains.mps.lang.textGen.structure.NodeAppendPart" flags="ng" index="l9hG8">
        <child id="1237305790512" name="value" index="lb14g" />
      </concept>
      <concept id="1237305491868" name="jetbrains.mps.lang.textGen.structure.CollectionAppendPart" flags="ng" index="l9S2W">
        <property id="1237306003719" name="separator" index="lbP0B" />
        <property id="1237983969951" name="withSeparator" index="XA4eZ" />
        <child id="1237305945551" name="list" index="lbANJ" />
      </concept>
      <concept id="1237305557638" name="jetbrains.mps.lang.textGen.structure.ConstantStringAppendPart" flags="ng" index="la8eA">
        <property id="1237305576108" name="value" index="lacIc" />
      </concept>
      <concept id="1237306079178" name="jetbrains.mps.lang.textGen.structure.AppendOperation" flags="nn" index="lc7rE">
        <child id="1237306115446" name="part" index="lcghm" />
      </concept>
      <concept id="1233670071145" name="jetbrains.mps.lang.textGen.structure.ConceptTextGenDeclaration" flags="ig" index="WtQ9Q">
        <reference id="1233670257997" name="conceptDeclaration" index="WuzLi" />
        <child id="1233749296504" name="textGenBlock" index="11c4hB" />
      </concept>
      <concept id="1233748055915" name="jetbrains.mps.lang.textGen.structure.NodeParameter" flags="nn" index="117lpO" />
      <concept id="1233749247888" name="jetbrains.mps.lang.textGen.structure.GenerateTextDeclaration" flags="in" index="11bSqf" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="daafa647-f1f7-4b0b-b096-69cd7c8408c0" name="jetbrains.mps.baseLanguage.regexp">
      <concept id="1175161264766" name="jetbrains.mps.baseLanguage.regexp.structure.LineStartRegexp" flags="ng" index="2t4tHJ" />
      <concept id="1175161300324" name="jetbrains.mps.baseLanguage.regexp.structure.LineEndRegexp" flags="ng" index="2t4AhP" />
      <concept id="6129327962763158517" name="jetbrains.mps.baseLanguage.regexp.structure.FindMatchExpression" flags="nn" index="3Dk_MV">
        <child id="6129327962763255289" name="inputExpression" index="3DkeaR" />
      </concept>
      <concept id="1174482753837" name="jetbrains.mps.baseLanguage.regexp.structure.StringLiteralRegexp" flags="ng" index="1OC9wW">
        <property id="1174482761807" name="text" index="1OCb_u" />
      </concept>
      <concept id="1174482804200" name="jetbrains.mps.baseLanguage.regexp.structure.PlusRegexp" flags="ng" index="1OClNT" />
      <concept id="1174484562151" name="jetbrains.mps.baseLanguage.regexp.structure.SeqRegexp" flags="ng" index="1OJ37Q" />
      <concept id="1174485167097" name="jetbrains.mps.baseLanguage.regexp.structure.BinaryRegexp" flags="ng" index="1OLmFC">
        <child id="1174485176897" name="left" index="1OLpdg" />
        <child id="1174485181039" name="right" index="1OLqdY" />
      </concept>
      <concept id="1174485235885" name="jetbrains.mps.baseLanguage.regexp.structure.UnaryRegexp" flags="ng" index="1OLBAW">
        <child id="1174485243418" name="regexp" index="1OLDsb" />
      </concept>
      <concept id="1174510540317" name="jetbrains.mps.baseLanguage.regexp.structure.InlineRegexpExpression" flags="nn" index="1Qi9sc">
        <child id="1174510571016" name="regexp" index="1QigWp" />
      </concept>
      <concept id="1174554186090" name="jetbrains.mps.baseLanguage.regexp.structure.SymbolClassRegexp" flags="ng" index="1SSD1V">
        <child id="1174557628217" name="part" index="1T5LoC" />
      </concept>
      <concept id="1174554211468" name="jetbrains.mps.baseLanguage.regexp.structure.PositiveSymbolClassRegexp" flags="ng" index="1SSJmt" />
      <concept id="1174557878319" name="jetbrains.mps.baseLanguage.regexp.structure.CharacterSymbolClassPart" flags="ng" index="1T6I$Y">
        <property id="1174557887320" name="character" index="1T6KD9" />
      </concept>
      <concept id="1174558792178" name="jetbrains.mps.baseLanguage.regexp.structure.PredefinedSymbolClassSymbolClassPart" flags="ng" index="1Tadzz">
        <reference id="1174558819022" name="declaration" index="1Takfv" />
      </concept>
      <concept id="1174564062919" name="jetbrains.mps.baseLanguage.regexp.structure.MatchParensRegexp" flags="ng" index="1Tukvm">
        <child id="1174564160889" name="regexp" index="1TuGhC" />
      </concept>
      <concept id="1174565027678" name="jetbrains.mps.baseLanguage.regexp.structure.MatchVariableReference" flags="nn" index="1TxZTf">
        <reference id="1174565035929" name="match" index="1Ty1U8" />
      </concept>
      <concept id="1174653354106" name="jetbrains.mps.baseLanguage.regexp.structure.RegexpUsingConstruction" flags="ng" index="1YMW5F">
        <child id="1174653387388" name="regexp" index="1YN4dH" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
    </language>
  </registry>
  <node concept="WtQ9Q" id="2GhJDbDjY1W">
    <ref role="WuzLi" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="11bSqf" id="2GhJDbDjY1Z" role="11c4hB">
      <node concept="3clFbS" id="2GhJDbDjY20" role="2VODD2">
        <node concept="3clFbJ" id="2NOvNR_w49d" role="3cqZAp">
          <node concept="3clFbS" id="2NOvNR_w49f" role="3clFbx">
            <node concept="lc7rE" id="2GhJDbDjY2e" role="3cqZAp">
              <node concept="l9hG8" id="2GhJDbDjY2s" role="lcghm">
                <node concept="2OqwBi" id="2GhJDbDjY5l" role="lb14g">
                  <node concept="117lpO" id="2GhJDbDjY3c" role="2Oq$k0" />
                  <node concept="3TrcHB" id="2GhJDbDjY9H" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="2GhJDbDkoY7" role="lcghm" />
            </node>
          </node>
          <node concept="3fqX7Q" id="2NOvNR_w4$W" role="3clFbw">
            <node concept="2OqwBi" id="2NOvNR_w4$Y" role="3fr31v">
              <node concept="117lpO" id="2NOvNR_w4$Z" role="2Oq$k0" />
              <node concept="3TrcHB" id="2NOvNR_w4_0" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2NOvNR_vw1V" resolve="isDeleted" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="2GhJDbDjYaP">
    <ref role="WuzLi" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="11bSqf" id="2GhJDbDjYaS" role="11c4hB">
      <node concept="3clFbS" id="2GhJDbDjYaT" role="2VODD2">
        <node concept="3clFbH" id="4a9_DK56aTZ" role="3cqZAp" />
        <node concept="3SKdUt" id="4a9_DK56b7f" role="3cqZAp">
          <node concept="3SKdUq" id="4a9_DK56b7h" role="3SKWNk">
            <property role="3SKdUp" value="#ifdef" />
          </node>
        </node>
        <node concept="3clFbJ" id="4a9_DK56mqP" role="3cqZAp">
          <node concept="3clFbS" id="4a9_DK56mqR" role="3clFbx">
            <node concept="lc7rE" id="4a9_DK56oFV" role="3cqZAp">
              <node concept="la8eA" id="4a9_DK56oFW" role="lcghm">
                <property role="lacIc" value="#ifdef " />
              </node>
              <node concept="l9hG8" id="4a9_DK56oFX" role="lcghm">
                <node concept="1TxZTf" id="4a9_DK56oGG" role="lb14g">
                  <ref role="1Ty1U8" node="4a9_DK56oCC" resolve="id" />
                </node>
              </node>
              <node concept="l8MVK" id="4a9_DK56oFZ" role="lcghm" />
            </node>
          </node>
          <node concept="3Dk_MV" id="4a9_DK56oo$" role="3clFbw">
            <node concept="2OqwBi" id="4a9_DK56mDl" role="3DkeaR">
              <node concept="117lpO" id="4a9_DK56mvQ" role="2Oq$k0" />
              <node concept="3TrcHB" id="4a9_DK56nsK" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
              </node>
            </node>
            <node concept="1Qi9sc" id="4a9_DK56oCy" role="1YN4dH">
              <node concept="1OJ37Q" id="4a9_DK56oCz" role="1QigWp">
                <node concept="1OJ37Q" id="4a9_DK56oC$" role="1OLqdY">
                  <node concept="2t4AhP" id="4a9_DK56oC_" role="1OLqdY" />
                  <node concept="1OJ37Q" id="4a9_DK56oCA" role="1OLpdg">
                    <node concept="1OC9wW" id="4a9_DK56oCB" role="1OLqdY">
                      <property role="1OCb_u" value=")" />
                    </node>
                    <node concept="1Tukvm" id="4a9_DK56oCC" role="1OLpdg">
                      <property role="TrG5h" value="id" />
                      <node concept="1OClNT" id="4a9_DK56oCD" role="1TuGhC">
                        <node concept="1SSJmt" id="4a9_DK56oCE" role="1OLDsb">
                          <node concept="1Tadzz" id="4a9_DK56oCF" role="1T5LoC">
                            <ref role="1Takfv" to="tpfp:h5SUJUw" resolve="\w" />
                          </node>
                          <node concept="1Tadzz" id="4a9_DK56oCG" role="1T5LoC">
                            <ref role="1Takfv" to="tpfp:h5SUwpi" resolve="\d" />
                          </node>
                          <node concept="1T6I$Y" id="4a9_DK56oCH" role="1T5LoC">
                            <property role="1T6KD9" value="_" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1OJ37Q" id="4a9_DK56oCI" role="1OLpdg">
                  <node concept="2t4tHJ" id="4a9_DK56oCJ" role="1OLpdg" />
                  <node concept="1OC9wW" id="4a9_DK56oCK" role="1OLqdY">
                    <property role="1OCb_u" value="defined(" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="4a9_DK56rLH" role="3eNLev">
            <node concept="3clFbS" id="4a9_DK56rLI" role="3eOfB_">
              <node concept="3SKdUt" id="4a9_DK56bfG" role="3cqZAp">
                <node concept="3SKdUq" id="4a9_DK56bfI" role="3SKWNk">
                  <property role="3SKdUp" value="#ifndef" />
                </node>
              </node>
              <node concept="lc7rE" id="4a9_DK56s0C" role="3cqZAp">
                <node concept="la8eA" id="4a9_DK56s0D" role="lcghm">
                  <property role="lacIc" value="#ifndef " />
                </node>
                <node concept="l9hG8" id="4a9_DK56s0E" role="lcghm">
                  <node concept="1TxZTf" id="4a9_DK56s0F" role="lb14g">
                    <ref role="1Ty1U8" node="4a9_DK56rXB" resolve="id" />
                  </node>
                </node>
                <node concept="l8MVK" id="4a9_DK56s0G" role="lcghm" />
              </node>
            </node>
            <node concept="3Dk_MV" id="4a9_DK56rXt" role="3eO9$A">
              <node concept="2OqwBi" id="4a9_DK56rXu" role="3DkeaR">
                <node concept="117lpO" id="4a9_DK56rXv" role="2Oq$k0" />
                <node concept="3TrcHB" id="4a9_DK56rXw" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                </node>
              </node>
              <node concept="1Qi9sc" id="4a9_DK56rXx" role="1YN4dH">
                <node concept="1OJ37Q" id="4a9_DK56rXy" role="1QigWp">
                  <node concept="1OJ37Q" id="4a9_DK56rXz" role="1OLqdY">
                    <node concept="2t4AhP" id="4a9_DK56rX$" role="1OLqdY" />
                    <node concept="1OJ37Q" id="4a9_DK56rX_" role="1OLpdg">
                      <node concept="1OC9wW" id="4a9_DK56rXA" role="1OLqdY">
                        <property role="1OCb_u" value=")" />
                      </node>
                      <node concept="1Tukvm" id="4a9_DK56rXB" role="1OLpdg">
                        <property role="TrG5h" value="id" />
                        <node concept="1OClNT" id="4a9_DK56rXC" role="1TuGhC">
                          <node concept="1SSJmt" id="4a9_DK56rXD" role="1OLDsb">
                            <node concept="1Tadzz" id="4a9_DK56rXE" role="1T5LoC">
                              <ref role="1Takfv" to="tpfp:h5SUJUw" resolve="\w" />
                            </node>
                            <node concept="1Tadzz" id="4a9_DK56rXF" role="1T5LoC">
                              <ref role="1Takfv" to="tpfp:h5SUwpi" resolve="\d" />
                            </node>
                            <node concept="1T6I$Y" id="4a9_DK56rXG" role="1T5LoC">
                              <property role="1T6KD9" value="_" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="1OJ37Q" id="4a9_DK56rXH" role="1OLpdg">
                    <node concept="2t4tHJ" id="4a9_DK56rXI" role="1OLpdg" />
                    <node concept="1OC9wW" id="4a9_DK56rXJ" role="1OLqdY">
                      <property role="1OCb_u" value="!defined(" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="4a9_DK56rRH" role="9aQIa">
            <node concept="3clFbS" id="4a9_DK56rRI" role="9aQI4">
              <node concept="lc7rE" id="4a9_DK56rLJ" role="3cqZAp">
                <node concept="la8eA" id="4a9_DK56rLK" role="lcghm">
                  <property role="lacIc" value="#if " />
                </node>
                <node concept="l9hG8" id="4a9_DK56rLL" role="lcghm">
                  <node concept="2OqwBi" id="4a9_DK56rLM" role="lb14g">
                    <node concept="117lpO" id="4a9_DK56rLN" role="2Oq$k0" />
                    <node concept="3TrcHB" id="4a9_DK56rLO" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                    </node>
                  </node>
                </node>
                <node concept="l8MVK" id="4a9_DK56rLP" role="lcghm" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4a9_DK56aZg" role="3cqZAp" />
        <node concept="lc7rE" id="2GhJDbDjYtk" role="3cqZAp">
          <node concept="l9S2W" id="2GhJDbDjYuA" role="lcghm">
            <node concept="2OqwBi" id="2GhJDbDjYwE" role="lbANJ">
              <node concept="117lpO" id="2GhJDbDjYuT" role="2Oq$k0" />
              <node concept="3Tsc0h" id="2GhJDbDjY$x" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="2GhJDbDjYAK" role="3cqZAp">
          <node concept="l9S2W" id="2GhJDbDjYCe" role="lcghm">
            <node concept="2OqwBi" id="2GhJDbDjYEe" role="lbANJ">
              <node concept="117lpO" id="2GhJDbDjYCx" role="2Oq$k0" />
              <node concept="3Tsc0h" id="2GhJDbDjYRs" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2GhJDbDjZEZ" role="3cqZAp">
          <node concept="3clFbS" id="2GhJDbDjZF1" role="3clFbx">
            <node concept="lc7rE" id="2GhJDbDk1Am" role="3cqZAp">
              <node concept="la8eA" id="2GhJDbDk1Bm" role="lcghm">
                <property role="lacIc" value="#else" />
              </node>
              <node concept="l8MVK" id="2GhJDbDk1BZ" role="lcghm" />
            </node>
            <node concept="lc7rE" id="2GhJDbDk1lG" role="3cqZAp">
              <node concept="l9S2W" id="2GhJDbDk1lZ" role="lcghm">
                <node concept="2OqwBi" id="2GhJDbDk1nZ" role="lbANJ">
                  <node concept="117lpO" id="2GhJDbDk1mi" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="2GhJDbDk1vd" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2GhJDbDk0a6" role="3clFbw">
            <node concept="2OqwBi" id="2GhJDbDjZIN" role="2Oq$k0">
              <node concept="117lpO" id="2GhJDbDjZGK" role="2Oq$k0" />
              <node concept="3Tsc0h" id="2GhJDbDjZMF" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
              </node>
            </node>
            <node concept="3GX2aA" id="2GhJDbDk1ko" role="2OqNvi" />
          </node>
        </node>
        <node concept="lc7rE" id="2GhJDbDk1yy" role="3cqZAp">
          <node concept="la8eA" id="2GhJDbDk1_6" role="lcghm">
            <property role="lacIc" value="#endif" />
          </node>
          <node concept="la8eA" id="4a9_DK56vXp" role="lcghm">
            <property role="lacIc" value=" /* " />
          </node>
          <node concept="l9hG8" id="4a9_DK56vYm" role="lcghm">
            <node concept="2OqwBi" id="4a9_DK56w96" role="lb14g">
              <node concept="117lpO" id="4a9_DK56vZh" role="2Oq$k0" />
              <node concept="3TrcHB" id="4a9_DK56wo7" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="4a9_DK56wtd" role="lcghm">
            <property role="lacIc" value=" */" />
          </node>
          <node concept="l8MVK" id="2GhJDbDk1_N" role="lcghm" />
        </node>
        <node concept="3clFbH" id="2GhJDbDjZux" role="3cqZAp" />
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="2GhJDbDknwW">
    <ref role="WuzLi" to="4s7a:2s5q4UVEuQ" resolve="MacroDefine" />
    <node concept="11bSqf" id="2GhJDbDknwZ" role="11c4hB">
      <node concept="3clFbS" id="2GhJDbDknx0" role="2VODD2">
        <node concept="lc7rE" id="2GhJDbDknxe" role="3cqZAp">
          <node concept="la8eA" id="2GhJDbDknxs" role="lcghm">
            <property role="lacIc" value="#define " />
          </node>
          <node concept="l9hG8" id="2GhJDbDknya" role="lcghm">
            <node concept="2OqwBi" id="2GhJDbDkn$Z" role="lb14g">
              <node concept="117lpO" id="2GhJDbDknyR" role="2Oq$k0" />
              <node concept="3TrcHB" id="2GhJDbDknDj" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2s5q4UXAbU" resolve="name" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2GhJDbDlNS_" role="3cqZAp">
          <node concept="3clFbS" id="2GhJDbDlNSB" role="3clFbx">
            <node concept="lc7rE" id="2GhJDbDlPWc" role="3cqZAp">
              <node concept="la8eA" id="2GhJDbDlPWs" role="lcghm">
                <property role="lacIc" value="(" />
              </node>
              <node concept="l9S2W" id="2GhJDbDlPWP" role="lcghm">
                <property role="XA4eZ" value="true" />
                <property role="lbP0B" value=", " />
                <node concept="2OqwBi" id="2GhJDbDlPYO" role="lbANJ">
                  <node concept="117lpO" id="2GhJDbDlPX6" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="2GhJDbDlQ2C" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:2GhJDbDlGI4" resolve="args" />
                  </node>
                </node>
              </node>
              <node concept="la8eA" id="2GhJDbDlQ65" role="lcghm">
                <property role="lacIc" value=")" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2GhJDbDlOwu" role="3clFbw">
            <node concept="2OqwBi" id="2GhJDbDlNWa" role="2Oq$k0">
              <node concept="117lpO" id="2GhJDbDlNU7" role="2Oq$k0" />
              <node concept="3Tsc0h" id="2GhJDbDlO3m" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:2GhJDbDlGI4" resolve="args" />
              </node>
            </node>
            <node concept="3GX2aA" id="2GhJDbDlPUP" role="2OqNvi" />
          </node>
        </node>
        <node concept="lc7rE" id="2GhJDbDlNGS" role="3cqZAp">
          <node concept="la8eA" id="2GhJDbDknEU" role="lcghm">
            <property role="lacIc" value=" " />
          </node>
          <node concept="l9S2W" id="2GhJDbDkQ7K" role="lcghm">
            <property role="XA4eZ" value="true" />
            <property role="lbP0B" value="\n" />
            <node concept="2OqwBi" id="2GhJDbDkQaN" role="lbANJ">
              <node concept="117lpO" id="2GhJDbDkQ92" role="2Oq$k0" />
              <node concept="3Tsc0h" id="2GhJDbDkQi1" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:2GhJDbDkLt$" resolve="content" />
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="6uMQMDgKxAJ" role="3cqZAp">
          <node concept="l8MVK" id="6uMQMDgKxEJ" role="lcghm" />
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="2GhJDbDkofV">
    <ref role="WuzLi" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="11bSqf" id="2GhJDbDkofY" role="11c4hB">
      <node concept="3clFbS" id="2GhJDbDkofZ" role="2VODD2">
        <node concept="lc7rE" id="2GhJDbDkor1" role="3cqZAp">
          <node concept="l9S2W" id="2GhJDbDkorf" role="lcghm">
            <node concept="2OqwBi" id="2GhJDbDkot8" role="lbANJ">
              <node concept="117lpO" id="2GhJDbDkorr" role="2Oq$k0" />
              <node concept="3Tsc0h" id="2GhJDbDkowS" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:2s5q4UUgwl" resolve="statements" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="2GhJDbDlgzg">
    <ref role="WuzLi" to="4s7a:2GhJDbDlgzd" resolve="MacroLine" />
    <node concept="11bSqf" id="2GhJDbDlgzh" role="11c4hB">
      <node concept="3clFbS" id="2GhJDbDlgzi" role="2VODD2">
        <node concept="lc7rE" id="2GhJDbDlgzu" role="3cqZAp">
          <node concept="l9hG8" id="2GhJDbDlgzG" role="lcghm">
            <node concept="2OqwBi" id="2GhJDbDlgA4" role="lb14g">
              <node concept="117lpO" id="2GhJDbDlg$o" role="2Oq$k0" />
              <node concept="3TrcHB" id="2GhJDbDlgDq" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2GhJDbDlgze" resolve="content" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="5KGsxZrwe$5">
    <ref role="WuzLi" to="4s7a:2s5q4UUeXK" resolve="InclusionDirective" />
    <node concept="11bSqf" id="5KGsxZrweFq" role="11c4hB">
      <node concept="3clFbS" id="5KGsxZrweFr" role="2VODD2">
        <node concept="lc7rE" id="5KGsxZrweFL" role="3cqZAp">
          <node concept="la8eA" id="5KGsxZrweG7" role="lcghm">
            <property role="lacIc" value="#include " />
          </node>
          <node concept="l9hG8" id="5KGsxZrweIP" role="lcghm">
            <node concept="2OqwBi" id="5KGsxZrweQG" role="lb14g">
              <node concept="117lpO" id="5KGsxZrweJI" role="2Oq$k0" />
              <node concept="3TrcHB" id="5KGsxZrweYM" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2s5q4UUeXL" resolve="filename" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="4GHRgmPM8jq">
    <ref role="WuzLi" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
    <node concept="11bSqf" id="4GHRgmPM8jt" role="11c4hB">
      <node concept="3clFbS" id="4GHRgmPM8ju" role="2VODD2">
        <node concept="lc7rE" id="4GHRgmPM8o6" role="3cqZAp">
          <node concept="la8eA" id="4GHRgmPM8ov" role="lcghm">
            <property role="lacIc" value="#elif " />
          </node>
          <node concept="l9hG8" id="4GHRgmPM8r2" role="lcghm">
            <node concept="2OqwBi" id="4GHRgmPM8yw" role="lb14g">
              <node concept="117lpO" id="4GHRgmPM8rY" role="2Oq$k0" />
              <node concept="3TrcHB" id="4GHRgmPM8J2" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:3fq3ZRIT59L" resolve="condition" />
              </node>
            </node>
          </node>
          <node concept="l8MVK" id="4GHRgmPM8O2" role="lcghm" />
        </node>
        <node concept="lc7rE" id="4GHRgmPMaG3" role="3cqZAp">
          <node concept="l9S2W" id="4GHRgmPMaIF" role="lcghm">
            <property role="XA4eZ" value="true" />
            <property role="lbP0B" value="\n" />
            <node concept="2OqwBi" id="4GHRgmPMbue" role="lbANJ">
              <node concept="117lpO" id="4GHRgmPMaJ6" role="2Oq$k0" />
              <node concept="3Tsc0h" id="4GHRgmPMbEc" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:3fq3ZRIT59N" resolve="branch" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

