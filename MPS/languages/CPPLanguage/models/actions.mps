<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ec62aa44-0231-4fd4-8c73-491ff8dc2687(CPPLanguage.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="3" />
  </languages>
  <imports>
    <import index="vt3f" ref="r:9545cdb8-8b5f-4177-be46-4ce51391ab21(CPPLanguage.editor)" />
    <import index="4s7a" ref="r:c67970af-4844-47d5-9471-54e2bda5945e(CPPLanguage.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="1235051137001" name="jetbrains.mps.lang.actions.structure.SmartEditorActions" flags="ng" index="2eMcrQ" />
      <concept id="562388756457499018" name="jetbrains.mps.lang.actions.structure.MigratedToAnnotation" flags="ng" index="xBawi">
        <reference id="562388756457499129" name="migratedTo" index="xBaxx" />
      </concept>
      <concept id="1138079221458" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActionsBuilder" flags="ig" index="3UNGvq">
        <reference id="1138079221462" name="applicableConcept" index="3UNGvu" />
      </concept>
      <concept id="1138079416598" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActions" flags="ng" index="3UOs0u" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
  </registry>
  <node concept="3UOs0u" id="2NOvNR_woqJ">
    <property role="TrG5h" value="test" />
    <node concept="1X3_iC" id="3Gq9cqnzLgR" role="lGtFl">
      <property role="3V$3am" value="actionsBuilder" />
      <property role="3V$3ak" value="aee9cad2-acd4-4608-aef2-0004f6a1cdbd/1138079416598/1138079416599" />
      <node concept="3UNGvq" id="2NOvNR_woqK" role="8Wnug">
        <ref role="3UNGvu" to="4s7a:3VMRtc4W287" resolve="CPP" />
        <node concept="xBawi" id="3Gq9cqnzLgQ" role="lGtFl">
          <ref role="xBaxx" to="vt3f:3Gq9cqnzLgH" resolve="CPP_TransformationMenu" />
        </node>
      </node>
    </node>
  </node>
  <node concept="2eMcrQ" id="5lhzOSqx4uS">
    <property role="TrG5h" value="h" />
  </node>
</model>

