<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:37d2e8f9-ac5a-49aa-9980-0dedb6738a39(CPPLanguage.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="-1" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <use id="6150e321-6a06-49cb-8a7d-9facbb581dc2" name="CPPLanguage" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="7zqe" ref="r:7e3a84cb-38df-4db2-adea-884d8987c992(CPPLanguage.behavior)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="9683" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:dk.itu.vts.parser(CPPLanguage/)" />
    <import index="kjvu" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:scala.util(CPPLanguage/)" />
    <import index="47h6" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:scala.collection.immutable(CPPLanguage/)" />
    <import index="w4xx" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:dk.itu.vts.model(CPPLanguage/)" />
    <import index="d64m" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:scala.collection(CPPLanguage/)" />
    <import index="m798" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:dk.itu.vts.expression(CPPLanguage/)" />
    <import index="7dys" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:dk.itu.vts.editing(CPPLanguage/)" />
    <import index="ctra" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:dk.itu.vts.util(CPPLanguage/)" />
    <import index="btm1" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:org.apache.commons.lang3(CPPLanguage/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="cmfw" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.event(MPS.OpenAPI/)" />
    <import index="t6h5" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang.reflect(JDK/)" />
    <import index="g3l6" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.extapi.model(MPS.Core/)" />
    <import index="j9co" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.event(MPS.Core/)" />
    <import index="lui2" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.module(MPS.OpenAPI/)" />
    <import index="mk8z" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.progress(MPS.Core/)" />
    <import index="9w4s" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.util(MPS.IDEA/)" />
    <import index="4s7a" ref="r:c67970af-4844-47d5-9471-54e2bda5945e(CPPLanguage.structure)" />
    <import index="bd8o" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.application(MPS.IDEA/)" />
    <import index="xygl" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.progress(MPS.IDEA/)" />
    <import index="18ew" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.util(MPS.Core/)" />
    <import index="4nm9" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.project(MPS.IDEA/)" />
    <import index="alof" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.ide.project(MPS.Platform/)" />
    <import index="mk90" ref="742f6602-5a2f-4313-aa6e-ae1cd4ffdc61/java:jetbrains.mps.progress(MPS.Platform/)" />
    <import index="yyf4" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.util(MPS.OpenAPI/)" />
    <import index="cj4x" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor(MPS.Editor/)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" implicit="true" />
    <import index="1miu" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:scala(CPPLanguage/)" implicit="true" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1194033889146" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1XNTG" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1224071154655" name="jetbrains.mps.baseLanguage.structure.AsExpression" flags="nn" index="0kSF2">
        <child id="1224071154657" name="classifierType" index="0kSFW" />
        <child id="1224071154656" name="expression" index="0kSFX" />
      </concept>
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1215695189714" name="jetbrains.mps.baseLanguage.structure.PlusAssignmentExpression" flags="nn" index="d57v9" />
      <concept id="1153422305557" name="jetbrains.mps.baseLanguage.structure.LessThanOrEqualsExpression" flags="nn" index="2dkUwp" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1153952380246" name="jetbrains.mps.baseLanguage.structure.TryStatement" flags="nn" index="2GUZhq">
        <child id="1153952416686" name="body" index="2GV8ay" />
        <child id="1153952429843" name="finallyBody" index="2GVbov" />
        <child id="1164903700860" name="catchClause" index="TEXxN" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1164879751025" name="jetbrains.mps.baseLanguage.structure.TryCatchStatement" flags="nn" index="SfApY">
        <child id="1164879758292" name="body" index="SfCbr" />
        <child id="1164903496223" name="catchClause" index="TEbGg" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1164903280175" name="jetbrains.mps.baseLanguage.structure.CatchClause" flags="nn" index="TDmWw">
        <child id="1164903359218" name="catchBody" index="TDEfX" />
        <child id="1164903359217" name="throwable" index="TDEfY" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1081256982272" name="jetbrains.mps.baseLanguage.structure.InstanceOfExpression" flags="nn" index="2ZW3vV">
        <child id="1081256993305" name="classType" index="2ZW6by" />
        <child id="1081256993304" name="leftExpression" index="2ZW6bz" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144226303539" name="jetbrains.mps.baseLanguage.structure.ForeachStatement" flags="nn" index="1DcWWT">
        <child id="1144226360166" name="iterable" index="1DdaDG" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795771125" name="jetbrains.mps.lang.intentions.structure.IsApplicableBlock" flags="in" index="2SaL7w" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <property id="2522969319638091386" name="isAvailableInChildNodes" index="2ZfUl0" />
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093995" name="isApplicableFunction" index="2ZfVeh" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
      <concept id="1240316299033" name="jetbrains.mps.lang.intentions.structure.QueryBlock" flags="in" index="38BcoT">
        <child id="1240393479918" name="paramType" index="3ddBve" />
      </concept>
      <concept id="1240322627579" name="jetbrains.mps.lang.intentions.structure.IntentionParameter" flags="nn" index="38Zlrr" />
      <concept id="1240395258925" name="jetbrains.mps.lang.intentions.structure.ParameterizedIntentionDeclaration" flags="ig" index="3dkpOd">
        <child id="1240395532443" name="queryBlock" index="3dlsAV" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167228628751" name="hasException" index="34fQS0" />
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
        <child id="1167227561449" name="exception" index="34bMjA" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <reference id="6733348108486823428" name="concept" index="1m5ApE" />
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
      </concept>
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1171310072040" name="jetbrains.mps.lang.smodel.structure.Node_GetContainingRootOperation" flags="nn" index="2Rxl7S" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1172326502327" name="jetbrains.mps.lang.smodel.structure.Concept_IsExactlyOperation" flags="nn" index="3O6GUB">
        <child id="1206733650006" name="conceptArgument" index="3QVz_e" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <property id="1238684351431" name="asCast" index="1BlNFB" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1151702311717" name="jetbrains.mps.baseLanguage.collections.structure.ToListOperation" flags="nn" index="ANE8D" />
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1162934736510" name="jetbrains.mps.baseLanguage.collections.structure.GetElementOperation" flags="nn" index="34jXtK" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1201792049884" name="jetbrains.mps.baseLanguage.collections.structure.TranslateOperation" flags="nn" index="3goQfb" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="2S6QgY" id="4P2s9PP_D3c">
    <property role="TrG5h" value="CheckSAT" />
    <ref role="2ZfgGC" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="2S6ZIM" id="4P2s9PP_D3d" role="2ZfVej">
      <node concept="3clFbS" id="4P2s9PP_D3e" role="2VODD2">
        <node concept="3clFbF" id="4P2s9PP_D4_" role="3cqZAp">
          <node concept="Xl_RD" id="4P2s9PP_D4$" role="3clFbG">
            <property role="Xl_RC" value="Check SAT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="4P2s9PP_D3f" role="2ZfgGD">
      <node concept="3clFbS" id="4P2s9PP_D3g" role="2VODD2">
        <node concept="3cpWs8" id="4P2s9PP_Eqv" role="3cqZAp">
          <node concept="3cpWsn" id="4P2s9PP_Eqy" role="3cpWs9">
            <property role="TrG5h" value="smt" />
            <node concept="17QB3L" id="4P2s9PP_Eqt" role="1tU5fm" />
            <node concept="3cpWs3" id="4P2s9PP_EFI" role="33vP2m">
              <node concept="Xl_RD" id="4P2s9PP_EFO" role="3uHU7w">
                <property role="Xl_RC" value=")\n" />
              </node>
              <node concept="3cpWs3" id="4P2s9PP_Ey0" role="3uHU7B">
                <node concept="Xl_RD" id="4P2s9PP_Esx" role="3uHU7B">
                  <property role="Xl_RC" value="(assert " />
                </node>
                <node concept="2OqwBi" id="4P2s9PP_E$N" role="3uHU7w">
                  <node concept="2Sf5sV" id="4P2s9PP_Eyv" role="2Oq$k0" />
                  <node concept="2qgKlT" id="4P2s9PP_ECN" role="2OqNvi">
                    <ref role="37wK5l" to="7zqe:79Do2olGns2" resolve="buildConstraints" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PP_F8B" role="3cqZAp" />
        <node concept="3clFbJ" id="4P2s9PP_Fi4" role="3cqZAp">
          <node concept="3clFbS" id="4P2s9PP_Fi6" role="3clFbx">
            <node concept="3clFbF" id="4P2s9PP_G_8" role="3cqZAp">
              <node concept="d57v9" id="4P2s9PP_GBp" role="3clFbG">
                <node concept="3cpWs3" id="4P2s9PP_GUz" role="37vLTx">
                  <node concept="Xl_RD" id="4P2s9PP_GX6" role="3uHU7w">
                    <property role="Xl_RC" value=")\n" />
                  </node>
                  <node concept="3cpWs3" id="4P2s9PP_GJI" role="3uHU7B">
                    <node concept="Xl_RD" id="4P2s9PP_GBM" role="3uHU7B">
                      <property role="Xl_RC" value="(assert " />
                    </node>
                    <node concept="2OqwBi" id="4P2s9PP_GK4" role="3uHU7w">
                      <node concept="2OqwBi" id="4P2s9PP_GK5" role="2Oq$k0">
                        <node concept="2Sf5sV" id="4P2s9PP_GK6" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="4P2s9PP_GK7" role="2OqNvi">
                          <node concept="1xMEDy" id="4P2s9PP_GK8" role="1xVPHs">
                            <node concept="chp4Y" id="4P2s9PP_GK9" role="ri$Ld">
                              <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3TrcHB" id="4P2s9PP_GKa" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="4P2s9PP_G_6" role="37vLTJ">
                  <ref role="3cqZAo" node="4P2s9PP_Eqy" resolve="smt" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4P2s9PP_G8k" role="3clFbw">
            <node concept="2OqwBi" id="4P2s9PP_FSr" role="2Oq$k0">
              <node concept="2OqwBi" id="4P2s9PP_FH1" role="2Oq$k0">
                <node concept="2Sf5sV" id="4P2s9PP_FDU" role="2Oq$k0" />
                <node concept="2Xjw5R" id="4P2s9PP_FPg" role="2OqNvi">
                  <node concept="1xMEDy" id="4P2s9PP_FPi" role="1xVPHs">
                    <node concept="chp4Y" id="4P2s9PP_FPY" role="ri$Ld">
                      <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3TrcHB" id="4P2s9PP_G2h" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
              </node>
            </node>
            <node concept="17RvpY" id="4P2s9PP_GzQ" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PP_Fcq" role="3cqZAp" />
        <node concept="3clFbF" id="4P2s9PP_DPS" role="3cqZAp">
          <node concept="2YIFZM" id="4P2s9PP_DRp" role="3clFbG">
            <ref role="37wK5l" to="7zqe:79Do2olIt_i" resolve="solve" />
            <ref role="1Pybhc" to="7zqe:79Do2olI4S$" resolve="MySolver" />
            <node concept="37vLTw" id="4P2s9PP_EJs" role="37wK5m">
              <ref role="3cqZAo" node="4P2s9PP_Eqy" resolve="smt" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="4P2s9PP_Hiv">
    <property role="TrG5h" value="HideUNSAT" />
    <ref role="2ZfgGC" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="2S6ZIM" id="4P2s9PP_Hiw" role="2ZfVej">
      <node concept="3clFbS" id="4P2s9PP_Hix" role="2VODD2">
        <node concept="3clFbF" id="4P2s9PP_HjW" role="3cqZAp">
          <node concept="Xl_RD" id="4P2s9PP_HjV" role="3clFbG">
            <property role="Xl_RC" value="Hide UNSAT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="4P2s9PP_Hiy" role="2ZfgGD">
      <node concept="3clFbS" id="4P2s9PP_Hiz" role="2VODD2">
        <node concept="3clFbH" id="4P2s9PPAoCw" role="3cqZAp" />
        <node concept="3SKdUt" id="4P2s9PPArd5" role="3cqZAp">
          <node concept="3SKdUq" id="4P2s9PPArd7" role="3SKWNk">
            <property role="3SKdUp" value="Reset" />
          </node>
        </node>
        <node concept="3clFbF" id="4P2s9PPAsG2" role="3cqZAp">
          <node concept="2OqwBi" id="4P2s9PPAtFc" role="3clFbG">
            <node concept="2OqwBi" id="4P2s9PPAt9A" role="2Oq$k0">
              <node concept="2Sf5sV" id="4P2s9PPAsG0" role="2Oq$k0" />
              <node concept="2Rf3mk" id="4P2s9PPAtm3" role="2OqNvi">
                <node concept="1xMEDy" id="4P2s9PPAtm5" role="1xVPHs">
                  <node concept="chp4Y" id="4P2s9PPAtoi" role="ri$Ld">
                    <ref role="cht4Q" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2es0OD" id="4P2s9PPAuWN" role="2OqNvi">
              <node concept="1bVj0M" id="4P2s9PPAuWP" role="23t8la">
                <node concept="3clFbS" id="4P2s9PPAuWQ" role="1bW5cS">
                  <node concept="3clFbF" id="4P2s9PPAuZ9" role="3cqZAp">
                    <node concept="37vLTI" id="4P2s9PPAvd1" role="3clFbG">
                      <node concept="3clFbT" id="4P2s9PPAvgk" role="37vLTx">
                        <property role="3clFbU" value="false" />
                      </node>
                      <node concept="2OqwBi" id="4P2s9PPAv1M" role="37vLTJ">
                        <node concept="37vLTw" id="4P2s9PPAuZ8" role="2Oq$k0">
                          <ref role="3cqZAo" node="4P2s9PPAuWR" resolve="it" />
                        </node>
                        <node concept="3TrcHB" id="4P2s9PPAv7n" role="2OqNvi">
                          <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="4P2s9PPAuWR" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="4P2s9PPAuWS" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PPAsbc" role="3cqZAp" />
        <node concept="1DcWWT" id="4P2s9PP_HOg" role="3cqZAp">
          <node concept="3clFbS" id="4P2s9PP_HOi" role="2LFqv$">
            <node concept="34ab3g" id="4P2s9PPAy5v" role="3cqZAp">
              <property role="35gtTG" value="warn" />
              <node concept="3cpWs3" id="4P2s9PPAyeI" role="34bqiv">
                <node concept="Xl_RD" id="4P2s9PPAy5x" role="3uHU7B">
                  <property role="Xl_RC" value="TrueBranchSat " />
                </node>
                <node concept="2OqwBi" id="4P2s9PPAyfc" role="3uHU7w">
                  <node concept="37vLTw" id="4P2s9PPAyfd" role="2Oq$k0">
                    <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                  </node>
                  <node concept="2qgKlT" id="4P2s9PPAyfe" role="2OqNvi">
                    <ref role="37wK5l" to="7zqe:4P2s9PP_NZt" resolve="TrueBranchSAT" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="34ab3g" id="4P2s9PPAyjh" role="3cqZAp">
              <property role="35gtTG" value="warn" />
              <node concept="3cpWs3" id="4P2s9PPAyji" role="34bqiv">
                <node concept="Xl_RD" id="4P2s9PPAyjj" role="3uHU7B">
                  <property role="Xl_RC" value="ElseBranchSat " />
                </node>
                <node concept="2OqwBi" id="4P2s9PPAyjk" role="3uHU7w">
                  <node concept="37vLTw" id="4P2s9PPAyjl" role="2Oq$k0">
                    <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                  </node>
                  <node concept="2qgKlT" id="4P2s9PPAyvw" role="2OqNvi">
                    <ref role="37wK5l" to="7zqe:4P2s9PPAkgR" resolve="ElseBranchSAT" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="4P2s9PPAyha" role="3cqZAp" />
            <node concept="3clFbJ" id="4P2s9PP_OW1" role="3cqZAp">
              <node concept="3clFbS" id="4P2s9PP_OW2" role="3clFbx">
                <node concept="1DcWWT" id="4P2s9PP_Pz$" role="3cqZAp">
                  <node concept="3cpWsn" id="4P2s9PP_Pz_" role="1Duv9x">
                    <property role="TrG5h" value="e" />
                    <node concept="3Tqbb2" id="4P2s9PP_PAp" role="1tU5fm">
                      <ref role="ehGHo" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                    </node>
                  </node>
                  <node concept="3clFbS" id="4P2s9PP_PzA" role="2LFqv$">
                    <node concept="3clFbF" id="4P2s9PP_QgV" role="3cqZAp">
                      <node concept="37vLTI" id="4P2s9PP_Qr4" role="3clFbG">
                        <node concept="3clFbT" id="4P2s9PP_Qrz" role="37vLTx">
                          <property role="3clFbU" value="true" />
                        </node>
                        <node concept="2OqwBi" id="4P2s9PP_Qi8" role="37vLTJ">
                          <node concept="37vLTw" id="4P2s9PP_QgU" role="2Oq$k0">
                            <ref role="3cqZAo" node="4P2s9PP_Pz_" resolve="e" />
                          </node>
                          <node concept="3TrcHB" id="4P2s9PP_QmS" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="4P2s9PP_PL7" role="1DdaDG">
                    <node concept="37vLTw" id="4P2s9PP_PGQ" role="2Oq$k0">
                      <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                    </node>
                    <node concept="3Tsc0h" id="4P2s9PPAhJW" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="7HV5jaFsJQQ" role="3cqZAp">
                  <node concept="3clFbS" id="7HV5jaFsJQS" role="3clFbx">
                    <node concept="3clFbF" id="7HV5jaFsLTo" role="3cqZAp">
                      <node concept="37vLTI" id="7HV5jaFsM7X" role="3clFbG">
                        <node concept="3clFbT" id="7HV5jaFsM8p" role="37vLTx">
                          <property role="3clFbU" value="true" />
                        </node>
                        <node concept="2OqwBi" id="7HV5jaFsLV3" role="37vLTJ">
                          <node concept="37vLTw" id="7HV5jaFsLTm" role="2Oq$k0">
                            <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                          </node>
                          <node concept="3TrcHB" id="7HV5jaFsM26" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="7HV5jaFsKtH" role="3clFbw">
                    <node concept="2OqwBi" id="7HV5jaFsJTw" role="2Oq$k0">
                      <node concept="37vLTw" id="7HV5jaFsJRp" role="2Oq$k0">
                        <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                      </node>
                      <node concept="3Tsc0h" id="7HV5jaFsK0F" role="2OqNvi">
                        <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                      </node>
                    </node>
                    <node concept="1v1jN8" id="7HV5jaFsLSB" role="2OqNvi" />
                  </node>
                </node>
              </node>
              <node concept="3fqX7Q" id="4P2s9PPAfVf" role="3clFbw">
                <node concept="2OqwBi" id="4P2s9PPAfVh" role="3fr31v">
                  <node concept="37vLTw" id="4P2s9PPAfVi" role="2Oq$k0">
                    <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                  </node>
                  <node concept="2qgKlT" id="4P2s9PPAfVj" role="2OqNvi">
                    <ref role="37wK5l" to="7zqe:4P2s9PP_NZt" resolve="TrueBranchSAT" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="7Y4iKHGsvnc" role="3cqZAp" />
            <node concept="1DcWWT" id="7Y4iKHGsvH9" role="3cqZAp">
              <node concept="3clFbS" id="7Y4iKHGsvHb" role="2LFqv$">
                <node concept="3clFbJ" id="7Y4iKHGsBzW" role="3cqZAp">
                  <node concept="3clFbS" id="7Y4iKHGsBzX" role="3clFbx">
                    <node concept="1DcWWT" id="7Y4iKHGsBGC" role="3cqZAp">
                      <node concept="3cpWsn" id="7Y4iKHGsBGD" role="1Duv9x">
                        <property role="TrG5h" value="e" />
                        <node concept="3Tqbb2" id="7Y4iKHGsBIm" role="1tU5fm">
                          <ref role="ehGHo" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="7Y4iKHGsBGE" role="2LFqv$">
                        <node concept="3clFbF" id="7Y4iKHGsCcM" role="3cqZAp">
                          <node concept="37vLTI" id="7Y4iKHGsCmV" role="3clFbG">
                            <node concept="3clFbT" id="7Y4iKHGsCnq" role="37vLTx">
                              <property role="3clFbU" value="true" />
                            </node>
                            <node concept="2OqwBi" id="7Y4iKHGsCdZ" role="37vLTJ">
                              <node concept="37vLTw" id="7Y4iKHGsCcL" role="2Oq$k0">
                                <ref role="3cqZAo" node="7Y4iKHGsBGD" resolve="e" />
                              </node>
                              <node concept="3TrcHB" id="7Y4iKHGsCiJ" role="2OqNvi">
                                <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="7Y4iKHGsBOV" role="1DdaDG">
                        <node concept="37vLTw" id="7Y4iKHGsBLu" role="2Oq$k0">
                          <ref role="3cqZAo" node="7Y4iKHGsvHd" resolve="elseIf" />
                        </node>
                        <node concept="3Tsc0h" id="7Y4iKHGsBTr" role="2OqNvi">
                          <ref role="3TtcxE" to="4s7a:3fq3ZRIT59N" resolve="branch" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3fqX7Q" id="7Y4iKHGsB$e" role="3clFbw">
                    <node concept="2OqwBi" id="7Y4iKHGsBAT" role="3fr31v">
                      <node concept="37vLTw" id="7Y4iKHGsB_9" role="2Oq$k0">
                        <ref role="3cqZAo" node="7Y4iKHGsvHd" resolve="elseIf" />
                      </node>
                      <node concept="2qgKlT" id="7Y4iKHGsBGd" role="2OqNvi">
                        <ref role="37wK5l" to="7zqe:7Y4iKHGsAyd" resolve="isSAT" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWsn" id="7Y4iKHGsvHd" role="1Duv9x">
                <property role="TrG5h" value="elseIf" />
                <node concept="3Tqbb2" id="7Y4iKHGs$l4" role="1tU5fm">
                  <ref role="ehGHo" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
                </node>
              </node>
              <node concept="2OqwBi" id="7Y4iKHGsvHi" role="1DdaDG">
                <node concept="37vLTw" id="7Y4iKHGsvHj" role="2Oq$k0">
                  <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                </node>
                <node concept="3Tsc0h" id="7Y4iKHGsvHk" role="2OqNvi">
                  <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="7Y4iKHGsvqH" role="3cqZAp" />
            <node concept="3clFbJ" id="4P2s9PPAnFx" role="3cqZAp">
              <node concept="3clFbS" id="4P2s9PPAnFy" role="3clFbx">
                <node concept="1DcWWT" id="4P2s9PPAnFz" role="3cqZAp">
                  <node concept="3cpWsn" id="4P2s9PPAnF$" role="1Duv9x">
                    <property role="TrG5h" value="e" />
                    <node concept="3Tqbb2" id="4P2s9PPAnF_" role="1tU5fm">
                      <ref role="ehGHo" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                    </node>
                  </node>
                  <node concept="3clFbS" id="4P2s9PPAnFA" role="2LFqv$">
                    <node concept="3clFbF" id="4P2s9PPAnFB" role="3cqZAp">
                      <node concept="37vLTI" id="4P2s9PPAnFC" role="3clFbG">
                        <node concept="3clFbT" id="4P2s9PPAnFD" role="37vLTx">
                          <property role="3clFbU" value="true" />
                        </node>
                        <node concept="2OqwBi" id="4P2s9PPAnFE" role="37vLTJ">
                          <node concept="37vLTw" id="4P2s9PPAnFF" role="2Oq$k0">
                            <ref role="3cqZAo" node="4P2s9PPAnF$" resolve="e" />
                          </node>
                          <node concept="3TrcHB" id="4P2s9PPAnFG" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="4P2s9PPAnFH" role="1DdaDG">
                    <node concept="37vLTw" id="4P2s9PPAnFI" role="2Oq$k0">
                      <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                    </node>
                    <node concept="3Tsc0h" id="4P2s9PPAoiD" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3fqX7Q" id="4P2s9PPAnFK" role="3clFbw">
                <node concept="2OqwBi" id="4P2s9PPAnFL" role="3fr31v">
                  <node concept="37vLTw" id="4P2s9PPAnFM" role="2Oq$k0">
                    <ref role="3cqZAo" node="4P2s9PP_HOk" resolve="macroIf" />
                  </node>
                  <node concept="2qgKlT" id="4P2s9PPAoa$" role="2OqNvi">
                    <ref role="37wK5l" to="7zqe:4P2s9PPAkgR" resolve="ElseBranchSAT" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="4P2s9PPAfWI" role="3cqZAp" />
          </node>
          <node concept="3cpWsn" id="4P2s9PP_HOk" role="1Duv9x">
            <property role="TrG5h" value="macroIf" />
            <node concept="3Tqbb2" id="4P2s9PP_IjO" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
            </node>
          </node>
          <node concept="2OqwBi" id="4P2s9PP_HOp" role="1DdaDG">
            <node concept="2Sf5sV" id="4P2s9PP_HOq" role="2Oq$k0" />
            <node concept="2Rf3mk" id="4P2s9PP_HOr" role="2OqNvi">
              <node concept="1xMEDy" id="4P2s9PP_HOs" role="1xVPHs">
                <node concept="chp4Y" id="4P2s9PP_HOt" role="ri$Ld">
                  <ref role="cht4Q" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="4P2s9PP_Lk5">
    <property role="TrG5h" value="Reset" />
    <ref role="2ZfgGC" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="2S6ZIM" id="4P2s9PP_Lk6" role="2ZfVej">
      <node concept="3clFbS" id="4P2s9PP_Lk7" role="2VODD2">
        <node concept="3clFbF" id="4P2s9PP_Llu" role="3cqZAp">
          <node concept="Xl_RD" id="4P2s9PP_Llt" role="3clFbG">
            <property role="Xl_RC" value="Reset SAT visibility" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="4P2s9PP_Lk8" role="2ZfgGD">
      <node concept="3clFbS" id="4P2s9PP_Lk9" role="2VODD2">
        <node concept="1DcWWT" id="4P2s9PP_LJH" role="3cqZAp">
          <node concept="3clFbS" id="4P2s9PP_LJJ" role="2LFqv$">
            <node concept="3clFbF" id="4P2s9PP_N_D" role="3cqZAp">
              <node concept="37vLTI" id="4P2s9PP_NHX" role="3clFbG">
                <node concept="3clFbT" id="4P2s9PP_NIp" role="37vLTx">
                  <property role="3clFbU" value="false" />
                </node>
                <node concept="2OqwBi" id="4P2s9PP_NAQ" role="37vLTJ">
                  <node concept="37vLTw" id="4P2s9PP_N_C" role="2Oq$k0">
                    <ref role="3cqZAo" node="4P2s9PP_LJL" resolve="e" />
                  </node>
                  <node concept="3TrcHB" id="4P2s9PP_NDu" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="4P2s9PP_LJL" role="1Duv9x">
            <property role="TrG5h" value="e" />
            <node concept="3Tqbb2" id="4P2s9PP_M5T" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
            </node>
          </node>
          <node concept="2OqwBi" id="4P2s9PP_LJQ" role="1DdaDG">
            <node concept="2Sf5sV" id="4P2s9PP_LJR" role="2Oq$k0" />
            <node concept="2Rf3mk" id="4P2s9PP_LJS" role="2OqNvi">
              <node concept="1xMEDy" id="4P2s9PP_LJT" role="1xVPHs">
                <node concept="chp4Y" id="4P2s9PP_LJU" role="ri$Ld">
                  <ref role="cht4Q" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="5a5L3hY8OSO">
    <property role="TrG5h" value="SetIsEdit" />
    <ref role="2ZfgGC" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="2S6ZIM" id="5a5L3hY8OSP" role="2ZfVej">
      <node concept="3clFbS" id="5a5L3hY8OSQ" role="2VODD2">
        <node concept="3clFbF" id="5a5L3hY8OVm" role="3cqZAp">
          <node concept="Xl_RD" id="5a5L3hY8OVl" role="3clFbG">
            <property role="Xl_RC" value="Set Is Edit (Demo only)" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="5a5L3hY8OSR" role="2ZfgGD">
      <node concept="3clFbS" id="5a5L3hY8OSS" role="2VODD2">
        <node concept="3clFbF" id="5a5L3hY8P55" role="3cqZAp">
          <node concept="37vLTI" id="5a5L3hY8Pjj" role="3clFbG">
            <node concept="3clFbT" id="5a5L3hY8PjK" role="37vLTx">
              <property role="3clFbU" value="true" />
            </node>
            <node concept="2OqwBi" id="5a5L3hY8P6J" role="37vLTJ">
              <node concept="2Sf5sV" id="5a5L3hY8P54" role="2Oq$k0" />
              <node concept="3TrcHB" id="5a5L3hY8PdX" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5a5L3hY9Dx7" role="3cqZAp">
          <node concept="37vLTI" id="5a5L3hY9DJT" role="3clFbG">
            <node concept="Xl_RD" id="5a5L3hY9DKe" role="37vLTx">
              <property role="Xl_RC" value="BUILDROB_CLONE" />
            </node>
            <node concept="2OqwBi" id="5a5L3hY9Dz2" role="37vLTJ">
              <node concept="2Sf5sV" id="5a5L3hY9Dx5" role="2Oq$k0" />
              <node concept="3TrcHB" id="5a5L3hY9DEj" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="5a5L3hY9yNF">
    <property role="TrG5h" value="Commit" />
    <ref role="2ZfgGC" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="2S6ZIM" id="5a5L3hY9yNG" role="2ZfVej">
      <node concept="3clFbS" id="5a5L3hY9yNH" role="2VODD2">
        <node concept="3clFbF" id="5a5L3hY9yP4" role="3cqZAp">
          <node concept="Xl_RD" id="5a5L3hY9yP3" role="3clFbG">
            <property role="Xl_RC" value="Commit" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="5a5L3hY9yNI" role="2ZfgGD">
      <node concept="3clFbS" id="5a5L3hY9yNJ" role="2VODD2">
        <node concept="3clFbH" id="33HeFyz2ibQ" role="3cqZAp" />
        <node concept="3cpWs8" id="33HeFyz1AHK" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz1AHL" role="3cpWs9">
            <property role="TrG5h" value="vts" />
            <node concept="3uibUv" id="33HeFyz1AHM" role="1tU5fm">
              <ref role="3uigEE" node="33HeFyz0Eqd" resolve="VTSWrapper" />
            </node>
            <node concept="2ShNRf" id="33HeFyz1Ckz" role="33vP2m">
              <node concept="HV5vD" id="33HeFyz2hnY" role="2ShVmc">
                <ref role="HV5vE" node="33HeFyz0Eqd" resolve="VTSWrapper" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyzkZRZ" role="3cqZAp" />
        <node concept="3cpWs8" id="5KGsxZrv7jD" role="3cqZAp">
          <node concept="3cpWsn" id="5KGsxZrv7jE" role="3cpWs9">
            <property role="TrG5h" value="prettyprinted" />
            <node concept="17QB3L" id="3c4d5Cy7Uh7" role="1tU5fm" />
            <node concept="2OqwBi" id="33HeFyz2hwR" role="33vP2m">
              <node concept="37vLTw" id="33HeFyz2hpJ" role="2Oq$k0">
                <ref role="3cqZAo" node="33HeFyz1AHL" resolve="vts" />
              </node>
              <node concept="liA8E" id="33HeFyz2hJi" role="2OqNvi">
                <ref role="37wK5l" node="33HeFyz10W0" resolve="commit" />
                <node concept="2Sf5sV" id="33HeFyz2hLF" role="37wK5m" />
                <node concept="2ShNRf" id="33HeFyzl6Kp" role="37wK5m">
                  <node concept="1pGfFk" id="33HeFyzl7K8" role="2ShVmc">
                    <ref role="37wK5l" to="mk8z:~EmptyProgressMonitor.&lt;init&gt;()" resolve="EmptyProgressMonitor" />
                  </node>
                </node>
                <node concept="2OqwBi" id="33HeFyzlOUG" role="37wK5m">
                  <node concept="1XNTG" id="33HeFyzlOKT" role="2Oq$k0" />
                  <node concept="liA8E" id="33HeFyzlPbu" role="2OqNvi">
                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5KGsxZrv9MS" role="3cqZAp" />
        <node concept="3cpWs8" id="6XRhSxwpizf" role="3cqZAp">
          <node concept="3cpWsn" id="6XRhSxwpizg" role="3cpWs9">
            <property role="TrG5h" value="filePrettyprinted" />
            <node concept="3uibUv" id="6XRhSxwpizh" role="1tU5fm">
              <ref role="3uigEE" to="guwi:~File" resolve="File" />
            </node>
            <node concept="2ShNRf" id="6XRhSxwp8RL" role="33vP2m">
              <node concept="1pGfFk" id="6XRhSxwp8RM" role="2ShVmc">
                <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                <node concept="2OqwBi" id="6XRhSxwp8RN" role="37wK5m">
                  <node concept="2YIFZM" id="6XRhSxwp8RO" role="2Oq$k0">
                    <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                    <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                  </node>
                  <node concept="liA8E" id="6XRhSxwp8RP" role="2OqNvi">
                    <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                    <node concept="Xl_RD" id="6XRhSxwp8RQ" role="37wK5m">
                      <property role="Xl_RC" value="${TempFolder}" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="6XRhSxwp8RR" role="37wK5m">
                  <property role="Xl_RC" value="vts.txt" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="6XRhSxwplWB" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="6XRhSxwpnXm" role="34bqiv">
            <node concept="2OqwBi" id="6XRhSxwppFM" role="3uHU7w">
              <node concept="37vLTw" id="6XRhSxwpppg" role="2Oq$k0">
                <ref role="3cqZAo" node="6XRhSxwpizg" resolve="filePrettyprinted" />
              </node>
              <node concept="liA8E" id="6XRhSxwpqfV" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
            <node concept="Xl_RD" id="6XRhSxwplWD" role="3uHU7B">
              <property role="Xl_RC" value="dump prettyprinted to " />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="6XRhSxwpqn5" role="3cqZAp" />
        <node concept="3cpWs8" id="5KGsxZrvtQL" role="3cqZAp">
          <node concept="3cpWsn" id="5KGsxZrvtQM" role="3cpWs9">
            <property role="TrG5h" value="writer" />
            <node concept="3uibUv" id="5KGsxZrvtQN" role="1tU5fm">
              <ref role="3uigEE" to="guwi:~PrintWriter" resolve="PrintWriter" />
            </node>
            <node concept="10Nm6u" id="5KGsxZrvvdj" role="33vP2m" />
          </node>
        </node>
        <node concept="2GUZhq" id="5KGsxZrvwzv" role="3cqZAp">
          <node concept="3clFbS" id="5KGsxZrvwzx" role="2GV8ay">
            <node concept="3clFbF" id="5KGsxZrvzer" role="3cqZAp">
              <node concept="37vLTI" id="5KGsxZrvzxF" role="3clFbG">
                <node concept="2ShNRf" id="5KGsxZrvzAJ" role="37vLTx">
                  <node concept="1pGfFk" id="5KGsxZrvHH9" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~PrintWriter.&lt;init&gt;(java.io.File,java.lang.String)" resolve="PrintWriter" />
                    <node concept="37vLTw" id="6XRhSxwpkmI" role="37wK5m">
                      <ref role="3cqZAo" node="6XRhSxwpizg" resolve="filePrettyprinted" />
                    </node>
                    <node concept="Xl_RD" id="5KGsxZrvHUC" role="37wK5m">
                      <property role="Xl_RC" value="UTF-8" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="3c4d5Cy7X_6" role="37vLTJ">
                  <ref role="3cqZAo" node="5KGsxZrvtQM" resolve="writer" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5KGsxZrvLlI" role="3cqZAp">
              <node concept="2OqwBi" id="5KGsxZrvL$j" role="3clFbG">
                <node concept="37vLTw" id="3c4d5Cy7XAH" role="2Oq$k0">
                  <ref role="3cqZAo" node="5KGsxZrvtQM" resolve="writer" />
                </node>
                <node concept="liA8E" id="5KGsxZrvMis" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintWriter.write(java.lang.String):void" resolve="write" />
                  <node concept="37vLTw" id="5KGsxZrvMjD" role="37wK5m">
                    <ref role="3cqZAo" node="5KGsxZrv7jE" resolve="prettyprinted" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5KGsxZrvwzy" role="2GVbov">
            <node concept="3clFbJ" id="5KGsxZrvxTp" role="3cqZAp">
              <node concept="3y3z36" id="5KGsxZrvycQ" role="3clFbw">
                <node concept="10Nm6u" id="5KGsxZrvyd0" role="3uHU7w" />
                <node concept="37vLTw" id="5KGsxZrvxTO" role="3uHU7B">
                  <ref role="3cqZAo" node="5KGsxZrvtQM" resolve="writer" />
                </node>
              </node>
              <node concept="3clFbS" id="5KGsxZrvxTr" role="3clFbx">
                <node concept="3clFbF" id="5KGsxZrvydw" role="3cqZAp">
                  <node concept="2OqwBi" id="5KGsxZrvywv" role="3clFbG">
                    <node concept="37vLTw" id="3c4d5Cy7XCe" role="2Oq$k0">
                      <ref role="3cqZAo" node="5KGsxZrvtQM" resolve="writer" />
                    </node>
                    <node concept="liA8E" id="5KGsxZrvzdS" role="2OqNvi">
                      <ref role="37wK5l" to="guwi:~PrintWriter.close():void" resolve="close" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="5KGsxZrvI41" role="TEXxN">
            <node concept="3cpWsn" id="5KGsxZrvI42" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="5KGsxZrvJnk" role="1tU5fm">
                <ref role="3uigEE" to="guwi:~UnsupportedEncodingException" resolve="UnsupportedEncodingException" />
              </node>
            </node>
            <node concept="3clFbS" id="5KGsxZrvI44" role="TDEfX">
              <node concept="34ab3g" id="5KGsxZrvJ$q" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="5KGsxZrvJ$s" role="34bqiv" />
                <node concept="37vLTw" id="5KGsxZrvJ$u" role="34bMjA">
                  <ref role="3cqZAo" node="5KGsxZrvI42" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="5KGsxZrvJBe" role="TEXxN">
            <node concept="3cpWsn" id="5KGsxZrvJBf" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="5KGsxZrvKWK" role="1tU5fm">
                <ref role="3uigEE" to="guwi:~FileNotFoundException" resolve="FileNotFoundException" />
              </node>
            </node>
            <node concept="3clFbS" id="5KGsxZrvJBh" role="TDEfX">
              <node concept="34ab3g" id="5KGsxZrvLfY" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="5KGsxZrvLg0" role="34bqiv" />
                <node concept="37vLTw" id="5KGsxZrvLg2" role="34bMjA">
                  <ref role="3cqZAo" node="5KGsxZrvJBf" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="2NOvNR_oqgD" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="2NOvNR_oqgF" role="34bqiv">
            <property role="Xl_RC" value="commit done" />
          </node>
        </node>
        <node concept="1X3_iC" id="5KGsxZrtE3C" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbF" id="5a5L3hY9yRN" role="8Wnug">
            <node concept="37vLTI" id="5a5L3hY9z61" role="3clFbG">
              <node concept="3clFbT" id="5a5L3hY9z6u" role="37vLTx">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="2OqwBi" id="5a5L3hY9yTt" role="37vLTJ">
                <node concept="2Sf5sV" id="5a5L3hY9yRM" role="2Oq$k0" />
                <node concept="3TrcHB" id="5a5L3hY9z0F" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="5KGsxZrsIFE">
    <property role="TrG5h" value="Checkout" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="2S6ZIM" id="5KGsxZrsIFF" role="2ZfVej">
      <node concept="3clFbS" id="5KGsxZrsIFG" role="2VODD2">
        <node concept="3clFbF" id="5KGsxZrsIFH" role="3cqZAp">
          <node concept="Xl_RD" id="5KGsxZrsIFI" role="3clFbG">
            <property role="Xl_RC" value="Checkout" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="5KGsxZrsIFJ" role="2ZfgGD">
      <node concept="3clFbS" id="5KGsxZrsIFK" role="2VODD2">
        <node concept="3clFbF" id="33HeFyzxykY" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzxysG" role="3clFbG">
            <node concept="2Sf5sV" id="33HeFyzxykX" role="2Oq$k0" />
            <node concept="2qgKlT" id="33HeFyzxyIF" role="2OqNvi">
              <ref role="37wK5l" to="7zqe:33HeFyzxxbm" resolve="checkout" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3dkpOd" id="4ofDlp56tvo">
    <property role="TrG5h" value="JumpToClone" />
    <ref role="2ZfgGC" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="2S6ZIM" id="4ofDlp56tvp" role="2ZfVej">
      <node concept="3clFbS" id="4ofDlp56tvq" role="2VODD2">
        <node concept="3clFbF" id="4ofDlp56tCl" role="3cqZAp">
          <node concept="3cpWs3" id="4ofDlp571Nt" role="3clFbG">
            <node concept="2YIFZM" id="2bFU2aD66q8" role="3uHU7w">
              <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
              <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
              <node concept="Xl_RD" id="2bFU2aD66AU" role="37wK5m">
                <property role="Xl_RC" value="%02d" />
              </node>
              <node concept="38Zlrr" id="2bFU2aD67FM" role="37wK5m" />
            </node>
            <node concept="Xl_RD" id="4ofDlp56tTl" role="3uHU7B">
              <property role="Xl_RC" value="Jump to Clone " />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="4ofDlp56tvr" role="2ZfgGD">
      <node concept="3clFbS" id="4ofDlp56tvs" role="2VODD2">
        <node concept="3cpWs8" id="4ofDlp57gqa" role="3cqZAp">
          <node concept="3cpWsn" id="4ofDlp57gqd" role="3cpWs9">
            <property role="TrG5h" value="cloneNode" />
            <node concept="3Tqbb2" id="4ofDlp57gq9" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
            </node>
            <node concept="2OqwBi" id="4ofDlp57r9T" role="33vP2m">
              <node concept="2OqwBi" id="4ofDlp57oRA" role="2Oq$k0">
                <node concept="2OqwBi" id="4ofDlp57isg" role="2Oq$k0">
                  <node concept="2OqwBi" id="4ofDlp57g$u" role="2Oq$k0">
                    <node concept="2Sf5sV" id="4ofDlp57gsK" role="2Oq$k0" />
                    <node concept="2Rf3mk" id="4ofDlp57gG5" role="2OqNvi">
                      <node concept="1xMEDy" id="4ofDlp57gG7" role="1xVPHs">
                        <node concept="chp4Y" id="4ofDlp57gUk" role="ri$Ld">
                          <ref role="cht4Q" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3zZkjj" id="4ofDlp57l$W" role="2OqNvi">
                    <node concept="1bVj0M" id="4ofDlp57l$Y" role="23t8la">
                      <node concept="3clFbS" id="4ofDlp57l$Z" role="1bW5cS">
                        <node concept="3clFbF" id="4ofDlp57lFr" role="3cqZAp">
                          <node concept="2OqwBi" id="4ofDlp57mBe" role="3clFbG">
                            <node concept="2OqwBi" id="4ofDlp57lSf" role="2Oq$k0">
                              <node concept="37vLTw" id="4ofDlp57lFq" role="2Oq$k0">
                                <ref role="3cqZAo" node="4ofDlp57l_0" resolve="it" />
                              </node>
                              <node concept="3TrcHB" id="4ofDlp57ma2" role="2OqNvi">
                                <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                              </node>
                            </node>
                            <node concept="liA8E" id="4ofDlp57ngd" role="2OqNvi">
                              <ref role="37wK5l" to="wyt6:~String.contains(java.lang.CharSequence):boolean" resolve="contains" />
                              <node concept="Xl_RD" id="4ofDlp57nnH" role="37wK5m">
                                <property role="Xl_RC" value="CLONE" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="4ofDlp57l_0" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="4ofDlp57l_1" role="1tU5fm" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="ANE8D" id="4ofDlp57pzN" role="2OqNvi" />
              </node>
              <node concept="34jXtK" id="4ofDlp57sRc" role="2OqNvi">
                <node concept="3cpWsd" id="4ofDlp57tCb" role="25WWJ7">
                  <node concept="3cmrfG" id="4ofDlp57tCh" role="3uHU7w">
                    <property role="3cmrfH" value="1" />
                  </node>
                  <node concept="38Zlrr" id="4ofDlp57t0z" role="3uHU7B" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4ofDlp57u5F" role="3cqZAp">
          <node concept="2OqwBi" id="4ofDlp57ulo" role="3clFbG">
            <node concept="1XNTG" id="4ofDlp57u5D" role="2Oq$k0" />
            <node concept="liA8E" id="4ofDlp57uvl" role="2OqNvi">
              <ref role="37wK5l" to="cj4x:~EditorContext.select(org.jetbrains.mps.openapi.model.SNode):void" resolve="select" />
              <node concept="37vLTw" id="4ofDlp57uvU" role="37wK5m">
                <ref role="3cqZAo" node="4ofDlp57gqd" resolve="cloneNode" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="38BcoT" id="4ofDlp56vC4" role="3dlsAV">
      <node concept="3clFbS" id="4ofDlp56vC5" role="2VODD2">
        <node concept="3cpWs8" id="4ofDlp56w3V" role="3cqZAp">
          <node concept="3cpWsn" id="4ofDlp56w3Y" role="3cpWs9">
            <property role="TrG5h" value="parameters" />
            <node concept="_YKpA" id="4ofDlp56w3T" role="1tU5fm">
              <node concept="10Oyi0" id="4ofDlp56wkI" role="_ZDj9" />
            </node>
            <node concept="2ShNRf" id="4ofDlp56wQ_" role="33vP2m">
              <node concept="Tc6Ow" id="4ofDlp56xpC" role="2ShVmc">
                <node concept="10Oyi0" id="4ofDlp56xFX" role="HW$YZ" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4ofDlp57GKM" role="3cqZAp" />
        <node concept="1Dw8fO" id="4ofDlp57Hul" role="3cqZAp">
          <node concept="3clFbS" id="4ofDlp57Hun" role="2LFqv$">
            <node concept="3clFbF" id="4ofDlp57Sep" role="3cqZAp">
              <node concept="2OqwBi" id="4ofDlp57TmX" role="3clFbG">
                <node concept="37vLTw" id="4ofDlp57Sen" role="2Oq$k0">
                  <ref role="3cqZAo" node="4ofDlp56w3Y" resolve="parameters" />
                </node>
                <node concept="TSZUe" id="4ofDlp57VBr" role="2OqNvi">
                  <node concept="37vLTw" id="4ofDlp57Wxm" role="25WWJ7">
                    <ref role="3cqZAo" node="4ofDlp57Huo" resolve="i" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="4ofDlp57Huo" role="1Duv9x">
            <property role="TrG5h" value="i" />
            <node concept="10Oyi0" id="4ofDlp57IbE" role="1tU5fm" />
            <node concept="3cmrfG" id="4ofDlp57KiP" role="33vP2m">
              <property role="3cmrfH" value="1" />
            </node>
          </node>
          <node concept="2dkUwp" id="4ofDlp57MO4" role="1Dwp0S">
            <node concept="37vLTw" id="4ofDlp57LGT" role="3uHU7B">
              <ref role="3cqZAo" node="4ofDlp57Huo" resolve="i" />
            </node>
            <node concept="2OqwBi" id="4ofDlp57OD3" role="3uHU7w">
              <node concept="2OqwBi" id="4ofDlp57Nxl" role="2Oq$k0">
                <node concept="2OqwBi" id="4ofDlp57Nxm" role="2Oq$k0">
                  <node concept="2Sf5sV" id="4ofDlp57Nxn" role="2Oq$k0" />
                  <node concept="2Rf3mk" id="4ofDlp57Nxo" role="2OqNvi">
                    <node concept="1xMEDy" id="4ofDlp57Nxp" role="1xVPHs">
                      <node concept="chp4Y" id="4ofDlp57Nxq" role="ri$Ld">
                        <ref role="cht4Q" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3zZkjj" id="4ofDlp57Nxr" role="2OqNvi">
                  <node concept="1bVj0M" id="4ofDlp57Nxs" role="23t8la">
                    <node concept="3clFbS" id="4ofDlp57Nxt" role="1bW5cS">
                      <node concept="3clFbF" id="4ofDlp57Nxu" role="3cqZAp">
                        <node concept="2OqwBi" id="4ofDlp57Nxv" role="3clFbG">
                          <node concept="2OqwBi" id="4ofDlp57Nxw" role="2Oq$k0">
                            <node concept="37vLTw" id="4ofDlp57Nxx" role="2Oq$k0">
                              <ref role="3cqZAo" node="4ofDlp57Nx_" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="4ofDlp57Nxy" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                            </node>
                          </node>
                          <node concept="liA8E" id="4ofDlp57Nxz" role="2OqNvi">
                            <ref role="37wK5l" to="wyt6:~String.contains(java.lang.CharSequence):boolean" resolve="contains" />
                            <node concept="Xl_RD" id="4ofDlp57Nx$" role="37wK5m">
                              <property role="Xl_RC" value="CLONE" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="4ofDlp57Nx_" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="4ofDlp57NxA" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="34oBXx" id="4ofDlp57PG5" role="2OqNvi" />
            </node>
          </node>
          <node concept="3uNrnE" id="4ofDlp57RtN" role="1Dwrff">
            <node concept="37vLTw" id="4ofDlp57RtP" role="2$L3a6">
              <ref role="3cqZAo" node="4ofDlp57Huo" resolve="i" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="4ofDlp56_Np" role="3cqZAp">
          <node concept="37vLTw" id="4ofDlp56A5T" role="3cqZAk">
            <ref role="3cqZAo" node="4ofDlp56w3Y" resolve="parameters" />
          </node>
        </node>
      </node>
      <node concept="10Oyi0" id="4ofDlp56vNi" role="3ddBve" />
    </node>
  </node>
  <node concept="2S6QgY" id="vk1xVpMYox">
    <property role="TrG5h" value="RegisterListeners" />
    <ref role="2ZfgGC" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="2S6ZIM" id="vk1xVpMYoy" role="2ZfVej">
      <node concept="3clFbS" id="vk1xVpMYoz" role="2VODD2">
        <node concept="3clFbF" id="vk1xVpNhcl" role="3cqZAp">
          <node concept="Xl_RD" id="vk1xVpNhck" role="3clFbG">
            <property role="Xl_RC" value="Register Listener" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="vk1xVpMYo$" role="2ZfgGD">
      <node concept="3clFbS" id="vk1xVpMYo_" role="2VODD2">
        <node concept="3SKdUt" id="27Ik2x6TMqA" role="3cqZAp">
          <node concept="3SKdUq" id="27Ik2x6TMqC" role="3SKWNk">
            <property role="3SKdUp" value="For some reason, logging only goes to idea.log" />
          </node>
        </node>
        <node concept="34ab3g" id="vk1xVpORhQ" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="vk1xVpORhS" role="34bqiv">
            <property role="Xl_RC" value="executer RegisterListener 2" />
          </node>
        </node>
        <node concept="3clFbH" id="27Ik2x6TMwT" role="3cqZAp" />
        <node concept="3cpWs8" id="vk1xVpNoYj" role="3cqZAp">
          <node concept="3cpWsn" id="vk1xVpNoYk" role="3cpWs9">
            <property role="TrG5h" value="model" />
            <node concept="3uibUv" id="vk1xVpNpu6" role="1tU5fm">
              <ref role="3uigEE" to="mhbf:~SModel" resolve="SModel" />
            </node>
            <node concept="2OqwBi" id="vk1xVpNplY" role="33vP2m">
              <node concept="1XNTG" id="vk1xVpNphB" role="2Oq$k0" />
              <node concept="liA8E" id="vk1xVpNpsG" role="2OqNvi">
                <ref role="37wK5l" to="cj4x:~EditorContext.getModel():org.jetbrains.mps.openapi.model.SModel" resolve="getModel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="1LOfuoilw6b" role="3cqZAp">
          <node concept="3SKdUq" id="1LOfuoilw6d" role="3SKWNk">
            <property role="3SKdUp" value=" remove old listener to prevent duplicates " />
          </node>
        </node>
        <node concept="3clFbH" id="6IXrOW8HAU9" role="3cqZAp" />
        <node concept="3SKdUt" id="6IXrOW8HBhg" role="3cqZAp">
          <node concept="3SKdUq" id="6IXrOW8HBhi" role="3SKWNk">
            <property role="3SKdUp" value="Use of unintended API and illegal reflection" />
          </node>
        </node>
        <node concept="SfApY" id="6IXrOW8HGBf" role="3cqZAp">
          <node concept="3clFbS" id="6IXrOW8HGBh" role="SfCbr">
            <node concept="3cpWs8" id="6IXrOW8HN5K" role="3cqZAp">
              <node concept="3cpWsn" id="6IXrOW8HN5L" role="3cpWs9">
                <property role="TrG5h" value="modelEventDispatch" />
                <node concept="3uibUv" id="6IXrOW8HN5M" role="1tU5fm">
                  <ref role="3uigEE" to="j9co:~ModelEventDispatch" resolve="ModelEventDispatch" />
                </node>
                <node concept="2OqwBi" id="6IXrOW8HG26" role="33vP2m">
                  <node concept="1eOMI4" id="6IXrOW8HG27" role="2Oq$k0">
                    <node concept="10QFUN" id="6IXrOW8HG28" role="1eOMHV">
                      <node concept="3uibUv" id="6IXrOW8HG29" role="10QFUM">
                        <ref role="3uigEE" to="g3l6:~SModelBase" resolve="SModelBase" />
                      </node>
                      <node concept="37vLTw" id="6IXrOW8HG2a" role="10QFUP">
                        <ref role="3cqZAo" node="vk1xVpNoYk" resolve="model" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="6IXrOW8HG2b" role="2OqNvi">
                    <ref role="37wK5l" to="g3l6:~SModelBase.getNodeEventDispatch():jetbrains.mps.smodel.event.ModelEventDispatch" resolve="getNodeEventDispatch" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="6IXrOW8HG21" role="3cqZAp">
              <node concept="3cpWsn" id="6IXrOW8HG22" role="3cpWs9">
                <property role="TrG5h" value="changeListenersField" />
                <node concept="3uibUv" id="6IXrOW8HG23" role="1tU5fm">
                  <ref role="3uigEE" to="t6h5:~Field" resolve="Field" />
                </node>
                <node concept="2OqwBi" id="6IXrOW8HG24" role="33vP2m">
                  <node concept="2OqwBi" id="6IXrOW8HG25" role="2Oq$k0">
                    <node concept="liA8E" id="6IXrOW8HG2c" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                    </node>
                    <node concept="37vLTw" id="6IXrOW8HNsB" role="2Oq$k0">
                      <ref role="3cqZAo" node="6IXrOW8HN5L" resolve="modelEventDispatch" />
                    </node>
                  </node>
                  <node concept="liA8E" id="6IXrOW8HG2d" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Class.getDeclaredField(java.lang.String):java.lang.reflect.Field" resolve="getDeclaredField" />
                    <node concept="Xl_RD" id="6IXrOW8HG2e" role="37wK5m">
                      <property role="Xl_RC" value="myChangeListeners" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6IXrOW8HJfL" role="3cqZAp">
              <node concept="2OqwBi" id="6IXrOW8HJtu" role="3clFbG">
                <node concept="37vLTw" id="6IXrOW8HJfJ" role="2Oq$k0">
                  <ref role="3cqZAo" node="6IXrOW8HG22" resolve="changeListenersField" />
                </node>
                <node concept="liA8E" id="6IXrOW8HJYc" role="2OqNvi">
                  <ref role="37wK5l" to="t6h5:~AccessibleObject.setAccessible(boolean):void" resolve="setAccessible" />
                  <node concept="3clFbT" id="6IXrOW8HK41" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="6IXrOW8HKwy" role="3cqZAp">
              <node concept="3cpWsn" id="6IXrOW8HKwC" role="3cpWs9">
                <property role="TrG5h" value="listeners" />
                <node concept="3uibUv" id="6IXrOW8HKwE" role="1tU5fm">
                  <ref role="3uigEE" to="33ny:~List" resolve="List" />
                  <node concept="3uibUv" id="6IXrOW8HK$V" role="11_B2D">
                    <ref role="3uigEE" to="mhbf:~SNodeChangeListener" resolve="SNodeChangeListener" />
                  </node>
                </node>
                <node concept="0kSF2" id="6IXrOW8HLxo" role="33vP2m">
                  <node concept="3uibUv" id="6IXrOW8HLxr" role="0kSFW">
                    <ref role="3uigEE" to="33ny:~List" resolve="List" />
                    <node concept="3uibUv" id="6IXrOW8HLxs" role="11_B2D">
                      <ref role="3uigEE" to="mhbf:~SNodeChangeListener" resolve="SNodeChangeListener" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="6IXrOW8HKQn" role="0kSFX">
                    <node concept="37vLTw" id="6IXrOW8HKE5" role="2Oq$k0">
                      <ref role="3cqZAo" node="6IXrOW8HG22" resolve="changeListenersField" />
                    </node>
                    <node concept="liA8E" id="6IXrOW8HLnB" role="2OqNvi">
                      <ref role="37wK5l" to="t6h5:~Field.get(java.lang.Object):java.lang.Object" resolve="get" />
                      <node concept="37vLTw" id="6IXrOW8HND9" role="37wK5m">
                        <ref role="3cqZAo" node="6IXrOW8HN5L" resolve="modelEventDispatch" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="6IXrOW8I9YQ" role="3cqZAp">
              <node concept="3cpWsn" id="6IXrOW8I9YW" role="3cpWs9">
                <property role="TrG5h" value="toRemove" />
                <node concept="3uibUv" id="6IXrOW8I9YY" role="1tU5fm">
                  <ref role="3uigEE" to="33ny:~List" resolve="List" />
                  <node concept="3uibUv" id="6IXrOW8Ia5s" role="11_B2D">
                    <ref role="3uigEE" to="mhbf:~SNodeChangeListener" resolve="SNodeChangeListener" />
                  </node>
                </node>
                <node concept="2ShNRf" id="6IXrOW8IacS" role="33vP2m">
                  <node concept="1pGfFk" id="6IXrOW8IaPx" role="2ShVmc">
                    <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                    <node concept="3uibUv" id="6IXrOW8Ib8e" role="1pMfVU">
                      <ref role="3uigEE" to="mhbf:~SNodeChangeListener" resolve="SNodeChangeListener" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2Gpval" id="6IXrOW8Iby2" role="3cqZAp">
              <node concept="2GrKxI" id="6IXrOW8Iby4" role="2Gsz3X">
                <property role="TrG5h" value="listener" />
              </node>
              <node concept="37vLTw" id="6IXrOW8IbHm" role="2GsD0m">
                <ref role="3cqZAo" node="6IXrOW8HKwC" resolve="listeners" />
              </node>
              <node concept="3clFbS" id="6IXrOW8Iby8" role="2LFqv$">
                <node concept="3clFbJ" id="6IXrOW8IbNI" role="3cqZAp">
                  <node concept="2ZW3vV" id="6IXrOW8Ic24" role="3clFbw">
                    <node concept="3uibUv" id="6IXrOW8IcaF" role="2ZW6by">
                      <ref role="3uigEE" node="27Ik2x6U3cB" resolve="MyChangeListener" />
                    </node>
                    <node concept="2GrUjf" id="6IXrOW8IbQe" role="2ZW6bz">
                      <ref role="2Gs0qQ" node="6IXrOW8Iby4" resolve="listener" />
                    </node>
                  </node>
                  <node concept="3clFbS" id="6IXrOW8IbNK" role="3clFbx">
                    <node concept="3clFbF" id="6IXrOW8IcgL" role="3cqZAp">
                      <node concept="2OqwBi" id="6IXrOW8IcNI" role="3clFbG">
                        <node concept="37vLTw" id="6IXrOW8IcgK" role="2Oq$k0">
                          <ref role="3cqZAo" node="6IXrOW8I9YW" resolve="toRemove" />
                        </node>
                        <node concept="liA8E" id="6IXrOW8Ie7a" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~List.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="2GrUjf" id="6IXrOW8Iemw" role="37wK5m">
                            <ref role="2Gs0qQ" node="6IXrOW8Iby4" resolve="listener" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6IXrOW8IfI4" role="3cqZAp">
              <node concept="2OqwBi" id="6IXrOW8Igey" role="3clFbG">
                <node concept="37vLTw" id="6IXrOW8IfI2" role="2Oq$k0">
                  <ref role="3cqZAo" node="6IXrOW8HKwC" resolve="listeners" />
                </node>
                <node concept="liA8E" id="6IXrOW8Ihzx" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~List.removeAll(java.util.Collection):boolean" resolve="removeAll" />
                  <node concept="37vLTw" id="6IXrOW8IhK1" role="37wK5m">
                    <ref role="3cqZAo" node="6IXrOW8I9YW" resolve="toRemove" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="6IXrOW8IbeY" role="3cqZAp" />
          </node>
          <node concept="TDmWw" id="6IXrOW8HGBi" role="TEbGg">
            <node concept="3cpWsn" id="6IXrOW8HGBk" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="6IXrOW8HHwZ" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~NoSuchFieldException" resolve="NoSuchFieldException" />
              </node>
            </node>
            <node concept="3clFbS" id="6IXrOW8HGBo" role="TDEfX">
              <node concept="34ab3g" id="6IXrOW8HHAw" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="6IXrOW8HHAy" role="34bqiv" />
                <node concept="37vLTw" id="6IXrOW8HHA$" role="34bMjA">
                  <ref role="3cqZAo" node="6IXrOW8HGBk" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="6IXrOW8IvhJ" role="TEbGg">
            <node concept="3cpWsn" id="6IXrOW8IvhK" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="6IXrOW8IvNL" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~IllegalAccessException" resolve="IllegalAccessException" />
              </node>
            </node>
            <node concept="3clFbS" id="6IXrOW8IvhM" role="TDEfX">
              <node concept="34ab3g" id="6IXrOW8Iwy3" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="6IXrOW8Iwy5" role="34bqiv" />
                <node concept="37vLTw" id="6IXrOW8Iwy7" role="34bMjA">
                  <ref role="3cqZAo" node="6IXrOW8IvhK" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="6IXrOW8HB1P" role="3cqZAp" />
        <node concept="34ab3g" id="6IXrOW8HhNN" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="6IXrOW8HiFZ" role="34bqiv">
            <node concept="2OqwBi" id="6IXrOW8HkiX" role="3uHU7w">
              <node concept="2OqwBi" id="6IXrOW8HiUE" role="2Oq$k0">
                <node concept="37vLTw" id="6IXrOW8HiHM" role="2Oq$k0">
                  <ref role="3cqZAo" node="vk1xVpNoYk" resolve="model" />
                </node>
                <node concept="liA8E" id="6IXrOW8Hj1_" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="liA8E" id="6IXrOW8Hlbl" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Class.getTypeName():java.lang.String" resolve="getTypeName" />
              </node>
            </node>
            <node concept="Xl_RD" id="6IXrOW8HhNP" role="3uHU7B">
              <property role="Xl_RC" value="model type" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="6IXrOW8HhLz" role="3cqZAp" />
        <node concept="3clFbF" id="1LOfuoilJwa" role="3cqZAp">
          <node concept="2OqwBi" id="1LOfuoilJwb" role="3clFbG">
            <node concept="37vLTw" id="1LOfuoilYqV" role="2Oq$k0">
              <ref role="3cqZAo" node="vk1xVpNoYk" resolve="model" />
            </node>
            <node concept="liA8E" id="1LOfuoilJwd" role="2OqNvi">
              <ref role="37wK5l" to="mhbf:~SModel.removeChangeListener(org.jetbrains.mps.openapi.model.SNodeChangeListener):void" resolve="removeChangeListener" />
              <node concept="2YIFZM" id="1LOfuoilJwe" role="37wK5m">
                <ref role="37wK5l" node="27Ik2x6U5wb" resolve="getINSTANCE" />
                <ref role="1Pybhc" node="27Ik2x6U3cB" resolve="MyChangeListener" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vk1xVpNGxU" role="3cqZAp">
          <node concept="2OqwBi" id="vk1xVpNGCx" role="3clFbG">
            <node concept="37vLTw" id="1LOfuoilYtB" role="2Oq$k0">
              <ref role="3cqZAo" node="vk1xVpNoYk" resolve="model" />
            </node>
            <node concept="liA8E" id="vk1xVpNGVj" role="2OqNvi">
              <ref role="37wK5l" to="mhbf:~SModel.addChangeListener(org.jetbrains.mps.openapi.model.SNodeChangeListener):void" resolve="addChangeListener" />
              <node concept="2YIFZM" id="27Ik2x6Ukys" role="37wK5m">
                <ref role="1Pybhc" node="27Ik2x6U3cB" resolve="MyChangeListener" />
                <ref role="37wK5l" node="27Ik2x6U5wb" resolve="getINSTANCE" />
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="vk1xVpOC6$" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="vk1xVpOC6A" role="34bqiv">
            <property role="Xl_RC" value="Register done" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="27Ik2x6U3cB">
    <property role="TrG5h" value="MyChangeListener" />
    <node concept="2tJIrI" id="27Ik2x6U3d7" role="jymVt" />
    <node concept="Wx3nA" id="27Ik2x6U3U2" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="INSTANCE" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="27Ik2x6U3FN" role="1B3o_S" />
      <node concept="3uibUv" id="27Ik2x6U3Pn" role="1tU5fm">
        <ref role="3uigEE" node="27Ik2x6U3cB" resolve="MyChangeListener" />
      </node>
    </node>
    <node concept="2tJIrI" id="27Ik2x6U3Zj" role="jymVt" />
    <node concept="3clFbW" id="27Ik2x6U4U1" role="jymVt">
      <node concept="3cqZAl" id="27Ik2x6U4U2" role="3clF45" />
      <node concept="3clFbS" id="27Ik2x6U4U4" role="3clF47" />
      <node concept="3Tm6S6" id="27Ik2x6UkbI" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="27Ik2x6U4Zx" role="jymVt" />
    <node concept="2YIFZL" id="27Ik2x6U5wb" role="jymVt">
      <property role="TrG5h" value="getINSTANCE" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="27Ik2x6U5wd" role="3clF47">
        <node concept="3clFbJ" id="27Ik2x6U5AL" role="3cqZAp">
          <node concept="3clFbC" id="27Ik2x6U5O$" role="3clFbw">
            <node concept="10Nm6u" id="27Ik2x6U5Tw" role="3uHU7w" />
            <node concept="37vLTw" id="27Ik2x6U5BR" role="3uHU7B">
              <ref role="3cqZAo" node="27Ik2x6U3U2" resolve="INSTANCE" />
            </node>
          </node>
          <node concept="3clFbS" id="27Ik2x6U5AN" role="3clFbx">
            <node concept="3clFbF" id="27Ik2x6U65L" role="3cqZAp">
              <node concept="37vLTI" id="27Ik2x6U6gE" role="3clFbG">
                <node concept="2ShNRf" id="27Ik2x6U6n$" role="37vLTx">
                  <node concept="1pGfFk" id="27Ik2x6Uk71" role="2ShVmc">
                    <ref role="37wK5l" node="27Ik2x6U4U1" resolve="MyChangeListener" />
                  </node>
                </node>
                <node concept="37vLTw" id="27Ik2x6U65K" role="37vLTJ">
                  <ref role="3cqZAo" node="27Ik2x6U3U2" resolve="INSTANCE" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="27Ik2x6U5UH" role="3cqZAp">
          <node concept="37vLTw" id="27Ik2x6U60g" role="3cqZAk">
            <ref role="3cqZAo" node="27Ik2x6U3U2" resolve="INSTANCE" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="27Ik2x6U5wf" role="3clF45">
        <ref role="3uigEE" node="27Ik2x6U3cB" resolve="MyChangeListener" />
      </node>
      <node concept="3Tm1VV" id="27Ik2x6U5we" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="27Ik2x6U3xq" role="jymVt" />
    <node concept="3Tm1VV" id="27Ik2x6U3cC" role="1B3o_S" />
    <node concept="3uibUv" id="27Ik2x6U3dq" role="EKbjA">
      <ref role="3uigEE" to="mhbf:~SNodeChangeListener" resolve="SNodeChangeListener" />
    </node>
    <node concept="3clFb_" id="27Ik2x6U3dI" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="propertyChanged" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="27Ik2x6U3dJ" role="1B3o_S" />
      <node concept="3cqZAl" id="27Ik2x6U3dL" role="3clF45" />
      <node concept="37vLTG" id="27Ik2x6U3dM" role="3clF46">
        <property role="TrG5h" value="event" />
        <node concept="3uibUv" id="27Ik2x6U3dN" role="1tU5fm">
          <ref role="3uigEE" to="cmfw:~SPropertyChangeEvent" resolve="SPropertyChangeEvent" />
        </node>
        <node concept="2AHcQZ" id="27Ik2x6U3dO" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="3clFbS" id="27Ik2x6U3dP" role="3clF47">
        <node concept="34ab3g" id="1LOfuoikWWL" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="1LOfuoilfV8" role="34bqiv">
            <node concept="2OqwBi" id="1LOfuoilftt" role="3uHU7w">
              <node concept="2OqwBi" id="1LOfuoileUe" role="2Oq$k0">
                <node concept="37vLTw" id="1LOfuoileGI" role="2Oq$k0">
                  <ref role="3cqZAo" node="27Ik2x6U3dM" resolve="event" />
                </node>
                <node concept="liA8E" id="1LOfuoilffI" role="2OqNvi">
                  <ref role="37wK5l" to="cmfw:~SPropertyChangeEvent.getProperty():org.jetbrains.mps.openapi.language.SProperty" resolve="getProperty" />
                </node>
              </node>
              <node concept="liA8E" id="1LOfuoilfOu" role="2OqNvi">
                <ref role="37wK5l" to="c17a:~SProperty.getName():java.lang.String" resolve="getName" />
              </node>
            </node>
            <node concept="3cpWs3" id="1LOfuoileop" role="3uHU7B">
              <node concept="3cpWs3" id="1LOfuoilca6" role="3uHU7B">
                <node concept="Xl_RD" id="1LOfuoikWWN" role="3uHU7B">
                  <property role="Xl_RC" value="Property Changed. Node Concept: " />
                </node>
                <node concept="2OqwBi" id="1LOfuoildml" role="3uHU7w">
                  <node concept="2OqwBi" id="1LOfuoilcN8" role="2Oq$k0">
                    <node concept="2OqwBi" id="1LOfuoilcnF" role="2Oq$k0">
                      <node concept="37vLTw" id="1LOfuoilcaV" role="2Oq$k0">
                        <ref role="3cqZAo" node="27Ik2x6U3dM" resolve="event" />
                      </node>
                      <node concept="liA8E" id="1LOfuoilcCr" role="2OqNvi">
                        <ref role="37wK5l" to="cmfw:~SPropertyChangeEvent.getNode():org.jetbrains.mps.openapi.model.SNode" resolve="getNode" />
                      </node>
                    </node>
                    <node concept="liA8E" id="1LOfuoild8u" role="2OqNvi">
                      <ref role="37wK5l" to="mhbf:~SNode.getConcept():org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
                    </node>
                  </node>
                  <node concept="liA8E" id="1LOfuoildRr" role="2OqNvi">
                    <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="1LOfuoilgbn" role="3uHU7w">
                <property role="Xl_RC" value=" PropertyName: " />
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="1LOfuoimdlT" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="1LOfuoimfI9" role="34bqiv">
            <node concept="2OqwBi" id="1LOfuoimg7e" role="3uHU7w">
              <node concept="37vLTw" id="1LOfuoimfU1" role="2Oq$k0">
                <ref role="3cqZAo" node="27Ik2x6U3dM" resolve="event" />
              </node>
              <node concept="liA8E" id="1LOfuoimgCp" role="2OqNvi">
                <ref role="37wK5l" to="cmfw:~SPropertyChangeEvent.getNewValue():java.lang.String" resolve="getNewValue" />
              </node>
            </node>
            <node concept="3cpWs3" id="1LOfuoimfbh" role="3uHU7B">
              <node concept="3cpWs3" id="1LOfuoimdPZ" role="3uHU7B">
                <node concept="Xl_RD" id="1LOfuoimdlV" role="3uHU7B">
                  <property role="Xl_RC" value="old: " />
                </node>
                <node concept="2OqwBi" id="1LOfuoime93" role="3uHU7w">
                  <node concept="37vLTw" id="1LOfuoimdWj" role="2Oq$k0">
                    <ref role="3cqZAo" node="27Ik2x6U3dM" resolve="event" />
                  </node>
                  <node concept="liA8E" id="1LOfuoimepN" role="2OqNvi">
                    <ref role="37wK5l" to="cmfw:~SPropertyChangeEvent.getOldValue():java.lang.String" resolve="getOldValue" />
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="1LOfuoimfnP" role="3uHU7w">
                <property role="Xl_RC" value=" new: " />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="27Ik2x6U3dQ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="27Ik2x6U3dR" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="referenceChanged" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="27Ik2x6U3dS" role="1B3o_S" />
      <node concept="3cqZAl" id="27Ik2x6U3dU" role="3clF45" />
      <node concept="37vLTG" id="27Ik2x6U3dV" role="3clF46">
        <property role="TrG5h" value="event" />
        <node concept="3uibUv" id="27Ik2x6U3dW" role="1tU5fm">
          <ref role="3uigEE" to="cmfw:~SReferenceChangeEvent" resolve="SReferenceChangeEvent" />
        </node>
        <node concept="2AHcQZ" id="27Ik2x6U3dX" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="3clFbS" id="27Ik2x6U3dY" role="3clF47" />
      <node concept="2AHcQZ" id="27Ik2x6U3dZ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="27Ik2x6U3e0" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="nodeAdded" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="27Ik2x6U3e1" role="1B3o_S" />
      <node concept="3cqZAl" id="27Ik2x6U3e3" role="3clF45" />
      <node concept="37vLTG" id="27Ik2x6U3e4" role="3clF46">
        <property role="TrG5h" value="event" />
        <node concept="3uibUv" id="27Ik2x6U3e5" role="1tU5fm">
          <ref role="3uigEE" to="cmfw:~SNodeAddEvent" resolve="SNodeAddEvent" />
        </node>
        <node concept="2AHcQZ" id="27Ik2x6U3e6" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="3clFbS" id="27Ik2x6U3e7" role="3clF47">
        <node concept="34ab3g" id="27Ik2x6UkId" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="27Ik2x6UlzC" role="34bqiv">
            <node concept="2OqwBi" id="27Ik2x6UCke" role="3uHU7w">
              <node concept="2OqwBi" id="27Ik2x6Umh9" role="2Oq$k0">
                <node concept="2OqwBi" id="27Ik2x6UlPG" role="2Oq$k0">
                  <node concept="37vLTw" id="27Ik2x6Ul$Q" role="2Oq$k0">
                    <ref role="3cqZAo" node="27Ik2x6U3e4" resolve="event" />
                  </node>
                  <node concept="liA8E" id="27Ik2x6Um6s" role="2OqNvi">
                    <ref role="37wK5l" to="cmfw:~SNodeAddEvent.getChild():org.jetbrains.mps.openapi.model.SNode" resolve="getChild" />
                  </node>
                </node>
                <node concept="liA8E" id="27Ik2x6UC43" role="2OqNvi">
                  <ref role="37wK5l" to="mhbf:~SNode.getConcept():org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
                </node>
              </node>
              <node concept="liA8E" id="27Ik2x6UCPc" role="2OqNvi">
                <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
              </node>
            </node>
            <node concept="Xl_RD" id="27Ik2x6UkIf" role="3uHU7B">
              <property role="Xl_RC" value="node added " />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="27Ik2x6U3e8" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="27Ik2x6U3e9" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="nodeRemoved" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="27Ik2x6U3ea" role="1B3o_S" />
      <node concept="3cqZAl" id="27Ik2x6U3ec" role="3clF45" />
      <node concept="37vLTG" id="27Ik2x6U3ed" role="3clF46">
        <property role="TrG5h" value="event" />
        <node concept="3uibUv" id="27Ik2x6U3ee" role="1tU5fm">
          <ref role="3uigEE" to="cmfw:~SNodeRemoveEvent" resolve="SNodeRemoveEvent" />
        </node>
        <node concept="2AHcQZ" id="27Ik2x6U3ef" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="3clFbS" id="27Ik2x6U3eg" role="3clF47">
        <node concept="34ab3g" id="27Ik2x6Um$i" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="27Ik2x6Um$j" role="34bqiv">
            <node concept="2OqwBi" id="27Ik2x6UBd3" role="3uHU7w">
              <node concept="2OqwBi" id="27Ik2x6Um$k" role="2Oq$k0">
                <node concept="2OqwBi" id="27Ik2x6Um$l" role="2Oq$k0">
                  <node concept="37vLTw" id="27Ik2x6UmN$" role="2Oq$k0">
                    <ref role="3cqZAo" node="27Ik2x6U3ed" resolve="event" />
                  </node>
                  <node concept="liA8E" id="27Ik2x6Um$n" role="2OqNvi">
                    <ref role="37wK5l" to="cmfw:~SNodeRemoveEvent.getChild():org.jetbrains.mps.openapi.model.SNode" resolve="getChild" />
                  </node>
                </node>
                <node concept="liA8E" id="27Ik2x6UAZv" role="2OqNvi">
                  <ref role="37wK5l" to="mhbf:~SNode.getConcept():org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
                </node>
              </node>
              <node concept="liA8E" id="27Ik2x6UBHT" role="2OqNvi">
                <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
              </node>
            </node>
            <node concept="Xl_RD" id="27Ik2x6Um$p" role="3uHU7B">
              <property role="Xl_RC" value="node removed 2 " />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="27Ik2x6U3eh" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="3Gq9cqnDDmG">
    <property role="TrG5h" value="MakeMandatory" />
    <ref role="2ZfgGC" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="2S6ZIM" id="3Gq9cqnDDmH" role="2ZfVej">
      <node concept="3clFbS" id="3Gq9cqnDDmI" role="2VODD2">
        <node concept="3clFbF" id="3Gq9cqnDWpo" role="3cqZAp">
          <node concept="Xl_RD" id="3Gq9cqnDWpn" role="3clFbG">
            <property role="Xl_RC" value="Make #if's true branch mandatory" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="3Gq9cqnDDmJ" role="2ZfgGD">
      <node concept="3clFbS" id="3Gq9cqnDDmK" role="2VODD2">
        <node concept="34ab3g" id="3Gq9cqnFnAg" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="3Gq9cqnFnAi" role="34bqiv">
            <property role="Xl_RC" value="Make mandatory" />
          </node>
        </node>
        <node concept="2Gpval" id="3Gq9cqnFVOP" role="3cqZAp">
          <node concept="2GrKxI" id="3Gq9cqnFVOR" role="2Gsz3X">
            <property role="TrG5h" value="statement" />
          </node>
          <node concept="2OqwBi" id="3Gq9cqnFVXA" role="2GsD0m">
            <node concept="2Sf5sV" id="3Gq9cqnFVPH" role="2Oq$k0" />
            <node concept="3Tsc0h" id="3Gq9cqnFW6T" role="2OqNvi">
              <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
            </node>
          </node>
          <node concept="3clFbS" id="3Gq9cqnFVOV" role="2LFqv$">
            <node concept="3clFbF" id="3Gq9cqnFW9B" role="3cqZAp">
              <node concept="2OqwBi" id="3Gq9cqnFY9t" role="3clFbG">
                <node concept="2OqwBi" id="3Gq9cqnFWf0" role="2Oq$k0">
                  <node concept="2GrUjf" id="3Gq9cqnFW9A" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="3Gq9cqnFVOR" resolve="statement" />
                  </node>
                  <node concept="2Rf3mk" id="3Gq9cqnFWoX" role="2OqNvi">
                    <node concept="1xMEDy" id="3Gq9cqnFWoZ" role="1xVPHs">
                      <node concept="chp4Y" id="3Gq9cqnFWvE" role="ri$Ld">
                        <ref role="cht4Q" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2es0OD" id="3Gq9cqnG1q1" role="2OqNvi">
                  <node concept="1bVj0M" id="3Gq9cqnG1q3" role="23t8la">
                    <node concept="3clFbS" id="3Gq9cqnG1q4" role="1bW5cS">
                      <node concept="3clFbF" id="3Gq9cqnG1tR" role="3cqZAp">
                        <node concept="37vLTI" id="3Gq9cqnG2jX" role="3clFbG">
                          <node concept="3clFbT" id="3Gq9cqnG2lV" role="37vLTx">
                            <property role="3clFbU" value="true" />
                          </node>
                          <node concept="2OqwBi" id="3Gq9cqnG1_h" role="37vLTJ">
                            <node concept="37vLTw" id="3Gq9cqnG1tQ" role="2Oq$k0">
                              <ref role="3cqZAo" node="3Gq9cqnG1q5" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3Gq9cqnG1SY" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="3Gq9cqnG2BF" role="3cqZAp">
                        <node concept="37vLTI" id="3Gq9cqnG3zG" role="3clFbG">
                          <node concept="Xl_RD" id="3Gq9cqnG3Ph" role="37vLTx">
                            <property role="Xl_RC" value="true" />
                          </node>
                          <node concept="2OqwBi" id="3Gq9cqnG2Jl" role="37vLTJ">
                            <node concept="37vLTw" id="3Gq9cqnG2BD" role="2Oq$k0">
                              <ref role="3cqZAo" node="3Gq9cqnG1q5" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3Gq9cqnG2W$" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="3Gq9cqnG1q5" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="3Gq9cqnG1q6" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3Gq9cqnG$$g" role="3cqZAp">
              <node concept="3clFbS" id="3Gq9cqnG$$i" role="3clFbx">
                <node concept="3clFbF" id="3Gq9cqnGAe0" role="3cqZAp">
                  <node concept="37vLTI" id="3Gq9cqnGCa0" role="3clFbG">
                    <node concept="3clFbT" id="3Gq9cqnGCdv" role="37vLTx">
                      <property role="3clFbU" value="true" />
                    </node>
                    <node concept="2OqwBi" id="3Gq9cqnGBtH" role="37vLTJ">
                      <node concept="1PxgMI" id="3Gq9cqnGB2$" role="2Oq$k0">
                        <property role="1BlNFB" value="true" />
                        <ref role="1m5ApE" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                        <node concept="2GrUjf" id="3Gq9cqnGAdY" role="1m5AlR">
                          <ref role="2Gs0qQ" node="3Gq9cqnFVOR" resolve="statement" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="3Gq9cqnGBLk" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3Gq9cqnGCp1" role="3cqZAp">
                  <node concept="37vLTI" id="3Gq9cqnGCp2" role="3clFbG">
                    <node concept="Xl_RD" id="3Gq9cqnGD37" role="37vLTx">
                      <property role="Xl_RC" value="true" />
                    </node>
                    <node concept="2OqwBi" id="3Gq9cqnGCp4" role="37vLTJ">
                      <node concept="1PxgMI" id="3Gq9cqnGCp5" role="2Oq$k0">
                        <property role="1BlNFB" value="true" />
                        <ref role="1m5ApE" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                        <node concept="2GrUjf" id="3Gq9cqnGCp6" role="1m5AlR">
                          <ref role="2Gs0qQ" node="3Gq9cqnFVOR" resolve="statement" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="3Gq9cqnGCTe" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="3Gq9cqnGCoM" role="3cqZAp" />
              </node>
              <node concept="2OqwBi" id="3Gq9cqnG_OF" role="3clFbw">
                <node concept="2OqwBi" id="3Gq9cqnG$Gh" role="2Oq$k0">
                  <node concept="2GrUjf" id="3Gq9cqnG$_h" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="3Gq9cqnFVOR" resolve="statement" />
                  </node>
                  <node concept="2yIwOk" id="3Gq9cqnG_1Z" role="2OqNvi" />
                </node>
                <node concept="3O6GUB" id="3Gq9cqnGA6K" role="2OqNvi">
                  <node concept="chp4Y" id="3Gq9cqnGAa0" role="3QVz_e">
                    <ref role="cht4Q" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="3Gq9cqnEHeE" role="2ZfVeh">
      <node concept="3clFbS" id="3Gq9cqnEHeF" role="2VODD2">
        <node concept="3clFbF" id="3Gq9cqnEHlO" role="3cqZAp">
          <node concept="1Wc70l" id="3Gq9cqnEMwI" role="3clFbG">
            <node concept="2OqwBi" id="3Gq9cqnEOUg" role="3uHU7w">
              <node concept="2OqwBi" id="3Gq9cqnEMQp" role="2Oq$k0">
                <node concept="2Sf5sV" id="3Gq9cqnEMER" role="2Oq$k0" />
                <node concept="3Tsc0h" id="3Gq9cqnENlg" role="2OqNvi">
                  <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                </node>
              </node>
              <node concept="1v1jN8" id="3Gq9cqnERRe" role="2OqNvi" />
            </node>
            <node concept="2OqwBi" id="3Gq9cqnEJmx" role="3uHU7B">
              <node concept="2OqwBi" id="3Gq9cqnEHx3" role="2Oq$k0">
                <node concept="2Sf5sV" id="3Gq9cqnEHlN" role="2Oq$k0" />
                <node concept="3Tsc0h" id="3Gq9cqnEHTS" role="2OqNvi">
                  <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                </node>
              </node>
              <node concept="1v1jN8" id="3Gq9cqnELND" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="33HeFyz0Eqd">
    <property role="TrG5h" value="VTSWrapper" />
    <node concept="3clFb_" id="33HeFyz10W0" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="commit" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="33HeFyz10W3" role="3clF47">
        <node concept="3cpWs8" id="33HeFyz10Wm" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10Wn" role="3cpWs9">
            <property role="TrG5h" value="pathOriginal" />
            <node concept="17QB3L" id="33HeFyz10Wo" role="1tU5fm" />
            <node concept="2OqwBi" id="33HeFyz10Wp" role="33vP2m">
              <node concept="2ShNRf" id="33HeFyz10Wq" role="2Oq$k0">
                <node concept="1pGfFk" id="33HeFyz10Wr" role="2ShVmc">
                  <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                  <node concept="2OqwBi" id="33HeFyz10Ws" role="37wK5m">
                    <node concept="2YIFZM" id="33HeFyz10Wt" role="2Oq$k0">
                      <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                      <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                    </node>
                    <node concept="liA8E" id="33HeFyz10Wu" role="2OqNvi">
                      <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                      <node concept="Xl_RD" id="33HeFyz10Wv" role="37wK5m">
                        <property role="Xl_RC" value="${TempFolder}" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="33HeFyz10Ww" role="37wK5m">
                    <property role="Xl_RC" value="MPSOutputFileCheckout.txt" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="33HeFyz10Wx" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz10Wy" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10Wz" role="3cpWs9">
            <property role="TrG5h" value="pathEdited" />
            <node concept="17QB3L" id="33HeFyz10W$" role="1tU5fm" />
            <node concept="2OqwBi" id="33HeFyz10W_" role="33vP2m">
              <node concept="2ShNRf" id="33HeFyz10WA" role="2Oq$k0">
                <node concept="1pGfFk" id="33HeFyz10WB" role="2ShVmc">
                  <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                  <node concept="2OqwBi" id="33HeFyz10WC" role="37wK5m">
                    <node concept="2YIFZM" id="33HeFyz10WD" role="2Oq$k0">
                      <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                      <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                    </node>
                    <node concept="liA8E" id="33HeFyz10WE" role="2OqNvi">
                      <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                      <node concept="Xl_RD" id="33HeFyz10WF" role="37wK5m">
                        <property role="Xl_RC" value="${TempFolder}" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="33HeFyz10WG" role="37wK5m">
                    <property role="Xl_RC" value="MPSOutputFileEdited.txt" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="33HeFyz10WH" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz4QXp" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz4QXq" role="3cpWs9">
            <property role="TrG5h" value="pathEditedPreEscaped" />
            <node concept="17QB3L" id="33HeFyz4QXr" role="1tU5fm" />
            <node concept="2OqwBi" id="33HeFyz4QXs" role="33vP2m">
              <node concept="2ShNRf" id="33HeFyz4QXt" role="2Oq$k0">
                <node concept="1pGfFk" id="33HeFyz4QXu" role="2ShVmc">
                  <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                  <node concept="2OqwBi" id="33HeFyz4QXv" role="37wK5m">
                    <node concept="2YIFZM" id="33HeFyz4QXw" role="2Oq$k0">
                      <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                      <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                    </node>
                    <node concept="liA8E" id="33HeFyz4QXx" role="2OqNvi">
                      <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                      <node concept="Xl_RD" id="33HeFyz4QXy" role="37wK5m">
                        <property role="Xl_RC" value="${TempFolder}" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="33HeFyz4QXz" role="37wK5m">
                    <property role="Xl_RC" value="MPSOutputFileEditedPreescape.txt" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="33HeFyz4QX$" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10WI" role="3cqZAp" />
        <node concept="3clFbF" id="33HeFyzkdcD" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzkeQ6" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzkdcB" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzkguX" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="Xl_RD" id="33HeFyzkgwI" role="37wK5m">
                <property role="Xl_RC" value="Generate edited source file." />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyzki2U" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzkjGL" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzki2S" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzklzC" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.advance(int):void" resolve="advance" />
              <node concept="3cmrfG" id="33HeFyzkl$C" role="37wK5m">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyzkl_h" role="3cqZAp" />
        <node concept="3cpWs8" id="33HeFyz4owO" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz4owR" role="3cpWs9">
            <property role="TrG5h" value="edited" />
            <node concept="17QB3L" id="33HeFyz4owM" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyzk2cK" role="3cqZAp">
          <node concept="2YIFZM" id="33HeFyzk57p" role="3clFbG">
            <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
            <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
            <node concept="1bVj0M" id="33HeFyzk6zS" role="37wK5m">
              <node concept="3clFbS" id="33HeFyzk6zT" role="1bW5cS">
                <node concept="3clFbF" id="33HeFyzlNuV" role="3cqZAp">
                  <node concept="2OqwBi" id="33HeFyzlNTm" role="3clFbG">
                    <node concept="2OqwBi" id="33HeFyzlN_s" role="2Oq$k0">
                      <node concept="37vLTw" id="33HeFyzlNuU" role="2Oq$k0">
                        <ref role="3cqZAo" node="33HeFyzlLWf" resolve="repo" />
                      </node>
                      <node concept="liA8E" id="33HeFyzlNN5" role="2OqNvi">
                        <ref role="37wK5l" to="lui2:~SRepository.getModelAccess():org.jetbrains.mps.openapi.module.ModelAccess" resolve="getModelAccess" />
                      </node>
                    </node>
                    <node concept="liA8E" id="33HeFyzlO7X" role="2OqNvi">
                      <ref role="37wK5l" to="lui2:~ModelAccess.runReadAction(java.lang.Runnable):void" resolve="runReadAction" />
                      <node concept="1bVj0M" id="33HeFyzlOaI" role="37wK5m">
                        <node concept="3clFbS" id="33HeFyzlOaJ" role="1bW5cS">
                          <node concept="3clFbF" id="33HeFyzjZfs" role="3cqZAp">
                            <node concept="37vLTI" id="33HeFyzjZfu" role="3clFbG">
                              <node concept="2YIFZM" id="33HeFyz4q4m" role="37vLTx">
                                <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
                                <ref role="37wK5l" to="7zqe:2NOvNR_l$0S" resolve="generateNode" />
                                <node concept="37vLTw" id="33HeFyz4q5$" role="37wK5m">
                                  <ref role="3cqZAo" node="33HeFyz14jk" resolve="node" />
                                </node>
                              </node>
                              <node concept="37vLTw" id="33HeFyzjZfy" role="37vLTJ">
                                <ref role="3cqZAo" node="33HeFyz4owR" resolve="edited" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz4rDV" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz4rDY" role="3cpWs9">
            <property role="TrG5h" value="escaped" />
            <node concept="17QB3L" id="33HeFyz4rDT" role="1tU5fm" />
            <node concept="2YIFZM" id="33HeFyz5jg7" role="33vP2m">
              <ref role="37wK5l" to="7zqe:33HeFyz5b2h" resolve="escapeNonBoolean" />
              <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
              <node concept="37vLTw" id="33HeFyz5kIT" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz4owR" resolve="edited" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz4xIa" role="3cqZAp" />
        <node concept="3clFbF" id="33HeFyzkoCi" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzkqix" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzkoCg" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzkrVY" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="Xl_RD" id="33HeFyzkrXJ" role="37wK5m">
                <property role="Xl_RC" value="Dump edited files to temp folder." />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyzks1_" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzktGb" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzks1z" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzkvlV" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.advance(int):void" resolve="advance" />
              <node concept="3cmrfG" id="33HeFyzkvnI" role="37wK5m">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyzkvwI" role="3cqZAp" />
        <node concept="3SKdUt" id="33HeFyz4KPC" role="3cqZAp">
          <node concept="3SKdUq" id="33HeFyz4KPE" role="3SKWNk">
            <property role="3SKdUp" value="DEBUG: dump file" />
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyz4Uwd" role="3cqZAp">
          <node concept="2YIFZM" id="33HeFyz4Uwe" role="3clFbG">
            <ref role="37wK5l" to="7zqe:33HeFyz4Cut" resolve="dumpFile" />
            <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
            <node concept="37vLTw" id="33HeFyz4W7J" role="37wK5m">
              <ref role="3cqZAo" node="33HeFyz4owR" resolve="edited" />
            </node>
            <node concept="37vLTw" id="33HeFyz4W4y" role="37wK5m">
              <ref role="3cqZAo" node="33HeFyz4QXq" resolve="pathEditedPreEscaped" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyz4NPl" role="3cqZAp">
          <node concept="2YIFZM" id="33HeFyz4Pmh" role="3clFbG">
            <ref role="37wK5l" to="7zqe:33HeFyz4Cut" resolve="dumpFile" />
            <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
            <node concept="37vLTw" id="33HeFyz4QOD" role="37wK5m">
              <ref role="3cqZAo" node="33HeFyz4rDY" resolve="escaped" />
            </node>
            <node concept="37vLTw" id="33HeFyz4QTr" role="37wK5m">
              <ref role="3cqZAo" node="33HeFyz10Wz" resolve="pathEdited" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10WN" role="3cqZAp" />
        <node concept="3clFbH" id="33HeFyzmsHe" role="3cqZAp" />
        <node concept="3cpWs8" id="33HeFyz10Xm" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10Xn" role="3cpWs9">
            <property role="TrG5h" value="ambition" />
            <node concept="17QB3L" id="33HeFyz10Xo" role="1tU5fm" />
            <node concept="Xl_RD" id="33HeFyz10Xp" role="33vP2m">
              <property role="Xl_RC" value="true" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyzoywi" role="3cqZAp">
          <node concept="2YIFZM" id="33HeFyzoywj" role="3clFbG">
            <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
            <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
            <node concept="1bVj0M" id="33HeFyzoywk" role="37wK5m">
              <node concept="3clFbS" id="33HeFyzoywl" role="1bW5cS">
                <node concept="3clFbF" id="33HeFyzoywm" role="3cqZAp">
                  <node concept="2OqwBi" id="33HeFyzoywn" role="3clFbG">
                    <node concept="2OqwBi" id="33HeFyzoywo" role="2Oq$k0">
                      <node concept="37vLTw" id="33HeFyzoywp" role="2Oq$k0">
                        <ref role="3cqZAo" node="33HeFyzlLWf" resolve="repo" />
                      </node>
                      <node concept="liA8E" id="33HeFyzoywq" role="2OqNvi">
                        <ref role="37wK5l" to="lui2:~SRepository.getModelAccess():org.jetbrains.mps.openapi.module.ModelAccess" resolve="getModelAccess" />
                      </node>
                    </node>
                    <node concept="liA8E" id="33HeFyzoywr" role="2OqNvi">
                      <ref role="37wK5l" to="lui2:~ModelAccess.runReadAction(java.lang.Runnable):void" resolve="runReadAction" />
                      <node concept="1bVj0M" id="33HeFyzoyws" role="37wK5m">
                        <node concept="3clFbS" id="33HeFyzoywt" role="1bW5cS">
                          <node concept="3clFbJ" id="33HeFyz10Xq" role="3cqZAp">
                            <node concept="3clFbS" id="33HeFyz10Xr" role="3clFbx">
                              <node concept="3clFbF" id="33HeFyz10Xs" role="3cqZAp">
                                <node concept="37vLTI" id="33HeFyz10Xt" role="3clFbG">
                                  <node concept="37vLTw" id="33HeFyz10Xu" role="37vLTJ">
                                    <ref role="3cqZAo" node="33HeFyz10Xn" resolve="ambition" />
                                  </node>
                                  <node concept="2OqwBi" id="33HeFyz10Xv" role="37vLTx">
                                    <node concept="37vLTw" id="33HeFyz1hLr" role="2Oq$k0">
                                      <ref role="3cqZAo" node="33HeFyz14jk" resolve="node" />
                                    </node>
                                    <node concept="3TrcHB" id="33HeFyz10Xx" role="2OqNvi">
                                      <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3y3z36" id="33HeFyz10Xy" role="3clFbw">
                              <node concept="10Nm6u" id="33HeFyz10Xz" role="3uHU7w" />
                              <node concept="2OqwBi" id="33HeFyz10X$" role="3uHU7B">
                                <node concept="37vLTw" id="33HeFyz1hIr" role="2Oq$k0">
                                  <ref role="3cqZAo" node="33HeFyz14jk" resolve="node" />
                                </node>
                                <node concept="3TrcHB" id="33HeFyz10XA" role="2OqNvi">
                                  <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyzowWZ" role="3cqZAp" />
        <node concept="34ab3g" id="33HeFyz10WO" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="33HeFyz10WP" role="34bqiv">
            <node concept="Xl_RD" id="33HeFyz10WT" role="3uHU7B">
              <property role="Xl_RC" value="Commit with ambition " />
            </node>
            <node concept="37vLTw" id="33HeFyzmxu7" role="3uHU7w">
              <ref role="3cqZAo" node="33HeFyz10Xn" resolve="ambition" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10WU" role="3cqZAp" />
        <node concept="3clFbF" id="33HeFyzkxhe" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzkxhf" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzkxhg" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzkxhh" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="3cpWs3" id="33HeFyzkyOe" role="37wK5m">
                <node concept="Xl_RD" id="33HeFyzkyOi" role="3uHU7B">
                  <property role="Xl_RC" value="Commit with ambition " />
                </node>
                <node concept="37vLTw" id="33HeFyzmz7b" role="3uHU7w">
                  <ref role="3cqZAo" node="33HeFyz10Xn" resolve="ambition" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyzkxhj" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzkxhk" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzkxhl" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzkxhm" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.advance(int):void" resolve="advance" />
              <node concept="3cmrfG" id="33HeFyzkxhn" role="37wK5m">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyzkxe3" role="3cqZAp" />
        <node concept="3cpWs8" id="33HeFyz10WV" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10WW" role="3cpWs9">
            <property role="TrG5h" value="parsedFile1" />
            <node concept="3uibUv" id="33HeFyz10WX" role="1tU5fm">
              <ref role="3uigEE" to="kjvu:~Either" resolve="Either" />
              <node concept="3uibUv" id="33HeFyz10WY" role="11_B2D">
                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
              </node>
              <node concept="3uibUv" id="33HeFyz10WZ" role="11_B2D">
                <ref role="3uigEE" to="d64m:~Seq" resolve="Seq" />
                <node concept="3uibUv" id="33HeFyz10X0" role="11_B2D">
                  <ref role="3uigEE" to="w4xx:~Choiced" resolve="Choiced" />
                  <node concept="3uibUv" id="33HeFyz10X1" role="11_B2D">
                    <ref role="3uigEE" to="w4xx:~Textline" resolve="Textline" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="33HeFyz10X2" role="33vP2m">
              <ref role="1Pybhc" to="9683:~Parser" resolve="Parser" />
              <ref role="37wK5l" to="9683:~Parser.parseFile(java.lang.String):scala.util.Either" resolve="parseFile" />
              <node concept="37vLTw" id="33HeFyz10X3" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz10Wn" resolve="pathOriginal" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz10X4" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10X5" role="3cpWs9">
            <property role="TrG5h" value="parsedFileEdited" />
            <node concept="3uibUv" id="33HeFyz10X6" role="1tU5fm">
              <ref role="3uigEE" to="kjvu:~Either" resolve="Either" />
              <node concept="3uibUv" id="33HeFyz10X7" role="11_B2D">
                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
              </node>
              <node concept="3uibUv" id="33HeFyz10X8" role="11_B2D">
                <ref role="3uigEE" to="d64m:~Seq" resolve="Seq" />
                <node concept="3uibUv" id="33HeFyz10X9" role="11_B2D">
                  <ref role="3uigEE" to="w4xx:~Choiced" resolve="Choiced" />
                  <node concept="3uibUv" id="33HeFyz10Xa" role="11_B2D">
                    <ref role="3uigEE" to="w4xx:~Textline" resolve="Textline" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="33HeFyz54IS" role="33vP2m">
              <ref role="37wK5l" to="9683:~Parser.parseString(java.lang.String):scala.util.Either" resolve="parseString" />
              <ref role="1Pybhc" to="9683:~Parser" resolve="Parser" />
              <node concept="37vLTw" id="33HeFyz56et" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz4rDY" resolve="escaped" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10Xd" role="3cqZAp" />
        <node concept="3cpWs8" id="33HeFyz10Xe" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10Xf" role="3cpWs9">
            <property role="TrG5h" value="exprTrue" />
            <node concept="3uibUv" id="33HeFyz10Xg" role="1tU5fm">
              <ref role="3uigEE" to="m798:~Expression" resolve="Expression" />
            </node>
            <node concept="2OqwBi" id="33HeFyz10Xh" role="33vP2m">
              <node concept="2YIFZM" id="33HeFyz10Xi" role="2Oq$k0">
                <ref role="37wK5l" to="m798:~ExpressionParser.parse(java.lang.String):scala.Option" resolve="parse" />
                <ref role="1Pybhc" to="m798:~ExpressionParser" resolve="ExpressionParser" />
                <node concept="Xl_RD" id="33HeFyz10Xj" role="37wK5m">
                  <property role="Xl_RC" value="true" />
                </node>
              </node>
              <node concept="liA8E" id="33HeFyz10Xk" role="2OqNvi">
                <ref role="37wK5l" to="1miu:~Option.get():java.lang.Object" resolve="get" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10Xl" role="3cqZAp" />
        <node concept="3cpWs8" id="33HeFyz10XB" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10XC" role="3cpWs9">
            <property role="TrG5h" value="exprAmbition" />
            <node concept="3uibUv" id="33HeFyz10XD" role="1tU5fm">
              <ref role="3uigEE" to="m798:~Expression" resolve="Expression" />
            </node>
            <node concept="2OqwBi" id="33HeFyz10XE" role="33vP2m">
              <node concept="2YIFZM" id="33HeFyz10XF" role="2Oq$k0">
                <ref role="37wK5l" to="m798:~ExpressionParser.parse(java.lang.String):scala.Option" resolve="parse" />
                <ref role="1Pybhc" to="m798:~ExpressionParser" resolve="ExpressionParser" />
                <node concept="37vLTw" id="33HeFyz10XG" role="37wK5m">
                  <ref role="3cqZAo" node="33HeFyz10Xn" resolve="ambition" />
                </node>
              </node>
              <node concept="liA8E" id="33HeFyz10XH" role="2OqNvi">
                <ref role="37wK5l" to="1miu:~Option.get():java.lang.Object" resolve="get" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10XI" role="3cqZAp" />
        <node concept="34ab3g" id="33HeFyz10XJ" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="33HeFyz10XK" role="34bqiv">
            <node concept="37vLTw" id="33HeFyz10XL" role="3uHU7w">
              <ref role="3cqZAo" node="33HeFyz10Xf" resolve="exprTrue" />
            </node>
            <node concept="Xl_RD" id="33HeFyz10XM" role="3uHU7B">
              <property role="Xl_RC" value="exprTrue " />
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="33HeFyz10XN" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="33HeFyz10XO" role="34bqiv">
            <node concept="37vLTw" id="33HeFyz10XP" role="3uHU7w">
              <ref role="3cqZAo" node="33HeFyz10XC" resolve="exprAmbition" />
            </node>
            <node concept="Xl_RD" id="33HeFyz10XQ" role="3uHU7B">
              <property role="Xl_RC" value="exprAmbition " />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10XS" role="3cqZAp" />
        <node concept="3cpWs8" id="33HeFyz10XT" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10XU" role="3cpWs9">
            <property role="TrG5h" value="checkedIn" />
            <node concept="3uibUv" id="33HeFyz10XV" role="1tU5fm">
              <ref role="3uigEE" to="d64m:~Seq" resolve="Seq" />
              <node concept="3uibUv" id="33HeFyz10XW" role="11_B2D">
                <ref role="3uigEE" to="w4xx:~Choiced" resolve="Choiced" />
                <node concept="3uibUv" id="33HeFyz10XX" role="11_B2D">
                  <ref role="3uigEE" to="w4xx:~Textline" resolve="Textline" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="33HeFyz10XY" role="33vP2m">
              <ref role="1Pybhc" to="7dys:~Checkin" resolve="Checkin" />
              <ref role="37wK5l" to="7dys:~Checkin.checkinWithNesting(dk.itu.vts.expression.Expression,dk.itu.vts.expression.Expression,scala.collection.Seq,scala.collection.Seq):scala.collection.Seq" resolve="checkinWithNesting" />
              <node concept="37vLTw" id="33HeFyz10XZ" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz10Xf" resolve="exprTrue" />
              </node>
              <node concept="37vLTw" id="33HeFyz10Y0" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz10XC" resolve="exprAmbition" />
              </node>
              <node concept="2OqwBi" id="33HeFyz10Y1" role="37wK5m">
                <node concept="2OqwBi" id="33HeFyz10Y2" role="2Oq$k0">
                  <node concept="37vLTw" id="33HeFyz10Y3" role="2Oq$k0">
                    <ref role="3cqZAo" node="33HeFyz10WW" resolve="parsedFile1" />
                  </node>
                  <node concept="liA8E" id="33HeFyz10Y4" role="2OqNvi">
                    <ref role="37wK5l" to="kjvu:~Either.right():scala.util.Either$RightProjection" resolve="right" />
                  </node>
                </node>
                <node concept="liA8E" id="33HeFyz10Y5" role="2OqNvi">
                  <ref role="37wK5l" to="kjvu:~Either$RightProjection.get():java.lang.Object" resolve="get" />
                </node>
              </node>
              <node concept="2OqwBi" id="33HeFyz10Y6" role="37wK5m">
                <node concept="2OqwBi" id="33HeFyz10Y7" role="2Oq$k0">
                  <node concept="37vLTw" id="33HeFyz10Y8" role="2Oq$k0">
                    <ref role="3cqZAo" node="33HeFyz10X5" resolve="parsedFileEdited" />
                  </node>
                  <node concept="liA8E" id="33HeFyz10Y9" role="2OqNvi">
                    <ref role="37wK5l" to="kjvu:~Either.right():scala.util.Either$RightProjection" resolve="right" />
                  </node>
                </node>
                <node concept="liA8E" id="33HeFyz10Ya" role="2OqNvi">
                  <ref role="37wK5l" to="kjvu:~Either$RightProjection.get():java.lang.Object" resolve="get" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10Yc" role="3cqZAp" />
        <node concept="34ab3g" id="33HeFyz10Yd" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="33HeFyz10Ye" role="34bqiv">
            <property role="Xl_RC" value="checkin done" />
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyzmzdm" role="3cqZAp" />
        <node concept="3clFbF" id="33HeFyzm$Np" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzm$Nq" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzm$Nr" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzm$Ns" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="Xl_RD" id="33HeFyzm$Nu" role="37wK5m">
                <property role="Xl_RC" value="Prettyprint result." />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyzm$Nw" role="3cqZAp">
          <node concept="2OqwBi" id="33HeFyzm$Nx" role="3clFbG">
            <node concept="37vLTw" id="33HeFyzm$Ny" role="2Oq$k0">
              <ref role="3cqZAo" node="33HeFyzkbIK" resolve="adapter" />
            </node>
            <node concept="liA8E" id="33HeFyzm$Nz" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.advance(int):void" resolve="advance" />
              <node concept="3cmrfG" id="33HeFyzm$N$" role="37wK5m">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="33HeFyz10Yf" role="3cqZAp" />
        <node concept="3cpWs8" id="DZ5VrSvMTx" role="3cqZAp">
          <node concept="3cpWsn" id="DZ5VrSvMTB" role="3cpWs9">
            <property role="TrG5h" value="scalaLines" />
            <node concept="3uibUv" id="DZ5VrSvMTD" role="1tU5fm">
              <ref role="3uigEE" to="47h6:~List" resolve="List" />
              <node concept="17QB3L" id="DZ5VrSvQRD" role="11_B2D" />
            </node>
            <node concept="2YIFZM" id="DZ5VrSvNNR" role="33vP2m">
              <ref role="37wK5l" to="ctra:~PrettyPrinter.prettyPrint(scala.collection.Seq):scala.collection.immutable.List" resolve="prettyPrint" />
              <ref role="1Pybhc" to="ctra:~PrettyPrinter" resolve="PrettyPrinter" />
              <node concept="37vLTw" id="DZ5VrSvNQp" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz10XU" resolve="checkedIn" />
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="33HeFyz10Ym" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="33HeFyz10Yn" role="34bqiv">
            <property role="Xl_RC" value="prettyprint done" />
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz10Yo" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10Yp" role="3cpWs9">
            <property role="TrG5h" value="lines" />
            <node concept="3uibUv" id="33HeFyz10Yq" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Collection" resolve="Collection" />
              <node concept="17QB3L" id="DZ5VrSvQ2G" role="11_B2D" />
            </node>
            <node concept="2YIFZM" id="33HeFyz10Ys" role="33vP2m">
              <ref role="1Pybhc" to="d64m:~JavaConversions" resolve="JavaConversions" />
              <ref role="37wK5l" to="d64m:~JavaConversions.asJavaCollection(scala.collection.Iterable):java.util.Collection" resolve="asJavaCollection" />
              <node concept="37vLTw" id="DZ5VrSzeu$" role="37wK5m">
                <ref role="3cqZAo" node="DZ5VrSvMTB" resolve="scalaLines" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz10Yu" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz10Yv" role="3cpWs9">
            <property role="TrG5h" value="prettyprinted" />
            <node concept="17QB3L" id="33HeFyz10Yw" role="1tU5fm" />
            <node concept="2YIFZM" id="33HeFyz10Yx" role="33vP2m">
              <ref role="37wK5l" to="btm1:~StringUtils.join(java.lang.Iterable,java.lang.String):java.lang.String" resolve="join" />
              <ref role="1Pybhc" to="btm1:~StringUtils" resolve="StringUtils" />
              <node concept="37vLTw" id="33HeFyz10Yy" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz10Yp" resolve="lines" />
              </node>
              <node concept="Xl_RD" id="33HeFyz10Yz" role="37wK5m">
                <property role="Xl_RC" value="\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="33HeFyz1mjE" role="3cqZAp">
          <node concept="37vLTw" id="33HeFyz1nN8" role="3cqZAk">
            <ref role="3cqZAo" node="33HeFyz10Yv" resolve="prettyprinted" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="33HeFyz10VK" role="1B3o_S" />
      <node concept="17QB3L" id="33HeFyz10VS" role="3clF45" />
      <node concept="37vLTG" id="33HeFyz14jk" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="33HeFyz14jj" role="1tU5fm">
          <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
        </node>
      </node>
      <node concept="37vLTG" id="33HeFyzkbIK" role="3clF46">
        <property role="TrG5h" value="adapter" />
        <node concept="3uibUv" id="33HeFyzl2ZQ" role="1tU5fm">
          <ref role="3uigEE" to="yyf4:~ProgressMonitor" resolve="ProgressMonitor" />
        </node>
      </node>
      <node concept="37vLTG" id="33HeFyzlLWf" role="3clF46">
        <property role="TrG5h" value="repo" />
        <node concept="3uibUv" id="33HeFyzlNsp" role="1tU5fm">
          <ref role="3uigEE" to="lui2:~SRepository" resolve="SRepository" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="33HeFyz0Eqe" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="5lhzOSqAL9w">
    <property role="TrG5h" value="VTSWrapperOptional" />
    <node concept="2tJIrI" id="34h7GzdX9rX" role="jymVt" />
    <node concept="3clFb_" id="5lhzOSqAL9x" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="commit" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5lhzOSqAL9y" role="3clF47">
        <node concept="3cpWs8" id="5lhzOSqAL9z" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqAL9$" role="3cpWs9">
            <property role="TrG5h" value="pathOriginal" />
            <node concept="17QB3L" id="5lhzOSqAL9_" role="1tU5fm" />
            <node concept="2OqwBi" id="5lhzOSqAL9A" role="33vP2m">
              <node concept="2ShNRf" id="5lhzOSqAL9B" role="2Oq$k0">
                <node concept="1pGfFk" id="5lhzOSqAL9C" role="2ShVmc">
                  <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                  <node concept="2OqwBi" id="5lhzOSqAL9D" role="37wK5m">
                    <node concept="2YIFZM" id="5lhzOSqAL9E" role="2Oq$k0">
                      <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                      <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                    </node>
                    <node concept="liA8E" id="5lhzOSqAL9F" role="2OqNvi">
                      <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                      <node concept="Xl_RD" id="5lhzOSqAL9G" role="37wK5m">
                        <property role="Xl_RC" value="${TempFolder}" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="5lhzOSqAL9H" role="37wK5m">
                    <property role="Xl_RC" value="MPSOutputFileCheckout.txt" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="5lhzOSqAL9I" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5lhzOSqAL9J" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqAL9K" role="3cpWs9">
            <property role="TrG5h" value="pathEdited" />
            <node concept="17QB3L" id="5lhzOSqAL9L" role="1tU5fm" />
            <node concept="2OqwBi" id="5lhzOSqAL9M" role="33vP2m">
              <node concept="2ShNRf" id="5lhzOSqAL9N" role="2Oq$k0">
                <node concept="1pGfFk" id="5lhzOSqAL9O" role="2ShVmc">
                  <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                  <node concept="2OqwBi" id="5lhzOSqAL9P" role="37wK5m">
                    <node concept="2YIFZM" id="5lhzOSqAL9Q" role="2Oq$k0">
                      <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                      <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                    </node>
                    <node concept="liA8E" id="5lhzOSqAL9R" role="2OqNvi">
                      <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                      <node concept="Xl_RD" id="5lhzOSqAL9S" role="37wK5m">
                        <property role="Xl_RC" value="${TempFolder}" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="5lhzOSqAL9T" role="37wK5m">
                    <property role="Xl_RC" value="MPSOutputFileEdited.txt" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="5lhzOSqAL9U" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5lhzOSqAL9V" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqAL9W" role="3cpWs9">
            <property role="TrG5h" value="pathEditedPreEscaped" />
            <node concept="17QB3L" id="5lhzOSqAL9X" role="1tU5fm" />
            <node concept="2OqwBi" id="5lhzOSqAL9Y" role="33vP2m">
              <node concept="2ShNRf" id="5lhzOSqAL9Z" role="2Oq$k0">
                <node concept="1pGfFk" id="5lhzOSqALa0" role="2ShVmc">
                  <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                  <node concept="2OqwBi" id="5lhzOSqALa1" role="37wK5m">
                    <node concept="2YIFZM" id="5lhzOSqALa2" role="2Oq$k0">
                      <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                      <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                    </node>
                    <node concept="liA8E" id="5lhzOSqALa3" role="2OqNvi">
                      <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                      <node concept="Xl_RD" id="5lhzOSqALa4" role="37wK5m">
                        <property role="Xl_RC" value="${TempFolder}" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="5lhzOSqALa5" role="37wK5m">
                    <property role="Xl_RC" value="MPSOutputFileEditedPreescape.txt" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="5lhzOSqALa6" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALa7" role="3cqZAp" />
        <node concept="3clFbF" id="5lhzOSqALa8" role="3cqZAp">
          <node concept="2OqwBi" id="5lhzOSqALa9" role="3clFbG">
            <node concept="37vLTw" id="5lhzOSqALaa" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALdr" resolve="adapter" />
            </node>
            <node concept="liA8E" id="5lhzOSqALab" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="Xl_RD" id="5lhzOSqALac" role="37wK5m">
                <property role="Xl_RC" value="Generate edited source file." />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5lhzOSqALad" role="3cqZAp">
          <node concept="2OqwBi" id="5lhzOSqALae" role="3clFbG">
            <node concept="37vLTw" id="5lhzOSqALaf" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALdr" resolve="adapter" />
            </node>
            <node concept="liA8E" id="5lhzOSqALag" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.advance(int):void" resolve="advance" />
              <node concept="3cmrfG" id="5lhzOSqALah" role="37wK5m">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALai" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqALaj" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALak" role="3cpWs9">
            <property role="TrG5h" value="edited" />
            <node concept="17QB3L" id="5lhzOSqALal" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="5lhzOSqALam" role="3cqZAp">
          <node concept="2YIFZM" id="5lhzOSqALan" role="3clFbG">
            <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
            <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
            <node concept="1bVj0M" id="5lhzOSqALao" role="37wK5m">
              <node concept="3clFbS" id="5lhzOSqALap" role="1bW5cS">
                <node concept="3clFbF" id="5lhzOSqALaq" role="3cqZAp">
                  <node concept="2OqwBi" id="5lhzOSqALar" role="3clFbG">
                    <node concept="2OqwBi" id="5lhzOSqALas" role="2Oq$k0">
                      <node concept="37vLTw" id="5lhzOSqALat" role="2Oq$k0">
                        <ref role="3cqZAo" node="5lhzOSqALdt" resolve="repo" />
                      </node>
                      <node concept="liA8E" id="5lhzOSqALau" role="2OqNvi">
                        <ref role="37wK5l" to="lui2:~SRepository.getModelAccess():org.jetbrains.mps.openapi.module.ModelAccess" resolve="getModelAccess" />
                      </node>
                    </node>
                    <node concept="liA8E" id="5lhzOSqALav" role="2OqNvi">
                      <ref role="37wK5l" to="lui2:~ModelAccess.runReadAction(java.lang.Runnable):void" resolve="runReadAction" />
                      <node concept="1bVj0M" id="5lhzOSqALaw" role="37wK5m">
                        <node concept="3clFbS" id="5lhzOSqALax" role="1bW5cS">
                          <node concept="3clFbF" id="5lhzOSqALay" role="3cqZAp">
                            <node concept="37vLTI" id="5lhzOSqALaz" role="3clFbG">
                              <node concept="2YIFZM" id="34h7Gze1aVj" role="37vLTx">
                                <ref role="37wK5l" to="7zqe:5lhzOSqAQG6" resolve="generateNodeButGivenOne" />
                                <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
                                <node concept="37vLTw" id="34h7Gze1aVk" role="37wK5m">
                                  <ref role="3cqZAo" node="5lhzOSqALdp" resolve="node" />
                                </node>
                              </node>
                              <node concept="37vLTw" id="5lhzOSqALaA" role="37vLTJ">
                                <ref role="3cqZAo" node="5lhzOSqALak" resolve="edited" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5lhzOSqALaB" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALaC" role="3cpWs9">
            <property role="TrG5h" value="escaped" />
            <node concept="17QB3L" id="5lhzOSqALaD" role="1tU5fm" />
            <node concept="2YIFZM" id="5lhzOSqALaE" role="33vP2m">
              <ref role="37wK5l" to="7zqe:33HeFyz5b2h" resolve="escapeNonBoolean" />
              <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
              <node concept="37vLTw" id="5lhzOSqALaF" role="37wK5m">
                <ref role="3cqZAo" node="5lhzOSqALak" resolve="edited" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALaG" role="3cqZAp" />
        <node concept="3clFbF" id="5lhzOSqALaH" role="3cqZAp">
          <node concept="2OqwBi" id="5lhzOSqALaI" role="3clFbG">
            <node concept="37vLTw" id="5lhzOSqALaJ" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALdr" resolve="adapter" />
            </node>
            <node concept="liA8E" id="5lhzOSqALaK" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="Xl_RD" id="5lhzOSqALaL" role="37wK5m">
                <property role="Xl_RC" value="Dump edited files to temp folder." />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5lhzOSqALaM" role="3cqZAp">
          <node concept="2OqwBi" id="5lhzOSqALaN" role="3clFbG">
            <node concept="37vLTw" id="5lhzOSqALaO" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALdr" resolve="adapter" />
            </node>
            <node concept="liA8E" id="5lhzOSqALaP" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.advance(int):void" resolve="advance" />
              <node concept="3cmrfG" id="5lhzOSqALaQ" role="37wK5m">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALaR" role="3cqZAp" />
        <node concept="3SKdUt" id="5lhzOSqALaS" role="3cqZAp">
          <node concept="3SKdUq" id="5lhzOSqALaT" role="3SKWNk">
            <property role="3SKdUp" value="DEBUG: dump file" />
          </node>
        </node>
        <node concept="3clFbF" id="5lhzOSqALaU" role="3cqZAp">
          <node concept="2YIFZM" id="5lhzOSqALaV" role="3clFbG">
            <ref role="37wK5l" to="7zqe:33HeFyz4Cut" resolve="dumpFile" />
            <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
            <node concept="37vLTw" id="5lhzOSqALaW" role="37wK5m">
              <ref role="3cqZAo" node="5lhzOSqALak" resolve="edited" />
            </node>
            <node concept="37vLTw" id="5lhzOSqALaX" role="37wK5m">
              <ref role="3cqZAo" node="5lhzOSqAL9W" resolve="pathEditedPreEscaped" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5lhzOSqALaY" role="3cqZAp">
          <node concept="2YIFZM" id="5lhzOSqALaZ" role="3clFbG">
            <ref role="37wK5l" to="7zqe:33HeFyz4Cut" resolve="dumpFile" />
            <ref role="1Pybhc" to="7zqe:5KGsxZrsCXs" resolve="MyHelper" />
            <node concept="37vLTw" id="5lhzOSqALb0" role="37wK5m">
              <ref role="3cqZAo" node="5lhzOSqALaC" resolve="escaped" />
            </node>
            <node concept="37vLTw" id="5lhzOSqALb1" role="37wK5m">
              <ref role="3cqZAo" node="5lhzOSqAL9K" resolve="pathEdited" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALb2" role="3cqZAp" />
        <node concept="3clFbH" id="5lhzOSqALb3" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqALb4" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALb5" role="3cpWs9">
            <property role="TrG5h" value="ambition" />
            <node concept="17QB3L" id="5lhzOSqALb6" role="1tU5fm" />
            <node concept="Xl_RD" id="5lhzOSqALb7" role="33vP2m">
              <property role="Xl_RC" value="true" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5lhzOSqALb8" role="3cqZAp">
          <node concept="2YIFZM" id="5lhzOSqALb9" role="3clFbG">
            <ref role="37wK5l" to="9w4s:~WaitForProgressToShow.runOrInvokeAndWaitAboveProgress(java.lang.Runnable):void" resolve="runOrInvokeAndWaitAboveProgress" />
            <ref role="1Pybhc" to="9w4s:~WaitForProgressToShow" resolve="WaitForProgressToShow" />
            <node concept="1bVj0M" id="5lhzOSqALba" role="37wK5m">
              <node concept="3clFbS" id="5lhzOSqALbb" role="1bW5cS">
                <node concept="3clFbF" id="5lhzOSqALbc" role="3cqZAp">
                  <node concept="2OqwBi" id="5lhzOSqALbd" role="3clFbG">
                    <node concept="2OqwBi" id="5lhzOSqALbe" role="2Oq$k0">
                      <node concept="37vLTw" id="5lhzOSqALbf" role="2Oq$k0">
                        <ref role="3cqZAo" node="5lhzOSqALdt" resolve="repo" />
                      </node>
                      <node concept="liA8E" id="5lhzOSqALbg" role="2OqNvi">
                        <ref role="37wK5l" to="lui2:~SRepository.getModelAccess():org.jetbrains.mps.openapi.module.ModelAccess" resolve="getModelAccess" />
                      </node>
                    </node>
                    <node concept="liA8E" id="5lhzOSqALbh" role="2OqNvi">
                      <ref role="37wK5l" to="lui2:~ModelAccess.runReadAction(java.lang.Runnable):void" resolve="runReadAction" />
                      <node concept="1bVj0M" id="5lhzOSqALbi" role="37wK5m">
                        <node concept="3clFbS" id="5lhzOSqALbj" role="1bW5cS">
                          <node concept="3clFbJ" id="5lhzOSqALbk" role="3cqZAp">
                            <node concept="3clFbS" id="5lhzOSqALbl" role="3clFbx">
                              <node concept="3clFbF" id="5lhzOSqALbm" role="3cqZAp">
                                <node concept="37vLTI" id="5lhzOSqALbn" role="3clFbG">
                                  <node concept="37vLTw" id="5lhzOSqALbo" role="37vLTJ">
                                    <ref role="3cqZAo" node="5lhzOSqALb5" resolve="ambition" />
                                  </node>
                                  <node concept="3cpWs3" id="5lhzOSqBn51" role="37vLTx">
                                    <node concept="2OqwBi" id="5lhzOSqBnpK" role="3uHU7w">
                                      <node concept="37vLTw" id="5lhzOSqBnc1" role="2Oq$k0">
                                        <ref role="3cqZAo" node="5lhzOSqALdp" resolve="node" />
                                      </node>
                                      <node concept="3TrcHB" id="5lhzOSqBnHR" role="2OqNvi">
                                        <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
                                      </node>
                                    </node>
                                    <node concept="Xl_RD" id="5lhzOSqBlIA" role="3uHU7B">
                                      <property role="Xl_RC" value="!" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3y3z36" id="5lhzOSqALbs" role="3clFbw">
                              <node concept="10Nm6u" id="5lhzOSqALbt" role="3uHU7w" />
                              <node concept="2OqwBi" id="5lhzOSqALbu" role="3uHU7B">
                                <node concept="37vLTw" id="5lhzOSqALbv" role="2Oq$k0">
                                  <ref role="3cqZAo" node="5lhzOSqALdp" resolve="node" />
                                </node>
                                <node concept="3TrcHB" id="5lhzOSqALbw" role="2OqNvi">
                                  <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALbx" role="3cqZAp" />
        <node concept="34ab3g" id="5lhzOSqALby" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="5lhzOSqALbz" role="34bqiv">
            <node concept="Xl_RD" id="5lhzOSqALb$" role="3uHU7B">
              <property role="Xl_RC" value="Commit with ambition " />
            </node>
            <node concept="37vLTw" id="5lhzOSqALb_" role="3uHU7w">
              <ref role="3cqZAo" node="5lhzOSqALb5" resolve="ambition" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALbA" role="3cqZAp" />
        <node concept="3clFbF" id="5lhzOSqALbB" role="3cqZAp">
          <node concept="2OqwBi" id="5lhzOSqALbC" role="3clFbG">
            <node concept="37vLTw" id="5lhzOSqALbD" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALdr" resolve="adapter" />
            </node>
            <node concept="liA8E" id="5lhzOSqALbE" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="3cpWs3" id="5lhzOSqALbF" role="37wK5m">
                <node concept="Xl_RD" id="5lhzOSqALbG" role="3uHU7B">
                  <property role="Xl_RC" value="Commit with ambition " />
                </node>
                <node concept="37vLTw" id="5lhzOSqALbH" role="3uHU7w">
                  <ref role="3cqZAo" node="5lhzOSqALb5" resolve="ambition" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="34h7GzeADW5" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqALbO" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALbP" role="3cpWs9">
            <property role="TrG5h" value="parsedFile1" />
            <node concept="3uibUv" id="5lhzOSqALbQ" role="1tU5fm">
              <ref role="3uigEE" to="kjvu:~Either" resolve="Either" />
              <node concept="3uibUv" id="5lhzOSqALbR" role="11_B2D">
                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
              </node>
              <node concept="3uibUv" id="5lhzOSqALbS" role="11_B2D">
                <ref role="3uigEE" to="d64m:~Seq" resolve="Seq" />
                <node concept="3uibUv" id="5lhzOSqALbT" role="11_B2D">
                  <ref role="3uigEE" to="w4xx:~Choiced" resolve="Choiced" />
                  <node concept="3uibUv" id="5lhzOSqALbU" role="11_B2D">
                    <ref role="3uigEE" to="w4xx:~Textline" resolve="Textline" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="34h7GzevQ4U" role="33vP2m">
              <ref role="37wK5l" to="9683:~Parser.parseFile(java.lang.String):scala.util.Either" resolve="parseFile" />
              <ref role="1Pybhc" to="9683:~Parser" resolve="Parser" />
              <node concept="37vLTw" id="34h7GzeAEQM" role="37wK5m">
                <ref role="3cqZAo" node="5lhzOSqAL9$" resolve="pathOriginal" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="34h7GzeC_ap" role="3cqZAp">
          <node concept="3cpWsn" id="34h7GzeC_av" role="3cpWs9">
            <property role="TrG5h" value="parsedEdited11" />
            <node concept="3uibUv" id="34h7GzeC_ax" role="1tU5fm">
              <ref role="3uigEE" to="kjvu:~Either" resolve="Either" />
              <node concept="3uibUv" id="34h7GzeCAkC" role="11_B2D">
                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
              </node>
              <node concept="3uibUv" id="34h7GzeCApW" role="11_B2D">
                <ref role="3uigEE" to="d64m:~Seq" resolve="Seq" />
                <node concept="3uibUv" id="34h7GzeCAQ2" role="11_B2D">
                  <ref role="3uigEE" to="w4xx:~Choiced" resolve="Choiced" />
                  <node concept="3uibUv" id="34h7GzeCBek" role="11_B2D">
                    <ref role="3uigEE" to="w4xx:~Textline" resolve="Textline" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="34h7GzeCC2l" role="33vP2m">
              <ref role="37wK5l" to="9683:~Parser.parseFile(java.lang.String):scala.util.Either" resolve="parseFile" />
              <ref role="1Pybhc" to="9683:~Parser" resolve="Parser" />
              <node concept="37vLTw" id="34h7GzeCCc_" role="37wK5m">
                <ref role="3cqZAo" node="5lhzOSqAL9K" resolve="pathEdited" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALc6" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqALc7" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALc8" role="3cpWs9">
            <property role="TrG5h" value="exprTrue" />
            <node concept="3uibUv" id="5lhzOSqALc9" role="1tU5fm">
              <ref role="3uigEE" to="m798:~Expression" resolve="Expression" />
            </node>
            <node concept="2OqwBi" id="5lhzOSqALca" role="33vP2m">
              <node concept="2YIFZM" id="5lhzOSqALcb" role="2Oq$k0">
                <ref role="1Pybhc" to="m798:~ExpressionParser" resolve="ExpressionParser" />
                <ref role="37wK5l" to="m798:~ExpressionParser.parse(java.lang.String):scala.Option" resolve="parse" />
                <node concept="Xl_RD" id="5lhzOSqALcc" role="37wK5m">
                  <property role="Xl_RC" value="true" />
                </node>
              </node>
              <node concept="liA8E" id="5lhzOSqALcd" role="2OqNvi">
                <ref role="37wK5l" to="1miu:~Option.get():java.lang.Object" resolve="get" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALce" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqALcf" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALcg" role="3cpWs9">
            <property role="TrG5h" value="exprAmbition" />
            <node concept="3uibUv" id="5lhzOSqALch" role="1tU5fm">
              <ref role="3uigEE" to="m798:~Expression" resolve="Expression" />
            </node>
            <node concept="2OqwBi" id="5lhzOSqALci" role="33vP2m">
              <node concept="2YIFZM" id="5lhzOSqALcj" role="2Oq$k0">
                <ref role="1Pybhc" to="m798:~ExpressionParser" resolve="ExpressionParser" />
                <ref role="37wK5l" to="m798:~ExpressionParser.parse(java.lang.String):scala.Option" resolve="parse" />
                <node concept="37vLTw" id="5lhzOSqALck" role="37wK5m">
                  <ref role="3cqZAo" node="5lhzOSqALb5" resolve="ambition" />
                </node>
              </node>
              <node concept="liA8E" id="5lhzOSqALcl" role="2OqNvi">
                <ref role="37wK5l" to="1miu:~Option.get():java.lang.Object" resolve="get" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALcm" role="3cqZAp" />
        <node concept="34ab3g" id="5lhzOSqALcn" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="5lhzOSqALco" role="34bqiv">
            <node concept="37vLTw" id="5lhzOSqALcp" role="3uHU7w">
              <ref role="3cqZAo" node="5lhzOSqALc8" resolve="exprTrue" />
            </node>
            <node concept="Xl_RD" id="5lhzOSqALcq" role="3uHU7B">
              <property role="Xl_RC" value="exprTrue " />
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="5lhzOSqALcr" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="5lhzOSqALcs" role="34bqiv">
            <node concept="37vLTw" id="5lhzOSqALct" role="3uHU7w">
              <ref role="3cqZAo" node="5lhzOSqALcg" resolve="exprAmbition" />
            </node>
            <node concept="Xl_RD" id="5lhzOSqALcu" role="3uHU7B">
              <property role="Xl_RC" value="exprAmbition " />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALcv" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqALcw" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALcx" role="3cpWs9">
            <property role="TrG5h" value="checkedIn" />
            <node concept="3uibUv" id="5lhzOSqALcy" role="1tU5fm">
              <ref role="3uigEE" to="d64m:~Seq" resolve="Seq" />
              <node concept="3uibUv" id="5lhzOSqALcz" role="11_B2D">
                <ref role="3uigEE" to="w4xx:~Choiced" resolve="Choiced" />
                <node concept="3uibUv" id="5lhzOSqALc$" role="11_B2D">
                  <ref role="3uigEE" to="w4xx:~Textline" resolve="Textline" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="5lhzOSqALc_" role="33vP2m">
              <ref role="37wK5l" to="7dys:~Checkin.checkinWithNesting(dk.itu.vts.expression.Expression,dk.itu.vts.expression.Expression,scala.collection.Seq,scala.collection.Seq):scala.collection.Seq" resolve="checkinWithNesting" />
              <ref role="1Pybhc" to="7dys:~Checkin" resolve="Checkin" />
              <node concept="37vLTw" id="5lhzOSqALcA" role="37wK5m">
                <ref role="3cqZAo" node="5lhzOSqALc8" resolve="exprTrue" />
              </node>
              <node concept="37vLTw" id="5lhzOSqALcB" role="37wK5m">
                <ref role="3cqZAo" node="5lhzOSqALcg" resolve="exprAmbition" />
              </node>
              <node concept="2OqwBi" id="5lhzOSqALcC" role="37wK5m">
                <node concept="2OqwBi" id="5lhzOSqALcD" role="2Oq$k0">
                  <node concept="37vLTw" id="5lhzOSqALcE" role="2Oq$k0">
                    <ref role="3cqZAo" node="5lhzOSqALbP" resolve="parsedFile1" />
                  </node>
                  <node concept="liA8E" id="5lhzOSqALcF" role="2OqNvi">
                    <ref role="37wK5l" to="kjvu:~Either.right():scala.util.Either$RightProjection" resolve="right" />
                  </node>
                </node>
                <node concept="liA8E" id="5lhzOSqALcG" role="2OqNvi">
                  <ref role="37wK5l" to="kjvu:~Either$RightProjection.get():java.lang.Object" resolve="get" />
                </node>
              </node>
              <node concept="2OqwBi" id="5lhzOSqALcH" role="37wK5m">
                <node concept="2OqwBi" id="5lhzOSqALcI" role="2Oq$k0">
                  <node concept="37vLTw" id="34h7GzeCD2D" role="2Oq$k0">
                    <ref role="3cqZAo" node="34h7GzeC_av" resolve="parsedEdited11" />
                  </node>
                  <node concept="liA8E" id="5lhzOSqALcK" role="2OqNvi">
                    <ref role="37wK5l" to="kjvu:~Either.right():scala.util.Either$RightProjection" resolve="right" />
                  </node>
                </node>
                <node concept="liA8E" id="5lhzOSqALcL" role="2OqNvi">
                  <ref role="37wK5l" to="kjvu:~Either$RightProjection.get():java.lang.Object" resolve="get" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALcM" role="3cqZAp" />
        <node concept="34ab3g" id="5lhzOSqALcN" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="5lhzOSqALcO" role="34bqiv">
            <property role="Xl_RC" value="checkin done" />
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALcP" role="3cqZAp" />
        <node concept="3clFbF" id="5lhzOSqALcQ" role="3cqZAp">
          <node concept="2OqwBi" id="5lhzOSqALcR" role="3clFbG">
            <node concept="37vLTw" id="5lhzOSqALcS" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALdr" resolve="adapter" />
            </node>
            <node concept="liA8E" id="5lhzOSqALcT" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.step(java.lang.String):void" resolve="step" />
              <node concept="Xl_RD" id="5lhzOSqALcU" role="37wK5m">
                <property role="Xl_RC" value="Prettyprint result." />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5lhzOSqALcV" role="3cqZAp">
          <node concept="2OqwBi" id="5lhzOSqALcW" role="3clFbG">
            <node concept="37vLTw" id="5lhzOSqALcX" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALdr" resolve="adapter" />
            </node>
            <node concept="liA8E" id="5lhzOSqALcY" role="2OqNvi">
              <ref role="37wK5l" to="yyf4:~ProgressMonitor.advance(int):void" resolve="advance" />
              <node concept="3cmrfG" id="5lhzOSqALcZ" role="37wK5m">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqALd0" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqALd1" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqALd2" role="3cpWs9">
            <property role="TrG5h" value="scalaLines" />
            <node concept="3uibUv" id="5lhzOSqALd3" role="1tU5fm">
              <ref role="3uigEE" to="47h6:~List" resolve="List" />
              <node concept="17QB3L" id="5lhzOSqALd4" role="11_B2D" />
            </node>
            <node concept="2YIFZM" id="5lhzOSqALd5" role="33vP2m">
              <ref role="1Pybhc" to="ctra:~PrettyPrinter" resolve="PrettyPrinter" />
              <ref role="37wK5l" to="ctra:~PrettyPrinter.prettyPrint(scala.collection.Seq):scala.collection.immutable.List" resolve="prettyPrint" />
              <node concept="37vLTw" id="5lhzOSqALd6" role="37wK5m">
                <ref role="3cqZAo" node="5lhzOSqALcx" resolve="checkedIn" />
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="5lhzOSqALd7" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="5lhzOSqALd8" role="34bqiv">
            <property role="Xl_RC" value="prettyprint done" />
          </node>
        </node>
        <node concept="3cpWs6" id="5lhzOSqALdl" role="3cqZAp">
          <node concept="2OqwBi" id="34h7GzenCMh" role="3cqZAk">
            <node concept="37vLTw" id="34h7GzecsL6" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqALd2" resolve="scalaLines" />
            </node>
            <node concept="liA8E" id="34h7GzeoXEn" role="2OqNvi">
              <ref role="37wK5l" to="d64m:~AbstractTraversable.mkString(java.lang.String):java.lang.String" resolve="mkString" />
              <node concept="Xl_RD" id="34h7Gzepdr3" role="37wK5m">
                <property role="Xl_RC" value="\n" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5lhzOSqALdn" role="1B3o_S" />
      <node concept="17QB3L" id="5lhzOSqALdo" role="3clF45" />
      <node concept="37vLTG" id="5lhzOSqALdp" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="5lhzOSqALdq" role="1tU5fm">
          <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
        </node>
      </node>
      <node concept="37vLTG" id="5lhzOSqALdr" role="3clF46">
        <property role="TrG5h" value="adapter" />
        <node concept="3uibUv" id="5lhzOSqALds" role="1tU5fm">
          <ref role="3uigEE" to="yyf4:~ProgressMonitor" resolve="ProgressMonitor" />
        </node>
      </node>
      <node concept="37vLTG" id="5lhzOSqALdt" role="3clF46">
        <property role="TrG5h" value="repo" />
        <node concept="3uibUv" id="5lhzOSqALdu" role="1tU5fm">
          <ref role="3uigEE" to="lui2:~SRepository" resolve="SRepository" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="5lhzOSqALdv" role="1B3o_S" />
  </node>
  <node concept="2S6QgY" id="5lhzOSqBR_M">
    <property role="TrG5h" value="MakeOptionalCommit" />
    <ref role="2ZfgGC" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="2S6ZIM" id="5lhzOSqBR_N" role="2ZfVej">
      <node concept="3clFbS" id="5lhzOSqBR_O" role="2VODD2">
        <node concept="3clFbF" id="5lhzOSqBSNk" role="3cqZAp">
          <node concept="Xl_RD" id="5lhzOSqBSNj" role="3clFbG">
            <property role="Xl_RC" value="Commit make optional operation" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="5lhzOSqBR_P" role="2ZfgGD">
      <node concept="3clFbS" id="5lhzOSqBR_Q" role="2VODD2">
        <node concept="3cpWs8" id="5lhzOSqBRIT" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqBRIU" role="3cpWs9">
            <property role="TrG5h" value="vts" />
            <node concept="3uibUv" id="5lhzOSqBS82" role="1tU5fm">
              <ref role="3uigEE" node="5lhzOSqAL9w" resolve="VTSWrapperOptional" />
            </node>
            <node concept="2ShNRf" id="5lhzOSqBRIW" role="33vP2m">
              <node concept="HV5vD" id="5lhzOSqBScS" role="2ShVmc">
                <ref role="HV5vE" node="5lhzOSqAL9w" resolve="VTSWrapperOptional" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqBRIY" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqBRIZ" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqBRJ0" role="3cpWs9">
            <property role="TrG5h" value="prettyprinted" />
            <node concept="17QB3L" id="5lhzOSqBRJ1" role="1tU5fm" />
            <node concept="2OqwBi" id="5lhzOSqBRJ2" role="33vP2m">
              <node concept="37vLTw" id="5lhzOSqBRJ3" role="2Oq$k0">
                <ref role="3cqZAo" node="5lhzOSqBRIU" resolve="vts" />
              </node>
              <node concept="liA8E" id="5lhzOSqBRJ4" role="2OqNvi">
                <ref role="37wK5l" node="5lhzOSqAL9x" resolve="commit" />
                <node concept="2Sf5sV" id="5lhzOSqBRJ5" role="37wK5m" />
                <node concept="2ShNRf" id="5lhzOSqBRJ6" role="37wK5m">
                  <node concept="1pGfFk" id="5lhzOSqBRJ7" role="2ShVmc">
                    <ref role="37wK5l" to="mk8z:~EmptyProgressMonitor.&lt;init&gt;()" resolve="EmptyProgressMonitor" />
                  </node>
                </node>
                <node concept="2OqwBi" id="5lhzOSqBRJ8" role="37wK5m">
                  <node concept="1XNTG" id="5lhzOSqBRJ9" role="2Oq$k0" />
                  <node concept="liA8E" id="5lhzOSqBRJa" role="2OqNvi">
                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqBRJb" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqBRJc" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqBRJd" role="3cpWs9">
            <property role="TrG5h" value="filePrettyprinted" />
            <node concept="3uibUv" id="5lhzOSqBRJe" role="1tU5fm">
              <ref role="3uigEE" to="guwi:~File" resolve="File" />
            </node>
            <node concept="2ShNRf" id="5lhzOSqBRJf" role="33vP2m">
              <node concept="1pGfFk" id="5lhzOSqBRJg" role="2ShVmc">
                <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                <node concept="2OqwBi" id="5lhzOSqBRJh" role="37wK5m">
                  <node concept="2YIFZM" id="5lhzOSqBRJi" role="2Oq$k0">
                    <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                    <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                  </node>
                  <node concept="liA8E" id="5lhzOSqBRJj" role="2OqNvi">
                    <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                    <node concept="Xl_RD" id="5lhzOSqBRJk" role="37wK5m">
                      <property role="Xl_RC" value="${TempFolder}" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="5lhzOSqBRJl" role="37wK5m">
                  <property role="Xl_RC" value="vts.txt" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="5lhzOSqBRJm" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="5lhzOSqBRJn" role="34bqiv">
            <node concept="2OqwBi" id="5lhzOSqBRJo" role="3uHU7w">
              <node concept="37vLTw" id="5lhzOSqBRJp" role="2Oq$k0">
                <ref role="3cqZAo" node="5lhzOSqBRJd" resolve="filePrettyprinted" />
              </node>
              <node concept="liA8E" id="5lhzOSqBRJq" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
            <node concept="Xl_RD" id="5lhzOSqBRJr" role="3uHU7B">
              <property role="Xl_RC" value="dump prettyprinted to " />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqBRJs" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqBRJt" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqBRJu" role="3cpWs9">
            <property role="TrG5h" value="writer" />
            <node concept="3uibUv" id="5lhzOSqBRJv" role="1tU5fm">
              <ref role="3uigEE" to="guwi:~PrintWriter" resolve="PrintWriter" />
            </node>
            <node concept="10Nm6u" id="5lhzOSqBRJw" role="33vP2m" />
          </node>
        </node>
        <node concept="2GUZhq" id="5lhzOSqBRJx" role="3cqZAp">
          <node concept="3clFbS" id="5lhzOSqBRJy" role="2GV8ay">
            <node concept="3clFbF" id="5lhzOSqBRJz" role="3cqZAp">
              <node concept="37vLTI" id="5lhzOSqBRJ$" role="3clFbG">
                <node concept="2ShNRf" id="5lhzOSqBRJ_" role="37vLTx">
                  <node concept="1pGfFk" id="5lhzOSqBRJA" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~PrintWriter.&lt;init&gt;(java.io.File,java.lang.String)" resolve="PrintWriter" />
                    <node concept="37vLTw" id="5lhzOSqBRJB" role="37wK5m">
                      <ref role="3cqZAo" node="5lhzOSqBRJd" resolve="filePrettyprinted" />
                    </node>
                    <node concept="Xl_RD" id="5lhzOSqBRJC" role="37wK5m">
                      <property role="Xl_RC" value="UTF-8" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="5lhzOSqBRJD" role="37vLTJ">
                  <ref role="3cqZAo" node="5lhzOSqBRJu" resolve="writer" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5lhzOSqBRJE" role="3cqZAp">
              <node concept="2OqwBi" id="5lhzOSqBRJF" role="3clFbG">
                <node concept="37vLTw" id="5lhzOSqBRJG" role="2Oq$k0">
                  <ref role="3cqZAo" node="5lhzOSqBRJu" resolve="writer" />
                </node>
                <node concept="liA8E" id="5lhzOSqBRJH" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintWriter.write(java.lang.String):void" resolve="write" />
                  <node concept="37vLTw" id="5lhzOSqBRJI" role="37wK5m">
                    <ref role="3cqZAo" node="5lhzOSqBRJ0" resolve="prettyprinted" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5lhzOSqBRJJ" role="2GVbov">
            <node concept="3clFbJ" id="5lhzOSqBRJK" role="3cqZAp">
              <node concept="3y3z36" id="5lhzOSqBRJL" role="3clFbw">
                <node concept="10Nm6u" id="5lhzOSqBRJM" role="3uHU7w" />
                <node concept="37vLTw" id="5lhzOSqBRJN" role="3uHU7B">
                  <ref role="3cqZAo" node="5lhzOSqBRJu" resolve="writer" />
                </node>
              </node>
              <node concept="3clFbS" id="5lhzOSqBRJO" role="3clFbx">
                <node concept="3clFbF" id="5lhzOSqBRJP" role="3cqZAp">
                  <node concept="2OqwBi" id="5lhzOSqBRJQ" role="3clFbG">
                    <node concept="37vLTw" id="5lhzOSqBRJR" role="2Oq$k0">
                      <ref role="3cqZAo" node="5lhzOSqBRJu" resolve="writer" />
                    </node>
                    <node concept="liA8E" id="5lhzOSqBRJS" role="2OqNvi">
                      <ref role="37wK5l" to="guwi:~PrintWriter.close():void" resolve="close" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="5lhzOSqBRJT" role="TEXxN">
            <node concept="3cpWsn" id="5lhzOSqBRJU" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="5lhzOSqBRJV" role="1tU5fm">
                <ref role="3uigEE" to="guwi:~UnsupportedEncodingException" resolve="UnsupportedEncodingException" />
              </node>
            </node>
            <node concept="3clFbS" id="5lhzOSqBRJW" role="TDEfX">
              <node concept="34ab3g" id="5lhzOSqBRJX" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="5lhzOSqBRJY" role="34bqiv" />
                <node concept="37vLTw" id="5lhzOSqBRJZ" role="34bMjA">
                  <ref role="3cqZAo" node="5lhzOSqBRJU" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="5lhzOSqBRK0" role="TEXxN">
            <node concept="3cpWsn" id="5lhzOSqBRK1" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="5lhzOSqBRK2" role="1tU5fm">
                <ref role="3uigEE" to="guwi:~FileNotFoundException" resolve="FileNotFoundException" />
              </node>
            </node>
            <node concept="3clFbS" id="5lhzOSqBRK3" role="TDEfX">
              <node concept="34ab3g" id="5lhzOSqBRK4" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="5lhzOSqBRK5" role="34bqiv" />
                <node concept="37vLTw" id="5lhzOSqBRK6" role="34bMjA">
                  <ref role="3cqZAo" node="5lhzOSqBRK1" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="5lhzOSqBRK7" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="5lhzOSqBRK8" role="34bqiv">
            <property role="Xl_RC" value="commit done" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="3$XByLKkzNU">
    <property role="TrG5h" value="MakeOptional" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="2S6ZIM" id="3$XByLKkzNV" role="2ZfVej">
      <node concept="3clFbS" id="3$XByLKkzNW" role="2VODD2">
        <node concept="3clFbF" id="3$XByLKk_PT" role="3cqZAp">
          <node concept="Xl_RD" id="3$XByLKk_PS" role="3clFbG">
            <property role="Xl_RC" value="Make Optional" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="3$XByLKkzNX" role="2ZfgGD">
      <node concept="3clFbS" id="3$XByLKkzNY" role="2VODD2">
        <node concept="34ab3g" id="3$XByLKkCwQ" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="3$XByLKkCwS" role="34bqiv">
            <property role="Xl_RC" value="Make optional" />
          </node>
        </node>
        <node concept="34ab3g" id="3$XByLKKsCF" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="2OqwBi" id="3$XByLKKsJZ" role="34bqiv">
            <node concept="2Sf5sV" id="3$XByLKKsDt" role="2Oq$k0" />
            <node concept="3TrcHB" id="3$XByLKKsTh" role="2OqNvi">
              <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5lhzOSq_sud" role="3cqZAp">
          <node concept="3clFbS" id="5lhzOSq_suf" role="3clFbx">
            <node concept="3clFbF" id="5lhzOSq_tWr" role="3cqZAp">
              <node concept="37vLTI" id="5lhzOSq_uP3" role="3clFbG">
                <node concept="3clFbT" id="5lhzOSq_uV5" role="37vLTx">
                  <property role="3clFbU" value="true" />
                </node>
                <node concept="2OqwBi" id="5lhzOSq_u49" role="37vLTJ">
                  <node concept="2Sf5sV" id="5lhzOSq_tWp" role="2Oq$k0" />
                  <node concept="3TrcHB" id="5lhzOSq_uds" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5lhzOSq_vg7" role="3cqZAp">
              <node concept="37vLTI" id="5lhzOSq_wji" role="3clFbG">
                <node concept="Xl_RD" id="5lhzOSq_wpX" role="37vLTx">
                  <property role="Xl_RC" value="OptionalFeature" />
                </node>
                <node concept="2OqwBi" id="5lhzOSq_vo6" role="37vLTJ">
                  <node concept="2Sf5sV" id="5lhzOSq_vg5" role="2Oq$k0" />
                  <node concept="3TrcHB" id="5lhzOSq_vG7" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5lhzOSq_tk1" role="3clFbw">
            <node concept="2OqwBi" id="5lhzOSq_sEY" role="2Oq$k0">
              <node concept="2Sf5sV" id="5lhzOSq_sxv" role="2Oq$k0" />
              <node concept="2yIwOk" id="5lhzOSq_sWS" role="2OqNvi" />
            </node>
            <node concept="3O6GUB" id="5lhzOSq_tMP" role="2OqNvi">
              <node concept="chp4Y" id="5lhzOSq_tRg" role="3QVz_e">
                <ref role="cht4Q" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="2ivPrbDx6eg">
    <property role="TrG5h" value="Scroll" />
    <ref role="2ZfgGC" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="2S6ZIM" id="2ivPrbDx6eh" role="2ZfVej">
      <node concept="3clFbS" id="2ivPrbDx6ei" role="2VODD2">
        <node concept="3clFbF" id="2ivPrbDxwSy" role="3cqZAp">
          <node concept="Xl_RD" id="2ivPrbDxwSx" role="3clFbG">
            <property role="Xl_RC" value="test scrolling to node" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="2ivPrbDx6ej" role="2ZfgGD">
      <node concept="3clFbS" id="2ivPrbDx6ek" role="2VODD2">
        <node concept="3cpWs8" id="2ivPrbDyLA5" role="3cqZAp">
          <node concept="3cpWsn" id="2ivPrbDyLA8" role="3cpWs9">
            <property role="TrG5h" value="root" />
            <node concept="3Tqbb2" id="2ivPrbDyLA3" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:3VMRtc4W287" resolve="CPP" />
            </node>
            <node concept="10QFUN" id="2ivPrbDyMbW" role="33vP2m">
              <node concept="2OqwBi" id="2ivPrbDyLMe" role="10QFUP">
                <node concept="2Sf5sV" id="2ivPrbDyLCM" role="2Oq$k0" />
                <node concept="2Rxl7S" id="2ivPrbDyM0M" role="2OqNvi" />
              </node>
              <node concept="3Tqbb2" id="2ivPrbDyMbX" role="10QFUM">
                <ref role="ehGHo" to="4s7a:3VMRtc4W287" resolve="CPP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="2ivPrbDCC3V" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="2ivPrbDCCNa" role="34bqiv">
            <node concept="Xl_RD" id="2ivPrbDCC3X" role="3uHU7B">
              <property role="Xl_RC" value="TextContent count " />
            </node>
            <node concept="2OqwBi" id="2ivPrbDCEvX" role="3uHU7w">
              <node concept="34oBXx" id="2ivPrbDCFOZ" role="2OqNvi" />
              <node concept="2OqwBi" id="2ivPrbDHLZo" role="2Oq$k0">
                <node concept="2OqwBi" id="2ivPrbDHLZp" role="2Oq$k0">
                  <node concept="2OqwBi" id="2ivPrbDHLZq" role="2Oq$k0">
                    <node concept="37vLTw" id="2ivPrbDHLZr" role="2Oq$k0">
                      <ref role="3cqZAo" node="2ivPrbDyLA8" resolve="root" />
                    </node>
                    <node concept="3Tsc0h" id="2ivPrbDHLZs" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UUgwl" resolve="statements" />
                    </node>
                  </node>
                  <node concept="3goQfb" id="2ivPrbDHLZt" role="2OqNvi">
                    <node concept="1bVj0M" id="2ivPrbDHLZu" role="23t8la">
                      <node concept="3clFbS" id="2ivPrbDHLZv" role="1bW5cS">
                        <node concept="3clFbF" id="2ivPrbDHLZw" role="3cqZAp">
                          <node concept="2OqwBi" id="2ivPrbDHLZx" role="3clFbG">
                            <node concept="37vLTw" id="2ivPrbDHLZy" role="2Oq$k0">
                              <ref role="3cqZAo" node="2ivPrbDHLZA" resolve="it" />
                            </node>
                            <node concept="2Rf3mk" id="2ivPrbDHLZz" role="2OqNvi">
                              <node concept="1xMEDy" id="2ivPrbDHLZ$" role="1xVPHs">
                                <node concept="chp4Y" id="2ivPrbDHLZ_" role="ri$Ld">
                                  <ref role="cht4Q" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="2ivPrbDHLZA" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="2ivPrbDHLZB" role="1tU5fm" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="ANE8D" id="2ivPrbDHLZC" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2ivPrbD_Cgf" role="3cqZAp">
          <node concept="3cpWsn" id="2ivPrbD_Cgi" role="3cpWs9">
            <property role="TrG5h" value="last" />
            <node concept="3Tqbb2" id="2ivPrbD_Cgd" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
            </node>
            <node concept="2OqwBi" id="2ivPrbDHIDL" role="33vP2m">
              <node concept="2OqwBi" id="2ivPrbDAdxc" role="2Oq$k0">
                <node concept="2OqwBi" id="2ivPrbDGZhE" role="2Oq$k0">
                  <node concept="2OqwBi" id="2ivPrbDGXB4" role="2Oq$k0">
                    <node concept="37vLTw" id="2ivPrbD_CoH" role="2Oq$k0">
                      <ref role="3cqZAo" node="2ivPrbDyLA8" resolve="root" />
                    </node>
                    <node concept="3Tsc0h" id="2ivPrbDGXZ3" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UUgwl" resolve="statements" />
                    </node>
                  </node>
                  <node concept="3goQfb" id="2ivPrbDH0pe" role="2OqNvi">
                    <node concept="1bVj0M" id="2ivPrbDH0pg" role="23t8la">
                      <node concept="3clFbS" id="2ivPrbDH0ph" role="1bW5cS">
                        <node concept="3clFbF" id="2ivPrbDH0Db" role="3cqZAp">
                          <node concept="2OqwBi" id="2ivPrbDH0Yk" role="3clFbG">
                            <node concept="37vLTw" id="2ivPrbDH0Da" role="2Oq$k0">
                              <ref role="3cqZAo" node="2ivPrbDH0pi" resolve="it" />
                            </node>
                            <node concept="2Rf3mk" id="2ivPrbDH1oK" role="2OqNvi">
                              <node concept="1xMEDy" id="2ivPrbDH1oM" role="1xVPHs">
                                <node concept="chp4Y" id="2ivPrbDH1JF" role="ri$Ld">
                                  <ref role="cht4Q" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="2ivPrbDH0pi" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="2ivPrbDH0pj" role="1tU5fm" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="ANE8D" id="2ivPrbDHGDh" role="2OqNvi" />
              </node>
              <node concept="34jXtK" id="2ivPrbDHLy4" role="2OqNvi">
                <node concept="3cmrfG" id="2ivPrbDHLGh" role="25WWJ7">
                  <property role="3cmrfH" value="770" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="2ivPrbDGtw2" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="2ivPrbDGufb" role="34bqiv">
            <node concept="2OqwBi" id="2ivPrbDGuym" role="3uHU7w">
              <node concept="37vLTw" id="2ivPrbDGumq" role="2Oq$k0">
                <ref role="3cqZAo" node="2ivPrbD_Cgi" resolve="last" />
              </node>
              <node concept="3TrcHB" id="2ivPrbDGuKZ" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
              </node>
            </node>
            <node concept="Xl_RD" id="2ivPrbDGtw4" role="3uHU7B">
              <property role="Xl_RC" value="last text content = " />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2ivPrbDFwdM" role="3cqZAp">
          <node concept="3clFbS" id="2ivPrbDFwdO" role="3clFbx">
            <node concept="34ab3g" id="2ivPrbDFwVt" role="3cqZAp">
              <property role="35gtTG" value="warn" />
              <node concept="Xl_RD" id="2ivPrbDFwVv" role="34bqiv">
                <property role="Xl_RC" value="found node" />
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="2ivPrbDFwLE" role="3clFbw">
            <node concept="10Nm6u" id="2ivPrbDFwUj" role="3uHU7w" />
            <node concept="2OqwBi" id="2ivPrbDFvA7" role="3uHU7B">
              <node concept="2OqwBi" id="2ivPrbDFvfp" role="2Oq$k0">
                <node concept="1XNTG" id="2ivPrbDFv01" role="2Oq$k0" />
                <node concept="liA8E" id="2ivPrbDFvwq" role="2OqNvi">
                  <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                </node>
              </node>
              <node concept="liA8E" id="2ivPrbDFvLN" role="2OqNvi">
                <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                <node concept="37vLTw" id="2ivPrbDFvN4" role="37wK5m">
                  <ref role="3cqZAo" node="2ivPrbD_Cgi" resolve="last" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="2ivPrbDGsfJ" role="9aQIa">
            <node concept="3clFbS" id="2ivPrbDGsfK" role="9aQI4">
              <node concept="34ab3g" id="2ivPrbDGsm3" role="3cqZAp">
                <property role="35gtTG" value="warn" />
                <node concept="Xl_RD" id="2ivPrbDGsm5" role="34bqiv">
                  <property role="Xl_RC" value="Could not find node cell" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2ivPrbDFvO$" role="3cqZAp" />
        <node concept="34ab3g" id="2ivPrbD_Czy" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="2ivPrbD_G9e" role="34bqiv">
            <node concept="2OqwBi" id="2ivPrbD_GJK" role="3uHU7w">
              <node concept="2OqwBi" id="2ivPrbD_Gl8" role="2Oq$k0">
                <node concept="37vLTw" id="2ivPrbD_Gaw" role="2Oq$k0">
                  <ref role="3cqZAo" node="2ivPrbD_Cgi" resolve="last" />
                </node>
                <node concept="2yIwOk" id="2ivPrbD_GuH" role="2OqNvi" />
              </node>
              <node concept="liA8E" id="2ivPrbD_GZY" role="2OqNvi">
                <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
              </node>
            </node>
            <node concept="Xl_RD" id="2ivPrbD_Cz$" role="3uHU7B">
              <property role="Xl_RC" value="Last " />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2ivPrbDxDQe" role="3cqZAp">
          <node concept="2OqwBi" id="2ivPrbDxE7D" role="3clFbG">
            <node concept="2OqwBi" id="2ivPrbDxDVT" role="2Oq$k0">
              <node concept="1XNTG" id="2ivPrbDxDQc" role="2Oq$k0" />
              <node concept="liA8E" id="2ivPrbDxE1D" role="2OqNvi">
                <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
              </node>
            </node>
            <node concept="liA8E" id="2ivPrbDxEiM" role="2OqNvi">
              <ref role="37wK5l" to="cj4x:~EditorComponent.scrollToNode(org.jetbrains.mps.openapi.model.SNode):void" resolve="scrollToNode" />
              <node concept="37vLTw" id="2ivPrbD_F3w" role="37wK5m">
                <ref role="3cqZAo" node="2ivPrbD_Cgi" resolve="last" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="2ivPrbD$GKU" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbF" id="2ivPrbDziH2" role="8Wnug">
            <node concept="2OqwBi" id="2ivPrbDziSW" role="3clFbG">
              <node concept="1XNTG" id="2ivPrbDziH0" role="2Oq$k0" />
              <node concept="liA8E" id="2ivPrbDzj1V" role="2OqNvi">
                <ref role="37wK5l" to="cj4x:~EditorContext.select(org.jetbrains.mps.openapi.model.SNode):void" resolve="select" />
                <node concept="2OqwBi" id="2ivPrbDzKl2" role="37wK5m">
                  <node concept="2OqwBi" id="2ivPrbDzKl3" role="2Oq$k0">
                    <node concept="37vLTw" id="2ivPrbDzKl4" role="2Oq$k0">
                      <ref role="3cqZAo" node="2ivPrbDyLA8" resolve="root" />
                    </node>
                    <node concept="3Tsc0h" id="2ivPrbDzKl5" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UUgwl" resolve="statements" />
                    </node>
                  </node>
                  <node concept="1uHKPH" id="2ivPrbD$exE" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="1cg5ESLvfHv" role="2ZfVeh">
      <node concept="3clFbS" id="1cg5ESLvfHw" role="2VODD2">
        <node concept="3SKdUt" id="1cg5ESLvxER" role="3cqZAp">
          <node concept="3SKdUq" id="1cg5ESLvxET" role="3SKWNk">
            <property role="3SKdUp" value="disabled" />
          </node>
        </node>
        <node concept="3clFbF" id="1cg5ESLvxoK" role="3cqZAp">
          <node concept="3clFbT" id="1cg5ESLvxoJ" role="3clFbG">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

