<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c67970af-4844-47d5-9471-54e2bda5945e(CPPLanguage.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="3" />
  </languages>
  <imports>
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="3VMRtc4W287">
    <property role="TrG5h" value="CPP" />
    <property role="19KtqR" value="true" />
    <property role="EcuMT" value="4535931673106194951" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2s5q4UUgwl" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="43933878589130773" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyj" id="3LUIgcmtu8f" role="1TKVEi">
      <property role="IQ2ns" value="4357999012347240975" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="future" />
      <ref role="20lvS9" node="3VMRtc4W287" resolve="CPP" />
    </node>
    <node concept="1TJgyj" id="3LUIgcmYqcd" role="1TKVEi">
      <property role="IQ2ns" value="4357999012355875597" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="intention" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="3LUIgcnlyz5" resolve="IntentionContainer" />
    </node>
    <node concept="PrWs8" id="3VMRtc4Wh4D" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="79Do2olI3d1" role="1TKVEl">
      <property role="TrG5h" value="constraints" />
      <property role="IQ2nx" value="8244226309567820609" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="2s5q4UUeXK">
    <property role="TrG5h" value="InclusionDirective" />
    <property role="EcuMT" value="43933878589124464" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="2s5q4UUeXL" role="1TKVEl">
      <property role="TrG5h" value="filename" />
      <property role="IQ2nx" value="43933878589124465" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="2s5q4UVw4_" role="1TKVEl">
      <property role="TrG5h" value="angledBracket" />
      <property role="IQ2nx" value="43933878589456677" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="PrWs8" id="2s5q4UUgwu" role="PzmwI">
      <ref role="PrY4T" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
  </node>
  <node concept="PlHQZ" id="2s5q4UUgwq">
    <property role="TrG5h" value="ICPPElement" />
    <property role="EcuMT" value="43933878589130778" />
    <node concept="1TJgyj" id="6NCueePLN3i" role="1TKVEi">
      <property role="IQ2ns" value="7847655270095728850" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="futureNode" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyi" id="2s5q4UYgRo" role="1TKVEl">
      <property role="TrG5h" value="location" />
      <property role="IQ2nx" value="43933878590180824" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="4P2s9PP_HnT" role="1TKVEl">
      <property role="TrG5h" value="hidden" />
      <property role="IQ2nx" value="5567135910968350201" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="1UsfiVCPgp0" role="1TKVEl">
      <property role="IQ2nx" value="2205705189188765248" />
      <property role="TrG5h" value="chunkID" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="2s5q4UUgwr">
    <property role="TrG5h" value="TextContent" />
    <property role="EcuMT" value="43933878589130779" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2s5q4UUgws" role="PzmwI">
      <ref role="PrY4T" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyi" id="2s5q4UUuYU" role="1TKVEl">
      <property role="TrG5h" value="text" />
      <property role="IQ2nx" value="43933878589190074" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="5a5L3hY8OSL" role="1TKVEl">
      <property role="TrG5h" value="isEdit" />
      <property role="IQ2nx" value="5946374612746194481" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="2NOvNR_vw1V" role="1TKVEl">
      <property role="IQ2nx" value="3239353935806201979" />
      <property role="TrG5h" value="isDeleted" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="2NOvNR_yT9t" role="1TKVEl">
      <property role="IQ2nx" value="3239353935807091293" />
      <property role="TrG5h" value="isNew" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="5a5L3hY9ALO" role="1TKVEl">
      <property role="TrG5h" value="ambition" />
      <property role="IQ2nx" value="5946374612746398836" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="2NOvNR_v8nc" role="1TKVEl">
      <property role="IQ2nx" value="3239353935806105036" />
      <property role="TrG5h" value="oldText" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="38Muwq1QG_A" role="1TKVEl">
      <property role="IQ2nx" value="3617087619098921318" />
      <property role="TrG5h" value="isSelected" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="2s5q4UVEuQ">
    <property role="TrG5h" value="MacroDefine" />
    <property role="EcuMT" value="43933878589499318" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2GhJDbDlGI4" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="args" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="3103471156424264580" />
      <ref role="20lvS9" node="2GhJDbDlgzd" resolve="MacroLine" />
    </node>
    <node concept="1TJgyj" id="2GhJDbDkLt$" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="3103471156424021860" />
      <ref role="20lvS9" node="2GhJDbDlgzd" resolve="MacroLine" />
    </node>
    <node concept="PrWs8" id="2s5q4UVEuR" role="PzmwI">
      <ref role="PrY4T" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyi" id="2s5q4UXAbU" role="1TKVEl">
      <property role="TrG5h" value="name" />
      <property role="IQ2nx" value="43933878590006010" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7Qhq5H9yj7t" role="1TKVEl">
      <property role="IQ2nx" value="9048127867649077725" />
      <property role="TrG5h" value="comment" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="2s5q4UVM$2">
    <property role="TrG5h" value="MacroIf" />
    <property role="EcuMT" value="43933878589532418" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2s5q4UVM$3" role="PzmwI">
      <ref role="PrY4T" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyj" id="2s5q4UWqKy" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="trueBranch" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="43933878589697058" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyj" id="3fq3ZRIT59G" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="elseIfs" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="3736316424166330988" />
      <ref role="20lvS9" node="3fq3ZRIT59K" resolve="MacroElseIf" />
    </node>
    <node concept="1TJgyj" id="2s5q4UWqK$" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="elseBranch" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="43933878589697060" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyi" id="3fq3ZRIT59E" role="1TKVEl">
      <property role="TrG5h" value="condition" />
      <property role="IQ2nx" value="3736316424166330986" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="3fq3ZRIT59K">
    <property role="TrG5h" value="MacroElseIf" />
    <property role="EcuMT" value="3736316424166330992" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="3fq3ZRIT59L" role="1TKVEl">
      <property role="TrG5h" value="condition" />
      <property role="IQ2nx" value="3736316424166330993" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="3fq3ZRIT59N" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="branch" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="3736316424166330995" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="2GhJDbDkDWY">
    <property role="TrG5h" value="MacroIfdef" />
    <property role="EcuMT" value="3103471156423991102" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="2GhJDbDlgzd">
    <property role="TrG5h" value="MacroLine" />
    <property role="EcuMT" value="3103471156424149197" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="2GhJDbDlgze" role="1TKVEl">
      <property role="TrG5h" value="content" />
      <property role="IQ2nx" value="3103471156424149198" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="5zZlI40Va1E">
    <property role="EcuMT" value="6412939918782013546" />
    <property role="TrG5h" value="TextContentComment" />
    <ref role="1TJDcQ" node="2s5q4UUgwr" resolve="TextContent" />
  </node>
  <node concept="1TIwiD" id="3LUIgcmYqc7">
    <property role="EcuMT" value="4357999012355875591" />
    <property role="TrG5h" value="Intention" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="3ie$Dw3G5cG" role="1TKVEl">
      <property role="IQ2nx" value="3787125518259737388" />
      <property role="TrG5h" value="type" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="1SJQf$Wm6pV" role="1TKVEl">
      <property role="IQ2nx" value="2175195710014121595" />
      <property role="TrG5h" value="view" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3jeRE17nVF2" role="1TKVEl">
      <property role="IQ2nx" value="3805223515148237506" />
      <property role="TrG5h" value="featurename" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7cMGUmZ7UHY" role="1TKVEl">
      <property role="IQ2nx" value="8300894587654744958" />
      <property role="TrG5h" value="isImplicit" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyj" id="3LUIgcnJeGE" role="1TKVEi">
      <property role="IQ2ns" value="4357999012368673578" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="3LUIgcnlyz5">
    <property role="EcuMT" value="4357999012361939141" />
    <property role="TrG5h" value="IntentionContainer" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3LUIgcnlyz6" role="1TKVEi">
      <property role="IQ2ns" value="4357999012361939142" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="intentions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="3LUIgcmYqc7" resolve="Intention" />
    </node>
  </node>
  <node concept="1TIwiD" id="43XlAbjeR$w">
    <property role="EcuMT" value="4682994169985857824" />
    <property role="TrG5h" value="ExclusiveSwitcher" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="43XlAbjeR$z" role="1TKVEi">
      <property role="IQ2ns" value="4682994169985857827" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="contentLeft" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="PrWs8" id="43XlAbjeR$x" role="PzmwI">
      <ref role="PrY4T" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyj" id="43XlAbjeSXw" role="1TKVEi">
      <property role="IQ2ns" value="4682994169985863520" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="contentRight" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
    <node concept="1TJgyi" id="43XlAbjXKnI" role="1TKVEl">
      <property role="IQ2nx" value="4682994169998149102" />
      <property role="TrG5h" value="featurename" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="43XlAbjBn0d">
    <property role="EcuMT" value="4682994169992278029" />
    <property role="TrG5h" value="ExclusiveIntention" />
    <ref role="1TJDcQ" node="3LUIgcmYqc7" resolve="Intention" />
    <node concept="1TJgyj" id="43XlAbjBn0n" role="1TKVEi">
      <property role="IQ2ns" value="4682994169992278039" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="left" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="43XlAbjBn0k" resolve="ExclusiveReference" />
    </node>
    <node concept="1TJgyj" id="43XlAbjBn0p" role="1TKVEi">
      <property role="IQ2ns" value="4682994169992278041" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="right" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="43XlAbjBn0k" resolve="ExclusiveReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="43XlAbjBn0k">
    <property role="EcuMT" value="4682994169992278036" />
    <property role="TrG5h" value="ExclusiveReference" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="43XlAbjBn0l" role="1TKVEi">
      <property role="IQ2ns" value="4682994169992278037" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="43XlAbkzFk6">
    <property role="EcuMT" value="4682994170008089862" />
    <property role="TrG5h" value="OrderSelector" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="43XlAbkzFk7" role="1TKVEi">
      <property role="IQ2ns" value="4682994170008089863" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="left" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="43XlAbjBn0k" resolve="ExclusiveReference" />
    </node>
    <node concept="1TJgyj" id="43XlAbkzFk9" role="1TKVEi">
      <property role="IQ2ns" value="4682994170008089865" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="right" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="43XlAbjBn0k" resolve="ExclusiveReference" />
    </node>
    <node concept="PrWs8" id="43XlAbkCLvj" role="PzmwI">
      <ref role="PrY4T" node="2s5q4UUgwq" resolve="ICPPElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="43XlAbkSjHr">
    <property role="EcuMT" value="4682994170013498203" />
    <property role="TrG5h" value="OrderIntention" />
    <ref role="1TJDcQ" node="3LUIgcmYqc7" resolve="Intention" />
    <node concept="1TJgyj" id="43XlAbkSjHv" role="1TKVEi">
      <property role="IQ2ns" value="4682994170013498207" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="left" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="43XlAbjBn0k" resolve="ExclusiveReference" />
    </node>
    <node concept="1TJgyj" id="43XlAbkSjHw" role="1TKVEi">
      <property role="IQ2ns" value="4682994170013498208" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="right" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="43XlAbjBn0k" resolve="ExclusiveReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="3aElbFxeJD9">
    <property role="EcuMT" value="3650823609556597321" />
    <property role="TrG5h" value="PostponeIntention" />
    <ref role="1TJDcQ" node="3LUIgcmYqc7" resolve="Intention" />
    <node concept="1TJgyi" id="3aElbFxeJDc" role="1TKVEl">
      <property role="IQ2nx" value="3650823609556597324" />
      <property role="TrG5h" value="note" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
</model>

