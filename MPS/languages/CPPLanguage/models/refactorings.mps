<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:aa8d0c1d-f2f8-4333-9776-ea7e27c19430(CPPLanguage.refactorings)">
  <persistence version="9" />
  <languages>
    <use id="3ecd7c84-cde3-45de-886c-135ecc69b742" name="jetbrains.mps.lang.refactoring" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports />
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="3ecd7c84-cde3-45de-886c-135ecc69b742" name="jetbrains.mps.lang.refactoring">
      <concept id="2058726427123891188" name="jetbrains.mps.lang.refactoring.structure.DoWhenDoneClause" flags="in" index="YZUGN" />
      <concept id="6895093993902236229" name="jetbrains.mps.lang.refactoring.structure.Refactoring" flags="ig" index="3SMa$L">
        <property id="6895093993902236371" name="userFriendlyName" index="3SMaAB" />
        <child id="2058726427123891193" name="doWhenDoneBlock" index="YZUGY" />
        <child id="6895093993902236381" name="doRefactorBlock" index="3SMaAD" />
        <child id="6895093993902310998" name="target" index="3SM$Oy" />
      </concept>
      <concept id="6895093993902310761" name="jetbrains.mps.lang.refactoring.structure.RefactoringTarget" flags="ng" index="3SM$St">
        <property id="6895093993902310999" name="allowMultiple" index="3SM$Oz" />
      </concept>
      <concept id="6895093993902310762" name="jetbrains.mps.lang.refactoring.structure.ModelTarget" flags="ng" index="3SM$Su" />
      <concept id="1189694053795" name="jetbrains.mps.lang.refactoring.structure.DoRefactorClause" flags="in" index="3ZiDMR" />
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3SMa$L" id="1LOfuoijJ3x">
    <property role="TrG5h" value="test" />
    <property role="3SMaAB" value="test" />
    <node concept="3ZiDMR" id="1LOfuoijJ3z" role="3SMaAD">
      <node concept="3clFbS" id="1LOfuoijJ3$" role="2VODD2">
        <node concept="34ab3g" id="1LOfuoijJ4i" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="1LOfuoijJ4k" role="34bqiv">
            <property role="Xl_RC" value="refactor()" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3SM$Su" id="2NOvNR_aMnc" role="3SM$Oy">
      <property role="3SM$Oz" value="true" />
    </node>
    <node concept="YZUGN" id="2NOvNR_aMBo" role="YZUGY">
      <node concept="3clFbS" id="2NOvNR_aMBp" role="2VODD2">
        <node concept="34ab3g" id="2NOvNR_aMBK" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="Xl_RD" id="2NOvNR_aMBM" role="34bqiv">
            <property role="Xl_RC" value="done" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

