<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7e3a84cb-38df-4db2-adea-884d8987c992(CPPLanguage.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="-1" />
    <use id="daafa647-f1f7-4b0b-b096-69cd7c8408c0" name="jetbrains.mps.baseLanguage.regexp" version="-1" />
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="-1" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="f7eu" ref="6150e321-6a06-49cb-8a7d-9facbb581dc2/java:com.microsoft.z3(CPPLanguage/)" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="4s7a" ref="r:c67970af-4844-47d5-9471-54e2bda5945e(CPPLanguage.structure)" />
    <import index="ao3" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.text(MPS.Core/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="18ew" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.util(MPS.Core/)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="iptd" ref="r:87ad25c8-6567-4dff-99e1-1dedfd4141eb(CPPImporter.plugin)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="tpfp" ref="r:00000000-0000-4000-0000-011c89590519(jetbrains.mps.baseLanguage.regexp.jetbrains.mps.regexp.accessory)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1224071154655" name="jetbrains.mps.baseLanguage.structure.AsExpression" flags="nn" index="0kSF2">
        <child id="1224071154657" name="classifierType" index="0kSFW" />
        <child id="1224071154656" name="expression" index="0kSFX" />
      </concept>
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1215695189714" name="jetbrains.mps.baseLanguage.structure.PlusAssignmentExpression" flags="nn" index="d57v9" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1076505808687" name="jetbrains.mps.baseLanguage.structure.WhileStatement" flags="nn" index="2$JKZl">
        <child id="1076505808688" name="condition" index="2$JKZa" />
      </concept>
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1153952380246" name="jetbrains.mps.baseLanguage.structure.TryStatement" flags="nn" index="2GUZhq">
        <child id="1153952416686" name="body" index="2GV8ay" />
        <child id="1153952429843" name="finallyBody" index="2GVbov" />
        <child id="1164903700860" name="catchClause" index="TEXxN" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1083260308424" name="jetbrains.mps.baseLanguage.structure.EnumConstantReference" flags="nn" index="Rm8GO">
        <reference id="1083260308426" name="enumConstantDeclaration" index="Rm8GQ" />
        <reference id="1144432896254" name="enumClass" index="1Px2BO" />
      </concept>
      <concept id="1164879751025" name="jetbrains.mps.baseLanguage.structure.TryCatchStatement" flags="nn" index="SfApY">
        <child id="1164879758292" name="body" index="SfCbr" />
        <child id="1164903496223" name="catchClause" index="TEbGg" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1164903280175" name="jetbrains.mps.baseLanguage.structure.CatchClause" flags="nn" index="TDmWw">
        <child id="1164903359218" name="catchBody" index="TDEfX" />
        <child id="1164903359217" name="throwable" index="TDEfY" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1164991038168" name="jetbrains.mps.baseLanguage.structure.ThrowStatement" flags="nn" index="YS8fn">
        <child id="1164991057263" name="throwable" index="YScLw" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144226303539" name="jetbrains.mps.baseLanguage.structure.ForeachStatement" flags="nn" index="1DcWWT">
        <child id="1144226360166" name="iterable" index="1DdaDG" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc">
      <concept id="5858074156537516430" name="jetbrains.mps.baseLanguage.javadoc.structure.ReturnBlockDocTag" flags="ng" index="x79VA">
        <property id="5858074156537516431" name="text" index="x79VB" />
      </concept>
      <concept id="5349172909345501395" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseDocComment" flags="ng" index="P$AiS">
        <child id="8465538089690331502" name="body" index="TZ5H$" />
      </concept>
      <concept id="5349172909345532724" name="jetbrains.mps.baseLanguage.javadoc.structure.MethodDocComment" flags="ng" index="P$JXv">
        <child id="5858074156537516440" name="return" index="x79VK" />
      </concept>
      <concept id="8465538089690331500" name="jetbrains.mps.baseLanguage.javadoc.structure.CommentLine" flags="ng" index="TZ5HA">
        <child id="8970989240999019149" name="part" index="1dT_Ay" />
      </concept>
      <concept id="8970989240999019143" name="jetbrains.mps.baseLanguage.javadoc.structure.TextCommentLinePart" flags="ng" index="1dT_AC">
        <property id="8970989240999019144" name="text" index="1dT_AB" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167228628751" name="hasException" index="34fQS0" />
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
        <child id="1167227561449" name="exception" index="34bMjA" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <reference id="6733348108486823428" name="concept" index="1m5ApE" />
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
      </concept>
      <concept id="1143234257716" name="jetbrains.mps.lang.smodel.structure.Node_GetModelOperation" flags="nn" index="I4A8Y" />
      <concept id="1171310072040" name="jetbrains.mps.lang.smodel.structure.Node_GetContainingRootOperation" flags="nn" index="2Rxl7S" />
      <concept id="1171315804604" name="jetbrains.mps.lang.smodel.structure.Model_RootsOperation" flags="nn" index="2RRcyG" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1144195091934" name="jetbrains.mps.lang.smodel.structure.Node_IsRoleOperation" flags="nn" index="1BlSNk">
        <reference id="1144195362400" name="conceptOfParent" index="1BmUXE" />
        <reference id="1144195396777" name="linkInParent" index="1Bn3mz" />
      </concept>
      <concept id="1140133623887" name="jetbrains.mps.lang.smodel.structure.Node_DeleteOperation" flags="nn" index="1PgB_6" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <property id="1238684351431" name="asCast" index="1BlNFB" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
    <language id="daafa647-f1f7-4b0b-b096-69cd7c8408c0" name="jetbrains.mps.baseLanguage.regexp">
      <concept id="1222260556146" name="jetbrains.mps.baseLanguage.regexp.structure.ReplaceWithRegexpOperation" flags="nn" index="2kq01I">
        <child id="1222261033031" name="replaceBlock" index="2krO_r" />
      </concept>
      <concept id="1175169009571" name="jetbrains.mps.baseLanguage.regexp.structure.FindMatchStatement" flags="nn" index="2ty0qM">
        <child id="1175169023932" name="expr" index="2ty3UH" />
        <child id="1175169154112" name="body" index="2tyzPh" />
      </concept>
      <concept id="1174482753837" name="jetbrains.mps.baseLanguage.regexp.structure.StringLiteralRegexp" flags="ng" index="1OC9wW">
        <property id="1174482761807" name="text" index="1OCb_u" />
      </concept>
      <concept id="1174482804200" name="jetbrains.mps.baseLanguage.regexp.structure.PlusRegexp" flags="ng" index="1OClNT" />
      <concept id="1174484562151" name="jetbrains.mps.baseLanguage.regexp.structure.SeqRegexp" flags="ng" index="1OJ37Q" />
      <concept id="1174485167097" name="jetbrains.mps.baseLanguage.regexp.structure.BinaryRegexp" flags="ng" index="1OLmFC">
        <child id="1174485176897" name="left" index="1OLpdg" />
        <child id="1174485181039" name="right" index="1OLqdY" />
      </concept>
      <concept id="1174485235885" name="jetbrains.mps.baseLanguage.regexp.structure.UnaryRegexp" flags="ng" index="1OLBAW">
        <child id="1174485243418" name="regexp" index="1OLDsb" />
      </concept>
      <concept id="1174510540317" name="jetbrains.mps.baseLanguage.regexp.structure.InlineRegexpExpression" flags="nn" index="1Qi9sc">
        <child id="1174510571016" name="regexp" index="1QigWp" />
      </concept>
      <concept id="1174552240608" name="jetbrains.mps.baseLanguage.regexp.structure.QuestionRegexp" flags="ng" index="1SLe3L" />
      <concept id="1174554186090" name="jetbrains.mps.baseLanguage.regexp.structure.SymbolClassRegexp" flags="ng" index="1SSD1V">
        <child id="1174557628217" name="part" index="1T5LoC" />
      </concept>
      <concept id="1174554211468" name="jetbrains.mps.baseLanguage.regexp.structure.PositiveSymbolClassRegexp" flags="ng" index="1SSJmt" />
      <concept id="1174555732504" name="jetbrains.mps.baseLanguage.regexp.structure.PredefinedSymbolClassRegexp" flags="ng" index="1SYyG9">
        <reference id="1174555843709" name="symbolClass" index="1SYXPG" />
      </concept>
      <concept id="1174557878319" name="jetbrains.mps.baseLanguage.regexp.structure.CharacterSymbolClassPart" flags="ng" index="1T6I$Y">
        <property id="1174557887320" name="character" index="1T6KD9" />
      </concept>
      <concept id="1174558301835" name="jetbrains.mps.baseLanguage.regexp.structure.IntervalSymbolClassPart" flags="ng" index="1T8lYq">
        <property id="1174558315290" name="start" index="1T8p8b" />
        <property id="1174558317822" name="end" index="1T8pRJ" />
      </concept>
      <concept id="1174564062919" name="jetbrains.mps.baseLanguage.regexp.structure.MatchParensRegexp" flags="ng" index="1Tukvm">
        <child id="1174564160889" name="regexp" index="1TuGhC" />
      </concept>
      <concept id="1174565027678" name="jetbrains.mps.baseLanguage.regexp.structure.MatchVariableReference" flags="nn" index="1TxZTf">
        <reference id="1174565035929" name="match" index="1Ty1U8" />
      </concept>
      <concept id="1174653354106" name="jetbrains.mps.baseLanguage.regexp.structure.RegexpUsingConstruction" flags="ng" index="1YMW5F">
        <child id="1174653387388" name="regexp" index="1YN4dH" />
      </concept>
      <concept id="1174656254036" name="jetbrains.mps.baseLanguage.regexp.structure.ReplaceBlock" flags="in" index="1YY055" />
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1171391069720" name="jetbrains.mps.baseLanguage.collections.structure.GetIndexOfOperation" flags="nn" index="2WmjW8" />
      <concept id="1162934736510" name="jetbrains.mps.baseLanguage.collections.structure.GetElementOperation" flags="nn" index="34jXtK" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="5633809102336885303" name="jetbrains.mps.baseLanguage.collections.structure.SubListOperation" flags="nn" index="3b24QK">
        <child id="5633809102336885321" name="upToIndex" index="3b24Re" />
        <child id="5633809102336885320" name="fromIndex" index="3b24Rf" />
      </concept>
      <concept id="1240687580870" name="jetbrains.mps.baseLanguage.collections.structure.JoinOperation" flags="nn" index="3uJxvA">
        <child id="1240687658305" name="delimiter" index="3uJOhx" />
      </concept>
      <concept id="1225711141656" name="jetbrains.mps.baseLanguage.collections.structure.ListElementAccessExpression" flags="nn" index="1y4W85">
        <child id="1225711182005" name="list" index="1y566C" />
        <child id="1225711191269" name="index" index="1y58nS" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="2s5q4UYcBk">
    <ref role="13h7C2" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
    <node concept="13i0hz" id="2s5q4UYcBn" role="13h7CS">
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getIndent" />
      <node concept="P$JXv" id="2bFU2aD3CBB" role="lGtFl">
        <node concept="TZ5HA" id="2bFU2aD3CBC" role="TZ5H$">
          <node concept="1dT_AC" id="2bFU2aD3CBD" role="1dT_Ay">
            <property role="1dT_AB" value="Currently disabled. Creates whitespaces based on the orignal source formatting." />
          </node>
        </node>
        <node concept="x79VA" id="2bFU2aD3CBE" role="x79VK">
          <property role="x79VB" value="custom indent using whitespaces" />
        </node>
      </node>
      <node concept="3Tm1VV" id="2s5q4UYcBo" role="1B3o_S" />
      <node concept="3clFbS" id="2s5q4UYcBp" role="3clF47">
        <node concept="3clFbJ" id="2s5q4UYmTD" role="3cqZAp">
          <node concept="3clFbS" id="2s5q4UYmTF" role="3clFbx">
            <node concept="1X3_iC" id="2bFU2aD3Cmv" role="lGtFl">
              <property role="3V$3am" value="statement" />
              <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
              <node concept="3cpWs8" id="5gPWvT9WpzG" role="8Wnug">
                <node concept="3cpWsn" id="5gPWvT9WpzJ" role="3cpWs9">
                  <property role="TrG5h" value="pos" />
                  <node concept="10Oyi0" id="5gPWvT9WpzE" role="1tU5fm" />
                  <node concept="2OqwBi" id="5gPWvT9Wq_I" role="33vP2m">
                    <node concept="2OqwBi" id="5gPWvT9Wq0x" role="2Oq$k0">
                      <node concept="13iPFW" id="5gPWvT9WpSK" role="2Oq$k0" />
                      <node concept="3TrcHB" id="5gPWvT9Wqc$" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:2s5q4UYgRo" resolve="location" />
                      </node>
                    </node>
                    <node concept="liA8E" id="5gPWvT9Wr3B" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.lastIndexOf(java.lang.String):int" resolve="lastIndexOf" />
                      <node concept="Xl_RD" id="5gPWvT9Wr3E" role="37wK5m">
                        <property role="Xl_RC" value=":" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1X3_iC" id="2bFU2aD3Cmw" role="lGtFl">
              <property role="3V$3am" value="statement" />
              <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
              <node concept="3clFbH" id="5gPWvT9WpjC" role="8Wnug" />
            </node>
            <node concept="1X3_iC" id="2bFU2aD3Cmx" role="lGtFl">
              <property role="3V$3am" value="statement" />
              <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
              <node concept="1Dw8fO" id="2s5q4UYo7k" role="8Wnug">
                <node concept="3clFbS" id="2s5q4UYo7m" role="2LFqv$">
                  <node concept="3clFbF" id="2s5q4UYoCK" role="3cqZAp">
                    <node concept="2OqwBi" id="2s5q4UYoEe" role="3clFbG">
                      <node concept="37vLTw" id="2s5q4UYoCI" role="2Oq$k0">
                        <ref role="3cqZAo" node="2s5q4UYnAd" resolve="sb" />
                      </node>
                      <node concept="liA8E" id="2s5q4UYoLO" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
                        <node concept="Xl_RD" id="2s5q4UYoMj" role="37wK5m">
                          <property role="Xl_RC" value=" " />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWsn" id="2s5q4UYo7n" role="1Duv9x">
                  <property role="TrG5h" value="i" />
                  <node concept="10Oyi0" id="2s5q4UYo9_" role="1tU5fm" />
                  <node concept="3cmrfG" id="2s5q4UYobi" role="33vP2m">
                    <property role="3cmrfH" value="0" />
                  </node>
                </node>
                <node concept="3eOVzh" id="2s5q4UYonb" role="1Dwp0S">
                  <node concept="2YIFZM" id="2s5q4UYooN" role="3uHU7w">
                    <ref role="37wK5l" to="wyt6:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
                    <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                    <node concept="2OqwBi" id="5gPWvT9Wspi" role="37wK5m">
                      <node concept="2OqwBi" id="5gPWvT9WrHN" role="2Oq$k0">
                        <node concept="13iPFW" id="5gPWvT9Wrxl" role="2Oq$k0" />
                        <node concept="3TrcHB" id="5gPWvT9WrYk" role="2OqNvi">
                          <ref role="3TsBF5" to="4s7a:2s5q4UYgRo" resolve="location" />
                        </node>
                      </node>
                      <node concept="liA8E" id="5gPWvT9WsWU" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~String.substring(int):java.lang.String" resolve="substring" />
                        <node concept="3cpWs3" id="5gPWvT9WtH_" role="37wK5m">
                          <node concept="3cmrfG" id="5gPWvT9WtHC" role="3uHU7w">
                            <property role="3cmrfH" value="1" />
                          </node>
                          <node concept="37vLTw" id="5gPWvT9Wt69" role="3uHU7B">
                            <ref role="3cqZAo" node="5gPWvT9WpzJ" resolve="pos" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTw" id="2s5q4UYobC" role="3uHU7B">
                    <ref role="3cqZAo" node="2s5q4UYo7n" resolve="i" />
                  </node>
                </node>
                <node concept="3uNrnE" id="2s5q4UYoAV" role="1Dwrff">
                  <node concept="37vLTw" id="2s5q4UYoAX" role="2$L3a6">
                    <ref role="3cqZAo" node="2s5q4UYo7n" resolve="i" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="2s5q4UYnAc" role="3cqZAp">
              <node concept="3cpWsn" id="2s5q4UYnAd" role="3cpWs9">
                <property role="TrG5h" value="sb" />
                <node concept="3uibUv" id="2s5q4UYnAe" role="1tU5fm">
                  <ref role="3uigEE" to="wyt6:~StringBuilder" resolve="StringBuilder" />
                </node>
                <node concept="2ShNRf" id="2s5q4UYnBK" role="33vP2m">
                  <node concept="1pGfFk" id="2s5q4UYnHV" role="2ShVmc">
                    <ref role="37wK5l" to="wyt6:~StringBuilder.&lt;init&gt;()" resolve="StringBuilder" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="2bFU2aD3CwP" role="3cqZAp">
              <node concept="3SKdUq" id="2bFU2aD3CwR" role="3SKWNk">
                <property role="3SKdUp" value="Disabled" />
              </node>
            </node>
            <node concept="3clFbH" id="2s5q4UYo4O" role="3cqZAp" />
            <node concept="3cpWs6" id="2s5q4UYnLd" role="3cqZAp">
              <node concept="2OqwBi" id="2s5q4UYnPu" role="3cqZAk">
                <node concept="37vLTw" id="2s5q4UYnMC" role="2Oq$k0">
                  <ref role="3cqZAo" node="2s5q4UYnAd" resolve="sb" />
                </node>
                <node concept="liA8E" id="2s5q4UYnY5" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="2s5q4UYn1P" role="3clFbw">
            <node concept="10Nm6u" id="2s5q4UYn2i" role="3uHU7w" />
            <node concept="2OqwBi" id="2s5q4UYmWc" role="3uHU7B">
              <node concept="13iPFW" id="2s5q4UYmUY" role="2Oq$k0" />
              <node concept="3TrcHB" id="2s5q4UYmYF" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2s5q4UYgRo" resolve="location" />
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="2s5q4UYn2D" role="9aQIa">
            <node concept="3clFbS" id="2s5q4UYn2E" role="9aQI4">
              <node concept="3cpWs6" id="2s5q4UYn33" role="3cqZAp">
                <node concept="Xl_RD" id="2s5q4UYn39" role="3cqZAk">
                  <property role="Xl_RC" value="" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="2s5q4UYcBy" role="3clF45" />
    </node>
    <node concept="13i0hz" id="1UsfiVCPqYb" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getChunkColor" />
      <node concept="3Tm1VV" id="1UsfiVCPqYc" role="1B3o_S" />
      <node concept="3uibUv" id="1UsfiVCP_Xs" role="3clF45">
        <ref role="3uigEE" to="z60i:~Color" resolve="Color" />
      </node>
      <node concept="3clFbS" id="1UsfiVCPqYe" role="3clF47">
        <node concept="3KaCP$" id="1UsfiVCPAcx" role="3cqZAp">
          <node concept="2OqwBi" id="1UsfiVCPAjI" role="3KbGdf">
            <node concept="13iPFW" id="1UsfiVCPAcH" role="2Oq$k0" />
            <node concept="3TrcHB" id="1UsfiVCPAv1" role="2OqNvi">
              <ref role="3TsBF5" to="4s7a:1UsfiVCPgp0" resolve="chunkID" />
            </node>
          </node>
          <node concept="3KbdKl" id="1UsfiVCPAwZ" role="3KbHQx">
            <node concept="3cmrfG" id="1UsfiVCPAz4" role="3Kbmr1">
              <property role="3cmrfH" value="0" />
            </node>
            <node concept="3clFbS" id="1UsfiVCPAx1" role="3Kbo56">
              <node concept="3cpWs6" id="1UsfiVCPA_5" role="3cqZAp">
                <node concept="10M0yZ" id="1UsfiVCPAD_" role="3cqZAk">
                  <ref role="1PxDUh" to="z60i:~Color" resolve="Color" />
                  <ref role="3cqZAo" to="z60i:~Color.WHITE" resolve="WHITE" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="1UsfiVCPAHU" role="3KbHQx">
            <node concept="3cmrfG" id="1UsfiVCPAKj" role="3Kbmr1">
              <property role="3cmrfH" value="1" />
            </node>
            <node concept="3clFbS" id="1UsfiVCPAHW" role="3Kbo56">
              <node concept="3cpWs6" id="1UsfiVCPAMq" role="3cqZAp">
                <node concept="2ShNRf" id="1UsfiVCPAMG" role="3cqZAk">
                  <node concept="1pGfFk" id="1UsfiVCPGZ4" role="2ShVmc">
                    <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                    <node concept="3cmrfG" id="1UsfiVCPH1q" role="37wK5m">
                      <property role="3cmrfH" value="205" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPHab" role="37wK5m">
                      <property role="3cmrfH" value="74" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPI9L" role="37wK5m">
                      <property role="3cmrfH" value="20" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="1UsfiVCPIfX" role="3KbHQx">
            <node concept="3cmrfG" id="1UsfiVCPIiZ" role="3Kbmr1">
              <property role="3cmrfH" value="2" />
            </node>
            <node concept="3clFbS" id="1UsfiVCPIfZ" role="3Kbo56">
              <node concept="3cpWs6" id="1UsfiVCPIlc" role="3cqZAp">
                <node concept="2ShNRf" id="1UsfiVCPIl$" role="3cqZAk">
                  <node concept="1pGfFk" id="1UsfiVCPIJL" role="2ShVmc">
                    <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                    <node concept="3cmrfG" id="1UsfiVCPIM$" role="37wK5m">
                      <property role="3cmrfH" value="153" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPIVd" role="37wK5m">
                      <property role="3cmrfH" value="88" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPIVk" role="37wK5m">
                      <property role="3cmrfH" value="61" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="1UsfiVCPJ0r" role="3KbHQx">
            <node concept="3clFbS" id="1UsfiVCPJ0t" role="3Kbo56">
              <node concept="3cpWs6" id="1UsfiVCPJ8I" role="3cqZAp">
                <node concept="2ShNRf" id="1UsfiVCPJ8O" role="3cqZAk">
                  <node concept="1pGfFk" id="1UsfiVCPJwG" role="2ShVmc">
                    <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                    <node concept="3cmrfG" id="1UsfiVCPJzW" role="37wK5m">
                      <property role="3cmrfH" value="255" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPJEJ" role="37wK5m">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPJRv" role="37wK5m">
                      <property role="3cmrfH" value="0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cmrfG" id="1UsfiVCPJ6r" role="3Kbmr1">
              <property role="3cmrfH" value="3" />
            </node>
          </node>
          <node concept="3KbdKl" id="1UsfiVCPJVK" role="3KbHQx">
            <node concept="3cmrfG" id="1UsfiVCPK04" role="3Kbmr1">
              <property role="3cmrfH" value="4" />
            </node>
            <node concept="3clFbS" id="1UsfiVCPJVM" role="3Kbo56">
              <node concept="3cpWs6" id="1UsfiVCPK2t" role="3cqZAp">
                <node concept="2ShNRf" id="1UsfiVCPK6o" role="3cqZAk">
                  <node concept="1pGfFk" id="1UsfiVCPKxv" role="2ShVmc">
                    <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                    <node concept="3cmrfG" id="1UsfiVCPK_c" role="37wK5m">
                      <property role="3cmrfH" value="64" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPKD5" role="37wK5m">
                      <property role="3cmrfH" value="255" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPKQd" role="37wK5m">
                      <property role="3cmrfH" value="64" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="1UsfiVCPKUV" role="3KbHQx">
            <node concept="3cmrfG" id="1UsfiVCPKZS" role="3Kbmr1">
              <property role="3cmrfH" value="5" />
            </node>
            <node concept="3clFbS" id="1UsfiVCPKUX" role="3Kbo56">
              <node concept="3cpWs6" id="1UsfiVCPL2n" role="3cqZAp">
                <node concept="2ShNRf" id="1UsfiVCPL2t" role="3cqZAk">
                  <node concept="1pGfFk" id="1UsfiVCPLqp" role="2ShVmc">
                    <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                    <node concept="3cmrfG" id="1UsfiVCPLuz" role="37wK5m">
                      <property role="3cmrfH" value="84" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPLuI" role="37wK5m">
                      <property role="3cmrfH" value="204" />
                    </node>
                    <node concept="3cmrfG" id="1UsfiVCPM51" role="37wK5m">
                      <property role="3cmrfH" value="20" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1UsfiVCPN4W" role="3cqZAp">
          <node concept="10Nm6u" id="1UsfiVCPNaj" role="3cqZAk" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="2s5q4UYcBl" role="13h7CW">
      <node concept="3clFbS" id="2s5q4UYcBm" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="79Do2olGnrZ">
    <ref role="13h7C2" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="13i0hz" id="79Do2olHknY" role="13h7CS">
      <property role="TrG5h" value="parentCondition" />
      <node concept="3Tm6S6" id="79Do2olHkwQ" role="1B3o_S" />
      <node concept="3clFbS" id="79Do2olHko0" role="3clF47">
        <node concept="3cpWs8" id="79Do2olHkwW" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olHkwX" role="3cpWs9">
            <property role="TrG5h" value="smtString" />
            <node concept="17QB3L" id="79Do2olHkwY" role="1tU5fm" />
            <node concept="Xl_RD" id="79Do2olHkwZ" role="33vP2m">
              <property role="Xl_RC" value="" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="79Do2olHkx0" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olHkx1" role="3clFbx">
            <node concept="3cpWs8" id="79Do2olHkx2" role="3cqZAp">
              <node concept="3cpWsn" id="79Do2olHkx3" role="3cpWs9">
                <property role="TrG5h" value="macroIf" />
                <node concept="3Tqbb2" id="79Do2olHkx4" role="1tU5fm">
                  <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                </node>
                <node concept="1PxgMI" id="79Do2olHkx5" role="33vP2m">
                  <property role="1BlNFB" value="true" />
                  <ref role="1m5ApE" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                  <node concept="2OqwBi" id="79Do2olHkx6" role="1m5AlR">
                    <node concept="13iPFW" id="79Do2olHkx7" role="2Oq$k0" />
                    <node concept="1mfA1w" id="79Do2olHkx8" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="79Do2olHkx9" role="3cqZAp" />
            <node concept="3clFbF" id="79Do2olHkxa" role="3cqZAp">
              <node concept="37vLTI" id="79Do2olHkxb" role="3clFbG">
                <node concept="2OqwBi" id="79Do2olHkxc" role="37vLTx">
                  <node concept="37vLTw" id="79Do2olHkxd" role="2Oq$k0">
                    <ref role="3cqZAo" node="79Do2olHkx3" resolve="macroIf" />
                  </node>
                  <node concept="2qgKlT" id="79Do2olHkxe" role="2OqNvi">
                    <ref role="37wK5l" node="79Do2olGns2" resolve="buildConstraints" />
                  </node>
                </node>
                <node concept="37vLTw" id="79Do2olHkxf" role="37vLTJ">
                  <ref role="3cqZAo" node="79Do2olHkwX" resolve="smtString" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="79Do2olHkxg" role="3clFbw">
            <node concept="13iPFW" id="79Do2olHkxh" role="2Oq$k0" />
            <node concept="1BlSNk" id="79Do2olHkxi" role="2OqNvi">
              <ref role="1BmUXE" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
              <ref role="1Bn3mz" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="79Do2olHkxj" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olHkxk" role="3clFbx">
            <node concept="3cpWs8" id="79Do2olHkxl" role="3cqZAp">
              <node concept="3cpWsn" id="79Do2olHkxm" role="3cpWs9">
                <property role="TrG5h" value="macroIf" />
                <node concept="3Tqbb2" id="79Do2olHkxn" role="1tU5fm">
                  <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                </node>
                <node concept="1PxgMI" id="79Do2olHkxo" role="33vP2m">
                  <property role="1BlNFB" value="true" />
                  <ref role="1m5ApE" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                  <node concept="2OqwBi" id="79Do2olHkxp" role="1m5AlR">
                    <node concept="13iPFW" id="79Do2olHkxq" role="2Oq$k0" />
                    <node concept="1mfA1w" id="79Do2olHkxr" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="79Do2olHkxs" role="3cqZAp" />
            <node concept="3clFbF" id="79Do2olHkxt" role="3cqZAp">
              <node concept="37vLTI" id="79Do2olHkxu" role="3clFbG">
                <node concept="2OqwBi" id="79Do2olHkxv" role="37vLTx">
                  <node concept="37vLTw" id="79Do2olHkxw" role="2Oq$k0">
                    <ref role="3cqZAo" node="79Do2olHkxm" resolve="macroIf" />
                  </node>
                  <node concept="2qgKlT" id="79Do2olHkxx" role="2OqNvi">
                    <ref role="37wK5l" node="79Do2olH5fT" resolve="conditionElse" />
                  </node>
                </node>
                <node concept="37vLTw" id="79Do2olHkxy" role="37vLTJ">
                  <ref role="3cqZAo" node="79Do2olHkwX" resolve="smtString" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="79Do2olHkxz" role="3clFbw">
            <node concept="13iPFW" id="79Do2olHkx$" role="2Oq$k0" />
            <node concept="1BlSNk" id="79Do2olHkx_" role="2OqNvi">
              <ref role="1Bn3mz" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
              <ref role="1BmUXE" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="79Do2olHkxA" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olHkxB" role="3clFbx">
            <node concept="3cpWs8" id="79Do2olHkxC" role="3cqZAp">
              <node concept="3cpWsn" id="79Do2olHkxD" role="3cpWs9">
                <property role="TrG5h" value="macroElseIf" />
                <node concept="3Tqbb2" id="79Do2olHkxE" role="1tU5fm">
                  <ref role="ehGHo" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
                </node>
                <node concept="1PxgMI" id="79Do2olHkxF" role="33vP2m">
                  <property role="1BlNFB" value="true" />
                  <ref role="1m5ApE" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
                  <node concept="2OqwBi" id="79Do2olHkxG" role="1m5AlR">
                    <node concept="13iPFW" id="79Do2olHkxH" role="2Oq$k0" />
                    <node concept="1mfA1w" id="79Do2olHkxI" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="79Do2olHkxJ" role="3cqZAp">
              <node concept="37vLTI" id="79Do2olHkxK" role="3clFbG">
                <node concept="2OqwBi" id="79Do2olHkxL" role="37vLTx">
                  <node concept="37vLTw" id="79Do2olHkxM" role="2Oq$k0">
                    <ref role="3cqZAo" node="79Do2olHkxD" resolve="macroElseIf" />
                  </node>
                  <node concept="2qgKlT" id="79Do2olHkxN" role="2OqNvi">
                    <ref role="37wK5l" node="79Do2olGNcB" resolve="buildConstraints" />
                  </node>
                </node>
                <node concept="37vLTw" id="79Do2olHkxO" role="37vLTJ">
                  <ref role="3cqZAo" node="79Do2olHkwX" resolve="smtString" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="79Do2olHkxP" role="3clFbw">
            <node concept="13iPFW" id="79Do2olHkxQ" role="2Oq$k0" />
            <node concept="1BlSNk" id="79Do2olHkxR" role="2OqNvi">
              <ref role="1BmUXE" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
              <ref role="1Bn3mz" to="4s7a:3fq3ZRIT59N" resolve="branch" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="79Do2olHkEm" role="3cqZAp">
          <node concept="37vLTw" id="79Do2olHkHZ" role="3cqZAk">
            <ref role="3cqZAo" node="79Do2olHkwX" resolve="smtString" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="79Do2olHkwT" role="3clF45" />
    </node>
    <node concept="13i0hz" id="79Do2olGns2" role="13h7CS">
      <property role="TrG5h" value="buildConstraints" />
      <node concept="3Tm1VV" id="79Do2olGns3" role="1B3o_S" />
      <node concept="3clFbS" id="79Do2olGns4" role="3clF47">
        <node concept="3cpWs8" id="79Do2olGntb" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olGnte" role="3cpWs9">
            <property role="TrG5h" value="condition" />
            <node concept="17QB3L" id="79Do2olGnta" role="1tU5fm" />
            <node concept="2OqwBi" id="79Do2olGnvF" role="33vP2m">
              <node concept="13iPFW" id="79Do2olGntF" role="2Oq$k0" />
              <node concept="3TrcHB" id="79Do2olGnzq" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olGMEO" role="3cqZAp" />
        <node concept="3cpWs8" id="6fzU_SFDcs2" role="3cqZAp">
          <node concept="3cpWsn" id="6fzU_SFDcs5" role="3cpWs9">
            <property role="TrG5h" value="parentCondition" />
            <node concept="17QB3L" id="6fzU_SFDcs0" role="1tU5fm" />
            <node concept="BsUDl" id="6fzU_SFDcA9" role="33vP2m">
              <ref role="37wK5l" node="79Do2olHknY" resolve="parentCondition" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="6fzU_SFDcF7" role="3cqZAp">
          <node concept="3cpWsn" id="6fzU_SFDcFa" role="3cpWs9">
            <property role="TrG5h" value="smtString" />
            <node concept="17QB3L" id="6fzU_SFDcF5" role="1tU5fm" />
            <node concept="2YIFZM" id="6fzU_SFDdz1" role="33vP2m">
              <ref role="37wK5l" node="79Do2olGLFV" resolve="parseCondition" />
              <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
              <node concept="37vLTw" id="6fzU_SFDdz2" role="37wK5m">
                <ref role="3cqZAo" node="79Do2olGnte" resolve="condition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="6fzU_SFDcM_" role="3cqZAp">
          <node concept="3clFbS" id="6fzU_SFDcMB" role="3clFbx">
            <node concept="3clFbF" id="6fzU_SFDd9f" role="3cqZAp">
              <node concept="37vLTI" id="6fzU_SFDdd9" role="3clFbG">
                <node concept="3cpWs3" id="6fzU_SFDdw5" role="37vLTx">
                  <node concept="Xl_RD" id="6fzU_SFDdw8" role="3uHU7w">
                    <property role="Xl_RC" value=")\n" />
                  </node>
                  <node concept="3cpWs3" id="6fzU_SFDdrl" role="3uHU7B">
                    <node concept="3cpWs3" id="6fzU_SFDdmr" role="3uHU7B">
                      <node concept="3cpWs3" id="6fzU_SFDdiQ" role="3uHU7B">
                        <node concept="Xl_RD" id="6fzU_SFDddT" role="3uHU7B">
                          <property role="Xl_RC" value="(and " />
                        </node>
                        <node concept="37vLTw" id="6fzU_SFDdjE" role="3uHU7w">
                          <ref role="3cqZAo" node="6fzU_SFDcs5" resolve="parentCondition" />
                        </node>
                      </node>
                      <node concept="Xl_RD" id="6fzU_SFDdnt" role="3uHU7w">
                        <property role="Xl_RC" value=" " />
                      </node>
                    </node>
                    <node concept="37vLTw" id="6fzU_SFDdt1" role="3uHU7w">
                      <ref role="3cqZAo" node="6fzU_SFDcFa" resolve="smtString" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="6fzU_SFDd9d" role="37vLTJ">
                  <ref role="3cqZAo" node="6fzU_SFDcFa" resolve="smtString" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6fzU_SFDcVi" role="3clFbw">
            <node concept="37vLTw" id="6fzU_SFDcQA" role="2Oq$k0">
              <ref role="3cqZAo" node="6fzU_SFDcs5" resolve="parentCondition" />
            </node>
            <node concept="17RvpY" id="6fzU_SFDd8V" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olHpDz" role="3cqZAp" />
        <node concept="3clFbJ" id="79Do2olGMsL" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olGMsN" role="3clFbx">
            <node concept="3cpWs6" id="79Do2olGM$D" role="3cqZAp">
              <node concept="37vLTw" id="6fzU_SFDdOK" role="3cqZAk">
                <ref role="3cqZAo" node="6fzU_SFDcFa" resolve="smtString" />
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="79Do2olGMzW" role="3clFbw">
            <node concept="10Nm6u" id="79Do2olGM$j" role="3uHU7w" />
            <node concept="37vLTw" id="6fzU_SFDdOt" role="3uHU7B">
              <ref role="3cqZAo" node="6fzU_SFDcFa" resolve="smtString" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="79Do2olGn$T" role="3cqZAp">
          <node concept="3cpWs3" id="79Do2olGtV3" role="3cqZAk">
            <node concept="37vLTw" id="79Do2olGtWf" role="3uHU7w">
              <ref role="3cqZAo" node="79Do2olGnte" resolve="condition" />
            </node>
            <node concept="Xl_RD" id="79Do2olGn_h" role="3uHU7B">
              <property role="Xl_RC" value="no match for " />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="79Do2olGnsa" role="3clF45" />
    </node>
    <node concept="13i0hz" id="79Do2olH5fT" role="13h7CS">
      <property role="TrG5h" value="conditionElse" />
      <node concept="3Tm1VV" id="79Do2olH5fU" role="1B3o_S" />
      <node concept="3clFbS" id="79Do2olH5fV" role="3clF47">
        <node concept="3cpWs8" id="79Do2olH5iM" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olH5iN" role="3cpWs9">
            <property role="TrG5h" value="condition" />
            <node concept="17QB3L" id="79Do2olH5iO" role="1tU5fm" />
            <node concept="2OqwBi" id="79Do2olH5iP" role="33vP2m">
              <node concept="13iPFW" id="79Do2olH5iQ" role="2Oq$k0" />
              <node concept="3TrcHB" id="79Do2olH5iR" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olHlgq" role="3cqZAp" />
        <node concept="3clFbH" id="79Do2olH$Tn" role="3cqZAp" />
        <node concept="3cpWs8" id="79Do2olHlcF" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olHlcG" role="3cpWs9">
            <property role="TrG5h" value="parentCondition" />
            <node concept="17QB3L" id="79Do2olHlcH" role="1tU5fm" />
            <node concept="BsUDl" id="79Do2olHlcI" role="33vP2m">
              <ref role="37wK5l" node="79Do2olHknY" resolve="parentCondition" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="79Do2olHAJR" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olHAJU" role="3cpWs9">
            <property role="TrG5h" value="smtString" />
            <node concept="17QB3L" id="79Do2olHAJP" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olHNe2" role="3cqZAp" />
        <node concept="3cpWs8" id="79Do2olHNt2" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olHNt5" role="3cpWs9">
            <property role="TrG5h" value="parts" />
            <node concept="_YKpA" id="79Do2olHNzW" role="1tU5fm">
              <node concept="17QB3L" id="79Do2olHN$7" role="_ZDj9" />
            </node>
            <node concept="2ShNRf" id="79Do2olHNFN" role="33vP2m">
              <node concept="Tc6Ow" id="79Do2olHNFJ" role="2ShVmc">
                <node concept="17QB3L" id="79Do2olHNFK" role="HW$YZ" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olHN$T" role="3cqZAp" />
        <node concept="3clFbJ" id="79Do2olHYbp" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olHYbr" role="3clFbx">
            <node concept="3clFbF" id="79Do2olHVO8" role="3cqZAp">
              <node concept="2OqwBi" id="79Do2olHW0g" role="3clFbG">
                <node concept="37vLTw" id="79Do2olHVO6" role="2Oq$k0">
                  <ref role="3cqZAo" node="79Do2olHNt5" resolve="parts" />
                </node>
                <node concept="TSZUe" id="79Do2olHWkI" role="2OqNvi">
                  <node concept="37vLTw" id="79Do2olHWm1" role="25WWJ7">
                    <ref role="3cqZAo" node="79Do2olHlcG" resolve="parentCondition" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="79Do2olHYls" role="3clFbw">
            <node concept="37vLTw" id="79Do2olHYgP" role="2Oq$k0">
              <ref role="3cqZAo" node="79Do2olHlcG" resolve="parentCondition" />
            </node>
            <node concept="17RvpY" id="79Do2olHYy4" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbF" id="79Do2olHNNc" role="3cqZAp">
          <node concept="2OqwBi" id="79Do2olHNZh" role="3clFbG">
            <node concept="37vLTw" id="79Do2olHNNa" role="2Oq$k0">
              <ref role="3cqZAo" node="79Do2olHNt5" resolve="parts" />
            </node>
            <node concept="TSZUe" id="79Do2olHOlf" role="2OqNvi">
              <node concept="2YIFZM" id="79Do2olHQSO" role="25WWJ7">
                <ref role="37wK5l" node="79Do2olGQZJ" resolve="negate" />
                <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                <node concept="2YIFZM" id="79Do2olHOoU" role="37wK5m">
                  <ref role="37wK5l" node="79Do2olGLFV" resolve="parseCondition" />
                  <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                  <node concept="37vLTw" id="79Do2olHOqh" role="37wK5m">
                    <ref role="3cqZAo" node="79Do2olH5iN" resolve="condition" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olHNmh" role="3cqZAp" />
        <node concept="2Gpval" id="79Do2olHODg" role="3cqZAp">
          <node concept="2GrKxI" id="79Do2olHODi" role="2Gsz3X">
            <property role="TrG5h" value="elseIf" />
          </node>
          <node concept="3clFbS" id="79Do2olHODk" role="2LFqv$">
            <node concept="3clFbF" id="79Do2olHPOU" role="3cqZAp">
              <node concept="2OqwBi" id="79Do2olHPWM" role="3clFbG">
                <node concept="37vLTw" id="79Do2olHPOT" role="2Oq$k0">
                  <ref role="3cqZAo" node="79Do2olHNt5" resolve="parts" />
                </node>
                <node concept="TSZUe" id="79Do2olHQf8" role="2OqNvi">
                  <node concept="2YIFZM" id="79Do2olHR2U" role="25WWJ7">
                    <ref role="37wK5l" node="79Do2olGQZJ" resolve="negate" />
                    <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                    <node concept="2YIFZM" id="79Do2olHQjM" role="37wK5m">
                      <ref role="37wK5l" node="79Do2olGLFV" resolve="parseCondition" />
                      <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                      <node concept="2OqwBi" id="79Do2olHQnI" role="37wK5m">
                        <node concept="2GrUjf" id="79Do2olHQl9" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="79Do2olHODi" resolve="elseIf" />
                        </node>
                        <node concept="3TrcHB" id="79Do2olHQwa" role="2OqNvi">
                          <ref role="3TsBF5" to="4s7a:3fq3ZRIT59L" resolve="condition" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="79Do2olHP_o" role="2GsD0m">
            <node concept="13iPFW" id="79Do2olHPxb" role="2Oq$k0" />
            <node concept="3Tsc0h" id="79Do2olHPKo" role="2OqNvi">
              <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="79Do2olHYCr" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olHYCt" role="3clFbx">
            <node concept="3clFbF" id="79Do2olHRUy" role="3cqZAp">
              <node concept="37vLTI" id="79Do2olHS49" role="3clFbG">
                <node concept="3cpWs3" id="79Do2olHTow" role="37vLTx">
                  <node concept="Xl_RD" id="79Do2olHTqa" role="3uHU7w">
                    <property role="Xl_RC" value=")" />
                  </node>
                  <node concept="3cpWs3" id="79Do2olHScM" role="3uHU7B">
                    <node concept="Xl_RD" id="79Do2olHS6X" role="3uHU7B">
                      <property role="Xl_RC" value="(and " />
                    </node>
                    <node concept="2OqwBi" id="79Do2olHSmf" role="3uHU7w">
                      <node concept="37vLTw" id="79Do2olHSd$" role="2Oq$k0">
                        <ref role="3cqZAo" node="79Do2olHNt5" resolve="parts" />
                      </node>
                      <node concept="3uJxvA" id="79Do2olHSUJ" role="2OqNvi">
                        <node concept="Xl_RD" id="79Do2olHThK" role="3uJOhx">
                          <property role="Xl_RC" value=" " />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="79Do2olHRUw" role="37vLTJ">
                  <ref role="3cqZAo" node="79Do2olHAJU" resolve="smtString" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOSWO" id="79Do2olI0wj" role="3clFbw">
            <node concept="3cmrfG" id="79Do2olI0wm" role="3uHU7w">
              <property role="3cmrfH" value="1" />
            </node>
            <node concept="2OqwBi" id="79Do2olHYU2" role="3uHU7B">
              <node concept="37vLTw" id="79Do2olHYI8" role="2Oq$k0">
                <ref role="3cqZAo" node="79Do2olHNt5" resolve="parts" />
              </node>
              <node concept="34oBXx" id="79Do2olI07t" role="2OqNvi" />
            </node>
          </node>
          <node concept="9aQIb" id="79Do2olI0Fl" role="9aQIa">
            <node concept="3clFbS" id="79Do2olI0Fm" role="9aQI4">
              <node concept="3clFbF" id="79Do2olI0J$" role="3cqZAp">
                <node concept="37vLTI" id="79Do2olI0NS" role="3clFbG">
                  <node concept="1y4W85" id="79Do2olI18l" role="37vLTx">
                    <node concept="3cmrfG" id="79Do2olI18X" role="1y58nS">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="37vLTw" id="79Do2olI0Oa" role="1y566C">
                      <ref role="3cqZAo" node="79Do2olHNt5" resolve="parts" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="79Do2olI0Jz" role="37vLTJ">
                    <ref role="3cqZAo" node="79Do2olHAJU" resolve="smtString" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="79Do2olHlTJ" role="3cqZAp">
          <node concept="37vLTw" id="79Do2olHBA6" role="3cqZAk">
            <ref role="3cqZAo" node="79Do2olHAJU" resolve="smtString" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="79Do2olH5iF" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4P2s9PP_NZt" role="13h7CS">
      <property role="TrG5h" value="TrueBranchSAT" />
      <node concept="3Tm1VV" id="4P2s9PP_NZu" role="1B3o_S" />
      <node concept="3clFbS" id="4P2s9PP_NZv" role="3clF47">
        <node concept="3cpWs8" id="4P2s9PP_Eqv" role="3cqZAp">
          <node concept="3cpWsn" id="4P2s9PP_Eqy" role="3cpWs9">
            <property role="TrG5h" value="smt" />
            <node concept="17QB3L" id="4P2s9PP_Eqt" role="1tU5fm" />
            <node concept="3cpWs3" id="4P2s9PP_EFI" role="33vP2m">
              <node concept="Xl_RD" id="4P2s9PP_EFO" role="3uHU7w">
                <property role="Xl_RC" value=")\n" />
              </node>
              <node concept="3cpWs3" id="4P2s9PP_Ey0" role="3uHU7B">
                <node concept="Xl_RD" id="4P2s9PP_Esx" role="3uHU7B">
                  <property role="Xl_RC" value="(assert " />
                </node>
                <node concept="2OqwBi" id="4P2s9PP_E$N" role="3uHU7w">
                  <node concept="13iPFW" id="4P2s9PP_Ovl" role="2Oq$k0" />
                  <node concept="2qgKlT" id="4P2s9PP_ECN" role="2OqNvi">
                    <ref role="37wK5l" node="79Do2olGns2" resolve="buildConstraints" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PP_F8B" role="3cqZAp" />
        <node concept="3clFbJ" id="4P2s9PP_Fi4" role="3cqZAp">
          <node concept="3clFbS" id="4P2s9PP_Fi6" role="3clFbx">
            <node concept="3clFbF" id="4P2s9PP_G_8" role="3cqZAp">
              <node concept="d57v9" id="4P2s9PP_GBp" role="3clFbG">
                <node concept="3cpWs3" id="4P2s9PP_GUz" role="37vLTx">
                  <node concept="Xl_RD" id="4P2s9PP_GX6" role="3uHU7w">
                    <property role="Xl_RC" value=")\n" />
                  </node>
                  <node concept="3cpWs3" id="4P2s9PP_GJI" role="3uHU7B">
                    <node concept="Xl_RD" id="4P2s9PP_GBM" role="3uHU7B">
                      <property role="Xl_RC" value="(assert " />
                    </node>
                    <node concept="2OqwBi" id="4P2s9PP_GK4" role="3uHU7w">
                      <node concept="2OqwBi" id="4P2s9PP_GK5" role="2Oq$k0">
                        <node concept="13iPFW" id="4P2s9PP_O$8" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="4P2s9PP_GK7" role="2OqNvi">
                          <node concept="1xMEDy" id="4P2s9PP_GK8" role="1xVPHs">
                            <node concept="chp4Y" id="4P2s9PP_GK9" role="ri$Ld">
                              <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3TrcHB" id="4P2s9PP_GKa" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="4P2s9PP_G_6" role="37vLTJ">
                  <ref role="3cqZAo" node="4P2s9PP_Eqy" resolve="smt" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4P2s9PP_G8k" role="3clFbw">
            <node concept="2OqwBi" id="4P2s9PP_FSr" role="2Oq$k0">
              <node concept="2OqwBi" id="4P2s9PP_FH1" role="2Oq$k0">
                <node concept="13iPFW" id="4P2s9PP_OyA" role="2Oq$k0" />
                <node concept="2Xjw5R" id="4P2s9PP_FPg" role="2OqNvi">
                  <node concept="1xMEDy" id="4P2s9PP_FPi" role="1xVPHs">
                    <node concept="chp4Y" id="4P2s9PP_FPY" role="ri$Ld">
                      <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3TrcHB" id="4P2s9PP_G2h" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
              </node>
            </node>
            <node concept="17RvpY" id="4P2s9PP_GzQ" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="4P2s9PP_OAe" role="3cqZAp">
          <node concept="2YIFZM" id="4P2s9PP_DRp" role="3cqZAk">
            <ref role="37wK5l" node="79Do2olIt_i" resolve="solve" />
            <ref role="1Pybhc" node="79Do2olI4S$" resolve="MySolver" />
            <node concept="37vLTw" id="4P2s9PP_EJs" role="37wK5m">
              <ref role="3cqZAo" node="4P2s9PP_Eqy" resolve="smt" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10P_77" id="4P2s9PP_O9N" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4P2s9PPAkgR" role="13h7CS">
      <property role="TrG5h" value="ElseBranchSAT" />
      <node concept="3Tm1VV" id="4P2s9PPAkgS" role="1B3o_S" />
      <node concept="3clFbS" id="4P2s9PPAkgT" role="3clF47">
        <node concept="3cpWs8" id="4P2s9PPAkgU" role="3cqZAp">
          <node concept="3cpWsn" id="4P2s9PPAkgV" role="3cpWs9">
            <property role="TrG5h" value="smt" />
            <node concept="17QB3L" id="4P2s9PPAkgW" role="1tU5fm" />
            <node concept="3cpWs3" id="4P2s9PPAkgX" role="33vP2m">
              <node concept="Xl_RD" id="4P2s9PPAkgY" role="3uHU7w">
                <property role="Xl_RC" value=")\n" />
              </node>
              <node concept="3cpWs3" id="4P2s9PPAkgZ" role="3uHU7B">
                <node concept="Xl_RD" id="4P2s9PPAkh0" role="3uHU7B">
                  <property role="Xl_RC" value="(assert " />
                </node>
                <node concept="2OqwBi" id="4P2s9PPAkh1" role="3uHU7w">
                  <node concept="13iPFW" id="4P2s9PPAkh2" role="2Oq$k0" />
                  <node concept="2qgKlT" id="4P2s9PPAl7W" role="2OqNvi">
                    <ref role="37wK5l" node="79Do2olH5fT" resolve="conditionElse" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PPAkh4" role="3cqZAp" />
        <node concept="3clFbJ" id="4P2s9PPAkh5" role="3cqZAp">
          <node concept="3clFbS" id="4P2s9PPAkh6" role="3clFbx">
            <node concept="3clFbF" id="4P2s9PPAkh7" role="3cqZAp">
              <node concept="d57v9" id="4P2s9PPAkh8" role="3clFbG">
                <node concept="3cpWs3" id="4P2s9PPAkh9" role="37vLTx">
                  <node concept="Xl_RD" id="4P2s9PPAkha" role="3uHU7w">
                    <property role="Xl_RC" value=")\n" />
                  </node>
                  <node concept="3cpWs3" id="4P2s9PPAkhb" role="3uHU7B">
                    <node concept="Xl_RD" id="4P2s9PPAkhc" role="3uHU7B">
                      <property role="Xl_RC" value="(assert " />
                    </node>
                    <node concept="2OqwBi" id="4P2s9PPAkhd" role="3uHU7w">
                      <node concept="2OqwBi" id="4P2s9PPAkhe" role="2Oq$k0">
                        <node concept="13iPFW" id="4P2s9PPAkhf" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="4P2s9PPAkhg" role="2OqNvi">
                          <node concept="1xMEDy" id="4P2s9PPAkhh" role="1xVPHs">
                            <node concept="chp4Y" id="4P2s9PPAkhi" role="ri$Ld">
                              <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3TrcHB" id="4P2s9PPAkhj" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="4P2s9PPAkhk" role="37vLTJ">
                  <ref role="3cqZAo" node="4P2s9PPAkgV" resolve="smt" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4P2s9PPAkhl" role="3clFbw">
            <node concept="2OqwBi" id="4P2s9PPAkhm" role="2Oq$k0">
              <node concept="2OqwBi" id="4P2s9PPAkhn" role="2Oq$k0">
                <node concept="13iPFW" id="4P2s9PPAkho" role="2Oq$k0" />
                <node concept="2Xjw5R" id="4P2s9PPAkhp" role="2OqNvi">
                  <node concept="1xMEDy" id="4P2s9PPAkhq" role="1xVPHs">
                    <node concept="chp4Y" id="4P2s9PPAkhr" role="ri$Ld">
                      <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3TrcHB" id="4P2s9PPAkhs" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
              </node>
            </node>
            <node concept="17RvpY" id="4P2s9PPAkht" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="4P2s9PPAkhu" role="3cqZAp">
          <node concept="2YIFZM" id="4P2s9PPAkhv" role="3cqZAk">
            <ref role="37wK5l" node="79Do2olIt_i" resolve="solve" />
            <ref role="1Pybhc" node="79Do2olI4S$" resolve="MySolver" />
            <node concept="37vLTw" id="4P2s9PPAkhw" role="37wK5m">
              <ref role="3cqZAo" node="4P2s9PPAkgV" resolve="smt" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10P_77" id="4P2s9PPAkhx" role="3clF45" />
    </node>
    <node concept="13i0hz" id="5a5L3hYbrek" role="13h7CS">
      <property role="TrG5h" value="trueBranchAligned" />
      <node concept="3Tm1VV" id="5a5L3hYbrel" role="1B3o_S" />
      <node concept="3clFbS" id="5a5L3hYbrem" role="3clF47">
        <node concept="3cpWs6" id="5a5L3hYbru1" role="3cqZAp">
          <node concept="2OqwBi" id="5a5L3hYbrwD" role="3cqZAk">
            <node concept="13iPFW" id="5a5L3hYbruA" role="2Oq$k0" />
            <node concept="3Tsc0h" id="5a5L3hYbr$n" role="2OqNvi">
              <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="5a5L3hYbrtX" role="3clF45">
        <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
      </node>
    </node>
    <node concept="13i0hz" id="2NfWTcLJRHk" role="13h7CS">
      <property role="TrG5h" value="isCloneIf" />
      <node concept="3Tm1VV" id="2NfWTcLJRHl" role="1B3o_S" />
      <node concept="10P_77" id="2NfWTcLJXQf" role="3clF45" />
      <node concept="3clFbS" id="2NfWTcLJRHn" role="3clF47">
        <node concept="3cpWs6" id="2NfWTcLJXRp" role="3cqZAp">
          <node concept="22lmx$" id="YL52UEtXDk" role="3cqZAk">
            <node concept="2OqwBi" id="YL52UEtYK3" role="3uHU7w">
              <node concept="2OqwBi" id="YL52UEtXX3" role="2Oq$k0">
                <node concept="13iPFW" id="YL52UEtXKj" role="2Oq$k0" />
                <node concept="3TrcHB" id="YL52UEtYgI" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                </node>
              </node>
              <node concept="liA8E" id="YL52UEtZgJ" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.contains(java.lang.CharSequence):boolean" resolve="contains" />
                <node concept="Xl_RD" id="YL52UEtZmW" role="37wK5m">
                  <property role="Xl_RC" value="FORK" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="2NfWTcLJYFv" role="3uHU7B">
              <node concept="2OqwBi" id="2NfWTcLJY16" role="2Oq$k0">
                <node concept="13iPFW" id="2NfWTcLJXRC" role="2Oq$k0" />
                <node concept="3TrcHB" id="2NfWTcLJYft" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                </node>
              </node>
              <node concept="liA8E" id="2NfWTcLJZ8N" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.contains(java.lang.CharSequence):boolean" resolve="contains" />
                <node concept="Xl_RD" id="2NfWTcLJZbG" role="37wK5m">
                  <property role="Xl_RC" value="CLONE" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="YL52UEtZzc" role="13h7CS">
      <property role="TrG5h" value="project" />
      <node concept="3Tm1VV" id="YL52UEtZzd" role="1B3o_S" />
      <node concept="17QB3L" id="YL52UEu0gn" role="3clF45" />
      <node concept="3clFbS" id="YL52UEtZzf" role="3clF47">
        <node concept="3cpWs8" id="YL52UEu2Cv" role="3cqZAp">
          <node concept="3cpWsn" id="YL52UEu2Cw" role="3cpWs9">
            <property role="TrG5h" value="d" />
            <node concept="3uibUv" id="775CKuYw4rg" role="1tU5fm">
              <ref role="3uigEE" to="iptd:775CKuYuMsv" resolve="Backend" />
            </node>
            <node concept="2YIFZM" id="3nQIFD_VGpG" role="33vP2m">
              <ref role="37wK5l" to="iptd:3nQIFD_UGB_" resolve="getInstance" />
              <ref role="1Pybhc" to="iptd:775CKuYuMsv" resolve="Backend" />
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="1h73n7Obsse" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="34ab3g" id="3QV_CwFGqCq" role="8Wnug">
            <property role="35gtTG" value="warn" />
            <node concept="3cpWs3" id="3QV_CwFGqCt" role="34bqiv">
              <node concept="3cpWs3" id="3QV_CwFGqCu" role="3uHU7B">
                <node concept="3cpWs3" id="3QV_CwFGqCv" role="3uHU7B">
                  <node concept="3cpWs3" id="3QV_CwFGqCw" role="3uHU7B">
                    <node concept="Xl_RD" id="3QV_CwFGqCx" role="3uHU7B">
                      <property role="Xl_RC" value="project(" />
                    </node>
                    <node concept="2OqwBi" id="775CKuY$mFR" role="3uHU7w">
                      <node concept="13iPFW" id="775CKuY$mvo" role="2Oq$k0" />
                      <node concept="3TrcHB" id="775CKuY$mU_" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="3QV_CwFGqCz" role="3uHU7w">
                    <property role="Xl_RC" value=", " />
                  </node>
                </node>
                <node concept="37vLTw" id="775CKuY$mlp" role="3uHU7w">
                  <ref role="3cqZAo" node="YL52UEu0es" resolve="projectionConstraint" />
                </node>
              </node>
              <node concept="Xl_RD" id="3QV_CwFGqC_" role="3uHU7w">
                <property role="Xl_RC" value=")" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="YL52UEuaW5" role="3cqZAp">
          <node concept="3cpWsn" id="YL52UEuaW6" role="3cpWs9">
            <property role="TrG5h" value="result" />
            <node concept="17QB3L" id="YL52UEub2s" role="1tU5fm" />
            <node concept="2OqwBi" id="YL52UEuaW8" role="33vP2m">
              <node concept="37vLTw" id="YL52UEuaW9" role="2Oq$k0">
                <ref role="3cqZAo" node="YL52UEu2Cw" resolve="d" />
              </node>
              <node concept="liA8E" id="YL52UEuaWa" role="2OqNvi">
                <ref role="37wK5l" to="iptd:775CKuYuMBd" resolve="project" />
                <node concept="2OqwBi" id="YL52UEuaWc" role="37wK5m">
                  <node concept="13iPFW" id="YL52UEuaWd" role="2Oq$k0" />
                  <node concept="3TrcHB" id="YL52UEuaWe" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                  </node>
                </node>
                <node concept="37vLTw" id="YL52UEuaWb" role="37wK5m">
                  <ref role="3cqZAo" node="YL52UEu0es" resolve="projectionConstraint" />
                </node>
                <node concept="37vLTw" id="775CKuYz1ki" role="37wK5m">
                  <ref role="3cqZAo" node="YL52UEu0fs" resolve="link" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="1h73n7Obs$I" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="34ab3g" id="YL52UEubbq" role="8Wnug">
            <property role="35gtTG" value="warn" />
            <node concept="3cpWs3" id="YL52UEueNC" role="34bqiv">
              <node concept="37vLTw" id="YL52UEueSX" role="3uHU7w">
                <ref role="3cqZAo" node="YL52UEuaW6" resolve="result" />
              </node>
              <node concept="3cpWs3" id="YL52UEue7Y" role="3uHU7B">
                <node concept="3cpWs3" id="YL52UEudjk" role="3uHU7B">
                  <node concept="3cpWs3" id="YL52UEucCt" role="3uHU7B">
                    <node concept="3cpWs3" id="YL52UEubLb" role="3uHU7B">
                      <node concept="Xl_RD" id="YL52UEubbs" role="3uHU7B">
                        <property role="Xl_RC" value="project(" />
                      </node>
                      <node concept="2OqwBi" id="775CKuY$n1s" role="3uHU7w">
                        <node concept="13iPFW" id="775CKuY$n1t" role="2Oq$k0" />
                        <node concept="3TrcHB" id="775CKuY$n1u" role="2OqNvi">
                          <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xl_RD" id="YL52UEucH9" role="3uHU7w">
                      <property role="Xl_RC" value=", " />
                    </node>
                  </node>
                  <node concept="37vLTw" id="775CKuY$mqk" role="3uHU7w">
                    <ref role="3cqZAo" node="YL52UEu0es" resolve="projectionConstraint" />
                  </node>
                </node>
                <node concept="Xl_RD" id="YL52UEue8i" role="3uHU7w">
                  <property role="Xl_RC" value=") = " />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="YL52UEub5P" role="3cqZAp">
          <node concept="37vLTw" id="YL52UEub6a" role="3cqZAk">
            <ref role="3cqZAo" node="YL52UEuaW6" resolve="result" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="YL52UEu0es" role="3clF46">
        <property role="TrG5h" value="projectionConstraint" />
        <node concept="17QB3L" id="YL52UEu0er" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="YL52UEu0fs" role="3clF46">
        <property role="TrG5h" value="link" />
        <node concept="17QB3L" id="YL52UEu0fG" role="1tU5fm" />
      </node>
    </node>
    <node concept="13hLZK" id="79Do2olGns0" role="13h7CW">
      <node concept="3clFbS" id="79Do2olGns1" role="2VODD2" />
    </node>
  </node>
  <node concept="312cEu" id="79Do2olGLFA">
    <property role="TrG5h" value="ConditionHelper" />
    <node concept="2YIFZL" id="79Do2olGLFV" role="jymVt">
      <property role="TrG5h" value="parseCondition" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="79Do2olGLFY" role="3clF47">
        <node concept="2ty0qM" id="79Do2olGLKf" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olGLKg" role="2tyzPh">
            <node concept="3cpWs6" id="79Do2olGLKh" role="3cqZAp">
              <node concept="3cpWs3" id="79Do2olGLKi" role="3cqZAk">
                <node concept="Xl_RD" id="79Do2olGLKj" role="3uHU7w">
                  <property role="Xl_RC" value=")" />
                </node>
                <node concept="3cpWs3" id="79Do2olGLKk" role="3uHU7B">
                  <node concept="3cpWs3" id="79Do2olGLKl" role="3uHU7B">
                    <node concept="3cpWs3" id="79Do2olGLKm" role="3uHU7B">
                      <node concept="3cpWs3" id="79Do2olGLKn" role="3uHU7B">
                        <node concept="3cpWs3" id="79Do2olGLKo" role="3uHU7B">
                          <node concept="Xl_RD" id="79Do2olGLKp" role="3uHU7B">
                            <property role="Xl_RC" value="(" />
                          </node>
                          <node concept="1TxZTf" id="79Do2olGLKq" role="3uHU7w">
                            <ref role="1Ty1U8" node="79Do2olGLKC" resolve="operator" />
                          </node>
                        </node>
                        <node concept="Xl_RD" id="79Do2olGLKr" role="3uHU7w">
                          <property role="Xl_RC" value=" " />
                        </node>
                      </node>
                      <node concept="1TxZTf" id="79Do2olGLKs" role="3uHU7w">
                        <ref role="1Ty1U8" node="79Do2olGLKI" resolve="identifier" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="79Do2olGLKt" role="3uHU7w">
                      <property role="Xl_RC" value=" " />
                    </node>
                  </node>
                  <node concept="1TxZTf" id="79Do2olGLKu" role="3uHU7w">
                    <ref role="1Ty1U8" node="79Do2olGLK_" resolve="value" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="79Do2olGLKv" role="2ty3UH">
            <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
          </node>
          <node concept="1Qi9sc" id="79Do2olGLKw" role="1YN4dH">
            <node concept="1OJ37Q" id="79Do2olGLKx" role="1QigWp">
              <node concept="1OJ37Q" id="79Do2olGLKy" role="1OLqdY">
                <node concept="1OJ37Q" id="79Do2olGLKz" role="1OLqdY">
                  <node concept="1SYyG9" id="79Do2olGLK$" role="1OLpdg">
                    <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                  </node>
                  <node concept="1Tukvm" id="79Do2olGLK_" role="1OLqdY">
                    <property role="TrG5h" value="value" />
                    <node concept="1OClNT" id="79Do2olGLKA" role="1TuGhC">
                      <node concept="1SYyG9" id="79Do2olGLKB" role="1OLDsb">
                        <ref role="1SYXPG" to="tpfp:h5SUwpi" resolve="\d" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1Tukvm" id="79Do2olGLKC" role="1OLpdg">
                  <property role="TrG5h" value="operator" />
                  <node concept="1SSJmt" id="79Do2olGLKD" role="1TuGhC">
                    <node concept="1T6I$Y" id="79Do2olGLKE" role="1T5LoC">
                      <property role="1T6KD9" value="&lt;" />
                    </node>
                    <node concept="1T6I$Y" id="79Do2olGLKF" role="1T5LoC">
                      <property role="1T6KD9" value="&gt;" />
                    </node>
                    <node concept="1T6I$Y" id="79Do2olGLKG" role="1T5LoC">
                      <property role="1T6KD9" value="=" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1OJ37Q" id="79Do2olGLKH" role="1OLpdg">
                <node concept="1Tukvm" id="79Do2olGLKI" role="1OLpdg">
                  <property role="TrG5h" value="identifier" />
                  <node concept="1OClNT" id="79Do2olGLKJ" role="1TuGhC">
                    <node concept="1SYyG9" id="79Do2olGLKK" role="1OLDsb">
                      <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                    </node>
                  </node>
                </node>
                <node concept="1SYyG9" id="79Do2olGLKL" role="1OLqdY">
                  <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2ty0qM" id="79Do2olGLKM" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olGLKN" role="2tyzPh">
            <node concept="3cpWs6" id="79Do2olGLKO" role="3cqZAp">
              <node concept="1TxZTf" id="79Do2olGLKT" role="3cqZAk">
                <ref role="1Ty1U8" node="79Do2olGLKZ" resolve="identifier" />
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="79Do2olGLKU" role="2ty3UH">
            <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
          </node>
          <node concept="1Qi9sc" id="79Do2olGLKV" role="1YN4dH">
            <node concept="1OJ37Q" id="79Do2olGLKW" role="1QigWp">
              <node concept="1OJ37Q" id="79Do2olGLKX" role="1OLqdY">
                <node concept="1OC9wW" id="79Do2olGLKY" role="1OLqdY">
                  <property role="1OCb_u" value=")" />
                </node>
                <node concept="1Tukvm" id="79Do2olGLKZ" role="1OLpdg">
                  <property role="TrG5h" value="identifier" />
                  <node concept="1OClNT" id="79Do2olGLL0" role="1TuGhC">
                    <node concept="1SYyG9" id="79Do2olGLL1" role="1OLDsb">
                      <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1OC9wW" id="79Do2olGLL2" role="1OLpdg">
                <property role="1OCb_u" value="ENABLED(" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2ty0qM" id="79Do2olGPYR" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olGPYS" role="2tyzPh">
            <node concept="3cpWs6" id="79Do2olGPYT" role="3cqZAp">
              <node concept="3cpWs3" id="79Do2olGQh9" role="3cqZAk">
                <node concept="Xl_RD" id="79Do2olGQj$" role="3uHU7w">
                  <property role="Xl_RC" value=")" />
                </node>
                <node concept="3cpWs3" id="79Do2olGQ2J" role="3uHU7B">
                  <node concept="Xl_RD" id="79Do2olGQ50" role="3uHU7B">
                    <property role="Xl_RC" value="(not " />
                  </node>
                  <node concept="1TxZTf" id="79Do2olGPYU" role="3uHU7w">
                    <ref role="1Ty1U8" node="79Do2olGPZ0" resolve="identifier" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="79Do2olGPYV" role="2ty3UH">
            <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
          </node>
          <node concept="1Qi9sc" id="79Do2olGPYW" role="1YN4dH">
            <node concept="1OJ37Q" id="79Do2olGPYX" role="1QigWp">
              <node concept="1OJ37Q" id="79Do2olGPYY" role="1OLqdY">
                <node concept="1OC9wW" id="79Do2olGPYZ" role="1OLqdY">
                  <property role="1OCb_u" value=")" />
                </node>
                <node concept="1Tukvm" id="79Do2olGPZ0" role="1OLpdg">
                  <property role="TrG5h" value="identifier" />
                  <node concept="1OClNT" id="79Do2olGPZ1" role="1TuGhC">
                    <node concept="1SYyG9" id="79Do2olGPZ2" role="1OLDsb">
                      <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1OC9wW" id="79Do2olGPZ3" role="1OLpdg">
                <property role="1OCb_u" value="DISABLED(" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2ty0qM" id="71CofnspEer" role="3cqZAp">
          <node concept="3clFbS" id="71CofnspEes" role="2tyzPh">
            <node concept="3cpWs6" id="71CofnspEet" role="3cqZAp">
              <node concept="3cpWs3" id="71CofnspFa_" role="3cqZAk">
                <node concept="Xl_RD" id="71CofnspFd_" role="3uHU7w">
                  <property role="Xl_RC" value=" -1)" />
                </node>
                <node concept="3cpWs3" id="71CofnspEZF" role="3uHU7B">
                  <node concept="Xl_RD" id="71CofnspEH5" role="3uHU7B">
                    <property role="Xl_RC" value="(&gt; " />
                  </node>
                  <node concept="1TxZTf" id="71CofnspF2G" role="3uHU7w">
                    <ref role="1Ty1U8" node="71CofnspEe$" resolve="identifier" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="71CofnspEev" role="2ty3UH">
            <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
          </node>
          <node concept="1Qi9sc" id="71CofnspEew" role="1YN4dH">
            <node concept="1OJ37Q" id="71CofnspEex" role="1QigWp">
              <node concept="1OJ37Q" id="71CofnspEey" role="1OLqdY">
                <node concept="1OJ37Q" id="71CofnspE_x" role="1OLqdY">
                  <node concept="1OJ37Q" id="71CofnspIis" role="1OLpdg">
                    <node concept="1OJ37Q" id="71Cofnsqo0V" role="1OLqdY">
                      <node concept="1SLe3L" id="71Cofnsqo3g" role="1OLqdY">
                        <node concept="1SSJmt" id="71Cofnsqo2H" role="1OLDsb">
                          <node concept="1T6I$Y" id="71Cofnsqo2Y" role="1T5LoC">
                            <property role="1T6KD9" value="(" />
                          </node>
                        </node>
                      </node>
                      <node concept="1SYyG9" id="71CofnspIiJ" role="1OLpdg">
                        <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                      </node>
                    </node>
                    <node concept="1OJ37Q" id="71CofnspLLN" role="1OLpdg">
                      <node concept="1OJ37Q" id="71CofnspUA1" role="1OLpdg">
                        <node concept="1OC9wW" id="71CofnspUAk" role="1OLpdg">
                          <property role="1OCb_u" value=")" />
                        </node>
                        <node concept="1SYyG9" id="71CofnspLM6" role="1OLqdY">
                          <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                        </node>
                      </node>
                      <node concept="1OC9wW" id="71CofnspIdS" role="1OLqdY">
                        <property role="1OCb_u" value="&amp;&amp;" />
                      </node>
                    </node>
                  </node>
                  <node concept="1OJ37Q" id="71CofnspEA6" role="1OLqdY">
                    <node concept="1OJ37Q" id="71CofnspRKJ" role="1OLqdY">
                      <node concept="1OJ37Q" id="71CofnspRLg" role="1OLqdY">
                        <node concept="1OJ37Q" id="71CofnspRLM" role="1OLqdY">
                          <node concept="1OC9wW" id="71CofnspRM5" role="1OLqdY">
                            <property role="1OCb_u" value="-1" />
                          </node>
                          <node concept="1SYyG9" id="71CofnspRLz" role="1OLpdg">
                            <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                          </node>
                        </node>
                        <node concept="1OC9wW" id="71CofnspRL2" role="1OLpdg">
                          <property role="1OCb_u" value="&gt;" />
                        </node>
                      </node>
                      <node concept="1SYyG9" id="71CofnspRID" role="1OLpdg">
                        <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                      </node>
                    </node>
                    <node concept="1OJ37Q" id="71Cofnsqo4i" role="1OLpdg">
                      <node concept="1Tukvm" id="71CofnspE_O" role="1OLpdg">
                        <property role="TrG5h" value="identifier2" />
                        <node concept="1OClNT" id="71CofnspE_P" role="1TuGhC">
                          <node concept="1SYyG9" id="71CofnspE_Q" role="1OLDsb">
                            <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                          </node>
                        </node>
                      </node>
                      <node concept="1SLe3L" id="71Cofnsqo4F" role="1OLqdY">
                        <node concept="1SSJmt" id="71Cofnsqo4G" role="1OLDsb">
                          <node concept="1T6I$Y" id="71Cofnsqo4H" role="1T5LoC">
                            <property role="1T6KD9" value=")" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1Tukvm" id="71CofnspEe$" role="1OLpdg">
                  <property role="TrG5h" value="identifier" />
                  <node concept="1OClNT" id="71CofnspEe_" role="1TuGhC">
                    <node concept="1SYyG9" id="71CofnspEeA" role="1OLDsb">
                      <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1OC9wW" id="71CofnspEeB" role="1OLpdg">
                <property role="1OCb_u" value="defined(" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="71CofnspErq" role="3cqZAp" />
        <node concept="2ty0qM" id="71CofnsmU$8" role="3cqZAp">
          <node concept="3clFbS" id="71CofnsmU$9" role="2tyzPh">
            <node concept="3cpWs6" id="71CofnsmU$a" role="3cqZAp">
              <node concept="1TxZTf" id="71CofnsmU$f" role="3cqZAk">
                <ref role="1Ty1U8" node="71CofnsmU$l" resolve="identifier" />
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="71CofnsmU$g" role="2ty3UH">
            <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
          </node>
          <node concept="1Qi9sc" id="71CofnsmU$h" role="1YN4dH">
            <node concept="1OJ37Q" id="71CofnsmU$i" role="1QigWp">
              <node concept="1OJ37Q" id="71CofnsmU$j" role="1OLqdY">
                <node concept="1OC9wW" id="71CofnsmU$k" role="1OLqdY">
                  <property role="1OCb_u" value=")" />
                </node>
                <node concept="1Tukvm" id="71CofnsmU$l" role="1OLpdg">
                  <property role="TrG5h" value="identifier" />
                  <node concept="1OClNT" id="71CofnsmU$m" role="1TuGhC">
                    <node concept="1SYyG9" id="71CofnsmU$n" role="1OLDsb">
                      <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1OC9wW" id="71CofnsmU$o" role="1OLpdg">
                <property role="1OCb_u" value="DEFINED(" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2ty0qM" id="71CofnspXqn" role="3cqZAp">
          <node concept="3clFbS" id="71CofnspXqo" role="2tyzPh">
            <node concept="3cpWs6" id="71CofnspXqp" role="3cqZAp">
              <node concept="3cpWs3" id="71CofnspYrV" role="3cqZAk">
                <node concept="Xl_RD" id="71CofnspYvM" role="3uHU7w">
                  <property role="Xl_RC" value=")" />
                </node>
                <node concept="3cpWs3" id="71CofnspYgM" role="3uHU7B">
                  <node concept="3cpWs3" id="71CofnspY2P" role="3uHU7B">
                    <node concept="3cpWs3" id="71CofnspXUp" role="3uHU7B">
                      <node concept="Xl_RD" id="71CofnspXF3" role="3uHU7B">
                        <property role="Xl_RC" value="(&lt; " />
                      </node>
                      <node concept="1TxZTf" id="71CofnspXX_" role="3uHU7w">
                        <ref role="1Ty1U8" node="71CofnspXqw" resolve="op1" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="71CofnspY38" role="3uHU7w">
                      <property role="Xl_RC" value=" " />
                    </node>
                  </node>
                  <node concept="1TxZTf" id="71CofnspYkx" role="3uHU7w">
                    <ref role="1Ty1U8" node="71CofnspXxO" resolve="op2" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="71CofnspXqr" role="2ty3UH">
            <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
          </node>
          <node concept="1Qi9sc" id="71CofnspXqs" role="1YN4dH">
            <node concept="1OJ37Q" id="71CofnspXqu" role="1QigWp">
              <node concept="1OJ37Q" id="71CofnspXwU" role="1OLqdY">
                <node concept="1Tukvm" id="71CofnspXxO" role="1OLqdY">
                  <property role="TrG5h" value="op2" />
                  <node concept="1OClNT" id="71CofnspXxP" role="1TuGhC">
                    <node concept="1SYyG9" id="71CofnspXxQ" role="1OLDsb">
                      <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                    </node>
                  </node>
                </node>
                <node concept="1OC9wW" id="71CofnspXqv" role="1OLpdg">
                  <property role="1OCb_u" value="&lt;" />
                </node>
              </node>
              <node concept="1Tukvm" id="71CofnspXqw" role="1OLpdg">
                <property role="TrG5h" value="op1" />
                <node concept="1OClNT" id="71CofnspXqx" role="1TuGhC">
                  <node concept="1SYyG9" id="71CofnspXqy" role="1OLDsb">
                    <ref role="1SYXPG" to="tpfp:h5SUJUw" resolve="\w" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="71CofnspE7c" role="3cqZAp" />
        <node concept="3cpWs6" id="5KGsxZrwmso" role="3cqZAp">
          <node concept="37vLTw" id="5KGsxZrwmHO" role="3cqZAk">
            <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
          </node>
        </node>
        <node concept="3clFbH" id="71CofnspEaN" role="3cqZAp" />
        <node concept="1X3_iC" id="5KGsxZrwmYY" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="34ab3g" id="71CofnsmPyz" role="8Wnug">
            <property role="35gtTG" value="warn" />
            <node concept="3cpWs3" id="71CofnsmPHS" role="34bqiv">
              <node concept="37vLTw" id="71CofnsmPIO" role="3uHU7w">
                <ref role="3cqZAo" node="79Do2olGLGf" resolve="condition" />
              </node>
              <node concept="Xl_RD" id="71CofnsmPy_" role="3uHU7B">
                <property role="Xl_RC" value="Could not parse condition " />
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="5KGsxZrwmPc" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3cpWs6" id="79Do2olGLR7" role="8Wnug">
            <node concept="10Nm6u" id="79Do2olGLWf" role="3cqZAk" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="79Do2olGLFQ" role="1B3o_S" />
      <node concept="17QB3L" id="79Do2olGLG8" role="3clF45" />
      <node concept="37vLTG" id="79Do2olGLGf" role="3clF46">
        <property role="TrG5h" value="condition" />
        <node concept="17QB3L" id="79Do2olGLGe" role="1tU5fm" />
      </node>
    </node>
    <node concept="2YIFZL" id="79Do2olGQZJ" role="jymVt">
      <property role="TrG5h" value="negate" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="79Do2olGQZM" role="3clF47">
        <node concept="3cpWs6" id="79Do2olGR1s" role="3cqZAp">
          <node concept="3cpWs3" id="79Do2olGRbI" role="3cqZAk">
            <node concept="Xl_RD" id="79Do2olGRcU" role="3uHU7w">
              <property role="Xl_RC" value=")" />
            </node>
            <node concept="3cpWs3" id="79Do2olGR6b" role="3uHU7B">
              <node concept="Xl_RD" id="79Do2olGR1Y" role="3uHU7B">
                <property role="Xl_RC" value="(not " />
              </node>
              <node concept="37vLTw" id="79Do2olGR6X" role="3uHU7w">
                <ref role="3cqZAo" node="79Do2olGR12" resolve="condition" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="79Do2olGQYi" role="1B3o_S" />
      <node concept="17QB3L" id="79Do2olGQZH" role="3clF45" />
      <node concept="37vLTG" id="79Do2olGR12" role="3clF46">
        <property role="TrG5h" value="condition" />
        <node concept="17QB3L" id="79Do2olGR11" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="79Do2olGLFB" role="1B3o_S" />
  </node>
  <node concept="13h7C7" id="79Do2olGNc$">
    <ref role="13h7C2" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
    <node concept="13i0hz" id="79Do2olGNcB" role="13h7CS">
      <property role="TrG5h" value="buildConstraints" />
      <node concept="3Tm1VV" id="79Do2olGNcC" role="1B3o_S" />
      <node concept="3clFbS" id="79Do2olGNcD" role="3clF47">
        <node concept="3cpWs8" id="79Do2olGNdr" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olGNdu" role="3cpWs9">
            <property role="TrG5h" value="condition" />
            <node concept="17QB3L" id="79Do2olGNdq" role="1tU5fm" />
            <node concept="2OqwBi" id="79Do2olGNg0" role="33vP2m">
              <node concept="13iPFW" id="79Do2olGNeo" role="2Oq$k0" />
              <node concept="3TrcHB" id="79Do2olGNlc" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:3fq3ZRIT59L" resolve="condition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olGNdM" role="3cqZAp" />
        <node concept="3cpWs8" id="79Do2olGNsx" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olGNs$" role="3cpWs9">
            <property role="TrG5h" value="parts" />
            <node concept="_YKpA" id="79Do2olGNst" role="1tU5fm">
              <node concept="17QB3L" id="79Do2olGNtW" role="_ZDj9" />
            </node>
            <node concept="2ShNRf" id="79Do2olGNuI" role="33vP2m">
              <node concept="Tc6Ow" id="79Do2olGNuE" role="2ShVmc">
                <node concept="17QB3L" id="79Do2olGNuF" role="HW$YZ" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olGOg2" role="3cqZAp" />
        <node concept="3cpWs8" id="79Do2olGOxs" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olGOxv" role="3cpWs9">
            <property role="TrG5h" value="macroIf" />
            <node concept="3Tqbb2" id="79Do2olGOxq" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
            </node>
            <node concept="10QFUN" id="79Do2olGOL$" role="33vP2m">
              <node concept="2OqwBi" id="79Do2olGOA5" role="10QFUP">
                <node concept="13iPFW" id="79Do2olGO$r" role="2Oq$k0" />
                <node concept="1mfA1w" id="79Do2olGOIf" role="2OqNvi" />
              </node>
              <node concept="3Tqbb2" id="79Do2olGOL_" role="10QFUM">
                <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olGOuO" role="3cqZAp" />
        <node concept="3clFbH" id="79Do2olHFm6" role="3cqZAp" />
        <node concept="3clFbF" id="79Do2olGOTr" role="3cqZAp">
          <node concept="2OqwBi" id="79Do2olGP3O" role="3clFbG">
            <node concept="37vLTw" id="79Do2olGOTp" role="2Oq$k0">
              <ref role="3cqZAo" node="79Do2olGNs$" resolve="parts" />
            </node>
            <node concept="TSZUe" id="79Do2olGPmd" role="2OqNvi">
              <node concept="2YIFZM" id="79Do2olGRkD" role="25WWJ7">
                <ref role="37wK5l" node="79Do2olGQZJ" resolve="negate" />
                <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                <node concept="2YIFZM" id="79Do2olGRoP" role="37wK5m">
                  <ref role="37wK5l" node="79Do2olGLFV" resolve="parseCondition" />
                  <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                  <node concept="2OqwBi" id="79Do2olGRtE" role="37wK5m">
                    <node concept="37vLTw" id="79Do2olGRqm" role="2Oq$k0">
                      <ref role="3cqZAo" node="79Do2olGOxv" resolve="macroIf" />
                    </node>
                    <node concept="3TrcHB" id="79Do2olGR_N" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olGWw2" role="3cqZAp" />
        <node concept="1DcWWT" id="79Do2olGWTK" role="3cqZAp">
          <node concept="3clFbS" id="79Do2olGWTM" role="2LFqv$">
            <node concept="3clFbF" id="79Do2olH3rz" role="3cqZAp">
              <node concept="2OqwBi" id="79Do2olH3r$" role="3clFbG">
                <node concept="37vLTw" id="79Do2olH3r_" role="2Oq$k0">
                  <ref role="3cqZAo" node="79Do2olGNs$" resolve="parts" />
                </node>
                <node concept="TSZUe" id="79Do2olH3rA" role="2OqNvi">
                  <node concept="2YIFZM" id="79Do2olH3rF" role="25WWJ7">
                    <ref role="37wK5l" node="79Do2olGQZJ" resolve="negate" />
                    <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                    <node concept="2YIFZM" id="79Do2olH3rG" role="37wK5m">
                      <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
                      <ref role="37wK5l" node="79Do2olGLFV" resolve="parseCondition" />
                      <node concept="2OqwBi" id="79Do2olH3rH" role="37wK5m">
                        <node concept="37vLTw" id="79Do2olH3uS" role="2Oq$k0">
                          <ref role="3cqZAo" node="79Do2olGWTN" resolve="elseIf" />
                        </node>
                        <node concept="3TrcHB" id="79Do2olH3$X" role="2OqNvi">
                          <ref role="3TsBF5" to="4s7a:3fq3ZRIT59L" resolve="condition" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="79Do2olGWTN" role="1Duv9x">
            <property role="TrG5h" value="elseIf" />
            <node concept="3Tqbb2" id="79Do2olGX3O" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
            </node>
          </node>
          <node concept="2OqwBi" id="79Do2olGY60" role="1DdaDG">
            <node concept="2OqwBi" id="79Do2olGXdT" role="2Oq$k0">
              <node concept="37vLTw" id="79Do2olGX7W" role="2Oq$k0">
                <ref role="3cqZAo" node="79Do2olGOxv" resolve="macroIf" />
              </node>
              <node concept="3Tsc0h" id="79Do2olGXmS" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
              </node>
            </node>
            <node concept="3b24QK" id="79Do2olGYXD" role="2OqNvi">
              <node concept="2OqwBi" id="79Do2olH11A" role="3b24Re">
                <node concept="2OqwBi" id="79Do2olGZTa" role="2Oq$k0">
                  <node concept="37vLTw" id="79Do2olGZzS" role="2Oq$k0">
                    <ref role="3cqZAo" node="79Do2olGOxv" resolve="macroIf" />
                  </node>
                  <node concept="3Tsc0h" id="79Do2olH0ju" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                  </node>
                </node>
                <node concept="2WmjW8" id="79Do2olH222" role="2OqNvi">
                  <node concept="13iPFW" id="79Do2olH2mj" role="25WWJ7" />
                </node>
              </node>
              <node concept="3cmrfG" id="79Do2olGZgw" role="3b24Rf">
                <property role="3cmrfH" value="0" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olGW_A" role="3cqZAp" />
        <node concept="3cpWs8" id="79Do2olGNmO" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olGNmR" role="3cpWs9">
            <property role="TrG5h" value="parsedCondition" />
            <node concept="17QB3L" id="79Do2olGNmM" role="1tU5fm" />
            <node concept="2YIFZM" id="79Do2olGNo$" role="33vP2m">
              <ref role="37wK5l" node="79Do2olGLFV" resolve="parseCondition" />
              <ref role="1Pybhc" node="79Do2olGLFA" resolve="ConditionHelper" />
              <node concept="37vLTw" id="79Do2olGNoS" role="37wK5m">
                <ref role="3cqZAo" node="79Do2olGNdu" resolve="condition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="79Do2olGNvg" role="3cqZAp">
          <node concept="2OqwBi" id="79Do2olGNBW" role="3clFbG">
            <node concept="37vLTw" id="79Do2olGNve" role="2Oq$k0">
              <ref role="3cqZAo" node="79Do2olGNs$" resolve="parts" />
            </node>
            <node concept="TSZUe" id="79Do2olGOck" role="2OqNvi">
              <node concept="37vLTw" id="79Do2olGOdB" role="25WWJ7">
                <ref role="3cqZAo" node="79Do2olGNmR" resolve="parsedCondition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olGNpd" role="3cqZAp" />
        <node concept="3cpWs8" id="79Do2olHGAm" role="3cqZAp">
          <node concept="3cpWsn" id="79Do2olHGAp" role="3cpWs9">
            <property role="TrG5h" value="smtString" />
            <node concept="17QB3L" id="79Do2olHGAk" role="1tU5fm" />
            <node concept="3cpWs3" id="79Do2olHK7N" role="33vP2m">
              <node concept="Xl_RD" id="79Do2olHK9O" role="3uHU7w">
                <property role="Xl_RC" value=")" />
              </node>
              <node concept="3cpWs3" id="79Do2olHIGD" role="3uHU7B">
                <node concept="Xl_RD" id="79Do2olHICj" role="3uHU7B">
                  <property role="Xl_RC" value="(and " />
                </node>
                <node concept="2OqwBi" id="79Do2olHIP_" role="3uHU7w">
                  <node concept="37vLTw" id="79Do2olHIGY" role="2Oq$k0">
                    <ref role="3cqZAo" node="79Do2olGNs$" resolve="parts" />
                  </node>
                  <node concept="3uJxvA" id="79Do2olHJq4" role="2OqNvi">
                    <node concept="Xl_RD" id="79Do2olHJMd" role="3uJOhx">
                      <property role="Xl_RC" value=" " />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="79Do2olHDMO" role="3cqZAp" />
        <node concept="3cpWs6" id="79Do2olGNdZ" role="3cqZAp">
          <node concept="37vLTw" id="79Do2olHKD_" role="3cqZAk">
            <ref role="3cqZAo" node="79Do2olHGAp" resolve="smtString" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="79Do2olGNcJ" role="3clF45" />
    </node>
    <node concept="13i0hz" id="7Y4iKHGsAyd" role="13h7CS">
      <property role="TrG5h" value="isSAT" />
      <node concept="3Tm1VV" id="7Y4iKHGsAye" role="1B3o_S" />
      <node concept="3clFbS" id="7Y4iKHGsAyf" role="3clF47">
        <node concept="3cpWs8" id="7Y4iKHGsAyg" role="3cqZAp">
          <node concept="3cpWsn" id="7Y4iKHGsAyh" role="3cpWs9">
            <property role="TrG5h" value="smt" />
            <node concept="17QB3L" id="7Y4iKHGsAyi" role="1tU5fm" />
            <node concept="3cpWs3" id="7Y4iKHGsAyj" role="33vP2m">
              <node concept="Xl_RD" id="7Y4iKHGsAyk" role="3uHU7w">
                <property role="Xl_RC" value=")\n" />
              </node>
              <node concept="3cpWs3" id="7Y4iKHGsAyl" role="3uHU7B">
                <node concept="Xl_RD" id="7Y4iKHGsAym" role="3uHU7B">
                  <property role="Xl_RC" value="(assert " />
                </node>
                <node concept="2OqwBi" id="7Y4iKHGsAyn" role="3uHU7w">
                  <node concept="13iPFW" id="7Y4iKHGsAyo" role="2Oq$k0" />
                  <node concept="2qgKlT" id="7Y4iKHGsBxD" role="2OqNvi">
                    <ref role="37wK5l" node="79Do2olGNcB" resolve="buildConstraints" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7Y4iKHGsAyq" role="3cqZAp" />
        <node concept="3clFbJ" id="7Y4iKHGsAyr" role="3cqZAp">
          <node concept="3clFbS" id="7Y4iKHGsAys" role="3clFbx">
            <node concept="3clFbF" id="7Y4iKHGsAyt" role="3cqZAp">
              <node concept="d57v9" id="7Y4iKHGsAyu" role="3clFbG">
                <node concept="3cpWs3" id="7Y4iKHGsAyv" role="37vLTx">
                  <node concept="Xl_RD" id="7Y4iKHGsAyw" role="3uHU7w">
                    <property role="Xl_RC" value=")\n" />
                  </node>
                  <node concept="3cpWs3" id="7Y4iKHGsAyx" role="3uHU7B">
                    <node concept="Xl_RD" id="7Y4iKHGsAyy" role="3uHU7B">
                      <property role="Xl_RC" value="(assert " />
                    </node>
                    <node concept="2OqwBi" id="7Y4iKHGsAyz" role="3uHU7w">
                      <node concept="2OqwBi" id="7Y4iKHGsAy$" role="2Oq$k0">
                        <node concept="13iPFW" id="7Y4iKHGsAy_" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="7Y4iKHGsAyA" role="2OqNvi">
                          <node concept="1xMEDy" id="7Y4iKHGsAyB" role="1xVPHs">
                            <node concept="chp4Y" id="7Y4iKHGsAyC" role="ri$Ld">
                              <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3TrcHB" id="7Y4iKHGsAyD" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="7Y4iKHGsAyE" role="37vLTJ">
                  <ref role="3cqZAo" node="7Y4iKHGsAyh" resolve="smt" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="7Y4iKHGsAyF" role="3clFbw">
            <node concept="2OqwBi" id="7Y4iKHGsAyG" role="2Oq$k0">
              <node concept="2OqwBi" id="7Y4iKHGsAyH" role="2Oq$k0">
                <node concept="13iPFW" id="7Y4iKHGsAyI" role="2Oq$k0" />
                <node concept="2Xjw5R" id="7Y4iKHGsAyJ" role="2OqNvi">
                  <node concept="1xMEDy" id="7Y4iKHGsAyK" role="1xVPHs">
                    <node concept="chp4Y" id="7Y4iKHGsAyL" role="ri$Ld">
                      <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3TrcHB" id="7Y4iKHGsAyM" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:79Do2olI3d1" resolve="constraints" />
              </node>
            </node>
            <node concept="17RvpY" id="7Y4iKHGsAyN" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="7Y4iKHGsAyO" role="3cqZAp">
          <node concept="2YIFZM" id="7Y4iKHGsAyP" role="3cqZAk">
            <ref role="37wK5l" node="79Do2olIt_i" resolve="solve" />
            <ref role="1Pybhc" node="79Do2olI4S$" resolve="MySolver" />
            <node concept="37vLTw" id="7Y4iKHGsAyQ" role="37wK5m">
              <ref role="3cqZAo" node="7Y4iKHGsAyh" resolve="smt" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10P_77" id="7Y4iKHGsAyR" role="3clF45" />
    </node>
    <node concept="13hLZK" id="79Do2olGNc_" role="13h7CW">
      <node concept="3clFbS" id="79Do2olGNcA" role="2VODD2" />
    </node>
  </node>
  <node concept="312cEu" id="79Do2olI4S$">
    <property role="TrG5h" value="MySolver" />
    <node concept="1X3_iC" id="4P2s9PP_eKp" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="Wx3nA" id="6fzU_SFDkfe" role="8Wnug">
        <property role="2dlcS1" value="false" />
        <property role="2dld4O" value="false" />
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="false" />
        <node concept="3uibUv" id="6fzU_SFDkgJ" role="1tU5fm">
          <ref role="3uigEE" to="f7eu:~Context" resolve="Context" />
        </node>
        <node concept="2ShNRf" id="6fzU_SFDkho" role="33vP2m">
          <node concept="1pGfFk" id="6fzU_SFDlpt" role="2ShVmc">
            <ref role="37wK5l" to="f7eu:~Context.&lt;init&gt;()" resolve="Context" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1X3_iC" id="4P2s9PP_eKq" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="Wx3nA" id="6fzU_SFDlrl" role="8Wnug">
        <property role="TrG5h" value="solver" />
        <node concept="3Tm6S6" id="6fzU_SFDlrn" role="1B3o_S" />
        <node concept="3uibUv" id="6fzU_SFDlt2" role="1tU5fm">
          <ref role="3uigEE" to="f7eu:~Solver" resolve="Solver" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="6fzU_SFDlU1" role="jymVt" />
    <node concept="2YIFZL" id="79Do2olIt_i" role="jymVt">
      <property role="TrG5h" value="solve" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="79Do2olIt_l" role="3clF47">
        <node concept="1X3_iC" id="4P2s9PP_e$B" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbJ" id="6fzU_SFDlXx" role="8Wnug">
            <node concept="3clFbS" id="6fzU_SFDlXz" role="3clFbx">
              <node concept="3clFbF" id="6fzU_SFDm8R" role="3cqZAp">
                <node concept="37vLTI" id="6fzU_SFDmc1" role="3clFbG">
                  <node concept="2OqwBi" id="6fzU_SFDmdB" role="37vLTx">
                    <node concept="37vLTw" id="6fzU_SFDmcA" role="2Oq$k0">
                      <ref role="3cqZAo" node="6fzU_SFDkfe" resolve="ctx" />
                    </node>
                    <node concept="liA8E" id="6fzU_SFDmgK" role="2OqNvi">
                      <ref role="37wK5l" to="f7eu:~Context.mkSolver():com.microsoft.z3.Solver" resolve="mkSolver" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="6fzU_SFDm8P" role="37vLTJ">
                    <ref role="3cqZAo" node="6fzU_SFDlrl" resolve="solver" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbC" id="6fzU_SFDm4a" role="3clFbw">
              <node concept="10Nm6u" id="6fzU_SFDm8h" role="3uHU7w" />
              <node concept="37vLTw" id="6fzU_SFDlYY" role="3uHU7B">
                <ref role="3cqZAo" node="6fzU_SFDlrl" resolve="solver" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="4P2s9PP_e$C" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbF" id="6fzU_SFDmtd" role="8Wnug">
            <node concept="2OqwBi" id="6fzU_SFDmzO" role="3clFbG">
              <node concept="37vLTw" id="6fzU_SFDmtb" role="2Oq$k0">
                <ref role="3cqZAo" node="6fzU_SFDlrl" resolve="solver" />
              </node>
              <node concept="liA8E" id="6fzU_SFDmEQ" role="2OqNvi">
                <ref role="37wK5l" to="f7eu:~Solver.reset():void" resolve="reset" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="6fzU_SFDmn9" role="3cqZAp" />
        <node concept="3cpWs8" id="750lIJWGgg0" role="3cqZAp">
          <node concept="3cpWsn" id="750lIJWGgg1" role="3cpWs9">
            <property role="TrG5h" value="sb" />
            <node concept="3uibUv" id="750lIJWGgg2" role="1tU5fm">
              <ref role="3uigEE" to="wyt6:~StringBuilder" resolve="StringBuilder" />
            </node>
            <node concept="2ShNRf" id="750lIJWGgh6" role="33vP2m">
              <node concept="1pGfFk" id="750lIJWGgh5" role="2ShVmc">
                <ref role="37wK5l" to="wyt6:~StringBuilder.&lt;init&gt;()" resolve="StringBuilder" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="750lIJWGgia" role="3cqZAp">
          <node concept="2OqwBi" id="750lIJWGgk8" role="3clFbG">
            <node concept="37vLTw" id="750lIJWGgi8" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="750lIJWGgrm" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="750lIJWGgrN" role="37wK5m">
                <property role="Xl_RC" value="(declare-const EXTRUDERS Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="750lIJWGg_J" role="3cqZAp">
          <node concept="2OqwBi" id="750lIJWGg_K" role="3clFbG">
            <node concept="37vLTw" id="750lIJWGg_L" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="750lIJWGg_M" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="750lIJWGg_N" role="37wK5m">
                <property role="Xl_RC" value="(declare-const DUAL_X_CARRIAGE Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="750lIJWGgEC" role="3cqZAp">
          <node concept="2OqwBi" id="750lIJWGgED" role="3clFbG">
            <node concept="37vLTw" id="750lIJWGgEE" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="750lIJWGgEF" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="750lIJWGgEG" role="37wK5m">
                <property role="Xl_RC" value="(declare-const ABORT_ON_ENDSTOP_HIT_FEATURE_ENABLED Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="750lIJWGgK0" role="3cqZAp">
          <node concept="2OqwBi" id="750lIJWGgK1" role="3clFbG">
            <node concept="37vLTw" id="750lIJWGgK2" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="750lIJWGgK3" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="750lIJWGgK4" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_DUAL_ENDSTOPS Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="750lIJWGgVt" role="3cqZAp">
          <node concept="2OqwBi" id="750lIJWGgVu" role="3clFbG">
            <node concept="37vLTw" id="750lIJWGgVv" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="750lIJWGgVw" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="750lIJWGgVx" role="37wK5m">
                <property role="Xl_RC" value="(declare-const BABYSTEPPING Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspa5B" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspa5C" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspa5D" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspa5E" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspa5F" role="37wK5m">
                <property role="Xl_RC" value="(declare-const ADVANCE Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="71Cofnsp9Iu" role="3cqZAp" />
        <node concept="3clFbF" id="71CofnspaDA" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspaDB" role="3clFbG">
            <node concept="37vLTw" id="71CofnspaDC" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspaDD" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspaDE" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_LATE_ENABLE Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspaJD" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspaJE" role="3clFbG">
            <node concept="37vLTw" id="71CofnspaJF" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspaJG" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspaJH" role="37wK5m">
                <property role="Xl_RC" value="(declare-const BUILDROB_CLONE Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspb21" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspb22" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspb23" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspb24" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspb25" role="37wK5m">
                <property role="Xl_RC" value="(declare-const COREXY Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="71CofnspcFK" role="3cqZAp">
          <node concept="3SKdUq" id="71CofnspcFM" role="3SKWNk">
            <property role="3SKdUp" value="also int" />
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspbt5" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspbt6" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspbt7" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspbt8" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspbt9" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X_MIN_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspdB7" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspdB8" role="3clFbG">
            <node concept="37vLTw" id="71CofnspdB9" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspdBa" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspdBb" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X_MAX_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspeb5" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspeb6" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspeb7" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspeb8" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspeb9" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Y_MIN_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspeHf" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspeHg" role="3clFbG">
            <node concept="37vLTw" id="71CofnspeHh" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspeHi" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspeHj" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Y_MAX_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspf7f" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspf7g" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspf7h" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspf7i" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspf7j" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_MIN_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspfBP" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspfBQ" role="3clFbG">
            <node concept="37vLTw" id="71CofnspfBR" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspfBS" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspfBT" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_MAX_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="71Cofnspfp5" role="3cqZAp" />
        <node concept="3clFbF" id="71CofnspghZ" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspgi0" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspgi1" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspgi2" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspgi3" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_DUAL_STEPPER_DRIVERS Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspgEP" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspgEQ" role="3clFbG">
            <node concept="37vLTw" id="71CofnspgER" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspgES" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspgET" role="37wK5m">
                <property role="Xl_RC" value="(declare-const AT90USB Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnsphFH" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnsphFI" role="3clFbG">
            <node concept="37vLTw" id="71CofnsphFJ" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnsphFK" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnsphFL" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspi2i" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspi2j" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspi2k" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspi2l" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspi2m" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X2_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspiph" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspipi" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspipj" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspipk" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspipl" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Y_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspiV8" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspiV9" role="3clFbG">
            <node concept="37vLTw" id="71CofnspiVa" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspiVb" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspiVc" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspjkn" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspjko" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspjkp" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspjkq" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspjkr" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z2_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspjRG" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspjRH" role="3clFbG">
            <node concept="37vLTw" id="71CofnspjRI" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspjRJ" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspjRK" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E0_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspkiF" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspkiG" role="3clFbG">
            <node concept="37vLTw" id="71CofnspkiH" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspkiI" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspkiJ" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E1_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnsplqq" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnsplqr" role="3clFbG">
            <node concept="37vLTw" id="71Cofnsplqs" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnsplqt" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnsplqu" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E2_DIR_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="71CofnspnKO" role="3cqZAp" />
        <node concept="3clFbF" id="71CofnspmFx" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspmFy" role="3clFbG">
            <node concept="37vLTw" id="71CofnspmFz" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspmF$" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspmF_" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X_ENABLE_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspnmn" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspnmo" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspnmp" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspnmq" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspnmr" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X2_ENABLE_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspo8b" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspo8c" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspo8d" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspo8e" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspo8f" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Y_ENABLE_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspoO_" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspoOA" role="3clFbG">
            <node concept="37vLTw" id="71CofnspoOB" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspoOC" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspoOD" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_ENABLE_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnsppxP" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnsppxQ" role="3clFbG">
            <node concept="37vLTw" id="71CofnsppxR" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnsppxS" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnsppxT" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E0_ENABLE_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspqfR" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspqfS" role="3clFbG">
            <node concept="37vLTw" id="71CofnspqfT" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspqfU" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspqfV" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E1_ENABLE_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspqYF" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspqYG" role="3clFbG">
            <node concept="37vLTw" id="71CofnspqYH" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspqYI" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspqYJ" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E2_ENABLE_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnsprIh" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnsprIi" role="3clFbG">
            <node concept="37vLTw" id="71CofnsprIj" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnsprIk" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnsprIl" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X_STEP_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspsu_" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspsuA" role="3clFbG">
            <node concept="37vLTw" id="71CofnspsuB" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspsuC" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspsuD" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X2_STEP_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnsptk7" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnsptk8" role="3clFbG">
            <node concept="37vLTw" id="71Cofnsptk9" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnsptka" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnsptkb" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Y_STEP_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspu5X" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspu5Y" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspu5Z" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspu60" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspu61" role="37wK5m">
                <property role="Xl_RC" value="(declare-const Z_STEP_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspuAW" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspuAX" role="3clFbG">
            <node concept="37vLTw" id="71CofnspuAY" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspuAZ" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspuB0" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E0_STEP_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspv2c" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspv2d" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspv2e" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspv2f" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspv2g" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E1_STEP_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspv$3" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspv$4" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspv$5" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspv$6" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspv$7" role="37wK5m">
                <property role="Xl_RC" value="(declare-const E2_STEP_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspwoZ" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspwp0" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspwp1" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspwp2" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspwp3" role="37wK5m">
                <property role="Xl_RC" value="(declare-const TCCR0A Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspwVO" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspwVP" role="3clFbG">
            <node concept="37vLTw" id="71CofnspwVQ" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspwVR" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspwVS" role="37wK5m">
                <property role="Xl_RC" value="(declare-const WGM01 Bool)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspx8T" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspx8U" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspx8V" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspx8W" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspx8X" role="37wK5m">
                <property role="Xl_RC" value="(declare-const DIGIPOTSS_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71Cofnspy0u" role="3cqZAp">
          <node concept="2OqwBi" id="71Cofnspy0v" role="3clFbG">
            <node concept="37vLTw" id="71Cofnspy0w" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71Cofnspy0x" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71Cofnspy0y" role="37wK5m">
                <property role="Xl_RC" value="(declare-const X_MS1_PIN Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="71CofnspbSV" role="3cqZAp">
          <node concept="2OqwBi" id="71CofnspbSW" role="3clFbG">
            <node concept="37vLTw" id="71CofnspbSX" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="71CofnspbSY" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="71CofnspbSZ" role="37wK5m">
                <property role="Xl_RC" value="(declare-const VERSION Int)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="750lIJWGh4K" role="3cqZAp">
          <node concept="2OqwBi" id="750lIJWGh8D" role="3clFbG">
            <node concept="37vLTw" id="750lIJWGh4I" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="750lIJWGhsc" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="37vLTw" id="750lIJWGhsT" role="37wK5m">
                <ref role="3cqZAo" node="750lIJWGhmP" resolve="smtString" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4P2s9PP_nw_" role="3cqZAp">
          <node concept="2OqwBi" id="4P2s9PP_nAV" role="3clFbG">
            <node concept="37vLTw" id="4P2s9PP_nwz" role="2Oq$k0">
              <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
            </node>
            <node concept="liA8E" id="4P2s9PP_nI1" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
              <node concept="Xl_RD" id="4P2s9PP_nIB" role="37wK5m">
                <property role="Xl_RC" value="\n(check-sat)\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="750lIJWGgEa" role="3cqZAp" />
        <node concept="34ab3g" id="750lIJWGqKw" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="750lIJWGqVb" role="34bqiv">
            <node concept="2OqwBi" id="4AVDYxlFhT1" role="3uHU7w">
              <node concept="37vLTw" id="4AVDYxlFhQN" role="2Oq$k0">
                <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
              </node>
              <node concept="liA8E" id="4AVDYxlFi0u" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
              </node>
            </node>
            <node concept="Xl_RD" id="750lIJWGqKy" role="3uHU7B">
              <property role="Xl_RC" value="SMT: " />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="750lIJWGqEk" role="3cqZAp" />
        <node concept="1X3_iC" id="4P2s9PP_eFy" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbF" id="750lIJWGh$e" role="8Wnug">
            <node concept="2OqwBi" id="750lIJWGhBX" role="3clFbG">
              <node concept="37vLTw" id="6fzU_SFDmFu" role="2Oq$k0">
                <ref role="3cqZAo" node="6fzU_SFDlrl" resolve="solver" />
              </node>
              <node concept="liA8E" id="750lIJWGhFv" role="2OqNvi">
                <ref role="37wK5l" to="f7eu:~Solver.add(com.microsoft.z3.BoolExpr...):void" resolve="add" />
                <node concept="2OqwBi" id="750lIJWGhHv" role="37wK5m">
                  <node concept="37vLTw" id="6fzU_SFDmGI" role="2Oq$k0">
                    <ref role="3cqZAo" node="6fzU_SFDkfe" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="750lIJWGhKz" role="2OqNvi">
                    <ref role="37wK5l" to="f7eu:~Context.parseSMTLIB2String(java.lang.String,com.microsoft.z3.Symbol[],com.microsoft.z3.Sort[],com.microsoft.z3.Symbol[],com.microsoft.z3.FuncDecl[]):com.microsoft.z3.BoolExpr" resolve="parseSMTLIB2String" />
                    <node concept="2OqwBi" id="750lIJWGhO6" role="37wK5m">
                      <node concept="37vLTw" id="750lIJWGhLF" role="2Oq$k0">
                        <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
                      </node>
                      <node concept="liA8E" id="750lIJWGhWa" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                      </node>
                    </node>
                    <node concept="10Nm6u" id="750lIJWGhYW" role="37wK5m" />
                    <node concept="10Nm6u" id="750lIJWGi5s" role="37wK5m" />
                    <node concept="10Nm6u" id="750lIJWGi8X" role="37wK5m" />
                    <node concept="10Nm6u" id="750lIJWGicV" role="37wK5m" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="4P2s9PP_eFz" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbH" id="750lIJWGg$H" role="8Wnug" />
        </node>
        <node concept="1X3_iC" id="4P2s9PP_eF$" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbJ" id="750lIJWGiXR" role="8Wnug">
            <node concept="3clFbS" id="750lIJWGiXT" role="3clFbx">
              <node concept="3cpWs6" id="750lIJWGjug" role="3cqZAp">
                <node concept="3clFbT" id="750lIJWGjuD" role="3cqZAk">
                  <property role="3clFbU" value="true" />
                </node>
              </node>
            </node>
            <node concept="3clFbC" id="750lIJWGjiQ" role="3clFbw">
              <node concept="Rm8GO" id="750lIJWGjoX" role="3uHU7w">
                <ref role="Rm8GQ" to="f7eu:~Status.SATISFIABLE" resolve="SATISFIABLE" />
                <ref role="1Px2BO" to="f7eu:~Status" resolve="Status" />
              </node>
              <node concept="2OqwBi" id="750lIJWGj7Z" role="3uHU7B">
                <node concept="37vLTw" id="6fzU_SFDmJ0" role="2Oq$k0">
                  <ref role="3cqZAo" node="6fzU_SFDlrl" resolve="solver" />
                </node>
                <node concept="liA8E" id="750lIJWGjeX" role="2OqNvi">
                  <ref role="37wK5l" to="f7eu:~Solver.check():com.microsoft.z3.Status" resolve="check" />
                </node>
              </node>
            </node>
            <node concept="9aQIb" id="750lIJWGjzm" role="9aQIa">
              <node concept="3clFbS" id="750lIJWGjzn" role="9aQI4">
                <node concept="3cpWs6" id="750lIJWGjC6" role="3cqZAp">
                  <node concept="3clFbT" id="750lIJWGjCw" role="3cqZAk">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PP_kCT" role="3cqZAp" />
        <node concept="3cpWs8" id="1JUF0Zp_89t" role="3cqZAp">
          <node concept="3cpWsn" id="1JUF0Zp_89u" role="3cpWs9">
            <property role="TrG5h" value="smtfile" />
            <node concept="3uibUv" id="1JUF0Zp_89v" role="1tU5fm">
              <ref role="3uigEE" to="guwi:~File" resolve="File" />
            </node>
            <node concept="2ShNRf" id="1JUF0Zp_944" role="33vP2m">
              <node concept="1pGfFk" id="1JUF0Zp_93F" role="2ShVmc">
                <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                <node concept="2OqwBi" id="6R47OjlPMu6" role="37wK5m">
                  <node concept="2YIFZM" id="6R47OjlPMu7" role="2Oq$k0">
                    <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                    <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                  </node>
                  <node concept="liA8E" id="6R47OjlPMu8" role="2OqNvi">
                    <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                    <node concept="Xl_RD" id="6R47OjlPMu9" role="37wK5m">
                      <property role="Xl_RC" value="${TempFolder}" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="6R47OjlPPmp" role="37wK5m">
                  <property role="Xl_RC" value="smt.txt" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1JUF0Zp_6Gf" role="3cqZAp" />
        <node concept="SfApY" id="4P2s9PP_kzk" role="3cqZAp">
          <node concept="3clFbS" id="4P2s9PP_kzm" role="SfCbr">
            <node concept="34ab3g" id="1JUF0Zp_bpH" role="3cqZAp">
              <property role="35gtTG" value="warn" />
              <node concept="3cpWs3" id="1JUF0Zp_c1M" role="34bqiv">
                <node concept="2OqwBi" id="1JUF0Zp_cnj" role="3uHU7w">
                  <node concept="37vLTw" id="1JUF0Zp_c7J" role="2Oq$k0">
                    <ref role="3cqZAo" node="1JUF0Zp_89u" resolve="smtfile" />
                  </node>
                  <node concept="liA8E" id="1JUF0Zp_cSS" role="2OqNvi">
                    <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
                  </node>
                </node>
                <node concept="Xl_RD" id="1JUF0Zp_bpJ" role="3uHU7B">
                  <property role="Xl_RC" value="Write smt to " />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="4P2s9PP_fU0" role="3cqZAp">
              <node concept="3cpWsn" id="4P2s9PP_fU1" role="3cpWs9">
                <property role="TrG5h" value="pw" />
                <node concept="3uibUv" id="4P2s9PP_fU2" role="1tU5fm">
                  <ref role="3uigEE" to="guwi:~PrintWriter" resolve="PrintWriter" />
                </node>
                <node concept="2ShNRf" id="4P2s9PP_fYs" role="33vP2m">
                  <node concept="1pGfFk" id="4P2s9PP_h94" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~PrintWriter.&lt;init&gt;(java.io.File,java.lang.String)" resolve="PrintWriter" />
                    <node concept="37vLTw" id="1JUF0Zp_9n_" role="37wK5m">
                      <ref role="3cqZAo" node="1JUF0Zp_89u" resolve="smtfile" />
                    </node>
                    <node concept="Xl_RD" id="4P2s9PP_htk" role="37wK5m">
                      <property role="Xl_RC" value="UTF-8" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4P2s9PP_hwk" role="3cqZAp">
              <node concept="2OqwBi" id="4P2s9PP_h_Y" role="3clFbG">
                <node concept="37vLTw" id="4P2s9PP_hwi" role="2Oq$k0">
                  <ref role="3cqZAo" node="4P2s9PP_fU1" resolve="pw" />
                </node>
                <node concept="liA8E" id="4P2s9PP_hMt" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintWriter.print(java.lang.String):void" resolve="print" />
                  <node concept="2OqwBi" id="4P2s9PP_hOU" role="37wK5m">
                    <node concept="37vLTw" id="4P2s9PP_hN4" role="2Oq$k0">
                      <ref role="3cqZAo" node="750lIJWGgg1" resolve="sb" />
                    </node>
                    <node concept="liA8E" id="4P2s9PP_hX4" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4P2s9PP_i7j" role="3cqZAp">
              <node concept="2OqwBi" id="4P2s9PP_id$" role="3clFbG">
                <node concept="37vLTw" id="4P2s9PP_i7h" role="2Oq$k0">
                  <ref role="3cqZAo" node="4P2s9PP_fU1" resolve="pw" />
                </node>
                <node concept="liA8E" id="4P2s9PP_iqx" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintWriter.close():void" resolve="close" />
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="4P2s9PP_kzn" role="TEbGg">
            <node concept="3cpWsn" id="4P2s9PP_kzp" role="TDEfY">
              <property role="TrG5h" value="e" />
              <node concept="3uibUv" id="4P2s9PP_kXJ" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
              </node>
            </node>
            <node concept="3clFbS" id="4P2s9PP_kzt" role="TDEfX">
              <node concept="34ab3g" id="4P2s9PP_l0z" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="4P2s9PP_l0_" role="34bqiv" />
                <node concept="37vLTw" id="4P2s9PP_l0B" role="34bMjA">
                  <ref role="3cqZAo" node="4P2s9PP_kzp" resolve="e" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PP_ktG" role="3cqZAp" />
        <node concept="3cpWs8" id="4P2s9PPAAgi" role="3cqZAp">
          <node concept="3cpWsn" id="4P2s9PPAAgl" role="3cpWs9">
            <property role="TrG5h" value="result" />
            <node concept="10P_77" id="4P2s9PPAAgg" role="1tU5fm" />
            <node concept="3clFbT" id="4P2s9PPAAtb" role="33vP2m">
              <property role="3clFbU" value="false" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PPAA3S" role="3cqZAp" />
        <node concept="SfApY" id="4P2s9PP_raI" role="3cqZAp">
          <node concept="3clFbS" id="4P2s9PP_raK" role="SfCbr">
            <node concept="3cpWs8" id="1JUF0Zp$Kon" role="3cqZAp">
              <node concept="3cpWsn" id="1JUF0Zp$Koq" role="3cpWs9">
                <property role="TrG5h" value="z3Path" />
                <node concept="17QB3L" id="1JUF0Zp$Kol" role="1tU5fm" />
                <node concept="2OqwBi" id="1JUF0Zp$LqJ" role="33vP2m">
                  <node concept="2YIFZM" id="1JUF0Zp$LqK" role="2Oq$k0">
                    <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                    <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                  </node>
                  <node concept="liA8E" id="1JUF0Zp$LqL" role="2OqNvi">
                    <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                    <node concept="Xl_RD" id="1JUF0Zp$LqM" role="37wK5m">
                      <property role="Xl_RC" value="${Z3Path}" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="1JUF0Zp$Mb_" role="3cqZAp">
              <node concept="3clFbS" id="1JUF0Zp$MbB" role="3clFbx">
                <node concept="YS8fn" id="1JUF0Zp$SZC" role="3cqZAp">
                  <node concept="2ShNRf" id="1JUF0Zp$T3y" role="YScLw">
                    <node concept="1pGfFk" id="1JUF0Zp$Tr0" role="2ShVmc">
                      <ref role="37wK5l" to="wyt6:~IllegalArgumentException.&lt;init&gt;(java.lang.String)" resolve="IllegalArgumentException" />
                      <node concept="3cpWs3" id="1JUF0Zp$VvB" role="37wK5m">
                        <node concept="Xl_RD" id="1JUF0Zp$Vwb" role="3uHU7w">
                          <property role="Xl_RC" value=". Check config!" />
                        </node>
                        <node concept="3cpWs3" id="1JUF0Zp$Uxo" role="3uHU7B">
                          <node concept="Xl_RD" id="1JUF0Zp$T$7" role="3uHU7B">
                            <property role="Xl_RC" value="Can not find z3 at " />
                          </node>
                          <node concept="37vLTw" id="1JUF0Zp$UDO" role="3uHU7w">
                            <ref role="3cqZAo" node="1JUF0Zp$Koq" resolve="z3Path" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3fqX7Q" id="1JUF0Zp$MqR" role="3clFbw">
                <node concept="2OqwBi" id="1JUF0Zp$SnY" role="3fr31v">
                  <node concept="2ShNRf" id="1JUF0Zp$Mrj" role="2Oq$k0">
                    <node concept="1pGfFk" id="1JUF0Zp$RY3" role="2ShVmc">
                      <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String)" resolve="File" />
                      <node concept="37vLTw" id="1JUF0Zp$S5B" role="37wK5m">
                        <ref role="3cqZAo" node="1JUF0Zp$Koq" resolve="z3Path" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="1JUF0Zp$SSx" role="2OqNvi">
                    <ref role="37wK5l" to="guwi:~File.exists():boolean" resolve="exists" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="34ab3g" id="1JUF0Zp$X_2" role="3cqZAp">
              <property role="35gtTG" value="warn" />
              <node concept="3cpWs3" id="1JUF0Zp$YMN" role="34bqiv">
                <node concept="37vLTw" id="1JUF0Zp$YSJ" role="3uHU7w">
                  <ref role="3cqZAo" node="1JUF0Zp$Koq" resolve="z3Path" />
                </node>
                <node concept="Xl_RD" id="1JUF0Zp$X_4" role="3uHU7B">
                  <property role="Xl_RC" value="z3path " />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="4P2s9PP_ofR" role="3cqZAp">
              <node concept="3cpWsn" id="4P2s9PP_ofS" role="3cpWs9">
                <property role="TrG5h" value="pb" />
                <node concept="3uibUv" id="4P2s9PP_ofT" role="1tU5fm">
                  <ref role="3uigEE" to="wyt6:~ProcessBuilder" resolve="ProcessBuilder" />
                </node>
                <node concept="2ShNRf" id="4P2s9PP_ont" role="33vP2m">
                  <node concept="1pGfFk" id="4P2s9PP_ozj" role="2ShVmc">
                    <ref role="37wK5l" to="wyt6:~ProcessBuilder.&lt;init&gt;(java.lang.String...)" resolve="ProcessBuilder" />
                    <node concept="37vLTw" id="1JUF0Zp$Xsq" role="37wK5m">
                      <ref role="3cqZAo" node="1JUF0Zp$Koq" resolve="z3Path" />
                    </node>
                    <node concept="Xl_RD" id="4P2s9PP_oS9" role="37wK5m">
                      <property role="Xl_RC" value="-smt2" />
                    </node>
                    <node concept="2OqwBi" id="1JUF0Zp_9UP" role="37wK5m">
                      <node concept="37vLTw" id="1JUF0Zp_9Bg" role="2Oq$k0">
                        <ref role="3cqZAo" node="1JUF0Zp_89u" resolve="smtfile" />
                      </node>
                      <node concept="liA8E" id="1JUF0Zp_aTX" role="2OqNvi">
                        <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="4P2s9PP_pIz" role="3cqZAp">
              <node concept="3cpWsn" id="4P2s9PP_pI$" role="3cpWs9">
                <property role="TrG5h" value="p" />
                <node concept="3uibUv" id="4P2s9PP_pI_" role="1tU5fm">
                  <ref role="3uigEE" to="wyt6:~Process" resolve="Process" />
                </node>
                <node concept="2OqwBi" id="4P2s9PP_q1A" role="33vP2m">
                  <node concept="37vLTw" id="4P2s9PP_q0J" role="2Oq$k0">
                    <ref role="3cqZAo" node="4P2s9PP_ofS" resolve="pb" />
                  </node>
                  <node concept="liA8E" id="4P2s9PP_q4i" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~ProcessBuilder.start():java.lang.Process" resolve="start" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="4P2s9PP_ps3" role="3cqZAp" />
            <node concept="3cpWs8" id="4P2s9PP_qnX" role="3cqZAp">
              <node concept="3cpWsn" id="4P2s9PP_qnY" role="3cpWs9">
                <property role="TrG5h" value="reader" />
                <node concept="3uibUv" id="4P2s9PP_qnZ" role="1tU5fm">
                  <ref role="3uigEE" to="guwi:~BufferedReader" resolve="BufferedReader" />
                </node>
                <node concept="2ShNRf" id="4P2s9PP_qu$" role="33vP2m">
                  <node concept="1pGfFk" id="4P2s9PP_qDZ" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~BufferedReader.&lt;init&gt;(java.io.Reader)" resolve="BufferedReader" />
                    <node concept="2ShNRf" id="4P2s9PP_qEv" role="37wK5m">
                      <node concept="1pGfFk" id="4P2s9PP_qQ4" role="2ShVmc">
                        <ref role="37wK5l" to="guwi:~InputStreamReader.&lt;init&gt;(java.io.InputStream)" resolve="InputStreamReader" />
                        <node concept="2OqwBi" id="4P2s9PP_qRz" role="37wK5m">
                          <node concept="37vLTw" id="4P2s9PP_qQH" role="2Oq$k0">
                            <ref role="3cqZAo" node="4P2s9PP_pI$" resolve="p" />
                          </node>
                          <node concept="liA8E" id="4P2s9PP_qUX" role="2OqNvi">
                            <ref role="37wK5l" to="wyt6:~Process.getInputStream():java.io.InputStream" resolve="getInputStream" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="4P2s9PP_rAW" role="3cqZAp">
              <node concept="3cpWsn" id="4P2s9PP_rAX" role="3cpWs9">
                <property role="TrG5h" value="s" />
                <node concept="3uibUv" id="4P2s9PP_rAY" role="1tU5fm">
                  <ref role="3uigEE" to="wyt6:~StringBuilder" resolve="StringBuilder" />
                </node>
                <node concept="2ShNRf" id="4P2s9PP_rFt" role="33vP2m">
                  <node concept="1pGfFk" id="4P2s9PP_rS3" role="2ShVmc">
                    <ref role="37wK5l" to="wyt6:~StringBuilder.&lt;init&gt;()" resolve="StringBuilder" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="4P2s9PP_rWP" role="3cqZAp">
              <node concept="3cpWsn" id="4P2s9PP_rWQ" role="3cpWs9">
                <property role="TrG5h" value="line" />
                <node concept="17QB3L" id="4P2s9PPARSQ" role="1tU5fm" />
              </node>
            </node>
            <node concept="2$JKZl" id="4P2s9PP_s4V" role="3cqZAp">
              <node concept="3clFbS" id="4P2s9PP_s4X" role="2LFqv$">
                <node concept="3clFbF" id="4P2s9PP_uqj" role="3cqZAp">
                  <node concept="2OqwBi" id="4P2s9PP_urE" role="3clFbG">
                    <node concept="37vLTw" id="4P2s9PP_uqi" role="2Oq$k0">
                      <ref role="3cqZAo" node="4P2s9PP_rAX" resolve="s" />
                    </node>
                    <node concept="liA8E" id="4P2s9PP_uxm" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~StringBuilder.append(java.lang.String):java.lang.StringBuilder" resolve="append" />
                      <node concept="37vLTw" id="4P2s9PP_uzG" role="37wK5m">
                        <ref role="3cqZAo" node="4P2s9PP_rWQ" resolve="line" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3y3z36" id="4P2s9PP_uc_" role="2$JKZa">
                <node concept="10Nm6u" id="4P2s9PP_uer" role="3uHU7w" />
                <node concept="1eOMI4" id="4P2s9PP_unb" role="3uHU7B">
                  <node concept="37vLTI" id="4P2s9PP_sf5" role="1eOMHV">
                    <node concept="37vLTw" id="4P2s9PP_s8n" role="37vLTJ">
                      <ref role="3cqZAo" node="4P2s9PP_rWQ" resolve="line" />
                    </node>
                    <node concept="2OqwBi" id="4P2s9PP_tYl" role="37vLTx">
                      <node concept="37vLTw" id="4P2s9PP_shH" role="2Oq$k0">
                        <ref role="3cqZAo" node="4P2s9PP_qnY" resolve="reader" />
                      </node>
                      <node concept="liA8E" id="4P2s9PP_u7L" role="2OqNvi">
                        <ref role="37wK5l" to="guwi:~BufferedReader.readLine():java.lang.String" resolve="readLine" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="4P2s9PP_ryA" role="3cqZAp" />
            <node concept="3clFbF" id="4P2s9PPASR1" role="3cqZAp">
              <node concept="37vLTI" id="4P2s9PPAT4P" role="3clFbG">
                <node concept="2OqwBi" id="4P2s9PPAWl1" role="37vLTx">
                  <node concept="2OqwBi" id="4P2s9PPAWl2" role="2Oq$k0">
                    <node concept="37vLTw" id="4P2s9PPB2CU" role="2Oq$k0">
                      <ref role="3cqZAo" node="4P2s9PP_rAX" resolve="s" />
                    </node>
                    <node concept="liA8E" id="4P2s9PPAWl4" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                    </node>
                  </node>
                  <node concept="liA8E" id="4P2s9PPAWl5" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                    <node concept="Xl_RD" id="4P2s9PPAWl6" role="37wK5m">
                      <property role="Xl_RC" value="sat" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="4P2s9PPASQZ" role="37vLTJ">
                  <ref role="3cqZAo" node="4P2s9PPAAgl" resolve="result" />
                </node>
              </node>
            </node>
            <node concept="34ab3g" id="4P2s9PP_v1u" role="3cqZAp">
              <property role="35gtTG" value="warn" />
              <node concept="3cpWs3" id="4P2s9PPABBm" role="34bqiv">
                <node concept="37vLTw" id="4P2s9PPABFq" role="3uHU7w">
                  <ref role="3cqZAo" node="4P2s9PPAAgl" resolve="result" />
                </node>
                <node concept="3cpWs3" id="4P2s9PPABrc" role="3uHU7B">
                  <node concept="3cpWs3" id="4P2s9PP_vb8" role="3uHU7B">
                    <node concept="Xl_RD" id="4P2s9PP_v1w" role="3uHU7B">
                      <property role="Xl_RC" value="'" />
                    </node>
                    <node concept="2OqwBi" id="4P2s9PP_vfI" role="3uHU7w">
                      <node concept="37vLTw" id="4P2s9PP_vdG" role="2Oq$k0">
                        <ref role="3cqZAo" node="4P2s9PP_rAX" resolve="s" />
                      </node>
                      <node concept="liA8E" id="4P2s9PP_vp8" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="4P2s9PPABsO" role="3uHU7w">
                    <property role="Xl_RC" value="' = " />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="4P2s9PP_raL" role="TEbGg">
            <node concept="3cpWsn" id="4P2s9PP_raN" role="TDEfY">
              <property role="TrG5h" value="e" />
              <node concept="3uibUv" id="4P2s9PP_rtB" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
              </node>
            </node>
            <node concept="3clFbS" id="4P2s9PP_raR" role="TDEfX">
              <node concept="34ab3g" id="4P2s9PP_rwY" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="4P2s9PP_rx0" role="34bqiv" />
                <node concept="37vLTw" id="4P2s9PP_rx2" role="34bMjA">
                  <ref role="3cqZAo" node="4P2s9PP_raN" resolve="e" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4P2s9PP_qWc" role="3cqZAp" />
        <node concept="3clFbH" id="4P2s9PP_qb0" role="3cqZAp" />
        <node concept="3cpWs6" id="4P2s9PP_eR2" role="3cqZAp">
          <node concept="37vLTw" id="4P2s9PPABcs" role="3cqZAk">
            <ref role="3cqZAo" node="4P2s9PPAAgl" resolve="result" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="79Do2olIt_b" role="1B3o_S" />
      <node concept="10P_77" id="750lIJWGjpw" role="3clF45" />
      <node concept="37vLTG" id="750lIJWGhmP" role="3clF46">
        <property role="TrG5h" value="smtString" />
        <node concept="17QB3L" id="750lIJWGhmO" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="79Do2olI4S_" role="1B3o_S" />
  </node>
  <node concept="13h7C7" id="2GhJDbDjFpv">
    <ref role="13h7C2" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="13hLZK" id="2GhJDbDjFpw" role="13h7CW">
      <node concept="3clFbS" id="2GhJDbDjFpx" role="2VODD2">
        <node concept="3clFbF" id="2GhJDbDjK5_" role="3cqZAp">
          <node concept="37vLTI" id="2GhJDbDjKjx" role="3clFbG">
            <node concept="3clFbT" id="2GhJDbDjKjV" role="37vLTx">
              <property role="3clFbU" value="true" />
            </node>
            <node concept="2OqwBi" id="2GhJDbDjK7e" role="37vLTJ">
              <node concept="13iPFW" id="2GhJDbDjK5$" role="2Oq$k0" />
              <node concept="3TrcHB" id="2GhJDbDjKef" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5KGsxZrsCXs">
    <property role="TrG5h" value="MyHelper" />
    <node concept="2tJIrI" id="2NOvNR_ltn4" role="jymVt" />
    <node concept="2YIFZL" id="2NOvNR_l$0S" role="jymVt">
      <property role="TrG5h" value="generateNode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2NOvNR_lyAN" role="3clF47">
        <node concept="3SKdUt" id="32JEsS3dbV$" role="3cqZAp">
          <node concept="3SKdUq" id="32JEsS3dbVA" role="3SKWNk">
            <property role="3SKdUp" value="Given the openapi.model.SNode parameter, we need to find the correct jetbrains.mps.smodel.SNode" />
          </node>
        </node>
        <node concept="3cpWs8" id="32JEsS3aS09" role="3cqZAp">
          <node concept="3cpWsn" id="32JEsS3aS0a" role="3cpWs9">
            <property role="TrG5h" value="model" />
            <node concept="3uibUv" id="32JEsS3aS0b" role="1tU5fm">
              <ref role="3uigEE" to="mhbf:~SModel" resolve="SModel" />
            </node>
            <node concept="2OqwBi" id="32JEsS3aSeo" role="33vP2m">
              <node concept="37vLTw" id="5KGsxZrsEz0" role="2Oq$k0">
                <ref role="3cqZAo" node="2NOvNR_lyEx" resolve="node" />
              </node>
              <node concept="I4A8Y" id="32JEsS3aSqs" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="32JEsS3cs9_" role="3cqZAp">
          <node concept="3cpWsn" id="32JEsS3cs9C" role="3cpWs9">
            <property role="TrG5h" value="rootNodes" />
            <node concept="_YKpA" id="32JEsS3cs9x" role="1tU5fm">
              <node concept="3uibUv" id="32JEsS3cwIn" role="_ZDj9">
                <ref role="3uigEE" to="w1kc:~SNode" resolve="SNode" />
              </node>
            </node>
            <node concept="2ShNRf" id="32JEsS3csCJ" role="33vP2m">
              <node concept="Tc6Ow" id="32JEsS3csCF" role="2ShVmc">
                <node concept="3uibUv" id="32JEsS3cwOY" role="HW$YZ">
                  <ref role="3uigEE" to="w1kc:~SNode" resolve="SNode" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="32JEsS3cAqK" role="3cqZAp">
          <node concept="2GrKxI" id="32JEsS3cAqM" role="2Gsz3X">
            <property role="TrG5h" value="rootNode" />
          </node>
          <node concept="3clFbS" id="32JEsS3cAqQ" role="2LFqv$">
            <node concept="3clFbF" id="32JEsS3cBLL" role="3cqZAp">
              <node concept="2OqwBi" id="32JEsS3cChp" role="3clFbG">
                <node concept="37vLTw" id="32JEsS3cBLK" role="2Oq$k0">
                  <ref role="3cqZAo" node="32JEsS3cs9C" resolve="rootNodes" />
                </node>
                <node concept="TSZUe" id="32JEsS3cDz9" role="2OqNvi">
                  <node concept="1eOMI4" id="32JEsS3cDBt" role="25WWJ7">
                    <node concept="10QFUN" id="32JEsS3cDBq" role="1eOMHV">
                      <node concept="3uibUv" id="32JEsS3cDDJ" role="10QFUM">
                        <ref role="3uigEE" to="w1kc:~SNode" resolve="SNode" />
                      </node>
                      <node concept="2GrUjf" id="32JEsS3cDId" role="10QFUP">
                        <ref role="2Gs0qQ" node="32JEsS3cAqM" resolve="rootNode" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="32JEsS3cBrx" role="2GsD0m">
            <node concept="liA8E" id="32JEsS3cBrz" role="2OqNvi">
              <ref role="37wK5l" to="mhbf:~SModel.getRootNodes():java.lang.Iterable" resolve="getRootNodes" />
            </node>
            <node concept="37vLTw" id="32JEsS3d2Vf" role="2Oq$k0">
              <ref role="3cqZAo" node="32JEsS3aS0a" resolve="model" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="32JEsS3cEhr" role="3cqZAp">
          <node concept="3cpWsn" id="32JEsS3cEhs" role="3cpWs9">
            <property role="TrG5h" value="snode" />
            <node concept="3uibUv" id="32JEsS3cEht" role="1tU5fm">
              <ref role="3uigEE" to="w1kc:~SNode" resolve="SNode" />
            </node>
            <node concept="2OqwBi" id="32JEsS3cFpK" role="33vP2m">
              <node concept="37vLTw" id="32JEsS3cEMg" role="2Oq$k0">
                <ref role="3cqZAo" node="32JEsS3cs9C" resolve="rootNodes" />
              </node>
              <node concept="34jXtK" id="32JEsS3cG3k" role="2OqNvi">
                <node concept="2OqwBi" id="32JEsS3cHOw" role="25WWJ7">
                  <node concept="2OqwBi" id="32JEsS3cGL6" role="2Oq$k0">
                    <node concept="2OqwBi" id="32JEsS3cGiE" role="2Oq$k0">
                      <node concept="37vLTw" id="5KGsxZrsECU" role="2Oq$k0">
                        <ref role="3cqZAo" node="2NOvNR_lyEx" resolve="node" />
                      </node>
                      <node concept="I4A8Y" id="32JEsS3cGyG" role="2OqNvi" />
                    </node>
                    <node concept="2RRcyG" id="32JEsS3cGU_" role="2OqNvi" />
                  </node>
                  <node concept="2WmjW8" id="32JEsS3cJH5" role="2OqNvi">
                    <node concept="2OqwBi" id="5KGsxZrsiRo" role="25WWJ7">
                      <node concept="37vLTw" id="5KGsxZrsEO8" role="2Oq$k0">
                        <ref role="3cqZAo" node="2NOvNR_lyEx" resolve="node" />
                      </node>
                      <node concept="2Rxl7S" id="5KGsxZrsjiq" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5lhzOSqExTi" role="3cqZAp" />
        <node concept="3clFbJ" id="32JEsS3bNQl" role="3cqZAp">
          <node concept="3clFbS" id="32JEsS3bNQn" role="3clFbx">
            <node concept="34ab3g" id="32JEsS3bPfR" role="3cqZAp">
              <property role="35gtTG" value="error" />
              <node concept="Xl_RD" id="32JEsS3bPfT" role="34bqiv">
                <property role="Xl_RC" value="Node not found" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="32JEsS3bSGc" role="3clFbw">
            <node concept="10Nm6u" id="32JEsS3bSQw" role="3uHU7w" />
            <node concept="37vLTw" id="32JEsS3cJQM" role="3uHU7B">
              <ref role="3cqZAo" node="32JEsS3cEhs" resolve="snode" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="32JEsS3bNuD" role="3cqZAp" />
        <node concept="3SKdUt" id="32JEsS3dh9U" role="3cqZAp">
          <node concept="3SKdUq" id="32JEsS3dh9W" role="3SKWNk">
            <property role="3SKdUp" value="Node must have a name, e.g., implement INamedConcept" />
          </node>
        </node>
        <node concept="3clFbJ" id="32JEsS3cTQ4" role="3cqZAp">
          <node concept="3clFbS" id="32JEsS3cTQ6" role="3clFbx">
            <node concept="34ab3g" id="32JEsS3cX2a" role="3cqZAp">
              <property role="35gtTG" value="error" />
              <node concept="Xl_RD" id="32JEsS3cX2c" role="34bqiv">
                <property role="Xl_RC" value="node must have a name" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="32JEsS3cWMN" role="3clFbw">
            <node concept="10Nm6u" id="32JEsS3cX0W" role="3uHU7w" />
            <node concept="2OqwBi" id="32JEsS3cVDF" role="3uHU7B">
              <node concept="37vLTw" id="32JEsS3d43j" role="2Oq$k0">
                <ref role="3cqZAo" node="32JEsS3cEhs" resolve="snode" />
              </node>
              <node concept="liA8E" id="32JEsS3cWjJ" role="2OqNvi">
                <ref role="37wK5l" to="w1kc:~SNode.getName():java.lang.String" resolve="getName" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="34h7Gze$3bk" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbF" id="34h7GzexV5v" role="8Wnug">
            <node concept="2OqwBi" id="34h7GzexW2f" role="3clFbG">
              <node concept="37vLTw" id="34h7GzexVMv" role="2Oq$k0">
                <ref role="3cqZAo" node="32JEsS3cEhs" resolve="snode" />
              </node>
              <node concept="liA8E" id="34h7GzexWkl" role="2OqNvi">
                <ref role="37wK5l" to="w1kc:~SNode.removeChild(org.jetbrains.mps.openapi.model.SNode):void" resolve="removeChild" />
                <node concept="2OqwBi" id="34h7GzexWE5" role="37wK5m">
                  <node concept="37vLTw" id="34h7GzexWpb" role="2Oq$k0">
                    <ref role="3cqZAo" node="32JEsS3cEhs" resolve="snode" />
                  </node>
                  <node concept="liA8E" id="34h7GzexWWp" role="2OqNvi">
                    <ref role="37wK5l" to="w1kc:~SNode.getLastChild():org.jetbrains.mps.openapi.model.SNode" resolve="getLastChild" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="32JEsS3bCCi" role="3cqZAp">
          <node concept="3cpWsn" id="32JEsS3bCCj" role="3cpWs9">
            <property role="TrG5h" value="text" />
            <node concept="17QB3L" id="32JEsS3bEUb" role="1tU5fm" />
            <node concept="2YIFZM" id="32JEsS3bCCl" role="33vP2m">
              <ref role="1Pybhc" to="ao3:~TextGeneratorEngine" resolve="TextGeneratorEngine" />
              <ref role="37wK5l" to="ao3:~TextGeneratorEngine.generateText(org.jetbrains.mps.openapi.model.SNode):java.lang.String" resolve="generateText" />
              <node concept="37vLTw" id="32JEsS3cJRW" role="37wK5m">
                <ref role="3cqZAo" node="32JEsS3cEhs" resolve="snode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2NOvNR_lyGq" role="3cqZAp" />
        <node concept="3cpWs6" id="2NOvNR_lz9q" role="3cqZAp">
          <node concept="37vLTw" id="2NOvNR_lzjo" role="3cqZAk">
            <ref role="3cqZAo" node="32JEsS3bCCj" resolve="text" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="2NOvNR_lyEx" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="2NOvNR_lyEy" role="1tU5fm" />
      </node>
      <node concept="17QB3L" id="2NOvNR_lyEM" role="3clF45" />
      <node concept="3Tm1VV" id="2NOvNR_l$_Q" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="5lhzOSqAQpX" role="jymVt" />
    <node concept="2YIFZL" id="5lhzOSqAQG6" role="jymVt">
      <property role="TrG5h" value="generateNodeButGivenOne" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5lhzOSqAQG9" role="3clF47">
        <node concept="3clFbH" id="34h7Gze31ti" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqAQQO" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqAQQP" role="3cpWs9">
            <property role="TrG5h" value="model" />
            <node concept="3uibUv" id="5lhzOSqBoUQ" role="1tU5fm">
              <ref role="3uigEE" to="mhbf:~SModel" resolve="SModel" />
            </node>
            <node concept="2OqwBi" id="5lhzOSqAR0h" role="33vP2m">
              <node concept="37vLTw" id="5lhzOSqAQSs" role="2Oq$k0">
                <ref role="3cqZAo" node="5lhzOSqAQMT" resolve="node" />
              </node>
              <node concept="I4A8Y" id="5lhzOSqAR5j" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5lhzOSqBo9n" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqBo9q" role="3cpWs9">
            <property role="TrG5h" value="rootNodes" />
            <node concept="_YKpA" id="5lhzOSqBo9j" role="1tU5fm">
              <node concept="3uibUv" id="5lhzOSqBoXv" role="_ZDj9">
                <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
              </node>
            </node>
            <node concept="2ShNRf" id="5lhzOSqBoc0" role="33vP2m">
              <node concept="Tc6Ow" id="5lhzOSqBowO" role="2ShVmc">
                <node concept="3uibUv" id="5lhzOSqBNPS" role="HW$YZ">
                  <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="5lhzOSqBp7M" role="3cqZAp">
          <node concept="2GrKxI" id="5lhzOSqBp7O" role="2Gsz3X">
            <property role="TrG5h" value="rootNode" />
          </node>
          <node concept="2OqwBi" id="5lhzOSqBpk1" role="2GsD0m">
            <node concept="37vLTw" id="5lhzOSqBpaO" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqAQQP" resolve="model" />
            </node>
            <node concept="liA8E" id="5lhzOSqBpuP" role="2OqNvi">
              <ref role="37wK5l" to="mhbf:~SModel.getRootNodes():java.lang.Iterable" resolve="getRootNodes" />
            </node>
          </node>
          <node concept="3clFbS" id="5lhzOSqBp7S" role="2LFqv$">
            <node concept="3clFbF" id="5lhzOSqBqrw" role="3cqZAp">
              <node concept="2OqwBi" id="5lhzOSqBrhG" role="3clFbG">
                <node concept="37vLTw" id="5lhzOSqBqrv" role="2Oq$k0">
                  <ref role="3cqZAo" node="5lhzOSqBo9q" resolve="rootNodes" />
                </node>
                <node concept="TSZUe" id="5lhzOSqBsUb" role="2OqNvi">
                  <node concept="0kSF2" id="5lhzOSqBBFr" role="25WWJ7">
                    <node concept="3uibUv" id="5lhzOSqBBL1" role="0kSFW">
                      <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
                    </node>
                    <node concept="2GrUjf" id="5lhzOSqBtld" role="0kSFX">
                      <ref role="2Gs0qQ" node="5lhzOSqBp7O" resolve="rootNode" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="34h7Gze4P2z" role="3cqZAp" />
        <node concept="3cpWs8" id="5lhzOSqBBYg" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqBBYh" role="3cpWs9">
            <property role="TrG5h" value="snode" />
            <node concept="3uibUv" id="5lhzOSqBBYi" role="1tU5fm">
              <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
            </node>
            <node concept="2OqwBi" id="5lhzOSqBD4S" role="33vP2m">
              <node concept="37vLTw" id="5lhzOSqBC43" role="2Oq$k0">
                <ref role="3cqZAo" node="5lhzOSqBo9q" resolve="rootNodes" />
              </node>
              <node concept="34jXtK" id="5lhzOSqBE7y" role="2OqNvi">
                <node concept="2OqwBi" id="5lhzOSqHlp5" role="25WWJ7">
                  <node concept="2OqwBi" id="5lhzOSqBEFM" role="2Oq$k0">
                    <node concept="2OqwBi" id="5lhzOSqBEkm" role="2Oq$k0">
                      <node concept="37vLTw" id="5lhzOSqBEb7" role="2Oq$k0">
                        <ref role="3cqZAo" node="5lhzOSqAQMT" resolve="node" />
                      </node>
                      <node concept="I4A8Y" id="5lhzOSqBEuG" role="2OqNvi" />
                    </node>
                    <node concept="2RRcyG" id="5lhzOSqBEQN" role="2OqNvi" />
                  </node>
                  <node concept="2WmjW8" id="5lhzOSqHme2" role="2OqNvi">
                    <node concept="2OqwBi" id="5lhzOSqHmPw" role="25WWJ7">
                      <node concept="37vLTw" id="5lhzOSqHmlK" role="2Oq$k0">
                        <ref role="3cqZAo" node="5lhzOSqAQMT" resolve="node" />
                      </node>
                      <node concept="2Rxl7S" id="5lhzOSqHn12" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="34h7Gze4Q5W" role="3cqZAp">
          <node concept="2OqwBi" id="34h7Gze4Qk9" role="3clFbG">
            <node concept="37vLTw" id="34h7Gze4Q5U" role="2Oq$k0">
              <ref role="3cqZAo" node="5lhzOSqAQMT" resolve="node" />
            </node>
            <node concept="1PgB_6" id="34h7Gze4Qu1" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbJ" id="5lhzOSqBJ8c" role="3cqZAp">
          <node concept="3clFbS" id="5lhzOSqBJ8e" role="3clFbx">
            <node concept="34ab3g" id="5lhzOSqBNym" role="3cqZAp">
              <property role="35gtTG" value="error" />
              <node concept="Xl_RD" id="5lhzOSqBNyo" role="34bqiv">
                <property role="Xl_RC" value="node must have a name" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="5lhzOSqBNuo" role="3clFbw">
            <node concept="10Nm6u" id="5lhzOSqBNxc" role="3uHU7w" />
            <node concept="37vLTw" id="5lhzOSqBJbM" role="3uHU7B">
              <ref role="3cqZAo" node="5lhzOSqBBYh" resolve="snode" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5lhzOSqBNAw" role="3cqZAp">
          <node concept="3cpWsn" id="5lhzOSqBNAz" role="3cpWs9">
            <property role="TrG5h" value="text" />
            <node concept="17QB3L" id="5lhzOSqBNAu" role="1tU5fm" />
            <node concept="2YIFZM" id="5lhzOSqBNFP" role="33vP2m">
              <ref role="37wK5l" to="ao3:~TextGeneratorEngine.generateText(org.jetbrains.mps.openapi.model.SNode):java.lang.String" resolve="generateText" />
              <ref role="1Pybhc" to="ao3:~TextGeneratorEngine" resolve="TextGeneratorEngine" />
              <node concept="37vLTw" id="5lhzOSqBNHX" role="37wK5m">
                <ref role="3cqZAo" node="5lhzOSqBBYh" resolve="snode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5lhzOSqBNL3" role="3cqZAp">
          <node concept="37vLTw" id="5lhzOSqBNMQ" role="3cqZAk">
            <ref role="3cqZAo" node="5lhzOSqBNAz" resolve="text" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5lhzOSqAQ_g" role="1B3o_S" />
      <node concept="17QB3L" id="5lhzOSqAQFT" role="3clF45" />
      <node concept="37vLTG" id="5lhzOSqAQMT" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="5lhzOSqAQMS" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="2NOvNR_ltu7" role="jymVt" />
    <node concept="2YIFZL" id="33HeFyz4Cut" role="jymVt">
      <property role="TrG5h" value="dumpFile" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="33HeFyz4Cuw" role="3clF47">
        <node concept="3cpWs8" id="5KGsxZrrQWs" role="3cqZAp">
          <node concept="3cpWsn" id="5KGsxZrrQWt" role="3cpWs9">
            <property role="TrG5h" value="writer" />
            <node concept="3uibUv" id="5KGsxZrrQWu" role="1tU5fm">
              <ref role="3uigEE" to="guwi:~PrintWriter" resolve="PrintWriter" />
            </node>
            <node concept="10Nm6u" id="5KGsxZrrQYh" role="33vP2m" />
          </node>
        </node>
        <node concept="2GUZhq" id="5KGsxZrrM$K" role="3cqZAp">
          <node concept="3clFbS" id="5KGsxZrrM$M" role="2GV8ay">
            <node concept="3clFbF" id="5KGsxZrrRYq" role="3cqZAp">
              <node concept="37vLTI" id="5KGsxZrrRYs" role="3clFbG">
                <node concept="2ShNRf" id="5KGsxZrrMPR" role="37vLTx">
                  <node concept="1pGfFk" id="5KGsxZrrPeo" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~PrintWriter.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="PrintWriter" />
                    <node concept="37vLTw" id="33HeFyz4CQe" role="37wK5m">
                      <ref role="3cqZAo" node="33HeFyz4CzT" resolve="path" />
                    </node>
                    <node concept="Xl_RD" id="5KGsxZrrPt1" role="37wK5m">
                      <property role="Xl_RC" value="UTF-8" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="5KGsxZrrS5G" role="37vLTJ">
                  <ref role="3cqZAo" node="5KGsxZrrQWt" resolve="writer" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5KGsxZrrTqR" role="3cqZAp">
              <node concept="2OqwBi" id="5KGsxZrrTDs" role="3clFbG">
                <node concept="37vLTw" id="5KGsxZrrTqP" role="2Oq$k0">
                  <ref role="3cqZAo" node="5KGsxZrrQWt" resolve="writer" />
                </node>
                <node concept="liA8E" id="5KGsxZrrU19" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintWriter.write(java.lang.String):void" resolve="write" />
                  <node concept="37vLTw" id="33HeFyz4CXM" role="37wK5m">
                    <ref role="3cqZAo" node="33HeFyz4Cyw" resolve="text" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5KGsxZrrM$N" role="2GVbov">
            <node concept="3clFbJ" id="5KGsxZrrS8a" role="3cqZAp">
              <node concept="3y3z36" id="5KGsxZrrSrV" role="3clFbw">
                <node concept="10Nm6u" id="5KGsxZrrSs5" role="3uHU7w" />
                <node concept="37vLTw" id="5KGsxZrrS8T" role="3uHU7B">
                  <ref role="3cqZAo" node="5KGsxZrrQWt" resolve="writer" />
                </node>
              </node>
              <node concept="3clFbS" id="5KGsxZrrS8c" role="3clFbx">
                <node concept="3clFbF" id="5KGsxZrrSsT" role="3cqZAp">
                  <node concept="2OqwBi" id="5KGsxZrrSFn" role="3clFbG">
                    <node concept="37vLTw" id="5KGsxZrrSsS" role="2Oq$k0">
                      <ref role="3cqZAo" node="5KGsxZrrQWt" resolve="writer" />
                    </node>
                    <node concept="liA8E" id="5KGsxZrrTp4" role="2OqNvi">
                      <ref role="37wK5l" to="guwi:~PrintWriter.close():void" resolve="close" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="5KGsxZrrPvK" role="TEXxN">
            <node concept="3cpWsn" id="5KGsxZrrPvL" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="5KGsxZrrPCG" role="1tU5fm">
                <ref role="3uigEE" to="guwi:~FileNotFoundException" resolve="FileNotFoundException" />
              </node>
            </node>
            <node concept="3clFbS" id="5KGsxZrrPvN" role="TDEfX">
              <node concept="34ab3g" id="5KGsxZrrPPG" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="5KGsxZrrPPI" role="34bqiv" />
                <node concept="37vLTw" id="5KGsxZrrPPK" role="34bMjA">
                  <ref role="3cqZAo" node="5KGsxZrrPvL" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="5KGsxZrrPSu" role="TEXxN">
            <node concept="3cpWsn" id="5KGsxZrrPSv" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="5KGsxZrrQ3B" role="1tU5fm">
                <ref role="3uigEE" to="guwi:~UnsupportedEncodingException" resolve="UnsupportedEncodingException" />
              </node>
            </node>
            <node concept="3clFbS" id="5KGsxZrrPSx" role="TDEfX">
              <node concept="34ab3g" id="5KGsxZrrQmT" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="5KGsxZrrQmV" role="34bqiv" />
                <node concept="37vLTw" id="5KGsxZrrQmX" role="34bMjA">
                  <ref role="3cqZAo" node="5KGsxZrrPSv" resolve="ex" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="33HeFyz4Cqi" role="1B3o_S" />
      <node concept="3cqZAl" id="33HeFyz4Cuj" role="3clF45" />
      <node concept="37vLTG" id="33HeFyz4Cyw" role="3clF46">
        <property role="TrG5h" value="text" />
        <node concept="17QB3L" id="33HeFyz4Cyv" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="33HeFyz4CzT" role="3clF46">
        <property role="TrG5h" value="path" />
        <node concept="17QB3L" id="33HeFyz4C$k" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="33HeFyzxx3Y" role="jymVt" />
    <node concept="2YIFZL" id="33HeFyz5b2h" role="jymVt">
      <property role="TrG5h" value="escapeNonBoolean" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="33HeFyz5b2j" role="3clF47">
        <node concept="3cpWs8" id="33HeFyz5b2k" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz5b2l" role="3cpWs9">
            <property role="TrG5h" value="replaced" />
            <node concept="17QB3L" id="33HeFyz5b2m" role="1tU5fm" />
            <node concept="37vLTw" id="33HeFyz5b2n" role="33vP2m">
              <ref role="3cqZAo" node="33HeFyz5b42" resolve="input" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyz5b2o" role="3cqZAp">
          <node concept="37vLTI" id="33HeFyz5b2p" role="3clFbG">
            <node concept="2OqwBi" id="33HeFyz5b2q" role="37vLTx">
              <node concept="37vLTw" id="33HeFyz5b2r" role="2Oq$k0">
                <ref role="3cqZAo" node="33HeFyz5b2l" resolve="replaced" />
              </node>
              <node concept="2kq01I" id="33HeFyz5b2s" role="2OqNvi">
                <node concept="1Qi9sc" id="33HeFyz5b2t" role="1YN4dH">
                  <node concept="1OJ37Q" id="33HeFyz5b2u" role="1QigWp">
                    <node concept="1Tukvm" id="33HeFyz5b2v" role="1OLpdg">
                      <property role="TrG5h" value="left" />
                      <node concept="1OClNT" id="33HeFyz5b2w" role="1TuGhC">
                        <node concept="1SSJmt" id="33HeFyz5b2x" role="1OLDsb">
                          <node concept="1T8lYq" id="33HeFyz5b2y" role="1T5LoC">
                            <property role="1T8p8b" value="A" />
                            <property role="1T8pRJ" value="Z" />
                          </node>
                          <node concept="1T6I$Y" id="33HeFyz5b2z" role="1T5LoC">
                            <property role="1T6KD9" value="_" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1OJ37Q" id="33HeFyz5b2$" role="1OLqdY">
                      <node concept="1OJ37Q" id="33HeFyz5b2_" role="1OLqdY">
                        <node concept="1OJ37Q" id="33HeFyz5b2A" role="1OLqdY">
                          <node concept="1OJ37Q" id="33HeFyz8oC9" role="1OLqdY">
                            <node concept="1SLe3L" id="33HeFyz8oYk" role="1OLpdg">
                              <node concept="1Tukvm" id="33HeFyz8oGB" role="1OLDsb">
                                <property role="TrG5h" value="minus" />
                                <node concept="1OC9wW" id="33HeFyz8oTA" role="1TuGhC">
                                  <property role="1OCb_u" value="-" />
                                </node>
                              </node>
                            </node>
                            <node concept="1Tukvm" id="33HeFyz5b2B" role="1OLqdY">
                              <property role="TrG5h" value="right" />
                              <node concept="1OClNT" id="33HeFyz5b2C" role="1TuGhC">
                                <node concept="1SSJmt" id="33HeFyz5b2D" role="1OLDsb">
                                  <node concept="1T8lYq" id="33HeFyz5b2E" role="1T5LoC">
                                    <property role="1T8p8b" value="A" />
                                    <property role="1T8pRJ" value="Z" />
                                  </node>
                                  <node concept="1T8lYq" id="33HeFyz5b2F" role="1T5LoC">
                                    <property role="1T8p8b" value="0" />
                                    <property role="1T8pRJ" value="9" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="1SLe3L" id="33HeFyz5b2G" role="1OLpdg">
                            <node concept="1SYyG9" id="33HeFyz5b2H" role="1OLDsb">
                              <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                            </node>
                          </node>
                        </node>
                        <node concept="1OC9wW" id="33HeFyz5b2I" role="1OLpdg">
                          <property role="1OCb_u" value="&gt;" />
                        </node>
                      </node>
                      <node concept="1SLe3L" id="33HeFyz5b2J" role="1OLpdg">
                        <node concept="1SYyG9" id="33HeFyz5b2K" role="1OLDsb">
                          <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1YY055" id="33HeFyz5b2L" role="2krO_r">
                  <node concept="3clFbS" id="33HeFyz5b2M" role="2VODD2">
                    <node concept="3clFbF" id="33HeFyz5b2N" role="3cqZAp">
                      <node concept="3cpWs3" id="33HeFyz5b2O" role="3clFbG">
                        <node concept="1TxZTf" id="33HeFyz5b2P" role="3uHU7w">
                          <ref role="1Ty1U8" node="33HeFyz5b2B" resolve="right" />
                        </node>
                        <node concept="3cpWs3" id="33HeFyz8pCf" role="3uHU7B">
                          <node concept="3cpWs3" id="33HeFyz5b2Q" role="3uHU7B">
                            <node concept="1TxZTf" id="33HeFyz5b2R" role="3uHU7B">
                              <ref role="1Ty1U8" node="33HeFyz5b2v" resolve="left" />
                            </node>
                            <node concept="Xl_RD" id="33HeFyz5b2S" role="3uHU7w">
                              <property role="Xl_RC" value="_GT_" />
                            </node>
                          </node>
                          <node concept="1eOMI4" id="33HeFyz8tsc" role="3uHU7w">
                            <node concept="3K4zz7" id="33HeFyz8pQE" role="1eOMHV">
                              <node concept="3clFbC" id="33HeFyza_T8" role="3K4Cdx">
                                <node concept="10Nm6u" id="33HeFyzaA7r" role="3uHU7w" />
                                <node concept="1TxZTf" id="33HeFyz8q4u" role="3uHU7B">
                                  <ref role="1Ty1U8" node="33HeFyz8oGB" resolve="minus" />
                                </node>
                              </node>
                              <node concept="Xl_RD" id="33HeFyz8rA_" role="3K4E3e">
                                <property role="Xl_RC" value="" />
                              </node>
                              <node concept="Xl_RD" id="33HeFyz8rOp" role="3K4GZi">
                                <property role="Xl_RC" value="MINUS_" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="37vLTw" id="33HeFyz5b2T" role="37vLTJ">
              <ref role="3cqZAo" node="33HeFyz5b2l" resolve="replaced" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyz5b2U" role="3cqZAp">
          <node concept="37vLTI" id="33HeFyz5b2V" role="3clFbG">
            <node concept="2OqwBi" id="33HeFyz5b2W" role="37vLTx">
              <node concept="37vLTw" id="33HeFyz5b2X" role="2Oq$k0">
                <ref role="3cqZAo" node="33HeFyz5b2l" resolve="replaced" />
              </node>
              <node concept="2kq01I" id="33HeFyz5b2Y" role="2OqNvi">
                <node concept="1Qi9sc" id="33HeFyz5b2Z" role="1YN4dH">
                  <node concept="1OJ37Q" id="33HeFyz5b30" role="1QigWp">
                    <node concept="1Tukvm" id="33HeFyz5b31" role="1OLpdg">
                      <property role="TrG5h" value="left" />
                      <node concept="1OClNT" id="33HeFyz5b32" role="1TuGhC">
                        <node concept="1SSJmt" id="33HeFyz5b33" role="1OLDsb">
                          <node concept="1T8lYq" id="33HeFyz5b34" role="1T5LoC">
                            <property role="1T8p8b" value="A" />
                            <property role="1T8pRJ" value="Z" />
                          </node>
                          <node concept="1T6I$Y" id="33HeFyz5b35" role="1T5LoC">
                            <property role="1T6KD9" value="_" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1OJ37Q" id="33HeFyz5b36" role="1OLqdY">
                      <node concept="1OJ37Q" id="33HeFyz5b37" role="1OLqdY">
                        <node concept="1OJ37Q" id="33HeFyz5b38" role="1OLqdY">
                          <node concept="1OJ37Q" id="33HeFyz8_Ke" role="1OLqdY">
                            <node concept="1Tukvm" id="33HeFyz5b39" role="1OLqdY">
                              <property role="TrG5h" value="right" />
                              <node concept="1OClNT" id="33HeFyz5b3a" role="1TuGhC">
                                <node concept="1SSJmt" id="33HeFyz5b3b" role="1OLDsb">
                                  <node concept="1T8lYq" id="33HeFyz5b3c" role="1T5LoC">
                                    <property role="1T8p8b" value="A" />
                                    <property role="1T8pRJ" value="Z" />
                                  </node>
                                  <node concept="1T8lYq" id="33HeFyz5b3d" role="1T5LoC">
                                    <property role="1T8p8b" value="0" />
                                    <property role="1T8pRJ" value="9" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="1SLe3L" id="33HeFyz8_KE" role="1OLpdg">
                              <node concept="1Tukvm" id="33HeFyz8_KF" role="1OLDsb">
                                <property role="TrG5h" value="minus" />
                                <node concept="1OC9wW" id="33HeFyz8_KG" role="1TuGhC">
                                  <property role="1OCb_u" value="-" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="1SLe3L" id="33HeFyz5b3e" role="1OLpdg">
                            <node concept="1SYyG9" id="33HeFyz5b3f" role="1OLDsb">
                              <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                            </node>
                          </node>
                        </node>
                        <node concept="1OC9wW" id="33HeFyz5b3g" role="1OLpdg">
                          <property role="1OCb_u" value="&lt;" />
                        </node>
                      </node>
                      <node concept="1SLe3L" id="33HeFyz5b3h" role="1OLpdg">
                        <node concept="1SYyG9" id="33HeFyz5b3i" role="1OLDsb">
                          <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1YY055" id="33HeFyz5b3j" role="2krO_r">
                  <node concept="3clFbS" id="33HeFyz5b3k" role="2VODD2">
                    <node concept="3clFbF" id="33HeFyz5b3l" role="3cqZAp">
                      <node concept="3cpWs3" id="33HeFyz5b3m" role="3clFbG">
                        <node concept="1TxZTf" id="33HeFyz5b3n" role="3uHU7w">
                          <ref role="1Ty1U8" node="33HeFyz5b39" resolve="right" />
                        </node>
                        <node concept="3cpWs3" id="33HeFyz8Aja" role="3uHU7B">
                          <node concept="3cpWs3" id="33HeFyz5b3o" role="3uHU7B">
                            <node concept="1TxZTf" id="33HeFyz5b3p" role="3uHU7B">
                              <ref role="1Ty1U8" node="33HeFyz5b31" resolve="left" />
                            </node>
                            <node concept="Xl_RD" id="33HeFyz5b3q" role="3uHU7w">
                              <property role="Xl_RC" value="_LT_" />
                            </node>
                          </node>
                          <node concept="1eOMI4" id="33HeFyz8Awa" role="3uHU7w">
                            <node concept="3K4zz7" id="33HeFyz8Awb" role="1eOMHV">
                              <node concept="3clFbC" id="33HeFyzaB5x" role="3K4Cdx">
                                <node concept="10Nm6u" id="33HeFyzaBfu" role="3uHU7w" />
                                <node concept="1TxZTf" id="33HeFyz8Awd" role="3uHU7B">
                                  <ref role="1Ty1U8" node="33HeFyz8_KF" resolve="minus" />
                                </node>
                              </node>
                              <node concept="Xl_RD" id="33HeFyz8Awf" role="3K4E3e">
                                <property role="Xl_RC" value="" />
                              </node>
                              <node concept="Xl_RD" id="33HeFyz8Awg" role="3K4GZi">
                                <property role="Xl_RC" value="MINUS_" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="37vLTw" id="33HeFyz5b3r" role="37vLTJ">
              <ref role="3cqZAo" node="33HeFyz5b2l" resolve="replaced" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyz5b3s" role="3cqZAp">
          <node concept="37vLTI" id="33HeFyz5b3t" role="3clFbG">
            <node concept="2OqwBi" id="33HeFyz5b3u" role="37vLTx">
              <node concept="37vLTw" id="33HeFyz5b3v" role="2Oq$k0">
                <ref role="3cqZAo" node="33HeFyz5b2l" resolve="replaced" />
              </node>
              <node concept="2kq01I" id="33HeFyz5b3w" role="2OqNvi">
                <node concept="1Qi9sc" id="33HeFyz5b3x" role="1YN4dH">
                  <node concept="1OJ37Q" id="33HeFyz5b3y" role="1QigWp">
                    <node concept="1Tukvm" id="33HeFyz5b3z" role="1OLpdg">
                      <property role="TrG5h" value="left" />
                      <node concept="1OClNT" id="33HeFyz5b3$" role="1TuGhC">
                        <node concept="1SSJmt" id="33HeFyz5b3_" role="1OLDsb">
                          <node concept="1T8lYq" id="33HeFyz5b3A" role="1T5LoC">
                            <property role="1T8p8b" value="A" />
                            <property role="1T8pRJ" value="Z" />
                          </node>
                          <node concept="1T6I$Y" id="33HeFyz5b3B" role="1T5LoC">
                            <property role="1T6KD9" value="_" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1OJ37Q" id="33HeFyz5b3C" role="1OLqdY">
                      <node concept="1OJ37Q" id="33HeFyz5b3D" role="1OLqdY">
                        <node concept="1OJ37Q" id="33HeFyz5b3E" role="1OLqdY">
                          <node concept="1OJ37Q" id="33HeFyz8_KU" role="1OLqdY">
                            <node concept="1Tukvm" id="33HeFyz5b3F" role="1OLqdY">
                              <property role="TrG5h" value="right" />
                              <node concept="1OClNT" id="33HeFyz5b3G" role="1TuGhC">
                                <node concept="1SSJmt" id="33HeFyz5b3H" role="1OLDsb">
                                  <node concept="1T8lYq" id="33HeFyz5b3I" role="1T5LoC">
                                    <property role="1T8p8b" value="A" />
                                    <property role="1T8pRJ" value="Z" />
                                  </node>
                                  <node concept="1T8lYq" id="33HeFyz5b3J" role="1T5LoC">
                                    <property role="1T8p8b" value="0" />
                                    <property role="1T8pRJ" value="9" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="1SLe3L" id="33HeFyz8_Lm" role="1OLpdg">
                              <node concept="1Tukvm" id="33HeFyz8_Ln" role="1OLDsb">
                                <property role="TrG5h" value="minus" />
                                <node concept="1OC9wW" id="33HeFyz8_Lo" role="1TuGhC">
                                  <property role="1OCb_u" value="-" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="1SLe3L" id="33HeFyz5b3K" role="1OLpdg">
                            <node concept="1SYyG9" id="33HeFyz5b3L" role="1OLDsb">
                              <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                            </node>
                          </node>
                        </node>
                        <node concept="1OC9wW" id="33HeFyz5b3M" role="1OLpdg">
                          <property role="1OCb_u" value="==" />
                        </node>
                      </node>
                      <node concept="1SLe3L" id="33HeFyz5b3N" role="1OLpdg">
                        <node concept="1SYyG9" id="33HeFyz5b3O" role="1OLDsb">
                          <ref role="1SYXPG" to="tpfp:h5SUD2M" resolve="\s" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1YY055" id="33HeFyz5b3P" role="2krO_r">
                  <node concept="3clFbS" id="33HeFyz5b3Q" role="2VODD2">
                    <node concept="3clFbF" id="33HeFyz5b3R" role="3cqZAp">
                      <node concept="3cpWs3" id="33HeFyz5b3S" role="3clFbG">
                        <node concept="1TxZTf" id="33HeFyz5b3T" role="3uHU7w">
                          <ref role="1Ty1U8" node="33HeFyz5b3F" resolve="right" />
                        </node>
                        <node concept="3cpWs3" id="33HeFyz8Bfv" role="3uHU7B">
                          <node concept="3cpWs3" id="33HeFyz5b3U" role="3uHU7B">
                            <node concept="1TxZTf" id="33HeFyz5b3V" role="3uHU7B">
                              <ref role="1Ty1U8" node="33HeFyz5b3z" resolve="left" />
                            </node>
                            <node concept="Xl_RD" id="33HeFyz5b3W" role="3uHU7w">
                              <property role="Xl_RC" value="_EQ_" />
                            </node>
                          </node>
                          <node concept="1eOMI4" id="33HeFyz8BfF" role="3uHU7w">
                            <node concept="3K4zz7" id="33HeFyz8BfG" role="1eOMHV">
                              <node concept="3clFbC" id="33HeFyzaCd$" role="3K4Cdx">
                                <node concept="10Nm6u" id="33HeFyzaCen" role="3uHU7w" />
                                <node concept="1TxZTf" id="33HeFyz8BfI" role="3uHU7B">
                                  <ref role="1Ty1U8" node="33HeFyz8_Ln" resolve="minus" />
                                </node>
                              </node>
                              <node concept="Xl_RD" id="33HeFyz8BfK" role="3K4E3e">
                                <property role="Xl_RC" value="" />
                              </node>
                              <node concept="Xl_RD" id="33HeFyz8BfL" role="3K4GZi">
                                <property role="Xl_RC" value="MINUS_" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="37vLTw" id="33HeFyz5b3X" role="37vLTJ">
              <ref role="3cqZAo" node="33HeFyz5b2l" resolve="replaced" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="33HeFyz5b3Y" role="3cqZAp">
          <node concept="37vLTw" id="33HeFyz5b3Z" role="3cqZAk">
            <ref role="3cqZAo" node="33HeFyz5b2l" resolve="replaced" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="33HeFyz5b41" role="3clF45" />
      <node concept="37vLTG" id="33HeFyz5b42" role="3clF46">
        <property role="TrG5h" value="input" />
        <node concept="17QB3L" id="33HeFyz5b43" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="33HeFyz5b40" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="33HeFyz4zl9" role="jymVt" />
    <node concept="1X3_iC" id="34h7GzesX0n" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2YIFZL" id="5KGsxZrsCY1" role="8Wnug">
        <property role="TrG5h" value="generteNodeToFile" />
        <property role="od$2w" value="false" />
        <property role="DiZV1" value="false" />
        <property role="2aFKle" value="false" />
        <node concept="3clFbS" id="5KGsxZrsCY4" role="3clF47">
          <node concept="1X3_iC" id="34h7GzesWZy" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3cpWs8" id="2NOvNR_lzyg" role="8Wnug">
              <node concept="3cpWsn" id="2NOvNR_lzyj" role="3cpWs9">
                <property role="TrG5h" value="text" />
                <node concept="17QB3L" id="2NOvNR_lzye" role="1tU5fm" />
                <node concept="1rXfSq" id="2NOvNR_lzGE" role="33vP2m">
                  <ref role="37wK5l" node="2NOvNR_l$0S" resolve="generateNode" />
                  <node concept="37vLTw" id="2NOvNR_lzJX" role="37wK5m">
                    <ref role="3cqZAo" node="5KGsxZrsEjo" resolve="node" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1X3_iC" id="34h7GzesWXt" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3clFbF" id="33HeFyz4D5i" role="8Wnug">
              <node concept="1rXfSq" id="33HeFyz4D5g" role="3clFbG">
                <ref role="37wK5l" node="33HeFyz4Cut" resolve="dumpFile" />
                <node concept="37vLTw" id="33HeFyz4D8n" role="37wK5m">
                  <ref role="3cqZAo" node="2NOvNR_lzyj" resolve="text" />
                </node>
                <node concept="37vLTw" id="33HeFyz4Dc1" role="37wK5m">
                  <ref role="3cqZAo" node="5KGsxZrsF3M" resolve="path" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3Tm1VV" id="5KGsxZrsCXM" role="1B3o_S" />
        <node concept="3cqZAl" id="5KGsxZrsCYm" role="3clF45" />
        <node concept="37vLTG" id="5KGsxZrsEjo" role="3clF46">
          <property role="TrG5h" value="node" />
          <node concept="3Tqbb2" id="5KGsxZrsEjn" role="1tU5fm" />
        </node>
        <node concept="37vLTG" id="5KGsxZrsF3M" role="3clF46">
          <property role="TrG5h" value="path" />
          <node concept="17QB3L" id="5KGsxZrsF3U" role="1tU5fm" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="5KGsxZrsCXt" role="1B3o_S" />
  </node>
  <node concept="13h7C7" id="33HeFyzxxbb">
    <ref role="13h7C2" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="13i0hz" id="33HeFyzxxbm" role="13h7CS">
      <property role="TrG5h" value="checkout" />
      <node concept="3Tm1VV" id="33HeFyzxxbn" role="1B3o_S" />
      <node concept="3cqZAl" id="33HeFyzxxbA" role="3clF45" />
      <node concept="3clFbS" id="33HeFyzxxbp" role="3clF47">
        <node concept="3cpWs8" id="6XRhSxwn$1R" role="3cqZAp">
          <node concept="3cpWsn" id="6XRhSxwn$1U" role="3cpWs9">
            <property role="TrG5h" value="path" />
            <node concept="17QB3L" id="6XRhSxwn$1P" role="1tU5fm" />
            <node concept="2OqwBi" id="6XRhSxwo8m2" role="33vP2m">
              <node concept="2ShNRf" id="6XRhSxwn$4i" role="2Oq$k0">
                <node concept="1pGfFk" id="6XRhSxwo6nh" role="2ShVmc">
                  <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                  <node concept="2OqwBi" id="5gPWvT9U4eZ" role="37wK5m">
                    <node concept="2YIFZM" id="5gPWvT9U49n" role="2Oq$k0">
                      <ref role="37wK5l" to="18ew:~MacrosFactory.getGlobal():jetbrains.mps.util.MacroHelper" resolve="getGlobal" />
                      <ref role="1Pybhc" to="18ew:~MacrosFactory" resolve="MacrosFactory" />
                    </node>
                    <node concept="liA8E" id="5gPWvT9U4rZ" role="2OqNvi">
                      <ref role="37wK5l" to="18ew:~MacroHelper.expandPath(java.lang.String):java.lang.String" resolve="expandPath" />
                      <node concept="Xl_RD" id="5gPWvT9U4tS" role="37wK5m">
                        <property role="Xl_RC" value="${TempFolder}" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="6XRhSxwo81Y" role="37wK5m">
                    <property role="Xl_RC" value="MPSOutputFileCheckout.txt" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="6XRhSxwo9jR" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~File.getAbsolutePath():java.lang.String" resolve="getAbsolutePath" />
              </node>
            </node>
          </node>
        </node>
        <node concept="34ab3g" id="vk1xVpP8wZ" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="6XRhSxwoazm" role="34bqiv">
            <node concept="37vLTw" id="6XRhSxwoaDn" role="3uHU7w">
              <ref role="3cqZAo" node="6XRhSxwn$1U" resolve="path" />
            </node>
            <node concept="Xl_RD" id="vk1xVpP8x1" role="3uHU7B">
              <property role="Xl_RC" value="Checkout. Write to temp file " />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz590Y" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz5911" role="3cpWs9">
            <property role="TrG5h" value="content" />
            <node concept="17QB3L" id="33HeFyz590W" role="1tU5fm" />
            <node concept="2YIFZM" id="33HeFyz59dJ" role="33vP2m">
              <ref role="1Pybhc" node="5KGsxZrsCXs" resolve="MyHelper" />
              <ref role="37wK5l" node="2NOvNR_l$0S" resolve="generateNode" />
              <node concept="13iPFW" id="33HeFyzxxqv" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="33HeFyz5me5" role="3cqZAp">
          <node concept="3cpWsn" id="33HeFyz5me8" role="3cpWs9">
            <property role="TrG5h" value="escaped" />
            <node concept="17QB3L" id="33HeFyz5me3" role="1tU5fm" />
            <node concept="2YIFZM" id="33HeFyz5mry" role="33vP2m">
              <ref role="37wK5l" node="33HeFyz5b2h" resolve="escapeNonBoolean" />
              <ref role="1Pybhc" node="5KGsxZrsCXs" resolve="MyHelper" />
              <node concept="37vLTw" id="33HeFyz5ms8" role="37wK5m">
                <ref role="3cqZAo" node="33HeFyz5911" resolve="content" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="33HeFyz59sF" role="3cqZAp">
          <node concept="2YIFZM" id="33HeFyz59MJ" role="3clFbG">
            <ref role="1Pybhc" node="5KGsxZrsCXs" resolve="MyHelper" />
            <ref role="37wK5l" node="33HeFyz4Cut" resolve="dumpFile" />
            <node concept="37vLTw" id="34h7Gze$fHe" role="37wK5m">
              <ref role="3cqZAo" node="33HeFyz5me8" resolve="escaped" />
            </node>
            <node concept="37vLTw" id="33HeFyz59Y3" role="37wK5m">
              <ref role="3cqZAo" node="6XRhSxwn$1U" resolve="path" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="33HeFyzxxbc" role="13h7CW">
      <node concept="3clFbS" id="33HeFyzxxbd" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="7cMGUmZ81bi">
    <ref role="13h7C2" to="4s7a:3LUIgcmYqc7" resolve="Intention" />
    <node concept="13hLZK" id="7cMGUmZ81bj" role="13h7CW">
      <node concept="3clFbS" id="7cMGUmZ81bk" role="2VODD2">
        <node concept="3clFbF" id="7cMGUmZ82aF" role="3cqZAp">
          <node concept="37vLTI" id="7cMGUmZ82Qh" role="3clFbG">
            <node concept="3clFbT" id="7cMGUmZ82S$" role="37vLTx">
              <property role="3clFbU" value="false" />
            </node>
            <node concept="2OqwBi" id="7cMGUmZ82hF" role="37vLTJ">
              <node concept="13iPFW" id="7cMGUmZ82aE" role="2Oq$k0" />
              <node concept="3TrcHB" id="7cMGUmZ82tu" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:7cMGUmZ7UHY" resolve="isImplicit" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

