<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9545cdb8-8b5f-4177-be46-4ce51391ab21(CPPLanguage.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="7zqe" ref="r:7e3a84cb-38df-4db2-adea-884d8987c992(CPPLanguage.behavior)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="4s7a" ref="r:c67970af-4844-47d5-9471-54e2bda5945e(CPPLanguage.structure)" />
    <import index="g51k" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.nodeEditor.cells(MPS.Editor/)" />
    <import index="y49u" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.util(MPS.OpenAPI/)" />
    <import index="f4zo" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor.cells(MPS.Editor/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="mnlj" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.beans(JDK/)" />
    <import index="hyam" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.event(JDK/)" />
    <import index="5ueo" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.editor.runtime.style(MPS.Editor/)" />
    <import index="hox0" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor.style(MPS.Editor/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="iptd" ref="r:87ad25c8-6567-4dff-99e1-1dedfd4141eb(CPPImporter.plugin)" />
    <import index="cj4x" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor(MPS.Editor/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="9z78" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.border(JDK/)" />
    <import index="jan3" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.image(JDK/)" />
    <import index="lui2" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.module(MPS.OpenAPI/)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" implicit="true" />
    <import index="22ra" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor.update(MPS.Editor/)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1402906326896143883" name="jetbrains.mps.lang.editor.structure.CellKeyMap_FunctionParm_selectedNode" flags="nn" index="0GJ7k" />
      <concept id="1402906326895675325" name="jetbrains.mps.lang.editor.structure.CellActionMap_FunctionParm_selectedNode" flags="nn" index="0IXxy" />
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi">
        <child id="1078153129734" name="inspectedCellModel" index="6VMZX" />
        <child id="2597348684684069742" name="contextHints" index="CpUAK" />
      </concept>
      <concept id="1176897764478" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeFactory" flags="in" index="4$FPG" />
      <concept id="1226339751946" name="jetbrains.mps.lang.editor.structure.PaddingTopStyleClassItem" flags="ln" index="27yT$n" />
      <concept id="1226339813308" name="jetbrains.mps.lang.editor.structure.PaddingBottomStyleClassItem" flags="ln" index="27z8qx" />
      <concept id="6822301196700715228" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclarationReference" flags="ig" index="2aJ2om">
        <reference id="5944657839026714445" name="hint" index="2$4xQ3" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1176897874615" name="nodeFactory" index="4_6I_" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="1140524464359" name="emptyCellModel" index="2czzBI" />
        <child id="6046489571270834038" name="foldedCellModel" index="3EmGlc" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="3459162043708467089" name="jetbrains.mps.lang.editor.structure.CellActionMap_CanExecuteFunction" flags="in" index="jK8Ss" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="4242538589859161874" name="jetbrains.mps.lang.editor.structure.ExplicitHintsSpecification" flags="ng" index="2w$q5c">
        <child id="4242538589859162459" name="hints" index="2w$qW5" />
      </concept>
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="5944657839000868711" name="jetbrains.mps.lang.editor.structure.ConceptEditorContextHints" flags="ig" index="2ABfQD">
        <child id="5944657839000877563" name="hints" index="2ABdcP" />
      </concept>
      <concept id="5944657839003601246" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclaration" flags="ig" index="2BsEeg">
        <property id="168363875802087287" name="showInUI" index="2gpH_U" />
        <property id="5944657839012629576" name="presentation" index="2BUmq6" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="3473224453637651916" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform_PlaceInCellHolder" flags="ng" index="CtIbL">
        <property id="3473224453637651917" name="placeInCell" index="CtIbK" />
      </concept>
      <concept id="1638911550608571617" name="jetbrains.mps.lang.editor.structure.TransformationMenu_Default" flags="ng" index="IW6AY" />
      <concept id="8449131619432941427" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_Super" flags="ng" index="L$LW2" />
      <concept id="1136916919141" name="jetbrains.mps.lang.editor.structure.CellKeyMapItem" flags="lg" index="2PxR9H">
        <property id="1141091278922" name="caretPolicy" index="2IlM53" />
        <child id="1136916998332" name="keystroke" index="2PyaAO" />
        <child id="1136917325338" name="isApplicableFunction" index="2Pzqsi" />
        <child id="1136920925604" name="executeFunction" index="2PL9iG" />
      </concept>
      <concept id="1136916976737" name="jetbrains.mps.lang.editor.structure.CellKeyMapKeystroke" flags="ng" index="2Py5lD">
        <property id="1136923970223" name="modifiers" index="2PWKIB" />
        <property id="1136923970224" name="keycode" index="2PWKIS" />
      </concept>
      <concept id="1136917249679" name="jetbrains.mps.lang.editor.structure.CellKeyMap_IsApplicableFunction" flags="in" index="2Pz7Y7" />
      <concept id="1136917288805" name="jetbrains.mps.lang.editor.structure.CellKeyMap_ExecuteFunction" flags="in" index="2PzhpH" />
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW" />
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="4323500428121233431" name="jetbrains.mps.lang.editor.structure.EditorCellId" flags="ng" index="2SqB2G" />
      <concept id="1186402211651" name="jetbrains.mps.lang.editor.structure.StyleSheet" flags="ng" index="V5hpn">
        <child id="1186402402630" name="styleClass" index="V601i" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
        <child id="1186403803051" name="query" index="VblUZ" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2">
        <property id="1186403771423" name="style" index="Vbekb" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186404574412" name="jetbrains.mps.lang.editor.structure.BackgroundColorStyleClassItem" flags="ln" index="Veino" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
        <child id="1223387335081" name="query" index="3n$kyP" />
      </concept>
      <concept id="1186414976055" name="jetbrains.mps.lang.editor.structure.DrawBorderStyleClassItem" flags="ln" index="VPXOz" />
      <concept id="1186414999511" name="jetbrains.mps.lang.editor.structure.UnderlinedStyleClassItem" flags="ln" index="VQ3r3">
        <property id="1214316229833" name="underlined" index="2USNnj" />
      </concept>
      <concept id="1186415722038" name="jetbrains.mps.lang.editor.structure.FontSizeStyleClassItem" flags="ln" index="VSNWy">
        <property id="1221209241505" name="value" index="1lJzqX" />
        <child id="1221064706952" name="query" index="1d8cEk" />
      </concept>
      <concept id="1074767920765" name="jetbrains.mps.lang.editor.structure.CellModel_ModelAccess" flags="sg" stub="8104358048506729357" index="XafU7">
        <child id="1176718152741" name="modelAcessor" index="3TRxkO" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="1182191800432" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeListFilter" flags="in" index="107P5z" />
      <concept id="1214406454886" name="jetbrains.mps.lang.editor.structure.TextBackgroundColorStyleClassItem" flags="ln" index="30gYXW" />
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1233759184865" name="jetbrains.mps.lang.editor.structure.PunctuationRightStyleClassItem" flags="ln" index="11LMrY" />
      <concept id="1081293058843" name="jetbrains.mps.lang.editor.structure.CellKeyMapDeclaration" flags="ig" index="325Ffw">
        <reference id="1139445935125" name="applicableConcept" index="1chiOs" />
        <child id="1136930944870" name="item" index="2QnnpI" />
      </concept>
      <concept id="1182233249301" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_childNode" flags="nn" index="12_Ws6" />
      <concept id="1233823429331" name="jetbrains.mps.lang.editor.structure.HorizontalGapStyleClassItem" flags="ln" index="15ARfc" />
      <concept id="8313721352726366579" name="jetbrains.mps.lang.editor.structure.CellModel_Empty" flags="ng" index="35HoNQ" />
      <concept id="1221057094638" name="jetbrains.mps.lang.editor.structure.QueryFunction_Integer" flags="in" index="1cFabM" />
      <concept id="2896773699153795590" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform" flags="ng" index="3cWJ9i">
        <child id="3473224453637651919" name="placeInCell" index="CtIbM" />
      </concept>
      <concept id="1103016434866" name="jetbrains.mps.lang.editor.structure.CellModel_JComponent" flags="sg" stub="8104358048506731196" index="3gTLQM">
        <child id="1176475119347" name="componentProvider" index="3FoqZy" />
      </concept>
      <concept id="1139535219966" name="jetbrains.mps.lang.editor.structure.CellActionMapDeclaration" flags="ig" index="1h_SRR">
        <reference id="1139535219968" name="applicableConcept" index="1h_SK9" />
        <child id="1139535219969" name="item" index="1h_SK8" />
      </concept>
      <concept id="1139535280617" name="jetbrains.mps.lang.editor.structure.CellActionMapItem" flags="lg" index="1hA7zw">
        <property id="1139535298778" name="actionId" index="1hAc7j" />
        <property id="1139537298254" name="description" index="1hHO97" />
        <child id="3459162043708468028" name="canExecuteFunction" index="jK8aL" />
        <child id="1139535280620" name="executeFunction" index="1hA7z_" />
      </concept>
      <concept id="1139535439104" name="jetbrains.mps.lang.editor.structure.CellActionMap_ExecuteFunction" flags="in" index="1hAIg9" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1223386653097" name="jetbrains.mps.lang.editor.structure.StrikeOutStyleSheet" flags="ln" index="3nxI2P" />
      <concept id="1223387125302" name="jetbrains.mps.lang.editor.structure.QueryFunction_Boolean" flags="in" index="3nzxsE" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="3982520150113085419" name="jetbrains.mps.lang.editor.structure.StyleAttributeDeclaration" flags="ig" index="3t5Usi">
        <property id="8714766435264464176" name="inherited" index="iBDjm" />
        <child id="3982520150113147643" name="defaultValue" index="3t49C2" />
        <child id="3982520150113092206" name="valueType" index="3t5Oan" />
      </concept>
      <concept id="1215007762405" name="jetbrains.mps.lang.editor.structure.FloatStyleClassItem" flags="ln" index="3$6MrZ">
        <property id="1215007802031" name="value" index="3$6WeP" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <property id="1130859485024" name="attractsFocus" index="1cu_pB" />
        <reference id="1081339532145" name="keyMap" index="34QXea" />
        <reference id="1139959269582" name="actionMap" index="1ERwB7" />
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
        <child id="4323500428121274054" name="id" index="2SqHTX" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <property id="1160590353935" name="usesFolding" index="S$Qs1" />
        <property id="6240706158490734113" name="collapseByDefault" index="3EXrWe" />
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="6240706158490734121" name="collapseByDefaultCondition" index="3EXrW6" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY">
        <property id="16410578721444372" name="customizeEmptyCell" index="2ru_X1" />
        <child id="16410578721629643" name="emptyCellModel" index="2ruayu" />
        <child id="5861024100072578576" name="removeHints" index="3xwHhd" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR">
        <child id="1182233390675" name="filter" index="12AuX0" />
      </concept>
      <concept id="1176474535556" name="jetbrains.mps.lang.editor.structure.QueryFunction_JComponent" flags="in" index="3Fmcul" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="5624877018228267058" name="jetbrains.mps.lang.editor.structure.ITransformationMenu" flags="ng" index="3INCJE">
        <child id="1638911550608572412" name="sections" index="IW6Ez" />
      </concept>
      <concept id="1161622981231" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1Q80Hx" />
      <concept id="1088612959204" name="jetbrains.mps.lang.editor.structure.CellModel_Alternation" flags="sg" stub="8104358048506729361" index="1QoScp">
        <property id="1088613081987" name="vertical" index="1QpmdY" />
        <child id="1145918517974" name="alternationCondition" index="3e4ffs" />
        <child id="1088612958265" name="ifTrueCellModel" index="1QoS34" />
        <child id="1088612973955" name="ifFalseCellModel" index="1QoVPY" />
      </concept>
      <concept id="7980428675268276156" name="jetbrains.mps.lang.editor.structure.TransformationMenuSection" flags="ng" index="1Qtc8_">
        <child id="7980428675268276157" name="locations" index="1Qtc8$" />
        <child id="7980428675268276159" name="parts" index="1Qtc8A" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1176717871254" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Setter" flags="in" index="3TQsA7" />
      <concept id="1176717888428" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Validator" flags="in" index="3TQwEX" />
      <concept id="1176717996748" name="jetbrains.mps.lang.editor.structure.ModelAccessor" flags="ng" index="3TQVft">
        <child id="1176718001874" name="getter" index="3TQWv3" />
        <child id="1176718007938" name="setter" index="3TQXYj" />
        <child id="1176718014393" name="validator" index="3TQZqC" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
      <concept id="1176809959526" name="jetbrains.mps.lang.editor.structure.QueryFunction_Color" flags="in" index="3ZlJ5R" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1215695189714" name="jetbrains.mps.baseLanguage.structure.PlusAssignmentExpression" flags="nn" index="d57v9" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1076505808687" name="jetbrains.mps.baseLanguage.structure.WhileStatement" flags="nn" index="2$JKZl">
        <child id="1076505808688" name="condition" index="2$JKZa" />
      </concept>
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1095950406618" name="jetbrains.mps.baseLanguage.structure.DivExpression" flags="nn" index="FJ1c_" />
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1083245097125" name="jetbrains.mps.baseLanguage.structure.EnumClass" flags="ig" index="Qs71p">
        <child id="1083245396908" name="enumConstant" index="Qtgdg" />
      </concept>
      <concept id="1083245299891" name="jetbrains.mps.baseLanguage.structure.EnumConstantDeclaration" flags="ig" index="QsSxf" />
      <concept id="1083260308424" name="jetbrains.mps.baseLanguage.structure.EnumConstantReference" flags="nn" index="Rm8GO">
        <reference id="1083260308426" name="enumConstantDeclaration" index="Rm8GQ" />
        <reference id="1144432896254" name="enumClass" index="1Px2BO" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1081256982272" name="jetbrains.mps.baseLanguage.structure.InstanceOfExpression" flags="nn" index="2ZW3vV">
        <child id="1081256993305" name="classType" index="2ZW6by" />
        <child id="1081256993304" name="leftExpression" index="2ZW6bz" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1092119917967" name="jetbrains.mps.baseLanguage.structure.MulExpression" flags="nn" index="17qRlL" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1225271221393" name="jetbrains.mps.baseLanguage.structure.NPENotEqualsExpression" flags="nn" index="17QLQc" />
      <concept id="1225271283259" name="jetbrains.mps.baseLanguage.structure.NPEEqualsExpression" flags="nn" index="17R0WA" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
        <child id="1206060520071" name="elsifClauses" index="3eNLev" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1206060495898" name="jetbrains.mps.baseLanguage.structure.ElsifClause" flags="ng" index="3eNFk2">
        <child id="1206060619838" name="condition" index="3eO9$A" />
        <child id="1206060644605" name="statementList" index="3eOfB_" />
      </concept>
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1073063089578" name="jetbrains.mps.baseLanguage.structure.SuperMethodCall" flags="nn" index="3nyPlj" />
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918975462" name="jetbrains.mps.baseLanguage.structure.PostfixDecrementExpression" flags="nn" index="3uO5VW" />
      <concept id="7024111702304501416" name="jetbrains.mps.baseLanguage.structure.OrAssignmentExpression" flags="nn" index="3vZ8r8" />
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
        <child id="1201186121363" name="typeParameter" index="2Ghqu4" />
      </concept>
      <concept id="8064396509828172209" name="jetbrains.mps.baseLanguage.structure.UnaryMinus" flags="nn" index="1ZRNhn" />
    </language>
    <language id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access">
      <concept id="5332677359380589431" name="jetbrains.mps.lang.access.structure.ExecuteTransparentCommandStatement" flags="nn" index="2LD9aU" />
      <concept id="8974276187400348173" name="jetbrains.mps.lang.access.structure.CommandClosureLiteral" flags="nn" index="1QHqEC" />
      <concept id="8974276187400348170" name="jetbrains.mps.lang.access.structure.BaseExecuteCommandStatement" flags="nn" index="1QHqEJ">
        <child id="1423104411234567454" name="repo" index="ukAjM" />
        <child id="8974276187400348171" name="commandClosureLiteral" index="1QHqEI" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="5820409030208923287" name="jetbrains.mps.lang.smodel.structure.Node_GetContainingLinkOperation" flags="nn" index="25OxAV" />
      <concept id="1226359078165" name="jetbrains.mps.lang.smodel.structure.LinkRefExpression" flags="nn" index="28GBK8">
        <reference id="1226359078166" name="conceptDeclaration" index="28GBKb" />
        <reference id="1226359192215" name="linkDeclaration" index="28H3Ia" />
      </concept>
      <concept id="1179168000618" name="jetbrains.mps.lang.smodel.structure.Node_GetIndexInParentOperation" flags="nn" index="2bSWHS" />
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1138661924179" name="jetbrains.mps.lang.smodel.structure.Property_SetOperation" flags="nn" index="tyxLq">
        <child id="1138662048170" name="value" index="tz02z" />
      </concept>
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1173122760281" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorsOperation" flags="nn" index="z$bX8" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <reference id="6733348108486823428" name="concept" index="1m5ApE" />
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
      </concept>
      <concept id="1143224066846" name="jetbrains.mps.lang.smodel.structure.Node_InsertNextSiblingOperation" flags="nn" index="HtI8k">
        <child id="1143224066849" name="insertedNode" index="HtI8F" />
      </concept>
      <concept id="1143224127713" name="jetbrains.mps.lang.smodel.structure.Node_InsertPrevSiblingOperation" flags="nn" index="HtX7F">
        <child id="1143224127716" name="insertedNode" index="HtX7I" />
      </concept>
      <concept id="1145383075378" name="jetbrains.mps.lang.smodel.structure.SNodeListType" flags="in" index="2I9FWS">
        <reference id="1145383142433" name="elementConcept" index="2I9WkF" />
      </concept>
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1145567426890" name="jetbrains.mps.lang.smodel.structure.SNodeListCreator" flags="nn" index="2T8Vx0">
        <child id="1145567471833" name="createdType" index="2T96Bj" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1144100932627" name="jetbrains.mps.lang.smodel.structure.OperationParm_Inclusion" flags="ng" index="1xIGOp" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1144195091934" name="jetbrains.mps.lang.smodel.structure.Node_IsRoleOperation" flags="nn" index="1BlSNk">
        <reference id="1144195362400" name="conceptOfParent" index="1BmUXE" />
        <reference id="1144195396777" name="linkInParent" index="1Bn3mz" />
      </concept>
      <concept id="1140133623887" name="jetbrains.mps.lang.smodel.structure.Node_DeleteOperation" flags="nn" index="1PgB_6" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <property id="1238684351431" name="asCast" index="1BlNFB" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1209727891789" name="jetbrains.mps.baseLanguage.collections.structure.ComparatorSortOperation" flags="nn" index="2DpFxk">
        <child id="1209727996925" name="ascending" index="2Dq5b$" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1235566554328" name="jetbrains.mps.baseLanguage.collections.structure.AnyOperation" flags="nn" index="2HwmR7" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1160666733551" name="jetbrains.mps.baseLanguage.collections.structure.AddAllElementsOperation" flags="nn" index="X8dFx" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1167380149909" name="jetbrains.mps.baseLanguage.collections.structure.RemoveElementOperation" flags="nn" index="3dhRuq" />
      <concept id="1178286324487" name="jetbrains.mps.baseLanguage.collections.structure.SortDirection" flags="nn" index="1nlBCl" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1165595910856" name="jetbrains.mps.baseLanguage.collections.structure.GetLastOperation" flags="nn" index="1yVyf7" />
      <concept id="1225727723840" name="jetbrains.mps.baseLanguage.collections.structure.FindFirstOperation" flags="nn" index="1z4cxt" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
      <concept id="1172254888721" name="jetbrains.mps.baseLanguage.collections.structure.ContainsOperation" flags="nn" index="3JPx81" />
    </language>
  </registry>
  <node concept="24kQdi" id="2s5q4UV$4A">
    <ref role="1XX52x" to="4s7a:2s5q4UUeXK" resolve="InclusionDirective" />
    <node concept="3EZMnI" id="2s5q4UV$4F" role="2wV5jI">
      <node concept="2iRfu4" id="2s5q4UV$4G" role="2iSdaV" />
      <node concept="3F0ifn" id="2s5q4UV$4C" role="3EZMnx">
        <property role="3F0ifm" value="#include" />
      </node>
      <node concept="1QoScp" id="2s5q4UV$5g" role="3EZMnx">
        <property role="1QpmdY" value="true" />
        <node concept="15ARfc" id="2s5q4UVByz" role="3F10Kt">
          <property role="3$6WeP" value="0" />
        </node>
        <node concept="3F0ifn" id="2s5q4UV$lQ" role="1QoS34">
          <property role="3F0ifm" value="&lt;" />
          <node concept="11LMrY" id="2s5q4UXtVj" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="pkWqt" id="2s5q4UV$5j" role="3e4ffs">
          <node concept="3clFbS" id="2s5q4UV$5l" role="2VODD2">
            <node concept="3clFbF" id="2s5q4UV$6y" role="3cqZAp">
              <node concept="2OqwBi" id="2s5q4UV$99" role="3clFbG">
                <node concept="pncrf" id="2s5q4UV$6x" role="2Oq$k0" />
                <node concept="3TrcHB" id="2s5q4UV$hX" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2s5q4UVw4_" resolve="angledBracket" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F0ifn" id="2s5q4UV$ny" role="1QoVPY">
          <property role="3F0ifm" value="&quot;" />
          <node concept="15ARfc" id="2s5q4UVASz" role="3F10Kt">
            <property role="3$6WeP" value="0" />
          </node>
          <node concept="11LMrY" id="2s5q4UXtX0" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="3F0A7n" id="2s5q4UV$52" role="3EZMnx">
        <ref role="1NtTu8" to="4s7a:2s5q4UUeXL" resolve="filename" />
      </node>
      <node concept="1QoScp" id="2s5q4UV$qa" role="3EZMnx">
        <property role="1QpmdY" value="true" />
        <node concept="3F0ifn" id="2s5q4UV$qb" role="1QoS34">
          <property role="3F0ifm" value="&gt;" />
          <node concept="11L4FC" id="2s5q4UXtYG" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="pkWqt" id="2s5q4UV$qc" role="3e4ffs">
          <node concept="3clFbS" id="2s5q4UV$qd" role="2VODD2">
            <node concept="3clFbF" id="2s5q4UV$qe" role="3cqZAp">
              <node concept="2OqwBi" id="2s5q4UV$qf" role="3clFbG">
                <node concept="pncrf" id="2s5q4UV$qg" role="2Oq$k0" />
                <node concept="3TrcHB" id="2s5q4UV$qh" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2s5q4UVw4_" resolve="angledBracket" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F0ifn" id="2s5q4UV$qi" role="1QoVPY">
          <property role="3F0ifm" value="&quot;" />
          <node concept="11L4FC" id="2s5q4UXu0p" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2s5q4UVEuY">
    <ref role="1XX52x" to="4s7a:2s5q4UVEuQ" resolve="MacroDefine" />
    <node concept="3EZMnI" id="2s5q4UXAbZ" role="2wV5jI">
      <node concept="2iRfu4" id="2s5q4UXAc0" role="2iSdaV" />
      <node concept="3F0ifn" id="2s5q4UVEv0" role="3EZMnx">
        <property role="3F0ifm" value="#define" />
      </node>
      <node concept="3F0A7n" id="2s5q4UXAc8" role="3EZMnx">
        <ref role="1NtTu8" to="4s7a:2s5q4UXAbU" resolve="name" />
      </node>
      <node concept="3EZMnI" id="2GhJDbDlGJW" role="3EZMnx">
        <node concept="3F0ifn" id="2GhJDbDlGK8" role="3EZMnx">
          <property role="3F0ifm" value="(" />
        </node>
        <node concept="3F2HdR" id="2GhJDbDlGJN" role="3EZMnx">
          <property role="2czwfO" value=", " />
          <ref role="1NtTu8" to="4s7a:2GhJDbDlGI4" resolve="args" />
          <node concept="2iRfu4" id="2GhJDbDlGJP" role="2czzBx" />
        </node>
        <node concept="3F0ifn" id="2GhJDbDlGKc" role="3EZMnx">
          <property role="3F0ifm" value=")" />
        </node>
        <node concept="pkWqt" id="2GhJDbDlGKh" role="pqm2j">
          <node concept="3clFbS" id="2GhJDbDlGKi" role="2VODD2">
            <node concept="3clFbF" id="2GhJDbDlGQF" role="3cqZAp">
              <node concept="2OqwBi" id="2GhJDbDlHw9" role="3clFbG">
                <node concept="2OqwBi" id="2GhJDbDlGTi" role="2Oq$k0">
                  <node concept="pncrf" id="2GhJDbDlGQE" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="2GhJDbDlH2I" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:2GhJDbDlGI4" resolve="args" />
                  </node>
                </node>
                <node concept="3GX2aA" id="2GhJDbDlJ5j" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2iRfu4" id="2GhJDbDm1Du" role="2iSdaV" />
      </node>
      <node concept="3F2HdR" id="2GhJDbDkLFm" role="3EZMnx">
        <ref role="1NtTu8" to="4s7a:2GhJDbDkLt$" resolve="content" />
        <node concept="2iRkQZ" id="2GhJDbDkLFT" role="2czzBx" />
        <node concept="3F0ifn" id="1JUF0Zp$7EP" role="2czzBI" />
      </node>
      <node concept="3F0A7n" id="7Qhq5H9yjhC" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="4s7a:7Qhq5H9yj7t" resolve="comment" />
        <node concept="VechU" id="7Qhq5H9ypup" role="3F10Kt">
          <property role="Vb096" value="gray" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2s5q4UVYvt">
    <ref role="1XX52x" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="3EZMnI" id="5a5L3hY9mHE" role="2wV5jI">
      <ref role="1ERwB7" node="2NOvNR_qwA8" resolve="MyActionMap" />
      <node concept="2iRfu4" id="5a5L3hY9mHF" role="2iSdaV" />
      <node concept="3F0A7n" id="2s5q4UVYvv" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <property role="1O74Pk" value="true" />
        <property role="1cu_pB" value="2" />
        <ref role="1NtTu8" to="4s7a:2s5q4UUuYU" resolve="text" />
        <ref role="34QXea" node="4ZLhJCqOuuf" resolve="MyTextContentKeyMap" />
        <ref role="1ERwB7" node="2NOvNR_qwA8" resolve="MyActionMap" />
        <node concept="Veino" id="5a5L3hY98WC" role="3F10Kt">
          <node concept="3ZlJ5R" id="5a5L3hY9bqT" role="VblUZ">
            <node concept="3clFbS" id="5a5L3hY9bqU" role="2VODD2">
              <node concept="3clFbH" id="5epN3$Z4Clu" role="3cqZAp" />
              <node concept="3clFbJ" id="5epN3$Z4DMY" role="3cqZAp">
                <node concept="3clFbS" id="5epN3$Z4DN0" role="3clFbx">
                  <node concept="3cpWs6" id="5epN3$Z4F3u" role="3cqZAp">
                    <node concept="2ShNRf" id="5epN3$Z4F3v" role="3cqZAk">
                      <node concept="1pGfFk" id="5epN3$Z4F3w" role="2ShVmc">
                        <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                        <node concept="3cmrfG" id="5epN3$Z4F3x" role="37wK5m">
                          <property role="3cmrfH" value="66" />
                        </node>
                        <node concept="3cmrfG" id="5epN3$Z4F3y" role="37wK5m">
                          <property role="3cmrfH" value="134" />
                        </node>
                        <node concept="3cmrfG" id="5epN3$Z4F3z" role="37wK5m">
                          <property role="3cmrfH" value="244" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="5epN3$Z4E9m" role="3clFbw">
                  <node concept="pncrf" id="5epN3$Z4DVw" role="2Oq$k0" />
                  <node concept="3TrcHB" id="5epN3$Z4Eqk" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:38Muwq1QG_A" resolve="isSelected" />
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="5epN3$Z4DkI" role="3cqZAp" />
              <node concept="3cpWs6" id="6F7CfdUZZnL" role="3cqZAp">
                <node concept="10M0yZ" id="6F7CfdV1OSf" role="3cqZAk">
                  <ref role="3cqZAo" to="z60i:~Color.WHITE" resolve="WHITE" />
                  <ref role="1PxDUh" to="z60i:~Color" resolve="Color" />
                </node>
              </node>
              <node concept="3SKdUt" id="6F7CfdUZYEB" role="3cqZAp">
                <node concept="3SKdUq" id="6F7CfdUZYED" role="3SKWNk">
                  <property role="3SKdUp" value="DISABLED" />
                </node>
              </node>
              <node concept="3SKdUt" id="6F7CfdUZYUa" role="3cqZAp">
                <node concept="3SKdUq" id="6F7CfdUZYUb" role="3SKWNk">
                  <property role="3SKdUp" value=" Highlight edited code or show grey background as default" />
                </node>
              </node>
              <node concept="1X3_iC" id="6F7CfdUZZ4B" role="lGtFl">
                <property role="3V$3am" value="statement" />
                <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                <node concept="3clFbJ" id="5a5L3hY9ejD" role="8Wnug">
                  <node concept="3clFbS" id="5a5L3hY9ejE" role="3clFbx">
                    <node concept="3SKdUt" id="5a5L3hY9kKR" role="3cqZAp">
                      <node concept="3SKdUq" id="5a5L3hY9kKT" role="3SKWNk">
                        <property role="3SKdUp" value="Light red" />
                      </node>
                    </node>
                    <node concept="3cpWs6" id="5a5L3hY9jak" role="3cqZAp">
                      <node concept="2ShNRf" id="5a5L3hY9jc6" role="3cqZAk">
                        <node concept="1pGfFk" id="5a5L3hY9ko5" role="2ShVmc">
                          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                          <node concept="3cmrfG" id="5a5L3hY9kq3" role="37wK5m">
                            <property role="3cmrfH" value="255" />
                          </node>
                          <node concept="3cmrfG" id="5a5L3hY9kwj" role="37wK5m">
                            <property role="3cmrfH" value="204" />
                          </node>
                          <node concept="3cmrfG" id="5a5L3hY9kCt" role="37wK5m">
                            <property role="3cmrfH" value="204" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="5a5L3hY9ena" role="3clFbw">
                    <node concept="pncrf" id="5a5L3hY9ekG" role="2Oq$k0" />
                    <node concept="3TrcHB" id="5a5L3hY9evM" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                    </node>
                  </node>
                  <node concept="9aQIb" id="5a5L3hY9ez8" role="9aQIa">
                    <node concept="3clFbS" id="5a5L3hY9ez9" role="9aQI4">
                      <node concept="3cpWs6" id="2bFU2aD4tde" role="3cqZAp">
                        <node concept="2ShNRf" id="2bFU2aD4tdf" role="3cqZAk">
                          <node concept="1pGfFk" id="2bFU2aD4tdg" role="2ShVmc">
                            <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int,int)" resolve="Color" />
                            <node concept="3cmrfG" id="2bFU2aD4tdh" role="37wK5m">
                              <property role="3cmrfH" value="240" />
                            </node>
                            <node concept="3cmrfG" id="2bFU2aD4tdi" role="37wK5m">
                              <property role="3cmrfH" value="240" />
                            </node>
                            <node concept="3cmrfG" id="56tVcQ3iwDC" role="37wK5m">
                              <property role="3cmrfH" value="240" />
                            </node>
                            <node concept="3cmrfG" id="56tVcQ3iMQF" role="37wK5m">
                              <property role="3cmrfH" value="150" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="5a5L3hY9exq" role="3cqZAp" />
            </node>
          </node>
        </node>
        <node concept="3nxI2P" id="2NOvNR_v$fu" role="3F10Kt">
          <property role="VOm3f" value="true" />
          <node concept="3nzxsE" id="2NOvNR_v$pv" role="3n$kyP">
            <node concept="3clFbS" id="2NOvNR_v$pw" role="2VODD2">
              <node concept="3clFbF" id="2NOvNR_v$wE" role="3cqZAp">
                <node concept="2OqwBi" id="2NOvNR_v$FT" role="3clFbG">
                  <node concept="pncrf" id="2NOvNR_v$wD" role="2Oq$k0" />
                  <node concept="3TrcHB" id="2NOvNR_v_cs" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:2NOvNR_vw1V" resolve="isDeleted" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="27yT$n" id="1pcbQKzsAP5" role="3F10Kt">
          <property role="3$6WeP" value="0" />
        </node>
        <node concept="27z8qx" id="1pcbQKzsBtf" role="3F10Kt">
          <property role="3$6WeP" value="0" />
        </node>
      </node>
      <node concept="2SqB2G" id="5a5L3hYf34_" role="2SqHTX">
        <property role="TrG5h" value="testID" />
      </node>
      <node concept="3EZMnI" id="7kI59toz5GE" role="3EZMnx">
        <node concept="2iRfu4" id="7kI59toz5GF" role="2iSdaV" />
        <node concept="3gTLQM" id="38Muwq1Q4Dm" role="3EZMnx">
          <node concept="3Fmcul" id="38Muwq1Q4Do" role="3FoqZy">
            <node concept="3clFbS" id="38Muwq1Q4Dq" role="2VODD2">
              <node concept="3clFbH" id="5i7kGdKzxgH" role="3cqZAp" />
              <node concept="1X3_iC" id="3qld1wemg6A" role="lGtFl">
                <property role="3V$3am" value="statement" />
                <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                <node concept="34ab3g" id="5i7kGdKzPjO" role="8Wnug">
                  <property role="35gtTG" value="warn" />
                  <node concept="3cpWs3" id="5i7kGdKzQl7" role="34bqiv">
                    <node concept="Xl_RD" id="5i7kGdKzPjQ" role="3uHU7B">
                      <property role="Xl_RC" value="canRead: " />
                    </node>
                    <node concept="2OqwBi" id="5i7kGdKzM6U" role="3uHU7w">
                      <node concept="2OqwBi" id="5i7kGdKzJCn" role="2Oq$k0">
                        <node concept="2OqwBi" id="5i7kGdKzIzD" role="2Oq$k0">
                          <node concept="1Q80Hx" id="5i7kGdKzIfS" role="2Oq$k0" />
                          <node concept="liA8E" id="5i7kGdKzJlo" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                          </node>
                        </node>
                        <node concept="liA8E" id="5i7kGdKzKkW" role="2OqNvi">
                          <ref role="37wK5l" to="lui2:~SRepository.getModelAccess():org.jetbrains.mps.openapi.module.ModelAccess" resolve="getModelAccess" />
                        </node>
                      </node>
                      <node concept="liA8E" id="5i7kGdKzMPB" role="2OqNvi">
                        <ref role="37wK5l" to="lui2:~ModelAccess.canRead():boolean" resolve="canRead" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="5i7kGdKzO9L" role="3cqZAp" />
              <node concept="3cpWs8" id="38Muwq1Rihm" role="3cqZAp">
                <node concept="3cpWsn" id="38Muwq1Rihn" role="3cpWs9">
                  <property role="TrG5h" value="checkBox" />
                  <node concept="3uibUv" id="38Muwq1Riho" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
                  </node>
                  <node concept="2ShNRf" id="38Muwq1QbPU" role="33vP2m">
                    <node concept="1pGfFk" id="38Muwq1QWfV" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JCheckBox.&lt;init&gt;(java.lang.String,boolean)" resolve="JCheckBox" />
                      <node concept="Xl_RD" id="38Muwq1QWGE" role="37wK5m" />
                      <node concept="2OqwBi" id="38Muwq1QXRR" role="37wK5m">
                        <node concept="pncrf" id="38Muwq1QXBl" role="2Oq$k0" />
                        <node concept="3TrcHB" id="38Muwq1QYE_" role="2OqNvi">
                          <ref role="3TsBF5" to="4s7a:38Muwq1QG_A" resolve="isSelected" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="56tVcQ3j4g9" role="3cqZAp">
                <node concept="2OqwBi" id="56tVcQ3j58O" role="3clFbG">
                  <node concept="37vLTw" id="56tVcQ3j4g7" role="2Oq$k0">
                    <ref role="3cqZAo" node="38Muwq1Rihn" resolve="checkBox" />
                  </node>
                  <node concept="liA8E" id="56tVcQ3j84$" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~JComponent.setOpaque(boolean):void" resolve="setOpaque" />
                    <node concept="3clFbT" id="56tVcQ3j8OL" role="37wK5m">
                      <property role="3clFbU" value="false" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="38Muwq1RkdF" role="3cqZAp">
                <node concept="2OqwBi" id="38Muwq1RkWU" role="3clFbG">
                  <node concept="37vLTw" id="38Muwq1RkdD" role="2Oq$k0">
                    <ref role="3cqZAo" node="38Muwq1Rihn" resolve="checkBox" />
                  </node>
                  <node concept="liA8E" id="38Muwq1Rnht" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="38Muwq1Rofs" role="37wK5m">
                      <node concept="YeOm9" id="38Muwq1Rq6e" role="2ShVmc">
                        <node concept="1Y3b0j" id="38Muwq1Rq6h" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <node concept="3Tm1VV" id="38Muwq1Rq6i" role="1B3o_S" />
                          <node concept="3clFb_" id="5i7kGdKyaLo" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="5i7kGdKyaLp" role="1B3o_S" />
                            <node concept="3cqZAl" id="5i7kGdKyaLr" role="3clF45" />
                            <node concept="37vLTG" id="5i7kGdKyaLs" role="3clF46">
                              <property role="TrG5h" value="event" />
                              <node concept="3uibUv" id="5i7kGdKyaLt" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="5i7kGdKyaLv" role="3clF47">
                              <node concept="34ab3g" id="5i7kGdKyJjD" role="3cqZAp">
                                <property role="35gtTG" value="warn" />
                                <node concept="Xl_RD" id="5i7kGdKyJjF" role="34bqiv">
                                  <property role="Xl_RC" value="checkbox" />
                                </node>
                              </node>
                              <node concept="3clFbF" id="5i7kGdK$pO7" role="3cqZAp">
                                <node concept="2OqwBi" id="5i7kGdK$rcW" role="3clFbG">
                                  <node concept="2OqwBi" id="5i7kGdK$qRx" role="2Oq$k0">
                                    <node concept="2OqwBi" id="5i7kGdK$q44" role="2Oq$k0">
                                      <node concept="1Q80Hx" id="5i7kGdK$pO5" role="2Oq$k0" />
                                      <node concept="liA8E" id="5i7kGdK$qLr" role="2OqNvi">
                                        <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                      </node>
                                    </node>
                                    <node concept="liA8E" id="5i7kGdK$r5U" role="2OqNvi">
                                      <ref role="37wK5l" to="lui2:~SRepository.getModelAccess():org.jetbrains.mps.openapi.module.ModelAccess" resolve="getModelAccess" />
                                    </node>
                                  </node>
                                  <node concept="liA8E" id="5i7kGdK$rtz" role="2OqNvi">
                                    <ref role="37wK5l" to="lui2:~ModelAccess.executeCommand(java.lang.Runnable):void" resolve="executeCommand" />
                                    <node concept="2ShNRf" id="5i7kGdK$rE4" role="37wK5m">
                                      <node concept="YeOm9" id="5i7kGdK$stg" role="2ShVmc">
                                        <node concept="1Y3b0j" id="5i7kGdK$stj" role="YeSDq">
                                          <property role="2bfB8j" value="true" />
                                          <ref role="1Y3XeK" to="wyt6:~Runnable" resolve="Runnable" />
                                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                                          <node concept="3Tm1VV" id="5i7kGdK$stk" role="1B3o_S" />
                                          <node concept="3clFb_" id="5i7kGdK$stl" role="jymVt">
                                            <property role="1EzhhJ" value="false" />
                                            <property role="TrG5h" value="run" />
                                            <property role="DiZV1" value="false" />
                                            <property role="od$2w" value="false" />
                                            <node concept="3Tm1VV" id="5i7kGdK$stm" role="1B3o_S" />
                                            <node concept="3cqZAl" id="5i7kGdK$sto" role="3clF45" />
                                            <node concept="3clFbS" id="5i7kGdK$stp" role="3clF47">
                                              <node concept="3clFbF" id="5i7kGdK$sXM" role="3cqZAp">
                                                <node concept="2OqwBi" id="5i7kGdK$u5E" role="3clFbG">
                                                  <node concept="2OqwBi" id="5i7kGdK$t5s" role="2Oq$k0">
                                                    <node concept="pncrf" id="5i7kGdK$sXL" role="2Oq$k0" />
                                                    <node concept="3TrcHB" id="5i7kGdK$tDo" role="2OqNvi">
                                                      <ref role="3TsBF5" to="4s7a:38Muwq1QG_A" resolve="isSelected" />
                                                    </node>
                                                  </node>
                                                  <node concept="tyxLq" id="5i7kGdK$um_" role="2OqNvi">
                                                    <node concept="3fqX7Q" id="5i7kGdK$uKc" role="tz02z">
                                                      <node concept="2OqwBi" id="5i7kGdK$vgG" role="3fr31v">
                                                        <node concept="pncrf" id="5i7kGdK$v71" role="2Oq$k0" />
                                                        <node concept="3TrcHB" id="5i7kGdK$vAK" role="2OqNvi">
                                                          <ref role="3TsBF5" to="4s7a:38Muwq1QG_A" resolve="isSelected" />
                                                        </node>
                                                      </node>
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="2AHcQZ" id="5i7kGdKyaLw" role="2AJF6D">
                              <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="38Muwq1RjIS" role="3cqZAp" />
              <node concept="3cpWs6" id="38Muwq1RjJp" role="3cqZAp">
                <node concept="37vLTw" id="38Muwq1RjX1" role="3cqZAk">
                  <ref role="3cqZAo" node="38Muwq1Rihn" resolve="checkBox" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="pkWqt" id="7kI59toz6B7" role="pqm2j">
          <node concept="3clFbS" id="7kI59toz6B8" role="2VODD2">
            <node concept="3clFbF" id="7kI59toz85T" role="3cqZAp">
              <node concept="3clFbT" id="7kI59toz85S" role="3clFbG">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="Veino" id="1UsfiVCPnlb" role="3F10Kt">
        <node concept="3ZlJ5R" id="1UsfiVCPo1r" role="VblUZ">
          <node concept="3clFbS" id="1UsfiVCPo1s" role="2VODD2">
            <node concept="3clFbJ" id="1UsfiVCPo8r" role="3cqZAp">
              <node concept="3eOSWO" id="1UsfiVCPqCK" role="3clFbw">
                <node concept="3cmrfG" id="1UsfiVCPqCQ" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="1UsfiVCPpfn" role="3uHU7B">
                  <node concept="pncrf" id="1UsfiVCPp2m" role="2Oq$k0" />
                  <node concept="3TrcHB" id="1UsfiVCPp$E" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:1UsfiVCPgp0" resolve="chunkID" />
                  </node>
                </node>
              </node>
              <node concept="3clFbS" id="1UsfiVCPo8t" role="3clFbx">
                <node concept="3cpWs6" id="1UsfiVCPNsg" role="3cqZAp">
                  <node concept="2OqwBi" id="1UsfiVCPObt" role="3cqZAk">
                    <node concept="pncrf" id="1UsfiVCPNT4" role="2Oq$k0" />
                    <node concept="2qgKlT" id="1UsfiVCPOFt" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:1UsfiVCPqYb" resolve="getChunkColor" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="9aQIb" id="1UsfiVCPofB" role="9aQIa">
                <node concept="3clFbS" id="1UsfiVCPofC" role="9aQI4">
                  <node concept="3cpWs6" id="1UsfiVCPomG" role="3cqZAp">
                    <node concept="10Nm6u" id="1UsfiVCPomT" role="3cqZAk" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="5a5L3hY9ALS" role="6VMZX">
      <node concept="2iRkQZ" id="5a5L3hY9ALT" role="2iSdaV" />
      <node concept="3EZMnI" id="5a5L3hY8PkL" role="3EZMnx">
        <node concept="2iRfu4" id="5a5L3hY8PkM" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hY8PkJ" role="3EZMnx">
          <property role="3F0ifm" value="isEdit: " />
        </node>
        <node concept="3F0A7n" id="5a5L3hY8Pl0" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
        </node>
      </node>
      <node concept="3EZMnI" id="2NOvNR_vwAW" role="3EZMnx">
        <node concept="2iRfu4" id="2NOvNR_vwAX" role="2iSdaV" />
        <node concept="3F0ifn" id="2NOvNR_vw21" role="3EZMnx">
          <property role="3F0ifm" value="isDeleted:" />
        </node>
        <node concept="3F0A7n" id="2NOvNR_vwOp" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:2NOvNR_vw1V" resolve="isDeleted" />
        </node>
      </node>
      <node concept="1QoScp" id="5a5L3hY9APA" role="3EZMnx">
        <property role="1QpmdY" value="true" />
        <node concept="3EZMnI" id="5a5L3hY9Bfo" role="1QoS34">
          <node concept="2iRfu4" id="5a5L3hY9Bfp" role="2iSdaV" />
          <node concept="3F0ifn" id="5a5L3hY9Bhg" role="3EZMnx">
            <property role="3F0ifm" value="Ambition:" />
          </node>
          <node concept="3F0A7n" id="5a5L3hY9B9I" role="3EZMnx">
            <ref role="1NtTu8" to="4s7a:5a5L3hY9ALO" resolve="ambition" />
          </node>
        </node>
        <node concept="pkWqt" id="5a5L3hY9APD" role="3e4ffs">
          <node concept="3clFbS" id="5a5L3hY9APF" role="2VODD2">
            <node concept="3clFbF" id="5a5L3hY9ASC" role="3cqZAp">
              <node concept="2OqwBi" id="5a5L3hY9AVf" role="3clFbG">
                <node concept="pncrf" id="5a5L3hY9ASB" role="2Oq$k0" />
                <node concept="3TrcHB" id="5a5L3hY9B43" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F0ifn" id="5a5L3hY9ASv" role="1QoVPY" />
      </node>
      <node concept="3EZMnI" id="5a5L3hYbzPr" role="3EZMnx">
        <node concept="2iRfu4" id="5a5L3hYbzPs" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYbzRC" role="3EZMnx">
          <property role="3F0ifm" value="Alignment:" />
        </node>
        <node concept="1HlG4h" id="5a5L3hYbzUE" role="3EZMnx">
          <node concept="1HfYo3" id="5a5L3hYbzUG" role="1HlULh">
            <node concept="3TQlhw" id="5a5L3hYbzUI" role="1Hhtcw">
              <node concept="3clFbS" id="5a5L3hYbzUK" role="2VODD2">
                <node concept="1X3_iC" id="5a5L3hYftU0" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="34ab3g" id="5a5L3hYcyz$" role="8Wnug">
                    <property role="35gtTG" value="warn" />
                    <node concept="3cpWs3" id="5a5L3hYcDn0" role="34bqiv">
                      <node concept="3cpWs3" id="5a5L3hYcC3e" role="3uHU7B">
                        <node concept="3cpWs3" id="5a5L3hYcyUh" role="3uHU7B">
                          <node concept="Xl_RD" id="5a5L3hYcyzA" role="3uHU7B">
                            <property role="Xl_RC" value="Node " />
                          </node>
                          <node concept="2OqwBi" id="5a5L3hYcz6v" role="3uHU7w">
                            <node concept="pncrf" id="5a5L3hYcyZB" role="2Oq$k0" />
                            <node concept="3TrcHB" id="5a5L3hYczjT" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="5a5L3hYcCa1" role="3uHU7w">
                          <property role="Xl_RC" value=" EditorContext " />
                        </node>
                      </node>
                      <node concept="1eOMI4" id="5a5L3hYcN$a" role="3uHU7w">
                        <node concept="3clFbC" id="5a5L3hYcMYR" role="1eOMHV">
                          <node concept="2OqwBi" id="5a5L3hYcDtZ" role="3uHU7B">
                            <node concept="2OqwBi" id="5a5L3hYcDu0" role="2Oq$k0">
                              <node concept="1Q80Hx" id="5a5L3hYcDu1" role="2Oq$k0" />
                              <node concept="liA8E" id="5a5L3hYcDu2" role="2OqNvi">
                                <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                              </node>
                            </node>
                            <node concept="liA8E" id="5a5L3hYcIhd" role="2OqNvi">
                              <ref role="37wK5l" to="cj4x:~EditorComponent.getEditedNode():org.jetbrains.mps.openapi.model.SNode" resolve="getEditedNode" />
                            </node>
                          </node>
                          <node concept="pncrf" id="5a5L3hYcN7N" role="3uHU7w" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYftU1" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYdP9c" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYftU2" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="34ab3g" id="5a5L3hYeC3Q" role="8Wnug">
                    <property role="35gtTG" value="warn" />
                    <node concept="3cpWs3" id="5a5L3hYfoPz" role="34bqiv">
                      <node concept="3cpWs3" id="5a5L3hYforO" role="3uHU7B">
                        <node concept="3cpWs3" id="5a5L3hYeCLw" role="3uHU7B">
                          <node concept="Xl_RD" id="5a5L3hYeC3S" role="3uHU7B">
                            <property role="Xl_RC" value="RootCell X" />
                          </node>
                          <node concept="2OqwBi" id="5a5L3hYeD7q" role="3uHU7w">
                            <node concept="2OqwBi" id="5a5L3hYeCVY" role="2Oq$k0">
                              <node concept="2OqwBi" id="5a5L3hYeCVZ" role="2Oq$k0">
                                <node concept="1Q80Hx" id="5a5L3hYeCW0" role="2Oq$k0" />
                                <node concept="liA8E" id="5a5L3hYeCW1" role="2OqNvi">
                                  <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                                </node>
                              </node>
                              <node concept="liA8E" id="5a5L3hYeCW2" role="2OqNvi">
                                <ref role="37wK5l" to="cj4x:~EditorComponent.getRootCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getRootCell" />
                              </node>
                            </node>
                            <node concept="liA8E" id="5a5L3hYeDn_" role="2OqNvi">
                              <ref role="37wK5l" to="f4zo:~EditorCell.getSNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSNode" />
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="5a5L3hYforU" role="3uHU7w">
                          <property role="Xl_RC" value=": " />
                        </node>
                      </node>
                      <node concept="2OqwBi" id="5a5L3hYfpcQ" role="3uHU7w">
                        <node concept="2OqwBi" id="5a5L3hYfp4X" role="2Oq$k0">
                          <node concept="2OqwBi" id="5a5L3hYfp4Y" role="2Oq$k0">
                            <node concept="1Q80Hx" id="5a5L3hYfp4Z" role="2Oq$k0" />
                            <node concept="liA8E" id="5a5L3hYfp50" role="2OqNvi">
                              <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                            </node>
                          </node>
                          <node concept="liA8E" id="5a5L3hYfp51" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorComponent.getRootCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getRootCell" />
                          </node>
                        </node>
                        <node concept="liA8E" id="5a5L3hYfpnA" role="2OqNvi">
                          <ref role="37wK5l" to="f4zo:~EditorCell.getX():int" resolve="getX" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYftU3" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYfm3f" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYftU4" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYfm3X" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYftU5" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="34ab3g" id="5a5L3hYf5rq" role="8Wnug">
                    <property role="35gtTG" value="warn" />
                    <node concept="3cpWs3" id="5a5L3hYf6AS" role="34bqiv">
                      <node concept="2OqwBi" id="5a5L3hYf7Le" role="3uHU7w">
                        <node concept="2OqwBi" id="5a5L3hYf7h2" role="2Oq$k0">
                          <node concept="1Q80Hx" id="5a5L3hYf73n" role="2Oq$k0" />
                          <node concept="liA8E" id="5a5L3hYf7wo" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                          </node>
                        </node>
                        <node concept="liA8E" id="5a5L3hYf83R" role="2OqNvi">
                          <ref role="37wK5l" to="cj4x:~EditorComponent.findCellWithId(org.jetbrains.mps.openapi.model.SNode,java.lang.String):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findCellWithId" />
                          <node concept="pncrf" id="5a5L3hYf8xH" role="37wK5m" />
                          <node concept="Xl_RD" id="5a5L3hYf94F" role="37wK5m">
                            <property role="Xl_RC" value="testID" />
                          </node>
                        </node>
                      </node>
                      <node concept="Xl_RD" id="5a5L3hYf5rs" role="3uHU7B">
                        <property role="Xl_RC" value="Cell by id " />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXb" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYf58z" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXc" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbJ" id="5a5L3hYeIwY" role="8Wnug">
                    <node concept="3clFbS" id="5a5L3hYeIx0" role="3clFbx">
                      <node concept="3cpWs8" id="5a5L3hYeKCp" role="3cqZAp">
                        <node concept="3cpWsn" id="5a5L3hYeKCs" role="3cpWs9">
                          <property role="TrG5h" value="textContentNode" />
                          <node concept="3Tqbb2" id="5a5L3hYeKCo" role="1tU5fm">
                            <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                          </node>
                          <node concept="1PxgMI" id="5a5L3hYeMAe" role="33vP2m">
                            <property role="1BlNFB" value="true" />
                            <ref role="1m5ApE" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                            <node concept="2OqwBi" id="5a5L3hYeM4E" role="1m5AlR">
                              <node concept="2OqwBi" id="5a5L3hYeM4F" role="2Oq$k0">
                                <node concept="2OqwBi" id="5a5L3hYeM4G" role="2Oq$k0">
                                  <node concept="1Q80Hx" id="5a5L3hYeM4H" role="2Oq$k0" />
                                  <node concept="liA8E" id="5a5L3hYeM4I" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                                  </node>
                                </node>
                                <node concept="liA8E" id="5a5L3hYeM4J" role="2OqNvi">
                                  <ref role="37wK5l" to="cj4x:~EditorComponent.getRootCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getRootCell" />
                                </node>
                              </node>
                              <node concept="liA8E" id="5a5L3hYeM4K" role="2OqNvi">
                                <ref role="37wK5l" to="f4zo:~EditorCell.getSNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSNode" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="34ab3g" id="5a5L3hYeNj0" role="3cqZAp">
                        <property role="35gtTG" value="warn" />
                        <node concept="3cpWs3" id="5a5L3hYeODI" role="34bqiv">
                          <node concept="2OqwBi" id="5a5L3hYeP85" role="3uHU7w">
                            <node concept="37vLTw" id="5a5L3hYeOTA" role="2Oq$k0">
                              <ref role="3cqZAo" node="5a5L3hYeKCs" resolve="textContentNode" />
                            </node>
                            <node concept="3TrcHB" id="5a5L3hYePvw" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                            </node>
                          </node>
                          <node concept="Xl_RD" id="5a5L3hYeNj2" role="3uHU7B">
                            <property role="Xl_RC" value="textContentNode: " />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbC" id="5a5L3hYeJtC" role="3clFbw">
                      <node concept="35c_gC" id="5a5L3hYeJGU" role="3uHU7w">
                        <ref role="35c_gD" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                      </node>
                      <node concept="2OqwBi" id="5a5L3hYeIYl" role="3uHU7B">
                        <node concept="2OqwBi" id="5a5L3hYeILy" role="2Oq$k0">
                          <node concept="2OqwBi" id="5a5L3hYeILz" role="2Oq$k0">
                            <node concept="2OqwBi" id="5a5L3hYeIL$" role="2Oq$k0">
                              <node concept="1Q80Hx" id="5a5L3hYeIL_" role="2Oq$k0" />
                              <node concept="liA8E" id="5a5L3hYeILA" role="2OqNvi">
                                <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                              </node>
                            </node>
                            <node concept="liA8E" id="5a5L3hYeILB" role="2OqNvi">
                              <ref role="37wK5l" to="cj4x:~EditorComponent.getRootCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getRootCell" />
                            </node>
                          </node>
                          <node concept="liA8E" id="5a5L3hYeILC" role="2OqNvi">
                            <ref role="37wK5l" to="f4zo:~EditorCell.getSNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSNode" />
                          </node>
                        </node>
                        <node concept="liA8E" id="5a5L3hYeJgK" role="2OqNvi">
                          <ref role="37wK5l" to="mhbf:~SNode.getConcept():org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXd" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYeIgA" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXe" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3cpWs8" id="5a5L3hYduKP" role="8Wnug">
                    <node concept="3cpWsn" id="5a5L3hYduKQ" role="3cpWs9">
                      <property role="TrG5h" value="foundCell" />
                      <node concept="3uibUv" id="5a5L3hYdwmM" role="1tU5fm">
                        <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXf" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbF" id="5a5L3hYdpIA" role="8Wnug">
                    <node concept="37vLTI" id="5a5L3hYdvDO" role="3clFbG">
                      <node concept="37vLTw" id="5a5L3hYdvXP" role="37vLTJ">
                        <ref role="3cqZAo" node="5a5L3hYduKQ" resolve="foundCell" />
                      </node>
                      <node concept="2YIFZM" id="5a5L3hYdqqC" role="37vLTx">
                        <ref role="1Pybhc" to="g51k:~CellFinderUtil" resolve="CellFinderUtil" />
                        <ref role="37wK5l" to="g51k:~CellFinderUtil.findChildByCondition(jetbrains.mps.openapi.editor.cells.EditorCell,org.jetbrains.mps.util.Condition,boolean,boolean):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findChildByCondition" />
                        <node concept="2OqwBi" id="5a5L3hYdqKw" role="37wK5m">
                          <node concept="2OqwBi" id="5a5L3hYdqCn" role="2Oq$k0">
                            <node concept="1Q80Hx" id="5a5L3hYdqCo" role="2Oq$k0" />
                            <node concept="liA8E" id="5a5L3hYdqCp" role="2OqNvi">
                              <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                            </node>
                          </node>
                          <node concept="liA8E" id="5a5L3hYdqVu" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorComponent.getRootCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getRootCell" />
                          </node>
                        </node>
                        <node concept="2ShNRf" id="5a5L3hYdtqv" role="37wK5m">
                          <node concept="YeOm9" id="5a5L3hYdu73" role="2ShVmc">
                            <node concept="1Y3b0j" id="5a5L3hYdu76" role="YeSDq">
                              <property role="2bfB8j" value="true" />
                              <ref role="1Y3XeK" to="y49u:~Condition" resolve="Condition" />
                              <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                              <node concept="3Tm1VV" id="5a5L3hYdu77" role="1B3o_S" />
                              <node concept="3clFb_" id="5a5L3hYdu78" role="jymVt">
                                <property role="1EzhhJ" value="false" />
                                <property role="TrG5h" value="met" />
                                <property role="DiZV1" value="false" />
                                <property role="od$2w" value="false" />
                                <node concept="3Tm1VV" id="5a5L3hYdu79" role="1B3o_S" />
                                <node concept="10P_77" id="5a5L3hYdu7b" role="3clF45" />
                                <node concept="37vLTG" id="5a5L3hYdu7c" role="3clF46">
                                  <property role="TrG5h" value="p0" />
                                  <node concept="3uibUv" id="5a5L3hYdu7j" role="1tU5fm">
                                    <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                                  </node>
                                </node>
                                <node concept="3clFbS" id="5a5L3hYdu7e" role="3clF47">
                                  <node concept="3clFbF" id="5a5L3hYdEDx" role="3cqZAp">
                                    <node concept="3clFbC" id="5a5L3hYdEDy" role="3clFbG">
                                      <node concept="pncrf" id="5a5L3hYdEDz" role="3uHU7w" />
                                      <node concept="2OqwBi" id="5a5L3hYdED$" role="3uHU7B">
                                        <node concept="37vLTw" id="5a5L3hYdED_" role="2Oq$k0">
                                          <ref role="3cqZAo" node="5a5L3hYdu7c" resolve="p0" />
                                        </node>
                                        <node concept="liA8E" id="5a5L3hYdEDA" role="2OqNvi">
                                          <ref role="37wK5l" to="f4zo:~EditorCell.getSNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSNode" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3uibUv" id="5a5L3hYdu7i" role="2Ghqu4">
                                <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbT" id="5a5L3hYd$C1" role="37wK5m">
                          <property role="3clFbU" value="true" />
                        </node>
                        <node concept="3clFbT" id="5a5L3hYd_42" role="37wK5m">
                          <property role="3clFbU" value="true" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXg" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYdx8p" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXh" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="34ab3g" id="5a5L3hYdxwg" role="8Wnug">
                    <property role="35gtTG" value="warn" />
                    <node concept="3cpWs3" id="5a5L3hYdz4Q" role="34bqiv">
                      <node concept="37vLTw" id="5a5L3hYdze7" role="3uHU7w">
                        <ref role="3cqZAo" node="5a5L3hYduKQ" resolve="foundCell" />
                      </node>
                      <node concept="Xl_RD" id="5a5L3hYdxwi" role="3uHU7B">
                        <property role="Xl_RC" value="foundCell " />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXi" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYeypc" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXj" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbF" id="5a5L3hYey8D" role="8Wnug">
                    <node concept="37vLTI" id="5a5L3hYey8E" role="3clFbG">
                      <node concept="37vLTw" id="5a5L3hYey8F" role="37vLTJ">
                        <ref role="3cqZAo" node="5a5L3hYduKQ" resolve="foundCell" />
                      </node>
                      <node concept="2YIFZM" id="5a5L3hYey8G" role="37vLTx">
                        <ref role="1Pybhc" to="g51k:~CellFinderUtil" resolve="CellFinderUtil" />
                        <ref role="37wK5l" to="g51k:~CellFinderUtil.findChildByCondition(jetbrains.mps.openapi.editor.cells.EditorCell,org.jetbrains.mps.util.Condition,boolean,boolean):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findChildByCondition" />
                        <node concept="2OqwBi" id="5a5L3hYey8H" role="37wK5m">
                          <node concept="2OqwBi" id="5a5L3hYey8I" role="2Oq$k0">
                            <node concept="1Q80Hx" id="5a5L3hYey8J" role="2Oq$k0" />
                            <node concept="liA8E" id="5a5L3hYey8K" role="2OqNvi">
                              <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                            </node>
                          </node>
                          <node concept="liA8E" id="5a5L3hYey8L" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorComponent.getRootCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getRootCell" />
                          </node>
                        </node>
                        <node concept="2ShNRf" id="5a5L3hYey8M" role="37wK5m">
                          <node concept="YeOm9" id="5a5L3hYey8N" role="2ShVmc">
                            <node concept="1Y3b0j" id="5a5L3hYey8O" role="YeSDq">
                              <property role="2bfB8j" value="true" />
                              <ref role="1Y3XeK" to="y49u:~Condition" resolve="Condition" />
                              <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                              <node concept="3Tm1VV" id="5a5L3hYey8P" role="1B3o_S" />
                              <node concept="3clFb_" id="5a5L3hYey8Q" role="jymVt">
                                <property role="1EzhhJ" value="false" />
                                <property role="TrG5h" value="met" />
                                <property role="DiZV1" value="false" />
                                <property role="od$2w" value="false" />
                                <node concept="3Tm1VV" id="5a5L3hYey8R" role="1B3o_S" />
                                <node concept="10P_77" id="5a5L3hYey8S" role="3clF45" />
                                <node concept="37vLTG" id="5a5L3hYey8T" role="3clF46">
                                  <property role="TrG5h" value="p0" />
                                  <node concept="3uibUv" id="5a5L3hYey8U" role="1tU5fm">
                                    <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                                  </node>
                                </node>
                                <node concept="3clFbS" id="5a5L3hYey8V" role="3clF47">
                                  <node concept="3clFbF" id="5a5L3hYey8W" role="3cqZAp">
                                    <node concept="3clFbC" id="5a5L3hYey8X" role="3clFbG">
                                      <node concept="pncrf" id="5a5L3hYey8Y" role="3uHU7w" />
                                      <node concept="2OqwBi" id="5a5L3hYey8Z" role="3uHU7B">
                                        <node concept="37vLTw" id="5a5L3hYey90" role="2Oq$k0">
                                          <ref role="3cqZAo" node="5a5L3hYey8T" resolve="p0" />
                                        </node>
                                        <node concept="liA8E" id="5a5L3hYey91" role="2OqNvi">
                                          <ref role="37wK5l" to="f4zo:~EditorCell.getSNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSNode" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3uibUv" id="5a5L3hYey92" role="2Ghqu4">
                                <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbT" id="5a5L3hYey93" role="37wK5m" />
                        <node concept="3clFbT" id="5a5L3hYey94" role="37wK5m">
                          <property role="3clFbU" value="true" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXk" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYey95" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXl" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="34ab3g" id="5a5L3hYey96" role="8Wnug">
                    <property role="35gtTG" value="warn" />
                    <node concept="3cpWs3" id="5a5L3hYey97" role="34bqiv">
                      <node concept="37vLTw" id="5a5L3hYey98" role="3uHU7w">
                        <ref role="3cqZAo" node="5a5L3hYduKQ" resolve="foundCell" />
                      </node>
                      <node concept="Xl_RD" id="5a5L3hYey99" role="3uHU7B">
                        <property role="Xl_RC" value="foundCell " />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXm" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbH" id="5a5L3hYcZg9" role="8Wnug" />
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXn" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbJ" id="5a5L3hYchQB" role="8Wnug">
                    <node concept="3clFbS" id="5a5L3hYchQD" role="3clFbx">
                      <node concept="3cpWs6" id="5a5L3hYcid1" role="3cqZAp">
                        <node concept="3cpWs3" id="5a5L3hYciG4" role="3cqZAk">
                          <node concept="2OqwBi" id="5a5L3hYcjq_" role="3uHU7w">
                            <node concept="2OqwBi" id="5a5L3hYcjad" role="2Oq$k0">
                              <node concept="2OqwBi" id="5a5L3hYciYN" role="2Oq$k0">
                                <node concept="1Q80Hx" id="5a5L3hYciUi" role="2Oq$k0" />
                                <node concept="liA8E" id="5a5L3hYcj4Y" role="2OqNvi">
                                  <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                                </node>
                              </node>
                              <node concept="liA8E" id="5a5L3hYcjgI" role="2OqNvi">
                                <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                                <node concept="pncrf" id="5a5L3hYcu73" role="37wK5m" />
                              </node>
                            </node>
                            <node concept="liA8E" id="5a5L3hYcjwq" role="2OqNvi">
                              <ref role="37wK5l" to="f4zo:~EditorCell.getX():int" resolve="getX" />
                            </node>
                          </node>
                          <node concept="Xl_RD" id="5a5L3hYcilh" role="3uHU7B">
                            <property role="Xl_RC" value="X_root " />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3y3z36" id="5a5L3hYci4F" role="3clFbw">
                      <node concept="10Nm6u" id="5a5L3hYci8I" role="3uHU7w" />
                      <node concept="2OqwBi" id="5a5L3hYchxo" role="3uHU7B">
                        <node concept="2OqwBi" id="5a5L3hYchn$" role="2Oq$k0">
                          <node concept="1Q80Hx" id="5a5L3hYchjh" role="2Oq$k0" />
                          <node concept="liA8E" id="5a5L3hYchtw" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                          </node>
                        </node>
                        <node concept="liA8E" id="5a5L3hYchEa" role="2OqNvi">
                          <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                          <node concept="pncrf" id="5a5L3hYctM5" role="37wK5m" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1X3_iC" id="5a5L3hYfeXo" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3clFbJ" id="5a5L3hYbQPN" role="8Wnug">
                    <node concept="3clFbS" id="5a5L3hYbQPP" role="3clFbx">
                      <node concept="3cpWs6" id="5a5L3hYbJCr" role="3cqZAp">
                        <node concept="3cpWs3" id="5a5L3hYbJID" role="3cqZAk">
                          <node concept="Xl_RD" id="5a5L3hYbJDL" role="3uHU7B">
                            <property role="Xl_RC" value="X_context: " />
                          </node>
                          <node concept="2OqwBi" id="5a5L3hYbG6E" role="3uHU7w">
                            <node concept="2OqwBi" id="5a5L3hYbFX_" role="2Oq$k0">
                              <node concept="1Q80Hx" id="5a5L3hYbFVr" role="2Oq$k0" />
                              <node concept="liA8E" id="5a5L3hYbG4E" role="2OqNvi">
                                <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                              </node>
                            </node>
                            <node concept="liA8E" id="5a5L3hYbGbh" role="2OqNvi">
                              <ref role="37wK5l" to="f4zo:~EditorCell.getX():int" resolve="getX" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3y3z36" id="5a5L3hYc4Lj" role="3clFbw">
                      <node concept="10Nm6u" id="5a5L3hYc4NH" role="3uHU7w" />
                      <node concept="2OqwBi" id="5a5L3hYc4Ii" role="3uHU7B">
                        <node concept="1Q80Hx" id="5a5L3hYc4Ij" role="2Oq$k0" />
                        <node concept="liA8E" id="5a5L3hYc4Ik" role="2OqNvi">
                          <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                        </node>
                      </node>
                    </node>
                    <node concept="3eNFk2" id="5a5L3hYcbiL" role="3eNLev">
                      <node concept="3clFbS" id="5a5L3hYcbiM" role="3eOfB_">
                        <node concept="3cpWs6" id="5a5L3hYcbK_" role="3cqZAp">
                          <node concept="3cpWs3" id="5a5L3hYcbKA" role="3cqZAk">
                            <node concept="Xl_RD" id="5a5L3hYcbKB" role="3uHU7B">
                              <property role="Xl_RC" value="X: " />
                            </node>
                            <node concept="2OqwBi" id="5a5L3hYcbKC" role="3uHU7w">
                              <node concept="2OqwBi" id="5a5L3hYcbZh" role="2Oq$k0">
                                <node concept="1Q80Hx" id="5a5L3hYcbKE" role="2Oq$k0" />
                                <node concept="liA8E" id="5a5L3hYcc4Q" role="2OqNvi">
                                  <ref role="37wK5l" to="cj4x:~EditorContext.getSelectedCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getSelectedCell" />
                                </node>
                              </node>
                              <node concept="liA8E" id="5a5L3hYcbKG" role="2OqNvi">
                                <ref role="37wK5l" to="f4zo:~EditorCell.getX():int" resolve="getX" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3y3z36" id="5a5L3hYcbEH" role="3eO9$A">
                        <node concept="10Nm6u" id="5a5L3hYcbHu" role="3uHU7w" />
                        <node concept="2OqwBi" id="5a5L3hYcbwF" role="3uHU7B">
                          <node concept="1Q80Hx" id="5a5L3hYcbtS" role="2Oq$k0" />
                          <node concept="liA8E" id="5a5L3hYcb$H" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorContext.getSelectedCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getSelectedCell" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="9aQIb" id="5a5L3hYcblH" role="9aQIa">
                      <node concept="3clFbS" id="5a5L3hYcblI" role="9aQI4">
                        <node concept="3cpWs6" id="5a5L3hYcbiN" role="3cqZAp">
                          <node concept="Xl_RD" id="5a5L3hYcbiO" role="3cqZAk">
                            <property role="Xl_RC" value="&lt;empty&gt;" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs6" id="5a5L3hYffaZ" role="3cqZAp">
                  <node concept="Xl_RD" id="5a5L3hYffb0" role="3cqZAk">
                    <property role="Xl_RC" value="&lt;empty&gt;" />
                  </node>
                </node>
                <node concept="3clFbH" id="5a5L3hYbFP9" role="3cqZAp" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="2NOvNR_v99n" role="3EZMnx">
        <node concept="2iRfu4" id="2NOvNR_v99o" role="2iSdaV" />
        <node concept="3F0ifn" id="2NOvNR_v9mG" role="3EZMnx">
          <property role="3F0ifm" value="old text:" />
        </node>
        <node concept="3F0A7n" id="2NOvNR_v8W8" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:2NOvNR_v8nc" resolve="oldText" />
        </node>
      </node>
      <node concept="3EZMnI" id="2NOvNR_zb1t" role="3EZMnx">
        <node concept="2iRfu4" id="2NOvNR_zb1u" role="2iSdaV" />
        <node concept="3F0ifn" id="2NOvNR_zbeU" role="3EZMnx">
          <property role="3F0ifm" value="isNew" />
        </node>
        <node concept="3F0A7n" id="2NOvNR_zaO6" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:2NOvNR_yT9t" resolve="isNew" />
        </node>
      </node>
      <node concept="3EZMnI" id="38Muwq1QGDU" role="3EZMnx">
        <node concept="2iRfu4" id="38Muwq1QGDV" role="2iSdaV" />
        <node concept="3F0ifn" id="38Muwq1QGDW" role="3EZMnx">
          <property role="3F0ifm" value="isSelected" />
        </node>
        <node concept="3F0A7n" id="38Muwq1QGDX" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:38Muwq1QG_A" resolve="isSelected" />
        </node>
      </node>
      <node concept="3EZMnI" id="1UsfiVCW88h" role="3EZMnx">
        <node concept="2iRfu4" id="1UsfiVCW88i" role="2iSdaV" />
        <node concept="3F0ifn" id="1UsfiVCW88j" role="3EZMnx">
          <property role="3F0ifm" value="chunkID" />
        </node>
        <node concept="3F0A7n" id="1UsfiVCW88k" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:1UsfiVCPgp0" resolve="chunkID" />
        </node>
      </node>
      <node concept="3EZMnI" id="6NCueePQIy3" role="3EZMnx">
        <node concept="2iRfu4" id="6NCueePQIy4" role="2iSdaV" />
        <node concept="3F0ifn" id="6NCueePMeo3" role="3EZMnx">
          <property role="3F0ifm" value="future node" />
        </node>
        <node concept="XafU7" id="6NCueePQIMR" role="3EZMnx">
          <node concept="3TQVft" id="6NCueePQIMT" role="3TRxkO">
            <node concept="3TQlhw" id="6NCueePQIMV" role="3TQWv3">
              <node concept="3clFbS" id="6NCueePQIMX" role="2VODD2">
                <node concept="3clFbJ" id="6NCueePQN31" role="3cqZAp">
                  <node concept="3clFbC" id="6NCueePQOSj" role="3clFbw">
                    <node concept="10Nm6u" id="6NCueePQP2T" role="3uHU7w" />
                    <node concept="2OqwBi" id="6NCueePQNp3" role="3uHU7B">
                      <node concept="pncrf" id="6NCueePQNby" role="2Oq$k0" />
                      <node concept="3TrEf2" id="6NCueePQOsS" role="2OqNvi">
                        <ref role="3Tt5mk" to="4s7a:6NCueePLN3i" resolve="futureNode" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbS" id="6NCueePQN33" role="3clFbx">
                    <node concept="3cpWs6" id="6NCueePQRlB" role="3cqZAp">
                      <node concept="Xl_RD" id="6NCueePQPdn" role="3cqZAk">
                        <property role="Xl_RC" value="no future node" />
                      </node>
                    </node>
                  </node>
                  <node concept="9aQIb" id="6NCueePQQcc" role="9aQIa">
                    <node concept="3clFbS" id="6NCueePQQcd" role="9aQI4">
                      <node concept="3cpWs6" id="6NCueePQRwo" role="3cqZAp">
                        <node concept="Xl_RD" id="6NCueePQQmG" role="3cqZAk">
                          <property role="Xl_RC" value="has future node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3TQsA7" id="6NCueePQIMZ" role="3TQXYj">
              <node concept="3clFbS" id="6NCueePQIN1" role="2VODD2" />
            </node>
            <node concept="3TQwEX" id="6NCueePQIN3" role="3TQZqC">
              <node concept="3clFbS" id="6NCueePQIN5" role="2VODD2">
                <node concept="3cpWs6" id="6NCueePR0kN" role="3cqZAp">
                  <node concept="3clFbT" id="6NCueePR0s5" role="3cqZAk">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6NCueePQIkA" role="3EZMnx" />
    </node>
  </node>
  <node concept="PKFIW" id="2s5q4UYfd6">
    <property role="TrG5h" value="customIndent" />
    <ref role="1XX52x" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
    <node concept="1HlG4h" id="2s5q4UYfd8" role="2wV5jI">
      <node concept="1HfYo3" id="2s5q4UYfda" role="1HlULh">
        <node concept="3TQlhw" id="2s5q4UYfdc" role="1Hhtcw">
          <node concept="3clFbS" id="2s5q4UYfde" role="2VODD2">
            <node concept="3clFbF" id="2s5q4UYfsH" role="3cqZAp">
              <node concept="2OqwBi" id="2s5q4UYfuO" role="3clFbG">
                <node concept="pncrf" id="2s5q4UYfsG" role="2Oq$k0" />
                <node concept="2qgKlT" id="2s5q4UYf_6" role="2OqNvi">
                  <ref role="37wK5l" to="7zqe:2s5q4UYcBn" resolve="getIndent" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3fq3ZRITBy9">
    <ref role="1XX52x" to="4s7a:3fq3ZRIT59K" resolve="MacroElseIf" />
    <node concept="3EZMnI" id="3fq3ZRITByA" role="2wV5jI">
      <property role="S$Qs1" value="true" />
      <node concept="2iRkQZ" id="3fq3ZRITByB" role="2iSdaV" />
      <node concept="3EZMnI" id="3fq3ZRITByl" role="3EZMnx">
        <node concept="2iRfu4" id="3fq3ZRITBym" role="2iSdaV" />
        <node concept="3F0ifn" id="3fq3ZRITByb" role="3EZMnx">
          <property role="3F0ifm" value="#elif" />
        </node>
        <node concept="3F0A7n" id="3fq3ZRITByy" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:3fq3ZRIT59L" resolve="condition" />
        </node>
      </node>
      <node concept="3EZMnI" id="4mTPMz56S5D" role="3EZMnx">
        <node concept="2iRfu4" id="4mTPMz56S5E" role="2iSdaV" />
        <node concept="3F2HdR" id="3fq3ZRITByS" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:3fq3ZRIT59N" resolve="branch" />
          <node concept="2iRkQZ" id="3fq3ZRITByU" role="2czzBx" />
          <node concept="4$FPG" id="6F7CfdVIg63" role="4_6I_">
            <node concept="3clFbS" id="6F7CfdVIg64" role="2VODD2">
              <node concept="3clFbF" id="6F7CfdVIgvL" role="3cqZAp">
                <node concept="2ShNRf" id="6F7CfdVIgvM" role="3clFbG">
                  <node concept="3zrR0B" id="6F7CfdVIgvN" role="2ShVmc">
                    <node concept="3Tqbb2" id="6F7CfdVIgvO" role="3zrR0E">
                      <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="pkWqt" id="3f2$bmXkrwM" role="3EXrW6">
        <node concept="3clFbS" id="3f2$bmXkrwN" role="2VODD2">
          <node concept="3clFbF" id="3f2$bmXkrxS" role="3cqZAp">
            <node concept="1Wc70l" id="3f2$bmXku0p" role="3clFbG">
              <node concept="2OqwBi" id="3f2$bmXkw$7" role="3uHU7w">
                <node concept="2OqwBi" id="3f2$bmXku$I" role="2Oq$k0">
                  <node concept="2OqwBi" id="3f2$bmXku5o" role="2Oq$k0">
                    <node concept="pncrf" id="3f2$bmXku2U" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="3f2$bmXkudc" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:3fq3ZRIT59N" resolve="branch" />
                    </node>
                  </node>
                  <node concept="1uHKPH" id="3f2$bmXkvSt" role="2OqNvi" />
                </node>
                <node concept="3TrcHB" id="3f2$bmXkwGo" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                </node>
              </node>
              <node concept="2OqwBi" id="3f2$bmXks20" role="3uHU7B">
                <node concept="2OqwBi" id="3f2$bmXkr$3" role="2Oq$k0">
                  <node concept="pncrf" id="3f2$bmXkrxR" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="3f2$bmXkrEL" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:3fq3ZRIT59N" resolve="branch" />
                  </node>
                </node>
                <node concept="3GX2aA" id="3f2$bmXktk3" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="79Do2olGUt5" role="6VMZX">
      <node concept="2iRfu4" id="79Do2olGUt6" role="2iSdaV" />
      <node concept="3F0ifn" id="79Do2olGUt7" role="3EZMnx">
        <property role="3F0ifm" value="Condition:" />
      </node>
      <node concept="1HlG4h" id="79Do2olGUt8" role="3EZMnx">
        <node concept="1HfYo3" id="79Do2olGUt9" role="1HlULh">
          <node concept="3TQlhw" id="79Do2olGUta" role="1Hhtcw">
            <node concept="3clFbS" id="79Do2olGUtb" role="2VODD2">
              <node concept="3clFbF" id="79Do2olGUtc" role="3cqZAp">
                <node concept="2OqwBi" id="79Do2olGUtd" role="3clFbG">
                  <node concept="pncrf" id="79Do2olGUte" role="2Oq$k0" />
                  <node concept="2qgKlT" id="79Do2olGUGq" role="2OqNvi">
                    <ref role="37wK5l" to="7zqe:79Do2olGNcB" resolve="buildConstraints" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5a5L3hYakPa">
    <ref role="1XX52x" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="3EZMnI" id="5a5L3hYakQP" role="6VMZX">
      <node concept="2iRkQZ" id="5a5L3hYakQQ" role="2iSdaV" />
      <node concept="3EZMnI" id="5a5L3hYakQR" role="3EZMnx">
        <node concept="2iRfu4" id="5a5L3hYakQS" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYakQT" role="3EZMnx">
          <property role="3F0ifm" value="Condition:" />
        </node>
        <node concept="1HlG4h" id="5a5L3hYakQU" role="3EZMnx">
          <node concept="1HfYo3" id="5a5L3hYakQV" role="1HlULh">
            <node concept="3TQlhw" id="5a5L3hYakQW" role="1Hhtcw">
              <node concept="3clFbS" id="5a5L3hYakQX" role="2VODD2">
                <node concept="3clFbF" id="5a5L3hYakQY" role="3cqZAp">
                  <node concept="2OqwBi" id="5a5L3hYakQZ" role="3clFbG">
                    <node concept="pncrf" id="5a5L3hYakR0" role="2Oq$k0" />
                    <node concept="2qgKlT" id="5a5L3hYakR1" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olGns2" resolve="buildConstraints" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="5a5L3hYakR2" role="3EZMnx">
        <node concept="2iRfu4" id="5a5L3hYakR3" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYakR4" role="3EZMnx">
          <property role="3F0ifm" value="Condition Else:" />
        </node>
        <node concept="1HlG4h" id="5a5L3hYakR5" role="3EZMnx">
          <node concept="1HfYo3" id="5a5L3hYakR6" role="1HlULh">
            <node concept="3TQlhw" id="5a5L3hYakR7" role="1Hhtcw">
              <node concept="3clFbS" id="5a5L3hYakR8" role="2VODD2">
                <node concept="3clFbF" id="5a5L3hYakR9" role="3cqZAp">
                  <node concept="2OqwBi" id="5a5L3hYakRa" role="3clFbG">
                    <node concept="pncrf" id="5a5L3hYakRb" role="2Oq$k0" />
                    <node concept="2qgKlT" id="5a5L3hYakRc" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olH5fT" resolve="conditionElse" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="5gPWvT9VaUS" role="3EZMnx">
        <node concept="2iRfu4" id="5gPWvT9VaUT" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYakRd" role="3EZMnx">
          <property role="3F0ifm" value="Location" />
        </node>
        <node concept="3F0A7n" id="5gPWvT9Vb8h" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:2s5q4UYgRo" resolve="location" />
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="5a5L3hYakPo" role="2wV5jI">
      <node concept="2iRkQZ" id="5a5L3hYakPp" role="2iSdaV" />
      <node concept="3EZMnI" id="5a5L3hYakPq" role="3EZMnx">
        <node concept="2iRfu4" id="5a5L3hYakPr" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYakPs" role="3EZMnx">
          <property role="3F0ifm" value="#if" />
        </node>
        <node concept="3F0A7n" id="5a5L3hYakPt" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:3fq3ZRIT59E" resolve="condition" />
        </node>
      </node>
      <node concept="3EZMnI" id="5a5L3hYakPu" role="3EZMnx">
        <property role="S$Qs1" value="true" />
        <property role="3EXrWe" value="true" />
        <node concept="2iRkQZ" id="5a5L3hYakPv" role="2iSdaV" />
        <node concept="3EZMnI" id="5zZlI40TnId" role="3EZMnx">
          <node concept="l2Vlx" id="5zZlI40TnIe" role="2iSdaV" />
          <node concept="3F2HdR" id="5a5L3hYakPw" role="3EZMnx">
            <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
            <node concept="lj46D" id="7Qhq5H9zS2e" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="pj6Ft" id="7Qhq5H9$R82" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="3F0ifn" id="5a5L3hYakPy" role="3EmGlc">
              <property role="3F0ifm" value="..." />
            </node>
            <node concept="l2Vlx" id="7Qhq5H9zDM0" role="2czzBx" />
            <node concept="4$FPG" id="6F7CfdVGvHk" role="4_6I_">
              <node concept="3clFbS" id="6F7CfdVGvHl" role="2VODD2">
                <node concept="3clFbF" id="6F7CfdVGvLm" role="3cqZAp">
                  <node concept="2ShNRf" id="6F7CfdVGvLk" role="3clFbG">
                    <node concept="3zrR0B" id="6F7CfdVGy7s" role="2ShVmc">
                      <node concept="3Tqbb2" id="6F7CfdVGy7u" role="3zrR0E">
                        <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="pkWqt" id="5a5L3hYakPz" role="3EXrW6">
          <node concept="3clFbS" id="5a5L3hYakP$" role="2VODD2">
            <node concept="3clFbF" id="5a5L3hYakP_" role="3cqZAp">
              <node concept="2OqwBi" id="5a5L3hYakPA" role="3clFbG">
                <node concept="2OqwBi" id="5a5L3hYakPB" role="2Oq$k0">
                  <node concept="2OqwBi" id="5a5L3hYakPC" role="2Oq$k0">
                    <node concept="pncrf" id="5a5L3hYakPD" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="5a5L3hYakPE" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                    </node>
                  </node>
                  <node concept="1uHKPH" id="5a5L3hYakPF" role="2OqNvi" />
                </node>
                <node concept="3TrcHB" id="5a5L3hYakPG" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="5zZlI40TPrW" role="3EZMnx">
        <node concept="lj46D" id="5zZlI40U4LZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="5zZlI40U4M0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="l2Vlx" id="5zZlI40TPrX" role="2iSdaV" />
        <node concept="3F2HdR" id="5a5L3hYakPH" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
          <node concept="lj46D" id="5zZlI40TPRY" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="5zZlI40TPRZ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="2iRkQZ" id="5a5L3hYakPI" role="2czzBx" />
          <node concept="pkWqt" id="5a5L3hYakPJ" role="pqm2j">
            <node concept="3clFbS" id="5a5L3hYakPK" role="2VODD2">
              <node concept="3clFbF" id="5a5L3hYakPL" role="3cqZAp">
                <node concept="2OqwBi" id="5a5L3hYakPM" role="3clFbG">
                  <node concept="2OqwBi" id="5a5L3hYakPN" role="2Oq$k0">
                    <node concept="pncrf" id="5a5L3hYakPO" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="5a5L3hYakPP" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                    </node>
                  </node>
                  <node concept="3GX2aA" id="5a5L3hYakPQ" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="5a5L3hYakPR" role="3EZMnx">
        <property role="S$Qs1" value="true" />
        <node concept="3F0ifn" id="5a5L3hYakPT" role="3EZMnx">
          <property role="3F0ifm" value="#else" />
          <node concept="pkWqt" id="5a5L3hYakPU" role="pqm2j">
            <node concept="3clFbS" id="5a5L3hYakPV" role="2VODD2">
              <node concept="3clFbF" id="5a5L3hYakPW" role="3cqZAp">
                <node concept="2OqwBi" id="5a5L3hYakPX" role="3clFbG">
                  <node concept="2OqwBi" id="5a5L3hYakPY" role="2Oq$k0">
                    <node concept="pncrf" id="5a5L3hYakPZ" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="5a5L3hYakQ0" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                    </node>
                  </node>
                  <node concept="3GX2aA" id="5a5L3hYakQ1" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2iRkQZ" id="5a5L3hYakPS" role="2iSdaV" />
        <node concept="3EZMnI" id="5zZlI40TAck" role="3EZMnx">
          <node concept="l2Vlx" id="5zZlI40TAcl" role="2iSdaV" />
          <node concept="3F2HdR" id="5a5L3hYakQ2" role="3EZMnx">
            <ref role="34QXea" node="4ZLhJCqOuuf" resolve="MyTextContentKeyMap" />
            <ref role="1ERwB7" node="2NOvNR_qwA8" resolve="MyActionMap" />
            <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
            <node concept="lj46D" id="5zZlI40TApF" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="pj6Ft" id="5zZlI40TApG" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="2iRkQZ" id="5a5L3hYakQ3" role="2czzBx" />
            <node concept="4$FPG" id="6F7CfdVIfTZ" role="4_6I_">
              <node concept="3clFbS" id="6F7CfdVIfU0" role="2VODD2">
                <node concept="3clFbF" id="6F7CfdVIfXm" role="3cqZAp">
                  <node concept="2ShNRf" id="6F7CfdVIfXn" role="3clFbG">
                    <node concept="3zrR0B" id="6F7CfdVIfXo" role="2ShVmc">
                      <node concept="3Tqbb2" id="6F7CfdVIfXp" role="3zrR0E">
                        <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="pkWqt" id="1pcbQKzrLu9" role="pqm2j">
            <node concept="3clFbS" id="1pcbQKzrLua" role="2VODD2">
              <node concept="3clFbF" id="1pcbQKzrLTd" role="3cqZAp">
                <node concept="2OqwBi" id="1pcbQKzrLTf" role="3clFbG">
                  <node concept="2OqwBi" id="1pcbQKzrLTg" role="2Oq$k0">
                    <node concept="pncrf" id="1pcbQKzrLTh" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="1pcbQKzrLTi" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                    </node>
                  </node>
                  <node concept="3GX2aA" id="1pcbQKzrLTj" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="pkWqt" id="5a5L3hYakQ4" role="3EXrW6">
          <node concept="3clFbS" id="5a5L3hYakQ5" role="2VODD2">
            <node concept="3clFbF" id="5a5L3hYakQ6" role="3cqZAp">
              <node concept="1Wc70l" id="5a5L3hYakQ7" role="3clFbG">
                <node concept="2OqwBi" id="5a5L3hYakQ8" role="3uHU7B">
                  <node concept="2OqwBi" id="5a5L3hYakQ9" role="2Oq$k0">
                    <node concept="pncrf" id="5a5L3hYakQa" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="5a5L3hYakQb" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                    </node>
                  </node>
                  <node concept="3GX2aA" id="5a5L3hYakQc" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="5a5L3hYakQd" role="3uHU7w">
                  <node concept="2OqwBi" id="5a5L3hYakQe" role="2Oq$k0">
                    <node concept="2OqwBi" id="5a5L3hYakQf" role="2Oq$k0">
                      <node concept="pncrf" id="5a5L3hYakQg" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="5a5L3hYakQh" role="2OqNvi">
                        <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                      </node>
                    </node>
                    <node concept="1uHKPH" id="5a5L3hYakQi" role="2OqNvi" />
                  </node>
                  <node concept="3TrcHB" id="5a5L3hYakQj" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="5a5L3hYakQk" role="3EZMnx">
        <property role="3F0ifm" value="#endif" />
      </node>
    </node>
  </node>
  <node concept="2ABfQD" id="5a5L3hYamhP">
    <property role="TrG5h" value="Demo" />
    <node concept="2BsEeg" id="5a5L3hYamhQ" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="BoxViewForCloneVariability" />
      <property role="2BUmq6" value="Integrated, inlined #ifdefs." />
    </node>
    <node concept="2BsEeg" id="5a5L3hYatMh" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="SideBySideClones" />
      <property role="2BUmq6" value="#ifdef branches shown side-by-side." />
    </node>
    <node concept="2BsEeg" id="16Y907hodbO" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="BoxViewNonClone" />
      <property role="2BUmq6" value="Mainline-only projection." />
    </node>
    <node concept="2BsEeg" id="16Y907hpg$k" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="BoxViewClones" />
      <property role="2BUmq6" value="Fork-only projection." />
    </node>
    <node concept="2BsEeg" id="3LUIgcmu_nc" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="Future" />
      <property role="2BUmq6" value="Preview of intentions." />
    </node>
  </node>
  <node concept="24kQdi" id="5a5L3hYavaS">
    <ref role="1XX52x" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="3EZMnI" id="5a5L3hYavaT" role="2wV5jI">
      <node concept="PMmxH" id="5a5L3hYavaU" role="3EZMnx">
        <ref role="PMmxG" node="2s5q4UYfd6" resolve="customIndent" />
      </node>
      <node concept="2iRfu4" id="5a5L3hYavaV" role="2iSdaV" />
      <node concept="1QoScp" id="5a5L3hYavaW" role="3EZMnx">
        <property role="1QpmdY" value="true" />
        <node concept="pkWqt" id="5a5L3hYavaX" role="3e4ffs">
          <node concept="3clFbS" id="5a5L3hYavaY" role="2VODD2">
            <node concept="3clFbF" id="5a5L3hYavaZ" role="3cqZAp">
              <node concept="1Wc70l" id="5a5L3hYaHoD" role="3clFbG">
                <node concept="2OqwBi" id="3nQIFD_RmKc" role="3uHU7B">
                  <node concept="pncrf" id="3nQIFD_RmyR" role="2Oq$k0" />
                  <node concept="2qgKlT" id="3nQIFD_RnaS" role="2OqNvi">
                    <ref role="37wK5l" to="7zqe:2NfWTcLJRHk" resolve="isCloneIf" />
                  </node>
                </node>
                <node concept="2OqwBi" id="5a5L3hYaHrM" role="3uHU7w">
                  <node concept="2OqwBi" id="5a5L3hYaHrN" role="2Oq$k0">
                    <node concept="pncrf" id="5a5L3hYaHrO" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="5a5L3hYaHrP" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                    </node>
                  </node>
                  <node concept="1v1jN8" id="5a5L3hYaIiX" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3EZMnI" id="5a5L3hYavb6" role="1QoVPY">
          <node concept="2iRkQZ" id="5a5L3hYavb7" role="2iSdaV" />
          <node concept="3EZMnI" id="5a5L3hYavb8" role="3EZMnx">
            <node concept="2iRfu4" id="5a5L3hYavb9" role="2iSdaV" />
            <node concept="3F0ifn" id="5a5L3hYavba" role="3EZMnx">
              <property role="3F0ifm" value="#if" />
            </node>
            <node concept="3F0A7n" id="5a5L3hYavbb" role="3EZMnx">
              <ref role="1NtTu8" to="4s7a:3fq3ZRIT59E" resolve="condition" />
            </node>
          </node>
          <node concept="3EZMnI" id="5a5L3hYavbc" role="3EZMnx">
            <property role="3EXrWe" value="true" />
            <node concept="2iRkQZ" id="5a5L3hYavbd" role="2iSdaV" />
            <node concept="3F2HdR" id="5a5L3hYavbe" role="3EZMnx">
              <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
              <node concept="2iRkQZ" id="5a5L3hYavbf" role="2czzBx" />
              <node concept="3F0ifn" id="5a5L3hYavbg" role="3EmGlc">
                <property role="3F0ifm" value="..." />
              </node>
              <node concept="35HoNQ" id="3nQIFD_TaEr" role="2czzBI" />
              <node concept="4$FPG" id="6F7CfdVIokj" role="4_6I_">
                <node concept="3clFbS" id="6F7CfdVIokk" role="2VODD2">
                  <node concept="3clFbF" id="6F7CfdVIokq" role="3cqZAp">
                    <node concept="2ShNRf" id="6F7CfdVIokr" role="3clFbG">
                      <node concept="3zrR0B" id="6F7CfdVIoks" role="2ShVmc">
                        <node concept="3Tqbb2" id="6F7CfdVIokt" role="3zrR0E">
                          <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="pkWqt" id="5a5L3hYavbh" role="3EXrW6">
              <node concept="3clFbS" id="5a5L3hYavbi" role="2VODD2">
                <node concept="3clFbF" id="5a5L3hYavbj" role="3cqZAp">
                  <node concept="2OqwBi" id="5a5L3hYavbk" role="3clFbG">
                    <node concept="2OqwBi" id="5a5L3hYavbl" role="2Oq$k0">
                      <node concept="2OqwBi" id="5a5L3hYavbm" role="2Oq$k0">
                        <node concept="pncrf" id="5a5L3hYavbn" role="2Oq$k0" />
                        <node concept="3Tsc0h" id="5a5L3hYavbo" role="2OqNvi">
                          <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                        </node>
                      </node>
                      <node concept="1uHKPH" id="5a5L3hYavbp" role="2OqNvi" />
                    </node>
                    <node concept="3TrcHB" id="5a5L3hYavbq" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3F2HdR" id="5a5L3hYavbr" role="3EZMnx">
            <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
            <node concept="2iRkQZ" id="5a5L3hYavbs" role="2czzBx" />
            <node concept="pkWqt" id="5a5L3hYavbt" role="pqm2j">
              <node concept="3clFbS" id="5a5L3hYavbu" role="2VODD2">
                <node concept="3clFbF" id="5a5L3hYavbv" role="3cqZAp">
                  <node concept="2OqwBi" id="5a5L3hYavbw" role="3clFbG">
                    <node concept="2OqwBi" id="5a5L3hYavbx" role="2Oq$k0">
                      <node concept="pncrf" id="5a5L3hYavby" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="5a5L3hYavbz" role="2OqNvi">
                        <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                      </node>
                    </node>
                    <node concept="3GX2aA" id="5a5L3hYavb$" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="35HoNQ" id="3nQIFD_Tawy" role="2czzBI" />
          </node>
          <node concept="3EZMnI" id="5a5L3hYavb_" role="3EZMnx">
            <property role="S$Qs1" value="false" />
            <node concept="2iRkQZ" id="5a5L3hYavbA" role="2iSdaV" />
            <node concept="3F0ifn" id="5a5L3hYavbB" role="3EZMnx">
              <property role="3F0ifm" value="#else" />
            </node>
            <node concept="3F2HdR" id="5a5L3hYavbK" role="3EZMnx">
              <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
              <node concept="2iRkQZ" id="5a5L3hYavbL" role="2czzBx" />
              <node concept="35HoNQ" id="3nQIFD_Tawq" role="2czzBI" />
              <node concept="4$FPG" id="6F7CfdVIoz6" role="4_6I_">
                <node concept="3clFbS" id="6F7CfdVIoz7" role="2VODD2">
                  <node concept="3clFbF" id="6F7CfdVIozd" role="3cqZAp">
                    <node concept="2ShNRf" id="6F7CfdVIoze" role="3clFbG">
                      <node concept="3zrR0B" id="6F7CfdVIozf" role="2ShVmc">
                        <node concept="3Tqbb2" id="6F7CfdVIozg" role="3zrR0E">
                          <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="pkWqt" id="39$s2GVS_dZ" role="pqm2j">
              <node concept="3clFbS" id="39$s2GVS_e0" role="2VODD2">
                <node concept="3clFbF" id="39$s2GVS_o3" role="3cqZAp">
                  <node concept="2OqwBi" id="39$s2GVS_o5" role="3clFbG">
                    <node concept="2OqwBi" id="39$s2GVS_o6" role="2Oq$k0">
                      <node concept="pncrf" id="39$s2GVS_o7" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="39$s2GVS_o8" role="2OqNvi">
                        <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                      </node>
                    </node>
                    <node concept="3GX2aA" id="39$s2GVS_o9" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3F0ifn" id="5a5L3hYavc2" role="3EZMnx">
            <property role="3F0ifm" value="#endif" />
          </node>
        </node>
        <node concept="3EZMnI" id="5a5L3hYavc3" role="1QoS34">
          <node concept="3EZMnI" id="5a5L3hYavAd" role="3EZMnx">
            <node concept="3F0ifn" id="5a5L3hYaMpa" role="3EZMnx">
              <property role="3F0ifm" value=" Mainline" />
              <node concept="Vb9p2" id="5a5L3hYaMAQ" role="3F10Kt">
                <property role="Vbekb" value="ITALIC" />
              </node>
              <node concept="VechU" id="5a5L3hYaMAR" role="3F10Kt">
                <property role="Vb096" value="gray" />
              </node>
              <node concept="VSNWy" id="5a5L3hYaMAS" role="3F10Kt">
                <node concept="1cFabM" id="5a5L3hYaMAT" role="1d8cEk">
                  <node concept="3clFbS" id="5a5L3hYaMAU" role="2VODD2">
                    <node concept="3clFbF" id="5a5L3hYaMAV" role="3cqZAp">
                      <node concept="3cmrfG" id="5a5L3hYaQMf" role="3clFbG">
                        <property role="3cmrfH" value="12" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3EZMnI" id="5XbSbOG7x_b" role="3EZMnx">
              <node concept="3gTLQM" id="5XbSbOG7ytc" role="3EZMnx">
                <node concept="3Fmcul" id="5XbSbOG7yte" role="3FoqZy">
                  <node concept="3clFbS" id="5XbSbOG7ytg" role="2VODD2">
                    <node concept="3cpWs8" id="5XbSbOG7z2t" role="3cqZAp">
                      <node concept="3cpWsn" id="5XbSbOG7z2u" role="3cpWs9">
                        <property role="TrG5h" value="ip" />
                        <node concept="3uibUv" id="5XbSbOG7z2v" role="1tU5fm">
                          <ref role="3uigEE" node="5wTmjWA3CRg" resolve="IntentionIndicatorPanel" />
                        </node>
                        <node concept="2ShNRf" id="5XbSbOG7z2w" role="33vP2m">
                          <node concept="HV5vD" id="5XbSbOG7z2x" role="2ShVmc">
                            <ref role="HV5vE" node="5wTmjWA3CRg" resolve="IntentionIndicatorPanel" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="76MUimxhuuA" role="3cqZAp">
                      <node concept="3clFbS" id="76MUimxhuuC" role="3clFbx">
                        <node concept="3cpWs6" id="5XbSbOG7z2y" role="3cqZAp">
                          <node concept="2OqwBi" id="5XbSbOG7z2z" role="3cqZAk">
                            <node concept="37vLTw" id="5XbSbOG7z2$" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOG7z2u" resolve="ip" />
                            </node>
                            <node concept="liA8E" id="5XbSbOG7z2_" role="2OqNvi">
                              <ref role="37wK5l" node="5XbSbOFT7DE" resolve="getSideBySideEditor" />
                              <node concept="pncrf" id="5XbSbOG7z2A" role="37wK5m" />
                              <node concept="1Q80Hx" id="5XbSbOG7z2B" role="37wK5m" />
                              <node concept="Rm8GO" id="5XbSbOG7z2C" role="37wK5m">
                                <ref role="Rm8GQ" node="5XbSbOFTdc$" resolve="TRUE_BRANCH" />
                                <ref role="1Px2BO" node="5XbSbOFTd8E" resolve="IfdefBranch" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="76MUimxhuLe" role="3clFbw">
                        <node concept="2OqwBi" id="76MUimxhuLf" role="2Oq$k0">
                          <node concept="pncrf" id="76MUimxhuLg" role="2Oq$k0" />
                          <node concept="2qgKlT" id="76MUimxhuLh" role="2OqNvi">
                            <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                            <node concept="Xl_RD" id="76MUimxhuLi" role="37wK5m">
                              <property role="Xl_RC" value="!defined(FORK)" />
                            </node>
                            <node concept="Xl_RD" id="76MUimxhuLj" role="37wK5m">
                              <property role="Xl_RC" value="true" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="76MUimxhuLk" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                          <node concept="Xl_RD" id="76MUimxhuLl" role="37wK5m">
                            <property role="Xl_RC" value="true" />
                          </node>
                        </node>
                      </node>
                      <node concept="9aQIb" id="76MUimxj6JQ" role="9aQIa">
                        <node concept="3clFbS" id="76MUimxj6JR" role="9aQI4">
                          <node concept="3cpWs8" id="76MUimxqrv7" role="3cqZAp">
                            <node concept="3cpWsn" id="76MUimxqrv8" role="3cpWs9">
                              <property role="TrG5h" value="panel" />
                              <node concept="3uibUv" id="76MUimxqrv6" role="1tU5fm">
                                <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
                              </node>
                              <node concept="2ShNRf" id="76MUimxqrv9" role="33vP2m">
                                <node concept="1pGfFk" id="76MUimxqrva" role="2ShVmc">
                                  <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="76MUimxqs8h" role="3cqZAp">
                            <node concept="2OqwBi" id="76MUimxqsRB" role="3clFbG">
                              <node concept="37vLTw" id="76MUimxqs8f" role="2Oq$k0">
                                <ref role="3cqZAo" node="76MUimxqrv8" resolve="panel" />
                              </node>
                              <node concept="liA8E" id="76MUimxqupt" role="2OqNvi">
                                <ref role="37wK5l" to="dxuu:~JComponent.setOpaque(boolean):void" resolve="setOpaque" />
                                <node concept="3clFbT" id="76MUimxquSU" role="37wK5m">
                                  <property role="3clFbU" value="false" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs6" id="76MUimxhv7L" role="3cqZAp">
                            <node concept="37vLTw" id="76MUimxqrvb" role="3cqZAk">
                              <ref role="3cqZAo" node="76MUimxqrv8" resolve="panel" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2iRfu4" id="5XbSbOG7x_c" role="2iSdaV" />
              <node concept="1QoScp" id="1UsfiVCVebm" role="3EZMnx">
                <property role="1QpmdY" value="true" />
                <node concept="3F2HdR" id="1UsfiVCVebn" role="1QoS34">
                  <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                  <node concept="2iRkQZ" id="1UsfiVCVebo" role="2czzBx" />
                  <node concept="35HoNQ" id="3nQIFD_T9Md" role="2czzBI" />
                  <node concept="4$FPG" id="6F7CfdVIkPB" role="4_6I_">
                    <node concept="3clFbS" id="6F7CfdVIkPC" role="2VODD2">
                      <node concept="3clFbF" id="6F7CfdVIl_J" role="3cqZAp">
                        <node concept="2ShNRf" id="6F7CfdVIl_K" role="3clFbG">
                          <node concept="3zrR0B" id="6F7CfdVIl_L" role="2ShVmc">
                            <node concept="3Tqbb2" id="6F7CfdVIl_M" role="3zrR0E">
                              <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="pkWqt" id="1UsfiVCVebp" role="3e4ffs">
                  <node concept="3clFbS" id="1UsfiVCVebq" role="2VODD2">
                    <node concept="3clFbF" id="3nQIFD_SaT_" role="3cqZAp">
                      <node concept="2OqwBi" id="3nQIFD_SaTB" role="3clFbG">
                        <node concept="2OqwBi" id="3nQIFD_SaTC" role="2Oq$k0">
                          <node concept="pncrf" id="3nQIFD_SaTD" role="2Oq$k0" />
                          <node concept="2qgKlT" id="3nQIFD_SaTE" role="2OqNvi">
                            <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                            <node concept="Xl_RD" id="3nQIFD_SaTF" role="37wK5m">
                              <property role="Xl_RC" value="!defined(FORK)" />
                            </node>
                            <node concept="Xl_RD" id="3nQIFD_SaTG" role="37wK5m">
                              <property role="Xl_RC" value="true" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="3nQIFD_SaTH" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                          <node concept="Xl_RD" id="3nQIFD_SaTI" role="37wK5m">
                            <property role="Xl_RC" value="true" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3EZMnI" id="1UsfiVCVeby" role="1QoVPY">
                  <node concept="3EZMnI" id="3nQIFD_S9lA" role="3EZMnx">
                    <node concept="3EZMnI" id="3nQIFD_S9lB" role="3EZMnx">
                      <node concept="2iRfu4" id="3nQIFD_S9lC" role="2iSdaV" />
                      <node concept="3F0ifn" id="3nQIFD_S9lD" role="3EZMnx">
                        <property role="3F0ifm" value="#if" />
                      </node>
                      <node concept="1HlG4h" id="3nQIFD_S9lE" role="3EZMnx">
                        <node concept="1HfYo3" id="3nQIFD_S9lF" role="1HlULh">
                          <node concept="3TQlhw" id="3nQIFD_S9lG" role="1Hhtcw">
                            <node concept="3clFbS" id="3nQIFD_S9lH" role="2VODD2">
                              <node concept="3clFbF" id="3nQIFD_S9lI" role="3cqZAp">
                                <node concept="2OqwBi" id="3nQIFD_S9lJ" role="3clFbG">
                                  <node concept="pncrf" id="3nQIFD_S9lK" role="2Oq$k0" />
                                  <node concept="2qgKlT" id="3nQIFD_S9lL" role="2OqNvi">
                                    <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                    <node concept="Xl_RD" id="3nQIFD_S9lM" role="37wK5m">
                                      <property role="Xl_RC" value="!defined(FORK)" />
                                    </node>
                                    <node concept="Xl_RD" id="3nQIFD_S9lN" role="37wK5m">
                                      <property role="Xl_RC" value="true" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2iRkQZ" id="3nQIFD_S9lO" role="2iSdaV" />
                    <node concept="3F2HdR" id="3nQIFD_S9lP" role="3EZMnx">
                      <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                      <node concept="2iRkQZ" id="3nQIFD_S9lQ" role="2czzBx" />
                      <node concept="35HoNQ" id="3nQIFD_T9M5" role="2czzBI" />
                      <node concept="4$FPG" id="6F7CfdVImQO" role="4_6I_">
                        <node concept="3clFbS" id="6F7CfdVImQP" role="2VODD2">
                          <node concept="3clFbF" id="6F7CfdVImUn" role="3cqZAp">
                            <node concept="2ShNRf" id="6F7CfdVImUo" role="3clFbG">
                              <node concept="3zrR0B" id="6F7CfdVImUp" role="2ShVmc">
                                <node concept="3Tqbb2" id="6F7CfdVImUq" role="3zrR0E">
                                  <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="pkWqt" id="3nQIFD_S9lR" role="pqm2j">
                      <node concept="3clFbS" id="3nQIFD_S9lS" role="2VODD2">
                        <node concept="3clFbF" id="3nQIFD_S9lT" role="3cqZAp">
                          <node concept="17QLQc" id="3nQIFD_S9lU" role="3clFbG">
                            <node concept="Xl_RD" id="3nQIFD_S9lV" role="3uHU7w">
                              <property role="Xl_RC" value="false" />
                            </node>
                            <node concept="2OqwBi" id="3nQIFD_S9lW" role="3uHU7B">
                              <node concept="pncrf" id="3nQIFD_S9lX" role="2Oq$k0" />
                              <node concept="2qgKlT" id="3nQIFD_S9lY" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="3nQIFD_S9lZ" role="37wK5m">
                                  <property role="Xl_RC" value="!defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="3nQIFD_S9m0" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2iRkQZ" id="1UsfiVCVebz" role="2iSdaV" />
                  <node concept="3F2HdR" id="1UsfiVCVeb$" role="3EZMnx">
                    <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                    <node concept="2iRkQZ" id="1UsfiVCVeb_" role="2czzBx" />
                    <node concept="pkWqt" id="1UsfiVCVebA" role="pqm2j">
                      <node concept="3clFbS" id="1UsfiVCVebB" role="2VODD2">
                        <node concept="3clFbF" id="1UsfiVCVebC" role="3cqZAp">
                          <node concept="2OqwBi" id="1UsfiVCVebD" role="3clFbG">
                            <node concept="2OqwBi" id="1UsfiVCVebE" role="2Oq$k0">
                              <node concept="pncrf" id="1UsfiVCVebF" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="1UsfiVCVebG" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="1UsfiVCVebH" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="35HoNQ" id="3nQIFD_T9Cc" role="2czzBI" />
                  </node>
                  <node concept="3F2HdR" id="1UsfiVCVebI" role="3EZMnx">
                    <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                    <node concept="2iRkQZ" id="1UsfiVCVebJ" role="2czzBx" />
                    <node concept="35HoNQ" id="3nQIFD_SN_K" role="2czzBI" />
                    <node concept="pkWqt" id="3nQIFD_TuNC" role="pqm2j">
                      <node concept="3clFbS" id="3nQIFD_TuND" role="2VODD2">
                        <node concept="3clFbF" id="3nQIFD_TuUM" role="3cqZAp">
                          <node concept="2OqwBi" id="3nQIFD_Txpa" role="3clFbG">
                            <node concept="2OqwBi" id="3nQIFD_Tv7J" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_TuUL" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="3nQIFD_TvVq" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="3nQIFD_TzUG" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="4$FPG" id="6F7CfdVIn34" role="4_6I_">
                      <node concept="3clFbS" id="6F7CfdVIn35" role="2VODD2">
                        <node concept="3clFbF" id="6F7CfdVInga" role="3cqZAp">
                          <node concept="2ShNRf" id="6F7CfdVIngb" role="3clFbG">
                            <node concept="3zrR0B" id="6F7CfdVIngc" role="2ShVmc">
                              <node concept="3Tqbb2" id="6F7CfdVIngd" role="3zrR0E">
                                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3EZMnI" id="3nQIFD_SP2p" role="3EZMnx">
                    <node concept="2iRkQZ" id="3nQIFD_SP2q" role="2iSdaV" />
                    <node concept="pkWqt" id="3nQIFD_SP2r" role="pqm2j">
                      <node concept="3clFbS" id="3nQIFD_SP2s" role="2VODD2">
                        <node concept="3clFbF" id="3nQIFD_SP2t" role="3cqZAp">
                          <node concept="17QLQc" id="3nQIFD_SP2u" role="3clFbG">
                            <node concept="Xl_RD" id="3nQIFD_SP2v" role="3uHU7w">
                              <property role="Xl_RC" value="false" />
                            </node>
                            <node concept="2OqwBi" id="3nQIFD_SP2w" role="3uHU7B">
                              <node concept="pncrf" id="3nQIFD_SP2x" role="2Oq$k0" />
                              <node concept="2qgKlT" id="3nQIFD_SP2y" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="3nQIFD_SP2z" role="37wK5m">
                                  <property role="Xl_RC" value="!defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="3nQIFD_SP2$" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3F0ifn" id="3nQIFD_SP2_" role="3EZMnx">
                      <property role="3F0ifm" value="#endif" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2iRkQZ" id="5a5L3hYavAe" role="2iSdaV" />
            <node concept="VPXOz" id="5a5L3hYaCJe" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
          </node>
          <node concept="2iRfu4" id="5a5L3hYavA8" role="2iSdaV" />
          <node concept="3EZMnI" id="5a5L3hYavNq" role="3EZMnx">
            <property role="S$Qs1" value="false" />
            <property role="3EXrWe" value="true" />
            <node concept="3F0ifn" id="5a5L3hYaMpi" role="3EZMnx">
              <property role="3F0ifm" value=" Fork" />
              <node concept="Vb9p2" id="5a5L3hYaMty" role="3F10Kt">
                <property role="Vbekb" value="ITALIC" />
              </node>
              <node concept="VechU" id="5a5L3hYaMvj" role="3F10Kt">
                <property role="Vb096" value="gray" />
              </node>
              <node concept="VSNWy" id="5a5L3hYaMx9" role="3F10Kt">
                <node concept="1cFabM" id="5a5L3hYaMxh" role="1d8cEk">
                  <node concept="3clFbS" id="5a5L3hYaMxi" role="2VODD2">
                    <node concept="3clFbF" id="5a5L3hYaMzY" role="3cqZAp">
                      <node concept="3cmrfG" id="5a5L3hYfuxN" role="3clFbG">
                        <property role="3cmrfH" value="12" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2iRkQZ" id="5a5L3hYavNr" role="2iSdaV" />
            <node concept="3EZMnI" id="5XbSbOG1qZy" role="3EZMnx">
              <node concept="3gTLQM" id="5XbSbOG1rUh" role="3EZMnx">
                <node concept="3Fmcul" id="5XbSbOG1rUj" role="3FoqZy">
                  <node concept="3clFbS" id="5XbSbOG1rUl" role="2VODD2">
                    <node concept="3cpWs8" id="5XbSbOG1sv$" role="3cqZAp">
                      <node concept="3cpWsn" id="5XbSbOG1sv_" role="3cpWs9">
                        <property role="TrG5h" value="ip" />
                        <node concept="3uibUv" id="5XbSbOG1svA" role="1tU5fm">
                          <ref role="3uigEE" node="5wTmjWA3CRg" resolve="IntentionIndicatorPanel" />
                        </node>
                        <node concept="2ShNRf" id="5XbSbOG1svB" role="33vP2m">
                          <node concept="HV5vD" id="5XbSbOG1svC" role="2ShVmc">
                            <ref role="HV5vE" node="5wTmjWA3CRg" resolve="IntentionIndicatorPanel" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="76MUimxfOsb" role="3cqZAp">
                      <node concept="3clFbS" id="76MUimxfOsd" role="3clFbx">
                        <node concept="3cpWs6" id="76MUimxfQri" role="3cqZAp">
                          <node concept="2OqwBi" id="76MUimxfR7n" role="3cqZAk">
                            <node concept="37vLTw" id="76MUimxfQGf" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOG1sv_" resolve="ip" />
                            </node>
                            <node concept="liA8E" id="76MUimxfRuV" role="2OqNvi">
                              <ref role="37wK5l" node="5XbSbOFT7DE" resolve="getSideBySideEditor" />
                              <node concept="pncrf" id="76MUimxfRSQ" role="37wK5m" />
                              <node concept="1Q80Hx" id="76MUimxfT8k" role="37wK5m" />
                              <node concept="Rm8GO" id="76MUimxfUhB" role="37wK5m">
                                <ref role="Rm8GQ" node="5XbSbOFTdc$" resolve="TRUE_BRANCH" />
                                <ref role="1Px2BO" node="5XbSbOFTd8E" resolve="IfdefBranch" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="76MUimxfOCa" role="3clFbw">
                        <node concept="2OqwBi" id="76MUimxfOCb" role="2Oq$k0">
                          <node concept="pncrf" id="76MUimxfOCc" role="2Oq$k0" />
                          <node concept="2qgKlT" id="76MUimxfOCd" role="2OqNvi">
                            <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                            <node concept="Xl_RD" id="76MUimxfOCe" role="37wK5m">
                              <property role="Xl_RC" value="defined(FORK)" />
                            </node>
                            <node concept="Xl_RD" id="76MUimxfOCf" role="37wK5m">
                              <property role="Xl_RC" value="true" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="76MUimxfOCg" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                          <node concept="Xl_RD" id="76MUimxfOCh" role="37wK5m">
                            <property role="Xl_RC" value="true" />
                          </node>
                        </node>
                      </node>
                      <node concept="9aQIb" id="76MUimxfPRz" role="9aQIa">
                        <node concept="3clFbS" id="76MUimxfPR$" role="9aQI4">
                          <node concept="3cpWs6" id="5XbSbOG1svD" role="3cqZAp">
                            <node concept="2OqwBi" id="5XbSbOG1svE" role="3cqZAk">
                              <node concept="37vLTw" id="5XbSbOG1svF" role="2Oq$k0">
                                <ref role="3cqZAo" node="5XbSbOG1sv_" resolve="ip" />
                              </node>
                              <node concept="liA8E" id="5XbSbOG1svG" role="2OqNvi">
                                <ref role="37wK5l" node="5XbSbOFT7DE" resolve="getSideBySideEditor" />
                                <node concept="pncrf" id="5XbSbOG1svH" role="37wK5m" />
                                <node concept="1Q80Hx" id="5XbSbOG1svI" role="37wK5m" />
                                <node concept="Rm8GO" id="5XbSbOG1svJ" role="37wK5m">
                                  <ref role="Rm8GQ" node="5XbSbOFTdd$" resolve="ELSE_BRANCH" />
                                  <ref role="1Px2BO" node="5XbSbOFTd8E" resolve="IfdefBranch" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2iRfu4" id="5XbSbOG1qZz" role="2iSdaV" />
              <node concept="1QoScp" id="1UsfiVCVdnI" role="3EZMnx">
                <property role="1QpmdY" value="true" />
                <node concept="3EZMnI" id="39$s2GVQ9Gk" role="1QoS34">
                  <node concept="2iRkQZ" id="39$s2GVQ9Gl" role="2iSdaV" />
                  <node concept="3F2HdR" id="1UsfiVCVdnJ" role="3EZMnx">
                    <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                    <node concept="2iRkQZ" id="1UsfiVCVdnK" role="2czzBx" />
                    <node concept="35HoNQ" id="3nQIFD_T9Mf" role="2czzBI" />
                    <node concept="4$FPG" id="6F7CfdVInsz" role="4_6I_">
                      <node concept="3clFbS" id="6F7CfdVIns$" role="2VODD2">
                        <node concept="3clFbF" id="6F7CfdVIn_U" role="3cqZAp">
                          <node concept="2ShNRf" id="6F7CfdVIn_V" role="3clFbG">
                            <node concept="3zrR0B" id="6F7CfdVIn_W" role="2ShVmc">
                              <node concept="3Tqbb2" id="6F7CfdVIn_X" role="3zrR0E">
                                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="pkWqt" id="1UsfiVCVdnL" role="3e4ffs">
                  <node concept="3clFbS" id="1UsfiVCVdnM" role="2VODD2">
                    <node concept="3clFbF" id="3nQIFD_Royj" role="3cqZAp">
                      <node concept="2OqwBi" id="3nQIFD_Rpv3" role="3clFbG">
                        <node concept="2OqwBi" id="3nQIFD_Royl" role="2Oq$k0">
                          <node concept="pncrf" id="3nQIFD_Roym" role="2Oq$k0" />
                          <node concept="2qgKlT" id="3nQIFD_Royn" role="2OqNvi">
                            <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                            <node concept="Xl_RD" id="3nQIFD_Royo" role="37wK5m">
                              <property role="Xl_RC" value="defined(FORK)" />
                            </node>
                            <node concept="Xl_RD" id="3nQIFD_Royp" role="37wK5m">
                              <property role="Xl_RC" value="true" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="3nQIFD_Rqhe" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                          <node concept="Xl_RD" id="3nQIFD_Rqtt" role="37wK5m">
                            <property role="Xl_RC" value="true" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3EZMnI" id="1UsfiVCVdnU" role="1QoVPY">
                  <node concept="3EZMnI" id="3nQIFD_RrKh" role="3EZMnx">
                    <node concept="3EZMnI" id="3nQIFD_RrU_" role="3EZMnx">
                      <node concept="2iRfu4" id="3nQIFD_RrUA" role="2iSdaV" />
                      <node concept="3F0ifn" id="3nQIFD_RrUm" role="3EZMnx">
                        <property role="3F0ifm" value="#if" />
                      </node>
                      <node concept="1HlG4h" id="3nQIFD_RrUK" role="3EZMnx">
                        <node concept="1HfYo3" id="3nQIFD_RrUM" role="1HlULh">
                          <node concept="3TQlhw" id="3nQIFD_RrUO" role="1Hhtcw">
                            <node concept="3clFbS" id="3nQIFD_RrUQ" role="2VODD2">
                              <node concept="3clFbF" id="3nQIFD_Rs3l" role="3cqZAp">
                                <node concept="2OqwBi" id="3nQIFD_Rs3n" role="3clFbG">
                                  <node concept="pncrf" id="3nQIFD_Rs3o" role="2Oq$k0" />
                                  <node concept="2qgKlT" id="3nQIFD_Rs3p" role="2OqNvi">
                                    <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                    <node concept="Xl_RD" id="3nQIFD_Rs3q" role="37wK5m">
                                      <property role="Xl_RC" value="defined(FORK)" />
                                    </node>
                                    <node concept="Xl_RD" id="3nQIFD_Rs3r" role="37wK5m">
                                      <property role="Xl_RC" value="true" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2iRkQZ" id="3nQIFD_RrKi" role="2iSdaV" />
                    <node concept="3F2HdR" id="3nQIFD_Rrib" role="3EZMnx">
                      <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                      <node concept="2iRkQZ" id="3nQIFD_Rrid" role="2czzBx" />
                      <node concept="35HoNQ" id="3nQIFD_T9Mh" role="2czzBI" />
                      <node concept="4$FPG" id="6F7CfdVInI$" role="4_6I_">
                        <node concept="3clFbS" id="6F7CfdVInI_" role="2VODD2">
                          <node concept="3clFbF" id="6F7CfdVInM7" role="3cqZAp">
                            <node concept="2ShNRf" id="6F7CfdVInM8" role="3clFbG">
                              <node concept="3zrR0B" id="6F7CfdVInM9" role="2ShVmc">
                                <node concept="3Tqbb2" id="6F7CfdVInMa" role="3zrR0E">
                                  <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="pkWqt" id="3nQIFD_RK50" role="pqm2j">
                      <node concept="3clFbS" id="3nQIFD_RK51" role="2VODD2">
                        <node concept="3clFbF" id="3nQIFD_RKo_" role="3cqZAp">
                          <node concept="17QLQc" id="3nQIFD_RO1g" role="3clFbG">
                            <node concept="Xl_RD" id="3nQIFD_ROd9" role="3uHU7w">
                              <property role="Xl_RC" value="false" />
                            </node>
                            <node concept="2OqwBi" id="3nQIFD_RKoB" role="3uHU7B">
                              <node concept="pncrf" id="3nQIFD_RKoC" role="2Oq$k0" />
                              <node concept="2qgKlT" id="3nQIFD_RKoD" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="3nQIFD_RKoE" role="37wK5m">
                                  <property role="Xl_RC" value="defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="3nQIFD_RKoF" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2iRkQZ" id="1UsfiVCVdnV" role="2iSdaV" />
                  <node concept="3F2HdR" id="1UsfiVCVdnW" role="3EZMnx">
                    <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                    <node concept="2iRkQZ" id="1UsfiVCVdnX" role="2czzBx" />
                    <node concept="pkWqt" id="1UsfiVCVdnY" role="pqm2j">
                      <node concept="3clFbS" id="1UsfiVCVdnZ" role="2VODD2">
                        <node concept="3clFbF" id="1UsfiVCVdo0" role="3cqZAp">
                          <node concept="2OqwBi" id="1UsfiVCVdo1" role="3clFbG">
                            <node concept="2OqwBi" id="1UsfiVCVdo2" role="2Oq$k0">
                              <node concept="pncrf" id="1UsfiVCVdo3" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="1UsfiVCVdo4" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="1UsfiVCVdo5" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="35HoNQ" id="39$s2GVP70o" role="2czzBI" />
                  </node>
                  <node concept="3F2HdR" id="1UsfiVCVdo6" role="3EZMnx">
                    <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                    <node concept="2iRkQZ" id="1UsfiVCVdo7" role="2czzBx" />
                    <node concept="35HoNQ" id="3nQIFD_SN_P" role="2czzBI" />
                    <node concept="pkWqt" id="3nQIFD_TSx8" role="pqm2j">
                      <node concept="3clFbS" id="3nQIFD_TSx9" role="2VODD2">
                        <node concept="3clFbF" id="3nQIFD_TSCi" role="3cqZAp">
                          <node concept="2OqwBi" id="3nQIFD_TVcC" role="3clFbG">
                            <node concept="2OqwBi" id="3nQIFD_TSPf" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_TSCh" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="3nQIFD_TTDk" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="3nQIFD_TXHu" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="4$FPG" id="6F7CfdVInUO" role="4_6I_">
                      <node concept="3clFbS" id="6F7CfdVInUP" role="2VODD2">
                        <node concept="3clFbF" id="6F7CfdVIo7U" role="3cqZAp">
                          <node concept="2ShNRf" id="6F7CfdVIo7V" role="3clFbG">
                            <node concept="3zrR0B" id="6F7CfdVIo7W" role="2ShVmc">
                              <node concept="3Tqbb2" id="6F7CfdVIo7X" role="3zrR0E">
                                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3EZMnI" id="3nQIFD_SNVK" role="3EZMnx">
                    <node concept="2iRkQZ" id="3nQIFD_SNVY" role="2iSdaV" />
                    <node concept="pkWqt" id="3nQIFD_SNW1" role="pqm2j">
                      <node concept="3clFbS" id="3nQIFD_SNW2" role="2VODD2">
                        <node concept="3clFbF" id="3nQIFD_SNW3" role="3cqZAp">
                          <node concept="17QLQc" id="3nQIFD_SNW4" role="3clFbG">
                            <node concept="Xl_RD" id="3nQIFD_SNW5" role="3uHU7w">
                              <property role="Xl_RC" value="false" />
                            </node>
                            <node concept="2OqwBi" id="3nQIFD_SNW6" role="3uHU7B">
                              <node concept="pncrf" id="3nQIFD_SNW7" role="2Oq$k0" />
                              <node concept="2qgKlT" id="3nQIFD_SNW8" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="3nQIFD_SNW9" role="37wK5m">
                                  <property role="Xl_RC" value="defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="3nQIFD_SNWa" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3F0ifn" id="3nQIFD_SOx9" role="3EZMnx">
                      <property role="3F0ifm" value="#endif" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="pkWqt" id="5a5L3hYavNv" role="3EXrW6">
              <node concept="3clFbS" id="5a5L3hYavNw" role="2VODD2">
                <node concept="3clFbF" id="5a5L3hYavNx" role="3cqZAp">
                  <node concept="2OqwBi" id="5a5L3hYavNy" role="3clFbG">
                    <node concept="2OqwBi" id="5a5L3hYavNz" role="2Oq$k0">
                      <node concept="2OqwBi" id="5a5L3hYavN$" role="2Oq$k0">
                        <node concept="pncrf" id="5a5L3hYavN_" role="2Oq$k0" />
                        <node concept="3Tsc0h" id="5a5L3hYavNA" role="2OqNvi">
                          <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                        </node>
                      </node>
                      <node concept="1uHKPH" id="5a5L3hYavNB" role="2OqNvi" />
                    </node>
                    <node concept="3TrcHB" id="5a5L3hYavNC" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="VPXOz" id="5a5L3hYaCQ7" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="Veino" id="56tVcQ3hRNW" role="3F10Kt" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="5a5L3hYavcz" role="6VMZX">
      <node concept="2iRkQZ" id="5a5L3hYavc$" role="2iSdaV" />
      <node concept="3EZMnI" id="5a5L3hYavc_" role="3EZMnx">
        <node concept="2iRfu4" id="5a5L3hYavcA" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYavcB" role="3EZMnx">
          <property role="3F0ifm" value="Condition:" />
        </node>
        <node concept="1HlG4h" id="5a5L3hYavcC" role="3EZMnx">
          <node concept="1HfYo3" id="5a5L3hYavcD" role="1HlULh">
            <node concept="3TQlhw" id="5a5L3hYavcE" role="1Hhtcw">
              <node concept="3clFbS" id="5a5L3hYavcF" role="2VODD2">
                <node concept="3clFbF" id="5a5L3hYavcG" role="3cqZAp">
                  <node concept="2OqwBi" id="5a5L3hYavcH" role="3clFbG">
                    <node concept="pncrf" id="5a5L3hYavcI" role="2Oq$k0" />
                    <node concept="2qgKlT" id="5a5L3hYavcJ" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olGns2" resolve="buildConstraints" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="5a5L3hYavcK" role="3EZMnx">
        <node concept="2iRfu4" id="5a5L3hYavcL" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYavcM" role="3EZMnx">
          <property role="3F0ifm" value="Condition Else:" />
        </node>
        <node concept="1HlG4h" id="5a5L3hYavcN" role="3EZMnx">
          <node concept="1HfYo3" id="5a5L3hYavcO" role="1HlULh">
            <node concept="3TQlhw" id="5a5L3hYavcP" role="1Hhtcw">
              <node concept="3clFbS" id="5a5L3hYavcQ" role="2VODD2">
                <node concept="3clFbF" id="5a5L3hYavcR" role="3cqZAp">
                  <node concept="2OqwBi" id="5a5L3hYavcS" role="3clFbG">
                    <node concept="pncrf" id="5a5L3hYavcT" role="2Oq$k0" />
                    <node concept="2qgKlT" id="5a5L3hYavcU" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olH5fT" resolve="conditionElse" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="7cMGUmYTbRI" role="3EZMnx">
        <node concept="2iRfu4" id="7cMGUmYTbRJ" role="2iSdaV" />
        <node concept="3F0ifn" id="5a5L3hYavcV" role="3EZMnx">
          <property role="3F0ifm" value="Has Intention" />
        </node>
        <node concept="1HlG4h" id="7cMGUmYTc57" role="3EZMnx">
          <node concept="1HfYo3" id="7cMGUmYTc59" role="1HlULh">
            <node concept="3TQlhw" id="7cMGUmYTc5b" role="1Hhtcw">
              <node concept="3clFbS" id="7cMGUmYTc5d" role="2VODD2">
                <node concept="3clFbF" id="7cMGUmYTceo" role="3cqZAp">
                  <node concept="3cpWs3" id="7cMGUmYTnwC" role="3clFbG">
                    <node concept="Xl_RD" id="7cMGUmYTnUe" role="3uHU7B">
                      <property role="Xl_RC" value="" />
                    </node>
                    <node concept="2OqwBi" id="7cMGUmYTh_S" role="3uHU7w">
                      <node concept="2OqwBi" id="7cMGUmYTfs8" role="2Oq$k0">
                        <node concept="2OqwBi" id="7cMGUmYTeCd" role="2Oq$k0">
                          <node concept="2OqwBi" id="7cMGUmYTcrX" role="2Oq$k0">
                            <node concept="pncrf" id="7cMGUmYTcen" role="2Oq$k0" />
                            <node concept="2Xjw5R" id="7cMGUmYTdUT" role="2OqNvi">
                              <node concept="1xMEDy" id="7cMGUmYTdUV" role="1xVPHs">
                                <node concept="chp4Y" id="7cMGUmYTele" role="ri$Ld">
                                  <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3TrEf2" id="7cMGUmYTf08" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                          </node>
                        </node>
                        <node concept="3Tsc0h" id="7cMGUmYTfQk" role="2OqNvi">
                          <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
                        </node>
                      </node>
                      <node concept="2HwmR7" id="7cMGUmYTjgr" role="2OqNvi">
                        <node concept="1bVj0M" id="7cMGUmYTjgt" role="23t8la">
                          <node concept="3clFbS" id="7cMGUmYTjgu" role="1bW5cS">
                            <node concept="3clFbF" id="7cMGUmYTjMk" role="3cqZAp">
                              <node concept="3clFbC" id="7cMGUmYTlGu" role="3clFbG">
                                <node concept="pncrf" id="7cMGUmYTmOX" role="3uHU7w" />
                                <node concept="2OqwBi" id="7cMGUmYTk5n" role="3uHU7B">
                                  <node concept="37vLTw" id="7cMGUmYTjMj" role="2Oq$k0">
                                    <ref role="3cqZAo" node="7cMGUmYTjgv" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="7cMGUmYTkIm" role="2OqNvi">
                                    <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="Rh6nW" id="7cMGUmYTjgv" role="1bW2Oz">
                            <property role="TrG5h" value="it" />
                            <node concept="2jxLKc" id="7cMGUmYTjgw" role="1tU5fm" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2aJ2om" id="5a5L3hYavqe" role="CpUAK">
      <ref role="2$4xQ3" node="5a5L3hYatMh" resolve="SideBySideClones" />
    </node>
  </node>
  <node concept="24kQdi" id="2GhJDbDllva">
    <ref role="1XX52x" to="4s7a:2GhJDbDlgzd" resolve="MacroLine" />
    <node concept="3F0A7n" id="2GhJDbDllvc" role="2wV5jI">
      <property role="39s7Ar" value="true" />
      <property role="1O74Pk" value="true" />
      <ref role="1NtTu8" to="4s7a:2GhJDbDlgze" resolve="content" />
    </node>
  </node>
  <node concept="24kQdi" id="16Y907ho6xB">
    <ref role="1XX52x" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="3EZMnI" id="16Y907ho6xC" role="2wV5jI">
      <node concept="PMmxH" id="16Y907ho6xD" role="3EZMnx">
        <ref role="PMmxG" node="2s5q4UYfd6" resolve="customIndent" />
      </node>
      <node concept="2iRfu4" id="16Y907ho6xE" role="2iSdaV" />
      <node concept="3EZMnI" id="775CKuY$SFZ" role="3EZMnx">
        <node concept="2iRkQZ" id="775CKuY$SG0" role="2iSdaV" />
        <node concept="1QoScp" id="775CKuY$vVr" role="3EZMnx">
          <property role="1QpmdY" value="true" />
          <node concept="pkWqt" id="775CKuY$vVu" role="3e4ffs">
            <node concept="3clFbS" id="775CKuY$vVw" role="2VODD2">
              <node concept="3clFbF" id="775CKuY$wwA" role="3cqZAp">
                <node concept="2OqwBi" id="775CKuY$BZ0" role="3clFbG">
                  <node concept="2OqwBi" id="775CKuY$wHz" role="2Oq$k0">
                    <node concept="pncrf" id="775CKuY$ww_" role="2Oq$k0" />
                    <node concept="2qgKlT" id="775CKuY$x6l" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                      <node concept="Xl_RD" id="775CKuY$z28" role="37wK5m">
                        <property role="Xl_RC" value="!defined(FORK)" />
                      </node>
                      <node concept="Xl_RD" id="775CKuY$_oT" role="37wK5m">
                        <property role="Xl_RC" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="775CKuY$CLf" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                    <node concept="Xl_RD" id="775CKuY$CM2" role="37wK5m">
                      <property role="Xl_RC" value="true" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3EZMnI" id="775CKuY$Fjt" role="1QoVPY">
            <node concept="1QoScp" id="775CKuY$KXo" role="3EZMnx">
              <property role="1QpmdY" value="true" />
              <node concept="pkWqt" id="775CKuY$KXr" role="3e4ffs">
                <node concept="3clFbS" id="775CKuY$KXt" role="2VODD2">
                  <node concept="3clFbF" id="775CKuY$LK7" role="3cqZAp">
                    <node concept="3fqX7Q" id="775CKuY$MpA" role="3clFbG">
                      <node concept="2OqwBi" id="775CKuY$MpC" role="3fr31v">
                        <node concept="2OqwBi" id="775CKuY$MpD" role="2Oq$k0">
                          <node concept="pncrf" id="775CKuY$MpE" role="2Oq$k0" />
                          <node concept="2qgKlT" id="775CKuY$MpF" role="2OqNvi">
                            <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                            <node concept="Xl_RD" id="775CKuY$MpG" role="37wK5m">
                              <property role="Xl_RC" value="!defined(FORK)" />
                            </node>
                            <node concept="Xl_RD" id="775CKuY$MpH" role="37wK5m">
                              <property role="Xl_RC" value="true" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="775CKuY$MpI" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                          <node concept="Xl_RD" id="775CKuY$MpJ" role="37wK5m">
                            <property role="Xl_RC" value="false" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3EZMnI" id="775CKuY$Op0" role="1QoS34">
                <node concept="VPXOz" id="3nQIFD_YM2Y" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                  <node concept="3nzxsE" id="3nQIFD_YM2Z" role="3n$kyP">
                    <node concept="3clFbS" id="3nQIFD_YM30" role="2VODD2">
                      <node concept="3clFbF" id="3nQIFD_YM31" role="3cqZAp">
                        <node concept="3fqX7Q" id="3nQIFD_YM32" role="3clFbG">
                          <node concept="2OqwBi" id="3nQIFD_YM33" role="3fr31v">
                            <node concept="2OqwBi" id="3nQIFD_YM34" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_YM35" role="2Oq$k0" />
                              <node concept="2qgKlT" id="3nQIFD_YM36" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="3nQIFD_YM37" role="37wK5m">
                                  <property role="Xl_RC" value="!defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="3nQIFD_YM38" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="3nQIFD_YM39" role="2OqNvi">
                              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                              <node concept="2OqwBi" id="3nQIFD_YM3a" role="37wK5m">
                                <node concept="pncrf" id="3nQIFD_YM3b" role="2Oq$k0" />
                                <node concept="3TrcHB" id="3nQIFD_YM3c" role="2OqNvi">
                                  <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2iRkQZ" id="775CKuY$Op1" role="2iSdaV" />
                <node concept="3EZMnI" id="775CKuY$O_j" role="3EZMnx">
                  <node concept="2iRfu4" id="775CKuY$O_k" role="2iSdaV" />
                  <node concept="3F0ifn" id="775CKuY$O_l" role="3EZMnx">
                    <property role="3F0ifm" value="#if" />
                  </node>
                  <node concept="1HlG4h" id="775CKuY$O_m" role="3EZMnx">
                    <node concept="1HfYo3" id="775CKuY$O_n" role="1HlULh">
                      <node concept="3TQlhw" id="775CKuY$O_o" role="1Hhtcw">
                        <node concept="3clFbS" id="775CKuY$O_p" role="2VODD2">
                          <node concept="3clFbF" id="775CKuY$O_q" role="3cqZAp">
                            <node concept="2OqwBi" id="775CKuY$O_r" role="3clFbG">
                              <node concept="pncrf" id="775CKuY$O_s" role="2Oq$k0" />
                              <node concept="2qgKlT" id="775CKuY$O_t" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="775CKuY$O_u" role="37wK5m">
                                  <property role="Xl_RC" value="!defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="775CKuY$O_v" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3EZMnI" id="775CKuY$O_w" role="3EZMnx">
                  <property role="S$Qs1" value="true" />
                  <property role="3EXrWe" value="true" />
                  <node concept="2iRkQZ" id="775CKuY$O_x" role="2iSdaV" />
                  <node concept="3F2HdR" id="775CKuY$O_y" role="3EZMnx">
                    <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                    <node concept="2iRkQZ" id="775CKuY$O_z" role="2czzBx" />
                    <node concept="3F0ifn" id="775CKuY$O_$" role="3EmGlc">
                      <property role="3F0ifm" value="..." />
                    </node>
                    <node concept="35HoNQ" id="3nQIFD_R18e" role="2czzBI" />
                    <node concept="4$FPG" id="6F7CfdVIhl8" role="4_6I_">
                      <node concept="3clFbS" id="6F7CfdVIhl9" role="2VODD2">
                        <node concept="3clFbF" id="6F7CfdVIhot" role="3cqZAp">
                          <node concept="2ShNRf" id="6F7CfdVIhou" role="3clFbG">
                            <node concept="3zrR0B" id="6F7CfdVIhov" role="2ShVmc">
                              <node concept="3Tqbb2" id="6F7CfdVIhow" role="3zrR0E">
                                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="pkWqt" id="775CKuY$O__" role="3EXrW6">
                    <node concept="3clFbS" id="775CKuY$O_A" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY$O_B" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY$O_C" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY$O_D" role="2Oq$k0">
                            <node concept="2OqwBi" id="775CKuY$O_E" role="2Oq$k0">
                              <node concept="pncrf" id="775CKuY$O_F" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="775CKuY$O_G" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                              </node>
                            </node>
                            <node concept="1uHKPH" id="775CKuY$O_H" role="2OqNvi" />
                          </node>
                          <node concept="3TrcHB" id="775CKuY$O_I" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F2HdR" id="775CKuY$TSF" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                  <node concept="2iRkQZ" id="775CKuY$TSG" role="2czzBx" />
                  <node concept="pkWqt" id="775CKuY$TSH" role="pqm2j">
                    <node concept="3clFbS" id="775CKuY$TSI" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY$TSJ" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY$TSK" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY$TSL" role="2Oq$k0">
                            <node concept="pncrf" id="775CKuY$TSM" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="775CKuY$TSN" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="775CKuY$TSO" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="35HoNQ" id="3nQIFD_R18m" role="2czzBI" />
                </node>
                <node concept="3F0ifn" id="775CKuY_jB_" role="3EZMnx">
                  <property role="3F0ifm" value="#else" />
                  <node concept="pkWqt" id="775CKuY_jBA" role="pqm2j">
                    <node concept="3clFbS" id="775CKuY_jBB" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY_jBC" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY_jBD" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY_jBE" role="2Oq$k0">
                            <node concept="pncrf" id="775CKuY_jBF" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="775CKuY_jBG" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="775CKuY_jBH" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F2HdR" id="775CKuY_jBI" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                  <node concept="2iRkQZ" id="775CKuY_jBJ" role="2czzBx" />
                  <node concept="pkWqt" id="1pcbQKzrKHW" role="pqm2j">
                    <node concept="3clFbS" id="1pcbQKzrKHX" role="2VODD2">
                      <node concept="3clFbF" id="1pcbQKzrLcS" role="3cqZAp">
                        <node concept="2OqwBi" id="1pcbQKzrLcU" role="3clFbG">
                          <node concept="2OqwBi" id="1pcbQKzrLcV" role="2Oq$k0">
                            <node concept="pncrf" id="1pcbQKzrLcW" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="1pcbQKzrLcX" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="1pcbQKzrLcY" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="35HoNQ" id="3nQIFD_R1ii" role="2czzBI" />
                  <node concept="4$FPG" id="6F7CfdVIhLh" role="4_6I_">
                    <node concept="3clFbS" id="6F7CfdVIhLi" role="2VODD2">
                      <node concept="3clFbF" id="6F7CfdVIhYn" role="3cqZAp">
                        <node concept="2ShNRf" id="6F7CfdVIhYo" role="3clFbG">
                          <node concept="3zrR0B" id="6F7CfdVIhYp" role="2ShVmc">
                            <node concept="3Tqbb2" id="6F7CfdVIhYq" role="3zrR0E">
                              <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F0ifn" id="775CKuY_kOC" role="3EZMnx">
                  <property role="3F0ifm" value="#endif" />
                </node>
              </node>
              <node concept="3EZMnI" id="775CKuY_Hdo" role="1QoVPY">
                <node concept="VPXOz" id="3nQIFD_R1Bw" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                  <node concept="3nzxsE" id="3nQIFD_R1Bx" role="3n$kyP">
                    <node concept="3clFbS" id="3nQIFD_R1By" role="2VODD2">
                      <node concept="3clFbF" id="3nQIFD_R1Bz" role="3cqZAp">
                        <node concept="22lmx$" id="3nQIFD_R1B$" role="3clFbG">
                          <node concept="2OqwBi" id="3nQIFD_R1B_" role="3uHU7w">
                            <node concept="2OqwBi" id="3nQIFD_R1BA" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_R1BB" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="3nQIFD_R1BC" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="3nQIFD_R1BD" role="2OqNvi" />
                          </node>
                          <node concept="2OqwBi" id="3nQIFD_R1BE" role="3uHU7B">
                            <node concept="2OqwBi" id="3nQIFD_R1BF" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_R1BG" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="3nQIFD_R1BH" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="3nQIFD_R1BI" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F2HdR" id="775CKuY_HBw" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                  <node concept="2iRkQZ" id="775CKuY_HBx" role="2czzBx" />
                  <node concept="pkWqt" id="775CKuY_HBy" role="pqm2j">
                    <node concept="3clFbS" id="775CKuY_HBz" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY_HB$" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY_HB_" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY_HBA" role="2Oq$k0">
                            <node concept="pncrf" id="775CKuY_HBB" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="775CKuY_HBC" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="775CKuY_HBD" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="35HoNQ" id="3nQIFD_R1s5" role="2czzBI" />
                </node>
                <node concept="2iRkQZ" id="775CKuY_Hdp" role="2iSdaV" />
                <node concept="3F2HdR" id="775CKuY_jZm" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                  <node concept="VPXOz" id="3nQIFD_K_DS" role="3F10Kt">
                    <property role="VOm3f" value="true" />
                  </node>
                  <node concept="2iRkQZ" id="775CKuY_jZn" role="2czzBx" />
                  <node concept="35HoNQ" id="3nQIFD_R1rZ" role="2czzBI" />
                  <node concept="pkWqt" id="39$s2GVM_hd" role="pqm2j">
                    <node concept="3clFbS" id="39$s2GVM_he" role="2VODD2">
                      <node concept="3clFbF" id="39$s2GVMAV7" role="3cqZAp">
                        <node concept="2OqwBi" id="39$s2GVMDk7" role="3clFbG">
                          <node concept="2OqwBi" id="39$s2GVMB84" role="2Oq$k0">
                            <node concept="pncrf" id="39$s2GVMAV6" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="39$s2GVMBKN" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="39$s2GVMFOX" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="4$FPG" id="6F7CfdVIiaQ" role="4_6I_">
                    <node concept="3clFbS" id="6F7CfdVIiaR" role="2VODD2">
                      <node concept="3clFbF" id="6F7CfdVIio_" role="3cqZAp">
                        <node concept="2ShNRf" id="6F7CfdVIioA" role="3clFbG">
                          <node concept="3zrR0B" id="6F7CfdVIioB" role="2ShVmc">
                            <node concept="3Tqbb2" id="6F7CfdVIioC" role="3zrR0E">
                              <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2iRkQZ" id="775CKuY$Fju" role="2iSdaV" />
          </node>
          <node concept="3F2HdR" id="775CKuY$Dbc" role="1QoS34">
            <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
            <node concept="VPXOz" id="3nQIFD_K_aW" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="2iRkQZ" id="775CKuYCiot" role="2czzBx" />
            <node concept="35HoNQ" id="3nQIFD_R18b" role="2czzBI" />
            <node concept="4$FPG" id="6F7CfdVIi$Y" role="4_6I_">
              <node concept="3clFbS" id="6F7CfdVIi$Z" role="2VODD2">
                <node concept="3clFbF" id="6F7CfdVIiIm" role="3cqZAp">
                  <node concept="2ShNRf" id="6F7CfdVIiIn" role="3clFbG">
                    <node concept="3zrR0B" id="6F7CfdVIiIo" role="2ShVmc">
                      <node concept="3Tqbb2" id="6F7CfdVIiIp" role="3zrR0E">
                        <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="16Y907ho6zi" role="6VMZX">
      <node concept="2iRkQZ" id="16Y907ho6zj" role="2iSdaV" />
      <node concept="3EZMnI" id="16Y907ho6zk" role="3EZMnx">
        <node concept="2iRfu4" id="16Y907ho6zl" role="2iSdaV" />
        <node concept="3F0ifn" id="16Y907ho6zm" role="3EZMnx">
          <property role="3F0ifm" value="Condition:" />
        </node>
        <node concept="1HlG4h" id="16Y907ho6zn" role="3EZMnx">
          <node concept="1HfYo3" id="16Y907ho6zo" role="1HlULh">
            <node concept="3TQlhw" id="16Y907ho6zp" role="1Hhtcw">
              <node concept="3clFbS" id="16Y907ho6zq" role="2VODD2">
                <node concept="3clFbF" id="16Y907ho6zr" role="3cqZAp">
                  <node concept="2OqwBi" id="16Y907ho6zs" role="3clFbG">
                    <node concept="pncrf" id="16Y907ho6zt" role="2Oq$k0" />
                    <node concept="2qgKlT" id="16Y907ho6zu" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olGns2" resolve="buildConstraints" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="16Y907ho6zv" role="3EZMnx">
        <node concept="2iRfu4" id="16Y907ho6zw" role="2iSdaV" />
        <node concept="3F0ifn" id="16Y907ho6zx" role="3EZMnx">
          <property role="3F0ifm" value="Condition Else:" />
        </node>
        <node concept="1HlG4h" id="16Y907ho6zy" role="3EZMnx">
          <node concept="1HfYo3" id="16Y907ho6zz" role="1HlULh">
            <node concept="3TQlhw" id="16Y907ho6z$" role="1Hhtcw">
              <node concept="3clFbS" id="16Y907ho6z_" role="2VODD2">
                <node concept="3clFbF" id="16Y907ho6zA" role="3cqZAp">
                  <node concept="2OqwBi" id="16Y907ho6zB" role="3clFbG">
                    <node concept="pncrf" id="16Y907ho6zC" role="2Oq$k0" />
                    <node concept="2qgKlT" id="16Y907ho6zD" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olH5fT" resolve="conditionElse" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="YL52UEw6N3" role="3EZMnx">
        <node concept="2iRfu4" id="YL52UEw6N4" role="2iSdaV" />
        <node concept="3F0ifn" id="YL52UEw6_J" role="3EZMnx">
          <property role="3F0ifm" value="Projection" />
        </node>
        <node concept="1HlG4h" id="YL52UEw70w" role="3EZMnx">
          <node concept="1HfYo3" id="YL52UEw70x" role="1HlULh">
            <node concept="3TQlhw" id="YL52UEw70y" role="1Hhtcw">
              <node concept="3clFbS" id="YL52UEw70z" role="2VODD2">
                <node concept="3clFbF" id="YL52UEw878" role="3cqZAp">
                  <node concept="2OqwBi" id="YL52UEw8kH" role="3clFbG">
                    <node concept="pncrf" id="YL52UEw877" role="2Oq$k0" />
                    <node concept="2qgKlT" id="YL52UEw8U9" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                      <node concept="Xl_RD" id="YL52UEw96x" role="37wK5m">
                        <property role="Xl_RC" value="!defined(FORK)" />
                      </node>
                      <node concept="Xl_RD" id="YL52UEwj0N" role="37wK5m">
                        <property role="Xl_RC" value="true" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="YL52UEw6os" role="3EZMnx" />
    </node>
    <node concept="2aJ2om" id="16Y907hodGr" role="CpUAK">
      <ref role="2$4xQ3" node="16Y907hodbO" resolve="BoxViewNonClone" />
    </node>
  </node>
  <node concept="24kQdi" id="16Y907hpg0X">
    <ref role="1XX52x" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
    <node concept="3EZMnI" id="16Y907hpg0Y" role="2wV5jI">
      <node concept="PMmxH" id="16Y907hpg0Z" role="3EZMnx">
        <ref role="PMmxG" node="2s5q4UYfd6" resolve="customIndent" />
      </node>
      <node concept="3EZMnI" id="775CKuY_IDe" role="3EZMnx">
        <node concept="2iRkQZ" id="775CKuY_IDf" role="2iSdaV" />
        <node concept="1QoScp" id="775CKuY_IDg" role="3EZMnx">
          <property role="1QpmdY" value="true" />
          <node concept="3F2HdR" id="775CKuY_IDh" role="1QoS34">
            <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
            <node concept="VPXOz" id="3nQIFD_K_aZ" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="2iRkQZ" id="775CKuYCYb_" role="2czzBx" />
            <node concept="35HoNQ" id="1pcbQKzpUkC" role="2czzBI" />
            <node concept="4$FPG" id="6F7CfdVIiR0" role="4_6I_">
              <node concept="3clFbS" id="6F7CfdVIiR1" role="2VODD2">
                <node concept="3clFbF" id="6F7CfdVIjtf" role="3cqZAp">
                  <node concept="2ShNRf" id="6F7CfdVIjtg" role="3clFbG">
                    <node concept="3zrR0B" id="6F7CfdVIjth" role="2ShVmc">
                      <node concept="3Tqbb2" id="6F7CfdVIjti" role="3zrR0E">
                        <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="pkWqt" id="775CKuY_IDi" role="3e4ffs">
            <node concept="3clFbS" id="775CKuY_IDj" role="2VODD2">
              <node concept="3clFbF" id="775CKuY_IDk" role="3cqZAp">
                <node concept="2OqwBi" id="775CKuY_IDl" role="3clFbG">
                  <node concept="2OqwBi" id="775CKuY_IDm" role="2Oq$k0">
                    <node concept="pncrf" id="775CKuY_IDn" role="2Oq$k0" />
                    <node concept="2qgKlT" id="775CKuY_IDo" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                      <node concept="Xl_RD" id="775CKuY_IDp" role="37wK5m">
                        <property role="Xl_RC" value="defined(FORK)" />
                      </node>
                      <node concept="Xl_RD" id="775CKuY_IDq" role="37wK5m">
                        <property role="Xl_RC" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="775CKuY_IDr" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                    <node concept="Xl_RD" id="775CKuY_IDs" role="37wK5m">
                      <property role="Xl_RC" value="true" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3EZMnI" id="775CKuY_IDt" role="1QoVPY">
            <node concept="1QoScp" id="775CKuY_IDu" role="3EZMnx">
              <property role="1QpmdY" value="true" />
              <node concept="pkWqt" id="775CKuY_IDv" role="3e4ffs">
                <node concept="3clFbS" id="775CKuY_IDw" role="2VODD2">
                  <node concept="3clFbF" id="775CKuY_IDx" role="3cqZAp">
                    <node concept="3fqX7Q" id="775CKuY_IDy" role="3clFbG">
                      <node concept="2OqwBi" id="775CKuY_IDz" role="3fr31v">
                        <node concept="2OqwBi" id="775CKuY_ID$" role="2Oq$k0">
                          <node concept="pncrf" id="775CKuY_ID_" role="2Oq$k0" />
                          <node concept="2qgKlT" id="775CKuY_IDA" role="2OqNvi">
                            <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                            <node concept="Xl_RD" id="775CKuY_IDB" role="37wK5m">
                              <property role="Xl_RC" value="defined(FORK)" />
                            </node>
                            <node concept="Xl_RD" id="775CKuY_IDC" role="37wK5m">
                              <property role="Xl_RC" value="true" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="775CKuY_IDD" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                          <node concept="Xl_RD" id="775CKuY_IDE" role="37wK5m">
                            <property role="Xl_RC" value="false" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3EZMnI" id="775CKuY_IDF" role="1QoS34">
                <node concept="VPXOz" id="3nQIFD_L66s" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                  <node concept="3nzxsE" id="3nQIFD_L6iV" role="3n$kyP">
                    <node concept="3clFbS" id="3nQIFD_L6iW" role="2VODD2">
                      <node concept="3clFbF" id="3nQIFD_L7Sm" role="3cqZAp">
                        <node concept="3fqX7Q" id="3nQIFD_LPw2" role="3clFbG">
                          <node concept="2OqwBi" id="3nQIFD_LPw4" role="3fr31v">
                            <node concept="2OqwBi" id="3nQIFD_LPw5" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_LPw6" role="2Oq$k0" />
                              <node concept="2qgKlT" id="3nQIFD_LPw7" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="3nQIFD_LPw8" role="37wK5m">
                                  <property role="Xl_RC" value="defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="3nQIFD_LPw9" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="3nQIFD_LPwa" role="2OqNvi">
                              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                              <node concept="2OqwBi" id="3nQIFD_LPwb" role="37wK5m">
                                <node concept="pncrf" id="3nQIFD_LPwc" role="2Oq$k0" />
                                <node concept="3TrcHB" id="3nQIFD_LPwd" role="2OqNvi">
                                  <ref role="3TsBF5" to="4s7a:3fq3ZRIT59E" resolve="condition" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2iRkQZ" id="775CKuY_IDG" role="2iSdaV" />
                <node concept="3EZMnI" id="775CKuY_IDH" role="3EZMnx">
                  <node concept="2iRfu4" id="775CKuY_IDI" role="2iSdaV" />
                  <node concept="3F0ifn" id="775CKuY_IDJ" role="3EZMnx">
                    <property role="3F0ifm" value="#if" />
                  </node>
                  <node concept="1HlG4h" id="775CKuY_IDK" role="3EZMnx">
                    <node concept="1HfYo3" id="775CKuY_IDL" role="1HlULh">
                      <node concept="3TQlhw" id="775CKuY_IDM" role="1Hhtcw">
                        <node concept="3clFbS" id="775CKuY_IDN" role="2VODD2">
                          <node concept="3clFbF" id="775CKuY_IDO" role="3cqZAp">
                            <node concept="2OqwBi" id="775CKuY_IDP" role="3clFbG">
                              <node concept="pncrf" id="775CKuY_IDQ" role="2Oq$k0" />
                              <node concept="2qgKlT" id="775CKuY_IDR" role="2OqNvi">
                                <ref role="37wK5l" to="7zqe:YL52UEtZzc" resolve="project" />
                                <node concept="Xl_RD" id="775CKuY_IDS" role="37wK5m">
                                  <property role="Xl_RC" value="defined(FORK)" />
                                </node>
                                <node concept="Xl_RD" id="775CKuY_IDT" role="37wK5m">
                                  <property role="Xl_RC" value="true" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3EZMnI" id="775CKuY_IDU" role="3EZMnx">
                  <property role="S$Qs1" value="true" />
                  <property role="3EXrWe" value="true" />
                  <node concept="2iRkQZ" id="775CKuY_IDV" role="2iSdaV" />
                  <node concept="3F2HdR" id="775CKuY_IDW" role="3EZMnx">
                    <ref role="1NtTu8" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                    <node concept="2iRkQZ" id="775CKuY_IDX" role="2czzBx" />
                    <node concept="3F0ifn" id="775CKuY_IDY" role="3EmGlc">
                      <property role="3F0ifm" value="..." />
                    </node>
                    <node concept="35HoNQ" id="1pcbQKzpUkF" role="2czzBI" />
                    <node concept="4$FPG" id="6F7CfdVIjPv" role="4_6I_">
                      <node concept="3clFbS" id="6F7CfdVIjPw" role="2VODD2">
                        <node concept="3clFbF" id="6F7CfdVIjT2" role="3cqZAp">
                          <node concept="2ShNRf" id="6F7CfdVIjT3" role="3clFbG">
                            <node concept="3zrR0B" id="6F7CfdVIjT4" role="2ShVmc">
                              <node concept="3Tqbb2" id="6F7CfdVIjT5" role="3zrR0E">
                                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="pkWqt" id="775CKuY_IDZ" role="3EXrW6">
                    <node concept="3clFbS" id="775CKuY_IE0" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY_IE1" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY_IE2" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY_IE3" role="2Oq$k0">
                            <node concept="2OqwBi" id="775CKuY_IE4" role="2Oq$k0">
                              <node concept="pncrf" id="775CKuY_IE5" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="775CKuY_IE6" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                              </node>
                            </node>
                            <node concept="1uHKPH" id="775CKuY_IE7" role="2OqNvi" />
                          </node>
                          <node concept="3TrcHB" id="775CKuY_IE8" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:4P2s9PP_HnT" resolve="hidden" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F2HdR" id="775CKuY_IE9" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                  <node concept="2iRkQZ" id="775CKuY_IEa" role="2czzBx" />
                  <node concept="pkWqt" id="775CKuY_IEb" role="pqm2j">
                    <node concept="3clFbS" id="775CKuY_IEc" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY_IEd" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY_IEe" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY_IEf" role="2Oq$k0">
                            <node concept="pncrf" id="775CKuY_IEg" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="775CKuY_IEh" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="775CKuY_IEi" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="35HoNQ" id="1pcbQKzpUkK" role="2czzBI" />
                </node>
                <node concept="3F0ifn" id="775CKuY_IEj" role="3EZMnx">
                  <property role="3F0ifm" value="#else" />
                  <node concept="pkWqt" id="775CKuY_IEk" role="pqm2j">
                    <node concept="3clFbS" id="775CKuY_IEl" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY_IEm" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY_IEn" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY_IEo" role="2Oq$k0">
                            <node concept="pncrf" id="775CKuY_IEp" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="775CKuY_IEq" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="775CKuY_IEr" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F2HdR" id="775CKuY_IEs" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                  <node concept="2iRkQZ" id="775CKuY_IEt" role="2czzBx" />
                  <node concept="35HoNQ" id="1pcbQKzpUuA" role="2czzBI" />
                  <node concept="pkWqt" id="1pcbQKzrkyo" role="pqm2j">
                    <node concept="3clFbS" id="1pcbQKzrkyp" role="2VODD2">
                      <node concept="3clFbF" id="1pcbQKzrlqN" role="3cqZAp">
                        <node concept="2OqwBi" id="1pcbQKzrnF$" role="3clFbG">
                          <node concept="2OqwBi" id="1pcbQKzrlBK" role="2Oq$k0">
                            <node concept="pncrf" id="1pcbQKzrlqM" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="1pcbQKzrmdO" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="1pcbQKzrqcq" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="4$FPG" id="6F7CfdVIk1J" role="4_6I_">
                    <node concept="3clFbS" id="6F7CfdVIk1K" role="2VODD2">
                      <node concept="3clFbF" id="6F7CfdVIkeP" role="3cqZAp">
                        <node concept="2ShNRf" id="6F7CfdVIkeQ" role="3clFbG">
                          <node concept="3zrR0B" id="6F7CfdVIkeR" role="2ShVmc">
                            <node concept="3Tqbb2" id="6F7CfdVIkeS" role="3zrR0E">
                              <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F0ifn" id="775CKuY_IEu" role="3EZMnx">
                  <property role="3F0ifm" value="#endif" />
                </node>
              </node>
              <node concept="3EZMnI" id="775CKuY_IEv" role="1QoVPY">
                <node concept="VPXOz" id="3nQIFD_K_DP" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                  <node concept="3nzxsE" id="3nQIFD_MSIO" role="3n$kyP">
                    <node concept="3clFbS" id="3nQIFD_MSIP" role="2VODD2">
                      <node concept="3clFbF" id="3nQIFD_MSPY" role="3cqZAp">
                        <node concept="22lmx$" id="3nQIFD_MSPZ" role="3clFbG">
                          <node concept="2OqwBi" id="3nQIFD_MSQ0" role="3uHU7w">
                            <node concept="2OqwBi" id="3nQIFD_MSQ1" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_MSQ2" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="3nQIFD_MSQ3" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="3nQIFD_MSQ4" role="2OqNvi" />
                          </node>
                          <node concept="2OqwBi" id="3nQIFD_OLMg" role="3uHU7B">
                            <node concept="2OqwBi" id="3nQIFD_OLMh" role="2Oq$k0">
                              <node concept="pncrf" id="3nQIFD_OLMi" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="3nQIFD_OLMj" role="2OqNvi">
                                <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                              </node>
                            </node>
                            <node concept="3GX2aA" id="3nQIFD_OLMk" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3F2HdR" id="775CKuY_IEw" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                  <node concept="2iRkQZ" id="775CKuY_IEx" role="2czzBx" />
                  <node concept="pkWqt" id="775CKuY_IEy" role="pqm2j">
                    <node concept="3clFbS" id="775CKuY_IEz" role="2VODD2">
                      <node concept="3clFbF" id="775CKuY_IE$" role="3cqZAp">
                        <node concept="2OqwBi" id="775CKuY_IE_" role="3clFbG">
                          <node concept="2OqwBi" id="775CKuY_IEA" role="2Oq$k0">
                            <node concept="pncrf" id="775CKuY_IEB" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="775CKuY_IEC" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:3fq3ZRIT59G" resolve="elseIfs" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="775CKuY_IED" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="35HoNQ" id="1pcbQKzpUuF" role="2czzBI" />
                </node>
                <node concept="2iRkQZ" id="775CKuY_IEE" role="2iSdaV" />
                <node concept="3F2HdR" id="775CKuY_IEF" role="3EZMnx">
                  <ref role="1NtTu8" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                  <node concept="2iRkQZ" id="775CKuY_IEG" role="2czzBx" />
                  <node concept="35HoNQ" id="1pcbQKzpUCu" role="2czzBI" />
                  <node concept="pkWqt" id="3nQIFD_QjS1" role="pqm2j">
                    <node concept="3clFbS" id="3nQIFD_QjS2" role="2VODD2">
                      <node concept="3clFbF" id="3nQIFD_QjZb" role="3cqZAp">
                        <node concept="2OqwBi" id="3nQIFD_Qmi6" role="3clFbG">
                          <node concept="2OqwBi" id="3nQIFD_Qkc8" role="2Oq$k0">
                            <node concept="pncrf" id="3nQIFD_QjZa" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="3nQIFD_QkOm" role="2OqNvi">
                              <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                            </node>
                          </node>
                          <node concept="3GX2aA" id="3nQIFD_QoMW" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="4$FPG" id="6F7CfdVIkrh" role="4_6I_">
                    <node concept="3clFbS" id="6F7CfdVIkri" role="2VODD2">
                      <node concept="3clFbF" id="6F7CfdVIkCZ" role="3cqZAp">
                        <node concept="2ShNRf" id="6F7CfdVIkD0" role="3clFbG">
                          <node concept="3zrR0B" id="6F7CfdVIkD1" role="2ShVmc">
                            <node concept="3Tqbb2" id="6F7CfdVIkD2" role="3zrR0E">
                              <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2iRkQZ" id="775CKuY_IEH" role="2iSdaV" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="16Y907hpg10" role="2iSdaV" />
    </node>
    <node concept="3EZMnI" id="16Y907hpg2A" role="6VMZX">
      <node concept="2iRkQZ" id="16Y907hpg2B" role="2iSdaV" />
      <node concept="3EZMnI" id="16Y907hpg2C" role="3EZMnx">
        <node concept="2iRfu4" id="16Y907hpg2D" role="2iSdaV" />
        <node concept="3F0ifn" id="16Y907hpg2E" role="3EZMnx">
          <property role="3F0ifm" value="Condition:" />
        </node>
        <node concept="1HlG4h" id="16Y907hpg2F" role="3EZMnx">
          <node concept="1HfYo3" id="16Y907hpg2G" role="1HlULh">
            <node concept="3TQlhw" id="16Y907hpg2H" role="1Hhtcw">
              <node concept="3clFbS" id="16Y907hpg2I" role="2VODD2">
                <node concept="3clFbF" id="16Y907hpg2J" role="3cqZAp">
                  <node concept="2OqwBi" id="16Y907hpg2K" role="3clFbG">
                    <node concept="pncrf" id="16Y907hpg2L" role="2Oq$k0" />
                    <node concept="2qgKlT" id="16Y907hpg2M" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olGns2" resolve="buildConstraints" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="16Y907hpg2N" role="3EZMnx">
        <node concept="2iRfu4" id="16Y907hpg2O" role="2iSdaV" />
        <node concept="3F0ifn" id="16Y907hpg2P" role="3EZMnx">
          <property role="3F0ifm" value="Condition Else:" />
        </node>
        <node concept="1HlG4h" id="16Y907hpg2Q" role="3EZMnx">
          <node concept="1HfYo3" id="16Y907hpg2R" role="1HlULh">
            <node concept="3TQlhw" id="16Y907hpg2S" role="1Hhtcw">
              <node concept="3clFbS" id="16Y907hpg2T" role="2VODD2">
                <node concept="3clFbF" id="16Y907hpg2U" role="3cqZAp">
                  <node concept="2OqwBi" id="16Y907hpg2V" role="3clFbG">
                    <node concept="pncrf" id="16Y907hpg2W" role="2Oq$k0" />
                    <node concept="2qgKlT" id="16Y907hpg2X" role="2OqNvi">
                      <ref role="37wK5l" to="7zqe:79Do2olH5fT" resolve="conditionElse" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="16Y907hpg2Y" role="3EZMnx" />
    </node>
    <node concept="2aJ2om" id="16Y907hpg$H" role="CpUAK">
      <ref role="2$4xQ3" node="16Y907hpg$k" resolve="BoxViewClones" />
    </node>
  </node>
  <node concept="24kQdi" id="16Y907hpsH2">
    <ref role="1XX52x" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="3EZMnI" id="3LUIgcn1gYx" role="2wV5jI">
      <node concept="2iRfu4" id="3LUIgcn1gYy" role="2iSdaV" />
      <node concept="3F1sOY" id="3LUIgcmYvMJ" role="3EZMnx">
        <property role="2ru_X1" value="true" />
        <ref role="1NtTu8" to="4s7a:3LUIgcmYqcd" resolve="intention" />
        <node concept="Veino" id="3LUIgcn9mAO" role="3F10Kt" />
        <node concept="35HoNQ" id="3LUIgcnOvEC" role="2ruayu" />
      </node>
      <node concept="3EZMnI" id="16Y907hpsH3" role="3EZMnx">
        <node concept="l2Vlx" id="16Y907hpsH4" role="2iSdaV" />
        <node concept="3EZMnI" id="7cMGUmYL6Yi" role="3EZMnx">
          <node concept="2iRfu4" id="7cMGUmYL6Yj" role="2iSdaV" />
          <node concept="3F0A7n" id="16Y907hpsH5" role="3EZMnx">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <node concept="ljvvj" id="16Y907hpsH6" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="Vb9p2" id="16Y907hpsH7" role="3F10Kt">
              <property role="Vbekb" value="BOLD" />
            </node>
            <node concept="VQ3r3" id="16Y907hpsH8" role="3F10Kt">
              <property role="2USNnj" value="2" />
            </node>
          </node>
          <node concept="3F0ifn" id="7cMGUmYXxZh" role="3EZMnx">
            <property role="3F0ifm" value="[" />
            <node concept="pkWqt" id="7cMGUmYZqiK" role="pqm2j">
              <node concept="3clFbS" id="7cMGUmYZqiL" role="2VODD2">
                <node concept="3clFbF" id="7cMGUmYZqpT" role="3cqZAp">
                  <node concept="3fqX7Q" id="7cMGUmYZqKM" role="3clFbG">
                    <node concept="1eOMI4" id="7cMGUmYZrxt" role="3fr31v">
                      <node concept="3clFbC" id="7cMGUmYZqKO" role="1eOMHV">
                        <node concept="2OqwBi" id="7cMGUmYZqKP" role="3uHU7B">
                          <node concept="pncrf" id="7cMGUmYZqKQ" role="2Oq$k0" />
                          <node concept="25OxAV" id="7cMGUmYZqKR" role="2OqNvi" />
                        </node>
                        <node concept="28GBK8" id="7cMGUmYZqKS" role="3uHU7w">
                          <ref role="28GBKb" to="4s7a:3VMRtc4W287" resolve="CPP" />
                          <ref role="28H3Ia" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3F0ifn" id="7cMGUmYXz8p" role="3EZMnx">
            <property role="3F0ifm" value="Open Integration Chunks:" />
            <node concept="pkWqt" id="7cMGUmYZrOr" role="pqm2j">
              <node concept="3clFbS" id="7cMGUmYZrOs" role="2VODD2">
                <node concept="3clFbF" id="7cMGUmYZrW2" role="3cqZAp">
                  <node concept="3fqX7Q" id="7cMGUmYZrW3" role="3clFbG">
                    <node concept="1eOMI4" id="7cMGUmYZrW4" role="3fr31v">
                      <node concept="3clFbC" id="7cMGUmYZrW5" role="1eOMHV">
                        <node concept="2OqwBi" id="7cMGUmYZrW6" role="3uHU7B">
                          <node concept="pncrf" id="7cMGUmYZrW7" role="2Oq$k0" />
                          <node concept="25OxAV" id="7cMGUmYZrW8" role="2OqNvi" />
                        </node>
                        <node concept="28GBK8" id="7cMGUmYZrW9" role="3uHU7w">
                          <ref role="28GBKb" to="4s7a:3VMRtc4W287" resolve="CPP" />
                          <ref role="28H3Ia" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1HlG4h" id="7cMGUmYXxZv" role="3EZMnx">
            <node concept="1HfYo3" id="7cMGUmYXxZw" role="1HlULh">
              <node concept="3TQlhw" id="7cMGUmYXxZx" role="1Hhtcw">
                <node concept="3clFbS" id="7cMGUmYXxZy" role="2VODD2">
                  <node concept="3cpWs8" id="7cMGUmYXxZz" role="3cqZAp">
                    <node concept="3cpWsn" id="7cMGUmYXxZ$" role="3cpWs9">
                      <property role="TrG5h" value="count" />
                      <node concept="10Oyi0" id="7cMGUmYXxZ_" role="1tU5fm" />
                      <node concept="3cmrfG" id="7cMGUmYXxZA" role="33vP2m">
                        <property role="3cmrfH" value="-1" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="7cMGUmYXxZB" role="3cqZAp" />
                  <node concept="3clFbJ" id="7cMGUmYXxZC" role="3cqZAp">
                    <node concept="3clFbS" id="7cMGUmYXxZD" role="3clFbx">
                      <node concept="3clFbF" id="7cMGUmYXxZE" role="3cqZAp">
                        <node concept="37vLTI" id="7cMGUmYXxZF" role="3clFbG">
                          <node concept="2OqwBi" id="7cMGUmYXxZG" role="37vLTx">
                            <node concept="2OqwBi" id="7cMGUmYXxZH" role="2Oq$k0">
                              <node concept="2OqwBi" id="7cMGUmYXxZI" role="2Oq$k0">
                                <node concept="2OqwBi" id="7cMGUmYXxZJ" role="2Oq$k0">
                                  <node concept="pncrf" id="7cMGUmYXxZK" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="7cMGUmYXxZL" role="2OqNvi">
                                    <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                                  </node>
                                </node>
                                <node concept="2Rf3mk" id="7cMGUmYXxZM" role="2OqNvi">
                                  <node concept="1xMEDy" id="7cMGUmYXxZN" role="1xVPHs">
                                    <node concept="chp4Y" id="7cMGUmYXxZO" role="ri$Ld">
                                      <ref role="cht4Q" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3zZkjj" id="7cMGUmYXxZP" role="2OqNvi">
                                <node concept="1bVj0M" id="7cMGUmYXxZQ" role="23t8la">
                                  <node concept="3clFbS" id="7cMGUmYXxZR" role="1bW5cS">
                                    <node concept="3clFbF" id="7cMGUmYXxZS" role="3cqZAp">
                                      <node concept="2OqwBi" id="7cMGUmYXxZT" role="3clFbG">
                                        <node concept="37vLTw" id="7cMGUmYXxZU" role="2Oq$k0">
                                          <ref role="3cqZAo" node="7cMGUmYXxZW" resolve="it" />
                                        </node>
                                        <node concept="2qgKlT" id="7cMGUmYXxZV" role="2OqNvi">
                                          <ref role="37wK5l" to="7zqe:2NfWTcLJRHk" resolve="isCloneIf" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="Rh6nW" id="7cMGUmYXxZW" role="1bW2Oz">
                                    <property role="TrG5h" value="it" />
                                    <node concept="2jxLKc" id="7cMGUmYXxZX" role="1tU5fm" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="34oBXx" id="7cMGUmYXxZY" role="2OqNvi" />
                          </node>
                          <node concept="37vLTw" id="7cMGUmYXxZZ" role="37vLTJ">
                            <ref role="3cqZAo" node="7cMGUmYXxZ$" resolve="count" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3y3z36" id="7cMGUmYXy00" role="3clFbw">
                      <node concept="10Nm6u" id="7cMGUmYXy01" role="3uHU7w" />
                      <node concept="2OqwBi" id="7cMGUmYXy02" role="3uHU7B">
                        <node concept="pncrf" id="7cMGUmYXy03" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7cMGUmYXy04" role="2OqNvi">
                          <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="7cMGUmYXy05" role="3cqZAp" />
                  <node concept="3cpWs6" id="7cMGUmYXy06" role="3cqZAp">
                    <node concept="2YIFZM" id="7cMGUmYXy07" role="3cqZAk">
                      <ref role="37wK5l" to="wyt6:~Integer.toString(int):java.lang.String" resolve="toString" />
                      <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                      <node concept="37vLTw" id="7cMGUmYXy08" role="37wK5m">
                        <ref role="3cqZAo" node="7cMGUmYXxZ$" resolve="count" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="pkWqt" id="7cMGUmYZs4b" role="pqm2j">
              <node concept="3clFbS" id="7cMGUmYZs4c" role="2VODD2">
                <node concept="3clFbF" id="7cMGUmYZsfm" role="3cqZAp">
                  <node concept="3fqX7Q" id="7cMGUmYZsfn" role="3clFbG">
                    <node concept="1eOMI4" id="7cMGUmYZsfo" role="3fr31v">
                      <node concept="3clFbC" id="7cMGUmYZsfp" role="1eOMHV">
                        <node concept="2OqwBi" id="7cMGUmYZsfq" role="3uHU7B">
                          <node concept="pncrf" id="7cMGUmYZsfr" role="2Oq$k0" />
                          <node concept="25OxAV" id="7cMGUmYZsfs" role="2OqNvi" />
                        </node>
                        <node concept="28GBK8" id="7cMGUmYZsft" role="3uHU7w">
                          <ref role="28GBKb" to="4s7a:3VMRtc4W287" resolve="CPP" />
                          <ref role="28H3Ia" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3F0ifn" id="7cMGUmYXzIm" role="3EZMnx">
            <property role="3F0ifm" value="]" />
            <node concept="pkWqt" id="7cMGUmYZsLo" role="pqm2j">
              <node concept="3clFbS" id="7cMGUmYZsLp" role="2VODD2">
                <node concept="3clFbF" id="7cMGUmYZt3A" role="3cqZAp">
                  <node concept="3fqX7Q" id="7cMGUmYZt3B" role="3clFbG">
                    <node concept="1eOMI4" id="7cMGUmYZt3C" role="3fr31v">
                      <node concept="3clFbC" id="7cMGUmYZt3D" role="1eOMHV">
                        <node concept="2OqwBi" id="7cMGUmYZt3E" role="3uHU7B">
                          <node concept="pncrf" id="7cMGUmYZt3F" role="2Oq$k0" />
                          <node concept="25OxAV" id="7cMGUmYZt3G" role="2OqNvi" />
                        </node>
                        <node concept="28GBK8" id="7cMGUmYZt3H" role="3uHU7w">
                          <ref role="28GBKb" to="4s7a:3VMRtc4W287" resolve="CPP" />
                          <ref role="28H3Ia" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3EZMnI" id="1mkwweCdO3Q" role="3EZMnx">
          <node concept="2iRfu4" id="1mkwweCdO3R" role="2iSdaV" />
          <node concept="3F0ifn" id="1mkwweCdOjP" role="3EZMnx">
            <property role="3F0ifm" value="Preview" />
            <node concept="30gYXW" id="1mkwweCdOvT" role="3F10Kt">
              <property role="Vb096" value="yellow" />
            </node>
          </node>
          <node concept="pkWqt" id="1mkwweCfgL7" role="pqm2j">
            <node concept="3clFbS" id="1mkwweCfgL8" role="2VODD2">
              <node concept="3clFbF" id="1mkwweCfgSg" role="3cqZAp">
                <node concept="3clFbC" id="3LUIgcmuWmv" role="3clFbG">
                  <node concept="2OqwBi" id="3LUIgcmuScp" role="3uHU7B">
                    <node concept="pncrf" id="3LUIgcmuRZr" role="2Oq$k0" />
                    <node concept="25OxAV" id="3LUIgcmuSIJ" role="2OqNvi" />
                  </node>
                  <node concept="28GBK8" id="3LUIgcmv0x3" role="3uHU7w">
                    <ref role="28GBKb" to="4s7a:3VMRtc4W287" resolve="CPP" />
                    <ref role="28H3Ia" to="4s7a:3LUIgcmtu8f" resolve="future" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="pVoyu" id="7cMGUmYL76z" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="16Y907hpsHH" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:2s5q4UUgwl" resolve="statements" />
          <node concept="l2Vlx" id="16Y907hpsHI" role="2czzBx" />
          <node concept="pj6Ft" id="16Y907hpsHJ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pVoyu" id="5U5wmQ0wUEU" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="4$FPG" id="6F7CfdVIoNM" role="4_6I_">
            <node concept="3clFbS" id="6F7CfdVIoNN" role="2VODD2">
              <node concept="3clFbF" id="6F7CfdVIoR9" role="3cqZAp">
                <node concept="2ShNRf" id="6F7CfdVIoRa" role="3clFbG">
                  <node concept="3zrR0B" id="6F7CfdVIoRb" role="2ShVmc">
                    <node concept="3Tqbb2" id="6F7CfdVIoRc" role="3zrR0E">
                      <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="3LUIgcm_ygC" role="6VMZX">
      <node concept="2iRkQZ" id="3LUIgcm_ygD" role="2iSdaV" />
      <node concept="3EZMnI" id="16Y907hpsHL" role="3EZMnx">
        <node concept="2iRfu4" id="16Y907hpsHM" role="2iSdaV" />
        <node concept="3F0ifn" id="16Y907hpsHN" role="3EZMnx">
          <property role="3F0ifm" value="Constraint" />
        </node>
        <node concept="3F0A7n" id="16Y907hpsHO" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:79Do2olI3d1" resolve="constraints" />
        </node>
      </node>
      <node concept="3EZMnI" id="3LUIgcm_yGV" role="3EZMnx">
        <node concept="3F0ifn" id="3LUIgcm_yH8" role="3EZMnx">
          <property role="3F0ifm" value="Future" />
        </node>
        <node concept="2iRfu4" id="3LUIgcm_yGW" role="2iSdaV" />
        <node concept="3F1sOY" id="3LUIgcm_yGO" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:3LUIgcmtu8f" resolve="future" />
        </node>
      </node>
    </node>
  </node>
  <node concept="325Ffw" id="4ZLhJCqOuuf">
    <property role="TrG5h" value="MyTextContentKeyMap" />
    <ref role="1chiOs" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="2PxR9H" id="4ZLhJCqOuug" role="2QnnpI">
      <property role="2IlM53" value="caret_at_intermediate_position" />
      <node concept="2Py5lD" id="4ZLhJCqOuuh" role="2PyaAO">
        <property role="2PWKIS" value="VK_D" />
        <property role="2PWKIB" value="alt" />
      </node>
      <node concept="2PzhpH" id="4ZLhJCqOuui" role="2PL9iG">
        <node concept="3clFbS" id="4ZLhJCqOuuj" role="2VODD2">
          <node concept="34ab3g" id="4ZLhJCqOuu$" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="Xl_RD" id="4ZLhJCqOuuA" role="34bqiv">
              <property role="Xl_RC" value="MyTextContentKeyMap" />
            </node>
          </node>
          <node concept="3cpWs8" id="4ZLhJCqOzyY" role="3cqZAp">
            <node concept="3cpWsn" id="4ZLhJCqOzyZ" role="3cpWs9">
              <property role="TrG5h" value="cell" />
              <node concept="3uibUv" id="4ZLhJCqOzz0" role="1tU5fm">
                <ref role="3uigEE" to="g51k:~EditorCell_Property" resolve="EditorCell_Property" />
              </node>
              <node concept="1eOMI4" id="4ZLhJCqOz$4" role="33vP2m">
                <node concept="10QFUN" id="4ZLhJCqOz$1" role="1eOMHV">
                  <node concept="3uibUv" id="4ZLhJCqOz$t" role="10QFUM">
                    <ref role="3uigEE" to="g51k:~EditorCell_Property" resolve="EditorCell_Property" />
                  </node>
                  <node concept="2OqwBi" id="4ZLhJCqOz5i" role="10QFUP">
                    <node concept="1Q80Hx" id="4ZLhJCqOz0U" role="2Oq$k0" />
                    <node concept="liA8E" id="4ZLhJCqOzkt" role="2OqNvi">
                      <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4ZLhJCqO$PR" role="3cqZAp">
            <node concept="3cpWsn" id="4ZLhJCqO$PU" role="3cpWs9">
              <property role="TrG5h" value="s1" />
              <node concept="17QB3L" id="4ZLhJCqO$PP" role="1tU5fm" />
              <node concept="2OqwBi" id="4ZLhJCqO_Cp" role="33vP2m">
                <node concept="2OqwBi" id="4ZLhJCqO$YU" role="2Oq$k0">
                  <node concept="0GJ7k" id="4ZLhJCqO$Ri" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4ZLhJCqO_gK" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                  </node>
                </node>
                <node concept="liA8E" id="4ZLhJCqOAkW" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.substring(int,int):java.lang.String" resolve="substring" />
                  <node concept="3cmrfG" id="4ZLhJCqOB2G" role="37wK5m">
                    <property role="3cmrfH" value="0" />
                  </node>
                  <node concept="2OqwBi" id="4ZLhJCqOBSS" role="37wK5m">
                    <node concept="37vLTw" id="4ZLhJCqOBac" role="2Oq$k0">
                      <ref role="3cqZAo" node="4ZLhJCqOzyZ" resolve="cell" />
                    </node>
                    <node concept="liA8E" id="4ZLhJCqODDH" role="2OqNvi">
                      <ref role="37wK5l" to="g51k:~EditorCell_Label.getCaretPosition():int" resolve="getCaretPosition" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4ZLhJCqODTz" role="3cqZAp">
            <node concept="3cpWsn" id="4ZLhJCqODTA" role="3cpWs9">
              <property role="TrG5h" value="s2" />
              <node concept="17QB3L" id="4ZLhJCqODTx" role="1tU5fm" />
              <node concept="2OqwBi" id="4ZLhJCqOECe" role="33vP2m">
                <node concept="2OqwBi" id="4ZLhJCqOE9o" role="2Oq$k0">
                  <node concept="0GJ7k" id="4ZLhJCqOE1K" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4ZLhJCqOEiz" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                  </node>
                </node>
                <node concept="liA8E" id="4ZLhJCqOFkL" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.substring(int):java.lang.String" resolve="substring" />
                  <node concept="2OqwBi" id="4ZLhJCqOG0H" role="37wK5m">
                    <node concept="37vLTw" id="4ZLhJCqOFrw" role="2Oq$k0">
                      <ref role="3cqZAo" node="4ZLhJCqOzyZ" resolve="cell" />
                    </node>
                    <node concept="liA8E" id="4ZLhJCqOHLw" role="2OqNvi">
                      <ref role="37wK5l" to="g51k:~EditorCell_Label.getCaretPosition():int" resolve="getCaretPosition" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4ZLhJCqOJM2" role="3cqZAp">
            <node concept="3cpWsn" id="4ZLhJCqOJM5" role="3cpWs9">
              <property role="TrG5h" value="nextLine" />
              <node concept="3Tqbb2" id="4ZLhJCqOJM0" role="1tU5fm">
                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
              </node>
              <node concept="2ShNRf" id="4ZLhJCqOJVK" role="33vP2m">
                <node concept="3zrR0B" id="4ZLhJCqOLHq" role="2ShVmc">
                  <node concept="3Tqbb2" id="4ZLhJCqOLHs" role="3zrR0E">
                    <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4ZLhJCqOLQC" role="3cqZAp">
            <node concept="2OqwBi" id="4ZLhJCqOMO5" role="3clFbG">
              <node concept="2OqwBi" id="4ZLhJCqOM39" role="2Oq$k0">
                <node concept="37vLTw" id="4ZLhJCqOLQA" role="2Oq$k0">
                  <ref role="3cqZAo" node="4ZLhJCqOJM5" resolve="nextLine" />
                </node>
                <node concept="3TrcHB" id="4ZLhJCqOMsz" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                </node>
              </node>
              <node concept="tyxLq" id="4ZLhJCqONhn" role="2OqNvi">
                <node concept="37vLTw" id="4ZLhJCqONj3" role="tz02z">
                  <ref role="3cqZAo" node="4ZLhJCqODTA" resolve="s2" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4ZLhJCqOJ0Y" role="3cqZAp">
            <node concept="2OqwBi" id="4ZLhJCqOJcz" role="3clFbG">
              <node concept="0GJ7k" id="4ZLhJCqOJ0W" role="2Oq$k0" />
              <node concept="HtI8k" id="4ZLhJCqOJtS" role="2OqNvi">
                <node concept="37vLTw" id="4ZLhJCqONnc" role="HtI8F">
                  <ref role="3cqZAo" node="4ZLhJCqOJM5" resolve="nextLine" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4ZLhJCqQiS9" role="3cqZAp">
            <node concept="37vLTI" id="4ZLhJCqQjQ_" role="3clFbG">
              <node concept="37vLTw" id="4ZLhJCqQjWY" role="37vLTx">
                <ref role="3cqZAo" node="4ZLhJCqO$PU" resolve="s1" />
              </node>
              <node concept="2OqwBi" id="4ZLhJCqQj4k" role="37vLTJ">
                <node concept="0GJ7k" id="4ZLhJCqQiS7" role="2Oq$k0" />
                <node concept="3TrcHB" id="4ZLhJCqQjuZ" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4ZLhJCqQhzM" role="3cqZAp">
            <node concept="37vLTI" id="4ZLhJCqQiyF" role="3clFbG">
              <node concept="3clFbT" id="4ZLhJCqQiD4" role="37vLTx">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="2OqwBi" id="4ZLhJCqQhJG" role="37vLTJ">
                <node concept="0GJ7k" id="4ZLhJCqQhzK" role="2Oq$k0" />
                <node concept="3TrcHB" id="4ZLhJCqQiaf" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2Pz7Y7" id="4ZLhJCqOzAk" role="2Pzqsi">
        <node concept="3clFbS" id="4ZLhJCqOzAl" role="2VODD2">
          <node concept="34ab3g" id="4ZLhJCqPmul" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="3cpWs3" id="4ZLhJCqPpc2" role="34bqiv">
              <node concept="1eOMI4" id="4ZLhJCqPq8J" role="3uHU7w">
                <node concept="2ZW3vV" id="4ZLhJCqPqg_" role="1eOMHV">
                  <node concept="3uibUv" id="4ZLhJCqPqgA" role="2ZW6by">
                    <ref role="3uigEE" to="g51k:~EditorCell_Property" resolve="EditorCell_Property" />
                  </node>
                  <node concept="2OqwBi" id="4ZLhJCqPqgB" role="2ZW6bz">
                    <node concept="1Q80Hx" id="4ZLhJCqPqgC" role="2Oq$k0" />
                    <node concept="liA8E" id="4ZLhJCqPqgD" role="2OqNvi">
                      <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="4ZLhJCqPmun" role="3uHU7B">
                <property role="Xl_RC" value="Test MyTextContentKeyMap applicability: " />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4ZLhJCqOzHu" role="3cqZAp">
            <node concept="2ZW3vV" id="4ZLhJCqO$yQ" role="3clFbG">
              <node concept="3uibUv" id="4ZLhJCqO$Hw" role="2ZW6by">
                <ref role="3uigEE" to="g51k:~EditorCell_Property" resolve="EditorCell_Property" />
              </node>
              <node concept="2OqwBi" id="4ZLhJCqOzVv" role="2ZW6bz">
                <node concept="1Q80Hx" id="4ZLhJCqOzHt" role="2Oq$k0" />
                <node concept="liA8E" id="4ZLhJCqO$hE" role="2OqNvi">
                  <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2PxR9H" id="2NOvNR_tNdd" role="2QnnpI">
      <node concept="2Py5lD" id="2NOvNR_tNde" role="2PyaAO">
        <property role="2PWKIB" value="none" />
        <property role="2PWKIS" value="VK_DELETE" />
      </node>
      <node concept="2PzhpH" id="2NOvNR_tNdf" role="2PL9iG">
        <node concept="3clFbS" id="2NOvNR_tNdg" role="2VODD2">
          <node concept="34ab3g" id="2NOvNR_tNnc" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="Xl_RD" id="2NOvNR_tNne" role="34bqiv">
              <property role="Xl_RC" value="MyTextContentKeyMap DELETE" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2PxR9H" id="2NOvNR_tNoA" role="2QnnpI">
      <node concept="2Py5lD" id="2NOvNR_tNoB" role="2PyaAO">
        <property role="2PWKIB" value="none" />
        <property role="2PWKIS" value="VK_BACK_SPACE" />
      </node>
      <node concept="2PzhpH" id="2NOvNR_tNoC" role="2PL9iG">
        <node concept="3clFbS" id="2NOvNR_tNoD" role="2VODD2">
          <node concept="34ab3g" id="2NOvNR_tNoE" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="Xl_RD" id="2NOvNR_tNoF" role="34bqiv">
              <property role="Xl_RC" value="MyTextContentKeyMap BACK SPACE" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1h_SRR" id="2NOvNR_qwA8">
    <property role="TrG5h" value="MyActionMap" />
    <ref role="1h_SK9" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
    <node concept="1hA7zw" id="2NOvNR_qwA9" role="1h_SK8">
      <property role="1hAc7j" value="delete_action_id" />
      <node concept="1hAIg9" id="2NOvNR_qwAa" role="1hA7z_">
        <node concept="3clFbS" id="2NOvNR_qwAb" role="2VODD2">
          <node concept="34ab3g" id="2NOvNR_qwAn" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="Xl_RD" id="2NOvNR_qwAp" role="34bqiv">
              <property role="Xl_RC" value="MyActionMap DELETE" />
            </node>
          </node>
          <node concept="3clFbF" id="2NOvNR_vOkW" role="3cqZAp">
            <node concept="37vLTI" id="2NOvNR_vP5f" role="3clFbG">
              <node concept="3clFbT" id="2NOvNR_vP5D" role="37vLTx">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="2OqwBi" id="2NOvNR_vOqM" role="37vLTJ">
                <node concept="0IXxy" id="2NOvNR_vOkU" role="2Oq$k0" />
                <node concept="3TrcHB" id="2NOvNR_vOGF" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2NOvNR_vw1V" resolve="isDeleted" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2NOvNR_w3bZ" role="3cqZAp">
            <node concept="37vLTI" id="2NOvNR_w3YB" role="3clFbG">
              <node concept="3clFbT" id="2NOvNR_w3Z1" role="37vLTx">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="2OqwBi" id="2NOvNR_w3i6" role="37vLTJ">
                <node concept="0IXxy" id="2NOvNR_w3bX" role="2Oq$k0" />
                <node concept="3TrcHB" id="2NOvNR_w3A3" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="2NOvNR_yTAn" role="3cqZAp">
            <node concept="3clFbS" id="2NOvNR_yTAp" role="3clFbx">
              <node concept="3clFbF" id="2NOvNR_yU5g" role="3cqZAp">
                <node concept="2OqwBi" id="2NOvNR_yUb6" role="3clFbG">
                  <node concept="0IXxy" id="2NOvNR_yU5e" role="2Oq$k0" />
                  <node concept="1PgB_6" id="2NOvNR_yUki" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="2NOvNR_yTL5" role="3clFbw">
              <node concept="0IXxy" id="2NOvNR_yTDu" role="2Oq$k0" />
              <node concept="3TrcHB" id="2NOvNR_yU2U" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2NOvNR_yT9t" resolve="isNew" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="jK8Ss" id="33HeFyzEoFD" role="jK8aL">
        <node concept="3clFbS" id="33HeFyzEoFE" role="2VODD2">
          <node concept="3clFbF" id="33HeFyzEoP_" role="3cqZAp">
            <node concept="2OqwBi" id="33HeFyzEpNY" role="3clFbG">
              <node concept="2OqwBi" id="33HeFyzEp2y" role="2Oq$k0">
                <node concept="0IXxy" id="33HeFyzEoP$" role="2Oq$k0" />
                <node concept="3TrcHB" id="33HeFyzEpiH" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                </node>
              </node>
              <node concept="17RvpY" id="33HeFyzEr1B" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1hA7zw" id="2NOvNR_zqCR" role="1h_SK8">
      <property role="1hAc7j" value="backspace_action_id" />
      <node concept="1hAIg9" id="2NOvNR_zqCS" role="1hA7z_">
        <node concept="3clFbS" id="2NOvNR_zqCT" role="2VODD2">
          <node concept="34ab3g" id="2NOvNR_zqZe" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="Xl_RD" id="2NOvNR_zqZg" role="34bqiv">
              <property role="Xl_RC" value="MyActionMap BACKSPACE" />
            </node>
          </node>
          <node concept="3clFbF" id="2NOvNR__hHP" role="3cqZAp">
            <node concept="37vLTI" id="2NOvNR__hHQ" role="3clFbG">
              <node concept="3clFbT" id="2NOvNR__hHR" role="37vLTx">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="2OqwBi" id="2NOvNR__hHS" role="37vLTJ">
                <node concept="0IXxy" id="2NOvNR__hHT" role="2Oq$k0" />
                <node concept="3TrcHB" id="2NOvNR__hHU" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2NOvNR_vw1V" resolve="isDeleted" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2NOvNR__hHV" role="3cqZAp">
            <node concept="37vLTI" id="2NOvNR__hHW" role="3clFbG">
              <node concept="3clFbT" id="2NOvNR__hHX" role="37vLTx">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="2OqwBi" id="2NOvNR__hHY" role="37vLTJ">
                <node concept="0IXxy" id="2NOvNR__hHZ" role="2Oq$k0" />
                <node concept="3TrcHB" id="2NOvNR__hI0" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:5a5L3hY8OSL" resolve="isEdit" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="2NOvNR__hI1" role="3cqZAp">
            <node concept="3clFbS" id="2NOvNR__hI2" role="3clFbx">
              <node concept="3clFbF" id="2NOvNR__hI3" role="3cqZAp">
                <node concept="2OqwBi" id="2NOvNR__hI4" role="3clFbG">
                  <node concept="0IXxy" id="2NOvNR__hI5" role="2Oq$k0" />
                  <node concept="1PgB_6" id="2NOvNR__hI6" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="2NOvNR__hI7" role="3clFbw">
              <node concept="0IXxy" id="2NOvNR__hI8" role="2Oq$k0" />
              <node concept="3TrcHB" id="2NOvNR__hI9" role="2OqNvi">
                <ref role="3TsBF5" to="4s7a:2NOvNR_yT9t" resolve="isNew" />
              </node>
            </node>
          </node>
          <node concept="3clFbH" id="2NOvNR__hHD" role="3cqZAp" />
        </node>
      </node>
      <node concept="jK8Ss" id="33HeFyzErem" role="jK8aL">
        <node concept="3clFbS" id="33HeFyzEren" role="2VODD2">
          <node concept="3clFbF" id="33HeFyzEroh" role="3cqZAp">
            <node concept="2OqwBi" id="33HeFyzEroi" role="3clFbG">
              <node concept="2OqwBi" id="33HeFyzEroj" role="2Oq$k0">
                <node concept="0IXxy" id="33HeFyzErok" role="2Oq$k0" />
                <node concept="3TrcHB" id="33HeFyzErol" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                </node>
              </node>
              <node concept="17RvpY" id="33HeFyzErom" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1hA7zw" id="2NOvNR_wouq" role="1h_SK8">
      <property role="1hAc7j" value="insert_action_id" />
      <property role="1hHO97" value="disabled" />
      <node concept="1hAIg9" id="2NOvNR_wour" role="1hA7z_">
        <node concept="3clFbS" id="2NOvNR_wous" role="2VODD2">
          <node concept="34ab3g" id="2NOvNR_woxH" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="Xl_RD" id="2NOvNR_woxJ" role="34bqiv">
              <property role="Xl_RC" value="MyActionMap INSERT" />
            </node>
          </node>
          <node concept="34ab3g" id="2NOvNR_wBeF" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="3cpWs3" id="2NOvNR_xlCX" role="34bqiv">
              <node concept="3cpWs3" id="2NOvNR_wC3o" role="3uHU7B">
                <node concept="Xl_RD" id="2NOvNR_wBeH" role="3uHU7B">
                  <property role="Xl_RC" value="node " />
                </node>
                <node concept="2OqwBi" id="2NOvNR_wCSG" role="3uHU7w">
                  <node concept="2OqwBi" id="2NOvNR_wCdT" role="2Oq$k0">
                    <node concept="0IXxy" id="2NOvNR_wC3O" role="2Oq$k0" />
                    <node concept="2yIwOk" id="2NOvNR_wCvP" role="2OqNvi" />
                  </node>
                  <node concept="liA8E" id="2NOvNR_wDb3" role="2OqNvi">
                    <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="2NOvNR_xlLe" role="3uHU7w">
                <property role="Xl_RC" value=" text " />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="2NOvNR_xmI8" role="3cqZAp">
            <node concept="3cpWsn" id="2NOvNR_xmIb" role="3cpWs9">
              <property role="TrG5h" value="textContentNode" />
              <node concept="3Tqbb2" id="2NOvNR_xmI6" role="1tU5fm">
                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
              </node>
              <node concept="1PxgMI" id="2NOvNR_xn6S" role="33vP2m">
                <property role="1BlNFB" value="true" />
                <ref role="1m5ApE" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                <node concept="0IXxy" id="2NOvNR_xmOw" role="1m5AlR" />
              </node>
            </node>
          </node>
          <node concept="34ab3g" id="2NOvNR_xnam" role="3cqZAp">
            <property role="35gtTG" value="warn" />
            <node concept="3cpWs3" id="2NOvNR_xnS2" role="34bqiv">
              <node concept="2OqwBi" id="2NOvNR_xo7K" role="3uHU7w">
                <node concept="37vLTw" id="2NOvNR_xnXF" role="2Oq$k0">
                  <ref role="3cqZAo" node="2NOvNR_xmIb" resolve="textContentNode" />
                </node>
                <node concept="3TrcHB" id="2NOvNR_xoh0" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2s5q4UUuYU" resolve="text" />
                </node>
              </node>
              <node concept="Xl_RD" id="2NOvNR_xnao" role="3uHU7B">
                <property role="Xl_RC" value="Text " />
              </node>
            </node>
          </node>
          <node concept="3clFbH" id="2NOvNR_ynGw" role="3cqZAp" />
          <node concept="3cpWs8" id="2NOvNR_yqHB" role="3cqZAp">
            <node concept="3cpWsn" id="2NOvNR_yqHE" role="3cpWs9">
              <property role="TrG5h" value="newNode" />
              <node concept="3Tqbb2" id="2NOvNR_yqH_" role="1tU5fm">
                <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
              </node>
              <node concept="2ShNRf" id="2NOvNR_yqR6" role="33vP2m">
                <node concept="3zrR0B" id="2NOvNR_yrfX" role="2ShVmc">
                  <node concept="3Tqbb2" id="2NOvNR_yrfZ" role="3zrR0E">
                    <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2NOvNR_yUmI" role="3cqZAp">
            <node concept="37vLTI" id="2NOvNR_yVs7" role="3clFbG">
              <node concept="3clFbT" id="2NOvNR_yVz_" role="37vLTx">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="2OqwBi" id="2NOvNR_yUGB" role="37vLTJ">
                <node concept="37vLTw" id="2NOvNR_yUmG" role="2Oq$k0">
                  <ref role="3cqZAo" node="2NOvNR_yqHE" resolve="newNode" />
                </node>
                <node concept="3TrcHB" id="2NOvNR_yV3A" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:2NOvNR_yT9t" resolve="isNew" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2NOvNR_yp8s" role="3cqZAp">
            <node concept="2OqwBi" id="2NOvNR_ypk2" role="3clFbG">
              <node concept="0IXxy" id="2NOvNR_yp8q" role="2Oq$k0" />
              <node concept="HtI8k" id="2NOvNR_yrph" role="2OqNvi">
                <node concept="37vLTw" id="2NOvNR_yrrr" role="HtI8F">
                  <ref role="3cqZAo" node="2NOvNR_yqHE" resolve="newNode" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="jK8Ss" id="2NOvNR_xB4J" role="jK8aL">
        <node concept="3clFbS" id="2NOvNR_xB4K" role="2VODD2">
          <node concept="3clFbJ" id="2NOvNR_y2ul" role="3cqZAp">
            <node concept="3clFbS" id="2NOvNR_y2un" role="3clFbx">
              <node concept="3cpWs8" id="2NOvNR_y4Ng" role="3cqZAp">
                <node concept="3cpWsn" id="2NOvNR_y4Nh" role="3cpWs9">
                  <property role="TrG5h" value="cell" />
                  <node concept="3uibUv" id="2NOvNR_y5jm" role="1tU5fm">
                    <ref role="3uigEE" to="g51k:~EditorCell_Constant" resolve="EditorCell_Constant" />
                  </node>
                  <node concept="1eOMI4" id="2NOvNR_y4Nj" role="33vP2m">
                    <node concept="10QFUN" id="2NOvNR_y4Nk" role="1eOMHV">
                      <node concept="3uibUv" id="2NOvNR_y53w" role="10QFUM">
                        <ref role="3uigEE" to="g51k:~EditorCell_Constant" resolve="EditorCell_Constant" />
                      </node>
                      <node concept="2OqwBi" id="2NOvNR_y4Nm" role="10QFUP">
                        <node concept="1Q80Hx" id="2NOvNR_y4Nn" role="2Oq$k0" />
                        <node concept="liA8E" id="2NOvNR_y4No" role="2OqNvi">
                          <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="2NOvNR_yn6$" role="3cqZAp">
                <node concept="3clFbT" id="2NOvNR_yn78" role="3cqZAk" />
              </node>
            </node>
            <node concept="2ZW3vV" id="2NOvNR_y41S" role="3clFbw">
              <node concept="3uibUv" id="2NOvNR_y4zQ" role="2ZW6by">
                <ref role="3uigEE" to="g51k:~EditorCell_Constant" resolve="EditorCell_Constant" />
              </node>
              <node concept="2OqwBi" id="2NOvNR_y36H" role="2ZW6bz">
                <node concept="1Q80Hx" id="2NOvNR_y2I9" role="2Oq$k0" />
                <node concept="liA8E" id="2NOvNR_y3$O" role="2OqNvi">
                  <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2NOvNR_xBVj" role="3cqZAp">
            <node concept="3clFbT" id="2NOvNR_xBVi" role="3clFbG" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="IW6AY" id="3Gq9cqnzLgH">
    <ref role="aqKnT" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="1Qtc8_" id="3Gq9cqnzLgI" role="IW6Ez">
      <node concept="3cWJ9i" id="3Gq9cqnzLgJ" role="1Qtc8$">
        <node concept="CtIbL" id="3Gq9cqnzLgK" role="CtIbM">
          <property role="CtIbK" value="LEFT" />
        </node>
        <node concept="CtIbL" id="3Gq9cqnzLgL" role="CtIbM">
          <property role="CtIbK" value="RIGHT" />
        </node>
      </node>
      <node concept="L$LW2" id="3Gq9cqnzLgM" role="1Qtc8A" />
    </node>
  </node>
  <node concept="24kQdi" id="5zZlI40Va1N">
    <ref role="1XX52x" to="4s7a:5zZlI40Va1E" resolve="TextContentComment" />
    <node concept="3F0A7n" id="5zZlI40Va$G" role="2wV5jI">
      <ref role="1NtTu8" to="4s7a:2s5q4UUuYU" resolve="text" />
      <node concept="VechU" id="5zZlI40VaCV" role="3F10Kt">
        <property role="Vb096" value="DARK_GREEN" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4rl$PSrincY">
    <ref role="1XX52x" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="3EZMnI" id="5Z3d8P6Uulc" role="2wV5jI">
      <node concept="3F1sOY" id="5epN3$YXH74" role="3EZMnx">
        <property role="2ru_X1" value="true" />
        <ref role="1NtTu8" to="4s7a:3LUIgcmYqcd" resolve="intention" />
        <node concept="Veino" id="5epN3$YXH75" role="3F10Kt" />
        <node concept="35HoNQ" id="5epN3$YXH76" role="2ruayu" />
      </node>
      <node concept="2iRfu4" id="5Z3d8P6Uuld" role="2iSdaV" />
      <node concept="3EZMnI" id="4rl$PSrincZ" role="3EZMnx">
        <node concept="l2Vlx" id="4rl$PSrind0" role="2iSdaV" />
        <node concept="3EZMnI" id="4rl$PSrind1" role="3EZMnx">
          <node concept="2iRfu4" id="4rl$PSrind2" role="2iSdaV" />
          <node concept="3F0A7n" id="4rl$PSrind3" role="3EZMnx">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <node concept="ljvvj" id="4rl$PSrind4" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="Vb9p2" id="4rl$PSrind5" role="3F10Kt">
              <property role="Vbekb" value="BOLD" />
            </node>
            <node concept="VQ3r3" id="4rl$PSrind6" role="3F10Kt">
              <property role="2USNnj" value="2" />
            </node>
          </node>
          <node concept="pVoyu" id="5U5wmQ0ygLh" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="3F0ifn" id="7cMGUmYXuVA" role="3EZMnx">
            <property role="3F0ifm" value="[" />
          </node>
          <node concept="3F0ifn" id="7cMGUmYXvgj" role="3EZMnx">
            <property role="3F0ifm" value="Open Integration Chunks:" />
          </node>
          <node concept="1HlG4h" id="7cMGUmYXv_c" role="3EZMnx">
            <node concept="1HfYo3" id="7cMGUmYXv_d" role="1HlULh">
              <node concept="3TQlhw" id="7cMGUmYXv_e" role="1Hhtcw">
                <node concept="3clFbS" id="7cMGUmYXv_f" role="2VODD2">
                  <node concept="3cpWs8" id="7cMGUmYXv_g" role="3cqZAp">
                    <node concept="3cpWsn" id="7cMGUmYXv_h" role="3cpWs9">
                      <property role="TrG5h" value="count" />
                      <node concept="10Oyi0" id="7cMGUmYXv_i" role="1tU5fm" />
                      <node concept="3cmrfG" id="7cMGUmYXv_j" role="33vP2m">
                        <property role="3cmrfH" value="-1" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="7cMGUmYXv_k" role="3cqZAp" />
                  <node concept="3clFbJ" id="7cMGUmYXv_l" role="3cqZAp">
                    <node concept="3clFbS" id="7cMGUmYXv_m" role="3clFbx">
                      <node concept="3clFbF" id="7cMGUmYXv_n" role="3cqZAp">
                        <node concept="37vLTI" id="7cMGUmYXv_o" role="3clFbG">
                          <node concept="2OqwBi" id="7cMGUmYXv_p" role="37vLTx">
                            <node concept="2OqwBi" id="7cMGUmYXv_q" role="2Oq$k0">
                              <node concept="2OqwBi" id="7cMGUmYXv_r" role="2Oq$k0">
                                <node concept="2OqwBi" id="7cMGUmYXv_s" role="2Oq$k0">
                                  <node concept="pncrf" id="7cMGUmYXv_t" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="7cMGUmYXv_u" role="2OqNvi">
                                    <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                                  </node>
                                </node>
                                <node concept="2Rf3mk" id="7cMGUmYXv_v" role="2OqNvi">
                                  <node concept="1xMEDy" id="7cMGUmYXv_w" role="1xVPHs">
                                    <node concept="chp4Y" id="7cMGUmYXv_x" role="ri$Ld">
                                      <ref role="cht4Q" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3zZkjj" id="7cMGUmYXv_y" role="2OqNvi">
                                <node concept="1bVj0M" id="7cMGUmYXv_z" role="23t8la">
                                  <node concept="3clFbS" id="7cMGUmYXv_$" role="1bW5cS">
                                    <node concept="3clFbF" id="7cMGUmYXv__" role="3cqZAp">
                                      <node concept="2OqwBi" id="7cMGUmYXv_A" role="3clFbG">
                                        <node concept="37vLTw" id="7cMGUmYXv_B" role="2Oq$k0">
                                          <ref role="3cqZAo" node="7cMGUmYXv_D" resolve="it" />
                                        </node>
                                        <node concept="2qgKlT" id="7cMGUmYXv_C" role="2OqNvi">
                                          <ref role="37wK5l" to="7zqe:2NfWTcLJRHk" resolve="isCloneIf" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="Rh6nW" id="7cMGUmYXv_D" role="1bW2Oz">
                                    <property role="TrG5h" value="it" />
                                    <node concept="2jxLKc" id="7cMGUmYXv_E" role="1tU5fm" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="34oBXx" id="7cMGUmYXv_F" role="2OqNvi" />
                          </node>
                          <node concept="37vLTw" id="7cMGUmYXv_G" role="37vLTJ">
                            <ref role="3cqZAo" node="7cMGUmYXv_h" resolve="count" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3y3z36" id="7cMGUmYXv_H" role="3clFbw">
                      <node concept="10Nm6u" id="7cMGUmYXv_I" role="3uHU7w" />
                      <node concept="2OqwBi" id="7cMGUmYXv_J" role="3uHU7B">
                        <node concept="pncrf" id="7cMGUmYXv_K" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7cMGUmYXv_L" role="2OqNvi">
                          <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="7cMGUmYXv_M" role="3cqZAp" />
                  <node concept="3cpWs6" id="7cMGUmYXv_N" role="3cqZAp">
                    <node concept="2YIFZM" id="7cMGUmYXv_O" role="3cqZAk">
                      <ref role="37wK5l" to="wyt6:~Integer.toString(int):java.lang.String" resolve="toString" />
                      <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                      <node concept="37vLTw" id="7cMGUmYXv_P" role="37wK5m">
                        <ref role="3cqZAo" node="7cMGUmYXv_h" resolve="count" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3F0ifn" id="7cMGUmYXww6" role="3EZMnx">
            <property role="3F0ifm" value="]" />
          </node>
        </node>
        <node concept="3F0ifn" id="775CKuYA4Bc" role="3EZMnx">
          <property role="3F0ifm" value="Mainline" />
          <node concept="30gYXW" id="775CKuYA4Oa" role="3F10Kt">
            <property role="Vb096" value="yellow" />
          </node>
          <node concept="pVoyu" id="4DqDj9GlGt5" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="4rl$PSrindF" role="3EZMnx">
          <ref role="1ERwB7" node="2NOvNR_qwA8" resolve="MyActionMap" />
          <ref role="1NtTu8" to="4s7a:2s5q4UUgwl" resolve="statements" />
          <node concept="l2Vlx" id="4rl$PSrindG" role="2czzBx" />
          <node concept="pj6Ft" id="4rl$PSrindH" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pVoyu" id="5U5wmQ0ygJN" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="4$FPG" id="6F7CfdVIp3C" role="4_6I_">
            <node concept="3clFbS" id="6F7CfdVIp3D" role="2VODD2">
              <node concept="3clFbF" id="6F7CfdVIpmU" role="3cqZAp">
                <node concept="2ShNRf" id="6F7CfdVIpmV" role="3clFbG">
                  <node concept="3zrR0B" id="6F7CfdVIpmW" role="2ShVmc">
                    <node concept="3Tqbb2" id="6F7CfdVIpmX" role="3zrR0E">
                      <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="4rl$PSrindJ" role="6VMZX">
      <node concept="2iRfu4" id="4rl$PSrindK" role="2iSdaV" />
      <node concept="3F0ifn" id="4rl$PSrindL" role="3EZMnx">
        <property role="3F0ifm" value="Constraint" />
      </node>
      <node concept="3F0A7n" id="4rl$PSrindM" role="3EZMnx">
        <ref role="1NtTu8" to="4s7a:79Do2olI3d1" resolve="constraints" />
      </node>
    </node>
    <node concept="2aJ2om" id="4rl$PSrinwn" role="CpUAK">
      <ref role="2$4xQ3" node="16Y907hodbO" resolve="BoxViewNonClone" />
    </node>
  </node>
  <node concept="24kQdi" id="4rl$PSrinLA">
    <ref role="1XX52x" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="3EZMnI" id="5Z3d8P9bit6" role="2wV5jI">
      <node concept="2iRfu4" id="5Z3d8P9bit7" role="2iSdaV" />
      <node concept="3F1sOY" id="5epN3$YUiRW" role="3EZMnx">
        <property role="2ru_X1" value="true" />
        <ref role="1NtTu8" to="4s7a:3LUIgcmYqcd" resolve="intention" />
        <node concept="Veino" id="5epN3$YUiRX" role="3F10Kt" />
        <node concept="35HoNQ" id="5epN3$YUiRY" role="2ruayu" />
      </node>
      <node concept="3EZMnI" id="4rl$PSrinLB" role="3EZMnx">
        <node concept="l2Vlx" id="4rl$PSrinLC" role="2iSdaV" />
        <node concept="3EZMnI" id="7cMGUmYLqZY" role="3EZMnx">
          <node concept="2iRfu4" id="7cMGUmYLqZZ" role="2iSdaV" />
          <node concept="3F0A7n" id="4rl$PSrinLF" role="3EZMnx">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <node concept="ljvvj" id="4rl$PSrinLG" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="Vb9p2" id="4rl$PSrinLH" role="3F10Kt">
              <property role="Vbekb" value="BOLD" />
            </node>
            <node concept="VQ3r3" id="4rl$PSrinLI" role="3F10Kt">
              <property role="2USNnj" value="2" />
            </node>
          </node>
          <node concept="3F0ifn" id="7cMGUmYXtJm" role="3EZMnx">
            <property role="3F0ifm" value="[" />
          </node>
          <node concept="3F0ifn" id="7cMGUmYLrl8" role="3EZMnx">
            <property role="3F0ifm" value="Open Integration Chunks:" />
          </node>
          <node concept="1HlG4h" id="7cMGUmYL7ey" role="3EZMnx">
            <node concept="1HfYo3" id="7cMGUmYL7e$" role="1HlULh">
              <node concept="3TQlhw" id="7cMGUmYL7eA" role="1Hhtcw">
                <node concept="3clFbS" id="7cMGUmYL7eC" role="2VODD2">
                  <node concept="3cpWs8" id="7cMGUmYLdIz" role="3cqZAp">
                    <node concept="3cpWsn" id="7cMGUmYLdIA" role="3cpWs9">
                      <property role="TrG5h" value="count" />
                      <node concept="10Oyi0" id="7cMGUmYLdIy" role="1tU5fm" />
                      <node concept="3cmrfG" id="7cMGUmYLdZM" role="33vP2m">
                        <property role="3cmrfH" value="-1" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="7cMGUmYVi_$" role="3cqZAp" />
                  <node concept="3clFbJ" id="7cMGUmYVkaA" role="3cqZAp">
                    <node concept="3clFbS" id="7cMGUmYVkaC" role="3clFbx">
                      <node concept="3clFbF" id="7cMGUmYVo_7" role="3cqZAp">
                        <node concept="37vLTI" id="7cMGUmYVqXB" role="3clFbG">
                          <node concept="2OqwBi" id="7cMGUmYVGuA" role="37vLTx">
                            <node concept="2OqwBi" id="7cMGUmYV$48" role="2Oq$k0">
                              <node concept="2OqwBi" id="7cMGUmYVu4y" role="2Oq$k0">
                                <node concept="2OqwBi" id="7cMGUmYVs$F" role="2Oq$k0">
                                  <node concept="pncrf" id="7cMGUmYVs2$" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="7cMGUmYVtf2" role="2OqNvi">
                                    <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                                  </node>
                                </node>
                                <node concept="2Rf3mk" id="7cMGUmYVvjN" role="2OqNvi">
                                  <node concept="1xMEDy" id="7cMGUmYVvjP" role="1xVPHs">
                                    <node concept="chp4Y" id="7cMGUmYVwI7" role="ri$Ld">
                                      <ref role="cht4Q" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3zZkjj" id="7cMGUmYVBOm" role="2OqNvi">
                                <node concept="1bVj0M" id="7cMGUmYVBOo" role="23t8la">
                                  <node concept="3clFbS" id="7cMGUmYVBOp" role="1bW5cS">
                                    <node concept="3clFbF" id="7cMGUmYVD5P" role="3cqZAp">
                                      <node concept="2OqwBi" id="7cMGUmYVDH4" role="3clFbG">
                                        <node concept="37vLTw" id="7cMGUmYVD5O" role="2Oq$k0">
                                          <ref role="3cqZAo" node="7cMGUmYVBOq" resolve="it" />
                                        </node>
                                        <node concept="2qgKlT" id="7cMGUmYVF2d" role="2OqNvi">
                                          <ref role="37wK5l" to="7zqe:2NfWTcLJRHk" resolve="isCloneIf" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="Rh6nW" id="7cMGUmYVBOq" role="1bW2Oz">
                                    <property role="TrG5h" value="it" />
                                    <node concept="2jxLKc" id="7cMGUmYVBOr" role="1tU5fm" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="34oBXx" id="7cMGUmYVGTq" role="2OqNvi" />
                          </node>
                          <node concept="37vLTw" id="7cMGUmYVo_5" role="37vLTJ">
                            <ref role="3cqZAo" node="7cMGUmYLdIA" resolve="count" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3y3z36" id="7cMGUmYVntt" role="3clFbw">
                      <node concept="10Nm6u" id="7cMGUmYVo16" role="3uHU7w" />
                      <node concept="2OqwBi" id="7cMGUmYVlND" role="3uHU7B">
                        <node concept="pncrf" id="7cMGUmYVlhR" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7cMGUmYVmCj" role="2OqNvi">
                          <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="7cMGUmYVjAk" role="3cqZAp" />
                  <node concept="3cpWs6" id="7cMGUmYLeVm" role="3cqZAp">
                    <node concept="2YIFZM" id="7cMGUmYLfkX" role="3cqZAk">
                      <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                      <ref role="37wK5l" to="wyt6:~Integer.toString(int):java.lang.String" resolve="toString" />
                      <node concept="37vLTw" id="7cMGUmYLfux" role="37wK5m">
                        <ref role="3cqZAo" node="7cMGUmYLdIA" resolve="count" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3F0ifn" id="7cMGUmYXuCn" role="3EZMnx">
            <property role="3F0ifm" value="]" />
          </node>
        </node>
        <node concept="3F0ifn" id="775CKuYA59N" role="3EZMnx">
          <property role="3F0ifm" value="Fork" />
          <node concept="30gYXW" id="775CKuYA5eb" role="3F10Kt">
            <property role="Vb096" value="yellow" />
          </node>
          <node concept="pVoyu" id="4DqDj9GnxDA" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="4rl$PSrinMk" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:2s5q4UUgwl" resolve="statements" />
          <ref role="1ERwB7" node="2NOvNR_qwA8" resolve="MyActionMap" />
          <node concept="l2Vlx" id="4rl$PSrinMl" role="2czzBx" />
          <node concept="pj6Ft" id="4rl$PSrinMm" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pVoyu" id="5U5wmQ0zsdQ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="4$FPG" id="6F7CfdVIpBP" role="4_6I_">
            <node concept="3clFbS" id="6F7CfdVIpBQ" role="2VODD2">
              <node concept="3clFbF" id="6F7CfdVIpV7" role="3cqZAp">
                <node concept="2ShNRf" id="6F7CfdVIpV8" role="3clFbG">
                  <node concept="3zrR0B" id="6F7CfdVIpV9" role="2ShVmc">
                    <node concept="3Tqbb2" id="6F7CfdVIpVa" role="3zrR0E">
                      <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="4rl$PSrinMo" role="6VMZX">
      <node concept="2iRfu4" id="4rl$PSrinMp" role="2iSdaV" />
      <node concept="3F0ifn" id="4rl$PSrinMq" role="3EZMnx">
        <property role="3F0ifm" value="Constraint" />
      </node>
      <node concept="3F0A7n" id="4rl$PSrinMr" role="3EZMnx">
        <ref role="1NtTu8" to="4s7a:79Do2olI3d1" resolve="constraints" />
      </node>
    </node>
    <node concept="2aJ2om" id="4rl$PSrio51" role="CpUAK">
      <ref role="2$4xQ3" node="16Y907hpg$k" resolve="BoxViewClones" />
    </node>
  </node>
  <node concept="24kQdi" id="3LUIgcmuyAZ">
    <ref role="1XX52x" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="2aJ2om" id="3LUIgcmu_nq" role="CpUAK">
      <ref role="2$4xQ3" node="3LUIgcmu_nc" resolve="Future" />
    </node>
    <node concept="3F1sOY" id="3LUIgcmuAa9" role="2wV5jI">
      <ref role="1NtTu8" to="4s7a:3LUIgcmtu8f" resolve="future" />
      <node concept="2w$q5c" id="3LUIgcm_Or8" role="3xwHhd">
        <node concept="2aJ2om" id="3LUIgcm_Or9" role="2w$qW5">
          <ref role="2$4xQ3" node="3LUIgcmu_nc" resolve="Future" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3LUIgcmYvMX">
    <ref role="1XX52x" to="4s7a:3LUIgcnlyz5" resolve="IntentionContainer" />
    <node concept="3EZMnI" id="3LUIgcnb3t_" role="2wV5jI">
      <node concept="2iRkQZ" id="3LUIgcnb3tA" role="2iSdaV" />
      <node concept="3gTLQM" id="3LUIgcn1QP1" role="3EZMnx">
        <node concept="3Fmcul" id="3LUIgcn1QP3" role="3FoqZy">
          <node concept="3clFbS" id="3LUIgcn1QP5" role="2VODD2">
            <node concept="3cpWs8" id="76MUimxvW9d" role="3cqZAp">
              <node concept="3cpWsn" id="76MUimxvW9e" role="3cpWs9">
                <property role="TrG5h" value="ip" />
                <node concept="3uibUv" id="76MUimxvW9f" role="1tU5fm">
                  <ref role="3uigEE" node="5wTmjWA3CRg" resolve="IntentionIndicatorPanel" />
                </node>
                <node concept="2ShNRf" id="76MUimxvWP5" role="33vP2m">
                  <node concept="HV5vD" id="76MUimxvXex" role="2ShVmc">
                    <ref role="HV5vE" node="5wTmjWA3CRg" resolve="IntentionIndicatorPanel" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="76MUimxvX_c" role="3cqZAp">
              <node concept="2OqwBi" id="76MUimxvXPV" role="3clFbG">
                <node concept="37vLTw" id="76MUimxvX_a" role="2Oq$k0">
                  <ref role="3cqZAo" node="76MUimxvW9e" resolve="ip" />
                </node>
                <node concept="liA8E" id="76MUimxvYdk" role="2OqNvi">
                  <ref role="37wK5l" node="50KMJClb6Rw" resolve="getFullIndicator" />
                  <node concept="pncrf" id="76MUimxvYva" role="37wK5m" />
                  <node concept="1Q80Hx" id="76MUimxvZ2t" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="V5hpn" id="3LUIgcmZR$u">
    <property role="TrG5h" value="test" />
    <node concept="3t5Usi" id="3LUIgcmZR$x" role="V601i">
      <property role="TrG5h" value="MyStyleAttribute" />
      <property role="iBDjm" value="true" />
      <node concept="10P_77" id="3LUIgcmZR$Z" role="3t5Oan" />
      <node concept="3clFbT" id="3LUIgcmZR$O" role="3t49C2">
        <property role="3clFbU" value="false" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="3LUIgcneGDT">
    <property role="TrG5h" value="IntentionIndicatorGraphic" />
    <node concept="2tJIrI" id="3LUIgcnh3I$" role="jymVt" />
    <node concept="312cEg" id="3LUIgcnh43d" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="yOffset" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="3LUIgcnh3Sz" role="1B3o_S" />
      <node concept="10Oyi0" id="3LUIgcnh41B" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="3LUIgcnhdQz" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="intention" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="3LUIgcnhdpQ" role="1B3o_S" />
      <node concept="3uibUv" id="67gZUMnqQvq" role="1tU5fm">
        <ref role="3uigEE" to="wyt6:~String" resolve="String" />
      </node>
    </node>
    <node concept="312cEg" id="67gZUMnr5Nn" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="featurename" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="67gZUMnr5sZ" role="1B3o_S" />
      <node concept="3uibUv" id="67gZUMnr5M9" role="1tU5fm">
        <ref role="3uigEE" to="wyt6:~String" resolve="String" />
      </node>
    </node>
    <node concept="312cEg" id="3LUIgcnNGDR" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="width" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="3LUIgcnNGnR" role="1B3o_S" />
      <node concept="10Oyi0" id="3LUIgcnNGCM" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="5XbSbOFZmmm" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="intentionType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5XbSbOFZlL3" role="1B3o_S" />
      <node concept="3uibUv" id="5XbSbOFZml9" role="1tU5fm">
        <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
      </node>
    </node>
    <node concept="2tJIrI" id="67gZUMnpVo1" role="jymVt" />
    <node concept="3Tm1VV" id="3LUIgcneGDU" role="1B3o_S" />
    <node concept="3uibUv" id="3LUIgcneGN$" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
    </node>
    <node concept="3clFbW" id="3LUIgcneH88" role="jymVt">
      <node concept="3cqZAl" id="3LUIgcneH89" role="3clF45" />
      <node concept="3clFbS" id="3LUIgcneH8b" role="3clF47">
        <node concept="3clFbF" id="3LUIgcnh4K5" role="3cqZAp">
          <node concept="37vLTI" id="3LUIgcnh8FW" role="3clFbG">
            <node concept="37vLTw" id="3LUIgcnh9Gt" role="37vLTx">
              <ref role="3cqZAo" node="3LUIgcnh4dT" resolve="yOffset" />
            </node>
            <node concept="2OqwBi" id="3LUIgcnh58X" role="37vLTJ">
              <node concept="Xjq3P" id="3LUIgcnh4K3" role="2Oq$k0" />
              <node concept="2OwXpG" id="3LUIgcnh6uX" role="2OqNvi">
                <ref role="2Oxat5" node="3LUIgcnh43d" resolve="yOffset" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3LUIgcnheTI" role="3cqZAp">
          <node concept="37vLTI" id="3LUIgcnhhsl" role="3clFbG">
            <node concept="37vLTw" id="67gZUMnqRi9" role="37vLTx">
              <ref role="3cqZAo" node="67gZUMnqMVJ" resolve="intention" />
            </node>
            <node concept="2OqwBi" id="3LUIgcnhfnQ" role="37vLTJ">
              <node concept="Xjq3P" id="3LUIgcnheTG" role="2Oq$k0" />
              <node concept="2OwXpG" id="3LUIgcnhg3g" role="2OqNvi">
                <ref role="2Oxat5" node="3LUIgcnhdQz" resolve="intention" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="67gZUMnr6Xs" role="3cqZAp">
          <node concept="37vLTI" id="67gZUMnr9C4" role="3clFbG">
            <node concept="37vLTw" id="67gZUMnravg" role="37vLTx">
              <ref role="3cqZAo" node="67gZUMnr60t" resolve="featurename" />
            </node>
            <node concept="2OqwBi" id="67gZUMnr7rg" role="37vLTJ">
              <node concept="Xjq3P" id="67gZUMnr6Xq" role="2Oq$k0" />
              <node concept="2OwXpG" id="67gZUMnr8O0" role="2OqNvi">
                <ref role="2Oxat5" node="67gZUMnr5Nn" resolve="featurename" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5XbSbOFZewt" role="3cqZAp">
          <node concept="37vLTI" id="5XbSbOFZjEK" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOFZkBn" role="37vLTx">
              <ref role="3cqZAo" node="5XbSbOFZbx2" resolve="width" />
            </node>
            <node concept="2OqwBi" id="5XbSbOFZeZY" role="37vLTJ">
              <node concept="Xjq3P" id="5XbSbOFZewr" role="2Oq$k0" />
              <node concept="2OwXpG" id="5XbSbOFZgle" role="2OqNvi">
                <ref role="2Oxat5" node="3LUIgcnNGDR" resolve="width" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5XbSbOFZna2" role="3cqZAp">
          <node concept="37vLTI" id="5XbSbOFZqgC" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOFZr61" role="37vLTx">
              <ref role="3cqZAo" node="5XbSbOFZkQu" resolve="intentionType" />
            </node>
            <node concept="2OqwBi" id="5XbSbOFZnDQ" role="37vLTJ">
              <node concept="Xjq3P" id="5XbSbOFZna0" role="2Oq$k0" />
              <node concept="2OwXpG" id="5XbSbOFZoZc" role="2OqNvi">
                <ref role="2Oxat5" node="5XbSbOFZmmm" resolve="intentionType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3LUIgcneI7h" role="3cqZAp">
          <node concept="1rXfSq" id="3LUIgcneI7g" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JComponent.setPreferredSize(java.awt.Dimension):void" resolve="setPreferredSize" />
            <node concept="2ShNRf" id="3LUIgcneKbd" role="37wK5m">
              <node concept="1pGfFk" id="3LUIgcneLoK" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                <node concept="37vLTw" id="5XbSbOFZdUB" role="37wK5m">
                  <ref role="3cqZAo" node="5XbSbOFZbx2" resolve="width" />
                </node>
                <node concept="37vLTw" id="3LUIgcnhbSm" role="37wK5m">
                  <ref role="3cqZAo" node="3LUIgcnh4dT" resolve="yOffset" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="3LUIgcneH8c" role="1B3o_S" />
      <node concept="37vLTG" id="5XbSbOFZbx2" role="3clF46">
        <property role="TrG5h" value="width" />
        <node concept="10Oyi0" id="5XbSbOFZbKG" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="3LUIgcnh4dT" role="3clF46">
        <property role="TrG5h" value="yOffset" />
        <node concept="10Oyi0" id="3LUIgcnh4dS" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5XbSbOFZkQu" role="3clF46">
        <property role="TrG5h" value="intentionType" />
        <node concept="3uibUv" id="5XbSbOFZldw" role="1tU5fm">
          <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
        </node>
      </node>
      <node concept="37vLTG" id="67gZUMnqMVJ" role="3clF46">
        <property role="TrG5h" value="intention" />
        <node concept="3uibUv" id="67gZUMnqN9s" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~String" resolve="String" />
        </node>
      </node>
      <node concept="37vLTG" id="67gZUMnr60t" role="3clF46">
        <property role="TrG5h" value="featurename" />
        <node concept="3uibUv" id="67gZUMnr6ek" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~String" resolve="String" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="3LUIgcneMu9" role="jymVt" />
    <node concept="3clFb_" id="3LUIgcneMz1" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="paintComponent" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tmbuc" id="3LUIgcneMz2" role="1B3o_S" />
      <node concept="3cqZAl" id="3LUIgcneMz4" role="3clF45" />
      <node concept="37vLTG" id="3LUIgcneMz5" role="3clF46">
        <property role="TrG5h" value="graphics" />
        <node concept="3uibUv" id="3LUIgcneMz6" role="1tU5fm">
          <ref role="3uigEE" to="z60i:~Graphics" resolve="Graphics" />
        </node>
      </node>
      <node concept="3clFbS" id="3LUIgcneMz7" role="3clF47">
        <node concept="3clFbF" id="3LUIgcneO9h" role="3cqZAp">
          <node concept="3nyPlj" id="3LUIgcneO9g" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JComponent.paintComponent(java.awt.Graphics):void" resolve="paintComponent" />
            <node concept="37vLTw" id="3LUIgcneP7U" role="37wK5m">
              <ref role="3cqZAo" node="3LUIgcneMz5" resolve="graphics" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3LUIgcnfwf9" role="3cqZAp">
          <node concept="2OqwBi" id="3LUIgcnfwHZ" role="3clFbG">
            <node concept="37vLTw" id="3LUIgcnfwf7" role="2Oq$k0">
              <ref role="3cqZAo" node="3LUIgcneMz5" resolve="graphics" />
            </node>
            <node concept="liA8E" id="3LUIgcnfwX3" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.setColor(java.awt.Color):void" resolve="setColor" />
              <node concept="1rXfSq" id="67gZUMnrbk2" role="37wK5m">
                <ref role="37wK5l" node="67gZUMnqSw2" resolve="getColor" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3LUIgcnePLT" role="3cqZAp">
          <node concept="2OqwBi" id="3LUIgcneQg7" role="3clFbG">
            <node concept="37vLTw" id="3LUIgcnePLR" role="2Oq$k0">
              <ref role="3cqZAo" node="3LUIgcneMz5" resolve="graphics" />
            </node>
            <node concept="liA8E" id="3LUIgcneQuF" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.fillRect(int,int,int,int):void" resolve="fillRect" />
              <node concept="3cmrfG" id="3LUIgcneRhr" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3cmrfG" id="3LUIgcneSle" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="3LUIgcnNJLX" role="37wK5m">
                <ref role="3cqZAo" node="3LUIgcnNGDR" resolve="width" />
              </node>
              <node concept="37vLTw" id="3LUIgcnhcIq" role="37wK5m">
                <ref role="3cqZAo" node="3LUIgcnh43d" resolve="yOffset" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3LUIgcneMz8" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="67gZUMnqRyF" role="jymVt" />
    <node concept="3clFb_" id="67gZUMnqSw2" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getColor" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="67gZUMnqSw5" role="3clF47">
        <node concept="3cpWs6" id="67gZUMnrlyU" role="3cqZAp">
          <node concept="2OqwBi" id="5XbSbOFZttO" role="3cqZAk">
            <node concept="37vLTw" id="5XbSbOFZsvY" role="2Oq$k0">
              <ref role="3cqZAo" node="5XbSbOFZmmm" resolve="intentionType" />
            </node>
            <node concept="2OwXpG" id="5XbSbOFZubV" role="2OqNvi">
              <ref role="2Oxat5" node="5XbSbOFYTCy" resolve="color" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="67gZUMnqRZJ" role="1B3o_S" />
      <node concept="3uibUv" id="67gZUMnqSuD" role="3clF45">
        <ref role="3uigEE" to="z60i:~Color" resolve="Color" />
      </node>
    </node>
    <node concept="2tJIrI" id="67gZUMnqqOp" role="jymVt" />
    <node concept="3clFb_" id="67gZUMnqLGW" role="jymVt">
      <property role="TrG5h" value="getTooltip" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="67gZUMnqLGY" role="3clF47">
        <node concept="3cpWs8" id="67gZUMnqYs3" role="3cqZAp">
          <node concept="3cpWsn" id="67gZUMnqYs4" role="3cpWs9">
            <property role="TrG5h" value="tooltip" />
            <node concept="17QB3L" id="67gZUMnr4wo" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbJ" id="67gZUMnqLGZ" role="3cqZAp">
          <node concept="3clFbS" id="67gZUMnqLH0" role="3clFbx">
            <node concept="3clFbF" id="67gZUMnqLHb" role="3cqZAp">
              <node concept="37vLTI" id="67gZUMnqLHc" role="3clFbG">
                <node concept="Xl_RD" id="67gZUMnqLHd" role="37vLTx">
                  <property role="Xl_RC" value="Keep" />
                </node>
                <node concept="37vLTw" id="67gZUMnr4pd" role="37vLTJ">
                  <ref role="3cqZAo" node="67gZUMnqYs4" resolve="tooltip" />
                </node>
              </node>
            </node>
          </node>
          <node concept="17R0WA" id="67gZUMnqLHf" role="3clFbw">
            <node concept="Xl_RD" id="67gZUMnqLHg" role="3uHU7w">
              <property role="Xl_RC" value="Keep" />
            </node>
            <node concept="37vLTw" id="67gZUMnqUaU" role="3uHU7B">
              <ref role="3cqZAo" node="3LUIgcnhdQz" resolve="intention" />
            </node>
          </node>
          <node concept="3eNFk2" id="67gZUMnqLHk" role="3eNLev">
            <node concept="3clFbS" id="67gZUMnqLHl" role="3eOfB_">
              <node concept="3clFbF" id="67gZUMnqLHw" role="3cqZAp">
                <node concept="37vLTI" id="67gZUMnqLHx" role="3clFbG">
                  <node concept="3cpWs3" id="67gZUMnqLHy" role="37vLTx">
                    <node concept="37vLTw" id="67gZUMnreGD" role="3uHU7w">
                      <ref role="3cqZAo" node="67gZUMnr5Nn" resolve="featurename" />
                    </node>
                    <node concept="Xl_RD" id="67gZUMnqLHA" role="3uHU7B">
                      <property role="Xl_RC" value="KeepAsFeature " />
                    </node>
                  </node>
                  <node concept="37vLTw" id="67gZUMnr3Ad" role="37vLTJ">
                    <ref role="3cqZAo" node="67gZUMnqYs4" resolve="tooltip" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="17R0WA" id="67gZUMnqLHC" role="3eO9$A">
              <node concept="Xl_RD" id="67gZUMnqLHD" role="3uHU7w">
                <property role="Xl_RC" value="KeepAsFeature" />
              </node>
              <node concept="37vLTw" id="67gZUMnqUec" role="3uHU7B">
                <ref role="3cqZAo" node="3LUIgcnhdQz" resolve="intention" />
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="67gZUMnqLHH" role="9aQIa">
            <node concept="3clFbS" id="67gZUMnqLHI" role="9aQI4">
              <node concept="3clFbF" id="67gZUMnqLHT" role="3cqZAp">
                <node concept="37vLTI" id="67gZUMnqLHU" role="3clFbG">
                  <node concept="37vLTw" id="67gZUMnr1U2" role="37vLTJ">
                    <ref role="3cqZAo" node="67gZUMnqYs4" resolve="tooltip" />
                  </node>
                  <node concept="3cpWs3" id="67gZUMnqLHW" role="37vLTx">
                    <node concept="37vLTw" id="67gZUMnrfNC" role="3uHU7w">
                      <ref role="3cqZAo" node="3LUIgcnhdQz" resolve="intention" />
                    </node>
                    <node concept="Xl_RD" id="67gZUMnqLI0" role="3uHU7B">
                      <property role="Xl_RC" value="Intention " />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="67gZUMnqLI1" role="3eNLev">
            <node concept="17R0WA" id="67gZUMnqLI2" role="3eO9$A">
              <node concept="Xl_RD" id="67gZUMnqLI3" role="3uHU7w">
                <property role="Xl_RC" value="Exclusive" />
              </node>
              <node concept="37vLTw" id="67gZUMnqV2C" role="3uHU7B">
                <ref role="3cqZAo" node="3LUIgcnhdQz" resolve="intention" />
              </node>
            </node>
            <node concept="3clFbS" id="67gZUMnqLI7" role="3eOfB_">
              <node concept="3clFbF" id="67gZUMnqLIi" role="3cqZAp">
                <node concept="37vLTI" id="67gZUMnqLIj" role="3clFbG">
                  <node concept="3cpWs3" id="67gZUMnqLIk" role="37vLTx">
                    <node concept="Xl_RD" id="67gZUMnqLIo" role="3uHU7B">
                      <property role="Xl_RC" value="Exclusive " />
                    </node>
                    <node concept="37vLTw" id="67gZUMnrcUQ" role="3uHU7w">
                      <ref role="3cqZAo" node="67gZUMnr5Nn" resolve="featurename" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="67gZUMnr2Nd" role="37vLTJ">
                    <ref role="3cqZAo" node="67gZUMnqYs4" resolve="tooltip" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="67gZUMnqZw7" role="3cqZAp">
          <node concept="37vLTw" id="67gZUMnr0qX" role="3cqZAk">
            <ref role="3cqZAo" node="67gZUMnqYs4" resolve="tooltip" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="67gZUMnr4Ly" role="3clF45" />
      <node concept="3Tm1VV" id="67gZUMnqLIN" role="1B3o_S" />
    </node>
  </node>
  <node concept="24kQdi" id="43XlAbjeR$H">
    <ref role="1XX52x" to="4s7a:43XlAbjeR$w" resolve="ExclusiveSwitcher" />
    <node concept="3EZMnI" id="43XlAbjeR$M" role="2wV5jI">
      <node concept="2iRkQZ" id="43XlAbjeR$N" role="2iSdaV" />
      <node concept="3F0ifn" id="43XlAbjeR$J" role="3EZMnx">
        <property role="3F0ifm" value="Exclusive Switcher" />
        <node concept="VSNWy" id="43XlAbjeSJA" role="3F10Kt">
          <property role="1lJzqX" value="12" />
        </node>
        <node concept="Vb9p2" id="43XlAbjeSNZ" role="3F10Kt">
          <property role="Vbekb" value="PLAIN" />
        </node>
        <node concept="VechU" id="43XlAbjeSSt" role="3F10Kt">
          <property role="Vb096" value="gray" />
        </node>
      </node>
      <node concept="3EZMnI" id="43XlAbjXKoh" role="3EZMnx">
        <node concept="2iRfu4" id="43XlAbjXKoi" role="2iSdaV" />
        <node concept="3F0ifn" id="43XlAbjXIFO" role="3EZMnx">
          <property role="3F0ifm" value="Featurename:" />
        </node>
        <node concept="3F0A7n" id="43XlAbk3tkd" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:43XlAbjXKnI" resolve="featurename" />
        </node>
      </node>
      <node concept="3EZMnI" id="43XlAbjeSX4" role="3EZMnx">
        <node concept="2iRfu4" id="43XlAbjeSX5" role="2iSdaV" />
        <node concept="3F2HdR" id="43XlAbjeSSA" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
          <node concept="2iRkQZ" id="43XlAbjeSSC" role="2czzBx" />
          <node concept="VPXOz" id="43XlAbjeSX1" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="107P5z" id="43XlAbjgQCS" role="12AuX0">
            <node concept="3clFbS" id="43XlAbjgQCT" role="2VODD2">
              <node concept="3clFbF" id="43XlAbjgQKi" role="3cqZAp">
                <node concept="3y3z36" id="43XlAbjgR00" role="3clFbG">
                  <node concept="10Nm6u" id="43XlAbjgR0a" role="3uHU7w" />
                  <node concept="12_Ws6" id="43XlAbjgQKh" role="3uHU7B" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F2HdR" id="43XlAbjgPVL" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
          <node concept="2iRkQZ" id="43XlAbjgPVO" role="2czzBx" />
          <node concept="107P5z" id="43XlAbjgPVU" role="12AuX0">
            <node concept="3clFbS" id="43XlAbjgPVV" role="2VODD2">
              <node concept="3clFbF" id="43XlAbjgQ34" role="3cqZAp">
                <node concept="3y3z36" id="43XlAbjgQiM" role="3clFbG">
                  <node concept="10Nm6u" id="43XlAbjgQqg" role="3uHU7w" />
                  <node concept="12_Ws6" id="43XlAbjgQ33" role="3uHU7B" />
                </node>
              </node>
            </node>
          </node>
          <node concept="VPXOz" id="43XlAbkalT0" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="43XlAbjpp1y" role="3EZMnx">
        <node concept="2iRfu4" id="43XlAbjpp1z" role="2iSdaV" />
        <node concept="3gTLQM" id="43XlAbjqZ1p" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbjqZ1q" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbjqZ1r" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbjqZ1s" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbjqZ1t" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbjqZ1u" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbjqZ1v" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbjqZ1w" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbjqZ1x" role="37wK5m">
                        <property role="Xl_RC" value="&lt;&lt;" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbjqZ1y" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbjqZ1z" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbjqZ1$" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbjqZ1t" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbjqZ1_" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Component.setFocusable(boolean):void" resolve="setFocusable" />
                    <node concept="3clFbT" id="43XlAbjqZ1A" role="37wK5m">
                      <property role="3clFbU" value="false" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbjqZ1B" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbjqZ1C" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbjqZ1D" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbjqZ1t" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbjqZ1E" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbjqZ1F" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbjqZ1G" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbjqZ1H" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <node concept="3Tm1VV" id="43XlAbjqZ1I" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbjqZ1J" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbjqZ1K" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbjqZ1L" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbjqZ1M" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbjqZ1N" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbjqZ1O" role="3clF47">
                              <node concept="2LD9aU" id="43XlAbjqZ1P" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbjqZ1Q" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbjqZ1R" role="1bW5cS">
                                    <node concept="3cpWs8" id="43XlAbjqZ1S" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbjqZ1T" role="3cpWs9">
                                        <property role="TrG5h" value="selectedNode" />
                                        <node concept="3Tqbb2" id="43XlAbjqZ1U" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                                        </node>
                                        <node concept="1PxgMI" id="43XlAbjqZ1V" role="33vP2m">
                                          <property role="1BlNFB" value="true" />
                                          <ref role="1m5ApE" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                                          <node concept="2OqwBi" id="43XlAbjqZ1W" role="1m5AlR">
                                            <node concept="1Q80Hx" id="43XlAbjqZ1X" role="2Oq$k0" />
                                            <node concept="liA8E" id="43XlAbjqZ1Y" role="2OqNvi">
                                              <ref role="37wK5l" to="cj4x:~EditorContext.getSelectedNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSelectedNode" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbJ" id="43XlAbjqZ23" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbjqZ24" role="3clFbx">
                                        <node concept="3clFbF" id="43XlAbjAU6p" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbjAU6q" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbjAU6r" role="2Oq$k0">
                                              <node concept="pncrf" id="43XlAbjAU6s" role="2Oq$k0" />
                                              <node concept="3Tsc0h" id="43XlAbjAUE8" role="2OqNvi">
                                                <ref role="3TtcxE" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                              </node>
                                            </node>
                                            <node concept="TSZUe" id="43XlAbjAU6u" role="2OqNvi">
                                              <node concept="37vLTw" id="43XlAbjAU6v" role="25WWJ7">
                                                <ref role="3cqZAo" node="43XlAbjqZ1T" resolve="selectedNode" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="43XlAbjqZ2c" role="3clFbw">
                                        <node concept="37vLTw" id="43XlAbjqZ2d" role="2Oq$k0">
                                          <ref role="3cqZAo" node="43XlAbjqZ1T" resolve="selectedNode" />
                                        </node>
                                        <node concept="1BlSNk" id="43XlAbjqZ2e" role="2OqNvi">
                                          <ref role="1BmUXE" to="4s7a:43XlAbjeR$w" resolve="ExclusiveSwitcher" />
                                          <ref role="1Bn3mz" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbjqZ2f" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbjqZ2g" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbjqZ2h" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbjqZ2i" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbjqZ2j" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbjqZ1t" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3gTLQM" id="43XlAbjppJT" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbjppJV" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbjppJX" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbjpq5p" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbjpq5q" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbjpq5r" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbjpq5s" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbjpq5t" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbjpq5u" role="37wK5m">
                        <property role="Xl_RC" value="&gt;&gt;" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbjqv47" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbjqvW4" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbjqv45" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbjpq5q" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbjqyFO" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Component.setFocusable(boolean):void" resolve="setFocusable" />
                    <node concept="3clFbT" id="43XlAbjqzac" role="37wK5m">
                      <property role="3clFbU" value="false" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbjpq5v" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbjpq5w" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbjpq5x" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbjpq5q" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbjpq5y" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbjpq5z" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbjpq5$" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbjpq5_" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <node concept="3Tm1VV" id="43XlAbjpq5A" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbjpq5B" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbjpq5C" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbjpq5D" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbjpq5E" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbjpq5F" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbjpq5G" role="3clF47">
                              <node concept="2LD9aU" id="43XlAbjpsWn" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbjpsWo" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbjpsWp" role="1bW5cS">
                                    <node concept="3cpWs8" id="43XlAbjpuar" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbjpuau" role="3cpWs9">
                                        <property role="TrG5h" value="selectedNode" />
                                        <node concept="3Tqbb2" id="43XlAbjpuaq" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                                        </node>
                                        <node concept="1PxgMI" id="43XlAbjp_IQ" role="33vP2m">
                                          <property role="1BlNFB" value="true" />
                                          <ref role="1m5ApE" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
                                          <node concept="2OqwBi" id="43XlAbjpuLP" role="1m5AlR">
                                            <node concept="1Q80Hx" id="43XlAbjpuwA" role="2Oq$k0" />
                                            <node concept="liA8E" id="43XlAbjpuZD" role="2OqNvi">
                                              <ref role="37wK5l" to="cj4x:~EditorContext.getSelectedNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSelectedNode" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbJ" id="43XlAbjpvhZ" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbjpvi1" role="3clFbx">
                                        <node concept="3clFbF" id="43XlAbjAOzc" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbjAQP8" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbjAOQn" role="2Oq$k0">
                                              <node concept="pncrf" id="43XlAbjAOza" role="2Oq$k0" />
                                              <node concept="3Tsc0h" id="43XlAbjAPBk" role="2OqNvi">
                                                <ref role="3TtcxE" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
                                              </node>
                                            </node>
                                            <node concept="TSZUe" id="43XlAbjATe6" role="2OqNvi">
                                              <node concept="37vLTw" id="43XlAbjATMS" role="25WWJ7">
                                                <ref role="3cqZAo" node="43XlAbjpuau" resolve="selectedNode" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="43XlAbjpvWy" role="3clFbw">
                                        <node concept="37vLTw" id="43XlAbjpvCN" role="2Oq$k0">
                                          <ref role="3cqZAo" node="43XlAbjpuau" resolve="selectedNode" />
                                        </node>
                                        <node concept="1BlSNk" id="43XlAbjpwaT" role="2OqNvi">
                                          <ref role="1BmUXE" to="4s7a:43XlAbjeR$w" resolve="ExclusiveSwitcher" />
                                          <ref role="1Bn3mz" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbjptx4" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbjpthq" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbjptI$" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbjpq67" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbjpq68" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbjpq5q" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3gTLQM" id="43XlAbjlESq" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbjlESs" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbjlESu" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbjlGOS" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbjlGOT" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbjlGOU" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbjlHfA" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbjlHN4" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbjlIdL" role="37wK5m">
                        <property role="Xl_RC" value="ok" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbjlM_p" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbjlNlI" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbjlM_n" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbjlGOT" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbjlPwB" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbjlPI_" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbjlQi7" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbjlQia" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <node concept="3Tm1VV" id="43XlAbjlQib" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbjlQic" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbjlQid" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbjlQif" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbjlQig" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbjlQih" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbjlQii" role="3clF47">
                              <node concept="34ab3g" id="4UnJuVxhIVj" role="3cqZAp">
                                <property role="35gtTG" value="info" />
                                <node concept="Xl_RD" id="4UnJuVxhIVl" role="34bqiv">
                                  <property role="Xl_RC" value="[LOG] Confirm execlusive intention." />
                                </node>
                              </node>
                              <node concept="2LD9aU" id="43XlAbjntTI" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbjntTK" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbjntTM" role="1bW5cS">
                                    <node concept="3cpWs8" id="43XlAbjCBuj" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbjCBum" role="3cpWs9">
                                        <property role="TrG5h" value="intention" />
                                        <node concept="3Tqbb2" id="43XlAbjCBuh" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:43XlAbjBn0d" resolve="ExclusiveIntention" />
                                        </node>
                                        <node concept="2ShNRf" id="43XlAbjCBVi" role="33vP2m">
                                          <node concept="3zrR0B" id="43XlAbjCCLz" role="2ShVmc">
                                            <node concept="3Tqbb2" id="43XlAbjCCL_" role="3zrR0E">
                                              <ref role="ehGHo" to="4s7a:43XlAbjBn0d" resolve="ExclusiveIntention" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbjCDbi" role="3cqZAp">
                                      <node concept="37vLTI" id="43XlAbjCEi3" role="3clFbG">
                                        <node concept="Xl_RD" id="43XlAbjCEuj" role="37vLTx">
                                          <property role="Xl_RC" value="Exclusive" />
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbjCDvA" role="37vLTJ">
                                          <node concept="37vLTw" id="43XlAbjCDbg" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                          </node>
                                          <node concept="3TrcHB" id="43XlAbjCDKA" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbjCFaO" role="3cqZAp">
                                      <node concept="37vLTI" id="43XlAbjCH7T" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbjCFve" role="37vLTJ">
                                          <node concept="37vLTw" id="43XlAbjCFaM" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                          </node>
                                          <node concept="3TrcHB" id="43XlAbjCFSX" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:3jeRE17nVF2" resolve="featurename" />
                                          </node>
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbjXOLY" role="37vLTx">
                                          <node concept="pncrf" id="43XlAbjXO7C" role="2Oq$k0" />
                                          <node concept="3TrcHB" id="43XlAbjXPQ7" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:43XlAbjXKnI" resolve="featurename" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkpa07" role="3cqZAp">
                                      <node concept="37vLTI" id="43XlAbkpcd_" role="3clFbG">
                                        <node concept="2YIFZM" id="43XlAbkpgbZ" role="37vLTx">
                                          <ref role="37wK5l" to="iptd:43XlAbkp7ZT" resolve="getView" />
                                          <ref role="1Pybhc" to="iptd:43XlAbkp4HG" resolve="IntentionHelper" />
                                          <node concept="2OqwBi" id="43XlAbkphTz" role="37wK5m">
                                            <node concept="1Q80Hx" id="43XlAbkph53" role="2Oq$k0" />
                                            <node concept="liA8E" id="43XlAbkpj6A" role="2OqNvi">
                                              <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkpaG9" role="37vLTJ">
                                          <node concept="37vLTw" id="43XlAbkpa05" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                          </node>
                                          <node concept="3TrcHB" id="43XlAbkpbc2" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:1SJQf$Wm6pV" resolve="view" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="34ab3g" id="4UnJuVxhJDo" role="3cqZAp">
                                      <property role="35gtTG" value="info" />
                                      <node concept="3cpWs3" id="4UnJuVxhOMb" role="34bqiv">
                                        <node concept="2OqwBi" id="4UnJuVxhPV0" role="3uHU7w">
                                          <node concept="37vLTw" id="4UnJuVxhPnm" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                          </node>
                                          <node concept="3TrcHB" id="4UnJuVxhQhW" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:1SJQf$Wm6pV" resolve="view" />
                                          </node>
                                        </node>
                                        <node concept="Xl_RD" id="4UnJuVxhJDq" role="3uHU7B">
                                          <property role="Xl_RC" value="[LOG] Exclusive intention on view " />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3SKdUt" id="43XlAbjDLAo" role="3cqZAp">
                                      <node concept="3SKdUq" id="43XlAbjDLAq" role="3SKWNk">
                                        <property role="3SKdUp" value="TODO- need to extend intention to multiple nodes" />
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbjDMNt" role="3cqZAp">
                                      <node concept="37vLTI" id="43XlAbjDPyQ" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbjDNGU" role="37vLTJ">
                                          <node concept="37vLTw" id="43XlAbjDMNr" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                          </node>
                                          <node concept="3TrEf2" id="43XlAbjDOHi" role="2OqNvi">
                                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                                          </node>
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbjDQxZ" role="37vLTx">
                                          <node concept="1uHKPH" id="43XlAbjDQy0" role="2OqNvi" />
                                          <node concept="2OqwBi" id="43XlAbjDQy1" role="2Oq$k0">
                                            <node concept="pncrf" id="43XlAbjDQy2" role="2Oq$k0" />
                                            <node concept="3Tsc0h" id="43XlAbjDQy3" role="2OqNvi">
                                              <ref role="3TtcxE" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbjCAOk" role="3cqZAp" />
                                    <node concept="2$JKZl" id="43XlAbjoQRG" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbjoQRI" role="2LFqv$">
                                        <node concept="3cpWs8" id="43XlAbjCLRA" role="3cqZAp">
                                          <node concept="3cpWsn" id="43XlAbjCLRD" role="3cpWs9">
                                            <property role="TrG5h" value="ref" />
                                            <node concept="3Tqbb2" id="43XlAbjCLR$" role="1tU5fm">
                                              <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                            </node>
                                            <node concept="2ShNRf" id="43XlAbjCN1f" role="33vP2m">
                                              <node concept="3zrR0B" id="43XlAbjCMR6" role="2ShVmc">
                                                <node concept="3Tqbb2" id="43XlAbjCMR7" role="3zrR0E">
                                                  <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbjCNDB" role="3cqZAp">
                                          <node concept="37vLTI" id="43XlAbjCOVQ" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbjCNTE" role="37vLTJ">
                                              <node concept="37vLTw" id="43XlAbjCND_" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbjCLRD" resolve="ref" />
                                              </node>
                                              <node concept="3TrEf2" id="43XlAbjCOo1" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                              </node>
                                            </node>
                                            <node concept="2OqwBi" id="43XlAbjCPxP" role="37vLTx">
                                              <node concept="1uHKPH" id="43XlAbjCPxQ" role="2OqNvi" />
                                              <node concept="2OqwBi" id="43XlAbjCPxR" role="2Oq$k0">
                                                <node concept="pncrf" id="43XlAbjCPxS" role="2Oq$k0" />
                                                <node concept="3Tsc0h" id="43XlAbjCPxT" role="2OqNvi">
                                                  <ref role="3TtcxE" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbjCQrb" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbjCTcG" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbjCQXd" role="2Oq$k0">
                                              <node concept="37vLTw" id="43XlAbjCQr9" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                              </node>
                                              <node concept="3Tsc0h" id="43XlAbjCRvL" role="2OqNvi">
                                                <ref role="3TtcxE" to="4s7a:43XlAbjBn0n" resolve="left" />
                                              </node>
                                            </node>
                                            <node concept="TSZUe" id="43XlAbjCWpM" role="2OqNvi">
                                              <node concept="37vLTw" id="43XlAbjCXdY" role="25WWJ7">
                                                <ref role="3cqZAo" node="43XlAbjCLRD" resolve="ref" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbjmvzz" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbjlVAf" role="3clFbG">
                                            <node concept="HtX7F" id="43XlAbjlW1E" role="2OqNvi">
                                              <node concept="2OqwBi" id="43XlAbjlY1u" role="HtX7I">
                                                <node concept="1uHKPH" id="43XlAbjm3y9" role="2OqNvi" />
                                                <node concept="2OqwBi" id="43XlAbjmvQP" role="2Oq$k0">
                                                  <node concept="pncrf" id="43XlAbjnSeZ" role="2Oq$k0" />
                                                  <node concept="3Tsc0h" id="43XlAbjmw7s" role="2OqNvi">
                                                    <ref role="3TtcxE" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="pncrf" id="43XlAbjnRRE" role="2Oq$k0" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="43XlAbjoT3d" role="2$JKZa">
                                        <node concept="2OqwBi" id="43XlAbjoRg3" role="2Oq$k0">
                                          <node concept="pncrf" id="43XlAbjoR5N" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="43XlAbjoR_n" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                          </node>
                                        </node>
                                        <node concept="3GX2aA" id="43XlAbjoVro" role="2OqNvi" />
                                      </node>
                                    </node>
                                    <node concept="2$JKZl" id="43XlAbjC_Ck" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbjC_Cl" role="2LFqv$">
                                        <node concept="3cpWs8" id="43XlAbjCXTE" role="3cqZAp">
                                          <node concept="3cpWsn" id="43XlAbjCXTF" role="3cpWs9">
                                            <property role="TrG5h" value="ref" />
                                            <node concept="3Tqbb2" id="43XlAbjCXTG" role="1tU5fm">
                                              <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                            </node>
                                            <node concept="2ShNRf" id="43XlAbjCXTH" role="33vP2m">
                                              <node concept="3zrR0B" id="43XlAbjCXTI" role="2ShVmc">
                                                <node concept="3Tqbb2" id="43XlAbjCXTJ" role="3zrR0E">
                                                  <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbjCXTK" role="3cqZAp">
                                          <node concept="37vLTI" id="43XlAbjCXTL" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbjCXTM" role="37vLTJ">
                                              <node concept="37vLTw" id="43XlAbjCXTN" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbjCXTF" resolve="ref" />
                                              </node>
                                              <node concept="3TrEf2" id="43XlAbjCXTO" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                              </node>
                                            </node>
                                            <node concept="2OqwBi" id="43XlAbjCXTP" role="37vLTx">
                                              <node concept="1uHKPH" id="43XlAbjCXTQ" role="2OqNvi" />
                                              <node concept="2OqwBi" id="43XlAbjCXTR" role="2Oq$k0">
                                                <node concept="pncrf" id="43XlAbjCXTS" role="2Oq$k0" />
                                                <node concept="3Tsc0h" id="43XlAbjCZ_x" role="2OqNvi">
                                                  <ref role="3TtcxE" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbjCXTU" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbjCXTV" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbjCXTW" role="2Oq$k0">
                                              <node concept="37vLTw" id="43XlAbjCXTX" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                              </node>
                                              <node concept="3Tsc0h" id="43XlAbjCYKS" role="2OqNvi">
                                                <ref role="3TtcxE" to="4s7a:43XlAbjBn0p" resolve="right" />
                                              </node>
                                            </node>
                                            <node concept="TSZUe" id="43XlAbjCXTZ" role="2OqNvi">
                                              <node concept="37vLTw" id="43XlAbjCXU0" role="25WWJ7">
                                                <ref role="3cqZAo" node="43XlAbjCXTF" resolve="ref" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbjC_Cm" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbjC_Cn" role="3clFbG">
                                            <node concept="HtX7F" id="43XlAbjC_Co" role="2OqNvi">
                                              <node concept="2OqwBi" id="43XlAbjC_Cp" role="HtX7I">
                                                <node concept="1uHKPH" id="43XlAbjC_Cq" role="2OqNvi" />
                                                <node concept="2OqwBi" id="43XlAbjC_Cr" role="2Oq$k0">
                                                  <node concept="pncrf" id="43XlAbjC_Cs" role="2Oq$k0" />
                                                  <node concept="3Tsc0h" id="43XlAbjCAGT" role="2OqNvi">
                                                    <ref role="3TtcxE" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="pncrf" id="43XlAbjC_Cu" role="2Oq$k0" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="43XlAbjC_Cv" role="2$JKZa">
                                        <node concept="2OqwBi" id="43XlAbjC_Cw" role="2Oq$k0">
                                          <node concept="pncrf" id="43XlAbjC_Cx" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="43XlAbjCAqE" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
                                          </node>
                                        </node>
                                        <node concept="3GX2aA" id="43XlAbjC_Cz" role="2OqNvi" />
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbjC_BV" role="3cqZAp" />
                                    <node concept="3cpWs8" id="43XlAbjD1HV" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbjD1HY" role="3cpWs9">
                                        <property role="TrG5h" value="root" />
                                        <node concept="3Tqbb2" id="43XlAbjD1HT" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:3VMRtc4W287" resolve="CPP" />
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbjD3HI" role="33vP2m">
                                          <node concept="pncrf" id="43XlAbjD3qa" role="2Oq$k0" />
                                          <node concept="2Xjw5R" id="43XlAbjD47E" role="2OqNvi">
                                            <node concept="1xMEDy" id="43XlAbjD47G" role="1xVPHs">
                                              <node concept="chp4Y" id="43XlAbjD4oZ" role="ri$Ld">
                                                <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                                              </node>
                                            </node>
                                            <node concept="1xIGOp" id="43XlAbjD4V$" role="1xVPHs" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3SKdUt" id="3jeRE171DW$" role="3cqZAp">
                                      <node concept="3SKdUq" id="3jeRE171DWA" role="3SKWNk">
                                        <property role="3SKdUp" value="Initialize intention container" />
                                      </node>
                                    </node>
                                    <node concept="3clFbJ" id="3jeRE171A7$" role="3cqZAp">
                                      <node concept="3clFbS" id="3jeRE171A7A" role="3clFbx">
                                        <node concept="3clFbF" id="3LUIgcnr9lD" role="3cqZAp">
                                          <node concept="37vLTI" id="3LUIgcnra8h" role="3clFbG">
                                            <node concept="2ShNRf" id="3LUIgcnracX" role="37vLTx">
                                              <node concept="3zrR0B" id="3LUIgcnracJ" role="2ShVmc">
                                                <node concept="3Tqbb2" id="3LUIgcnracK" role="3zrR0E">
                                                  <ref role="ehGHo" to="4s7a:3LUIgcnlyz5" resolve="IntentionContainer" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2OqwBi" id="3LUIgcnr9vh" role="37vLTJ">
                                              <node concept="37vLTw" id="3LUIgcnr9lB" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbjD1HY" resolve="root" />
                                              </node>
                                              <node concept="3TrEf2" id="3LUIgcnr9OC" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="3clFbC" id="3jeRE171C2r" role="3clFbw">
                                        <node concept="10Nm6u" id="3jeRE171C2X" role="3uHU7w" />
                                        <node concept="2OqwBi" id="3jeRE171BnL" role="3uHU7B">
                                          <node concept="37vLTw" id="3jeRE171ASJ" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbjD1HY" resolve="root" />
                                          </node>
                                          <node concept="3TrEf2" id="3jeRE171BJj" role="2OqNvi">
                                            <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbjD8YB" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbjDdA9" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbjDbor" role="2Oq$k0">
                                          <node concept="2OqwBi" id="43XlAbjD9tS" role="2Oq$k0">
                                            <node concept="37vLTw" id="43XlAbjD8Y_" role="2Oq$k0">
                                              <ref role="3cqZAo" node="43XlAbjD1HY" resolve="root" />
                                            </node>
                                            <node concept="3TrEf2" id="43XlAbjDaXV" role="2OqNvi">
                                              <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                                            </node>
                                          </node>
                                          <node concept="3Tsc0h" id="43XlAbjDc09" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
                                          </node>
                                        </node>
                                        <node concept="TSZUe" id="43XlAbjDgSf" role="2OqNvi">
                                          <node concept="37vLTw" id="43XlAbjDhV$" role="25WWJ7">
                                            <ref role="3cqZAo" node="43XlAbjCBum" resolve="intention" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbjG4El" role="3cqZAp" />
                                    <node concept="3clFbF" id="43XlAbjoVP2" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbjoVXI" role="3clFbG">
                                        <node concept="pncrf" id="43XlAbjoVP0" role="2Oq$k0" />
                                        <node concept="1PgB_6" id="43XlAbjoWyN" role="2OqNvi" />
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbjG5iU" role="3cqZAp" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbjoY0i" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbjoXJ9" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbjoYbq" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                              <node concept="3clFbF" id="nocPt_q7f1" role="3cqZAp">
                                <node concept="2OqwBi" id="nocPt_q9k3" role="3clFbG">
                                  <node concept="2OqwBi" id="nocPt_q7K3" role="2Oq$k0">
                                    <node concept="1Q80Hx" id="nocPt_q7eZ" role="2Oq$k0" />
                                    <node concept="liA8E" id="nocPt_q9ep" role="2OqNvi">
                                      <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                                    </node>
                                  </node>
                                  <node concept="liA8E" id="nocPt_q9sS" role="2OqNvi">
                                    <ref role="37wK5l" to="f4zo:~EditorCell.relayout():void" resolve="relayout" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbjlF3M" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbjlIQP" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbjlGOT" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3gTLQM" id="43XlAbkbuII" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbkbuIJ" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbkbuIK" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbkbuIL" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbkbuIM" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbkbuIN" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbkbuIO" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbkbuIP" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbkbuIQ" role="37wK5m">
                        <property role="Xl_RC" value="cancel" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbkbuIR" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbkbuIS" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbkbuIT" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbkbuIM" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbkbuIU" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbkbuIV" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbkbuIW" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbkbuIX" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <node concept="3Tm1VV" id="43XlAbkbuIY" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbkbuIZ" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbkbuJ0" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbkbuJ1" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbkbuJ2" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbkbuJ3" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbkbuJ4" role="3clF47">
                              <node concept="34ab3g" id="4UnJuVxhI6v" role="3cqZAp">
                                <property role="35gtTG" value="info" />
                                <node concept="Xl_RD" id="4UnJuVxhI6x" role="34bqiv">
                                  <property role="Xl_RC" value="[LOG] Cancel Exclusive intention." />
                                </node>
                              </node>
                              <node concept="2LD9aU" id="43XlAbkbuJ5" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbkbuJ6" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbkbuJ7" role="1bW5cS">
                                    <node concept="2$JKZl" id="43XlAbkbuKm" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbkbuKn" role="2LFqv$">
                                        <node concept="3clFbF" id="43XlAbkbuKJ" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbkbuKK" role="3clFbG">
                                            <node concept="HtX7F" id="43XlAbkbuKL" role="2OqNvi">
                                              <node concept="2OqwBi" id="43XlAbkbuKM" role="HtX7I">
                                                <node concept="1uHKPH" id="43XlAbkbuKN" role="2OqNvi" />
                                                <node concept="2OqwBi" id="43XlAbkbuKO" role="2Oq$k0">
                                                  <node concept="pncrf" id="43XlAbkbuKP" role="2Oq$k0" />
                                                  <node concept="3Tsc0h" id="43XlAbkbuKQ" role="2OqNvi">
                                                    <ref role="3TtcxE" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="pncrf" id="43XlAbkbuKR" role="2Oq$k0" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="43XlAbkbuKS" role="2$JKZa">
                                        <node concept="2OqwBi" id="43XlAbkbuKT" role="2Oq$k0">
                                          <node concept="pncrf" id="43XlAbkbuKU" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="43XlAbkbuKV" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbjeR$z" resolve="contentLeft" />
                                          </node>
                                        </node>
                                        <node concept="3GX2aA" id="43XlAbkbuKW" role="2OqNvi" />
                                      </node>
                                    </node>
                                    <node concept="2$JKZl" id="43XlAbkbuKX" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbkbuKY" role="2LFqv$">
                                        <node concept="3clFbF" id="43XlAbkbuLm" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbkbuLn" role="3clFbG">
                                            <node concept="HtX7F" id="43XlAbkbuLo" role="2OqNvi">
                                              <node concept="2OqwBi" id="43XlAbkbuLp" role="HtX7I">
                                                <node concept="1uHKPH" id="43XlAbkbuLq" role="2OqNvi" />
                                                <node concept="2OqwBi" id="43XlAbkbuLr" role="2Oq$k0">
                                                  <node concept="pncrf" id="43XlAbkbuLs" role="2Oq$k0" />
                                                  <node concept="3Tsc0h" id="43XlAbkbuLt" role="2OqNvi">
                                                    <ref role="3TtcxE" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="pncrf" id="43XlAbkbuLu" role="2Oq$k0" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="43XlAbkbuLv" role="2$JKZa">
                                        <node concept="2OqwBi" id="43XlAbkbuLw" role="2Oq$k0">
                                          <node concept="pncrf" id="43XlAbkbuLx" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="43XlAbkbuLy" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbjeSXw" resolve="contentRight" />
                                          </node>
                                        </node>
                                        <node concept="3GX2aA" id="43XlAbkbuLz" role="2OqNvi" />
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkbuMo" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkbuMp" role="3clFbG">
                                        <node concept="pncrf" id="43XlAbkbuMq" role="2Oq$k0" />
                                        <node concept="1PgB_6" id="43XlAbkbuMr" role="2OqNvi" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbkbuMt" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbkbuMu" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbkbuMv" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbkbuMw" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbkbuMx" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbkbuIM" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="43XlAbkzFk$">
    <ref role="1XX52x" to="4s7a:43XlAbkzFk6" resolve="OrderSelector" />
    <node concept="3EZMnI" id="43XlAbkzJW6" role="2wV5jI">
      <node concept="2iRkQZ" id="43XlAbkzJW7" role="2iSdaV" />
      <node concept="3F0ifn" id="43XlAbkzJW8" role="3EZMnx">
        <property role="3F0ifm" value="Order Selector" />
        <node concept="VSNWy" id="43XlAbkzJW9" role="3F10Kt">
          <property role="1lJzqX" value="12" />
        </node>
        <node concept="Vb9p2" id="43XlAbkzJWa" role="3F10Kt">
          <property role="Vbekb" value="PLAIN" />
        </node>
        <node concept="VechU" id="43XlAbkzJWb" role="3F10Kt">
          <property role="Vb096" value="gray" />
        </node>
      </node>
      <node concept="3EZMnI" id="43XlAbkzJWg" role="3EZMnx">
        <node concept="2iRfu4" id="43XlAbkzJWh" role="2iSdaV" />
        <node concept="3F2HdR" id="43XlAbkzOnL" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:43XlAbkzFk7" resolve="left" />
          <node concept="2iRkQZ" id="43XlAbkCM8q" role="2czzBx" />
          <node concept="VPXOz" id="43XlAbkzOwI" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0ifn" id="43XlAbkF_82" role="3EZMnx">
          <property role="3F0ifm" value="[&gt;]" />
        </node>
        <node concept="3F2HdR" id="43XlAbkzOo4" role="3EZMnx">
          <ref role="1NtTu8" to="4s7a:43XlAbkzFk9" resolve="right" />
          <node concept="2iRkQZ" id="43XlAbkCM8t" role="2czzBx" />
          <node concept="VPXOz" id="43XlAbkzOsr" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="43XlAbkFBd_" role="3EZMnx">
        <node concept="2iRfu4" id="43XlAbkFBdA" role="2iSdaV" />
        <node concept="3gTLQM" id="43XlAbkFBdB" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbkFBdC" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbkFBdD" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbkFBdE" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbkFBdF" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbkFBdG" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbkFBdH" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbkFBdI" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbkFBdJ" role="37wK5m">
                        <property role="Xl_RC" value="&lt;&lt;" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbkFBdK" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbkFBdL" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbkFBdM" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbkFBdF" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbkFBdN" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Component.setFocusable(boolean):void" resolve="setFocusable" />
                    <node concept="3clFbT" id="43XlAbkFBdO" role="37wK5m">
                      <property role="3clFbU" value="false" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbkFBdP" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbkFBdQ" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbkFBdR" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbkFBdF" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbkFBdS" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbkFBdT" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbkFBdU" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbkFBdV" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <node concept="3Tm1VV" id="43XlAbkFBdW" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbkFBdX" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbkFBdY" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbkFBdZ" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbkFBe0" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbkFBe1" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbkFBe2" role="3clF47">
                              <node concept="2LD9aU" id="43XlAbkFBe3" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbkFBe4" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbkFBe5" role="1bW5cS">
                                    <node concept="3cpWs8" id="43XlAbkFBe6" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbkFBe7" role="3cpWs9">
                                        <property role="TrG5h" value="selectedNode" />
                                        <node concept="3Tqbb2" id="43XlAbkFBe8" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                        </node>
                                        <node concept="1PxgMI" id="43XlAbkO3Xo" role="33vP2m">
                                          <property role="1BlNFB" value="true" />
                                          <ref role="1m5ApE" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                          <node concept="2OqwBi" id="43XlAbkFBea" role="1m5AlR">
                                            <node concept="1Q80Hx" id="43XlAbkFBeb" role="2Oq$k0" />
                                            <node concept="liA8E" id="43XlAbkFBec" role="2OqNvi">
                                              <ref role="37wK5l" to="cj4x:~EditorContext.getSelectedNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSelectedNode" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkL$MF" role="3cqZAp" />
                                    <node concept="3clFbJ" id="43XlAbkL_Z$" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbkL_ZA" role="3clFbx">
                                        <node concept="34ab3g" id="43XlAbkLGLH" role="3cqZAp">
                                          <property role="35gtTG" value="warn" />
                                          <node concept="Xl_RD" id="43XlAbkLGLJ" role="34bqiv">
                                            <property role="Xl_RC" value="no node selected / could not cast" />
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="43XlAbkLLU_" role="3cqZAp" />
                                      </node>
                                      <node concept="3clFbC" id="43XlAbkLDWY" role="3clFbw">
                                        <node concept="10Nm6u" id="43XlAbkLE7f" role="3uHU7w" />
                                        <node concept="37vLTw" id="43XlAbkLCOE" role="3uHU7B">
                                          <ref role="3cqZAo" node="43XlAbkFBe7" resolve="selectedNode" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkL$OG" role="3cqZAp" />
                                    <node concept="3cpWs8" id="43XlAbkGttk" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbkGttn" role="3cpWs9">
                                        <property role="TrG5h" value="ref" />
                                        <node concept="3Tqbb2" id="43XlAbkGtti" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkGGR8" role="33vP2m">
                                          <node concept="2OqwBi" id="43XlAbkGADg" role="2Oq$k0">
                                            <node concept="pncrf" id="43XlAbkG$ma" role="2Oq$k0" />
                                            <node concept="3Tsc0h" id="43XlAbkPDTZ" role="2OqNvi">
                                              <ref role="3TtcxE" to="4s7a:43XlAbkzFk9" resolve="right" />
                                            </node>
                                          </node>
                                          <node concept="1z4cxt" id="43XlAbkGLU1" role="2OqNvi">
                                            <node concept="1bVj0M" id="43XlAbkGLU3" role="23t8la">
                                              <node concept="3clFbS" id="43XlAbkGLU4" role="1bW5cS">
                                                <node concept="3clFbF" id="43XlAbkGOpG" role="3cqZAp">
                                                  <node concept="3clFbC" id="43XlAbkGY20" role="3clFbG">
                                                    <node concept="37vLTw" id="43XlAbkH2Q6" role="3uHU7w">
                                                      <ref role="3cqZAo" node="43XlAbkFBe7" resolve="selectedNode" />
                                                    </node>
                                                    <node concept="37vLTw" id="43XlAbkGOpF" role="3uHU7B">
                                                      <ref role="3cqZAo" node="43XlAbkGLU5" resolve="it" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="Rh6nW" id="43XlAbkGLU5" role="1bW2Oz">
                                                <property role="TrG5h" value="it" />
                                                <node concept="2jxLKc" id="43XlAbkGLU6" role="1tU5fm" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkFEOJ" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkFH8L" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbkFF2G" role="2Oq$k0">
                                          <node concept="pncrf" id="43XlAbkFEOH" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="43XlAbkPHeb" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbkzFk9" resolve="right" />
                                          </node>
                                        </node>
                                        <node concept="3dhRuq" id="43XlAbkHfbh" role="2OqNvi">
                                          <node concept="37vLTw" id="43XlAbkHfG4" role="25WWJ7">
                                            <ref role="3cqZAo" node="43XlAbkGttn" resolve="ref" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkHkYU" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkHutV" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbkHmQv" role="2Oq$k0">
                                          <node concept="pncrf" id="43XlAbkHkYS" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="43XlAbkPJeq" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbkzFk7" resolve="left" />
                                          </node>
                                        </node>
                                        <node concept="TSZUe" id="43XlAbkH$Uc" role="2OqNvi">
                                          <node concept="37vLTw" id="43XlAbkHBAU" role="25WWJ7">
                                            <ref role="3cqZAo" node="43XlAbkGttn" resolve="ref" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbkFBep" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbkFBeq" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbkFBer" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbkFBes" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbkFBet" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbkFBdF" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3gTLQM" id="43XlAbkFBeu" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbkFBev" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbkFBew" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbkFBex" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbkFBey" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbkFBez" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbkFBe$" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbkFBe_" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbkFBeA" role="37wK5m">
                        <property role="Xl_RC" value="&gt;&gt;" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbkFBeB" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbkFBeC" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbkFBeD" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbkFBey" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbkFBeE" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Component.setFocusable(boolean):void" resolve="setFocusable" />
                    <node concept="3clFbT" id="43XlAbkFBeF" role="37wK5m">
                      <property role="3clFbU" value="false" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbkFBeG" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbkFBeH" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbkFBeI" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbkFBey" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbkFBeJ" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbkFBeK" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbkFBeL" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbkFBeM" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <node concept="3Tm1VV" id="43XlAbkFBeN" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbkFBeO" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbkFBeP" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbkFBeQ" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbkFBeR" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbkFBeS" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbkFBeT" role="3clF47">
                              <node concept="2LD9aU" id="43XlAbkFBeU" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbkFBeV" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbkFBeW" role="1bW5cS">
                                    <node concept="3cpWs8" id="43XlAbkPudp" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbkPudq" role="3cpWs9">
                                        <property role="TrG5h" value="selectedNode" />
                                        <node concept="3Tqbb2" id="43XlAbkPudr" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                        </node>
                                        <node concept="1PxgMI" id="43XlAbkPuds" role="33vP2m">
                                          <property role="1BlNFB" value="true" />
                                          <ref role="1m5ApE" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                          <node concept="2OqwBi" id="43XlAbkPudt" role="1m5AlR">
                                            <node concept="1Q80Hx" id="43XlAbkPudu" role="2Oq$k0" />
                                            <node concept="liA8E" id="43XlAbkPudv" role="2OqNvi">
                                              <ref role="37wK5l" to="cj4x:~EditorContext.getSelectedNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSelectedNode" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkPudw" role="3cqZAp" />
                                    <node concept="3clFbJ" id="43XlAbkPudx" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbkPudy" role="3clFbx">
                                        <node concept="34ab3g" id="43XlAbkPudz" role="3cqZAp">
                                          <property role="35gtTG" value="warn" />
                                          <node concept="3cpWs3" id="5epN3$ZelX7" role="34bqiv">
                                            <node concept="2OqwBi" id="5epN3$Zeys7" role="3uHU7w">
                                              <node concept="2OqwBi" id="5epN3$Zeujn" role="2Oq$k0">
                                                <node concept="2OqwBi" id="5epN3$Zepl4" role="2Oq$k0">
                                                  <node concept="1Q80Hx" id="5epN3$ZenBx" role="2Oq$k0" />
                                                  <node concept="liA8E" id="5epN3$ZerSm" role="2OqNvi">
                                                    <ref role="37wK5l" to="cj4x:~EditorContext.getSelectedNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSelectedNode" />
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="5epN3$ZewTl" role="2OqNvi">
                                                  <ref role="37wK5l" to="mhbf:~SNode.getConcept():org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
                                                </node>
                                              </node>
                                              <node concept="liA8E" id="5epN3$ZezJF" role="2OqNvi">
                                                <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="43XlAbkPud$" role="3uHU7B">
                                              <property role="Xl_RC" value="no node selected / could not cast " />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="43XlAbkPud_" role="3cqZAp" />
                                      </node>
                                      <node concept="3clFbC" id="43XlAbkPudA" role="3clFbw">
                                        <node concept="10Nm6u" id="43XlAbkPudB" role="3uHU7w" />
                                        <node concept="37vLTw" id="43XlAbkPudC" role="3uHU7B">
                                          <ref role="3cqZAo" node="43XlAbkPudq" resolve="selectedNode" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkPudD" role="3cqZAp" />
                                    <node concept="3cpWs8" id="43XlAbkPudE" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbkPudF" role="3cpWs9">
                                        <property role="TrG5h" value="ref" />
                                        <node concept="3Tqbb2" id="43XlAbkPudG" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkPudH" role="33vP2m">
                                          <node concept="2OqwBi" id="43XlAbkPudI" role="2Oq$k0">
                                            <node concept="pncrf" id="43XlAbkPudJ" role="2Oq$k0" />
                                            <node concept="3Tsc0h" id="43XlAbkPBD8" role="2OqNvi">
                                              <ref role="3TtcxE" to="4s7a:43XlAbkzFk7" resolve="left" />
                                            </node>
                                          </node>
                                          <node concept="1z4cxt" id="43XlAbkPudL" role="2OqNvi">
                                            <node concept="1bVj0M" id="43XlAbkPudM" role="23t8la">
                                              <node concept="3clFbS" id="43XlAbkPudN" role="1bW5cS">
                                                <node concept="3clFbF" id="43XlAbkPudO" role="3cqZAp">
                                                  <node concept="3clFbC" id="43XlAbkPudP" role="3clFbG">
                                                    <node concept="37vLTw" id="43XlAbkPudQ" role="3uHU7w">
                                                      <ref role="3cqZAo" node="43XlAbkPudq" resolve="selectedNode" />
                                                    </node>
                                                    <node concept="37vLTw" id="43XlAbkPudR" role="3uHU7B">
                                                      <ref role="3cqZAo" node="43XlAbkPudS" resolve="it" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="Rh6nW" id="43XlAbkPudS" role="1bW2Oz">
                                                <property role="TrG5h" value="it" />
                                                <node concept="2jxLKc" id="43XlAbkPudT" role="1tU5fm" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="34ab3g" id="5epN3$Zgh6K" role="3cqZAp">
                                      <property role="35gtTG" value="warn" />
                                      <node concept="3cpWs3" id="5epN3$ZjLvW" role="34bqiv">
                                        <node concept="3cpWs3" id="5epN3$ZjSq0" role="3uHU7B">
                                          <node concept="2OqwBi" id="5epN3$Zk35t" role="3uHU7w">
                                            <node concept="2OqwBi" id="5epN3$Zk0at" role="2Oq$k0">
                                              <node concept="2OqwBi" id="5epN3$ZjY3B" role="2Oq$k0">
                                                <node concept="37vLTw" id="5epN3$ZjVVV" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="43XlAbkPudF" resolve="ref" />
                                                </node>
                                                <node concept="3TrEf2" id="5epN3$ZjZ5U" role="2OqNvi">
                                                  <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                                </node>
                                              </node>
                                              <node concept="2yIwOk" id="5epN3$Zk1pw" role="2OqNvi" />
                                            </node>
                                            <node concept="liA8E" id="5epN3$Zk5nP" role="2OqNvi">
                                              <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                                            </node>
                                          </node>
                                          <node concept="Xl_RD" id="5epN3$ZjO6U" role="3uHU7B">
                                            <property role="Xl_RC" value="move " />
                                          </node>
                                        </node>
                                        <node concept="Xl_RD" id="5epN3$Zgh6M" role="3uHU7w">
                                          <property role="Xl_RC" value=" from left to right" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkPudU" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkPudV" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbkPudW" role="2Oq$k0">
                                          <node concept="pncrf" id="43XlAbkPudX" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="43XlAbkPudY" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbkzFk7" resolve="left" />
                                          </node>
                                        </node>
                                        <node concept="3dhRuq" id="43XlAbkPudZ" role="2OqNvi">
                                          <node concept="37vLTw" id="43XlAbkPue0" role="25WWJ7">
                                            <ref role="3cqZAo" node="43XlAbkPudF" resolve="ref" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="jguzMl2qOK" role="3cqZAp" />
                                    <node concept="3cpWs8" id="jguzMl2Lzo" role="3cqZAp">
                                      <node concept="3cpWsn" id="jguzMl2Lzr" role="3cpWs9">
                                        <property role="TrG5h" value="newRef" />
                                        <node concept="3Tqbb2" id="jguzMl2Lzm" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                        </node>
                                        <node concept="2ShNRf" id="jguzMl2T5S" role="33vP2m">
                                          <node concept="3zrR0B" id="jguzMl2Yis" role="2ShVmc">
                                            <node concept="3Tqbb2" id="jguzMl2Yiu" role="3zrR0E">
                                              <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="jguzMl32pJ" role="3cqZAp">
                                      <node concept="37vLTI" id="jguzMl3eQm" role="3clFbG">
                                        <node concept="2OqwBi" id="jguzMl3iEo" role="37vLTx">
                                          <node concept="37vLTw" id="jguzMl3gWh" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkPudF" resolve="ref" />
                                          </node>
                                          <node concept="3TrEf2" id="jguzMl3lTP" role="2OqNvi">
                                            <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                          </node>
                                        </node>
                                        <node concept="2OqwBi" id="jguzMl37eF" role="37vLTJ">
                                          <node concept="37vLTw" id="jguzMl32pH" role="2Oq$k0">
                                            <ref role="3cqZAo" node="jguzMl2Lzr" resolve="newRef" />
                                          </node>
                                          <node concept="3TrEf2" id="jguzMl6hr3" role="2OqNvi">
                                            <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="jguzMl1UWi" role="3cqZAp">
                                      <node concept="2OqwBi" id="jguzMl22Cq" role="3clFbG">
                                        <node concept="2OqwBi" id="jguzMl1WXZ" role="2Oq$k0">
                                          <node concept="pncrf" id="jguzMl1UWg" role="2Oq$k0" />
                                          <node concept="3Tsc0h" id="jguzMl20ac" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:43XlAbkzFk9" resolve="right" />
                                          </node>
                                        </node>
                                        <node concept="TSZUe" id="jguzMl28ir" role="2OqNvi">
                                          <node concept="37vLTw" id="jguzMl3ANo" role="25WWJ7">
                                            <ref role="3cqZAo" node="jguzMl2Lzr" resolve="newRef" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbkFBfg" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbkFBfh" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbkFBfi" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbkFBfj" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbkFBfk" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbkFBey" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3gTLQM" id="43XlAbkFBfl" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbkFBfm" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbkFBfn" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbkFBfo" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbkFBfp" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbkFBfq" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbkFBfr" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbkFBfs" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbkFBft" role="37wK5m">
                        <property role="Xl_RC" value="ok" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbkFBfu" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbkFBfv" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbkFBfw" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbkFBfp" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbkFBfx" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbkFBfy" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbkFBfz" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbkFBf$" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <node concept="3Tm1VV" id="43XlAbkFBf_" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbkFBfA" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbkFBfB" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbkFBfC" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbkFBfD" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbkFBfE" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbkFBfF" role="3clF47">
                              <node concept="2LD9aU" id="43XlAbkS5qF" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbkS5qG" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbkS5qH" role="1bW5cS">
                                    <node concept="3cpWs8" id="43XlAbkS5qI" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbkS5qJ" role="3cpWs9">
                                        <property role="TrG5h" value="intention" />
                                        <node concept="3Tqbb2" id="43XlAbkS5qK" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:43XlAbkSjHr" resolve="OrderIntention" />
                                        </node>
                                        <node concept="2ShNRf" id="43XlAbkS5qL" role="33vP2m">
                                          <node concept="3zrR0B" id="43XlAbkS5qM" role="2ShVmc">
                                            <node concept="3Tqbb2" id="43XlAbkS5qN" role="3zrR0E">
                                              <ref role="ehGHo" to="4s7a:43XlAbkSjHr" resolve="OrderIntention" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkS5qO" role="3cqZAp">
                                      <node concept="37vLTI" id="43XlAbkS5qP" role="3clFbG">
                                        <node concept="Xl_RD" id="43XlAbkS5qQ" role="37vLTx">
                                          <property role="Xl_RC" value="Order" />
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkS5qR" role="37vLTJ">
                                          <node concept="37vLTw" id="43XlAbkS5qS" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkS5qJ" resolve="intention" />
                                          </node>
                                          <node concept="3TrcHB" id="43XlAbkS5qT" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkS5r2" role="3cqZAp">
                                      <node concept="37vLTI" id="43XlAbkS5r3" role="3clFbG">
                                        <node concept="2YIFZM" id="43XlAbkS5r4" role="37vLTx">
                                          <ref role="37wK5l" to="iptd:43XlAbkp7ZT" resolve="getView" />
                                          <ref role="1Pybhc" to="iptd:43XlAbkp4HG" resolve="IntentionHelper" />
                                          <node concept="2OqwBi" id="43XlAbkS5r5" role="37wK5m">
                                            <node concept="1Q80Hx" id="43XlAbkS5r6" role="2Oq$k0" />
                                            <node concept="liA8E" id="43XlAbkS5r7" role="2OqNvi">
                                              <ref role="37wK5l" to="cj4x:~EditorContext.getContextCell():jetbrains.mps.openapi.editor.cells.EditorCell" resolve="getContextCell" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkS5r8" role="37vLTJ">
                                          <node concept="37vLTw" id="43XlAbkS5r9" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkS5qJ" resolve="intention" />
                                          </node>
                                          <node concept="3TrcHB" id="43XlAbkS5ra" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:1SJQf$Wm6pV" resolve="view" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkS5rb" role="3cqZAp" />
                                    <node concept="34ab3g" id="4UnJuVxjC8p" role="3cqZAp">
                                      <property role="35gtTG" value="info" />
                                      <node concept="3cpWs3" id="4UnJuVxjGJp" role="34bqiv">
                                        <node concept="2OqwBi" id="4UnJuVxjHVz" role="3uHU7w">
                                          <node concept="37vLTw" id="4UnJuVxjHn2" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkS5qJ" resolve="intention" />
                                          </node>
                                          <node concept="3TrcHB" id="4UnJuVxjIjm" role="2OqNvi">
                                            <ref role="3TsBF5" to="4s7a:1SJQf$Wm6pV" resolve="view" />
                                          </node>
                                        </node>
                                        <node concept="Xl_RD" id="4UnJuVxjC8r" role="3uHU7B">
                                          <property role="Xl_RC" value="[LOG] Confirm order intention through view " />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="4UnJuVxjBrU" role="3cqZAp" />
                                    <node concept="3SKdUt" id="43XlAbkS5rc" role="3cqZAp">
                                      <node concept="3SKdUq" id="43XlAbkS5rd" role="3SKWNk">
                                        <property role="3SKdUp" value="TODO- need to extend intention to multiple nodes" />
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkS5re" role="3cqZAp">
                                      <node concept="37vLTI" id="43XlAbkS5rf" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbkS5rg" role="37vLTJ">
                                          <node concept="37vLTw" id="43XlAbkS5rh" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkS5qJ" resolve="intention" />
                                          </node>
                                          <node concept="3TrEf2" id="43XlAbkS5ri" role="2OqNvi">
                                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                                          </node>
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkSiff" role="37vLTx">
                                          <node concept="2OqwBi" id="43XlAbkS5rj" role="2Oq$k0">
                                            <node concept="1uHKPH" id="43XlAbkS5rk" role="2OqNvi" />
                                            <node concept="2OqwBi" id="43XlAbkS5rl" role="2Oq$k0">
                                              <node concept="pncrf" id="43XlAbkS5rm" role="2Oq$k0" />
                                              <node concept="3Tsc0h" id="43XlAbkSaUJ" role="2OqNvi">
                                                <ref role="3TtcxE" to="4s7a:43XlAbkzFk7" resolve="left" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="3TrEf2" id="43XlAbkSjex" role="2OqNvi">
                                            <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkS5ro" role="3cqZAp" />
                                    <node concept="2Gpval" id="nocPt_mNIS" role="3cqZAp">
                                      <node concept="2GrKxI" id="nocPt_mNIU" role="2Gsz3X">
                                        <property role="TrG5h" value="leftNode" />
                                      </node>
                                      <node concept="2OqwBi" id="nocPt_mPym" role="2GsD0m">
                                        <node concept="pncrf" id="nocPt_mP87" role="2Oq$k0" />
                                        <node concept="3Tsc0h" id="nocPt_mPUk" role="2OqNvi">
                                          <ref role="3TtcxE" to="4s7a:43XlAbkzFk7" resolve="left" />
                                        </node>
                                      </node>
                                      <node concept="3clFbS" id="nocPt_mNIY" role="2LFqv$">
                                        <node concept="3cpWs8" id="43XlAbkS5rr" role="3cqZAp">
                                          <node concept="3cpWsn" id="43XlAbkS5rs" role="3cpWs9">
                                            <property role="TrG5h" value="ref" />
                                            <node concept="3Tqbb2" id="43XlAbkS5rt" role="1tU5fm">
                                              <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                            </node>
                                            <node concept="2ShNRf" id="43XlAbkS5ru" role="33vP2m">
                                              <node concept="3zrR0B" id="43XlAbkS5rv" role="2ShVmc">
                                                <node concept="3Tqbb2" id="43XlAbkS5rw" role="3zrR0E">
                                                  <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbkS5rx" role="3cqZAp">
                                          <node concept="37vLTI" id="43XlAbkS5ry" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbkS5rz" role="37vLTJ">
                                              <node concept="37vLTw" id="43XlAbkS5r$" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbkS5rs" resolve="ref" />
                                              </node>
                                              <node concept="3TrEf2" id="43XlAbkS5r_" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                              </node>
                                            </node>
                                            <node concept="2OqwBi" id="43XlAbkT6wi" role="37vLTx">
                                              <node concept="2GrUjf" id="nocPt_mT1Q" role="2Oq$k0">
                                                <ref role="2Gs0qQ" node="nocPt_mNIU" resolve="leftNode" />
                                              </node>
                                              <node concept="3TrEf2" id="43XlAbkT7d9" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="43XlAbkS5rF" role="3cqZAp">
                                          <node concept="2OqwBi" id="43XlAbkS5rG" role="3clFbG">
                                            <node concept="2OqwBi" id="43XlAbkS5rH" role="2Oq$k0">
                                              <node concept="37vLTw" id="43XlAbkS5rI" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbkS5qJ" resolve="intention" />
                                              </node>
                                              <node concept="3Tsc0h" id="nocPt_oZt1" role="2OqNvi">
                                                <ref role="3TtcxE" to="4s7a:43XlAbkSjHv" resolve="left" />
                                              </node>
                                            </node>
                                            <node concept="TSZUe" id="43XlAbkS5rK" role="2OqNvi">
                                              <node concept="37vLTw" id="43XlAbkS5rL" role="25WWJ7">
                                                <ref role="3cqZAo" node="43XlAbkS5rs" resolve="ref" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="2Gpval" id="nocPt_mTK$" role="3cqZAp">
                                      <node concept="2GrKxI" id="nocPt_mTK_" role="2Gsz3X">
                                        <property role="TrG5h" value="rightNode" />
                                      </node>
                                      <node concept="2OqwBi" id="nocPt_mTKA" role="2GsD0m">
                                        <node concept="pncrf" id="nocPt_mTKB" role="2Oq$k0" />
                                        <node concept="3Tsc0h" id="nocPt_mV$X" role="2OqNvi">
                                          <ref role="3TtcxE" to="4s7a:43XlAbkzFk9" resolve="right" />
                                        </node>
                                      </node>
                                      <node concept="3clFbS" id="nocPt_mTKD" role="2LFqv$">
                                        <node concept="3cpWs8" id="nocPt_mTKE" role="3cqZAp">
                                          <node concept="3cpWsn" id="nocPt_mTKF" role="3cpWs9">
                                            <property role="TrG5h" value="ref" />
                                            <node concept="3Tqbb2" id="nocPt_mTKG" role="1tU5fm">
                                              <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                            </node>
                                            <node concept="2ShNRf" id="nocPt_mTKH" role="33vP2m">
                                              <node concept="3zrR0B" id="nocPt_mTKI" role="2ShVmc">
                                                <node concept="3Tqbb2" id="nocPt_mTKJ" role="3zrR0E">
                                                  <ref role="ehGHo" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="nocPt_mTKK" role="3cqZAp">
                                          <node concept="37vLTI" id="nocPt_mTKL" role="3clFbG">
                                            <node concept="2OqwBi" id="nocPt_mTKM" role="37vLTJ">
                                              <node concept="37vLTw" id="nocPt_mTKN" role="2Oq$k0">
                                                <ref role="3cqZAo" node="nocPt_mTKF" resolve="ref" />
                                              </node>
                                              <node concept="3TrEf2" id="nocPt_mTKO" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                              </node>
                                            </node>
                                            <node concept="2OqwBi" id="nocPt_mTKP" role="37vLTx">
                                              <node concept="2GrUjf" id="nocPt_mTKQ" role="2Oq$k0">
                                                <ref role="2Gs0qQ" node="nocPt_mTK_" resolve="rightNode" />
                                              </node>
                                              <node concept="3TrEf2" id="nocPt_mTKR" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbF" id="nocPt_mTKS" role="3cqZAp">
                                          <node concept="2OqwBi" id="nocPt_mTKT" role="3clFbG">
                                            <node concept="2OqwBi" id="nocPt_mTKU" role="2Oq$k0">
                                              <node concept="37vLTw" id="nocPt_mTKV" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbkS5qJ" resolve="intention" />
                                              </node>
                                              <node concept="3Tsc0h" id="nocPt_oYlU" role="2OqNvi">
                                                <ref role="3TtcxE" to="4s7a:43XlAbkSjHw" resolve="right" />
                                              </node>
                                            </node>
                                            <node concept="TSZUe" id="nocPt_mTKX" role="2OqNvi">
                                              <node concept="37vLTw" id="nocPt_mTKY" role="25WWJ7">
                                                <ref role="3cqZAo" node="nocPt_mTKF" resolve="ref" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkS5sB" role="3cqZAp" />
                                    <node concept="3cpWs8" id="43XlAbkS5sC" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbkS5sD" role="3cpWs9">
                                        <property role="TrG5h" value="root" />
                                        <node concept="3Tqbb2" id="43XlAbkS5sE" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:3VMRtc4W287" resolve="CPP" />
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkS5sF" role="33vP2m">
                                          <node concept="pncrf" id="43XlAbkS5sG" role="2Oq$k0" />
                                          <node concept="2Xjw5R" id="43XlAbkS5sH" role="2OqNvi">
                                            <node concept="1xMEDy" id="43XlAbkS5sI" role="1xVPHs">
                                              <node concept="chp4Y" id="43XlAbkS5sJ" role="ri$Ld">
                                                <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                                              </node>
                                            </node>
                                            <node concept="1xIGOp" id="43XlAbkS5sK" role="1xVPHs" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3SKdUt" id="43XlAbkS5sL" role="3cqZAp">
                                      <node concept="3SKdUq" id="43XlAbkS5sM" role="3SKWNk">
                                        <property role="3SKdUp" value="Initialize intention container" />
                                      </node>
                                    </node>
                                    <node concept="3clFbJ" id="43XlAbkS5sN" role="3cqZAp">
                                      <node concept="3clFbS" id="43XlAbkS5sO" role="3clFbx">
                                        <node concept="3clFbF" id="43XlAbkS5sP" role="3cqZAp">
                                          <node concept="37vLTI" id="43XlAbkS5sQ" role="3clFbG">
                                            <node concept="2ShNRf" id="43XlAbkS5sR" role="37vLTx">
                                              <node concept="3zrR0B" id="43XlAbkS5sS" role="2ShVmc">
                                                <node concept="3Tqbb2" id="43XlAbkS5sT" role="3zrR0E">
                                                  <ref role="ehGHo" to="4s7a:3LUIgcnlyz5" resolve="IntentionContainer" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2OqwBi" id="43XlAbkS5sU" role="37vLTJ">
                                              <node concept="37vLTw" id="43XlAbkS5sV" role="2Oq$k0">
                                                <ref role="3cqZAo" node="43XlAbkS5sD" resolve="root" />
                                              </node>
                                              <node concept="3TrEf2" id="43XlAbkS5sW" role="2OqNvi">
                                                <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="3clFbC" id="43XlAbkS5sX" role="3clFbw">
                                        <node concept="10Nm6u" id="43XlAbkS5sY" role="3uHU7w" />
                                        <node concept="2OqwBi" id="43XlAbkS5sZ" role="3uHU7B">
                                          <node concept="37vLTw" id="43XlAbkS5t0" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkS5sD" resolve="root" />
                                          </node>
                                          <node concept="3TrEf2" id="43XlAbkS5t1" role="2OqNvi">
                                            <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkS5tf" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkS5tg" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbkS5th" role="2Oq$k0">
                                          <node concept="2OqwBi" id="43XlAbkS5ti" role="2Oq$k0">
                                            <node concept="37vLTw" id="43XlAbkS5tj" role="2Oq$k0">
                                              <ref role="3cqZAo" node="43XlAbkS5sD" resolve="root" />
                                            </node>
                                            <node concept="3TrEf2" id="43XlAbkS5tk" role="2OqNvi">
                                              <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                                            </node>
                                          </node>
                                          <node concept="3Tsc0h" id="43XlAbkS5tl" role="2OqNvi">
                                            <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
                                          </node>
                                        </node>
                                        <node concept="TSZUe" id="43XlAbkS5tm" role="2OqNvi">
                                          <node concept="37vLTw" id="43XlAbkS5tn" role="25WWJ7">
                                            <ref role="3cqZAo" node="43XlAbkS5qJ" resolve="intention" />
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkTjy$" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkTjy_" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbkTjyA" role="2Oq$k0">
                                          <node concept="37vLTw" id="43XlAbkTjyB" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkS5sD" resolve="root" />
                                          </node>
                                          <node concept="2Rf3mk" id="43XlAbkTjyC" role="2OqNvi">
                                            <node concept="1xMEDy" id="43XlAbkTjyD" role="1xVPHs">
                                              <node concept="chp4Y" id="43XlAbkTjyE" role="ri$Ld">
                                                <ref role="cht4Q" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="2es0OD" id="43XlAbkTjyF" role="2OqNvi">
                                          <node concept="1bVj0M" id="43XlAbkTjyG" role="23t8la">
                                            <node concept="3clFbS" id="43XlAbkTjyH" role="1bW5cS">
                                              <node concept="3clFbF" id="43XlAbkTjyI" role="3cqZAp">
                                                <node concept="37vLTI" id="43XlAbkTjyJ" role="3clFbG">
                                                  <node concept="3clFbT" id="43XlAbkTjyK" role="37vLTx">
                                                    <property role="3clFbU" value="false" />
                                                  </node>
                                                  <node concept="2OqwBi" id="43XlAbkTjyL" role="37vLTJ">
                                                    <node concept="37vLTw" id="43XlAbkTjyM" role="2Oq$k0">
                                                      <ref role="3cqZAo" node="43XlAbkTjyO" resolve="it" />
                                                    </node>
                                                    <node concept="3TrcHB" id="43XlAbkTjyN" role="2OqNvi">
                                                      <ref role="3TsBF5" to="4s7a:38Muwq1QG_A" resolve="isSelected" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Rh6nW" id="43XlAbkTjyO" role="1bW2Oz">
                                              <property role="TrG5h" value="it" />
                                              <node concept="2jxLKc" id="43XlAbkTjyP" role="1tU5fm" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkS5tq" role="3cqZAp" />
                                    <node concept="3clFbF" id="43XlAbkS5tr" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkS5ts" role="3clFbG">
                                        <node concept="pncrf" id="43XlAbkS5tt" role="2Oq$k0" />
                                        <node concept="1PgB_6" id="43XlAbkS5tu" role="2OqNvi" />
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="nocPt_tQ1h" role="3cqZAp" />
                                    <node concept="3SKdUt" id="nocPt_tKJd" role="3cqZAp">
                                      <node concept="3SKdUq" id="nocPt_tKJf" role="3SKWNk">
                                        <property role="3SKdUp" value="Update the editor so that the intention indicators are better positioned" />
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="nocPt_r$ZL" role="3cqZAp">
                                      <node concept="2OqwBi" id="nocPt_rADq" role="3clFbG">
                                        <node concept="2OqwBi" id="nocPt_rAmS" role="2Oq$k0">
                                          <node concept="2OqwBi" id="nocPt_r_wM" role="2Oq$k0">
                                            <node concept="1Q80Hx" id="nocPt_r$ZJ" role="2Oq$k0" />
                                            <node concept="liA8E" id="nocPt_rA4k" role="2OqNvi">
                                              <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                                            </node>
                                          </node>
                                          <node concept="liA8E" id="nocPt_rAvA" role="2OqNvi">
                                            <ref role="37wK5l" to="cj4x:~EditorComponent.getUpdater():jetbrains.mps.openapi.editor.update.Updater" resolve="getUpdater" />
                                          </node>
                                        </node>
                                        <node concept="liA8E" id="nocPt_rAOf" role="2OqNvi">
                                          <ref role="37wK5l" to="22ra:~Updater.update():void" resolve="update" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbH" id="43XlAbkS5tv" role="3cqZAp" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbkS5tw" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbkS5tx" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbkS5ty" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbkFBi$" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbkFBi_" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbkFBfp" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3gTLQM" id="43XlAbkFBiA" role="3EZMnx">
          <node concept="3Fmcul" id="43XlAbkFBiB" role="3FoqZy">
            <node concept="3clFbS" id="43XlAbkFBiC" role="2VODD2">
              <node concept="3cpWs8" id="43XlAbkFBiD" role="3cqZAp">
                <node concept="3cpWsn" id="43XlAbkFBiE" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <node concept="3uibUv" id="43XlAbkFBiF" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="43XlAbkFBiG" role="33vP2m">
                    <node concept="1pGfFk" id="43XlAbkFBiH" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="43XlAbkFBiI" role="37wK5m">
                        <property role="Xl_RC" value="cancel" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="43XlAbkFBiJ" role="3cqZAp">
                <node concept="2OqwBi" id="43XlAbkFBiK" role="3clFbG">
                  <node concept="37vLTw" id="43XlAbkFBiL" role="2Oq$k0">
                    <ref role="3cqZAo" node="43XlAbkFBiE" resolve="button" />
                  </node>
                  <node concept="liA8E" id="43XlAbkFBiM" role="2OqNvi">
                    <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                    <node concept="2ShNRf" id="43XlAbkFBiN" role="37wK5m">
                      <node concept="YeOm9" id="43XlAbkFBiO" role="2ShVmc">
                        <node concept="1Y3b0j" id="43XlAbkFBiP" role="YeSDq">
                          <property role="2bfB8j" value="true" />
                          <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                          <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                          <node concept="3Tm1VV" id="43XlAbkFBiQ" role="1B3o_S" />
                          <node concept="3clFb_" id="43XlAbkFBiR" role="jymVt">
                            <property role="1EzhhJ" value="false" />
                            <property role="TrG5h" value="actionPerformed" />
                            <property role="DiZV1" value="false" />
                            <property role="od$2w" value="false" />
                            <node concept="3Tm1VV" id="43XlAbkFBiS" role="1B3o_S" />
                            <node concept="3cqZAl" id="43XlAbkFBiT" role="3clF45" />
                            <node concept="37vLTG" id="43XlAbkFBiU" role="3clF46">
                              <property role="TrG5h" value="p0" />
                              <node concept="3uibUv" id="43XlAbkFBiV" role="1tU5fm">
                                <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="43XlAbkFBiW" role="3clF47">
                              <node concept="34ab3g" id="4UnJuVxjAPr" role="3cqZAp">
                                <property role="35gtTG" value="info" />
                                <node concept="Xl_RD" id="4UnJuVxjAPt" role="34bqiv">
                                  <property role="Xl_RC" value="[LOG] Cancel order intention." />
                                </node>
                              </node>
                              <node concept="2LD9aU" id="43XlAbkFBiX" role="3cqZAp">
                                <node concept="1QHqEC" id="43XlAbkFBiY" role="1QHqEI">
                                  <node concept="3clFbS" id="43XlAbkFBiZ" role="1bW5cS">
                                    <node concept="3cpWs8" id="43XlAbkQYQt" role="3cqZAp">
                                      <node concept="3cpWsn" id="43XlAbkQYQw" role="3cpWs9">
                                        <property role="TrG5h" value="root" />
                                        <node concept="3Tqbb2" id="43XlAbkQYQr" role="1tU5fm">
                                          <ref role="ehGHo" to="4s7a:3VMRtc4W287" resolve="CPP" />
                                        </node>
                                        <node concept="2OqwBi" id="43XlAbkQZe1" role="33vP2m">
                                          <node concept="pncrf" id="43XlAbkQZ4g" role="2Oq$k0" />
                                          <node concept="2Xjw5R" id="43XlAbkQZwL" role="2OqNvi">
                                            <node concept="1xMEDy" id="43XlAbkQZwN" role="1xVPHs">
                                              <node concept="chp4Y" id="43XlAbkQZyg" role="ri$Ld">
                                                <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkQZPO" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkR29A" role="3clFbG">
                                        <node concept="2OqwBi" id="43XlAbkR0aT" role="2Oq$k0">
                                          <node concept="37vLTw" id="43XlAbkQZPM" role="2Oq$k0">
                                            <ref role="3cqZAo" node="43XlAbkQYQw" resolve="root" />
                                          </node>
                                          <node concept="2Rf3mk" id="43XlAbkR0uj" role="2OqNvi">
                                            <node concept="1xMEDy" id="43XlAbkR0ul" role="1xVPHs">
                                              <node concept="chp4Y" id="43XlAbkR0AN" role="ri$Ld">
                                                <ref role="cht4Q" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="2es0OD" id="43XlAbkR5rx" role="2OqNvi">
                                          <node concept="1bVj0M" id="43XlAbkR5rz" role="23t8la">
                                            <node concept="3clFbS" id="43XlAbkR5r$" role="1bW5cS">
                                              <node concept="3clFbF" id="43XlAbkR5GW" role="3cqZAp">
                                                <node concept="37vLTI" id="43XlAbkR6Gj" role="3clFbG">
                                                  <node concept="3clFbT" id="43XlAbkR6SP" role="37vLTx">
                                                    <property role="3clFbU" value="false" />
                                                  </node>
                                                  <node concept="2OqwBi" id="43XlAbkR5Q$" role="37vLTJ">
                                                    <node concept="37vLTw" id="43XlAbkR5GV" role="2Oq$k0">
                                                      <ref role="3cqZAo" node="43XlAbkR5r_" resolve="it" />
                                                    </node>
                                                    <node concept="3TrcHB" id="43XlAbkR6cN" role="2OqNvi">
                                                      <ref role="3TsBF5" to="4s7a:38Muwq1QG_A" resolve="isSelected" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Rh6nW" id="43XlAbkR5r_" role="1bW2Oz">
                                              <property role="TrG5h" value="it" />
                                              <node concept="2jxLKc" id="43XlAbkR5rA" role="1tU5fm" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="3clFbF" id="43XlAbkFBjw" role="3cqZAp">
                                      <node concept="2OqwBi" id="43XlAbkFBjx" role="3clFbG">
                                        <node concept="pncrf" id="43XlAbkFBjy" role="2Oq$k0" />
                                        <node concept="1PgB_6" id="43XlAbkFBjz" role="2OqNvi" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="43XlAbkFBj$" role="ukAjM">
                                  <node concept="1Q80Hx" id="43XlAbkFBj_" role="2Oq$k0" />
                                  <node concept="liA8E" id="43XlAbkFBjA" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository():org.jetbrains.mps.openapi.module.SRepository" resolve="getRepository" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="43XlAbkFBjB" role="3cqZAp">
                <node concept="37vLTw" id="43XlAbkFBjC" role="3cqZAk">
                  <ref role="3cqZAo" node="43XlAbkFBiE" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="43XlAbkFBdm" role="3EZMnx" />
    </node>
  </node>
  <node concept="24kQdi" id="43XlAbkDHrG">
    <ref role="1XX52x" to="4s7a:43XlAbjBn0k" resolve="ExclusiveReference" />
    <node concept="1iCGBv" id="43XlAbkEDpv" role="2wV5jI">
      <ref role="1NtTu8" to="4s7a:43XlAbjBn0l" resolve="content" />
      <node concept="1sVBvm" id="43XlAbkEDpx" role="1sWHZn">
        <node concept="B$lHz" id="43XlAbkEDpC" role="2wV5jI" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5wTmjWA3CRg">
    <property role="TrG5h" value="IntentionIndicatorPanel" />
    <node concept="2tJIrI" id="5XbSbOFTac8" role="jymVt" />
    <node concept="3clFb_" id="5XbSbOFT7DE" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSideBySideEditor" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="37vLTG" id="5XbSbOFT84w" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="5XbSbOFT84x" role="1tU5fm">
          <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
        </node>
      </node>
      <node concept="37vLTG" id="5XbSbOFT8LF" role="3clF46">
        <property role="TrG5h" value="editorContext" />
        <node concept="3uibUv" id="5XbSbOFT8LG" role="1tU5fm">
          <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
        </node>
      </node>
      <node concept="37vLTG" id="5XbSbOFT9sR" role="3clF46">
        <property role="TrG5h" value="branch" />
        <node concept="3uibUv" id="5XbSbOFTe0F" role="1tU5fm">
          <ref role="3uigEE" node="5XbSbOFTd8E" resolve="IfdefBranch" />
        </node>
      </node>
      <node concept="3clFbS" id="5XbSbOFT7DH" role="3clF47">
        <node concept="3clFbJ" id="5XbSbOFTefZ" role="3cqZAp">
          <node concept="17R0WA" id="5XbSbOFTfBp" role="3clFbw">
            <node concept="Rm8GO" id="5XbSbOFTfSB" role="3uHU7w">
              <ref role="Rm8GQ" node="5XbSbOFTdc$" resolve="TRUE_BRANCH" />
              <ref role="1Px2BO" node="5XbSbOFTd8E" resolve="IfdefBranch" />
            </node>
            <node concept="37vLTw" id="5XbSbOFTe$X" role="3uHU7B">
              <ref role="3cqZAo" node="5XbSbOFT9sR" resolve="branch" />
            </node>
          </node>
          <node concept="3clFbS" id="5XbSbOFTeg1" role="3clFbx">
            <node concept="3cpWs6" id="5XbSbOFTg7G" role="3cqZAp">
              <node concept="1rXfSq" id="5XbSbOFTgBV" role="3cqZAk">
                <ref role="37wK5l" node="5XbSbOFPiJS" resolve="getPanel" />
                <node concept="37vLTw" id="5XbSbOFTh8a" role="37wK5m">
                  <ref role="3cqZAo" node="5XbSbOFT84w" resolve="node" />
                </node>
                <node concept="37vLTw" id="5XbSbOFThYm" role="37wK5m">
                  <ref role="3cqZAo" node="5XbSbOFT8LF" resolve="editorContext" />
                </node>
                <node concept="2OqwBi" id="5XbSbOFTwl1" role="37wK5m">
                  <node concept="37vLTw" id="5XbSbOFTvZD" role="2Oq$k0">
                    <ref role="3cqZAo" node="5XbSbOFT84w" resolve="node" />
                  </node>
                  <node concept="3Tsc0h" id="5XbSbOFTwXi" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:2s5q4UWqKy" resolve="trueBranch" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="5XbSbOFTrh8" role="9aQIa">
            <node concept="3clFbS" id="5XbSbOFTrh9" role="9aQI4">
              <node concept="3cpWs6" id="5XbSbOFTrOP" role="3cqZAp">
                <node concept="1rXfSq" id="5XbSbOFTskv" role="3cqZAk">
                  <ref role="37wK5l" node="5XbSbOFPiJS" resolve="getPanel" />
                  <node concept="37vLTw" id="5XbSbOFTsOn" role="37wK5m">
                    <ref role="3cqZAo" node="5XbSbOFT84w" resolve="node" />
                  </node>
                  <node concept="37vLTw" id="5XbSbOFTtEC" role="37wK5m">
                    <ref role="3cqZAo" node="5XbSbOFT8LF" resolve="editorContext" />
                  </node>
                  <node concept="2OqwBi" id="5XbSbOFTuJ3" role="37wK5m">
                    <node concept="37vLTw" id="5XbSbOFTupM" role="2Oq$k0">
                      <ref role="3cqZAo" node="5XbSbOFT84w" resolve="node" />
                    </node>
                    <node concept="3Tsc0h" id="5XbSbOFTvcN" role="2OqNvi">
                      <ref role="3TtcxE" to="4s7a:2s5q4UWqK$" resolve="elseBranch" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5XbSbOFTqHw" role="3cqZAp" />
      </node>
      <node concept="3Tm1VV" id="5XbSbOFT70n" role="1B3o_S" />
      <node concept="3uibUv" id="5XbSbOFT7Dw" role="3clF45">
        <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
      </node>
    </node>
    <node concept="2tJIrI" id="5XbSbOFT6xF" role="jymVt" />
    <node concept="3clFb_" id="5XbSbOFPiJS" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getPanel" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5XbSbOFPiJV" role="3clF47">
        <node concept="3cpWs8" id="5XbSbOFQkj0" role="3cqZAp">
          <node concept="3cpWsn" id="5XbSbOFQkj1" role="3cpWs9">
            <property role="TrG5h" value="width" />
            <node concept="10Oyi0" id="5XbSbOFQkj2" role="1tU5fm" />
            <node concept="3cmrfG" id="5XbSbOFQkj3" role="33vP2m">
              <property role="3cmrfH" value="15" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5XbSbOFV9fb" role="3cqZAp">
          <node concept="3cpWsn" id="5XbSbOFV9fe" role="3cpWs9">
            <property role="TrG5h" value="intentionWidth" />
            <node concept="10Oyi0" id="5XbSbOFV9f9" role="1tU5fm" />
            <node concept="3cmrfG" id="5XbSbOFVak2" role="33vP2m">
              <property role="3cmrfH" value="3" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5XbSbOFQkj4" role="3cqZAp">
          <node concept="3cpWsn" id="5XbSbOFQkj5" role="3cpWs9">
            <property role="TrG5h" value="maxY" />
            <node concept="10Oyi0" id="5XbSbOFQkj6" role="1tU5fm" />
            <node concept="3cmrfG" id="5XbSbOFQkj7" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5XbSbOFQkj8" role="3cqZAp" />
        <node concept="3cpWs8" id="5XbSbOFQkj9" role="3cqZAp">
          <node concept="3cpWsn" id="5XbSbOFQkja" role="3cpWs9">
            <property role="TrG5h" value="containingRoot" />
            <node concept="3Tqbb2" id="5XbSbOFQkjb" role="1tU5fm">
              <ref role="ehGHo" to="4s7a:3VMRtc4W287" resolve="CPP" />
            </node>
            <node concept="2OqwBi" id="5XbSbOFQkjc" role="33vP2m">
              <node concept="37vLTw" id="5XbSbOFQLxB" role="2Oq$k0">
                <ref role="3cqZAo" node="5XbSbOFQ$1M" resolve="node" />
              </node>
              <node concept="2Xjw5R" id="5XbSbOFQkje" role="2OqNvi">
                <node concept="1xMEDy" id="5XbSbOFQkjf" role="1xVPHs">
                  <node concept="chp4Y" id="5XbSbOFQkjg" role="ri$Ld">
                    <ref role="cht4Q" to="4s7a:3VMRtc4W287" resolve="CPP" />
                  </node>
                </node>
                <node concept="1xIGOp" id="5XbSbOFQkjh" role="1xVPHs" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5XbSbOFV4id" role="3cqZAp" />
        <node concept="3cpWs8" id="5XbSbOFQkjj" role="3cqZAp">
          <node concept="3cpWsn" id="5XbSbOFQkjk" role="3cpWs9">
            <property role="TrG5h" value="pane" />
            <node concept="3uibUv" id="5XbSbOFQkjl" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="5XbSbOFQkjm" role="33vP2m">
              <node concept="1pGfFk" id="5XbSbOFQkjn" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5XbSbOFQkjo" role="3cqZAp" />
        <node concept="3cpWs8" id="5XbSbOFQkjp" role="3cqZAp">
          <node concept="3cpWsn" id="5XbSbOFQkjq" role="3cpWs9">
            <property role="TrG5h" value="activated" />
            <node concept="10P_77" id="5XbSbOFQkjr" role="1tU5fm" />
            <node concept="3clFbT" id="5XbSbOFQkjs" role="33vP2m">
              <property role="3clFbU" value="false" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="5XbSbOFQkjt" role="3cqZAp">
          <node concept="3SKdUq" id="5XbSbOFQkju" role="3SKWNk">
            <property role="3SKdUp" value="Sort the intention nodes from outer to inner, and first to last" />
          </node>
        </node>
        <node concept="2Gpval" id="5XbSbOFQkjv" role="3cqZAp">
          <node concept="2GrKxI" id="5XbSbOFQkjw" role="2Gsz3X">
            <property role="TrG5h" value="intention" />
          </node>
          <node concept="2OqwBi" id="5XbSbOFQkjx" role="2GsD0m">
            <node concept="2OqwBi" id="5XbSbOFQkjy" role="2Oq$k0">
              <node concept="2OqwBi" id="5XbSbOFQkjz" role="2Oq$k0">
                <node concept="3TrEf2" id="5XbSbOFQkj$" role="2OqNvi">
                  <ref role="3Tt5mk" to="4s7a:3LUIgcmYqcd" resolve="intention" />
                </node>
                <node concept="37vLTw" id="5XbSbOFQkj_" role="2Oq$k0">
                  <ref role="3cqZAo" node="5XbSbOFQkja" resolve="containingRoot" />
                </node>
              </node>
              <node concept="3Tsc0h" id="5XbSbOFQkjA" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
              </node>
            </node>
            <node concept="2DpFxk" id="5XbSbOFQkjB" role="2OqNvi">
              <node concept="1bVj0M" id="5XbSbOFQkjC" role="23t8la">
                <node concept="3clFbS" id="5XbSbOFQkjD" role="1bW5cS">
                  <node concept="3clFbJ" id="5XbSbOFQkjE" role="3cqZAp">
                    <node concept="3clFbC" id="5XbSbOFQkjF" role="3clFbw">
                      <node concept="2OqwBi" id="5XbSbOFQkjG" role="3uHU7w">
                        <node concept="2OqwBi" id="5XbSbOFQkjH" role="2Oq$k0">
                          <node concept="37vLTw" id="5XbSbOFQkjI" role="2Oq$k0">
                            <ref role="3cqZAo" node="5XbSbOFQkk_" resolve="b" />
                          </node>
                          <node concept="3TrEf2" id="5XbSbOFQkjJ" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                        <node concept="1mfA1w" id="5XbSbOFQkjK" role="2OqNvi" />
                      </node>
                      <node concept="2OqwBi" id="5XbSbOFQkjL" role="3uHU7B">
                        <node concept="2OqwBi" id="5XbSbOFQkjM" role="2Oq$k0">
                          <node concept="37vLTw" id="5XbSbOFQkjN" role="2Oq$k0">
                            <ref role="3cqZAo" node="5XbSbOFQkkz" resolve="a" />
                          </node>
                          <node concept="3TrEf2" id="5XbSbOFQkjO" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                        <node concept="1mfA1w" id="5XbSbOFQkjP" role="2OqNvi" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="5XbSbOFQkjQ" role="3clFbx">
                      <node concept="3cpWs6" id="5XbSbOFQkjR" role="3cqZAp">
                        <node concept="2YIFZM" id="5XbSbOFQkjS" role="3cqZAk">
                          <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                          <ref role="37wK5l" to="wyt6:~Integer.compare(int,int):int" resolve="compare" />
                          <node concept="2OqwBi" id="5XbSbOFQkjT" role="37wK5m">
                            <node concept="2OqwBi" id="5XbSbOFQkjU" role="2Oq$k0">
                              <node concept="37vLTw" id="5XbSbOFQkjV" role="2Oq$k0">
                                <ref role="3cqZAo" node="5XbSbOFQkkz" resolve="a" />
                              </node>
                              <node concept="3TrEf2" id="5XbSbOFQkjW" role="2OqNvi">
                                <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                              </node>
                            </node>
                            <node concept="2bSWHS" id="5XbSbOFQkjX" role="2OqNvi" />
                          </node>
                          <node concept="2OqwBi" id="5XbSbOFQkjY" role="37wK5m">
                            <node concept="2OqwBi" id="5XbSbOFQkjZ" role="2Oq$k0">
                              <node concept="37vLTw" id="5XbSbOFQkk0" role="2Oq$k0">
                                <ref role="3cqZAo" node="5XbSbOFQkk_" resolve="b" />
                              </node>
                              <node concept="3TrEf2" id="5XbSbOFQkk1" role="2OqNvi">
                                <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                              </node>
                            </node>
                            <node concept="2bSWHS" id="5XbSbOFQkk2" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3eNFk2" id="5XbSbOFQkk3" role="3eNLev">
                      <node concept="3clFbS" id="5XbSbOFQkk4" role="3eOfB_">
                        <node concept="3cpWs6" id="5XbSbOFQkk5" role="3cqZAp">
                          <node concept="3cmrfG" id="5XbSbOFQkk6" role="3cqZAk">
                            <property role="3cmrfH" value="1" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="5XbSbOFQkk7" role="3eO9$A">
                        <node concept="2OqwBi" id="5XbSbOFQkk8" role="2Oq$k0">
                          <node concept="2OqwBi" id="5XbSbOFQkk9" role="2Oq$k0">
                            <node concept="37vLTw" id="5XbSbOFQkka" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOFQkkz" resolve="a" />
                            </node>
                            <node concept="3TrEf2" id="5XbSbOFQkkb" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                          <node concept="z$bX8" id="5XbSbOFQkkc" role="2OqNvi" />
                        </node>
                        <node concept="3JPx81" id="5XbSbOFQkkd" role="2OqNvi">
                          <node concept="2OqwBi" id="5XbSbOFQkke" role="25WWJ7">
                            <node concept="37vLTw" id="5XbSbOFQkkf" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOFQkk_" resolve="b" />
                            </node>
                            <node concept="3TrEf2" id="5XbSbOFQkkg" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3eNFk2" id="5XbSbOFQkkh" role="3eNLev">
                      <node concept="2OqwBi" id="5XbSbOFQkki" role="3eO9$A">
                        <node concept="2OqwBi" id="5XbSbOFQkkj" role="2Oq$k0">
                          <node concept="2OqwBi" id="5XbSbOFQkkk" role="2Oq$k0">
                            <node concept="37vLTw" id="5XbSbOFQkkl" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOFQkk_" resolve="b" />
                            </node>
                            <node concept="3TrEf2" id="5XbSbOFQkkm" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                          <node concept="z$bX8" id="5XbSbOFQkkn" role="2OqNvi" />
                        </node>
                        <node concept="3JPx81" id="5XbSbOFQkko" role="2OqNvi">
                          <node concept="2OqwBi" id="5XbSbOFQkkp" role="25WWJ7">
                            <node concept="37vLTw" id="5XbSbOFQkkq" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOFQkkz" resolve="a" />
                            </node>
                            <node concept="3TrEf2" id="5XbSbOFQkkr" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="5XbSbOFQkks" role="3eOfB_">
                        <node concept="3cpWs6" id="5XbSbOFQkkt" role="3cqZAp">
                          <node concept="3cmrfG" id="5XbSbOFQkku" role="3cqZAk">
                            <property role="3cmrfH" value="-1" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="9aQIb" id="5XbSbOFQkkv" role="9aQIa">
                      <node concept="3clFbS" id="5XbSbOFQkkw" role="9aQI4">
                        <node concept="3cpWs6" id="5XbSbOFQkkx" role="3cqZAp">
                          <node concept="3cmrfG" id="5XbSbOFQkky" role="3cqZAk">
                            <property role="3cmrfH" value="0" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="5XbSbOFQkkz" role="1bW2Oz">
                  <property role="TrG5h" value="a" />
                  <node concept="2jxLKc" id="5XbSbOFQkk$" role="1tU5fm" />
                </node>
                <node concept="Rh6nW" id="5XbSbOFQkk_" role="1bW2Oz">
                  <property role="TrG5h" value="b" />
                  <node concept="2jxLKc" id="5XbSbOFQkkA" role="1tU5fm" />
                </node>
              </node>
              <node concept="1nlBCl" id="5XbSbOFQkkB" role="2Dq5b$">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5XbSbOFQkkC" role="2LFqv$">
            <node concept="3cpWs8" id="5XbSbOFQkkD" role="3cqZAp">
              <node concept="3cpWsn" id="5XbSbOFQkkE" role="3cpWs9">
                <property role="TrG5h" value="containedInBranch" />
                <node concept="10P_77" id="5XbSbOFQkkF" role="1tU5fm" />
                <node concept="3clFbT" id="5XbSbOFQkkG" role="33vP2m">
                  <property role="3clFbU" value="false" />
                </node>
              </node>
            </node>
            <node concept="2Gpval" id="5XbSbOFQkkH" role="3cqZAp">
              <node concept="2GrKxI" id="5XbSbOFQkkI" role="2Gsz3X">
                <property role="TrG5h" value="branchNode" />
              </node>
              <node concept="37vLTw" id="5XbSbOFTm7D" role="2GsD0m">
                <ref role="3cqZAo" node="5XbSbOFTiki" resolve="branchContents" />
              </node>
              <node concept="3clFbS" id="5XbSbOFQkkM" role="2LFqv$">
                <node concept="3clFbF" id="5XbSbOFQkkN" role="3cqZAp">
                  <node concept="3vZ8r8" id="5XbSbOFQkkO" role="3clFbG">
                    <node concept="37vLTw" id="5XbSbOFQkkP" role="37vLTJ">
                      <ref role="3cqZAo" node="5XbSbOFQkkE" resolve="containedInBranch" />
                    </node>
                    <node concept="1eOMI4" id="5XbSbOFQkkQ" role="37vLTx">
                      <node concept="22lmx$" id="5XbSbOFQkkR" role="1eOMHV">
                        <node concept="17R0WA" id="5XbSbOFQkkS" role="3uHU7w">
                          <node concept="2GrUjf" id="5XbSbOFQkkT" role="3uHU7w">
                            <ref role="2Gs0qQ" node="5XbSbOFQkkI" resolve="branchNode" />
                          </node>
                          <node concept="2OqwBi" id="5XbSbOFQkkU" role="3uHU7B">
                            <node concept="2GrUjf" id="5XbSbOFQkkV" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="5XbSbOFQkjw" resolve="intention" />
                            </node>
                            <node concept="3TrEf2" id="5XbSbOFQkkW" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="5XbSbOFQkkX" role="3uHU7B">
                          <node concept="2OqwBi" id="5XbSbOFQkkY" role="2Oq$k0">
                            <node concept="2OqwBi" id="5XbSbOFQkkZ" role="2Oq$k0">
                              <node concept="2GrUjf" id="5XbSbOFQkl0" role="2Oq$k0">
                                <ref role="2Gs0qQ" node="5XbSbOFQkjw" resolve="intention" />
                              </node>
                              <node concept="3TrEf2" id="5XbSbOFQkl1" role="2OqNvi">
                                <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                              </node>
                            </node>
                            <node concept="z$bX8" id="5XbSbOFQkl2" role="2OqNvi" />
                          </node>
                          <node concept="3JPx81" id="5XbSbOFQkl3" role="2OqNvi">
                            <node concept="2GrUjf" id="5XbSbOFQkl4" role="25WWJ7">
                              <ref role="2Gs0qQ" node="5XbSbOFQkkI" resolve="branchNode" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="5XbSbOFQkl5" role="3cqZAp" />
            <node concept="3cpWs8" id="5XbSbOFZ$5B" role="3cqZAp">
              <node concept="3cpWsn" id="5XbSbOFZ$5C" role="3cpWs9">
                <property role="TrG5h" value="intentionType" />
                <node concept="3uibUv" id="5XbSbOFZ$4j" role="1tU5fm">
                  <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
                <node concept="2YIFZM" id="5XbSbOFZ$5D" role="33vP2m">
                  <ref role="37wK5l" node="5XbSbOFVHhB" resolve="getIntention" />
                  <ref role="1Pybhc" node="5XbSbOFV6mO" resolve="IntentionType" />
                  <node concept="2OqwBi" id="5XbSbOFZ$5E" role="37wK5m">
                    <node concept="2GrUjf" id="5XbSbOFZCT8" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="5XbSbOFQkjw" resolve="intention" />
                    </node>
                    <node concept="3TrcHB" id="5XbSbOFZ$5G" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="5XbSbOFQkl6" role="3cqZAp" />
            <node concept="3clFbJ" id="5XbSbOFQkl7" role="3cqZAp">
              <node concept="3clFbS" id="5XbSbOFQkl8" role="3clFbx">
                <node concept="3cpWs8" id="5XbSbOFQkl9" role="3cqZAp">
                  <node concept="3cpWsn" id="5XbSbOFQkla" role="3cpWs9">
                    <property role="TrG5h" value="panel" />
                    <node concept="3uibUv" id="5XbSbOFQklb" role="1tU5fm">
                      <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
                    </node>
                    <node concept="2ShNRf" id="5XbSbOFQklc" role="33vP2m">
                      <node concept="1pGfFk" id="5XbSbOFQkld" role="2ShVmc">
                        <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="5XbSbOFQkle" role="3cqZAp">
                  <node concept="2OqwBi" id="5XbSbOFQklf" role="3clFbG">
                    <node concept="37vLTw" id="5XbSbOFQklg" role="2Oq$k0">
                      <ref role="3cqZAo" node="5XbSbOFQkla" resolve="panel" />
                    </node>
                    <node concept="liA8E" id="5XbSbOFQklh" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
                      <node concept="2ShNRf" id="5XbSbOFQkli" role="37wK5m">
                        <node concept="1pGfFk" id="5XbSbOFQklj" role="2ShVmc">
                          <ref role="37wK5l" to="dxuu:~BoxLayout.&lt;init&gt;(java.awt.Container,int)" resolve="BoxLayout" />
                          <node concept="37vLTw" id="5XbSbOFQklk" role="37wK5m">
                            <ref role="3cqZAo" node="5XbSbOFQkla" resolve="panel" />
                          </node>
                          <node concept="10M0yZ" id="5XbSbOFQkll" role="37wK5m">
                            <ref role="3cqZAo" to="dxuu:~BoxLayout.PAGE_AXIS" resolve="PAGE_AXIS" />
                            <ref role="1PxDUh" to="dxuu:~BoxLayout" resolve="BoxLayout" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="5XbSbOFQklm" role="3cqZAp" />
                <node concept="3cpWs8" id="5XbSbOFQkln" role="3cqZAp">
                  <node concept="3cpWsn" id="5XbSbOFQklo" role="3cpWs9">
                    <property role="TrG5h" value="panelstart" />
                    <node concept="10Oyi0" id="5XbSbOFQklp" role="1tU5fm" />
                    <node concept="3cmrfG" id="5XbSbOFQklq" role="33vP2m">
                      <property role="3cmrfH" value="0" />
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="5XbSbOFQklr" role="3cqZAp">
                  <node concept="3cpWsn" id="5XbSbOFQkls" role="3cpWs9">
                    <property role="TrG5h" value="panelend" />
                    <node concept="10Oyi0" id="5XbSbOFQklt" role="1tU5fm" />
                    <node concept="3cmrfG" id="5XbSbOFQklu" role="33vP2m">
                      <property role="3cmrfH" value="0" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="5XbSbOFQklv" role="3cqZAp" />
                <node concept="3cpWs8" id="5XbSbOFQklw" role="3cqZAp">
                  <node concept="3cpWsn" id="5XbSbOFQklx" role="3cpWs9">
                    <property role="TrG5h" value="editorcell" />
                    <node concept="3uibUv" id="5XbSbOFQkly" role="1tU5fm">
                      <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                    </node>
                    <node concept="2OqwBi" id="5XbSbOFQklz" role="33vP2m">
                      <node concept="2OqwBi" id="5XbSbOFQkl$" role="2Oq$k0">
                        <node concept="37vLTw" id="5XbSbOFQMeq" role="2Oq$k0">
                          <ref role="3cqZAo" node="5XbSbOFQ$Ld" resolve="editorContext" />
                        </node>
                        <node concept="liA8E" id="5XbSbOFQklA" role="2OqNvi">
                          <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                        </node>
                      </node>
                      <node concept="liA8E" id="5XbSbOFQklB" role="2OqNvi">
                        <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                        <node concept="2OqwBi" id="5XbSbOFQklC" role="37wK5m">
                          <node concept="2GrUjf" id="5XbSbOFQklD" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="5XbSbOFQkjw" resolve="intention" />
                          </node>
                          <node concept="3TrEf2" id="5XbSbOFQklE" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="5XbSbOFQklF" role="3cqZAp" />
                <node concept="3clFbJ" id="5XbSbOFQklG" role="3cqZAp">
                  <node concept="3clFbS" id="5XbSbOFQklH" role="3clFbx">
                    <node concept="1X3_iC" id="76MUimx2$L1" role="lGtFl">
                      <property role="3V$3am" value="statement" />
                      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                      <node concept="3cpWs8" id="76MUimx2yRY" role="8Wnug">
                        <node concept="3cpWsn" id="76MUimx2yS1" role="3cpWs9">
                          <property role="TrG5h" value="start" />
                          <node concept="10Oyi0" id="76MUimx2yS2" role="1tU5fm" />
                          <node concept="3cpWsd" id="76MUimx2yS3" role="33vP2m">
                            <node concept="2OqwBi" id="76MUimx2yS4" role="3uHU7w">
                              <node concept="2OqwBi" id="76MUimx2yS5" role="2Oq$k0">
                                <node concept="2OqwBi" id="76MUimx2yS6" role="2Oq$k0">
                                  <node concept="37vLTw" id="76MUimx2yS7" role="2Oq$k0">
                                    <ref role="3cqZAo" node="5XbSbOFQ$Ld" resolve="editorContext" />
                                  </node>
                                  <node concept="liA8E" id="76MUimx2yS8" role="2OqNvi">
                                    <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                                  </node>
                                </node>
                                <node concept="liA8E" id="76MUimx2yS9" role="2OqNvi">
                                  <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                                  <node concept="37vLTw" id="76MUimx2ySa" role="37wK5m">
                                    <ref role="3cqZAo" node="5XbSbOFQ$1M" resolve="node" />
                                  </node>
                                </node>
                              </node>
                              <node concept="liA8E" id="76MUimx2ySb" role="2OqNvi">
                                <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="76MUimx2ySc" role="3uHU7B">
                              <node concept="37vLTw" id="76MUimx2ySd" role="2Oq$k0">
                                <ref role="3cqZAo" node="5XbSbOFQklx" resolve="editorcell" />
                              </node>
                              <node concept="liA8E" id="76MUimx2ySe" role="2OqNvi">
                                <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3cpWs8" id="1Jdu5E3cOwM" role="3cqZAp">
                      <node concept="3cpWsn" id="1Jdu5E3cOwN" role="3cpWs9">
                        <property role="TrG5h" value="nodeCell" />
                        <node concept="3uibUv" id="1Jdu5E3cOwO" role="1tU5fm">
                          <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                        </node>
                        <node concept="2OqwBi" id="1Jdu5E3cP0F" role="33vP2m">
                          <node concept="2OqwBi" id="1Jdu5E3cP0G" role="2Oq$k0">
                            <node concept="37vLTw" id="1Jdu5E3cP0H" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOFQ$Ld" resolve="editorContext" />
                            </node>
                            <node concept="liA8E" id="1Jdu5E3cP0I" role="2OqNvi">
                              <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                            </node>
                          </node>
                          <node concept="liA8E" id="1Jdu5E3cP0J" role="2OqNvi">
                            <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                            <node concept="37vLTw" id="1Jdu5E3cP0K" role="37wK5m">
                              <ref role="3cqZAo" node="5XbSbOFQ$1M" resolve="node" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="1Jdu5E3cQ2l" role="3cqZAp" />
                    <node concept="3clFbJ" id="1Jdu5E3cR01" role="3cqZAp">
                      <node concept="3clFbS" id="1Jdu5E3cR03" role="3clFbx">
                        <node concept="3cpWs8" id="5XbSbOFQklI" role="3cqZAp">
                          <node concept="3cpWsn" id="5XbSbOFQklJ" role="3cpWs9">
                            <property role="TrG5h" value="start" />
                            <node concept="10Oyi0" id="5XbSbOFQklK" role="1tU5fm" />
                            <node concept="3cpWsd" id="76MUimx9XKD" role="33vP2m">
                              <node concept="3cmrfG" id="76MUimxa2Cy" role="3uHU7w">
                                <property role="3cmrfH" value="16" />
                              </node>
                              <node concept="3cpWsd" id="5XbSbOFQklL" role="3uHU7B">
                                <node concept="2OqwBi" id="5XbSbOFQklU" role="3uHU7B">
                                  <node concept="37vLTw" id="5XbSbOFQklV" role="2Oq$k0">
                                    <ref role="3cqZAo" node="5XbSbOFQklx" resolve="editorcell" />
                                  </node>
                                  <node concept="liA8E" id="5XbSbOFQklW" role="2OqNvi">
                                    <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="5XbSbOFQklM" role="3uHU7w">
                                  <node concept="37vLTw" id="1Jdu5E3cPyR" role="2Oq$k0">
                                    <ref role="3cqZAo" node="1Jdu5E3cOwN" resolve="nodeCell" />
                                  </node>
                                  <node concept="liA8E" id="5XbSbOFQklT" role="2OqNvi">
                                    <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3cpWs8" id="5XbSbOFQklX" role="3cqZAp">
                          <node concept="3cpWsn" id="5XbSbOFQklY" role="3cpWs9">
                            <property role="TrG5h" value="end" />
                            <node concept="10Oyi0" id="5XbSbOFQklZ" role="1tU5fm" />
                            <node concept="3cpWs3" id="5XbSbOFQkm0" role="33vP2m">
                              <node concept="37vLTw" id="5XbSbOFQkm1" role="3uHU7B">
                                <ref role="3cqZAo" node="5XbSbOFQklJ" resolve="start" />
                              </node>
                              <node concept="2OqwBi" id="5XbSbOFQkm2" role="3uHU7w">
                                <node concept="37vLTw" id="5XbSbOFQkm3" role="2Oq$k0">
                                  <ref role="3cqZAo" node="5XbSbOFQklx" resolve="editorcell" />
                                </node>
                                <node concept="liA8E" id="5XbSbOFQkm4" role="2OqNvi">
                                  <ref role="37wK5l" to="f4zo:~EditorCell.getHeight():int" resolve="getHeight" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbH" id="5XbSbOFQkm5" role="3cqZAp" />
                        <node concept="3clFbF" id="5XbSbOFQkm6" role="3cqZAp">
                          <node concept="37vLTI" id="5XbSbOFQkm7" role="3clFbG">
                            <node concept="37vLTw" id="5XbSbOFQkm8" role="37vLTx">
                              <ref role="3cqZAo" node="5XbSbOFQklJ" resolve="start" />
                            </node>
                            <node concept="37vLTw" id="5XbSbOFQkm9" role="37vLTJ">
                              <ref role="3cqZAo" node="5XbSbOFQklo" resolve="panelstart" />
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbF" id="5XbSbOFQkma" role="3cqZAp">
                          <node concept="37vLTI" id="5XbSbOFQkmb" role="3clFbG">
                            <node concept="37vLTw" id="5XbSbOFQkmc" role="37vLTx">
                              <ref role="3cqZAo" node="5XbSbOFQklY" resolve="end" />
                            </node>
                            <node concept="37vLTw" id="5XbSbOFQkmd" role="37vLTJ">
                              <ref role="3cqZAo" node="5XbSbOFQkls" resolve="panelend" />
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbH" id="5XbSbOFQkme" role="3cqZAp" />
                        <node concept="3SKdUt" id="5XbSbOFQkmf" role="3cqZAp">
                          <node concept="3SKdUq" id="5XbSbOFQkmg" role="3SKWNk">
                            <property role="3SKdUp" value="Intention display" />
                          </node>
                        </node>
                        <node concept="3clFbF" id="5XbSbOFQkmh" role="3cqZAp">
                          <node concept="2OqwBi" id="5XbSbOFQkmi" role="3clFbG">
                            <node concept="37vLTw" id="5XbSbOFQkmj" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOFQkla" resolve="panel" />
                            </node>
                            <node concept="liA8E" id="5XbSbOFQkmk" role="2OqNvi">
                              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
                              <node concept="2ShNRf" id="5XbSbOFQkml" role="37wK5m">
                                <node concept="1pGfFk" id="5XbSbOFQkmm" role="2ShVmc">
                                  <ref role="37wK5l" node="3LUIgcneH88" resolve="IntentionIndicatorGraphic" />
                                  <node concept="37vLTw" id="5XbSbOFZwmy" role="37wK5m">
                                    <ref role="3cqZAo" node="5XbSbOFV9fe" resolve="intentionWidth" />
                                  </node>
                                  <node concept="3cpWsd" id="5XbSbOFQkmn" role="37wK5m">
                                    <node concept="37vLTw" id="5XbSbOFQkmo" role="3uHU7w">
                                      <ref role="3cqZAo" node="5XbSbOFQklJ" resolve="start" />
                                    </node>
                                    <node concept="37vLTw" id="5XbSbOFQkmp" role="3uHU7B">
                                      <ref role="3cqZAo" node="5XbSbOFQklY" resolve="end" />
                                    </node>
                                  </node>
                                  <node concept="37vLTw" id="5XbSbOFZHWd" role="37wK5m">
                                    <ref role="3cqZAo" node="5XbSbOFZ$5C" resolve="intentionType" />
                                  </node>
                                  <node concept="2OqwBi" id="5XbSbOFQkmq" role="37wK5m">
                                    <node concept="2GrUjf" id="5XbSbOFQkmr" role="2Oq$k0">
                                      <ref role="2Gs0qQ" node="5XbSbOFQkjw" resolve="intention" />
                                    </node>
                                    <node concept="3TrcHB" id="5XbSbOFQkms" role="2OqNvi">
                                      <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="5XbSbOFQkmt" role="37wK5m">
                                    <node concept="2GrUjf" id="5XbSbOFQkmu" role="2Oq$k0">
                                      <ref role="2Gs0qQ" node="5XbSbOFQkjw" resolve="intention" />
                                    </node>
                                    <node concept="3TrcHB" id="5XbSbOFQkmv" role="2OqNvi">
                                      <ref role="3TsBF5" to="4s7a:3jeRE17nVF2" resolve="featurename" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3y3z36" id="1Jdu5E3cRKe" role="3clFbw">
                        <node concept="10Nm6u" id="1Jdu5E3cRRh" role="3uHU7w" />
                        <node concept="37vLTw" id="1Jdu5E3cRyI" role="3uHU7B">
                          <ref role="3cqZAo" node="1Jdu5E3cOwN" resolve="nodeCell" />
                        </node>
                      </node>
                      <node concept="9aQIb" id="1Jdu5E3cS0c" role="9aQIa">
                        <node concept="3clFbS" id="1Jdu5E3cS0d" role="9aQI4">
                          <node concept="34ab3g" id="1Jdu5E3cSd7" role="3cqZAp">
                            <property role="35gtTG" value="warn" />
                            <node concept="3cpWs3" id="1Jdu5E3cSd8" role="34bqiv">
                              <node concept="2OqwBi" id="1Jdu5E3cSd9" role="3uHU7w">
                                <node concept="2OqwBi" id="1Jdu5E3cSda" role="2Oq$k0">
                                  <node concept="37vLTw" id="1Jdu5E3cVvy" role="2Oq$k0">
                                    <ref role="3cqZAo" node="5XbSbOFQ$1M" resolve="node" />
                                  </node>
                                  <node concept="2yIwOk" id="1Jdu5E3cSde" role="2OqNvi" />
                                </node>
                                <node concept="liA8E" id="1Jdu5E3cSdf" role="2OqNvi">
                                  <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                                </node>
                              </node>
                              <node concept="Xl_RD" id="1Jdu5E3cSdg" role="3uHU7B">
                                <property role="Xl_RC" value="IntentionContainer_Editor: Could not find node cell for node " />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3y3z36" id="5XbSbOFQkmy" role="3clFbw">
                    <node concept="10Nm6u" id="5XbSbOFQkmz" role="3uHU7w" />
                    <node concept="37vLTw" id="5XbSbOFQkm$" role="3uHU7B">
                      <ref role="3cqZAo" node="5XbSbOFQklx" resolve="editorcell" />
                    </node>
                  </node>
                  <node concept="9aQIb" id="5XbSbOFQkm_" role="9aQIa">
                    <node concept="3clFbS" id="5XbSbOFQkmA" role="9aQI4">
                      <node concept="34ab3g" id="5XbSbOFQkmB" role="3cqZAp">
                        <property role="35gtTG" value="warn" />
                        <node concept="3cpWs3" id="5XbSbOFQkmC" role="34bqiv">
                          <node concept="2OqwBi" id="5XbSbOFQkmD" role="3uHU7w">
                            <node concept="2OqwBi" id="5XbSbOFQkmE" role="2Oq$k0">
                              <node concept="2OqwBi" id="5XbSbOFQkmF" role="2Oq$k0">
                                <node concept="2GrUjf" id="5XbSbOFQkmG" role="2Oq$k0">
                                  <ref role="2Gs0qQ" node="5XbSbOFQkjw" resolve="intention" />
                                </node>
                                <node concept="3TrEf2" id="5XbSbOFQkmH" role="2OqNvi">
                                  <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                                </node>
                              </node>
                              <node concept="2yIwOk" id="5XbSbOFQkmI" role="2OqNvi" />
                            </node>
                            <node concept="liA8E" id="5XbSbOFQkmJ" role="2OqNvi">
                              <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                            </node>
                          </node>
                          <node concept="Xl_RD" id="5XbSbOFQkmK" role="3uHU7B">
                            <property role="Xl_RC" value="IntentionContainer_Editor: Could not find editor cell for node " />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="5XbSbOFQkmL" role="3cqZAp" />
                <node concept="3clFbF" id="5XbSbOFQkmM" role="3cqZAp">
                  <node concept="2OqwBi" id="5XbSbOFQkmN" role="3clFbG">
                    <node concept="37vLTw" id="5XbSbOFQkmO" role="2Oq$k0">
                      <ref role="3cqZAo" node="5XbSbOFQkla" resolve="panel" />
                    </node>
                    <node concept="liA8E" id="5XbSbOFQkmP" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~Component.setSize(int,int):void" resolve="setSize" />
                      <node concept="37vLTw" id="5XbSbOFVaGj" role="37wK5m">
                        <ref role="3cqZAo" node="5XbSbOFV9fe" resolve="intentionWidth" />
                      </node>
                      <node concept="3cpWsd" id="5XbSbOFQkmR" role="37wK5m">
                        <node concept="37vLTw" id="5XbSbOFQkmS" role="3uHU7w">
                          <ref role="3cqZAo" node="5XbSbOFQklo" resolve="panelstart" />
                        </node>
                        <node concept="37vLTw" id="5XbSbOFQkmT" role="3uHU7B">
                          <ref role="3cqZAo" node="5XbSbOFQkls" resolve="panelend" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3SKdUt" id="5XbSbOFQkmU" role="3cqZAp">
                  <node concept="3SKdUq" id="5XbSbOFQkmV" role="3SKWNk">
                    <property role="3SKdUp" value="Offset the indicator on x" />
                  </node>
                </node>
                <node concept="3clFbF" id="5XbSbOFQkmW" role="3cqZAp">
                  <node concept="2OqwBi" id="5XbSbOFQkmX" role="3clFbG">
                    <node concept="37vLTw" id="5XbSbOFQkmY" role="2Oq$k0">
                      <ref role="3cqZAo" node="5XbSbOFQkla" resolve="panel" />
                    </node>
                    <node concept="liA8E" id="5XbSbOFQkmZ" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~Component.setLocation(int,int):void" resolve="setLocation" />
                      <node concept="3cpWs3" id="5XbSbOG2Wbk" role="37wK5m">
                        <node concept="3cmrfG" id="5XbSbOG2ZSt" role="3uHU7B">
                          <property role="3cmrfH" value="1" />
                        </node>
                        <node concept="17qRlL" id="5XbSbOFVQtj" role="3uHU7w">
                          <node concept="37vLTw" id="5XbSbOFVVD2" role="3uHU7w">
                            <ref role="3cqZAo" node="5XbSbOFV9fe" resolve="intentionWidth" />
                          </node>
                          <node concept="2OqwBi" id="5XbSbOFVKim" role="3uHU7B">
                            <node concept="37vLTw" id="5XbSbOFZGZg" role="2Oq$k0">
                              <ref role="3cqZAo" node="5XbSbOFZ$5C" resolve="intentionType" />
                            </node>
                            <node concept="liA8E" id="5XbSbOFVLwd" role="2OqNvi">
                              <ref role="37wK5l" to="wyt6:~Enum.ordinal():int" resolve="ordinal" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="37vLTw" id="5XbSbOFQkn1" role="37wK5m">
                        <ref role="3cqZAo" node="5XbSbOFQklo" resolve="panelstart" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="5XbSbOFQkn2" role="3cqZAp">
                  <node concept="2OqwBi" id="5XbSbOFQkn3" role="3clFbG">
                    <node concept="37vLTw" id="5XbSbOFQkn4" role="2Oq$k0">
                      <ref role="3cqZAo" node="5XbSbOFQkla" resolve="panel" />
                    </node>
                    <node concept="liA8E" id="5XbSbOFQkn5" role="2OqNvi">
                      <ref role="37wK5l" to="dxuu:~JComponent.setToolTipText(java.lang.String):void" resolve="setToolTipText" />
                      <node concept="Xl_RD" id="5XbSbOFQkn6" role="37wK5m" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="5XbSbOFQkn7" role="3cqZAp">
                  <node concept="2OqwBi" id="5XbSbOFQkn8" role="3clFbG">
                    <node concept="37vLTw" id="5XbSbOFQkn9" role="2Oq$k0">
                      <ref role="3cqZAo" node="5XbSbOFQkjk" resolve="pane" />
                    </node>
                    <node concept="liA8E" id="5XbSbOFQkna" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
                      <node concept="37vLTw" id="5XbSbOFQknb" role="37wK5m">
                        <ref role="3cqZAo" node="5XbSbOFQkla" resolve="panel" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="5XbSbOFQknc" role="3cqZAp">
                  <node concept="37vLTI" id="5XbSbOFQknd" role="3clFbG">
                    <node concept="3clFbT" id="5XbSbOFQkne" role="37vLTx">
                      <property role="3clFbU" value="true" />
                    </node>
                    <node concept="37vLTw" id="5XbSbOFQknf" role="37vLTJ">
                      <ref role="3cqZAo" node="5XbSbOFQkjq" resolve="activated" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="5XbSbOFQkng" role="3cqZAp" />
                <node concept="3clFbJ" id="5XbSbOFQknh" role="3cqZAp">
                  <node concept="3clFbS" id="5XbSbOFQkni" role="3clFbx">
                    <node concept="3clFbF" id="5XbSbOFQknj" role="3cqZAp">
                      <node concept="37vLTI" id="5XbSbOFQknk" role="3clFbG">
                        <node concept="37vLTw" id="5XbSbOFQknl" role="37vLTx">
                          <ref role="3cqZAo" node="5XbSbOFQkls" resolve="panelend" />
                        </node>
                        <node concept="37vLTw" id="5XbSbOFQknm" role="37vLTJ">
                          <ref role="3cqZAo" node="5XbSbOFQkj5" resolve="maxY" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3eOSWO" id="5XbSbOFQknn" role="3clFbw">
                    <node concept="37vLTw" id="5XbSbOFQkno" role="3uHU7w">
                      <ref role="3cqZAo" node="5XbSbOFQkj5" resolve="maxY" />
                    </node>
                    <node concept="37vLTw" id="5XbSbOFQknp" role="3uHU7B">
                      <ref role="3cqZAo" node="5XbSbOFQkls" resolve="panelend" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5XbSbOFQknq" role="3clFbw">
                <ref role="3cqZAo" node="5XbSbOFQkkE" resolve="containedInBranch" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5XbSbOFQknt" role="3cqZAp" />
        <node concept="3clFbF" id="5XbSbOFQknu" role="3cqZAp">
          <node concept="2OqwBi" id="5XbSbOFQknv" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOFQknw" role="2Oq$k0">
              <ref role="3cqZAo" node="5XbSbOFQkjk" resolve="pane" />
            </node>
            <node concept="liA8E" id="5XbSbOFQknx" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="10Nm6u" id="5XbSbOFQkny" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5XbSbOFQknz" role="3cqZAp">
          <node concept="3cpWsn" id="5XbSbOFQkn$" role="3cpWs9">
            <property role="TrG5h" value="myWidth" />
            <node concept="10Oyi0" id="5XbSbOFQkn_" role="1tU5fm" />
            <node concept="3cmrfG" id="5XbSbOFQknA" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5XbSbOFQknB" role="3cqZAp">
          <node concept="3clFbS" id="5XbSbOFQknC" role="3clFbx">
            <node concept="3clFbF" id="5XbSbOFQknD" role="3cqZAp">
              <node concept="37vLTI" id="5XbSbOFQknE" role="3clFbG">
                <node concept="3cpWs3" id="5XbSbOFQknF" role="37vLTx">
                  <node concept="3cmrfG" id="5XbSbOFQknG" role="3uHU7w">
                    <property role="3cmrfH" value="2" />
                  </node>
                  <node concept="37vLTw" id="5XbSbOFQknH" role="3uHU7B">
                    <ref role="3cqZAo" node="5XbSbOFQkj1" resolve="width" />
                  </node>
                </node>
                <node concept="37vLTw" id="5XbSbOFQknI" role="37vLTJ">
                  <ref role="3cqZAo" node="5XbSbOFQkn$" resolve="myWidth" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="5XbSbOFQknJ" role="3clFbw">
            <ref role="3cqZAo" node="5XbSbOFQkjq" resolve="activated" />
          </node>
        </node>
        <node concept="3clFbF" id="5XbSbOFQknK" role="3cqZAp">
          <node concept="2OqwBi" id="5XbSbOFQknL" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOFQknM" role="2Oq$k0">
              <ref role="3cqZAo" node="5XbSbOFQkjk" resolve="pane" />
            </node>
            <node concept="liA8E" id="5XbSbOFQknN" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setPreferredSize(java.awt.Dimension):void" resolve="setPreferredSize" />
              <node concept="2ShNRf" id="5XbSbOFQknO" role="37wK5m">
                <node concept="1pGfFk" id="5XbSbOFQknP" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                  <node concept="37vLTw" id="5XbSbOFQknQ" role="37wK5m">
                    <ref role="3cqZAo" node="5XbSbOFQkn$" resolve="myWidth" />
                  </node>
                  <node concept="37vLTw" id="5XbSbOFQknR" role="37wK5m">
                    <ref role="3cqZAo" node="5XbSbOFQkj5" resolve="maxY" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5XbSbOFQknS" role="3cqZAp">
          <node concept="2OqwBi" id="5XbSbOFQknT" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOFQknU" role="2Oq$k0">
              <ref role="3cqZAo" node="5XbSbOFQkjk" resolve="pane" />
            </node>
            <node concept="liA8E" id="5XbSbOFQknV" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Component.setSize(int,int):void" resolve="setSize" />
              <node concept="37vLTw" id="5XbSbOFQknW" role="37wK5m">
                <ref role="3cqZAo" node="5XbSbOFQkn$" resolve="myWidth" />
              </node>
              <node concept="37vLTw" id="5XbSbOFQknX" role="37wK5m">
                <ref role="3cqZAo" node="5XbSbOFQkj5" resolve="maxY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5XbSbOFQknY" role="3cqZAp">
          <node concept="2OqwBi" id="5XbSbOFQknZ" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOFQko0" role="2Oq$k0">
              <ref role="3cqZAo" node="5XbSbOFQkjk" resolve="pane" />
            </node>
            <node concept="liA8E" id="5XbSbOFQko1" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Component.setBounds(int,int,int,int):void" resolve="setBounds" />
              <node concept="3cmrfG" id="5XbSbOFQko2" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3cmrfG" id="5XbSbOFQko3" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="5XbSbOFQko4" role="37wK5m">
                <ref role="3cqZAo" node="5XbSbOFQkn$" resolve="myWidth" />
              </node>
              <node concept="37vLTw" id="5XbSbOFQko5" role="37wK5m">
                <ref role="3cqZAo" node="5XbSbOFQkj5" resolve="maxY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5XbSbOG4vGa" role="3cqZAp">
          <node concept="2OqwBi" id="5XbSbOG4wYS" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOG4vG8" role="2Oq$k0">
              <ref role="3cqZAo" node="5XbSbOFQkjk" resolve="pane" />
            </node>
            <node concept="liA8E" id="5XbSbOG4z9$" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setOpaque(boolean):void" resolve="setOpaque" />
              <node concept="3clFbT" id="5XbSbOG63JF" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5XbSbOFQko7" role="3cqZAp">
          <node concept="37vLTw" id="5XbSbOFQko8" role="3cqZAk">
            <ref role="3cqZAo" node="5XbSbOFQkjk" resolve="pane" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5XbSbOFPaZO" role="1B3o_S" />
      <node concept="3uibUv" id="5XbSbOFPiJK" role="3clF45">
        <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
      </node>
      <node concept="37vLTG" id="5XbSbOFQ$1M" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="5XbSbOFR0Ky" role="1tU5fm">
          <ref role="ehGHo" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
        </node>
      </node>
      <node concept="37vLTG" id="5XbSbOFQ$Ld" role="3clF46">
        <property role="TrG5h" value="editorContext" />
        <node concept="3uibUv" id="5XbSbOFQLmm" role="1tU5fm">
          <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
        </node>
      </node>
      <node concept="37vLTG" id="5XbSbOFTiki" role="3clF46">
        <property role="TrG5h" value="branchContents" />
        <node concept="2I9FWS" id="5XbSbOFTjdd" role="1tU5fm">
          <ref role="2I9WkF" to="4s7a:2s5q4UUgwq" resolve="ICPPElement" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="50KMJClb27I" role="jymVt" />
    <node concept="3clFb_" id="50KMJClb6Rw" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getFullIndicator" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="37vLTG" id="50KMJClbm6i" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="50KMJClbm6j" role="1tU5fm">
          <ref role="ehGHo" to="4s7a:3LUIgcnlyz5" resolve="IntentionContainer" />
        </node>
      </node>
      <node concept="37vLTG" id="50KMJClbnq3" role="3clF46">
        <property role="TrG5h" value="editorContext" />
        <node concept="3uibUv" id="50KMJClbnq4" role="1tU5fm">
          <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
        </node>
      </node>
      <node concept="3clFbS" id="50KMJClb6Rz" role="3clF47">
        <node concept="3cpWs8" id="50KMJClb7bU" role="3cqZAp">
          <node concept="3cpWsn" id="50KMJClb7bV" role="3cpWs9">
            <property role="TrG5h" value="width" />
            <node concept="10Oyi0" id="50KMJClb7bW" role="1tU5fm" />
            <node concept="3cmrfG" id="50KMJClb7bX" role="33vP2m">
              <property role="3cmrfH" value="30" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="50KMJClb7bY" role="3cqZAp">
          <node concept="3cpWsn" id="50KMJClb7bZ" role="3cpWs9">
            <property role="TrG5h" value="intentionWidth" />
            <node concept="10Oyi0" id="50KMJClb7c0" role="1tU5fm" />
            <node concept="3cmrfG" id="50KMJClb7c1" role="33vP2m">
              <property role="3cmrfH" value="6" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="50KMJClb7c2" role="3cqZAp">
          <node concept="3cpWsn" id="50KMJClb7c3" role="3cpWs9">
            <property role="TrG5h" value="maxY" />
            <node concept="10Oyi0" id="50KMJClb7c4" role="1tU5fm" />
            <node concept="3cmrfG" id="50KMJClb7c5" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="50KMJClb7cg" role="3cqZAp" />
        <node concept="3cpWs8" id="50KMJClb7ch" role="3cqZAp">
          <node concept="3cpWsn" id="50KMJClb7ci" role="3cpWs9">
            <property role="TrG5h" value="pane" />
            <node concept="3uibUv" id="50KMJClb7cj" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="50KMJClb7ck" role="33vP2m">
              <node concept="1pGfFk" id="50KMJClb7cl" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="50KMJClb7cm" role="3cqZAp" />
        <node concept="3SKdUt" id="50KMJClb7cr" role="3cqZAp">
          <node concept="3SKdUq" id="50KMJClb7cs" role="3SKWNk">
            <property role="3SKdUp" value="Sort the intention nodes from outer to inner, and first to last" />
          </node>
        </node>
        <node concept="2Gpval" id="50KMJClb7ct" role="3cqZAp">
          <node concept="2GrKxI" id="50KMJClb7cu" role="2Gsz3X">
            <property role="TrG5h" value="intention" />
          </node>
          <node concept="2OqwBi" id="50KMJClb7cv" role="2GsD0m">
            <node concept="2OqwBi" id="50KMJClb7cw" role="2Oq$k0">
              <node concept="37vLTw" id="50KMJClbCt1" role="2Oq$k0">
                <ref role="3cqZAo" node="50KMJClbm6i" resolve="node" />
              </node>
              <node concept="3Tsc0h" id="50KMJClb7c$" role="2OqNvi">
                <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
              </node>
            </node>
            <node concept="2DpFxk" id="50KMJClb7c_" role="2OqNvi">
              <node concept="1bVj0M" id="50KMJClb7cA" role="23t8la">
                <node concept="3clFbS" id="50KMJClb7cB" role="1bW5cS">
                  <node concept="3clFbJ" id="50KMJClb7cC" role="3cqZAp">
                    <node concept="3clFbC" id="50KMJClb7cD" role="3clFbw">
                      <node concept="2OqwBi" id="50KMJClb7cE" role="3uHU7w">
                        <node concept="2OqwBi" id="50KMJClb7cF" role="2Oq$k0">
                          <node concept="37vLTw" id="50KMJClb7cG" role="2Oq$k0">
                            <ref role="3cqZAo" node="50KMJClb7dz" resolve="b" />
                          </node>
                          <node concept="3TrEf2" id="50KMJClb7cH" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                        <node concept="1mfA1w" id="50KMJClb7cI" role="2OqNvi" />
                      </node>
                      <node concept="2OqwBi" id="50KMJClb7cJ" role="3uHU7B">
                        <node concept="2OqwBi" id="50KMJClb7cK" role="2Oq$k0">
                          <node concept="37vLTw" id="50KMJClb7cL" role="2Oq$k0">
                            <ref role="3cqZAo" node="50KMJClb7dx" resolve="a" />
                          </node>
                          <node concept="3TrEf2" id="50KMJClb7cM" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                        <node concept="1mfA1w" id="50KMJClb7cN" role="2OqNvi" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="50KMJClb7cO" role="3clFbx">
                      <node concept="3cpWs6" id="50KMJClb7cP" role="3cqZAp">
                        <node concept="2YIFZM" id="50KMJClb7cQ" role="3cqZAk">
                          <ref role="37wK5l" to="wyt6:~Integer.compare(int,int):int" resolve="compare" />
                          <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                          <node concept="2OqwBi" id="50KMJClb7cR" role="37wK5m">
                            <node concept="2OqwBi" id="50KMJClb7cS" role="2Oq$k0">
                              <node concept="37vLTw" id="50KMJClb7cT" role="2Oq$k0">
                                <ref role="3cqZAo" node="50KMJClb7dx" resolve="a" />
                              </node>
                              <node concept="3TrEf2" id="50KMJClb7cU" role="2OqNvi">
                                <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                              </node>
                            </node>
                            <node concept="2bSWHS" id="50KMJClb7cV" role="2OqNvi" />
                          </node>
                          <node concept="2OqwBi" id="50KMJClb7cW" role="37wK5m">
                            <node concept="2OqwBi" id="50KMJClb7cX" role="2Oq$k0">
                              <node concept="37vLTw" id="50KMJClb7cY" role="2Oq$k0">
                                <ref role="3cqZAo" node="50KMJClb7dz" resolve="b" />
                              </node>
                              <node concept="3TrEf2" id="50KMJClb7cZ" role="2OqNvi">
                                <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                              </node>
                            </node>
                            <node concept="2bSWHS" id="50KMJClb7d0" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3eNFk2" id="50KMJClb7d1" role="3eNLev">
                      <node concept="3clFbS" id="50KMJClb7d2" role="3eOfB_">
                        <node concept="3cpWs6" id="50KMJClb7d3" role="3cqZAp">
                          <node concept="3cmrfG" id="50KMJClb7d4" role="3cqZAk">
                            <property role="3cmrfH" value="1" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="50KMJClb7d5" role="3eO9$A">
                        <node concept="2OqwBi" id="50KMJClb7d6" role="2Oq$k0">
                          <node concept="2OqwBi" id="50KMJClb7d7" role="2Oq$k0">
                            <node concept="37vLTw" id="50KMJClb7d8" role="2Oq$k0">
                              <ref role="3cqZAo" node="50KMJClb7dx" resolve="a" />
                            </node>
                            <node concept="3TrEf2" id="50KMJClb7d9" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                          <node concept="z$bX8" id="50KMJClb7da" role="2OqNvi" />
                        </node>
                        <node concept="3JPx81" id="50KMJClb7db" role="2OqNvi">
                          <node concept="2OqwBi" id="50KMJClb7dc" role="25WWJ7">
                            <node concept="37vLTw" id="50KMJClb7dd" role="2Oq$k0">
                              <ref role="3cqZAo" node="50KMJClb7dz" resolve="b" />
                            </node>
                            <node concept="3TrEf2" id="50KMJClb7de" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3eNFk2" id="50KMJClb7df" role="3eNLev">
                      <node concept="2OqwBi" id="50KMJClb7dg" role="3eO9$A">
                        <node concept="2OqwBi" id="50KMJClb7dh" role="2Oq$k0">
                          <node concept="2OqwBi" id="50KMJClb7di" role="2Oq$k0">
                            <node concept="37vLTw" id="50KMJClb7dj" role="2Oq$k0">
                              <ref role="3cqZAo" node="50KMJClb7dz" resolve="b" />
                            </node>
                            <node concept="3TrEf2" id="50KMJClb7dk" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                          <node concept="z$bX8" id="50KMJClb7dl" role="2OqNvi" />
                        </node>
                        <node concept="3JPx81" id="50KMJClb7dm" role="2OqNvi">
                          <node concept="2OqwBi" id="50KMJClb7dn" role="25WWJ7">
                            <node concept="37vLTw" id="50KMJClb7do" role="2Oq$k0">
                              <ref role="3cqZAo" node="50KMJClb7dx" resolve="a" />
                            </node>
                            <node concept="3TrEf2" id="50KMJClb7dp" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="50KMJClb7dq" role="3eOfB_">
                        <node concept="3cpWs6" id="50KMJClb7dr" role="3cqZAp">
                          <node concept="3cmrfG" id="50KMJClb7ds" role="3cqZAk">
                            <property role="3cmrfH" value="-1" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="9aQIb" id="50KMJClb7dt" role="9aQIa">
                      <node concept="3clFbS" id="50KMJClb7du" role="9aQI4">
                        <node concept="3cpWs6" id="50KMJClb7dv" role="3cqZAp">
                          <node concept="3cmrfG" id="50KMJClb7dw" role="3cqZAk">
                            <property role="3cmrfH" value="0" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="50KMJClb7dx" role="1bW2Oz">
                  <property role="TrG5h" value="a" />
                  <node concept="2jxLKc" id="50KMJClb7dy" role="1tU5fm" />
                </node>
                <node concept="Rh6nW" id="50KMJClb7dz" role="1bW2Oz">
                  <property role="TrG5h" value="b" />
                  <node concept="2jxLKc" id="50KMJClb7d$" role="1tU5fm" />
                </node>
              </node>
              <node concept="1nlBCl" id="50KMJClb7d_" role="2Dq5b$">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="50KMJClb7dA" role="2LFqv$">
            <node concept="3cpWs8" id="50KMJClfR6O" role="3cqZAp">
              <node concept="3cpWsn" id="50KMJClfR6P" role="3cpWs9">
                <property role="TrG5h" value="intentionInstance" />
                <node concept="3uibUv" id="50KMJClfR6Q" role="1tU5fm">
                  <ref role="3uigEE" node="50KMJClf7Sg" resolve="IntentionInstance" />
                </node>
                <node concept="2ShNRf" id="50KMJClfRO9" role="33vP2m">
                  <node concept="1pGfFk" id="50KMJClfYSV" role="2ShVmc">
                    <ref role="37wK5l" node="50KMJClf8hc" resolve="IntentionInstance" />
                    <node concept="2GrUjf" id="50KMJClfZDJ" role="37wK5m">
                      <ref role="2Gs0qQ" node="50KMJClb7cu" resolve="intention" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="50KMJClfYUf" role="3cqZAp" />
            <node concept="3cpWs8" id="50KMJClb7ec" role="3cqZAp">
              <node concept="3cpWsn" id="50KMJClb7ed" role="3cpWs9">
                <property role="TrG5h" value="panel" />
                <node concept="3uibUv" id="50KMJClb7ee" role="1tU5fm">
                  <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
                </node>
                <node concept="2ShNRf" id="50KMJClb7ef" role="33vP2m">
                  <node concept="1pGfFk" id="50KMJClb7eg" role="2ShVmc">
                    <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="50KMJClb7eh" role="3cqZAp">
              <node concept="2OqwBi" id="50KMJClb7ei" role="3clFbG">
                <node concept="37vLTw" id="50KMJClb7ej" role="2Oq$k0">
                  <ref role="3cqZAo" node="50KMJClb7ed" resolve="panel" />
                </node>
                <node concept="liA8E" id="50KMJClb7ek" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
                  <node concept="2ShNRf" id="50KMJClb7el" role="37wK5m">
                    <node concept="1pGfFk" id="50KMJClb7em" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~BoxLayout.&lt;init&gt;(java.awt.Container,int)" resolve="BoxLayout" />
                      <node concept="37vLTw" id="50KMJClb7en" role="37wK5m">
                        <ref role="3cqZAo" node="50KMJClb7ed" resolve="panel" />
                      </node>
                      <node concept="10M0yZ" id="50KMJClb7eo" role="37wK5m">
                        <ref role="1PxDUh" to="dxuu:~BoxLayout" resolve="BoxLayout" />
                        <ref role="3cqZAo" to="dxuu:~BoxLayout.PAGE_AXIS" resolve="PAGE_AXIS" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="50KMJClb7ep" role="3cqZAp" />
            <node concept="3cpWs8" id="50KMJClb7eq" role="3cqZAp">
              <node concept="3cpWsn" id="50KMJClb7er" role="3cpWs9">
                <property role="TrG5h" value="panelstart" />
                <node concept="10Oyi0" id="50KMJClb7es" role="1tU5fm" />
                <node concept="3cmrfG" id="50KMJClb7et" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="50KMJClb7eu" role="3cqZAp">
              <node concept="3cpWsn" id="50KMJClb7ev" role="3cpWs9">
                <property role="TrG5h" value="panelend" />
                <node concept="10Oyi0" id="50KMJClb7ew" role="1tU5fm" />
                <node concept="3cmrfG" id="50KMJClb7ex" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="50KMJClb7ey" role="3cqZAp" />
            <node concept="3cpWs8" id="50KMJClb7ez" role="3cqZAp">
              <node concept="3cpWsn" id="50KMJClb7e$" role="3cpWs9">
                <property role="TrG5h" value="editorcell" />
                <node concept="3uibUv" id="50KMJClb7e_" role="1tU5fm">
                  <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                </node>
                <node concept="2OqwBi" id="50KMJClb7eA" role="33vP2m">
                  <node concept="2OqwBi" id="50KMJClb7eB" role="2Oq$k0">
                    <node concept="37vLTw" id="50KMJClbpL3" role="2Oq$k0">
                      <ref role="3cqZAo" node="50KMJClbnq3" resolve="editorContext" />
                    </node>
                    <node concept="liA8E" id="50KMJClb7eD" role="2OqNvi">
                      <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                    </node>
                  </node>
                  <node concept="liA8E" id="50KMJClb7eE" role="2OqNvi">
                    <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                    <node concept="2OqwBi" id="50KMJClb7eF" role="37wK5m">
                      <node concept="2GrUjf" id="50KMJClb7eG" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="50KMJClb7cu" resolve="intention" />
                      </node>
                      <node concept="3TrEf2" id="50KMJClb7eH" role="2OqNvi">
                        <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="50KMJClb7eI" role="3cqZAp" />
            <node concept="3clFbJ" id="50KMJClb7eJ" role="3cqZAp">
              <node concept="3clFbS" id="50KMJClb7eK" role="3clFbx">
                <node concept="1X3_iC" id="50KMJClb7eL" role="lGtFl">
                  <property role="3V$3am" value="statement" />
                  <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                  <node concept="3cpWs8" id="50KMJClb7eM" role="8Wnug">
                    <node concept="3cpWsn" id="50KMJClb7eN" role="3cpWs9">
                      <property role="TrG5h" value="start" />
                      <node concept="10Oyi0" id="50KMJClb7eO" role="1tU5fm" />
                      <node concept="3cpWsd" id="50KMJClb7eP" role="33vP2m">
                        <node concept="2OqwBi" id="50KMJClb7eQ" role="3uHU7w">
                          <node concept="2OqwBi" id="50KMJClb7eR" role="2Oq$k0">
                            <node concept="2OqwBi" id="50KMJClb7eS" role="2Oq$k0">
                              <node concept="37vLTw" id="50KMJClb7eT" role="2Oq$k0">
                                <ref role="3cqZAo" node="5XbSbOFQ$Ld" resolve="editorContext" />
                              </node>
                              <node concept="liA8E" id="50KMJClb7eU" role="2OqNvi">
                                <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                              </node>
                            </node>
                            <node concept="liA8E" id="50KMJClb7eV" role="2OqNvi">
                              <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                              <node concept="37vLTw" id="50KMJClb7eW" role="37wK5m">
                                <ref role="3cqZAo" node="5XbSbOFQ$1M" resolve="node" />
                              </node>
                            </node>
                          </node>
                          <node concept="liA8E" id="50KMJClb7eX" role="2OqNvi">
                            <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="50KMJClb7eY" role="3uHU7B">
                          <node concept="37vLTw" id="50KMJClb7eZ" role="2Oq$k0">
                            <ref role="3cqZAo" node="50KMJClb7e$" resolve="editorcell" />
                          </node>
                          <node concept="liA8E" id="50KMJClb7f0" role="2OqNvi">
                            <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="1Jdu5E3jsmI" role="3cqZAp">
                  <node concept="3cpWsn" id="1Jdu5E3jsmJ" role="3cpWs9">
                    <property role="TrG5h" value="nodeCell" />
                    <node concept="3uibUv" id="1Jdu5E3jsmK" role="1tU5fm">
                      <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                    </node>
                    <node concept="2OqwBi" id="1Jdu5E3jsON" role="33vP2m">
                      <node concept="2OqwBi" id="1Jdu5E3jsOO" role="2Oq$k0">
                        <node concept="37vLTw" id="1Jdu5E3jsOP" role="2Oq$k0">
                          <ref role="3cqZAo" node="50KMJClbnq3" resolve="editorContext" />
                        </node>
                        <node concept="liA8E" id="1Jdu5E3jsOQ" role="2OqNvi">
                          <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                        </node>
                      </node>
                      <node concept="liA8E" id="1Jdu5E3jsOR" role="2OqNvi">
                        <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                        <node concept="37vLTw" id="1Jdu5E3jsOS" role="37wK5m">
                          <ref role="3cqZAo" node="50KMJClbm6i" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="1Jdu5E3rtF3" role="3cqZAp" />
                <node concept="3cpWs8" id="1Jdu5E3ruGQ" role="3cqZAp">
                  <node concept="3cpWsn" id="1Jdu5E3ruGT" role="3cpWs9">
                    <property role="TrG5h" value="start" />
                    <node concept="10Oyi0" id="1Jdu5E3ruGO" role="1tU5fm" />
                    <node concept="2OqwBi" id="1Jdu5E3tBOj" role="33vP2m">
                      <node concept="37vLTw" id="1Jdu5E3tBOk" role="2Oq$k0">
                        <ref role="3cqZAo" node="50KMJClb7e$" resolve="editorcell" />
                      </node>
                      <node concept="liA8E" id="1Jdu5E3tBOl" role="2OqNvi">
                        <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="1Jdu5E3jubt" role="3cqZAp">
                  <node concept="3clFbS" id="1Jdu5E3jubv" role="3clFbx">
                    <node concept="3clFbF" id="1Jdu5E3rvKq" role="3cqZAp">
                      <node concept="37vLTI" id="1Jdu5E3rxwb" role="3clFbG">
                        <node concept="37vLTw" id="1Jdu5E3rvKo" role="37vLTJ">
                          <ref role="3cqZAo" node="1Jdu5E3ruGT" resolve="start" />
                        </node>
                        <node concept="3cpWsd" id="50KMJClb7f6" role="37vLTx">
                          <node concept="2OqwBi" id="50KMJClb7f7" role="3uHU7B">
                            <node concept="37vLTw" id="50KMJClb7f8" role="2Oq$k0">
                              <ref role="3cqZAo" node="50KMJClb7e$" resolve="editorcell" />
                            </node>
                            <node concept="liA8E" id="50KMJClb7f9" role="2OqNvi">
                              <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="50KMJClb7fa" role="3uHU7w">
                            <node concept="37vLTw" id="1Jdu5E3jtgM" role="2Oq$k0">
                              <ref role="3cqZAo" node="1Jdu5E3jsmJ" resolve="nodeCell" />
                            </node>
                            <node concept="liA8E" id="50KMJClb7fh" role="2OqNvi">
                              <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3y3z36" id="1Jdu5E3juTu" role="3clFbw">
                    <node concept="10Nm6u" id="1Jdu5E3jv0i" role="3uHU7w" />
                    <node concept="37vLTw" id="1Jdu5E3juGd" role="3uHU7B">
                      <ref role="3cqZAo" node="1Jdu5E3jsmJ" resolve="nodeCell" />
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="50KMJClb7fi" role="3cqZAp">
                  <node concept="3cpWsn" id="50KMJClb7fj" role="3cpWs9">
                    <property role="TrG5h" value="end" />
                    <node concept="10Oyi0" id="50KMJClb7fk" role="1tU5fm" />
                    <node concept="3cpWs3" id="50KMJClb7fl" role="33vP2m">
                      <node concept="37vLTw" id="1Jdu5E3rzTK" role="3uHU7B">
                        <ref role="3cqZAo" node="1Jdu5E3ruGT" resolve="start" />
                      </node>
                      <node concept="2OqwBi" id="50KMJClb7fn" role="3uHU7w">
                        <node concept="37vLTw" id="50KMJClb7fo" role="2Oq$k0">
                          <ref role="3cqZAo" node="50KMJClb7e$" resolve="editorcell" />
                        </node>
                        <node concept="liA8E" id="50KMJClb7fp" role="2OqNvi">
                          <ref role="37wK5l" to="f4zo:~EditorCell.getHeight():int" resolve="getHeight" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="50KMJClb7fq" role="3cqZAp" />
                <node concept="3clFbF" id="50KMJClb7fr" role="3cqZAp">
                  <node concept="37vLTI" id="50KMJClb7fs" role="3clFbG">
                    <node concept="37vLTw" id="1Jdu5E3r$ke" role="37vLTx">
                      <ref role="3cqZAo" node="1Jdu5E3ruGT" resolve="start" />
                    </node>
                    <node concept="37vLTw" id="50KMJClb7fu" role="37vLTJ">
                      <ref role="3cqZAo" node="50KMJClb7er" resolve="panelstart" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="50KMJClb7fv" role="3cqZAp">
                  <node concept="37vLTI" id="50KMJClb7fw" role="3clFbG">
                    <node concept="37vLTw" id="50KMJClb7fx" role="37vLTx">
                      <ref role="3cqZAo" node="50KMJClb7fj" resolve="end" />
                    </node>
                    <node concept="37vLTw" id="50KMJClb7fy" role="37vLTJ">
                      <ref role="3cqZAo" node="50KMJClb7ev" resolve="panelend" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="50KMJClb7fz" role="3cqZAp" />
                <node concept="3SKdUt" id="50KMJClb7f$" role="3cqZAp">
                  <node concept="3SKdUq" id="50KMJClb7f_" role="3SKWNk">
                    <property role="3SKdUp" value="Intention display" />
                  </node>
                </node>
                <node concept="3clFbF" id="50KMJClb7fA" role="3cqZAp">
                  <node concept="2OqwBi" id="50KMJClb7fB" role="3clFbG">
                    <node concept="37vLTw" id="50KMJClb7fC" role="2Oq$k0">
                      <ref role="3cqZAo" node="50KMJClb7ed" resolve="panel" />
                    </node>
                    <node concept="liA8E" id="50KMJClb7fD" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
                      <node concept="2ShNRf" id="50KMJClb7fE" role="37wK5m">
                        <node concept="1pGfFk" id="50KMJClb7fF" role="2ShVmc">
                          <ref role="37wK5l" node="3LUIgcneH88" resolve="IntentionIndicatorGraphic" />
                          <node concept="37vLTw" id="50KMJClb7fG" role="37wK5m">
                            <ref role="3cqZAo" node="50KMJClb7bZ" resolve="intentionWidth" />
                          </node>
                          <node concept="3cpWsd" id="50KMJClb7fH" role="37wK5m">
                            <node concept="37vLTw" id="1Jdu5E3r$zz" role="3uHU7w">
                              <ref role="3cqZAo" node="1Jdu5E3ruGT" resolve="start" />
                            </node>
                            <node concept="37vLTw" id="50KMJClb7fJ" role="3uHU7B">
                              <ref role="3cqZAo" node="50KMJClb7fj" resolve="end" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="50KMJClg1N3" role="37wK5m">
                            <node concept="37vLTw" id="50KMJClg1rF" role="2Oq$k0">
                              <ref role="3cqZAo" node="50KMJClfR6P" resolve="intentionInstance" />
                            </node>
                            <node concept="liA8E" id="50KMJClg2fk" role="2OqNvi">
                              <ref role="37wK5l" node="50KMJClfZJT" resolve="getIntentionType" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="50KMJClb7fL" role="37wK5m">
                            <node concept="2GrUjf" id="50KMJClb7fM" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="50KMJClb7cu" resolve="intention" />
                            </node>
                            <node concept="3TrcHB" id="50KMJClb7fN" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="50KMJClb7fO" role="37wK5m">
                            <node concept="2GrUjf" id="50KMJClb7fP" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="50KMJClb7cu" resolve="intention" />
                            </node>
                            <node concept="3TrcHB" id="50KMJClb7fQ" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:3jeRE17nVF2" resolve="featurename" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="50KMJClb7fS" role="3cqZAp" />
              </node>
              <node concept="3y3z36" id="50KMJClb7fT" role="3clFbw">
                <node concept="10Nm6u" id="50KMJClb7fU" role="3uHU7w" />
                <node concept="37vLTw" id="50KMJClb7fV" role="3uHU7B">
                  <ref role="3cqZAo" node="50KMJClb7e$" resolve="editorcell" />
                </node>
              </node>
              <node concept="9aQIb" id="50KMJClb7fW" role="9aQIa">
                <node concept="3clFbS" id="50KMJClb7fX" role="9aQI4">
                  <node concept="34ab3g" id="50KMJClb7fY" role="3cqZAp">
                    <property role="35gtTG" value="warn" />
                    <node concept="3cpWs3" id="50KMJClb7fZ" role="34bqiv">
                      <node concept="2OqwBi" id="50KMJClb7g0" role="3uHU7w">
                        <node concept="2OqwBi" id="50KMJClb7g1" role="2Oq$k0">
                          <node concept="2OqwBi" id="50KMJClb7g2" role="2Oq$k0">
                            <node concept="2GrUjf" id="50KMJClb7g3" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="50KMJClb7cu" resolve="intention" />
                            </node>
                            <node concept="3TrEf2" id="50KMJClb7g4" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                          <node concept="2yIwOk" id="50KMJClb7g5" role="2OqNvi" />
                        </node>
                        <node concept="liA8E" id="50KMJClb7g6" role="2OqNvi">
                          <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                        </node>
                      </node>
                      <node concept="Xl_RD" id="50KMJClb7g7" role="3uHU7B">
                        <property role="Xl_RC" value="IntentionContainer_Editor: Could not find editor cell for node " />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="50KMJClb7g8" role="3cqZAp" />
            <node concept="3clFbF" id="50KMJClb7g9" role="3cqZAp">
              <node concept="2OqwBi" id="50KMJClb7ga" role="3clFbG">
                <node concept="37vLTw" id="50KMJClb7gb" role="2Oq$k0">
                  <ref role="3cqZAo" node="50KMJClb7ed" resolve="panel" />
                </node>
                <node concept="liA8E" id="50KMJClb7gc" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Component.setSize(int,int):void" resolve="setSize" />
                  <node concept="37vLTw" id="50KMJClb7gd" role="37wK5m">
                    <ref role="3cqZAo" node="50KMJClb7bZ" resolve="intentionWidth" />
                  </node>
                  <node concept="3cpWsd" id="50KMJClb7ge" role="37wK5m">
                    <node concept="37vLTw" id="50KMJClb7gf" role="3uHU7w">
                      <ref role="3cqZAo" node="50KMJClb7er" resolve="panelstart" />
                    </node>
                    <node concept="37vLTw" id="50KMJClb7gg" role="3uHU7B">
                      <ref role="3cqZAo" node="50KMJClb7ev" resolve="panelend" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="50KMJClb7gh" role="3cqZAp">
              <node concept="3SKdUq" id="50KMJClb7gi" role="3SKWNk">
                <property role="3SKdUp" value="Offset the indicator on x" />
              </node>
            </node>
            <node concept="3clFbF" id="50KMJClb7gj" role="3cqZAp">
              <node concept="2OqwBi" id="50KMJClb7gk" role="3clFbG">
                <node concept="37vLTw" id="50KMJClb7gl" role="2Oq$k0">
                  <ref role="3cqZAo" node="50KMJClb7ed" resolve="panel" />
                </node>
                <node concept="liA8E" id="50KMJClb7gm" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Component.setLocation(int,int):void" resolve="setLocation" />
                  <node concept="3cpWs3" id="50KMJClb7gn" role="37wK5m">
                    <node concept="3cmrfG" id="50KMJClb7go" role="3uHU7B">
                      <property role="3cmrfH" value="1" />
                    </node>
                    <node concept="17qRlL" id="50KMJClb7gp" role="3uHU7w">
                      <node concept="37vLTw" id="50KMJClb7gq" role="3uHU7w">
                        <ref role="3cqZAo" node="50KMJClb7bZ" resolve="intentionWidth" />
                      </node>
                      <node concept="2OqwBi" id="50KMJClb7gr" role="3uHU7B">
                        <node concept="liA8E" id="50KMJClb7gt" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~Enum.ordinal():int" resolve="ordinal" />
                        </node>
                        <node concept="2OqwBi" id="50KMJClg3Ck" role="2Oq$k0">
                          <node concept="37vLTw" id="50KMJClg3Cl" role="2Oq$k0">
                            <ref role="3cqZAo" node="50KMJClfR6P" resolve="intentionInstance" />
                          </node>
                          <node concept="liA8E" id="50KMJClg3Cm" role="2OqNvi">
                            <ref role="37wK5l" node="50KMJClfZJT" resolve="getIntentionType" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTw" id="50KMJClb7gu" role="37wK5m">
                    <ref role="3cqZAo" node="50KMJClb7er" resolve="panelstart" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="50KMJClb7gv" role="3cqZAp">
              <node concept="2OqwBi" id="50KMJClb7gw" role="3clFbG">
                <node concept="37vLTw" id="50KMJClb7gx" role="2Oq$k0">
                  <ref role="3cqZAo" node="50KMJClb7ed" resolve="panel" />
                </node>
                <node concept="liA8E" id="50KMJClb7gy" role="2OqNvi">
                  <ref role="37wK5l" to="dxuu:~JComponent.setToolTipText(java.lang.String):void" resolve="setToolTipText" />
                  <node concept="2OqwBi" id="50KMJClg5Dq" role="37wK5m">
                    <node concept="37vLTw" id="50KMJClg5rA" role="2Oq$k0">
                      <ref role="3cqZAo" node="50KMJClfR6P" resolve="intentionInstance" />
                    </node>
                    <node concept="liA8E" id="50KMJClg5TH" role="2OqNvi">
                      <ref role="37wK5l" node="50KMJClfdlu" resolve="getTooltip" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="50KMJClb7g$" role="3cqZAp">
              <node concept="2OqwBi" id="50KMJClb7g_" role="3clFbG">
                <node concept="37vLTw" id="50KMJClb7gA" role="2Oq$k0">
                  <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
                </node>
                <node concept="liA8E" id="50KMJClb7gB" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
                  <node concept="37vLTw" id="50KMJClb7gC" role="37wK5m">
                    <ref role="3cqZAo" node="50KMJClb7ed" resolve="panel" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="50KMJClb7gH" role="3cqZAp" />
            <node concept="3clFbJ" id="50KMJClb7gI" role="3cqZAp">
              <node concept="3clFbS" id="50KMJClb7gJ" role="3clFbx">
                <node concept="3clFbF" id="50KMJClb7gK" role="3cqZAp">
                  <node concept="37vLTI" id="50KMJClb7gL" role="3clFbG">
                    <node concept="37vLTw" id="50KMJClb7gM" role="37vLTx">
                      <ref role="3cqZAo" node="50KMJClb7ev" resolve="panelend" />
                    </node>
                    <node concept="37vLTw" id="50KMJClb7gN" role="37vLTJ">
                      <ref role="3cqZAo" node="50KMJClb7c3" resolve="maxY" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="50KMJClb7gO" role="3clFbw">
                <node concept="37vLTw" id="50KMJClb7gP" role="3uHU7w">
                  <ref role="3cqZAo" node="50KMJClb7c3" resolve="maxY" />
                </node>
                <node concept="37vLTw" id="50KMJClb7gQ" role="3uHU7B">
                  <ref role="3cqZAo" node="50KMJClb7ev" resolve="panelend" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="50KMJClb7h4" role="3cqZAp" />
        <node concept="3clFbF" id="50KMJClb7h5" role="3cqZAp">
          <node concept="2OqwBi" id="50KMJClb7h6" role="3clFbG">
            <node concept="37vLTw" id="50KMJClb7h7" role="2Oq$k0">
              <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
            </node>
            <node concept="liA8E" id="50KMJClb7h8" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="10Nm6u" id="50KMJClb7h9" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="50KMJClmUKM" role="3cqZAp">
          <node concept="3cpWsn" id="50KMJClmUKP" role="3cpWs9">
            <property role="TrG5h" value="myWidth" />
            <node concept="10Oyi0" id="50KMJClmUKK" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="50KMJClb7hg" role="3cqZAp">
          <node concept="37vLTI" id="50KMJClb7hh" role="3clFbG">
            <node concept="3cpWs3" id="50KMJClb7hi" role="37vLTx">
              <node concept="3cmrfG" id="50KMJClb7hj" role="3uHU7w">
                <property role="3cmrfH" value="2" />
              </node>
              <node concept="37vLTw" id="50KMJClb7hk" role="3uHU7B">
                <ref role="3cqZAo" node="50KMJClb7bV" resolve="width" />
              </node>
            </node>
            <node concept="37vLTw" id="50KMJClmVFB" role="37vLTJ">
              <ref role="3cqZAo" node="50KMJClmUKP" resolve="myWidth" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="50KMJClb7hn" role="3cqZAp">
          <node concept="2OqwBi" id="50KMJClb7ho" role="3clFbG">
            <node concept="37vLTw" id="50KMJClb7hp" role="2Oq$k0">
              <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
            </node>
            <node concept="liA8E" id="50KMJClb7hq" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setPreferredSize(java.awt.Dimension):void" resolve="setPreferredSize" />
              <node concept="2ShNRf" id="50KMJClb7hr" role="37wK5m">
                <node concept="1pGfFk" id="50KMJClb7hs" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                  <node concept="37vLTw" id="50KMJClmW04" role="37wK5m">
                    <ref role="3cqZAo" node="50KMJClmUKP" resolve="myWidth" />
                  </node>
                  <node concept="37vLTw" id="50KMJClb7hu" role="37wK5m">
                    <ref role="3cqZAo" node="50KMJClb7c3" resolve="maxY" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="50KMJClb7hv" role="3cqZAp">
          <node concept="2OqwBi" id="50KMJClb7hw" role="3clFbG">
            <node concept="37vLTw" id="50KMJClb7hx" role="2Oq$k0">
              <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
            </node>
            <node concept="liA8E" id="50KMJClb7hy" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Component.setSize(int,int):void" resolve="setSize" />
              <node concept="37vLTw" id="50KMJClmWbY" role="37wK5m">
                <ref role="3cqZAo" node="50KMJClmUKP" resolve="myWidth" />
              </node>
              <node concept="37vLTw" id="50KMJClb7h$" role="37wK5m">
                <ref role="3cqZAo" node="50KMJClb7c3" resolve="maxY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="50KMJClb7h_" role="3cqZAp">
          <node concept="2OqwBi" id="50KMJClb7hA" role="3clFbG">
            <node concept="37vLTw" id="50KMJClb7hB" role="2Oq$k0">
              <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
            </node>
            <node concept="liA8E" id="50KMJClb7hC" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Component.setBounds(int,int,int,int):void" resolve="setBounds" />
              <node concept="3cmrfG" id="50KMJClb7hD" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3cmrfG" id="50KMJClb7hE" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="50KMJClmWoL" role="37wK5m">
                <ref role="3cqZAo" node="50KMJClmUKP" resolve="myWidth" />
              </node>
              <node concept="37vLTw" id="50KMJClb7hG" role="37wK5m">
                <ref role="3cqZAo" node="50KMJClb7c3" resolve="maxY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="50KMJClb7hH" role="3cqZAp">
          <node concept="2OqwBi" id="50KMJClb7hI" role="3clFbG">
            <node concept="37vLTw" id="50KMJClb7hJ" role="2Oq$k0">
              <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
            </node>
            <node concept="liA8E" id="50KMJClb7hK" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setOpaque(boolean):void" resolve="setOpaque" />
              <node concept="3clFbT" id="50KMJClb7hL" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1X3_iC" id="50KMJClqu0F" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="3clFbF" id="50KMJCloDn7" role="8Wnug">
            <node concept="2OqwBi" id="50KMJCloFkz" role="3clFbG">
              <node concept="37vLTw" id="50KMJCloEbv" role="2Oq$k0">
                <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
              </node>
              <node concept="liA8E" id="50KMJCloHd$" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~JComponent.setBorder(javax.swing.border.Border):void" resolve="setBorder" />
                <node concept="2YIFZM" id="50KMJCloJOG" role="37wK5m">
                  <ref role="37wK5l" to="dxuu:~BorderFactory.createMatteBorder(int,int,int,int,java.awt.Color):javax.swing.border.MatteBorder" resolve="createMatteBorder" />
                  <ref role="1Pybhc" to="dxuu:~BorderFactory" resolve="BorderFactory" />
                  <node concept="3cmrfG" id="50KMJCloJZI" role="37wK5m">
                    <property role="3cmrfH" value="0" />
                  </node>
                  <node concept="3cmrfG" id="50KMJCloKg3" role="37wK5m">
                    <property role="3cmrfH" value="0" />
                  </node>
                  <node concept="3cmrfG" id="50KMJCloKqD" role="37wK5m">
                    <property role="3cmrfH" value="0" />
                  </node>
                  <node concept="3cmrfG" id="50KMJCloK_g" role="37wK5m">
                    <property role="3cmrfH" value="1" />
                  </node>
                  <node concept="10M0yZ" id="50KMJCloKYl" role="37wK5m">
                    <ref role="3cqZAo" to="z60i:~Color.GRAY" resolve="GRAY" />
                    <ref role="1PxDUh" to="z60i:~Color" resolve="Color" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="50KMJClb7hM" role="3cqZAp">
          <node concept="37vLTw" id="50KMJClb7hN" role="3cqZAk">
            <ref role="3cqZAo" node="50KMJClb7ci" resolve="pane" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="50KMJClb62k" role="1B3o_S" />
      <node concept="3uibUv" id="50KMJClb6Rb" role="3clF45">
        <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
      </node>
    </node>
    <node concept="2tJIrI" id="76MUimxuW4T" role="jymVt" />
    <node concept="3clFb_" id="76MUimxuXbg" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="container_old_behavior" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="37vLTG" id="76MUimxvMcI" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="76MUimxvMcJ" role="1tU5fm">
          <ref role="ehGHo" to="4s7a:3LUIgcnlyz5" resolve="IntentionContainer" />
        </node>
      </node>
      <node concept="37vLTG" id="76MUimxvJwi" role="3clF46">
        <property role="TrG5h" value="editorContext" />
        <node concept="3uibUv" id="76MUimxvJwj" role="1tU5fm">
          <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
        </node>
      </node>
      <node concept="3clFbS" id="76MUimxuXbj" role="3clF47">
        <node concept="3cpWs8" id="76MUimxuXlR" role="3cqZAp">
          <node concept="3cpWsn" id="76MUimxuXlS" role="3cpWs9">
            <property role="TrG5h" value="width" />
            <node concept="10Oyi0" id="76MUimxuXlT" role="1tU5fm" />
            <node concept="3cmrfG" id="76MUimxuXlU" role="33vP2m">
              <property role="3cmrfH" value="15" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="76MUimxuXlV" role="3cqZAp">
          <node concept="3cpWsn" id="76MUimxuXlW" role="3cpWs9">
            <property role="TrG5h" value="maxY" />
            <node concept="10Oyi0" id="76MUimxuXlX" role="1tU5fm" />
            <node concept="3cmrfG" id="76MUimxuXlY" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="76MUimxuXlZ" role="3cqZAp" />
        <node concept="34ab3g" id="76MUimxuXm0" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="76MUimxuXm1" role="34bqiv">
            <node concept="3cpWs3" id="76MUimxuXm2" role="3uHU7B">
              <node concept="Xl_RD" id="76MUimxuXm3" role="3uHU7B">
                <property role="Xl_RC" value="Show " />
              </node>
              <node concept="2OqwBi" id="76MUimxuXm4" role="3uHU7w">
                <node concept="2OqwBi" id="76MUimxuXm5" role="2Oq$k0">
                  <node concept="37vLTw" id="76MUimxvOex" role="2Oq$k0">
                    <ref role="3cqZAo" node="76MUimxvMcI" resolve="node" />
                  </node>
                  <node concept="3Tsc0h" id="76MUimxvPax" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
                  </node>
                </node>
                <node concept="34oBXx" id="76MUimxuXm8" role="2OqNvi" />
              </node>
            </node>
            <node concept="Xl_RD" id="76MUimxuXm9" role="3uHU7w">
              <property role="Xl_RC" value=" intentions." />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="76MUimxuXma" role="3cqZAp">
          <node concept="3cpWsn" id="76MUimxuXmb" role="3cpWs9">
            <property role="TrG5h" value="pane" />
            <node concept="3uibUv" id="76MUimxuXmc" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JLayeredPane" resolve="JLayeredPane" />
            </node>
            <node concept="2ShNRf" id="76MUimxuXmd" role="33vP2m">
              <node concept="1pGfFk" id="76MUimxuXme" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JLayeredPane.&lt;init&gt;()" resolve="JLayeredPane" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="76MUimxuXmf" role="3cqZAp" />
        <node concept="3cpWs8" id="76MUimxuXmg" role="3cqZAp">
          <node concept="3cpWsn" id="76MUimxuXmh" role="3cpWs9">
            <property role="TrG5h" value="layer" />
            <node concept="10Oyi0" id="76MUimxuXmi" role="1tU5fm" />
            <node concept="3cmrfG" id="76MUimxuXmj" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="76MUimxuXmk" role="3cqZAp">
          <node concept="3SKdUq" id="76MUimxuXml" role="3SKWNk">
            <property role="3SKdUp" value="Sort the intention nodes from outer to inner, and first to last" />
          </node>
        </node>
        <node concept="2Gpval" id="76MUimxuXmm" role="3cqZAp">
          <node concept="2GrKxI" id="76MUimxuXmn" role="2Gsz3X">
            <property role="TrG5h" value="intention" />
          </node>
          <node concept="2OqwBi" id="76MUimxuXmo" role="2GsD0m">
            <node concept="2OqwBi" id="76MUimxuXmp" role="2Oq$k0">
              <node concept="2OqwBi" id="76MUimxuXmq" role="2Oq$k0">
                <node concept="37vLTw" id="76MUimxvPtZ" role="2Oq$k0">
                  <ref role="3cqZAo" node="76MUimxvMcI" resolve="node" />
                </node>
                <node concept="3Tsc0h" id="76MUimxuXms" role="2OqNvi">
                  <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
                </node>
              </node>
              <node concept="3zZkjj" id="76MUimxuXmt" role="2OqNvi">
                <node concept="1bVj0M" id="76MUimxuXmu" role="23t8la">
                  <node concept="3clFbS" id="76MUimxuXmv" role="1bW5cS">
                    <node concept="3clFbF" id="76MUimxuXmw" role="3cqZAp">
                      <node concept="3fqX7Q" id="76MUimxuXmx" role="3clFbG">
                        <node concept="2OqwBi" id="76MUimxuXmy" role="3fr31v">
                          <node concept="2OqwBi" id="76MUimxuXmz" role="2Oq$k0">
                            <node concept="37vLTw" id="76MUimxuXm$" role="2Oq$k0">
                              <ref role="3cqZAo" node="76MUimxuXmC" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="76MUimxuXm_" role="2OqNvi">
                              <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                            </node>
                          </node>
                          <node concept="liA8E" id="76MUimxuXmA" role="2OqNvi">
                            <ref role="37wK5l" to="wyt6:~String.contains(java.lang.CharSequence):boolean" resolve="contains" />
                            <node concept="Xl_RD" id="76MUimxuXmB" role="37wK5m">
                              <property role="Xl_RC" value="Generic" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="76MUimxuXmC" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="76MUimxuXmD" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2DpFxk" id="76MUimxuXmE" role="2OqNvi">
              <node concept="1bVj0M" id="76MUimxuXmF" role="23t8la">
                <node concept="3clFbS" id="76MUimxuXmG" role="1bW5cS">
                  <node concept="3clFbJ" id="76MUimxuXmH" role="3cqZAp">
                    <node concept="3clFbC" id="76MUimxuXmI" role="3clFbw">
                      <node concept="2OqwBi" id="76MUimxuXmJ" role="3uHU7w">
                        <node concept="2OqwBi" id="76MUimxuXmK" role="2Oq$k0">
                          <node concept="37vLTw" id="76MUimxuXmL" role="2Oq$k0">
                            <ref role="3cqZAo" node="76MUimxuXnC" resolve="b" />
                          </node>
                          <node concept="3TrEf2" id="76MUimxuXmM" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                        <node concept="1mfA1w" id="76MUimxuXmN" role="2OqNvi" />
                      </node>
                      <node concept="2OqwBi" id="76MUimxuXmO" role="3uHU7B">
                        <node concept="2OqwBi" id="76MUimxuXmP" role="2Oq$k0">
                          <node concept="37vLTw" id="76MUimxuXmQ" role="2Oq$k0">
                            <ref role="3cqZAo" node="76MUimxuXnA" resolve="a" />
                          </node>
                          <node concept="3TrEf2" id="76MUimxuXmR" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                        <node concept="1mfA1w" id="76MUimxuXmS" role="2OqNvi" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="76MUimxuXmT" role="3clFbx">
                      <node concept="3cpWs6" id="76MUimxuXmU" role="3cqZAp">
                        <node concept="2YIFZM" id="76MUimxuXmV" role="3cqZAk">
                          <ref role="37wK5l" to="wyt6:~Integer.compare(int,int):int" resolve="compare" />
                          <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                          <node concept="2OqwBi" id="76MUimxuXmW" role="37wK5m">
                            <node concept="2OqwBi" id="76MUimxuXmX" role="2Oq$k0">
                              <node concept="37vLTw" id="76MUimxuXmY" role="2Oq$k0">
                                <ref role="3cqZAo" node="76MUimxuXnA" resolve="a" />
                              </node>
                              <node concept="3TrEf2" id="76MUimxuXmZ" role="2OqNvi">
                                <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                              </node>
                            </node>
                            <node concept="2bSWHS" id="76MUimxuXn0" role="2OqNvi" />
                          </node>
                          <node concept="2OqwBi" id="76MUimxuXn1" role="37wK5m">
                            <node concept="2OqwBi" id="76MUimxuXn2" role="2Oq$k0">
                              <node concept="37vLTw" id="76MUimxuXn3" role="2Oq$k0">
                                <ref role="3cqZAo" node="76MUimxuXnC" resolve="b" />
                              </node>
                              <node concept="3TrEf2" id="76MUimxuXn4" role="2OqNvi">
                                <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                              </node>
                            </node>
                            <node concept="2bSWHS" id="76MUimxuXn5" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3eNFk2" id="76MUimxuXn6" role="3eNLev">
                      <node concept="3clFbS" id="76MUimxuXn7" role="3eOfB_">
                        <node concept="3cpWs6" id="76MUimxuXn8" role="3cqZAp">
                          <node concept="3cmrfG" id="76MUimxuXn9" role="3cqZAk">
                            <property role="3cmrfH" value="1" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="76MUimxuXna" role="3eO9$A">
                        <node concept="2OqwBi" id="76MUimxuXnb" role="2Oq$k0">
                          <node concept="2OqwBi" id="76MUimxuXnc" role="2Oq$k0">
                            <node concept="37vLTw" id="76MUimxuXnd" role="2Oq$k0">
                              <ref role="3cqZAo" node="76MUimxuXnA" resolve="a" />
                            </node>
                            <node concept="3TrEf2" id="76MUimxuXne" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                          <node concept="z$bX8" id="76MUimxuXnf" role="2OqNvi" />
                        </node>
                        <node concept="3JPx81" id="76MUimxuXng" role="2OqNvi">
                          <node concept="2OqwBi" id="76MUimxuXnh" role="25WWJ7">
                            <node concept="37vLTw" id="76MUimxuXni" role="2Oq$k0">
                              <ref role="3cqZAo" node="76MUimxuXnC" resolve="b" />
                            </node>
                            <node concept="3TrEf2" id="76MUimxuXnj" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3eNFk2" id="76MUimxuXnk" role="3eNLev">
                      <node concept="2OqwBi" id="76MUimxuXnl" role="3eO9$A">
                        <node concept="2OqwBi" id="76MUimxuXnm" role="2Oq$k0">
                          <node concept="2OqwBi" id="76MUimxuXnn" role="2Oq$k0">
                            <node concept="37vLTw" id="76MUimxuXno" role="2Oq$k0">
                              <ref role="3cqZAo" node="76MUimxuXnC" resolve="b" />
                            </node>
                            <node concept="3TrEf2" id="76MUimxuXnp" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                          <node concept="z$bX8" id="76MUimxuXnq" role="2OqNvi" />
                        </node>
                        <node concept="3JPx81" id="76MUimxuXnr" role="2OqNvi">
                          <node concept="2OqwBi" id="76MUimxuXns" role="25WWJ7">
                            <node concept="37vLTw" id="76MUimxuXnt" role="2Oq$k0">
                              <ref role="3cqZAo" node="76MUimxuXnA" resolve="a" />
                            </node>
                            <node concept="3TrEf2" id="76MUimxuXnu" role="2OqNvi">
                              <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="76MUimxuXnv" role="3eOfB_">
                        <node concept="3cpWs6" id="76MUimxuXnw" role="3cqZAp">
                          <node concept="3cmrfG" id="76MUimxuXnx" role="3cqZAk">
                            <property role="3cmrfH" value="-1" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="9aQIb" id="76MUimxuXny" role="9aQIa">
                      <node concept="3clFbS" id="76MUimxuXnz" role="9aQI4">
                        <node concept="3cpWs6" id="76MUimxuXn$" role="3cqZAp">
                          <node concept="3cmrfG" id="76MUimxuXn_" role="3cqZAk">
                            <property role="3cmrfH" value="0" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="76MUimxuXnA" role="1bW2Oz">
                  <property role="TrG5h" value="a" />
                  <node concept="2jxLKc" id="76MUimxuXnB" role="1tU5fm" />
                </node>
                <node concept="Rh6nW" id="76MUimxuXnC" role="1bW2Oz">
                  <property role="TrG5h" value="b" />
                  <node concept="2jxLKc" id="76MUimxuXnD" role="1tU5fm" />
                </node>
              </node>
              <node concept="1nlBCl" id="76MUimxuXnE" role="2Dq5b$">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="76MUimxuXnF" role="2LFqv$">
            <node concept="3clFbH" id="76MUimxuXnG" role="3cqZAp" />
            <node concept="3cpWs8" id="76MUimxuXnH" role="3cqZAp">
              <node concept="3cpWsn" id="76MUimxuXnI" role="3cpWs9">
                <property role="TrG5h" value="nodes" />
                <node concept="2I9FWS" id="76MUimxuXnJ" role="1tU5fm" />
                <node concept="2ShNRf" id="76MUimxuXnK" role="33vP2m">
                  <node concept="2T8Vx0" id="76MUimxuXnL" role="2ShVmc">
                    <node concept="2I9FWS" id="76MUimxuXnM" role="2T96Bj" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="76MUimxuXnN" role="3cqZAp">
              <node concept="3clFbS" id="76MUimxuXnO" role="3clFbx">
                <node concept="3cpWs8" id="76MUimxuXnP" role="3cqZAp">
                  <node concept="3cpWsn" id="76MUimxuXnQ" role="3cpWs9">
                    <property role="TrG5h" value="exclusiveIntention" />
                    <node concept="3Tqbb2" id="76MUimxuXnR" role="1tU5fm">
                      <ref role="ehGHo" to="4s7a:43XlAbjBn0d" resolve="ExclusiveIntention" />
                    </node>
                    <node concept="1PxgMI" id="76MUimxuXnS" role="33vP2m">
                      <property role="1BlNFB" value="true" />
                      <ref role="1m5ApE" to="4s7a:43XlAbjBn0d" resolve="ExclusiveIntention" />
                      <node concept="2GrUjf" id="76MUimxuXnT" role="1m5AlR">
                        <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="76MUimxuXnU" role="3cqZAp">
                  <node concept="2OqwBi" id="76MUimxuXnV" role="3clFbG">
                    <node concept="37vLTw" id="76MUimxuXnW" role="2Oq$k0">
                      <ref role="3cqZAo" node="76MUimxuXnI" resolve="nodes" />
                    </node>
                    <node concept="X8dFx" id="76MUimxuXnX" role="2OqNvi">
                      <node concept="2OqwBi" id="76MUimxuXnY" role="25WWJ7">
                        <node concept="2OqwBi" id="76MUimxuXnZ" role="2Oq$k0">
                          <node concept="37vLTw" id="76MUimxuXo0" role="2Oq$k0">
                            <ref role="3cqZAo" node="76MUimxuXnQ" resolve="exclusiveIntention" />
                          </node>
                          <node concept="3Tsc0h" id="76MUimxuXo1" role="2OqNvi">
                            <ref role="3TtcxE" to="4s7a:43XlAbjBn0n" resolve="left" />
                          </node>
                        </node>
                        <node concept="3$u5V9" id="76MUimxuXo2" role="2OqNvi">
                          <node concept="1bVj0M" id="76MUimxuXo3" role="23t8la">
                            <node concept="3clFbS" id="76MUimxuXo4" role="1bW5cS">
                              <node concept="3clFbF" id="76MUimxuXo5" role="3cqZAp">
                                <node concept="2OqwBi" id="76MUimxuXo6" role="3clFbG">
                                  <node concept="37vLTw" id="76MUimxuXo7" role="2Oq$k0">
                                    <ref role="3cqZAo" node="76MUimxuXo9" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="76MUimxuXo8" role="2OqNvi">
                                    <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="Rh6nW" id="76MUimxuXo9" role="1bW2Oz">
                              <property role="TrG5h" value="it" />
                              <node concept="2jxLKc" id="76MUimxuXoa" role="1tU5fm" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="76MUimxuXob" role="3cqZAp">
                  <node concept="2OqwBi" id="76MUimxuXoc" role="3clFbG">
                    <node concept="37vLTw" id="76MUimxuXod" role="2Oq$k0">
                      <ref role="3cqZAo" node="76MUimxuXnI" resolve="nodes" />
                    </node>
                    <node concept="X8dFx" id="76MUimxuXoe" role="2OqNvi">
                      <node concept="2OqwBi" id="76MUimxuXof" role="25WWJ7">
                        <node concept="2OqwBi" id="76MUimxuXog" role="2Oq$k0">
                          <node concept="37vLTw" id="76MUimxuXoh" role="2Oq$k0">
                            <ref role="3cqZAo" node="76MUimxuXnQ" resolve="exclusiveIntention" />
                          </node>
                          <node concept="3Tsc0h" id="76MUimxuXoi" role="2OqNvi">
                            <ref role="3TtcxE" to="4s7a:43XlAbjBn0p" resolve="right" />
                          </node>
                        </node>
                        <node concept="3$u5V9" id="76MUimxuXoj" role="2OqNvi">
                          <node concept="1bVj0M" id="76MUimxuXok" role="23t8la">
                            <node concept="3clFbS" id="76MUimxuXol" role="1bW5cS">
                              <node concept="3clFbF" id="76MUimxuXom" role="3cqZAp">
                                <node concept="2OqwBi" id="76MUimxuXon" role="3clFbG">
                                  <node concept="37vLTw" id="76MUimxuXoo" role="2Oq$k0">
                                    <ref role="3cqZAo" node="76MUimxuXoq" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="76MUimxuXop" role="2OqNvi">
                                    <ref role="3Tt5mk" to="4s7a:43XlAbjBn0l" resolve="content" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="Rh6nW" id="76MUimxuXoq" role="1bW2Oz">
                              <property role="TrG5h" value="it" />
                              <node concept="2jxLKc" id="76MUimxuXor" role="1tU5fm" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="17R0WA" id="76MUimxuXos" role="3clFbw">
                <node concept="2OqwBi" id="76MUimxuXot" role="3uHU7B">
                  <node concept="2GrUjf" id="76MUimxuXou" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                  </node>
                  <node concept="3TrcHB" id="76MUimxuXov" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                  </node>
                </node>
                <node concept="Xl_RD" id="76MUimxuXow" role="3uHU7w">
                  <property role="Xl_RC" value="Exclusive" />
                </node>
              </node>
              <node concept="9aQIb" id="76MUimxuXox" role="9aQIa">
                <node concept="3clFbS" id="76MUimxuXoy" role="9aQI4">
                  <node concept="3clFbF" id="76MUimxuXoz" role="3cqZAp">
                    <node concept="2OqwBi" id="76MUimxuXo$" role="3clFbG">
                      <node concept="37vLTw" id="76MUimxuXo_" role="2Oq$k0">
                        <ref role="3cqZAo" node="76MUimxuXnI" resolve="nodes" />
                      </node>
                      <node concept="TSZUe" id="76MUimxuXoA" role="2OqNvi">
                        <node concept="2OqwBi" id="76MUimxuXoB" role="25WWJ7">
                          <node concept="2GrUjf" id="76MUimxuXoC" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                          </node>
                          <node concept="3TrEf2" id="76MUimxuXoD" role="2OqNvi">
                            <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXoE" role="3cqZAp" />
            <node concept="3cpWs8" id="76MUimxuXoF" role="3cqZAp">
              <node concept="3cpWsn" id="76MUimxuXoG" role="3cpWs9">
                <property role="TrG5h" value="panel" />
                <node concept="3uibUv" id="76MUimxuXoH" role="1tU5fm">
                  <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
                </node>
                <node concept="2ShNRf" id="76MUimxuXoI" role="33vP2m">
                  <node concept="1pGfFk" id="76MUimxuXoJ" role="2ShVmc">
                    <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="76MUimxuXoK" role="3cqZAp">
              <node concept="2OqwBi" id="76MUimxuXoL" role="3clFbG">
                <node concept="37vLTw" id="76MUimxuXoM" role="2Oq$k0">
                  <ref role="3cqZAo" node="76MUimxuXoG" resolve="panel" />
                </node>
                <node concept="liA8E" id="76MUimxuXoN" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
                  <node concept="2ShNRf" id="76MUimxuXoO" role="37wK5m">
                    <node concept="1pGfFk" id="76MUimxuXoP" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~BoxLayout.&lt;init&gt;(java.awt.Container,int)" resolve="BoxLayout" />
                      <node concept="37vLTw" id="76MUimxuXoQ" role="37wK5m">
                        <ref role="3cqZAo" node="76MUimxuXoG" resolve="panel" />
                      </node>
                      <node concept="10M0yZ" id="76MUimxuXoR" role="37wK5m">
                        <ref role="1PxDUh" to="dxuu:~BoxLayout" resolve="BoxLayout" />
                        <ref role="3cqZAo" to="dxuu:~BoxLayout.PAGE_AXIS" resolve="PAGE_AXIS" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXoS" role="3cqZAp" />
            <node concept="3cpWs8" id="76MUimxuXoT" role="3cqZAp">
              <node concept="3cpWsn" id="76MUimxuXoU" role="3cpWs9">
                <property role="TrG5h" value="color" />
                <node concept="3uibUv" id="76MUimxuXoV" role="1tU5fm">
                  <ref role="3uigEE" to="z60i:~Color" resolve="Color" />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="76MUimxuXoW" role="3cqZAp">
              <node concept="3cpWsn" id="76MUimxuXoX" role="3cpWs9">
                <property role="TrG5h" value="tooltip" />
                <node concept="17QB3L" id="76MUimxuXoY" role="1tU5fm" />
              </node>
            </node>
            <node concept="3clFbJ" id="76MUimxuXoZ" role="3cqZAp">
              <node concept="3clFbS" id="76MUimxuXp0" role="3clFbx">
                <node concept="3SKdUt" id="76MUimxuXp1" role="3cqZAp">
                  <node concept="3SKdUq" id="76MUimxuXp2" role="3SKWNk">
                    <property role="3SKdUp" value="Green" />
                  </node>
                </node>
                <node concept="3clFbF" id="76MUimxuXp3" role="3cqZAp">
                  <node concept="37vLTI" id="76MUimxuXp4" role="3clFbG">
                    <node concept="2ShNRf" id="76MUimxuXp5" role="37vLTx">
                      <node concept="1pGfFk" id="76MUimxuXp6" role="2ShVmc">
                        <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                        <node concept="3cmrfG" id="76MUimxuXp7" role="37wK5m">
                          <property role="3cmrfH" value="44" />
                        </node>
                        <node concept="3cmrfG" id="76MUimxuXp8" role="37wK5m">
                          <property role="3cmrfH" value="163" />
                        </node>
                        <node concept="3cmrfG" id="76MUimxuXp9" role="37wK5m">
                          <property role="3cmrfH" value="48" />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="76MUimxuXpa" role="37vLTJ">
                      <ref role="3cqZAo" node="76MUimxuXoU" resolve="color" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="76MUimxuXpb" role="3cqZAp">
                  <node concept="37vLTI" id="76MUimxuXpc" role="3clFbG">
                    <node concept="Xl_RD" id="76MUimxuXpd" role="37vLTx">
                      <property role="Xl_RC" value="Keep" />
                    </node>
                    <node concept="37vLTw" id="76MUimxuXpe" role="37vLTJ">
                      <ref role="3cqZAo" node="76MUimxuXoX" resolve="tooltip" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="17R0WA" id="76MUimxuXpf" role="3clFbw">
                <node concept="Xl_RD" id="76MUimxuXpg" role="3uHU7w">
                  <property role="Xl_RC" value="Keep" />
                </node>
                <node concept="2OqwBi" id="76MUimxuXph" role="3uHU7B">
                  <node concept="2GrUjf" id="76MUimxuXpi" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                  </node>
                  <node concept="3TrcHB" id="76MUimxuXpj" role="2OqNvi">
                    <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                  </node>
                </node>
              </node>
              <node concept="3eNFk2" id="76MUimxuXpk" role="3eNLev">
                <node concept="3clFbS" id="76MUimxuXpl" role="3eOfB_">
                  <node concept="3SKdUt" id="76MUimxuXpm" role="3cqZAp">
                    <node concept="3SKdUq" id="76MUimxuXpn" role="3SKWNk">
                      <property role="3SKdUp" value="Other Green" />
                    </node>
                  </node>
                  <node concept="3clFbF" id="76MUimxuXpo" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXpp" role="3clFbG">
                      <node concept="2ShNRf" id="76MUimxuXpq" role="37vLTx">
                        <node concept="1pGfFk" id="76MUimxuXpr" role="2ShVmc">
                          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                          <node concept="3cmrfG" id="76MUimxuXps" role="37wK5m">
                            <property role="3cmrfH" value="191" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXpt" role="37wK5m">
                            <property role="3cmrfH" value="244" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXpu" role="37wK5m">
                            <property role="3cmrfH" value="66" />
                          </node>
                        </node>
                      </node>
                      <node concept="37vLTw" id="76MUimxuXpv" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoU" resolve="color" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="76MUimxuXpw" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXpx" role="3clFbG">
                      <node concept="3cpWs3" id="76MUimxuXpy" role="37vLTx">
                        <node concept="2OqwBi" id="76MUimxuXpz" role="3uHU7w">
                          <node concept="2GrUjf" id="76MUimxuXp$" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                          </node>
                          <node concept="3TrcHB" id="76MUimxuXp_" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:3jeRE17nVF2" resolve="featurename" />
                          </node>
                        </node>
                        <node concept="Xl_RD" id="76MUimxuXpA" role="3uHU7B">
                          <property role="Xl_RC" value="KeepAsFeature " />
                        </node>
                      </node>
                      <node concept="37vLTw" id="76MUimxuXpB" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoX" resolve="tooltip" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="17R0WA" id="76MUimxuXpC" role="3eO9$A">
                  <node concept="Xl_RD" id="76MUimxuXpD" role="3uHU7w">
                    <property role="Xl_RC" value="KeepAsFeature" />
                  </node>
                  <node concept="2OqwBi" id="76MUimxuXpE" role="3uHU7B">
                    <node concept="2GrUjf" id="76MUimxuXpF" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                    </node>
                    <node concept="3TrcHB" id="76MUimxuXpG" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="9aQIb" id="76MUimxuXpH" role="9aQIa">
                <node concept="3clFbS" id="76MUimxuXpI" role="9aQI4">
                  <node concept="3SKdUt" id="76MUimxuXpJ" role="3cqZAp">
                    <node concept="3SKdUq" id="76MUimxuXpK" role="3SKWNk">
                      <property role="3SKdUp" value="Grey" />
                    </node>
                  </node>
                  <node concept="3clFbF" id="76MUimxuXpL" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXpM" role="3clFbG">
                      <node concept="2ShNRf" id="76MUimxuXpN" role="37vLTx">
                        <node concept="1pGfFk" id="76MUimxuXpO" role="2ShVmc">
                          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                          <node concept="3cmrfG" id="76MUimxuXpP" role="37wK5m">
                            <property role="3cmrfH" value="178" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXpQ" role="37wK5m">
                            <property role="3cmrfH" value="178" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXpR" role="37wK5m">
                            <property role="3cmrfH" value="178" />
                          </node>
                        </node>
                      </node>
                      <node concept="37vLTw" id="76MUimxuXpS" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoU" resolve="color" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="76MUimxuXpT" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXpU" role="3clFbG">
                      <node concept="37vLTw" id="76MUimxuXpV" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoX" resolve="tooltip" />
                      </node>
                      <node concept="3cpWs3" id="76MUimxuXpW" role="37vLTx">
                        <node concept="2OqwBi" id="76MUimxuXpX" role="3uHU7w">
                          <node concept="2GrUjf" id="76MUimxuXpY" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                          </node>
                          <node concept="3TrcHB" id="76MUimxuXpZ" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                          </node>
                        </node>
                        <node concept="Xl_RD" id="76MUimxuXq0" role="3uHU7B">
                          <property role="Xl_RC" value="Intention " />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eNFk2" id="76MUimxuXq1" role="3eNLev">
                <node concept="17R0WA" id="76MUimxuXq2" role="3eO9$A">
                  <node concept="Xl_RD" id="76MUimxuXq3" role="3uHU7w">
                    <property role="Xl_RC" value="Exclusive" />
                  </node>
                  <node concept="2OqwBi" id="76MUimxuXq4" role="3uHU7B">
                    <node concept="2GrUjf" id="76MUimxuXq5" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                    </node>
                    <node concept="3TrcHB" id="76MUimxuXq6" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbS" id="76MUimxuXq7" role="3eOfB_">
                  <node concept="3SKdUt" id="76MUimxuXq8" role="3cqZAp">
                    <node concept="3SKdUq" id="76MUimxuXq9" role="3SKWNk">
                      <property role="3SKdUp" value="Orange" />
                    </node>
                  </node>
                  <node concept="3clFbF" id="76MUimxuXqa" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXqb" role="3clFbG">
                      <node concept="2ShNRf" id="76MUimxuXqc" role="37vLTx">
                        <node concept="1pGfFk" id="76MUimxuXqd" role="2ShVmc">
                          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                          <node concept="3cmrfG" id="76MUimxuXqe" role="37wK5m">
                            <property role="3cmrfH" value="244" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXqf" role="37wK5m">
                            <property role="3cmrfH" value="176" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXqg" role="37wK5m">
                            <property role="3cmrfH" value="66" />
                          </node>
                        </node>
                      </node>
                      <node concept="37vLTw" id="76MUimxuXqh" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoU" resolve="color" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="76MUimxuXqi" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXqj" role="3clFbG">
                      <node concept="3cpWs3" id="76MUimxuXqk" role="37vLTx">
                        <node concept="2OqwBi" id="76MUimxuXql" role="3uHU7w">
                          <node concept="2GrUjf" id="76MUimxuXqm" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                          </node>
                          <node concept="3TrcHB" id="76MUimxuXqn" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:3jeRE17nVF2" resolve="featurename" />
                          </node>
                        </node>
                        <node concept="Xl_RD" id="76MUimxuXqo" role="3uHU7B">
                          <property role="Xl_RC" value="Exclusive " />
                        </node>
                      </node>
                      <node concept="37vLTw" id="76MUimxuXqp" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoX" resolve="tooltip" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eNFk2" id="76MUimxuXqq" role="3eNLev">
                <node concept="17R0WA" id="76MUimxuXqr" role="3eO9$A">
                  <node concept="Xl_RD" id="76MUimxuXqs" role="3uHU7w">
                    <property role="Xl_RC" value="Postpone" />
                  </node>
                  <node concept="2OqwBi" id="76MUimxuXqt" role="3uHU7B">
                    <node concept="2GrUjf" id="76MUimxuXqu" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                    </node>
                    <node concept="3TrcHB" id="76MUimxuXqv" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbS" id="76MUimxuXqw" role="3eOfB_">
                  <node concept="3clFbF" id="76MUimxuXqx" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXqy" role="3clFbG">
                      <node concept="37vLTw" id="76MUimxuXqz" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoU" resolve="color" />
                      </node>
                      <node concept="2ShNRf" id="76MUimxuXq$" role="37vLTx">
                        <node concept="1pGfFk" id="76MUimxuXq_" role="2ShVmc">
                          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                          <node concept="3cmrfG" id="76MUimxuXqA" role="37wK5m">
                            <property role="3cmrfH" value="220" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXqB" role="37wK5m">
                            <property role="3cmrfH" value="228" />
                          </node>
                          <node concept="3cmrfG" id="76MUimxuXqC" role="37wK5m">
                            <property role="3cmrfH" value="242" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="76MUimxuXqD" role="3cqZAp">
                    <node concept="37vLTI" id="76MUimxuXqE" role="3clFbG">
                      <node concept="3cpWs3" id="76MUimxuXqF" role="37vLTx">
                        <node concept="2OqwBi" id="76MUimxuXqG" role="3uHU7w">
                          <node concept="1eOMI4" id="76MUimxuXqH" role="2Oq$k0">
                            <node concept="1PxgMI" id="76MUimxuXqI" role="1eOMHV">
                              <property role="1BlNFB" value="true" />
                              <ref role="1m5ApE" to="4s7a:3aElbFxeJD9" resolve="PostponeIntention" />
                              <node concept="2GrUjf" id="76MUimxuXqJ" role="1m5AlR">
                                <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                              </node>
                            </node>
                          </node>
                          <node concept="3TrcHB" id="76MUimxuXqK" role="2OqNvi">
                            <ref role="3TsBF5" to="4s7a:3aElbFxeJDc" resolve="note" />
                          </node>
                        </node>
                        <node concept="Xl_RD" id="76MUimxuXqL" role="3uHU7B">
                          <property role="Xl_RC" value="Postpone " />
                        </node>
                      </node>
                      <node concept="37vLTw" id="76MUimxuXqM" role="37vLTJ">
                        <ref role="3cqZAo" node="76MUimxuXoX" resolve="tooltip" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXqN" role="3cqZAp" />
            <node concept="3clFbF" id="76MUimxuXqO" role="3cqZAp">
              <node concept="d57v9" id="76MUimxuXqP" role="3clFbG">
                <node concept="3cpWs3" id="76MUimxuXqQ" role="37vLTx">
                  <node concept="2OqwBi" id="76MUimxuXqR" role="3uHU7w">
                    <node concept="2GrUjf" id="76MUimxuXqS" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                    </node>
                    <node concept="3TrcHB" id="76MUimxuXqT" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:1SJQf$Wm6pV" resolve="view" />
                    </node>
                  </node>
                  <node concept="Xl_RD" id="76MUimxuXqU" role="3uHU7B">
                    <property role="Xl_RC" value=" view " />
                  </node>
                </node>
                <node concept="37vLTw" id="76MUimxuXqV" role="37vLTJ">
                  <ref role="3cqZAo" node="76MUimxuXoX" resolve="tooltip" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXqW" role="3cqZAp" />
            <node concept="3cpWs8" id="76MUimxuXqX" role="3cqZAp">
              <node concept="3cpWsn" id="76MUimxuXqY" role="3cpWs9">
                <property role="TrG5h" value="panelstart" />
                <node concept="10Oyi0" id="76MUimxuXqZ" role="1tU5fm" />
                <node concept="3cmrfG" id="76MUimxuXr0" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="76MUimxuXr1" role="3cqZAp">
              <node concept="3cpWsn" id="76MUimxuXr2" role="3cpWs9">
                <property role="TrG5h" value="panelend" />
                <node concept="10Oyi0" id="76MUimxuXr3" role="1tU5fm" />
                <node concept="3cmrfG" id="76MUimxuXr4" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXr5" role="3cqZAp" />
            <node concept="2Gpval" id="76MUimxuXr6" role="3cqZAp">
              <node concept="2GrKxI" id="76MUimxuXr7" role="2Gsz3X">
                <property role="TrG5h" value="intentionnode" />
              </node>
              <node concept="37vLTw" id="76MUimxuXr8" role="2GsD0m">
                <ref role="3cqZAo" node="76MUimxuXnI" resolve="nodes" />
              </node>
              <node concept="3clFbS" id="76MUimxuXr9" role="2LFqv$">
                <node concept="3cpWs8" id="76MUimxuXra" role="3cqZAp">
                  <node concept="3cpWsn" id="76MUimxuXrb" role="3cpWs9">
                    <property role="TrG5h" value="editorcell" />
                    <node concept="3uibUv" id="76MUimxuXrc" role="1tU5fm">
                      <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
                    </node>
                    <node concept="2OqwBi" id="76MUimxuXrd" role="33vP2m">
                      <node concept="2OqwBi" id="76MUimxuXre" role="2Oq$k0">
                        <node concept="37vLTw" id="76MUimxvPO1" role="2Oq$k0">
                          <ref role="3cqZAo" node="76MUimxvJwi" resolve="editorContext" />
                        </node>
                        <node concept="liA8E" id="76MUimxuXrg" role="2OqNvi">
                          <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                        </node>
                      </node>
                      <node concept="liA8E" id="76MUimxuXrh" role="2OqNvi">
                        <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                        <node concept="2GrUjf" id="76MUimxuXri" role="37wK5m">
                          <ref role="2Gs0qQ" node="76MUimxuXr7" resolve="intentionnode" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="76MUimxuXrj" role="3cqZAp">
                  <node concept="3cpWsn" id="76MUimxuXrk" role="3cpWs9">
                    <property role="TrG5h" value="intentionType" />
                    <node concept="3uibUv" id="76MUimxuXrl" role="1tU5fm">
                      <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
                    </node>
                    <node concept="2YIFZM" id="76MUimxuXrm" role="33vP2m">
                      <ref role="37wK5l" node="5XbSbOFVHhB" resolve="getIntention" />
                      <ref role="1Pybhc" node="5XbSbOFV6mO" resolve="IntentionType" />
                      <node concept="2OqwBi" id="76MUimxuXrn" role="37wK5m">
                        <node concept="2GrUjf" id="76MUimxuXro" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                        </node>
                        <node concept="3TrcHB" id="76MUimxuXrp" role="2OqNvi">
                          <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="76MUimxuXrq" role="3cqZAp" />
                <node concept="3clFbJ" id="76MUimxuXrr" role="3cqZAp">
                  <node concept="3clFbS" id="76MUimxuXrs" role="3clFbx">
                    <node concept="3SKdUt" id="76MUimxuXrt" role="3cqZAp">
                      <node concept="3SKdUq" id="76MUimxuXru" role="3SKWNk">
                        <property role="3SKdUp" value=" Should subtract (lineHeight / 2) because getY() starts in the middle of the first line" />
                      </node>
                    </node>
                    <node concept="3cpWs8" id="76MUimxuXrv" role="3cqZAp">
                      <node concept="3cpWsn" id="76MUimxuXrw" role="3cpWs9">
                        <property role="TrG5h" value="start" />
                        <node concept="10Oyi0" id="76MUimxuXrx" role="1tU5fm" />
                        <node concept="3cpWsd" id="76MUimxuXry" role="33vP2m">
                          <node concept="3cmrfG" id="76MUimxuXrz" role="3uHU7w">
                            <property role="3cmrfH" value="9" />
                          </node>
                          <node concept="2OqwBi" id="76MUimxuXr$" role="3uHU7B">
                            <node concept="37vLTw" id="76MUimxuXr_" role="2Oq$k0">
                              <ref role="3cqZAo" node="76MUimxuXrb" resolve="editorcell" />
                            </node>
                            <node concept="liA8E" id="76MUimxuXrA" role="2OqNvi">
                              <ref role="37wK5l" to="f4zo:~EditorCell.getY():int" resolve="getY" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3cpWs8" id="76MUimxuXrB" role="3cqZAp">
                      <node concept="3cpWsn" id="76MUimxuXrC" role="3cpWs9">
                        <property role="TrG5h" value="end" />
                        <node concept="10Oyi0" id="76MUimxuXrD" role="1tU5fm" />
                        <node concept="3cpWs3" id="76MUimxuXrE" role="33vP2m">
                          <node concept="37vLTw" id="76MUimxuXrF" role="3uHU7B">
                            <ref role="3cqZAo" node="76MUimxuXrw" resolve="start" />
                          </node>
                          <node concept="2OqwBi" id="76MUimxuXrG" role="3uHU7w">
                            <node concept="37vLTw" id="76MUimxuXrH" role="2Oq$k0">
                              <ref role="3cqZAo" node="76MUimxuXrb" resolve="editorcell" />
                            </node>
                            <node concept="liA8E" id="76MUimxuXrI" role="2OqNvi">
                              <ref role="37wK5l" to="f4zo:~EditorCell.getHeight():int" resolve="getHeight" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="76MUimxuXrJ" role="3cqZAp" />
                    <node concept="3clFbJ" id="76MUimxuXrK" role="3cqZAp">
                      <node concept="3clFbS" id="76MUimxuXrL" role="3clFbx">
                        <node concept="3clFbF" id="76MUimxuXrM" role="3cqZAp">
                          <node concept="37vLTI" id="76MUimxuXrN" role="3clFbG">
                            <node concept="37vLTw" id="76MUimxuXrO" role="37vLTx">
                              <ref role="3cqZAo" node="76MUimxuXrw" resolve="start" />
                            </node>
                            <node concept="37vLTw" id="76MUimxuXrP" role="37vLTJ">
                              <ref role="3cqZAo" node="76MUimxuXqY" resolve="panelstart" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbC" id="76MUimxuXrQ" role="3clFbw">
                        <node concept="2GrUjf" id="76MUimxuXrR" role="3uHU7w">
                          <ref role="2Gs0qQ" node="76MUimxuXr7" resolve="intentionnode" />
                        </node>
                        <node concept="2OqwBi" id="76MUimxuXrS" role="3uHU7B">
                          <node concept="37vLTw" id="76MUimxuXrT" role="2Oq$k0">
                            <ref role="3cqZAo" node="76MUimxuXnI" resolve="nodes" />
                          </node>
                          <node concept="1uHKPH" id="76MUimxuXrU" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="76MUimxuXrV" role="3cqZAp">
                      <node concept="3clFbS" id="76MUimxuXrW" role="3clFbx">
                        <node concept="3clFbF" id="76MUimxuXrX" role="3cqZAp">
                          <node concept="37vLTI" id="76MUimxuXrY" role="3clFbG">
                            <node concept="37vLTw" id="76MUimxuXrZ" role="37vLTx">
                              <ref role="3cqZAo" node="76MUimxuXrC" resolve="end" />
                            </node>
                            <node concept="37vLTw" id="76MUimxuXs0" role="37vLTJ">
                              <ref role="3cqZAo" node="76MUimxuXr2" resolve="panelend" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbC" id="76MUimxuXs1" role="3clFbw">
                        <node concept="2GrUjf" id="76MUimxuXs2" role="3uHU7w">
                          <ref role="2Gs0qQ" node="76MUimxuXr7" resolve="intentionnode" />
                        </node>
                        <node concept="2OqwBi" id="76MUimxuXs3" role="3uHU7B">
                          <node concept="37vLTw" id="76MUimxuXs4" role="2Oq$k0">
                            <ref role="3cqZAo" node="76MUimxuXnI" resolve="nodes" />
                          </node>
                          <node concept="1yVyf7" id="76MUimxuXs5" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="76MUimxuXs6" role="3cqZAp" />
                    <node concept="3SKdUt" id="76MUimxuXs7" role="3cqZAp">
                      <node concept="3SKdUq" id="76MUimxuXs8" role="3SKWNk">
                        <property role="3SKdUp" value="Intention display" />
                      </node>
                    </node>
                    <node concept="3clFbF" id="76MUimxuXs9" role="3cqZAp">
                      <node concept="2OqwBi" id="76MUimxuXsa" role="3clFbG">
                        <node concept="37vLTw" id="76MUimxuXsb" role="2Oq$k0">
                          <ref role="3cqZAo" node="76MUimxuXoG" resolve="panel" />
                        </node>
                        <node concept="liA8E" id="76MUimxuXsc" role="2OqNvi">
                          <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
                          <node concept="2ShNRf" id="76MUimxuXsd" role="37wK5m">
                            <node concept="1pGfFk" id="76MUimxuXse" role="2ShVmc">
                              <ref role="37wK5l" node="3LUIgcneH88" resolve="IntentionIndicatorGraphic" />
                              <node concept="3cmrfG" id="76MUimxuXsf" role="37wK5m">
                                <property role="3cmrfH" value="15" />
                              </node>
                              <node concept="3cpWsd" id="76MUimxuXsg" role="37wK5m">
                                <node concept="37vLTw" id="76MUimxuXsh" role="3uHU7w">
                                  <ref role="3cqZAo" node="76MUimxuXrw" resolve="start" />
                                </node>
                                <node concept="37vLTw" id="76MUimxuXsi" role="3uHU7B">
                                  <ref role="3cqZAo" node="76MUimxuXrC" resolve="end" />
                                </node>
                              </node>
                              <node concept="37vLTw" id="76MUimxuXsj" role="37wK5m">
                                <ref role="3cqZAo" node="76MUimxuXrk" resolve="intentionType" />
                              </node>
                              <node concept="2OqwBi" id="76MUimxuXsk" role="37wK5m">
                                <node concept="2GrUjf" id="76MUimxuXsl" role="2Oq$k0">
                                  <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                                </node>
                                <node concept="3TrcHB" id="76MUimxuXsm" role="2OqNvi">
                                  <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="76MUimxuXsn" role="37wK5m">
                                <node concept="2GrUjf" id="76MUimxuXso" role="2Oq$k0">
                                  <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                                </node>
                                <node concept="3TrcHB" id="76MUimxuXsp" role="2OqNvi">
                                  <ref role="3TsBF5" to="4s7a:3jeRE17nVF2" resolve="featurename" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="76MUimxuXsq" role="3cqZAp" />
                    <node concept="3clFbH" id="76MUimxuXsr" role="3cqZAp" />
                  </node>
                  <node concept="3y3z36" id="76MUimxuXss" role="3clFbw">
                    <node concept="10Nm6u" id="76MUimxuXst" role="3uHU7w" />
                    <node concept="37vLTw" id="76MUimxuXsu" role="3uHU7B">
                      <ref role="3cqZAo" node="76MUimxuXrb" resolve="editorcell" />
                    </node>
                  </node>
                  <node concept="9aQIb" id="76MUimxuXsv" role="9aQIa">
                    <node concept="3clFbS" id="76MUimxuXsw" role="9aQI4">
                      <node concept="34ab3g" id="76MUimxuXsx" role="3cqZAp">
                        <property role="35gtTG" value="warn" />
                        <node concept="3cpWs3" id="76MUimxuXsy" role="34bqiv">
                          <node concept="2OqwBi" id="76MUimxuXsz" role="3uHU7w">
                            <node concept="2OqwBi" id="76MUimxuXs$" role="2Oq$k0">
                              <node concept="2OqwBi" id="76MUimxuXs_" role="2Oq$k0">
                                <node concept="2GrUjf" id="76MUimxuXsA" role="2Oq$k0">
                                  <ref role="2Gs0qQ" node="76MUimxuXmn" resolve="intention" />
                                </node>
                                <node concept="3TrEf2" id="76MUimxuXsB" role="2OqNvi">
                                  <ref role="3Tt5mk" to="4s7a:3LUIgcnJeGE" resolve="content" />
                                </node>
                              </node>
                              <node concept="2yIwOk" id="76MUimxuXsC" role="2OqNvi" />
                            </node>
                            <node concept="liA8E" id="76MUimxuXsD" role="2OqNvi">
                              <ref role="37wK5l" to="c17a:~SAbstractConcept.getName():java.lang.String" resolve="getName" />
                            </node>
                          </node>
                          <node concept="Xl_RD" id="76MUimxuXsE" role="3uHU7B">
                            <property role="Xl_RC" value="IntentionContainer_Editor: Could not find editor cell for node " />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXsF" role="3cqZAp" />
            <node concept="3clFbF" id="76MUimxuXsG" role="3cqZAp">
              <node concept="2OqwBi" id="76MUimxuXsH" role="3clFbG">
                <node concept="37vLTw" id="76MUimxuXsI" role="2Oq$k0">
                  <ref role="3cqZAo" node="76MUimxuXoG" resolve="panel" />
                </node>
                <node concept="liA8E" id="76MUimxuXsJ" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Component.setSize(int,int):void" resolve="setSize" />
                  <node concept="37vLTw" id="76MUimxuXsK" role="37wK5m">
                    <ref role="3cqZAo" node="76MUimxuXlS" resolve="width" />
                  </node>
                  <node concept="3cpWsd" id="76MUimxuXsL" role="37wK5m">
                    <node concept="37vLTw" id="76MUimxuXsM" role="3uHU7w">
                      <ref role="3cqZAo" node="76MUimxuXqY" resolve="panelstart" />
                    </node>
                    <node concept="37vLTw" id="76MUimxuXsN" role="3uHU7B">
                      <ref role="3cqZAo" node="76MUimxuXr2" resolve="panelend" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="76MUimxuXsO" role="3cqZAp">
              <node concept="3SKdUq" id="76MUimxuXsP" role="3SKWNk">
                <property role="3SKdUp" value="Offset the indicator on x" />
              </node>
            </node>
            <node concept="3clFbF" id="76MUimxuXsQ" role="3cqZAp">
              <node concept="2OqwBi" id="76MUimxuXsR" role="3clFbG">
                <node concept="37vLTw" id="76MUimxuXsS" role="2Oq$k0">
                  <ref role="3cqZAo" node="76MUimxuXoG" resolve="panel" />
                </node>
                <node concept="liA8E" id="76MUimxuXsT" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Component.setLocation(int,int):void" resolve="setLocation" />
                  <node concept="1ZRNhn" id="76MUimxuXsU" role="37wK5m">
                    <node concept="1eOMI4" id="76MUimxuXsV" role="2$L3a6">
                      <node concept="17qRlL" id="76MUimxuXsW" role="1eOMHV">
                        <node concept="37vLTw" id="76MUimxuXsX" role="3uHU7B">
                          <ref role="3cqZAo" node="76MUimxuXmh" resolve="layer" />
                        </node>
                        <node concept="1eOMI4" id="76MUimxuXsY" role="3uHU7w">
                          <node concept="3cpWs3" id="76MUimxuXsZ" role="1eOMHV">
                            <node concept="37vLTw" id="76MUimxuXt0" role="3uHU7B">
                              <ref role="3cqZAo" node="76MUimxuXlS" resolve="width" />
                            </node>
                            <node concept="3cmrfG" id="76MUimxuXt1" role="3uHU7w">
                              <property role="3cmrfH" value="2" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTw" id="76MUimxuXt2" role="37wK5m">
                    <ref role="3cqZAo" node="76MUimxuXqY" resolve="panelstart" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="76MUimxuXt3" role="3cqZAp">
              <node concept="2OqwBi" id="76MUimxuXt4" role="3clFbG">
                <node concept="37vLTw" id="76MUimxuXt5" role="2Oq$k0">
                  <ref role="3cqZAo" node="76MUimxuXoG" resolve="panel" />
                </node>
                <node concept="liA8E" id="76MUimxuXt6" role="2OqNvi">
                  <ref role="37wK5l" to="dxuu:~JComponent.setToolTipText(java.lang.String):void" resolve="setToolTipText" />
                  <node concept="37vLTw" id="76MUimxuXt7" role="37wK5m">
                    <ref role="3cqZAo" node="76MUimxuXoX" resolve="tooltip" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="76MUimxuXt8" role="3cqZAp">
              <node concept="2OqwBi" id="76MUimxuXt9" role="3clFbG">
                <node concept="37vLTw" id="76MUimxuXta" role="2Oq$k0">
                  <ref role="3cqZAo" node="76MUimxuXmb" resolve="pane" />
                </node>
                <node concept="liA8E" id="76MUimxuXtb" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,int):java.awt.Component" resolve="add" />
                  <node concept="37vLTw" id="76MUimxuXtc" role="37wK5m">
                    <ref role="3cqZAo" node="76MUimxuXoG" resolve="panel" />
                  </node>
                  <node concept="3uO5VW" id="76MUimxuXtd" role="37wK5m">
                    <node concept="37vLTw" id="76MUimxuXte" role="2$L3a6">
                      <ref role="3cqZAo" node="76MUimxuXmh" resolve="layer" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXtf" role="3cqZAp" />
            <node concept="3clFbJ" id="76MUimxuXtg" role="3cqZAp">
              <node concept="3clFbS" id="76MUimxuXth" role="3clFbx">
                <node concept="3clFbF" id="76MUimxuXti" role="3cqZAp">
                  <node concept="37vLTI" id="76MUimxuXtj" role="3clFbG">
                    <node concept="37vLTw" id="76MUimxuXtk" role="37vLTx">
                      <ref role="3cqZAo" node="76MUimxuXr2" resolve="panelend" />
                    </node>
                    <node concept="37vLTw" id="76MUimxuXtl" role="37vLTJ">
                      <ref role="3cqZAo" node="76MUimxuXlW" resolve="maxY" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="76MUimxuXtm" role="3clFbw">
                <node concept="37vLTw" id="76MUimxuXtn" role="3uHU7w">
                  <ref role="3cqZAo" node="76MUimxuXlW" resolve="maxY" />
                </node>
                <node concept="37vLTw" id="76MUimxuXto" role="3uHU7B">
                  <ref role="3cqZAo" node="76MUimxuXr2" resolve="panelend" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="76MUimxuXtp" role="3cqZAp" />
            <node concept="3clFbH" id="76MUimxuXtq" role="3cqZAp" />
          </node>
        </node>
        <node concept="3clFbH" id="76MUimxuXtA" role="3cqZAp" />
        <node concept="3cpWs8" id="76MUimxuXtB" role="3cqZAp">
          <node concept="3cpWsn" id="76MUimxuXtC" role="3cpWs9">
            <property role="TrG5h" value="intentionCount" />
            <node concept="10Oyi0" id="76MUimxuXtD" role="1tU5fm" />
            <node concept="2OqwBi" id="76MUimxuXtE" role="33vP2m">
              <node concept="2OqwBi" id="76MUimxuXtF" role="2Oq$k0">
                <node concept="2OqwBi" id="76MUimxuXtG" role="2Oq$k0">
                  <node concept="37vLTw" id="76MUimxvQmg" role="2Oq$k0">
                    <ref role="3cqZAo" node="76MUimxvMcI" resolve="node" />
                  </node>
                  <node concept="3Tsc0h" id="76MUimxuXtI" role="2OqNvi">
                    <ref role="3TtcxE" to="4s7a:3LUIgcnlyz6" resolve="intentions" />
                  </node>
                </node>
                <node concept="3zZkjj" id="76MUimxuXtJ" role="2OqNvi">
                  <node concept="1bVj0M" id="76MUimxuXtK" role="23t8la">
                    <node concept="3clFbS" id="76MUimxuXtL" role="1bW5cS">
                      <node concept="3clFbF" id="76MUimxuXtM" role="3cqZAp">
                        <node concept="3fqX7Q" id="76MUimxuXtN" role="3clFbG">
                          <node concept="2OqwBi" id="76MUimxuXtO" role="3fr31v">
                            <node concept="2OqwBi" id="76MUimxuXtP" role="2Oq$k0">
                              <node concept="37vLTw" id="76MUimxuXtQ" role="2Oq$k0">
                                <ref role="3cqZAo" node="76MUimxuXtU" resolve="it" />
                              </node>
                              <node concept="3TrcHB" id="76MUimxuXtR" role="2OqNvi">
                                <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                              </node>
                            </node>
                            <node concept="liA8E" id="76MUimxuXtS" role="2OqNvi">
                              <ref role="37wK5l" to="wyt6:~String.contains(java.lang.CharSequence):boolean" resolve="contains" />
                              <node concept="Xl_RD" id="76MUimxuXtT" role="37wK5m">
                                <property role="Xl_RC" value="Generic" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="76MUimxuXtU" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="76MUimxuXtV" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="34oBXx" id="76MUimxuXtW" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="76MUimxuXtX" role="3cqZAp">
          <node concept="2OqwBi" id="76MUimxuXtY" role="3clFbG">
            <node concept="37vLTw" id="76MUimxuXtZ" role="2Oq$k0">
              <ref role="3cqZAo" node="76MUimxuXmb" resolve="pane" />
            </node>
            <node concept="liA8E" id="76MUimxuXu0" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="10Nm6u" id="76MUimxuXu1" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="76MUimxuXu2" role="3cqZAp">
          <node concept="2OqwBi" id="76MUimxuXu3" role="3clFbG">
            <node concept="37vLTw" id="76MUimxuXu4" role="2Oq$k0">
              <ref role="3cqZAo" node="76MUimxuXmb" resolve="pane" />
            </node>
            <node concept="liA8E" id="76MUimxuXu5" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setPreferredSize(java.awt.Dimension):void" resolve="setPreferredSize" />
              <node concept="2ShNRf" id="76MUimxuXu6" role="37wK5m">
                <node concept="1pGfFk" id="76MUimxuXu7" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                  <node concept="17qRlL" id="76MUimxuXu8" role="37wK5m">
                    <node concept="1eOMI4" id="76MUimxuXu9" role="3uHU7B">
                      <node concept="3cpWs3" id="76MUimxuXua" role="1eOMHV">
                        <node concept="3cmrfG" id="76MUimxuXub" role="3uHU7w">
                          <property role="3cmrfH" value="2" />
                        </node>
                        <node concept="37vLTw" id="76MUimxuXuc" role="3uHU7B">
                          <ref role="3cqZAo" node="76MUimxuXlS" resolve="width" />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="76MUimxuXud" role="3uHU7w">
                      <ref role="3cqZAo" node="76MUimxuXtC" resolve="intentionCount" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="76MUimxuXue" role="37wK5m">
                    <ref role="3cqZAo" node="76MUimxuXlW" resolve="maxY" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="76MUimxuXuf" role="3cqZAp">
          <node concept="2OqwBi" id="76MUimxuXug" role="3clFbG">
            <node concept="37vLTw" id="76MUimxuXuh" role="2Oq$k0">
              <ref role="3cqZAo" node="76MUimxuXmb" resolve="pane" />
            </node>
            <node concept="liA8E" id="76MUimxuXui" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Component.setSize(int,int):void" resolve="setSize" />
              <node concept="17qRlL" id="76MUimxuXuj" role="37wK5m">
                <node concept="1eOMI4" id="76MUimxuXuk" role="3uHU7B">
                  <node concept="3cpWs3" id="76MUimxuXul" role="1eOMHV">
                    <node concept="3cmrfG" id="76MUimxuXum" role="3uHU7w">
                      <property role="3cmrfH" value="2" />
                    </node>
                    <node concept="37vLTw" id="76MUimxuXun" role="3uHU7B">
                      <ref role="3cqZAo" node="76MUimxuXlS" resolve="width" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="76MUimxuXuo" role="3uHU7w">
                  <ref role="3cqZAo" node="76MUimxuXtC" resolve="intentionCount" />
                </node>
              </node>
              <node concept="37vLTw" id="76MUimxuXup" role="37wK5m">
                <ref role="3cqZAo" node="76MUimxuXlW" resolve="maxY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="76MUimxuXuq" role="3cqZAp">
          <node concept="2OqwBi" id="76MUimxuXur" role="3clFbG">
            <node concept="37vLTw" id="76MUimxuXus" role="2Oq$k0">
              <ref role="3cqZAo" node="76MUimxuXmb" resolve="pane" />
            </node>
            <node concept="liA8E" id="76MUimxuXut" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Component.setBounds(int,int,int,int):void" resolve="setBounds" />
              <node concept="3cmrfG" id="76MUimxuXuu" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3cmrfG" id="76MUimxuXuv" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="17qRlL" id="76MUimxuXuw" role="37wK5m">
                <node concept="37vLTw" id="76MUimxuXux" role="3uHU7w">
                  <ref role="3cqZAo" node="76MUimxuXtC" resolve="intentionCount" />
                </node>
                <node concept="1eOMI4" id="76MUimxuXuy" role="3uHU7B">
                  <node concept="3cpWs3" id="76MUimxuXuz" role="1eOMHV">
                    <node concept="37vLTw" id="76MUimxuXu$" role="3uHU7B">
                      <ref role="3cqZAo" node="76MUimxuXlS" resolve="width" />
                    </node>
                    <node concept="3cmrfG" id="76MUimxuXu_" role="3uHU7w">
                      <property role="3cmrfH" value="2" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="76MUimxuXuA" role="37wK5m">
                <ref role="3cqZAo" node="76MUimxuXlW" resolve="maxY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="76MUimxuXuB" role="3cqZAp" />
        <node concept="3cpWs6" id="76MUimxuXuC" role="3cqZAp">
          <node concept="37vLTw" id="76MUimxuXuD" role="3cqZAk">
            <ref role="3cqZAo" node="76MUimxuXmb" resolve="pane" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="76MUimxuWtE" role="1B3o_S" />
      <node concept="3uibUv" id="76MUimxuWQ3" role="3clF45">
        <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
      </node>
    </node>
    <node concept="3Tm1VV" id="5wTmjWA3CRh" role="1B3o_S" />
  </node>
  <node concept="Qs71p" id="5XbSbOFTd8E">
    <property role="TrG5h" value="IfdefBranch" />
    <node concept="3Tm1VV" id="5XbSbOFTd8F" role="1B3o_S" />
    <node concept="QsSxf" id="5XbSbOFTdc$" role="Qtgdg">
      <property role="TrG5h" value="TRUE_BRANCH" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
    </node>
    <node concept="QsSxf" id="5XbSbOFTdd$" role="Qtgdg">
      <property role="TrG5h" value="ELSE_BRANCH" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
    </node>
  </node>
  <node concept="Qs71p" id="5XbSbOFV6mO">
    <property role="TrG5h" value="IntentionType" />
    <node concept="2tJIrI" id="5XbSbOFYSe8" role="jymVt" />
    <node concept="312cEg" id="5XbSbOFYTCy" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="color" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm1VV" id="5XbSbOFZ8cT" role="1B3o_S" />
      <node concept="3uibUv" id="5XbSbOFYTBD" role="1tU5fm">
        <ref role="3uigEE" to="z60i:~Color" resolve="Color" />
      </node>
    </node>
    <node concept="312cEg" id="50KMJClfiOk" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="presentationName" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm1VV" id="50KMJClfieB" role="1B3o_S" />
      <node concept="17QB3L" id="50KMJClfiOi" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="5XbSbOFYYgh" role="jymVt" />
    <node concept="3clFbW" id="5XbSbOFYY1l" role="jymVt">
      <node concept="3cqZAl" id="5XbSbOFYY1m" role="3clF45" />
      <node concept="3clFbS" id="5XbSbOFYY1o" role="3clF47">
        <node concept="3clFbF" id="5XbSbOFYZ7G" role="3cqZAp">
          <node concept="37vLTI" id="5XbSbOFZ0CH" role="3clFbG">
            <node concept="37vLTw" id="5XbSbOFZ17j" role="37vLTx">
              <ref role="3cqZAo" node="5XbSbOFYY$y" resolve="color" />
            </node>
            <node concept="2OqwBi" id="5XbSbOFYZrS" role="37vLTJ">
              <node concept="Xjq3P" id="5XbSbOFYZ7F" role="2Oq$k0" />
              <node concept="2OwXpG" id="5XbSbOFYZRj" role="2OqNvi">
                <ref role="2Oxat5" node="5XbSbOFYTCy" resolve="color" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="50KMJClfjrS" role="3cqZAp">
          <node concept="37vLTI" id="50KMJClflwW" role="3clFbG">
            <node concept="37vLTw" id="50KMJClfmpu" role="37vLTx">
              <ref role="3cqZAo" node="50KMJClfhHS" resolve="presentation" />
            </node>
            <node concept="2OqwBi" id="50KMJClfjL2" role="37vLTJ">
              <node concept="Xjq3P" id="50KMJClfjrQ" role="2Oq$k0" />
              <node concept="2OwXpG" id="50KMJClfkBr" role="2OqNvi">
                <ref role="2Oxat5" node="50KMJClfiOk" resolve="presentationName" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="5XbSbOFYY$y" role="3clF46">
        <property role="TrG5h" value="color" />
        <node concept="3uibUv" id="5XbSbOFYY$x" role="1tU5fm">
          <ref role="3uigEE" to="z60i:~Color" resolve="Color" />
        </node>
      </node>
      <node concept="37vLTG" id="50KMJClfhHS" role="3clF46">
        <property role="TrG5h" value="presentation" />
        <node concept="17QB3L" id="50KMJClfhSv" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="5XbSbOFYXPF" role="jymVt" />
    <node concept="2YIFZL" id="5XbSbOFVHhB" role="jymVt">
      <property role="TrG5h" value="getIntention" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5XbSbOFVHhD" role="3clF47">
        <node concept="3clFbJ" id="5XbSbOFVHhE" role="3cqZAp">
          <node concept="3clFbS" id="5XbSbOFVHhF" role="3clFbx">
            <node concept="3cpWs6" id="5XbSbOFVHhG" role="3cqZAp">
              <node concept="Rm8GO" id="5XbSbOFZ86f" role="3cqZAk">
                <ref role="Rm8GQ" node="5XbSbOFZ801" resolve="KEEP" />
                <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
              </node>
            </node>
          </node>
          <node concept="17R0WA" id="5XbSbOFVHhI" role="3clFbw">
            <node concept="Xl_RD" id="5XbSbOFVHhJ" role="3uHU7w">
              <property role="Xl_RC" value="Keep" />
            </node>
            <node concept="37vLTw" id="5XbSbOFVHhK" role="3uHU7B">
              <ref role="3cqZAo" node="5XbSbOFVHir" resolve="name" />
            </node>
          </node>
          <node concept="3eNFk2" id="5XbSbOFVHhL" role="3eNLev">
            <node concept="3clFbS" id="5XbSbOFVHhM" role="3eOfB_">
              <node concept="3cpWs6" id="5XbSbOFVHhN" role="3cqZAp">
                <node concept="Rm8GO" id="5XbSbOFVHhO" role="3cqZAk">
                  <ref role="Rm8GQ" node="5XbSbOFV6O9" resolve="KEEP_AS_FEATURE" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
            </node>
            <node concept="17R0WA" id="5XbSbOFVHhP" role="3eO9$A">
              <node concept="Xl_RD" id="5XbSbOFVHhQ" role="3uHU7w">
                <property role="Xl_RC" value="KeepAsFeature" />
              </node>
              <node concept="37vLTw" id="5XbSbOFVHhR" role="3uHU7B">
                <ref role="3cqZAo" node="5XbSbOFVHir" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="5XbSbOFVHhS" role="3eNLev">
            <node concept="17R0WA" id="5XbSbOFVHhT" role="3eO9$A">
              <node concept="Xl_RD" id="5XbSbOFVHhU" role="3uHU7w">
                <property role="Xl_RC" value="Remove" />
              </node>
              <node concept="37vLTw" id="5XbSbOFVHhV" role="3uHU7B">
                <ref role="3cqZAo" node="5XbSbOFVHir" resolve="name" />
              </node>
            </node>
            <node concept="3clFbS" id="5XbSbOFVHhW" role="3eOfB_">
              <node concept="3cpWs6" id="5XbSbOFVHhX" role="3cqZAp">
                <node concept="Rm8GO" id="5XbSbOFVHhY" role="3cqZAk">
                  <ref role="Rm8GQ" node="5XbSbOFV6oU" resolve="REMOVE" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="5XbSbOFVHhZ" role="3eNLev">
            <node concept="3clFbS" id="5XbSbOFVHi0" role="3eOfB_">
              <node concept="3cpWs6" id="5XbSbOFVHi1" role="3cqZAp">
                <node concept="Rm8GO" id="5XbSbOFVHi2" role="3cqZAk">
                  <ref role="Rm8GQ" node="5XbSbOFV6T8" resolve="CHANGE_PC" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
            </node>
            <node concept="17R0WA" id="5XbSbOFVHi3" role="3eO9$A">
              <node concept="Xl_RD" id="5XbSbOFVHi4" role="3uHU7w">
                <property role="Xl_RC" value="ChangePC" />
              </node>
              <node concept="37vLTw" id="5XbSbOFVHi5" role="3uHU7B">
                <ref role="3cqZAo" node="5XbSbOFVHir" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="5XbSbOFVHi6" role="3eNLev">
            <node concept="17R0WA" id="5XbSbOFVHi7" role="3eO9$A">
              <node concept="Xl_RD" id="5XbSbOFVHi8" role="3uHU7w">
                <property role="Xl_RC" value="Postpone" />
              </node>
              <node concept="37vLTw" id="5XbSbOFVHi9" role="3uHU7B">
                <ref role="3cqZAo" node="5XbSbOFVHir" resolve="name" />
              </node>
            </node>
            <node concept="3clFbS" id="5XbSbOFVHia" role="3eOfB_">
              <node concept="3cpWs6" id="5XbSbOFVHib" role="3cqZAp">
                <node concept="Rm8GO" id="5XbSbOFVHic" role="3cqZAk">
                  <ref role="Rm8GQ" node="5XbSbOFVqpJ" resolve="POSTPONE" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="5XbSbOFVHid" role="3eNLev">
            <node concept="17R0WA" id="5XbSbOFVHie" role="3eO9$A">
              <node concept="Xl_RD" id="5XbSbOFVHif" role="3uHU7w">
                <property role="Xl_RC" value="Exclusive" />
              </node>
              <node concept="37vLTw" id="5XbSbOFVHig" role="3uHU7B">
                <ref role="3cqZAo" node="5XbSbOFVHir" resolve="name" />
              </node>
            </node>
            <node concept="3clFbS" id="5XbSbOFVHih" role="3eOfB_">
              <node concept="3cpWs6" id="5XbSbOFVHii" role="3cqZAp">
                <node concept="Rm8GO" id="5XbSbOFVHij" role="3cqZAk">
                  <ref role="Rm8GQ" node="5XbSbOFVqD6" resolve="EXCLUSIVE" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="5XbSbOFVHik" role="9aQIa">
            <node concept="3clFbS" id="5XbSbOFVHil" role="9aQI4">
              <node concept="3cpWs6" id="5XbSbOFVHim" role="3cqZAp">
                <node concept="Rm8GO" id="5XbSbOFVHin" role="3cqZAk">
                  <ref role="Rm8GQ" node="5XbSbOFVDQL" resolve="ORDER" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5XbSbOFVHio" role="3cqZAp" />
      </node>
      <node concept="3uibUv" id="5XbSbOFVHiq" role="3clF45">
        <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
      </node>
      <node concept="37vLTG" id="5XbSbOFVHir" role="3clF46">
        <property role="TrG5h" value="name" />
        <node concept="17QB3L" id="5XbSbOFVHis" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="5XbSbOFVHip" role="1B3o_S" />
    </node>
    <node concept="3Tm1VV" id="5XbSbOFV6mP" role="1B3o_S" />
    <node concept="QsSxf" id="5XbSbOFZ801" role="Qtgdg">
      <property role="TrG5h" value="KEEP" />
      <ref role="37wK5l" node="5XbSbOFYY1l" resolve="IntentionType" />
      <node concept="2ShNRf" id="5XbSbOFYWqs" role="37wK5m">
        <node concept="1pGfFk" id="5XbSbOFYWqt" role="2ShVmc">
          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
          <node concept="3cmrfG" id="5XbSbOFYWqu" role="37wK5m">
            <property role="3cmrfH" value="120" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFYWqv" role="37wK5m">
            <property role="3cmrfH" value="217" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFYWqw" role="37wK5m">
            <property role="3cmrfH" value="103" />
          </node>
        </node>
      </node>
      <node concept="Xl_RD" id="50KMJClfmU7" role="37wK5m">
        <property role="Xl_RC" value="Keep" />
      </node>
    </node>
    <node concept="QsSxf" id="5XbSbOFV6oU" role="Qtgdg">
      <property role="TrG5h" value="REMOVE" />
      <ref role="37wK5l" node="5XbSbOFYY1l" resolve="IntentionType" />
      <node concept="2ShNRf" id="5XbSbOFZ2Ko" role="37wK5m">
        <node concept="1pGfFk" id="5XbSbOFZ2Kp" role="2ShVmc">
          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
          <node concept="3cmrfG" id="5XbSbOFZ2Kq" role="37wK5m">
            <property role="3cmrfH" value="178" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Kr" role="37wK5m">
            <property role="3cmrfH" value="178" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Ks" role="37wK5m">
            <property role="3cmrfH" value="178" />
          </node>
        </node>
      </node>
      <node concept="Xl_RD" id="50KMJClfnnR" role="37wK5m">
        <property role="Xl_RC" value="Remove" />
      </node>
    </node>
    <node concept="QsSxf" id="5XbSbOFV6O9" role="Qtgdg">
      <property role="TrG5h" value="KEEP_AS_FEATURE" />
      <ref role="37wK5l" node="5XbSbOFYY1l" resolve="IntentionType" />
      <node concept="2ShNRf" id="5XbSbOFZ2B7" role="37wK5m">
        <node concept="1pGfFk" id="5XbSbOFZ2B8" role="2ShVmc">
          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
          <node concept="3cmrfG" id="5XbSbOFZ2B9" role="37wK5m">
            <property role="3cmrfH" value="209" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Ba" role="37wK5m">
            <property role="3cmrfH" value="230" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Bb" role="37wK5m">
            <property role="3cmrfH" value="98" />
          </node>
        </node>
      </node>
      <node concept="Xl_RD" id="50KMJClfnPB" role="37wK5m">
        <property role="Xl_RC" value="KeepAsFeature" />
      </node>
    </node>
    <node concept="QsSxf" id="5XbSbOFV6T8" role="Qtgdg">
      <property role="TrG5h" value="CHANGE_PC" />
      <ref role="37wK5l" node="5XbSbOFYY1l" resolve="IntentionType" />
      <node concept="2ShNRf" id="5XbSbOFZ2Q5" role="37wK5m">
        <node concept="1pGfFk" id="5XbSbOFZ2Q6" role="2ShVmc">
          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
          <node concept="3cmrfG" id="5XbSbOFZ2Q7" role="37wK5m">
            <property role="3cmrfH" value="55" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Q8" role="37wK5m">
            <property role="3cmrfH" value="129" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Q9" role="37wK5m">
            <property role="3cmrfH" value="190" />
          </node>
        </node>
      </node>
      <node concept="Xl_RD" id="50KMJClfojq" role="37wK5m">
        <property role="Xl_RC" value="AssignFeature" />
      </node>
    </node>
    <node concept="QsSxf" id="5XbSbOFVqpJ" role="Qtgdg">
      <property role="TrG5h" value="POSTPONE" />
      <ref role="37wK5l" node="5XbSbOFYY1l" resolve="IntentionType" />
      <node concept="2ShNRf" id="5XbSbOFZ2Vh" role="37wK5m">
        <node concept="1pGfFk" id="5XbSbOFZ2Vi" role="2ShVmc">
          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
          <node concept="3cmrfG" id="5XbSbOFZ2Vj" role="37wK5m">
            <property role="3cmrfH" value="219" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Vk" role="37wK5m">
            <property role="3cmrfH" value="186" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ2Vl" role="37wK5m">
            <property role="3cmrfH" value="163" />
          </node>
        </node>
      </node>
      <node concept="Xl_RD" id="50KMJClfoLj" role="37wK5m">
        <property role="Xl_RC" value="Postpone" />
      </node>
    </node>
    <node concept="QsSxf" id="5XbSbOFVqD6" role="Qtgdg">
      <property role="TrG5h" value="EXCLUSIVE" />
      <ref role="37wK5l" node="5XbSbOFYY1l" resolve="IntentionType" />
      <node concept="2ShNRf" id="5XbSbOFZ31l" role="37wK5m">
        <node concept="1pGfFk" id="5XbSbOFZ31m" role="2ShVmc">
          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
          <node concept="3cmrfG" id="5XbSbOFZ31n" role="37wK5m">
            <property role="3cmrfH" value="250" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ31o" role="37wK5m">
            <property role="3cmrfH" value="220" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ31p" role="37wK5m">
            <property role="3cmrfH" value="74" />
          </node>
        </node>
      </node>
      <node concept="Xl_RD" id="50KMJClfpf3" role="37wK5m">
        <property role="Xl_RC" value="Exclusive" />
      </node>
    </node>
    <node concept="QsSxf" id="5XbSbOFVDQL" role="Qtgdg">
      <property role="TrG5h" value="ORDER" />
      <ref role="37wK5l" node="5XbSbOFYY1l" resolve="IntentionType" />
      <node concept="2ShNRf" id="5XbSbOFZ3wT" role="37wK5m">
        <node concept="1pGfFk" id="5XbSbOFZ51F" role="2ShVmc">
          <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
          <node concept="3cmrfG" id="5XbSbOFZ5AA" role="37wK5m">
            <property role="3cmrfH" value="253" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ60u" role="37wK5m">
            <property role="3cmrfH" value="182" />
          </node>
          <node concept="3cmrfG" id="5XbSbOFZ6NV" role="37wK5m">
            <property role="3cmrfH" value="62" />
          </node>
        </node>
      </node>
      <node concept="Xl_RD" id="50KMJClfpGQ" role="37wK5m">
        <property role="Xl_RC" value="Order" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5Z3d8P6XHFp">
    <property role="TrG5h" value="ViewIndicatorGraphic" />
    <node concept="2tJIrI" id="5Z3d8P6XHFq" role="jymVt" />
    <node concept="312cEg" id="5Z3d8P6XHFr" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="yOffset" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P6XHFs" role="1B3o_S" />
      <node concept="10Oyi0" id="5Z3d8P6XHFt" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="5Z3d8P6XHF$" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="width" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P6XHF_" role="1B3o_S" />
      <node concept="10Oyi0" id="5Z3d8P6XHFA" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="5Z3d8P6XHFB" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="intentionType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P6XHFC" role="1B3o_S" />
      <node concept="3uibUv" id="5Z3d8P6XHFD" role="1tU5fm">
        <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
      </node>
    </node>
    <node concept="312cEg" id="5Z3d8P7mtrM" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="editorContext" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P7msTH" role="1B3o_S" />
      <node concept="3uibUv" id="5Z3d8P7mtpI" role="1tU5fm">
        <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
      </node>
    </node>
    <node concept="312cEg" id="5Z3d8P7muvh" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="rootNode" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P7mu0l" role="1B3o_S" />
      <node concept="3Tqbb2" id="5Z3d8P7muth" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="5Z3d8P7urwx" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="graphics" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P7uqNC" role="1B3o_S" />
      <node concept="3uibUv" id="5Z3d8P7urwv" role="1tU5fm">
        <ref role="3uigEE" to="z60i:~Graphics" resolve="Graphics" />
      </node>
    </node>
    <node concept="312cEg" id="5Z3d8P7_3FE" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="height" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P7_2VS" role="1B3o_S" />
      <node concept="10Oyi0" id="5Z3d8P7_3FC" role="1tU5fm" />
      <node concept="3cmrfG" id="5Z3d8P7_4F2" role="33vP2m">
        <property role="3cmrfH" value="-1" />
      </node>
    </node>
    <node concept="312cEg" id="5Z3d8P9590U" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="text" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="5Z3d8P958ad" role="1B3o_S" />
      <node concept="17QB3L" id="5Z3d8P958YU" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="5Z3d8P957lz" role="jymVt" />
    <node concept="3Tm1VV" id="5Z3d8P6XHFF" role="1B3o_S" />
    <node concept="3uibUv" id="5Z3d8P6XHFG" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
    </node>
    <node concept="3clFbW" id="5Z3d8P6XHFH" role="jymVt">
      <node concept="3cqZAl" id="5Z3d8P6XHFI" role="3clF45" />
      <node concept="3clFbS" id="5Z3d8P6XHFJ" role="3clF47">
        <node concept="3clFbF" id="5Z3d8P6XHFK" role="3cqZAp">
          <node concept="37vLTI" id="5Z3d8P6XHFL" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P6XHFM" role="37vLTx">
              <ref role="3cqZAo" node="5Z3d8P6XHGn" resolve="yOffset" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P6XHFN" role="37vLTJ">
              <node concept="Xjq3P" id="5Z3d8P6XHFO" role="2Oq$k0" />
              <node concept="2OwXpG" id="5Z3d8P6XHFP" role="2OqNvi">
                <ref role="2Oxat5" node="5Z3d8P6XHFr" resolve="yOffset" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P6XHG2" role="3cqZAp">
          <node concept="37vLTI" id="5Z3d8P6XHG3" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P6XHG4" role="37vLTx">
              <ref role="3cqZAo" node="5Z3d8P6XHGl" resolve="width" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P6XHG5" role="37vLTJ">
              <node concept="Xjq3P" id="5Z3d8P6XHG6" role="2Oq$k0" />
              <node concept="2OwXpG" id="5Z3d8P6XHG7" role="2OqNvi">
                <ref role="2Oxat5" node="5Z3d8P6XHF$" resolve="width" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P6XHG8" role="3cqZAp">
          <node concept="37vLTI" id="5Z3d8P6XHG9" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P6XHGa" role="37vLTx">
              <ref role="3cqZAo" node="5Z3d8P6XHGp" resolve="intentionType" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P6XHGb" role="37vLTJ">
              <node concept="Xjq3P" id="5Z3d8P6XHGc" role="2Oq$k0" />
              <node concept="2OwXpG" id="5Z3d8P6XHGd" role="2OqNvi">
                <ref role="2Oxat5" node="5Z3d8P6XHFB" resolve="intentionType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P7mvjV" role="3cqZAp">
          <node concept="37vLTI" id="5Z3d8P7my0L" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P7myMM" role="37vLTx">
              <ref role="3cqZAo" node="5Z3d8P7mrq8" resolve="editorContext" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P7mvO4" role="37vLTJ">
              <node concept="Xjq3P" id="5Z3d8P7mvjT" role="2Oq$k0" />
              <node concept="2OwXpG" id="5Z3d8P7mx9i" role="2OqNvi">
                <ref role="2Oxat5" node="5Z3d8P7mtrM" resolve="editorContext" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P95b5s" role="3cqZAp">
          <node concept="37vLTI" id="5Z3d8P95dgN" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P95dCV" role="37vLTx">
              <ref role="3cqZAo" node="5Z3d8P95a9m" resolve="text" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P95bBF" role="37vLTJ">
              <node concept="Xjq3P" id="5Z3d8P95b5q" role="2Oq$k0" />
              <node concept="2OwXpG" id="5Z3d8P95ciW" role="2OqNvi">
                <ref role="2Oxat5" node="5Z3d8P9590U" resolve="text" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P7mzqR" role="3cqZAp">
          <node concept="37vLTI" id="5Z3d8P7mAaO" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P7mAXa" role="37vLTx">
              <ref role="3cqZAo" node="5Z3d8P7mrMA" resolve="rootNode" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P7mzVX" role="37vLTJ">
              <node concept="Xjq3P" id="5Z3d8P7mzqP" role="2Oq$k0" />
              <node concept="2OwXpG" id="5Z3d8P7m_hj" role="2OqNvi">
                <ref role="2Oxat5" node="5Z3d8P7muvh" resolve="rootNode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P6XHGe" role="3cqZAp">
          <node concept="1rXfSq" id="5Z3d8P6XHGf" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JComponent.setPreferredSize(java.awt.Dimension):void" resolve="setPreferredSize" />
            <node concept="2ShNRf" id="5Z3d8P6XHGg" role="37wK5m">
              <node concept="1pGfFk" id="5Z3d8P6XHGh" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                <node concept="37vLTw" id="5Z3d8P6XHGi" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P6XHGl" resolve="width" />
                </node>
                <node concept="37vLTw" id="5Z3d8P6XHGj" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P6XHGn" resolve="yOffset" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5Z3d8P6XHGk" role="1B3o_S" />
      <node concept="37vLTG" id="5Z3d8P6XHGl" role="3clF46">
        <property role="TrG5h" value="width" />
        <node concept="10Oyi0" id="5Z3d8P6XHGm" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5Z3d8P6XHGn" role="3clF46">
        <property role="TrG5h" value="yOffset" />
        <node concept="10Oyi0" id="5Z3d8P6XHGo" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5Z3d8P6XHGp" role="3clF46">
        <property role="TrG5h" value="intentionType" />
        <node concept="3uibUv" id="5Z3d8P6XHGq" role="1tU5fm">
          <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
        </node>
      </node>
      <node concept="37vLTG" id="5Z3d8P7mrq8" role="3clF46">
        <property role="TrG5h" value="editorContext" />
        <node concept="3uibUv" id="5Z3d8P7mrMk" role="1tU5fm">
          <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
        </node>
      </node>
      <node concept="37vLTG" id="5Z3d8P7mrMA" role="3clF46">
        <property role="TrG5h" value="rootNode" />
        <node concept="3Tqbb2" id="5Z3d8P7msb9" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5Z3d8P95a9m" role="3clF46">
        <property role="TrG5h" value="text" />
        <node concept="17QB3L" id="5Z3d8P95asI" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="5Z3d8P6XHGv" role="jymVt" />
    <node concept="3clFb_" id="5Z3d8P6XHGw" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="paintComponent" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tmbuc" id="5Z3d8P6XHGx" role="1B3o_S" />
      <node concept="3cqZAl" id="5Z3d8P6XHGy" role="3clF45" />
      <node concept="37vLTG" id="5Z3d8P6XHGz" role="3clF46">
        <property role="TrG5h" value="graphics" />
        <node concept="3uibUv" id="5Z3d8P6XHG$" role="1tU5fm">
          <ref role="3uigEE" to="z60i:~Graphics" resolve="Graphics" />
        </node>
      </node>
      <node concept="3clFbS" id="5Z3d8P6XHG_" role="3clF47">
        <node concept="3clFbF" id="5Z3d8P6XHGA" role="3cqZAp">
          <node concept="3nyPlj" id="5Z3d8P6XHGB" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JComponent.paintComponent(java.awt.Graphics):void" resolve="paintComponent" />
            <node concept="37vLTw" id="5Z3d8P6XHGC" role="37wK5m">
              <ref role="3cqZAo" node="5Z3d8P6XHGz" resolve="graphics" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8Z70C" role="3cqZAp" />
        <node concept="3cpWs8" id="5Z3d8P8Z7Rx" role="3cqZAp">
          <node concept="3cpWsn" id="5Z3d8P8Z7R$" role="3cpWs9">
            <property role="TrG5h" value="textureHeight" />
            <node concept="10Oyi0" id="5Z3d8P8Z7Rv" role="1tU5fm" />
            <node concept="3cmrfG" id="5Z3d8P8Z8AS" role="33vP2m">
              <property role="3cmrfH" value="200" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8ea1s" role="3cqZAp" />
        <node concept="3cpWs8" id="5Z3d8P8ednF" role="3cqZAp">
          <node concept="3cpWsn" id="5Z3d8P8ednG" role="3cpWs9">
            <property role="TrG5h" value="bufferedImage" />
            <node concept="3uibUv" id="5Z3d8P8ednH" role="1tU5fm">
              <ref role="3uigEE" to="jan3:~BufferedImage" resolve="BufferedImage" />
            </node>
            <node concept="2ShNRf" id="5Z3d8P8ee1h" role="33vP2m">
              <node concept="1pGfFk" id="5Z3d8P8eeFE" role="2ShVmc">
                <ref role="37wK5l" to="jan3:~BufferedImage.&lt;init&gt;(int,int,int)" resolve="BufferedImage" />
                <node concept="37vLTw" id="5Z3d8P8OxMy" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P6XHF$" resolve="width" />
                </node>
                <node concept="37vLTw" id="5Z3d8P8Z8Yb" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P8Z7R$" resolve="textureHeight" />
                </node>
                <node concept="10M0yZ" id="5Z3d8P8egUy" role="37wK5m">
                  <ref role="3cqZAo" to="jan3:~BufferedImage.TYPE_INT_RGB" resolve="TYPE_INT_RGB" />
                  <ref role="1PxDUh" to="jan3:~BufferedImage" resolve="BufferedImage" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5Z3d8P8eiYq" role="3cqZAp">
          <node concept="3cpWsn" id="5Z3d8P8eiYr" role="3cpWs9">
            <property role="TrG5h" value="imageGraphics" />
            <node concept="3uibUv" id="5Z3d8P8eiYs" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Graphics2D" resolve="Graphics2D" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P8ek$S" role="33vP2m">
              <node concept="37vLTw" id="5Z3d8P8ejYg" role="2Oq$k0">
                <ref role="3cqZAo" node="5Z3d8P8ednG" resolve="bufferedImage" />
              </node>
              <node concept="liA8E" id="5Z3d8P8elbD" role="2OqNvi">
                <ref role="37wK5l" to="jan3:~BufferedImage.createGraphics():java.awt.Graphics2D" resolve="createGraphics" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8egVm" role="3cqZAp" />
        <node concept="3clFbF" id="5Z3d8P8eo03" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P8eoKZ" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P8eo01" role="2Oq$k0">
              <ref role="3cqZAo" node="5Z3d8P8eiYr" resolve="imageGraphics" />
            </node>
            <node concept="liA8E" id="5Z3d8P8epjr" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.setColor(java.awt.Color):void" resolve="setColor" />
              <node concept="2ShNRf" id="5Z3d8P8epls" role="37wK5m">
                <node concept="1pGfFk" id="5Z3d8P8eplt" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Color.&lt;init&gt;(int,int,int)" resolve="Color" />
                  <node concept="3cmrfG" id="5Z3d8P8eqn2" role="37wK5m">
                    <property role="3cmrfH" value="219" />
                  </node>
                  <node concept="3cmrfG" id="5Z3d8P8eplv" role="37wK5m">
                    <property role="3cmrfH" value="219" />
                  </node>
                  <node concept="3cmrfG" id="5Z3d8P8FdLZ" role="37wK5m">
                    <property role="3cmrfH" value="219" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P8erBc" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P8esoT" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P8erBa" role="2Oq$k0">
              <ref role="3cqZAo" node="5Z3d8P8eiYr" resolve="imageGraphics" />
            </node>
            <node concept="liA8E" id="5Z3d8P8esVV" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.fillRect(int,int,int,int):void" resolve="fillRect" />
              <node concept="3cmrfG" id="5Z3d8P8etkD" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3cmrfG" id="5Z3d8P8euvv" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="5Z3d8P8CciQ" role="37wK5m">
                <ref role="3cqZAo" node="5Z3d8P6XHF$" resolve="width" />
              </node>
              <node concept="37vLTw" id="5Z3d8P8Z9DM" role="37wK5m">
                <ref role="3cqZAo" node="5Z3d8P8Z7R$" resolve="textureHeight" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8kS_d" role="3cqZAp" />
        <node concept="3clFbF" id="5Z3d8P8m457" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P8m458" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P8m45c" role="2Oq$k0">
              <ref role="3cqZAo" node="5Z3d8P8eiYr" resolve="imageGraphics" />
            </node>
            <node concept="liA8E" id="5Z3d8P8m45d" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics2D.rotate(double,double,double):void" resolve="rotate" />
              <node concept="FJ1c_" id="5Z3d8P8m45e" role="37wK5m">
                <node concept="3cmrfG" id="5Z3d8P8m45f" role="3uHU7w">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="1ZRNhn" id="5Z3d8P8m45g" role="3uHU7B">
                  <node concept="10M0yZ" id="5Z3d8P8m45h" role="2$L3a6">
                    <ref role="1PxDUh" to="wyt6:~Math" resolve="Math" />
                    <ref role="3cqZAo" to="wyt6:~Math.PI" resolve="PI" />
                  </node>
                </node>
              </node>
              <node concept="3cmrfG" id="5Z3d8P8w$DF" role="37wK5m">
                <property role="3cmrfH" value="12" />
              </node>
              <node concept="3cmrfG" id="5Z3d8P8m45j" role="37wK5m">
                <property role="3cmrfH" value="100" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P8kUMG" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P8kUMH" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P8kWT2" role="2Oq$k0">
              <ref role="3cqZAo" node="5Z3d8P8eiYr" resolve="imageGraphics" />
            </node>
            <node concept="liA8E" id="5Z3d8P8kUMJ" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.setColor(java.awt.Color):void" resolve="setColor" />
              <node concept="10M0yZ" id="5Z3d8P8kUMK" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~Color" resolve="Color" />
                <ref role="3cqZAo" to="z60i:~Color.BLACK" resolve="BLACK" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P8kVbO" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P8kVbP" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P8kXio" role="2Oq$k0">
              <ref role="3cqZAo" node="5Z3d8P8eiYr" resolve="imageGraphics" />
            </node>
            <node concept="liA8E" id="5Z3d8P8kVbR" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics2D.drawString(java.lang.String,int,int):void" resolve="drawString" />
              <node concept="37vLTw" id="5Z3d8P95esy" role="37wK5m">
                <ref role="3cqZAo" node="5Z3d8P9590U" resolve="text" />
              </node>
              <node concept="3cmrfG" id="5Z3d8P8kVbT" role="37wK5m">
                <property role="3cmrfH" value="10" />
              </node>
              <node concept="3cmrfG" id="5Z3d8P8kVbU" role="37wK5m">
                <property role="3cmrfH" value="100" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8kSCM" role="3cqZAp" />
        <node concept="3cpWs8" id="5Z3d8P8ewIk" role="3cqZAp">
          <node concept="3cpWsn" id="5Z3d8P8ewIl" role="3cpWs9">
            <property role="TrG5h" value="r" />
            <node concept="3uibUv" id="5Z3d8P8ewIm" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Rectangle" resolve="Rectangle" />
            </node>
            <node concept="2ShNRf" id="5Z3d8P8exJM" role="33vP2m">
              <node concept="1pGfFk" id="5Z3d8P8expc" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~Rectangle.&lt;init&gt;(int,int,int,int)" resolve="Rectangle" />
                <node concept="3cmrfG" id="5Z3d8P8eylg" role="37wK5m">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="3cmrfG" id="5Z3d8P8ezK3" role="37wK5m">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="37vLTw" id="5Z3d8P8tGuE" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P6XHF$" resolve="width" />
                </node>
                <node concept="37vLTw" id="5Z3d8P8ZatT" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P8Z7R$" resolve="textureHeight" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8GNzA" role="3cqZAp" />
        <node concept="3cpWs8" id="5Z3d8P8eAlO" role="3cqZAp">
          <node concept="3cpWsn" id="5Z3d8P8eAlP" role="3cpWs9">
            <property role="TrG5h" value="tp" />
            <node concept="3uibUv" id="5Z3d8P8eAlQ" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~TexturePaint" resolve="TexturePaint" />
            </node>
            <node concept="2ShNRf" id="5Z3d8P8eB0R" role="33vP2m">
              <node concept="1pGfFk" id="5Z3d8P8eClh" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~TexturePaint.&lt;init&gt;(java.awt.image.BufferedImage,java.awt.geom.Rectangle2D)" resolve="TexturePaint" />
                <node concept="37vLTw" id="5Z3d8P8eD2M" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P8ednG" resolve="bufferedImage" />
                </node>
                <node concept="37vLTw" id="5Z3d8P8eDqt" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P8ewIl" resolve="r" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8e$vK" role="3cqZAp" />
        <node concept="3clFbF" id="5Z3d8P8eJOQ" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P8eL8p" role="3clFbG">
            <node concept="1eOMI4" id="5Z3d8P8eJOO" role="2Oq$k0">
              <node concept="10QFUN" id="5Z3d8P8eJOL" role="1eOMHV">
                <node concept="3uibUv" id="5Z3d8P8eKez" role="10QFUM">
                  <ref role="3uigEE" to="z60i:~Graphics2D" resolve="Graphics2D" />
                </node>
                <node concept="37vLTw" id="5Z3d8P8eKXV" role="10QFUP">
                  <ref role="3cqZAo" node="5Z3d8P6XHGz" resolve="graphics" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="5Z3d8P8eLA5" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics2D.setPaint(java.awt.Paint):void" resolve="setPaint" />
              <node concept="37vLTw" id="5Z3d8P8eLXr" role="37wK5m">
                <ref role="3cqZAo" node="5Z3d8P8eAlP" resolve="tp" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5Z3d8P8jnbj" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P8jnbk" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P8jnbl" role="2Oq$k0">
              <ref role="3cqZAo" node="5Z3d8P6XHGz" resolve="graphics" />
            </node>
            <node concept="liA8E" id="5Z3d8P8jnbm" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.fillRect(int,int,int,int):void" resolve="fillRect" />
              <node concept="3cmrfG" id="5Z3d8P8jnbn" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3cmrfG" id="5Z3d8P8jnbo" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="5Z3d8P8jnbp" role="37wK5m">
                <ref role="3cqZAo" node="5Z3d8P6XHF$" resolve="width" />
              </node>
              <node concept="2YIFZM" id="5Z3d8P8jnbq" role="37wK5m">
                <ref role="1Pybhc" to="wyt6:~Math" resolve="Math" />
                <ref role="37wK5l" to="wyt6:~Math.max(int,int):int" resolve="max" />
                <node concept="37vLTw" id="5Z3d8P8jnbr" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P6XHFr" resolve="yOffset" />
                </node>
                <node concept="37vLTw" id="5Z3d8P8jnbs" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P7_3FE" resolve="height" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P8e$xz" role="3cqZAp" />
        <node concept="3clFbF" id="5Z3d8P7uwu1" role="3cqZAp">
          <node concept="37vLTI" id="5Z3d8P7uzB5" role="3clFbG">
            <node concept="37vLTw" id="5Z3d8P7u$uW" role="37vLTx">
              <ref role="3cqZAo" node="5Z3d8P6XHGz" resolve="graphics" />
            </node>
            <node concept="2OqwBi" id="5Z3d8P7uxjt" role="37vLTJ">
              <node concept="Xjq3P" id="5Z3d8P7uwtZ" role="2Oq$k0" />
              <node concept="2OwXpG" id="5Z3d8P7uyI8" role="2OqNvi">
                <ref role="2Oxat5" node="5Z3d8P7urwx" resolve="graphics" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="5Z3d8P6XHGQ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="5Z3d8P6XHGR" role="jymVt" />
    <node concept="3clFb_" id="5Z3d8P6XHGS" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getColor" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5Z3d8P6XHGT" role="3clF47">
        <node concept="3cpWs6" id="5Z3d8P6XHGU" role="3cqZAp">
          <node concept="2OqwBi" id="5Z3d8P6XHGV" role="3cqZAk">
            <node concept="37vLTw" id="5Z3d8P6XHGW" role="2Oq$k0">
              <ref role="3cqZAo" node="5Z3d8P6XHFB" resolve="intentionType" />
            </node>
            <node concept="2OwXpG" id="5Z3d8P6XHGX" role="2OqNvi">
              <ref role="2Oxat5" node="5XbSbOFYTCy" resolve="color" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5Z3d8P6XHGY" role="1B3o_S" />
      <node concept="3uibUv" id="5Z3d8P6XHGZ" role="3clF45">
        <ref role="3uigEE" to="z60i:~Color" resolve="Color" />
      </node>
    </node>
    <node concept="2tJIrI" id="5Z3d8P6XHH0" role="jymVt" />
    <node concept="3clFb_" id="5Z3d8P78dNJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getPreferredSize" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="5Z3d8P78dNK" role="1B3o_S" />
      <node concept="2AHcQZ" id="5Z3d8P78dNM" role="2AJF6D">
        <ref role="2AI5Lk" to="mnlj:~Transient" resolve="Transient" />
      </node>
      <node concept="3uibUv" id="5Z3d8P78dNN" role="3clF45">
        <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
      </node>
      <node concept="3clFbS" id="5Z3d8P78dNO" role="3clF47">
        <node concept="1X3_iC" id="5Z3d8P954fe" role="lGtFl">
          <property role="3V$3am" value="statement" />
          <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
          <node concept="34ab3g" id="5Z3d8P7kYux" role="8Wnug">
            <property role="35gtTG" value="warn" />
            <node concept="Xl_RD" id="5Z3d8P7kYuz" role="34bqiv">
              <property role="Xl_RC" value="getPreferredSize" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5Z3d8P7obCW" role="3cqZAp">
          <node concept="3cpWsn" id="5Z3d8P7obCX" role="3cpWs9">
            <property role="TrG5h" value="rootCell" />
            <node concept="2OqwBi" id="5Z3d8P7ocgH" role="33vP2m">
              <node concept="2OqwBi" id="5Z3d8P7ocgI" role="2Oq$k0">
                <node concept="37vLTw" id="5Z3d8P7ocgJ" role="2Oq$k0">
                  <ref role="3cqZAo" node="5Z3d8P7mtrM" resolve="editorContext" />
                </node>
                <node concept="liA8E" id="5Z3d8P7ocgK" role="2OqNvi">
                  <ref role="37wK5l" to="cj4x:~EditorContext.getEditorComponent():jetbrains.mps.openapi.editor.EditorComponent" resolve="getEditorComponent" />
                </node>
              </node>
              <node concept="liA8E" id="5Z3d8P7ocgL" role="2OqNvi">
                <ref role="37wK5l" to="cj4x:~EditorComponent.findNodeCell(org.jetbrains.mps.openapi.model.SNode):jetbrains.mps.openapi.editor.cells.EditorCell" resolve="findNodeCell" />
                <node concept="37vLTw" id="5Z3d8P7xZK8" role="37wK5m">
                  <ref role="3cqZAo" node="5Z3d8P7muvh" resolve="rootNode" />
                </node>
              </node>
            </node>
            <node concept="3uibUv" id="5Z3d8P7odz2" role="1tU5fm">
              <ref role="3uigEE" to="f4zo:~EditorCell" resolve="EditorCell" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P7oah1" role="3cqZAp" />
        <node concept="3clFbJ" id="5Z3d8P7mBWw" role="3cqZAp">
          <node concept="3clFbS" id="5Z3d8P7mBWy" role="3clFbx">
            <node concept="1X3_iC" id="5Z3d8P954Yu" role="lGtFl">
              <property role="3V$3am" value="statement" />
              <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
              <node concept="34ab3g" id="5Z3d8P7mFZl" role="8Wnug">
                <property role="35gtTG" value="warn" />
                <node concept="3cpWs3" id="5Z3d8P7pHOU" role="34bqiv">
                  <node concept="2OqwBi" id="5Z3d8P7pJlQ" role="3uHU7w">
                    <node concept="37vLTw" id="5Z3d8P7pIP1" role="2Oq$k0">
                      <ref role="3cqZAo" node="5Z3d8P7obCX" resolve="rootCell" />
                    </node>
                    <node concept="liA8E" id="5Z3d8P7pJBY" role="2OqNvi">
                      <ref role="37wK5l" to="f4zo:~EditorCell.getHeight():int" resolve="getHeight" />
                    </node>
                  </node>
                  <node concept="3cpWs3" id="5Z3d8P7pGiP" role="3uHU7B">
                    <node concept="3cpWs3" id="5Z3d8P7of3J" role="3uHU7B">
                      <node concept="Xl_RD" id="5Z3d8P7oepq" role="3uHU7B">
                        <property role="Xl_RC" value="found node cell getX()=" />
                      </node>
                      <node concept="2OqwBi" id="5Z3d8P7ofV2" role="3uHU7w">
                        <node concept="37vLTw" id="5Z3d8P7ofqM" role="2Oq$k0">
                          <ref role="3cqZAo" node="5Z3d8P7obCX" resolve="rootCell" />
                        </node>
                        <node concept="liA8E" id="5Z3d8P7og2c" role="2OqNvi">
                          <ref role="37wK5l" to="f4zo:~EditorCell.getX():int" resolve="getX" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xl_RD" id="5Z3d8P7pH9R" role="3uHU7w">
                      <property role="Xl_RC" value=" height " />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5Z3d8P7AAQc" role="3cqZAp">
              <node concept="37vLTI" id="5Z3d8P7AD79" role="3clFbG">
                <node concept="37vLTw" id="5Z3d8P7AAQa" role="37vLTJ">
                  <ref role="3cqZAo" node="5Z3d8P7_3FE" resolve="height" />
                </node>
                <node concept="3cpWs3" id="5Z3d8P7rheB" role="37vLTx">
                  <node concept="2OqwBi" id="5Z3d8P7riXH" role="3uHU7B">
                    <node concept="37vLTw" id="5Z3d8P7ri49" role="2Oq$k0">
                      <ref role="3cqZAo" node="5Z3d8P7obCX" resolve="rootCell" />
                    </node>
                    <node concept="liA8E" id="5Z3d8P7rjq7" role="2OqNvi">
                      <ref role="37wK5l" to="f4zo:~EditorCell.getX():int" resolve="getX" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="5Z3d8P7rfVo" role="3uHU7w">
                    <node concept="37vLTw" id="5Z3d8P7rf8D" role="2Oq$k0">
                      <ref role="3cqZAo" node="5Z3d8P7obCX" resolve="rootCell" />
                    </node>
                    <node concept="liA8E" id="5Z3d8P7rgta" role="2OqNvi">
                      <ref role="37wK5l" to="f4zo:~EditorCell.getHeight():int" resolve="getHeight" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="5Z3d8P7utHM" role="3cqZAp">
              <node concept="3clFbS" id="5Z3d8P7utHO" role="3clFbx">
                <node concept="3clFbF" id="5Z3d8P7u$z$" role="3cqZAp">
                  <node concept="2OqwBi" id="5Z3d8P7u$z_" role="3clFbG">
                    <node concept="37vLTw" id="5Z3d8P7u$zA" role="2Oq$k0">
                      <ref role="3cqZAo" node="5Z3d8P7urwx" resolve="graphics" />
                    </node>
                    <node concept="liA8E" id="5Z3d8P7u$zB" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~Graphics.fillRect(int,int,int,int):void" resolve="fillRect" />
                      <node concept="3cmrfG" id="5Z3d8P7u$zC" role="37wK5m">
                        <property role="3cmrfH" value="0" />
                      </node>
                      <node concept="3cmrfG" id="5Z3d8P7u$zD" role="37wK5m">
                        <property role="3cmrfH" value="0" />
                      </node>
                      <node concept="37vLTw" id="5Z3d8P7u$zE" role="37wK5m">
                        <ref role="3cqZAo" node="5Z3d8P6XHF$" resolve="width" />
                      </node>
                      <node concept="37vLTw" id="5Z3d8P7AJCh" role="37wK5m">
                        <ref role="3cqZAo" node="5Z3d8P7_3FE" resolve="height" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3y3z36" id="5Z3d8P7uvuN" role="3clFbw">
                <node concept="10Nm6u" id="5Z3d8P7uvSx" role="3uHU7w" />
                <node concept="37vLTw" id="5Z3d8P7uuYK" role="3uHU7B">
                  <ref role="3cqZAo" node="5Z3d8P7urwx" resolve="graphics" />
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="5Z3d8P7reef" role="3cqZAp">
              <node concept="2ShNRf" id="5Z3d8P7reeg" role="3cqZAk">
                <node concept="1pGfFk" id="5Z3d8P7reeh" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                  <node concept="37vLTw" id="5Z3d8P7reei" role="37wK5m">
                    <ref role="3cqZAo" node="5Z3d8P6XHF$" resolve="width" />
                  </node>
                  <node concept="37vLTw" id="5Z3d8P7AI7M" role="37wK5m">
                    <ref role="3cqZAo" node="5Z3d8P7_3FE" resolve="height" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="5Z3d8P7mETh" role="3clFbw">
            <node concept="10Nm6u" id="5Z3d8P7mFv5" role="3uHU7w" />
            <node concept="37vLTw" id="5Z3d8P7ocLS" role="3uHU7B">
              <ref role="3cqZAo" node="5Z3d8P7obCX" resolve="rootCell" />
            </node>
          </node>
          <node concept="9aQIb" id="5Z3d8P7mGuz" role="9aQIa">
            <node concept="3clFbS" id="5Z3d8P7mGu$" role="9aQI4">
              <node concept="1X3_iC" id="5Z3d8P955UP" role="lGtFl">
                <property role="3V$3am" value="statement" />
                <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                <node concept="34ab3g" id="5Z3d8P7mGYO" role="8Wnug">
                  <property role="35gtTG" value="warn" />
                  <node concept="Xl_RD" id="5Z3d8P7mGYQ" role="34bqiv">
                    <property role="Xl_RC" value="could not find node cell" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5Z3d8P7mB08" role="3cqZAp" />
        <node concept="3cpWs6" id="5Z3d8P78fDW" role="3cqZAp">
          <node concept="2ShNRf" id="5Z3d8P78gdj" role="3cqZAk">
            <node concept="1pGfFk" id="5Z3d8P78hGx" role="2ShVmc">
              <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
              <node concept="37vLTw" id="5Z3d8P78isA" role="37wK5m">
                <ref role="3cqZAo" node="5Z3d8P6XHF$" resolve="width" />
              </node>
              <node concept="37vLTw" id="5Z3d8P7hI_x" role="37wK5m">
                <ref role="3cqZAo" node="5Z3d8P6XHFr" resolve="yOffset" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="5Z3d8P78dNP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="50KMJClf7Sg">
    <property role="TrG5h" value="IntentionInstance" />
    <node concept="312cEg" id="50KMJClf8Cq" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="intentionType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="50KMJClf8zM" role="1B3o_S" />
      <node concept="3uibUv" id="50KMJClf8Ch" role="1tU5fm">
        <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
      </node>
    </node>
    <node concept="312cEg" id="50KMJClf8Ob" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="intention" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="50KMJClf8H6" role="1B3o_S" />
      <node concept="3Tqbb2" id="50KMJClf8O6" role="1tU5fm">
        <ref role="ehGHo" to="4s7a:3LUIgcmYqc7" resolve="Intention" />
      </node>
    </node>
    <node concept="2tJIrI" id="50KMJClf8vv" role="jymVt" />
    <node concept="3clFbW" id="50KMJClf8hc" role="jymVt">
      <node concept="3cqZAl" id="50KMJClf8he" role="3clF45" />
      <node concept="3Tm1VV" id="50KMJClf8hf" role="1B3o_S" />
      <node concept="3clFbS" id="50KMJClf8hg" role="3clF47">
        <node concept="3clFbF" id="50KMJClf8VT" role="3cqZAp">
          <node concept="37vLTI" id="50KMJClf9Sw" role="3clFbG">
            <node concept="37vLTw" id="50KMJClfa6h" role="37vLTx">
              <ref role="3cqZAo" node="50KMJClf8nv" resolve="intention" />
            </node>
            <node concept="2OqwBi" id="50KMJClf91j" role="37vLTJ">
              <node concept="Xjq3P" id="50KMJClf8VS" role="2Oq$k0" />
              <node concept="2OwXpG" id="50KMJClf9ek" role="2OqNvi">
                <ref role="2Oxat5" node="50KMJClf8Ob" resolve="intention" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="50KMJClfagz" role="3cqZAp">
          <node concept="37vLTI" id="50KMJClfbxC" role="3clFbG">
            <node concept="2YIFZM" id="50KMJClfbZ0" role="37vLTx">
              <ref role="37wK5l" node="5XbSbOFVHhB" resolve="getIntention" />
              <ref role="1Pybhc" node="5XbSbOFV6mO" resolve="IntentionType" />
              <node concept="2OqwBi" id="50KMJClfcuO" role="37wK5m">
                <node concept="37vLTw" id="50KMJClfcaR" role="2Oq$k0">
                  <ref role="3cqZAo" node="50KMJClf8nv" resolve="intention" />
                </node>
                <node concept="3TrcHB" id="50KMJClfcNG" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:3ie$Dw3G5cG" resolve="type" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="50KMJClfane" role="37vLTJ">
              <node concept="Xjq3P" id="50KMJClfagx" role="2Oq$k0" />
              <node concept="2OwXpG" id="50KMJClfa$l" role="2OqNvi">
                <ref role="2Oxat5" node="50KMJClf8Cq" resolve="intentionType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="50KMJClf8nv" role="3clF46">
        <property role="TrG5h" value="intention" />
        <node concept="3Tqbb2" id="50KMJClf8rS" role="1tU5fm">
          <ref role="ehGHo" to="4s7a:3LUIgcmYqc7" resolve="Intention" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="50KMJClfd0y" role="jymVt" />
    <node concept="3clFb_" id="50KMJClfdlu" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getTooltip" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="50KMJClfdlx" role="3clF47">
        <node concept="3cpWs8" id="50KMJClfft6" role="3cqZAp">
          <node concept="3cpWsn" id="50KMJClfft9" role="3cpWs9">
            <property role="TrG5h" value="tooltip" />
            <node concept="17QB3L" id="50KMJClfft4" role="1tU5fm" />
            <node concept="2OqwBi" id="50KMJClfsnH" role="33vP2m">
              <node concept="37vLTw" id="50KMJClfrOy" role="2Oq$k0">
                <ref role="3cqZAo" node="50KMJClf8Cq" resolve="intentionType" />
              </node>
              <node concept="2OwXpG" id="50KMJClfti4" role="2OqNvi">
                <ref role="2Oxat5" node="50KMJClfiOk" resolve="presentationName" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="50KMJClftOB" role="3cqZAp" />
        <node concept="3clFbJ" id="50KMJClfud7" role="3cqZAp">
          <node concept="3clFbS" id="50KMJClfud9" role="3clFbx">
            <node concept="3clFbF" id="50KMJClfzSt" role="3cqZAp">
              <node concept="d57v9" id="50KMJClf$eK" role="3clFbG">
                <node concept="3cpWs3" id="50KMJClfD5T" role="37vLTx">
                  <node concept="Xl_RD" id="50KMJClfDeB" role="3uHU7B">
                    <property role="Xl_RC" value=" " />
                  </node>
                  <node concept="2OqwBi" id="50KMJClf$Fg" role="3uHU7w">
                    <node concept="37vLTw" id="50KMJClf$pB" role="2Oq$k0">
                      <ref role="3cqZAo" node="50KMJClf8Ob" resolve="intention" />
                    </node>
                    <node concept="3TrcHB" id="50KMJClf$Of" role="2OqNvi">
                      <ref role="3TsBF5" to="4s7a:3jeRE17nVF2" resolve="featurename" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="50KMJClfzSr" role="37vLTJ">
                  <ref role="3cqZAo" node="50KMJClfft9" resolve="tooltip" />
                </node>
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="50KMJClfygf" role="3clFbw">
            <node concept="17R0WA" id="50KMJClfzhf" role="3uHU7w">
              <node concept="Rm8GO" id="50KMJClfzBo" role="3uHU7w">
                <ref role="Rm8GQ" node="5XbSbOFVqD6" resolve="EXCLUSIVE" />
                <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
              </node>
              <node concept="37vLTw" id="50KMJClfyta" role="3uHU7B">
                <ref role="3cqZAo" node="50KMJClf8Cq" resolve="intentionType" />
              </node>
            </node>
            <node concept="22lmx$" id="50KMJClfw$3" role="3uHU7B">
              <node concept="17R0WA" id="50KMJClfvhv" role="3uHU7B">
                <node concept="37vLTw" id="50KMJClfuuc" role="3uHU7B">
                  <ref role="3cqZAo" node="50KMJClf8Cq" resolve="intentionType" />
                </node>
                <node concept="Rm8GO" id="50KMJClfvJK" role="3uHU7w">
                  <ref role="Rm8GQ" node="5XbSbOFV6O9" resolve="KEEP_AS_FEATURE" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
              <node concept="17R0WA" id="50KMJClfx$3" role="3uHU7w">
                <node concept="37vLTw" id="50KMJClfwKo" role="3uHU7B">
                  <ref role="3cqZAo" node="50KMJClf8Cq" resolve="intentionType" />
                </node>
                <node concept="Rm8GO" id="50KMJClfxTt" role="3uHU7w">
                  <ref role="Rm8GQ" node="5XbSbOFV6T8" resolve="CHANGE_PC" />
                  <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="50KMJClfA8f" role="3eNLev">
            <node concept="17R0WA" id="50KMJClfBtm" role="3eO9$A">
              <node concept="Rm8GO" id="50KMJClfBL_" role="3uHU7w">
                <ref role="Rm8GQ" node="5XbSbOFVqpJ" resolve="POSTPONE" />
                <ref role="1Px2BO" node="5XbSbOFV6mO" resolve="IntentionType" />
              </node>
              <node concept="37vLTw" id="50KMJClfAE4" role="3uHU7B">
                <ref role="3cqZAo" node="50KMJClf8Cq" resolve="intentionType" />
              </node>
            </node>
            <node concept="3clFbS" id="50KMJClfA8h" role="3eOfB_">
              <node concept="3clFbF" id="50KMJClfDHc" role="3cqZAp">
                <node concept="d57v9" id="50KMJClfESb" role="3clFbG">
                  <node concept="3cpWs3" id="50KMJClfFE_" role="37vLTx">
                    <node concept="2OqwBi" id="50KMJClfIDp" role="3uHU7w">
                      <node concept="1eOMI4" id="50KMJClfFW0" role="2Oq$k0">
                        <node concept="1PxgMI" id="50KMJClfJLK" role="1eOMHV">
                          <property role="1BlNFB" value="true" />
                          <ref role="1m5ApE" to="4s7a:3aElbFxeJD9" resolve="PostponeIntention" />
                          <node concept="37vLTw" id="50KMJClfGhs" role="1m5AlR">
                            <ref role="3cqZAo" node="50KMJClf8Ob" resolve="intention" />
                          </node>
                        </node>
                      </node>
                      <node concept="3TrcHB" id="50KMJClfK0H" role="2OqNvi">
                        <ref role="3TsBF5" to="4s7a:3aElbFxeJDc" resolve="note" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="50KMJClfEXL" role="3uHU7B">
                      <property role="Xl_RC" value=" " />
                    </node>
                  </node>
                  <node concept="37vLTw" id="50KMJClfDHb" role="37vLTJ">
                    <ref role="3cqZAo" node="50KMJClfft9" resolve="tooltip" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="50KMJClftUl" role="3cqZAp" />
        <node concept="3clFbF" id="50KMJClfL74" role="3cqZAp">
          <node concept="d57v9" id="50KMJClfM_q" role="3clFbG">
            <node concept="3cpWs3" id="50KMJClfNKr" role="37vLTx">
              <node concept="2OqwBi" id="50KMJClfOdS" role="3uHU7w">
                <node concept="37vLTw" id="50KMJClfNVl" role="2Oq$k0">
                  <ref role="3cqZAo" node="50KMJClf8Ob" resolve="intention" />
                </node>
                <node concept="3TrcHB" id="50KMJClfOud" role="2OqNvi">
                  <ref role="3TsBF5" to="4s7a:1SJQf$Wm6pV" resolve="view" />
                </node>
              </node>
              <node concept="Xl_RD" id="50KMJClfMF2" role="3uHU7B">
                <property role="Xl_RC" value=" | View: " />
              </node>
            </node>
            <node concept="37vLTw" id="50KMJClfL72" role="37vLTJ">
              <ref role="3cqZAo" node="50KMJClfft9" resolve="tooltip" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="50KMJClfKpp" role="3cqZAp" />
        <node concept="3cpWs6" id="50KMJClffNS" role="3cqZAp">
          <node concept="37vLTw" id="50KMJClfg4g" role="3cqZAk">
            <ref role="3cqZAo" node="50KMJClfft9" resolve="tooltip" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="50KMJClfdcq" role="1B3o_S" />
      <node concept="17QB3L" id="50KMJClfdlp" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="50KMJClf7Sh" role="1B3o_S" />
    <node concept="2tJIrI" id="50KMJClg0iO" role="jymVt" />
    <node concept="3clFb_" id="50KMJClfZJT" role="jymVt">
      <property role="TrG5h" value="getIntentionType" />
      <node concept="3uibUv" id="50KMJClfZJU" role="3clF45">
        <ref role="3uigEE" node="5XbSbOFV6mO" resolve="IntentionType" />
      </node>
      <node concept="3Tm1VV" id="50KMJClfZJV" role="1B3o_S" />
      <node concept="3clFbS" id="50KMJClfZJW" role="3clF47">
        <node concept="3clFbF" id="50KMJClfZJX" role="3cqZAp">
          <node concept="37vLTw" id="50KMJClfZJS" role="3clFbG">
            <ref role="3cqZAo" node="50KMJClf8Cq" resolve="intentionType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="50KMJClg01w" role="jymVt" />
    <node concept="3clFb_" id="50KMJClfZJZ" role="jymVt">
      <property role="TrG5h" value="getIntention" />
      <node concept="3Tqbb2" id="50KMJClfZK0" role="3clF45">
        <ref role="ehGHo" to="4s7a:3LUIgcmYqc7" resolve="Intention" />
      </node>
      <node concept="3Tm1VV" id="50KMJClfZK1" role="1B3o_S" />
      <node concept="3clFbS" id="50KMJClfZK2" role="3clF47">
        <node concept="3clFbF" id="50KMJClfZK3" role="3cqZAp">
          <node concept="37vLTw" id="50KMJClfZJY" role="3clFbG">
            <ref role="3cqZAo" node="50KMJClf8Ob" resolve="intention" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4DqDj9Gi68n">
    <ref role="1XX52x" to="4s7a:3VMRtc4W287" resolve="CPP" />
    <node concept="3EZMnI" id="4DqDj9Gi68o" role="2wV5jI">
      <node concept="3F1sOY" id="4DqDj9Gi68p" role="3EZMnx">
        <property role="2ru_X1" value="true" />
        <ref role="1NtTu8" to="4s7a:3LUIgcmYqcd" resolve="intention" />
        <node concept="Veino" id="4DqDj9Gi68q" role="3F10Kt" />
        <node concept="35HoNQ" id="4DqDj9Gi68r" role="2ruayu" />
      </node>
      <node concept="2iRfu4" id="4DqDj9Gi68s" role="2iSdaV" />
      <node concept="3EZMnI" id="4DqDj9Gi68t" role="3EZMnx">
        <node concept="l2Vlx" id="4DqDj9Gi68u" role="2iSdaV" />
        <node concept="3EZMnI" id="4DqDj9Gi68v" role="3EZMnx">
          <node concept="2iRfu4" id="4DqDj9Gi68w" role="2iSdaV" />
          <node concept="3F0A7n" id="4DqDj9Gi68x" role="3EZMnx">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <node concept="ljvvj" id="4DqDj9Gi68y" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="Vb9p2" id="4DqDj9Gi68z" role="3F10Kt">
              <property role="Vbekb" value="BOLD" />
            </node>
            <node concept="VQ3r3" id="4DqDj9Gi68$" role="3F10Kt">
              <property role="2USNnj" value="2" />
            </node>
          </node>
          <node concept="pVoyu" id="4DqDj9Gi68_" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="3F0ifn" id="4DqDj9Gi68A" role="3EZMnx">
            <property role="3F0ifm" value="[" />
          </node>
          <node concept="3F0ifn" id="4DqDj9Gi68B" role="3EZMnx">
            <property role="3F0ifm" value="Open Integration Chunks:" />
          </node>
          <node concept="1HlG4h" id="4DqDj9Gi68C" role="3EZMnx">
            <node concept="1HfYo3" id="4DqDj9Gi68D" role="1HlULh">
              <node concept="3TQlhw" id="4DqDj9Gi68E" role="1Hhtcw">
                <node concept="3clFbS" id="4DqDj9Gi68F" role="2VODD2">
                  <node concept="3cpWs8" id="4DqDj9Gi68G" role="3cqZAp">
                    <node concept="3cpWsn" id="4DqDj9Gi68H" role="3cpWs9">
                      <property role="TrG5h" value="count" />
                      <node concept="10Oyi0" id="4DqDj9Gi68I" role="1tU5fm" />
                      <node concept="3cmrfG" id="4DqDj9Gi68J" role="33vP2m">
                        <property role="3cmrfH" value="-1" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="4DqDj9Gi68K" role="3cqZAp" />
                  <node concept="3clFbJ" id="4DqDj9Gi68L" role="3cqZAp">
                    <node concept="3clFbS" id="4DqDj9Gi68M" role="3clFbx">
                      <node concept="3clFbF" id="4DqDj9Gi68N" role="3cqZAp">
                        <node concept="37vLTI" id="4DqDj9Gi68O" role="3clFbG">
                          <node concept="2OqwBi" id="4DqDj9Gi68P" role="37vLTx">
                            <node concept="2OqwBi" id="4DqDj9Gi68Q" role="2Oq$k0">
                              <node concept="2OqwBi" id="4DqDj9Gi68R" role="2Oq$k0">
                                <node concept="2OqwBi" id="4DqDj9Gi68S" role="2Oq$k0">
                                  <node concept="pncrf" id="4DqDj9Gi68T" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="4DqDj9Gi68U" role="2OqNvi">
                                    <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                                  </node>
                                </node>
                                <node concept="2Rf3mk" id="4DqDj9Gi68V" role="2OqNvi">
                                  <node concept="1xMEDy" id="4DqDj9Gi68W" role="1xVPHs">
                                    <node concept="chp4Y" id="4DqDj9Gi68X" role="ri$Ld">
                                      <ref role="cht4Q" to="4s7a:2s5q4UVM$2" resolve="MacroIf" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3zZkjj" id="4DqDj9Gi68Y" role="2OqNvi">
                                <node concept="1bVj0M" id="4DqDj9Gi68Z" role="23t8la">
                                  <node concept="3clFbS" id="4DqDj9Gi690" role="1bW5cS">
                                    <node concept="3clFbF" id="4DqDj9Gi691" role="3cqZAp">
                                      <node concept="2OqwBi" id="4DqDj9Gi692" role="3clFbG">
                                        <node concept="37vLTw" id="4DqDj9Gi693" role="2Oq$k0">
                                          <ref role="3cqZAo" node="4DqDj9Gi695" resolve="it" />
                                        </node>
                                        <node concept="2qgKlT" id="4DqDj9Gi694" role="2OqNvi">
                                          <ref role="37wK5l" to="7zqe:2NfWTcLJRHk" resolve="isCloneIf" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="Rh6nW" id="4DqDj9Gi695" role="1bW2Oz">
                                    <property role="TrG5h" value="it" />
                                    <node concept="2jxLKc" id="4DqDj9Gi696" role="1tU5fm" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="34oBXx" id="4DqDj9Gi697" role="2OqNvi" />
                          </node>
                          <node concept="37vLTw" id="4DqDj9Gi698" role="37vLTJ">
                            <ref role="3cqZAo" node="4DqDj9Gi68H" resolve="count" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3y3z36" id="4DqDj9Gi699" role="3clFbw">
                      <node concept="10Nm6u" id="4DqDj9Gi69a" role="3uHU7w" />
                      <node concept="2OqwBi" id="4DqDj9Gi69b" role="3uHU7B">
                        <node concept="pncrf" id="4DqDj9Gi69c" role="2Oq$k0" />
                        <node concept="3TrEf2" id="4DqDj9Gi69d" role="2OqNvi">
                          <ref role="3Tt5mk" to="4s7a:3LUIgcmtu8f" resolve="future" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="4DqDj9Gi69e" role="3cqZAp" />
                  <node concept="3cpWs6" id="4DqDj9Gi69f" role="3cqZAp">
                    <node concept="2YIFZM" id="4DqDj9Gi69g" role="3cqZAk">
                      <ref role="37wK5l" to="wyt6:~Integer.toString(int):java.lang.String" resolve="toString" />
                      <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                      <node concept="37vLTw" id="4DqDj9Gi69h" role="37wK5m">
                        <ref role="3cqZAo" node="4DqDj9Gi68H" resolve="count" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3F0ifn" id="4DqDj9Gi69i" role="3EZMnx">
            <property role="3F0ifm" value="]" />
          </node>
        </node>
        <node concept="3F0ifn" id="4DqDj9GlFJZ" role="3EZMnx">
          <property role="3F0ifm" value="Side-by-Side" />
          <node concept="30gYXW" id="4DqDj9GlFK0" role="3F10Kt">
            <property role="Vb096" value="yellow" />
          </node>
          <node concept="pVoyu" id="4DqDj9GnxDF" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="4DqDj9Gi69l" role="3EZMnx">
          <ref role="1ERwB7" node="2NOvNR_qwA8" resolve="MyActionMap" />
          <ref role="1NtTu8" to="4s7a:2s5q4UUgwl" resolve="statements" />
          <node concept="l2Vlx" id="4DqDj9Gi69m" role="2czzBx" />
          <node concept="pj6Ft" id="4DqDj9Gi69n" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pVoyu" id="4DqDj9Gi69o" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="4$FPG" id="4DqDj9Gi69p" role="4_6I_">
            <node concept="3clFbS" id="4DqDj9Gi69q" role="2VODD2">
              <node concept="3clFbF" id="4DqDj9Gi69r" role="3cqZAp">
                <node concept="2ShNRf" id="4DqDj9Gi69s" role="3clFbG">
                  <node concept="3zrR0B" id="4DqDj9Gi69t" role="2ShVmc">
                    <node concept="3Tqbb2" id="4DqDj9Gi69u" role="3zrR0E">
                      <ref role="ehGHo" to="4s7a:2s5q4UUgwr" resolve="TextContent" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="4DqDj9Gi69v" role="6VMZX">
      <node concept="2iRfu4" id="4DqDj9Gi69w" role="2iSdaV" />
      <node concept="3F0ifn" id="4DqDj9Gi69x" role="3EZMnx">
        <property role="3F0ifm" value="Constraint" />
      </node>
      <node concept="3F0A7n" id="4DqDj9Gi69y" role="3EZMnx">
        <ref role="1NtTu8" to="4s7a:79Do2olI3d1" resolve="constraints" />
      </node>
    </node>
    <node concept="2aJ2om" id="4DqDj9Gi6$X" role="CpUAK">
      <ref role="2$4xQ3" node="5a5L3hYatMh" resolve="SideBySideClones" />
    </node>
  </node>
</model>

