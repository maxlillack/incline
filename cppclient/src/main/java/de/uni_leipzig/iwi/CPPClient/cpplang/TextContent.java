/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient.cpplang;

import de.uni_leipzig.iwi.CPPClient.Location;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.List;

public class TextContent implements ICPPElement {
	private Location location;
	private String content;

	public TextContent(Location location, List<String> contentLines) {
		this.location = location;
		this.content = StringUtils.join(contentLines, "\n");
	}

	@Override
	public Location getLocation() {
		return location;
	}

	public String getContent()
	{
		return content;
	}

	@Override
	public String toString() {
		return "::" + content.replaceAll("\n", "\n::");
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextContent other = (TextContent) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}

	@Override
	public void toXML(Element parent) {
		Document doc = parent.getOwnerDocument();

		String[] lines = content.split("\\r?\\n", -1);

		String line = "";
		boolean inBlockComment = false;
		for(int i = 0; i < lines.length; i++)
		{
			if(lines[i].trim().startsWith("/*") && !lines[i].trim().endsWith("*/")) {
				inBlockComment = true;
				line = lines[i];
			} else if (inBlockComment) {
				line += "\n" + lines[i];
				if(lines[i].trim().endsWith("*/"))
				{
					Element e = doc.createElement("text");
					e.appendChild(doc.createCDATASection(line));
					parent.appendChild(e);
					inBlockComment = false;
					line = "";
				}
			} else {
				line = lines[i];
				Element e = doc.createElement("text");
				e.appendChild(doc.createCDATASection(line));
				parent.appendChild(e);
			}
		}

	}

}
