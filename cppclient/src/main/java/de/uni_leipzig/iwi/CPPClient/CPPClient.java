/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import de.uni_leipzig.iwi.CPPClient.cpplang.ICPPElement;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ThreadLocalRandom;

public class CPPClient implements LLVMPreprocessor {

    private DocumentBuilder builder;

    private Config config = ConfigFactory.load();


    public CPPClient(File configFile) throws ParserConfigurationException
    {
        config = ConfigFactory.parseFile(configFile);

        if(config == null || config.isEmpty()) {
            throw new IllegalStateException("CPPClient could not load config file " + configFile.getAbsolutePath());
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();
    }

    public CPPClient() throws ParserConfigurationException
    {
        if(config == null || config.isEmpty()) {
            throw new IllegalStateException("CPPClient could not load config file.\n" + new File(".").getAbsolutePath());
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();
    }

    @Override
    public String preprocessLocal(String content) throws IOException {
        File clang = new File(config.getString("llvmPath"), Platform.getClangExecutable());
        String cppxmlpluginPath = config.getString("CPPXMLPlugin");
        File cppxmlplugin = new File(cppxmlpluginPath, Platform.getCPPXMLPluginLibrary());
        File tinyxml2 = new File(cppxmlpluginPath, "tinyxml2/libtinyxml2" + Platform.getDynamicLibrarySuffix());
        String tempPath = config.getString("tempPath");
        File samplesFolder = new File(config.getString("CPPListener"), "cppclient/src/main/resources");


        if(!clang.exists())
        {
            throw new IllegalArgumentException("clang++ could not be found at " + clang.getAbsolutePath() + ". Please check config.");
        }
        if(!cppxmlplugin.exists())
        {
            String filename = "CPPXMLPlugin" + Platform.getDynamicLibrarySuffix();
            throw new IllegalArgumentException(filename + " could not be found at " + cppxmlplugin + ". Please check config.");
        }
        if(!new File(tempPath).exists())
        {
            throw new IllegalArgumentException("tempPath could not be found at " + tempPath + ". Please check config.");
        }

        if(!new File(samplesFolder.getAbsolutePath(), "header-stub.h").exists())
        {
            throw new IllegalArgumentException("Could not find header-stub.h at " + samplesFolder.getAbsolutePath());
        }

        File inputFile = new File(tempPath, "hello.cpp");

        // DISABLED -> works for import but breaks behaviour in MPS
//        if(!Files.isWritable(Paths.get(inputFile.getPath())))
//        {
//        	// Variant with random name part
//        	inputFile =  new File(tempPath, "hello" + ThreadLocalRandom.current().nextInt(10000, 99999) + ".cpp");
//        }
        
        FileUtils.writeStringToFile(inputFile, content, Charsets.UTF_8);

        ProcessBuilder pb = new ProcessBuilder(clang.getAbsolutePath(),
                "-Wno-macro-redefined",
                "-Wno-expansion-to-defined",
                "-I", samplesFolder.getAbsolutePath(),
                "-include", "header-stub.h",
                "-Xclang", "-load",
                "-Xclang", cppxmlplugin.getAbsolutePath(),
                "-Xclang", "-load",
                "-Xclang", tinyxml2.getAbsolutePath(),
                "-Xclang", "-plugin",
                "-Xclang", "print-pp",
                "-E",
                "-Xclang", "-E",
                inputFile.getAbsolutePath().replace('\\', '/'));

        pb.directory(new File(cppxmlpluginPath));
        pb.redirectErrorStream(true);
        Process p = pb.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ( (line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }

        return builder.toString();
    }


    public ResultModel start(File file) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        String inputFile = FileUtils.readFileToString(file, Charsets.UTF_8);

        return start(inputFile);
    }


    public ResultModel start(String input) throws IOException, SAXException, XPathExpressionException {
        //TODO: Make this preprocessing optional depending on whether we're parsing the integrated AST or source files
        input = PreprocessorLinter.adjustIncludes(input);
        input = PreprocessorLinter.rewriteStatementsWithSpaces(input);
        input = PreprocessorLinter.removeTrailingWhitespace(input);

        String[] lines = input.split("\\r?\\n");

        String result = preprocessLocal(input);

        // Get XML
//		System.out.println(result);

        // Parse XML to Model
        Document doc = null;
        try {
            doc = builder.parse(new InputSource(new StringReader(result)));
        } catch(SAXParseException e)
        {
            System.err.println(result);
            System.err.println(e);
            System.err.println(result.substring(0, 100));
            throw e;
        }
        // Check for skipped ranges
        ResultModel resultModel = new ResultModel(lines);
        XMLSyntaxtree xmlSyntaxtree = new XMLSyntaxtree(lines, resultModel, builder, this);
        Element root = (Element)doc.getElementsByTagName("PPTest").item(0);
        for(ICPPElement cppElement : xmlSyntaxtree.getCPPElementsFromChildren(root)) {
            resultModel.getCppElements().addAll(xmlSyntaxtree.handleElement(cppElement, doc, input));
        }

        resultModel.addTextFragments();

        return resultModel;
    }
}
