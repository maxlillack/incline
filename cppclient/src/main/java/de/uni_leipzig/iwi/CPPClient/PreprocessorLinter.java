/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import java.util.regex.Pattern;

public class PreprocessorLinter {
    private static final Pattern includePattern = Pattern.compile("^\\s*#\\s*include", Pattern.MULTILINE);
    private static final Pattern errorPattern = Pattern.compile("^\\s*#\\s*error", Pattern.MULTILINE);
    private static final Pattern pragmaPattern = Pattern.compile("^\\s*#\\s*pragma", Pattern.MULTILINE);

    private static final Pattern ifPattern = Pattern.compile("^#\\s*if", Pattern.MULTILINE);
    private static final Pattern elifPattern = Pattern.compile("^#\\s*elif", Pattern.MULTILINE);
    private static final Pattern elsePattern = Pattern.compile("^#\\s*else", Pattern.MULTILINE);
    private static final Pattern endifPattern = Pattern.compile("^#\\s*endif", Pattern.MULTILINE);
    private static final Pattern definePattern = Pattern.compile("^#\\s*define", Pattern.MULTILINE);

    private static final Pattern trailingWhitespace = Pattern.compile("([ \t])*$", Pattern.MULTILINE);

    public static String adjustIncludes(String input) {
        // Ignore #includes
        input = includePattern.matcher(input).replaceAll("//#include");
        // Ignore #errors
        input = errorPattern.matcher(input).replaceAll("//#error");
        // Ignore #pragma
        input = pragmaPattern.matcher(input).replaceAll("//#pragma");

        return input;
    }

    public static String rewriteStatementsWithSpaces(String input) {
        String rewritten;
        rewritten = ifPattern.matcher(input).replaceAll("#if");
        rewritten = elifPattern.matcher(rewritten).replaceAll("#elif");
        rewritten = elsePattern.matcher(rewritten).replaceAll("#else");
        rewritten = endifPattern.matcher(rewritten).replaceAll("#endif");
        rewritten = definePattern.matcher(rewritten).replaceAll("#define");

        return rewritten;
    }

    public static String removeTrailingWhitespace(String input) {
        return trailingWhitespace.matcher(input).replaceAll("");
    }
}
