/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient.cpplang;

import de.uni_leipzig.iwi.CPPClient.Location;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.List;

public class MacroDefined implements ICPPElement {

	private Location location;
	private Location locationLastToken;
	private String name;
	private List<String> args;
	private List<String> content;
	private String comment;

	public MacroDefined(Location location, Location locationLastToken, String name, List<String> args, List<String> content) {
		this.location = location;
		this.locationLastToken = locationLastToken;
		this.name = name;
		this.args = args;
		this.content = content;
	}

	public MacroDefined(Location location, Location locationLastToken, String name, List<String> args, List<String> content, String comment) {
		this(location, locationLastToken, name, args, content);
		this.comment = comment;
	}

	@Override
	public Location getLocation() {
		return location;
	}



	public Location getLocationLastToken() {
		return locationLastToken;
	}

	public String getName()
	{
		return name;
	}

	public List<String> getContent()
	{
		return content;
	}

	public List<String> getArgs() {
		return args;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MacroDefined " + name);
		if(!args.isEmpty()) {
			sb.append("(").append(StringUtils.join(args, ", ")).append(")");
		}
		sb.append(" [location=" + location + "]");
		sb.append(StringUtils.join(content, "\n"));
		return sb.toString();
	}

	public String getComment() {
		return comment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MacroDefined other = (MacroDefined) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}

	@Override
	public void toXML(Element parent) {

		Document doc = parent.getOwnerDocument();
		Element e = doc.createElement("MacroDefined");
		e.setAttribute("name", name);
		e.setAttribute("args", StringUtils.join(args, ", "));

		StringBuilder sb = new StringBuilder();
		for(String content : this.content) {
			sb.append(content + "\n");

		}
		Element c = doc.createElement("content");
		c.appendChild(doc.createCDATASection(sb.toString()));
		e.appendChild(c);

		parent.appendChild(e);
	}

}
