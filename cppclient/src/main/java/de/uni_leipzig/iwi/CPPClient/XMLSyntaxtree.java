/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import de.uni_leipzig.iwi.CPPClient.cpplang.*;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

public class XMLSyntaxtree {
    private final XPath xpath = XPathFactory.newInstance().newXPath();
    private final String[] lines;
    private final ResultModel resultModel;
    private final DocumentBuilder builder;
    private final LLVMPreprocessor llvmPreprocessor;

    public XMLSyntaxtree(String[] lines, ResultModel resultModel, DocumentBuilder builder, LLVMPreprocessor llvmPreprocessor) {
        this.lines = lines;
        this.resultModel = resultModel;
        this.builder = builder;
        this.llvmPreprocessor = llvmPreprocessor;
    }

    public List<ICPPElement> getCPPElementsFromChildren(Element root) throws XPathExpressionException, IOException, SAXException {

        Stack<List<ICPPElement>> stack = new Stack<>();
        stack.push(new ArrayList<>());

        List<ICPPElement> results = stack.peek();

        NodeList childs = (NodeList) xpath.evaluate("//*[self::MacroDefined or self::If or self::ElIf or self::Ifdef or self::Ifndef or self::Else or self::SourceRangeSkipped or self::Endif]", root, XPathConstants.NODESET);

        for(int i = 0; i < childs.getLength(); i++)
        {
            if(childs.item(i) instanceof Element)
            {
                Element element = (Element) childs.item(i);

                getCPPElementsFromSingle(stack, results, element);

            }
        }

        return results;
    }

    private List<ICPPElement> getCPPElements(Element element) throws XPathExpressionException, IOException, SAXException
    {

        Stack<List<ICPPElement>> stack = new Stack<>();
        stack.push(new ArrayList<>());

        List<ICPPElement> results = stack.peek();

        getCPPElementsFromSingle(stack, results, element);

        return results;
    }

    private void getCPPElementsFromSingle(Stack<List<ICPPElement>> stack, List<ICPPElement> results, Element element) {
        // MacroDefined
        if(element.getNodeName().equals("MacroDefined"))
        {
            Location location = new Location(element.getElementsByTagName("Location").item(0).getTextContent());
            Location locationLastToken = new Location(element.getElementsByTagName("LocationLastToken").item(0).getTextContent());
            Location locationEnd = new Location(element.getElementsByTagName("LocationEnd").item(0).getTextContent());
            String name = element.getElementsByTagName("Name").item(0).getTextContent().trim();

            List<String> args = new ArrayList<>();
            NodeList argNodes = element.getElementsByTagName("Arg");
            for(int j = 0; j < argNodes.getLength(); j++)
            {
                Node arg = argNodes.item(j);
                args.add(arg.getTextContent().trim());
            }

            List<String> tokenParts = new ArrayList<>();
            NodeList tokens = element.getElementsByTagName("Token");
            for(int j = 0; j < tokens.getLength(); j++)
            {
                Node token = tokens.item(j);

                String tokenPart = token.getTextContent().trim();

                if(tokenPart.startsWith("\\")) {
                    tokenPart = tokenPart.substring(1).trim();
                }

                if(tokenPart.endsWith("\\")) {
                    tokenPart = tokenPart.substring(0, tokenPart.length() - 2);
                }

                tokenParts.add(tokenPart);
            }


            String allContent = StringUtils.join(tokenParts, ' ');
            List<String> content = Arrays.asList(allContent.split("\\r?\\n"));

            MacroDefined macroDefined;

            String line = lines[location.getLine() - 1];
            if(line.length() > locationEnd.getColumn() && !line.endsWith("\\"))
            {
                String comment = line.substring(locationEnd.getColumn());
                macroDefined = new MacroDefined(location, locationEnd, name, args, content, comment);
            } else {
                macroDefined = new MacroDefined(location, locationEnd, name, args, content);
            }

            if(!stack.peek().contains(macroDefined)) {
                stack.peek().add(macroDefined);
            }
        }
        // If
        else if(element.getNodeName().equals("If"))
        {
            Location location = new Location(element.getElementsByTagName("LocationIf").item(0).getTextContent());

            Location conditionStart = new Location(element.getElementsByTagName("ConditionRangeBegin").item(0).getTextContent());
            Location conditionEnd = new Location(element.getElementsByTagName("ConditionRangeEnd").item(0).getTextContent());

            StringBuilder condition = new StringBuilder();
            for(int linenumber = conditionStart.getLine(); linenumber <= conditionEnd.getLine(); linenumber++)
            {
                String line = lines[linenumber - 1];
                String conditionPart;
                if (line.trim().isEmpty()) {
                    conditionPart = "";
                }
                else if(linenumber == conditionStart.getLine()) {
                    // First line
                    conditionPart = line.substring(conditionStart.getColumn());
                } else if(linenumber == conditionEnd.getLine()) {
                    // Last line
                    conditionPart = line.substring(0, Math.min(conditionEnd.getColumn() - 1, line.length()));
                } else {
                    conditionPart = line;
                }

                if(conditionPart.endsWith("\\")) {
                    conditionPart = conditionPart.substring(0, conditionPart.length() - 2);
                }

                // condition may include comments which we will now strip
                int commentPos = conditionPart.indexOf("//");
                if(commentPos > -1)
                {
                    conditionPart = conditionPart.substring(0, conditionPart.indexOf("//"));
                }

                condition.append(conditionPart);
            }


            If ifElement = searchIfStatement(location);
            if(ifElement == null) {
                ifElement = new If(location, condition.toString());
                ifElement.conditionEnd = conditionEnd;
            }
            if(!stack.peek().contains(ifElement)) {
                stack.peek().add(ifElement);
            }
            stack.push(ifElement.getTrueBranch());
        }
        // Ifdef
        else if(element.getNodeName().equals("Ifdef"))
        {
            Location location = new Location(element.getElementsByTagName("Location").item(0).getTextContent());
            String condition = element.getElementsByTagName("Condition").item(0).getTextContent();
            If ifElement = searchIfStatement(location);
            if(ifElement == null) {
                ifElement = new If(location, "defined(" + condition + ")");
            }
            if(!stack.peek().contains(ifElement)) {
                stack.peek().add(ifElement);
            }
            stack.push(ifElement.getTrueBranch());

        }
        // Ifndef
        else if(element.getNodeName().equals("Ifndef"))
        {
            Location location = new Location(element.getElementsByTagName("Location").item(0).getTextContent());
            String condition = element.getElementsByTagName("Condition").item(0).getTextContent();
            If ifElement = searchIfStatement(location);
            if(ifElement == null) {
                ifElement = new If(location, "!defined(" + condition + ")");
            }
            if(!stack.peek().contains(ifElement)) {
                stack.peek().add(ifElement);
            }
            stack.push(ifElement.getTrueBranch());
        }
        // SourceRangeSkipped
        else if(element.getNodeName().equals("SourceRangeSkipped"))
        {
            String rangeBegin = element.getElementsByTagName("RangeBegin").item(0).getTextContent();
            String rangeEnd = element.getElementsByTagName("RangeEnd").item(0).getTextContent();

            LocationRange range = new LocationRange(new Location(rangeBegin), new Location(rangeEnd));

            // SourceRangeSkipped is not properly places in XML, we explicitly place it:
//					If ifElement = searchIfStatement(range.getFrom(), stack.peek());
//					MacroElIf elifElement = searchlElIfStatement(range.getFrom(), stack.peek());
//					If ifWithElseElement = searchElseStatement(range.getFrom(), stack.peek());

            // Check if rangeBegin equals
            // a) #if -> add to true branch
            // b) #elif -> add to elif's branch
            // c) #else -> add to else branch
            SourceRangeSkipped sourceRangeSkipped = new SourceRangeSkipped(range);
//
//					if(ifElement != null)
//					{
//						ifElement.getTrueBranch().add(sourceRangeSkipped);
//					} else if(elifElement != null)
//					{
//						elifElement.getBranch().add(sourceRangeSkipped);
//					} else if(ifWithElseElement != null)
//					{
//						ifWithElseElement.getElseBranch().add(sourceRangeSkipped);
//					} else {
//						throw new IllegalStateException("Unable to place SourceRangeSkipped " + sourceRangeSkipped);
//					}

            results.add(sourceRangeSkipped);
        }
        else if(element.getNodeName().equals("ElIf"))
        {
            // Select corresponding if and add information on endif
            Location location = new Location(element.getElementsByTagName("LocationElIf").item(0).getTextContent());
            Location ifLocation = new Location(element.getElementsByTagName("LocationCorrespondingIf").item(0).getTextContent());

            Location conditionStart = new Location(element.getElementsByTagName("ConditionRangeBegin").item(0).getTextContent());
            Location conditionEnd = new Location(element.getElementsByTagName("ConditionRangeEnd").item(0).getTextContent());

            String condition;
            String line = lines[conditionStart.getLine() - 1];
            if(conditionStart.getLine() != conditionEnd.getLine()) {
                if(conditionEnd.getLine() != conditionStart.getLine() + 1 || conditionEnd.getColumn() != 1)
                {
                    throw new IllegalStateException("Multi-line condition not implemented.");
                }
                condition = line.substring(conditionStart.getColumn());
            } else {
                condition = line.substring(conditionStart.getColumn(), conditionEnd.getColumn() - 2);
            }

            MacroElIf elIf = searchlElIfStatement(location, stack.peek());
            if(stack.size() < 2) {
                throw new IllegalStateException();
            }
            stack.pop();

            if(elIf == null) {
                elIf = new MacroElIf(location, condition, new ArrayList<>());
                If cppIf = null;
                cppIf = searchIfStatement(ifLocation, stack.peek());

                if(cppIf != null) {
                    cppIf.addElIf(elIf);
                }
            }

            stack.push(elIf.getBranch());
        }
        else if(element.getNodeName().equals("Else"))
        {
            // Select corresponding if and add information on else
            Location location = new Location(element.getElementsByTagName("LocationElse").item(0).getTextContent());
            Location ifLocation = new Location(element.getElementsByTagName("LocationCorrespondingIf").item(0).getTextContent());
            if(stack.size() < 2) {
                throw new IllegalStateException();
            }
            stack.pop();
            If cppIf = searchIfStatement(ifLocation, stack.peek());

            cppIf.setElseLocation(location);
            stack.push(cppIf.getElseBranch());
        }
        else if(element.getNodeName().equals("Endif"))
        {
            // Select corresponding if and add information on endif
            Location location = new Location(element.getElementsByTagName("LocationEndIf").item(0).getTextContent());
            Location ifLocation = new Location(element.getElementsByTagName("LocationCorrespondingIf").item(0).getTextContent());

            // @ TODO - We currently see #endif for #ifdef/#ifndef we do not handle yet

            If cppIf = null;

            if(stack.size() > 1) {
                stack.pop();
                cppIf = searchIfStatement(ifLocation, stack.peek());
            } else {
                cppIf = searchIfStatement(ifLocation);
            }
            if(cppIf != null) {
                cppIf.setEndIfLocation(location);
            }


        }
        else if(element.getNodeName().equals("MacroExpands"))
        {
            String rangeBegin = element.getElementsByTagName("RangeBegin").item(0).getTextContent();
            String rangeEnd = element.getElementsByTagName("RangeEnd").item(0).getTextContent();

            LocationRange range = new LocationRange(new Location(rangeBegin), new Location(rangeEnd));
            MacroExpands macroExpands = new MacroExpands(range);
            if(!stack.peek().contains(macroExpands)) {
                stack.peek().add(macroExpands);
            }
        }
        else if(element.getNodeName().equals("InclusionDirective"))
        {
            Location location = new Location(element.getElementsByTagName("HashLoc").item(0).getTextContent());
            String fileName = element.getElementsByTagName("FileName").item(0).getTextContent();
            boolean isAngled = element.getElementsByTagName("IsAngled").item(0).getTextContent().equals("1");

            InclusionDirective id = new InclusionDirective(location, fileName, isAngled);
            if(!stack.peek().contains(id)) {
                stack.peek().add(id);
            }
        }
        else {
            System.err.println("Unhandled type " + element.getNodeName());
        }
    }

    public List<ICPPElement> handleElement(ICPPElement cppElement, Document doc, String inputFile) throws XPathExpressionException, IOException, SAXException {
        List<ICPPElement> results = new ArrayList<>();

        if(cppElement instanceof SourceRangeSkipped)
        {
//			System.out.println("Add SourceRangeSkipped " + cppElement);

            SourceRangeSkipped sourceRangeSkipped = (SourceRangeSkipped) cppElement;
            LocationRange range = sourceRangeSkipped.getRange();
            // Why was it skipped? Look for if or else element

            // else
            String xpathExpr = "/PPTest/Else[LocationElse/text()='" + range.getFrom() + "']";
            Element elseElement = (Element) xpath.evaluate(xpathExpr, doc, XPathConstants.NODE);

            // elif
            xpathExpr = "/PPTest/ElIf[LocationElIf/text()='" + range.getFrom() + "']";
            Element elifElement = (Element) xpath.evaluate(xpathExpr, doc, XPathConstants.NODE);

            xpathExpr = "/PPTest/If[LocationIf/text()='" + range.getFrom() + "'] | /PPTest/Ifdef[Location/text()='" + range.getFrom() + "'] | /PPTest/Ifndef[Location/text()='" + range.getFrom() + "']";
            Element ifElement = (Element) xpath.evaluate(xpathExpr, doc, XPathConstants.NODE);

            if(elseElement != null)
            {
//				System.out.println("Found else");

                // Get corresponding #if
                String ifLocation = elseElement.getElementsByTagName("LocationCorrespondingIf").item(0).getTextContent();
                xpathExpr = "/PPTest/If[LocationIf/text()='" + ifLocation + "'] | /PPTest/Ifdef[Location/text()='" + ifLocation + "'] | /PPTest/Ifndef[Location/text()='" + ifLocation + "']";
                ifElement = (Element) xpath.evaluate(xpathExpr, doc, XPathConstants.NODE);
            } else if(elifElement != null)
            {
                String ifLocationText = elifElement.getElementsByTagName("LocationElIf").item(0).getTextContent();
                String locationCorrespondingIf = elifElement.getElementsByTagName("LocationCorrespondingIf").item(0).getTextContent();
                Location ifLocation = new Location(ifLocationText);

                // Search if
                MacroElIf macroElIf = searchlElIfStatement(ifLocation, results);
                if(macroElIf == null && cppElement.getLocation().toString().equals(ifLocation))
                {
                    macroElIf = (MacroElIf) cppElement;
                }


                // Need to set condition of #if and prev. #elif to false
                xpathExpr = "/PPTest/If[LocationIf/text()='" + locationCorrespondingIf + "'] | /PPTest/Ifdef[Location/text()='" + locationCorrespondingIf + "'] | /PPTest/Ifndef[Location/text()='" + locationCorrespondingIf + "']";
                Element correspondingIfElement = (Element) xpath.evaluate(xpathExpr, doc, XPathConstants.NODE);

                // #if
                String replacedInput = setCondition(inputFile, correspondingIfElement, "0");

                // Look for other #elif
                xpathExpr = "/PPTest/ElIf[LocationCorrespondingIf/text()='" + locationCorrespondingIf + "' and LocationElIf/text()!='" + ifLocationText + "']";
                NodeList otherElIfNodes = (NodeList) xpath.evaluate(xpathExpr, doc, XPathConstants.NODESET);
                for(int i = 0; i < otherElIfNodes.getLength(); i++)
                {
                    Element elIf = (Element) otherElIfNodes.item(i);
                    replacedInput = setCondition(replacedInput, elIf, "0");
                }

                // Set current #elif to true
                replacedInput = setCondition(replacedInput, elifElement, "1");
                // Execute again
                String result = llvmPreprocessor.preprocessLocal(replacedInput);
//				System.out.println("re-run result\n" + result);
                Document doc2 = builder.parse(new InputSource(new StringReader(result)));

                for(ICPPElement cppElement2 : getCPPElementsFromChildren((Element) doc2.getElementsByTagName("PPTest").item(0)))
                {
                    if(range.isInRange(cppElement2.getLocation()) && !range.getFrom().equals(cppElement2.getLocation()))
                    {
                        macroElIf.getBranch().addAll((handleElement(cppElement2, doc2, replacedInput)));
                    }
                    if(cppElement2 instanceof If)
                    {
                        If ifElement2 = (If) cppElement2;
                        for(ICPPElement elIf : ifElement2.getElIfs())
                        {
                            List<ICPPElement> toAdd = null;
                            for(ICPPElement e : ((MacroElIf) elIf).getBranch()) {
                                if(range.isInRange(e.getLocation()) && !range.getFrom().equals(e.getLocation()))
                                {
                                    toAdd = handleElement(e, doc2, inputFile);
                                }
                            }
                            if(toAdd != null)
                            {
                                for(ICPPElement e: toAdd) {
                                    if(!macroElIf.getBranch().contains(e)) {
                                        macroElIf.getBranch().add(e);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            if(ifElement != null) {
                String ifLocationText = null;
                if(ifElement.getElementsByTagName("LocationIf").getLength() > 0) {
                    ifLocationText = ifElement.getElementsByTagName("LocationIf").item(0).getTextContent();
                }
                if(ifElement.getElementsByTagName("Location").getLength() > 0) {
                    ifLocationText = ifElement.getElementsByTagName("Location").item(0).getTextContent();
                }

                Location ifLocation = new Location(ifLocationText);

                // Search if
                If cppIf = searchIfStatement(ifLocation);

                String replacedInput = replaceCondition(inputFile, ifElement);
                // Execute again
                String result = llvmPreprocessor.preprocessLocal(replacedInput);
//				System.out.println("re-run result\n" + result);
                Document doc2;
                try {
                    doc2 = builder.parse(new InputSource(new StringReader(result)));
                } catch(SAXParseException e)
                {
                    System.err.println("Parse error after request \n" + replacedInput);
                    System.err.println("Result:\n\n" + result);
                    System.err.println("Less:\n\n" + result.substring(0, 100));
                    throw e;
                }

                for(ICPPElement cppElement2 : getCPPElementsFromChildren((Element) doc2.getElementsByTagName("PPTest").item(0)))
                {
                    if(range.isInRange(cppElement2.getLocation()) && !range.getFrom().equals(cppElement2.getLocation()))
                    {
                        if(elseElement != null)
                        {
                            List<ICPPElement> toAdd = handleElement(cppElement2, doc2, replacedInput);
                            for(ICPPElement e: toAdd) {
                                if(!cppIf.getElseBranch().contains(e)) {
                                    cppIf.getElseBranch().add(e);
                                }
                            }
                        } else {
                            for(ICPPElement e : handleElement(cppElement2, doc2, replacedInput))
                            {
                                if(!cppIf.getTrueBranch().contains(e)) {
                                    cppIf.getTrueBranch().add(e);
                                }
                            }
                        }
                    } else if(cppElement2 instanceof If)
                    {
                        List<ICPPElement> toAdd = null;
                        for(ICPPElement e : ((If)cppElement2).getTrueBranch())
                        {
                            if(range.isInRange(e.getLocation()) && !range.getFrom().equals(e.getLocation()))
                            {
                                toAdd = handleElement(e, doc2, inputFile);
                            }
                        }
                        if(toAdd != null) {
                            for(ICPPElement e: toAdd) {
                                if(elseElement != null)
                                {
                                    if(!cppIf.getElseBranch().contains(e)) {
                                        cppIf.getElseBranch().add(e);
                                    }
                                } else {
                                    if(!cppIf.getTrueBranch().contains(e)) {
                                        cppIf.getTrueBranch().add(e);
                                    }
                                }
                            }

                        }
                    } else if(cppElement2 instanceof MacroElIf)
                    {
                        List<ICPPElement> toAdd = null;
                        for(ICPPElement e : ((MacroElIf)cppElement2).getBranch())
                        {
                            if(range.isInRange(e.getLocation()) && !range.getFrom().equals(e.getLocation()))
                            {
                                toAdd = handleElement(e, doc2, inputFile);
                            }
                        }
                        if(toAdd != null) {
                            for(ICPPElement e: toAdd) {
                                if(elseElement != null)
                                {
                                    if(!cppIf.getElseBranch().contains(e)) {
                                        cppIf.getElseBranch().add(e);
                                    }
                                } else {
                                    if(!cppIf.getTrueBranch().contains(e)) {
                                        cppIf.getTrueBranch().add(e);
                                    }
                                }
                            }

                        }
                    }
                }



                // Look for results within skipped range
                // For our test, we look for defines and ifs
            } else if(elifElement == null) {
                System.err.println("Found no corresponding #if");
            }
        }
        else {
            results.add(cppElement);
        }

        // Fill result model with text blocks
        return results;
    }

    private If searchIfStatement(Location ifLocation) {
        return searchIfStatement(ifLocation, Collections.emptyList());
    }

    private If searchIfStatement(Location ifLocation, Collection<ICPPElement> additionalSearches) {
        If cppIf = null;

        Stack<ICPPElement> toSearch = new Stack<>();
        toSearch.addAll(additionalSearches);
        toSearch.addAll(resultModel.getCppElements());

        while(!toSearch.isEmpty() && cppIf == null)
        {
            ICPPElement element = toSearch.pop();
            if(element instanceof If)
            {
                if(element.getLocation().equals(ifLocation))
                {
                    cppIf = (If) element;
                } else {
                    If ifElement = (If) element;
                    // Add nested statement to search list
                    toSearch.addAll(ifElement.getElIfs());
                    toSearch.addAll(ifElement.getTrueBranch());
                    toSearch.addAll(ifElement.getElseBranch());
                }
            }
            if(element instanceof MacroElIf)
            {
                toSearch.addAll(((MacroElIf)element).getBranch());
            }
        }

        return cppIf;
    }



    private If searchElseStatement(Location elseLocation, Collection<ICPPElement> additionalSearches) {
        If cppIf = null;

        Stack<ICPPElement> toSearch = new Stack<>();
        toSearch.addAll(additionalSearches);
        toSearch.addAll(resultModel.getCppElements());

        while(!toSearch.isEmpty() && cppIf == null)
        {
            ICPPElement element = toSearch.pop();
            if(element instanceof If)
            {
                If ifElement = (If) element;
                if(ifElement.getElseLocation() != null && ifElement.getElseLocation().equals(elseLocation))
                {
                    cppIf = ifElement;
                } else {
                    // Add nested statement to search list
                    toSearch.addAll(ifElement.getElIfs());
                    toSearch.addAll(ifElement.getTrueBranch());
                    toSearch.addAll(ifElement.getElseBranch());
                }
            }
            if(element instanceof MacroElIf)
            {
                toSearch.addAll(((MacroElIf)element).getBranch());
            }
        }

        return cppIf;
    }

    private MacroElIf searchlElIfStatement(Location ifLocation, Collection<ICPPElement> additionalSearches) {
        MacroElIf cppIf = null;

        Stack<ICPPElement> toSearch = new Stack<>();
        toSearch.addAll(additionalSearches);
        toSearch.addAll(resultModel.getCppElements());

        while(!toSearch.isEmpty() && cppIf == null)
        {
            ICPPElement element = toSearch.pop();
            if(element instanceof If)
            {
                If ifElement = (If) element;
                // Add nested statement to search list
                toSearch.addAll(ifElement.getElIfs());
                toSearch.addAll(ifElement.getTrueBranch());
                toSearch.addAll(ifElement.getElseBranch());
            }
            if(element instanceof MacroElIf)
            {
                if(element.getLocation().equals(ifLocation))
                {
                    cppIf = (MacroElIf) element;
                } else {
                    MacroElIf elifElement = (MacroElIf) element;
                    // Add nested statement to search list
                    toSearch.addAll(elifElement.getBranch());
                }
            }
        }

//		if(cppIf == null)
//		{
//			throw new IllegalStateException("If (Location " + ifLocation + ") not found in results");
//		}
        return cppIf;
    }

    private String replaceCondition(String inputFile, Element ifElement)
            throws IOException, SAXException {
//		System.out.println("Found corresponding #if");


        // Get condition value
        String conditionValue = ifElement.getElementsByTagName("ConditionValue").item(0).getTextContent();

        // Change condition value
        String newConditionValue = "";
        if(conditionValue.equals("CVK_True")) {
            newConditionValue = "0";
        } else if(conditionValue.equals("CVK_False") || conditionValue.equals("CVK_NotEvaluated"))
        {
            newConditionValue = "1";
        } else {
            throw new IllegalStateException();
        }

        // Handle negation
        if(ifElement.getNodeName().equals("Ifndef"))
        {
            if(newConditionValue.equals("0")) {
                newConditionValue = "1";
            } else {
                newConditionValue = "0";
            }
        }

        return setCondition(inputFile, ifElement, newConditionValue);
    }

    private String setCondition(String inputFile, Element ifElement, String newConditionValue) {
        // Get location of condition
        String conditionRangeBegin = ifElement.getElementsByTagName("ConditionRangeBegin").item(0).getTextContent();
        String conditionRangeEnd = ifElement.getElementsByTagName("ConditionRangeEnd").item(0).getTextContent();

        // Transform input
        Location conditionBegin = new Location(conditionRangeBegin);
        Location conditionEnd = new Location(conditionRangeEnd);

        String linesCopy[] = inputFile.split("\\r?\\n");

        String conditionLine = linesCopy[conditionBegin.getLine() - 1];


//		System.out.println("conditionLine " + conditionLine);

        if(conditionEnd.getLine() > conditionBegin.getLine())
        {
            conditionLine = StringUtils.overlay(conditionLine, newConditionValue, conditionBegin.getColumn(), conditionLine.length());
        } else {
            conditionLine = StringUtils.overlay(conditionLine, newConditionValue, conditionBegin.getColumn(), conditionEnd.getColumn() + 1);
        }

        conditionLine = conditionLine.replace("#ifdef", "#if   ");
        conditionLine = conditionLine.replace("#ifndef", "#if    ");
        linesCopy[conditionBegin.getLine() - 1] = conditionLine;

        String replacedInput = StringUtils.join(linesCopy, System.lineSeparator());

        return replacedInput;
    }
}
