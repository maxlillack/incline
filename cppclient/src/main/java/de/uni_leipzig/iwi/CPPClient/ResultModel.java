/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import de.uni_leipzig.iwi.CPPClient.cpplang.*;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class ResultModel {
	private List<ICPPElement> cppElements = new ArrayList<>();
	private final String[] lines;

	public ResultModel(String[] lines) {
		this.lines = lines;
	}

	public List<ICPPElement> getCppElements() {
		return cppElements;
	}

	@Override
	public String toString() {
		List<String> parts = new ArrayList<>();
		for(ICPPElement element : cppElements)
		{
			parts.add(element.toString());
		}
		return StringUtils.join(parts, "\n");
	}

	public void addTextFragments() {
		// Iterate model and identify gaps in their location
		// Fill location with text content from input file

		// Any text before first element?
		List<ICPPElement> elements = getCppElements();

		int lastLine = 0;

		Map<ICPPElement, List<ICPPElement>> addBefore = new HashMap<>();

		// Text before first element
		if(elements.size() > 0 && elements.get(0).getLocation() != null && elements.get(0).getLocation().getLine() > 1) {
			int refLine = elements.get(0).getLocation().getLine();

			List<String> textLines = new ArrayList<>();
			for(int i = 1; i < refLine; i++)
			{
				textLines.add(lines[i - 1]);
			}
			addBefore.put(elements.get(0), Collections.singletonList(new TextContent(null, textLines)));
		}

		for(ICPPElement element : elements) {
			List<ICPPElement> before = new ArrayList<>();
			List<ICPPElement> after = new ArrayList<>();
			lastLine = getTextContentBeforeElement(lines, lastLine, element, before, after);

			addBefore.put(element, before);

			if(after.size() > 0) {
				throw new IllegalStateException("not handled yet");
			}
		}

		for(Map.Entry<ICPPElement, List<ICPPElement>> entry : addBefore.entrySet()) {
			for(ICPPElement e : entry.getValue()) {
				elements.add(elements.indexOf(entry.getKey()), e);
			}
		}


		List<String> textLines = new ArrayList<>();
		for(int i = lastLine + 1; i <= lines.length; i++) {
			textLines.add(lines[i - 1]);
		}

		if(!textLines.isEmpty()) {
			elements.add(elements.size(), new TextContent(null, textLines));
		}
	}

	private int getTextContentBeforeElement(String[] lines, int lastLine, ICPPElement element, List<ICPPElement> before, List<ICPPElement> after) {
		// We assume all elements to whole lines expect MacroExpansion
		if(element instanceof MacroExpands) {

		}
		else if(element.getLocation() != null) {
			int lineNumber = element.getLocation().getLine();

			List<String> textLines = new ArrayList<>();
			for(int i = lastLine + 1; i < lineNumber; i++) {
				textLines.add(lines[i - 1]);
			}
			if(!textLines.isEmpty()) {
				before.add(new TextContent(null, textLines));
			}
//			System.out.println("text content: " + sbPre);

			lastLine = lineNumber;

			if(element instanceof MacroDefined) {
				lastLine = ((MacroDefined) element).getLocationLastToken().getLine();
			}

			if(element instanceof If) {
				If cppIf = (If) element;

				if(cppIf.conditionEnd != null) {
					// location of conditionEnd includes "next" line but no actual content of it, i.e. columns == 0
					if(cppIf.conditionEnd.getColumn() == 1) {
						lastLine = cppIf.conditionEnd.getLine() - 1;
					}
					else {
						lastLine = cppIf.conditionEnd.getLine();
					}

				}

				int refLine = -1;
				if(cppIf.getTrueBranch().isEmpty()) {
					if(!cppIf.getElIfs().isEmpty()) {
						refLine = cppIf.getElIfs().get(0).getLocation().getLine();
					}
					else if(cppIf.getElseLocation() != null) {
						refLine = cppIf.getElseLocation().getLine();
					} else {
						refLine = cppIf.getEndIfLocation().getLine();
					}
				} else if(cppIf.getTrueBranch().get(0).getLocation() != null) {
					refLine = cppIf.getTrueBranch().get(0).getLocation().getLine();
				} else {
					// TODO check logic
					refLine = cppIf.getEndIfLocation().getLine();
				}

				textLines = new ArrayList<>();

				for(int i = lastLine + 1; i < refLine; i++)
				{
					textLines.add(lines[i - 1]);
				}
				if(!textLines.isEmpty()) {
					cppIf.getTrueBranch().add(0, new TextContent(null, textLines));
				}

//				System.out.println("text content: " + sbPre);
				lastLine = refLine;

				Map<ICPPElement, List<ICPPElement>> addBefore = new HashMap<>();
				for(ICPPElement subElement : cppIf.getTrueBranch())
				{
					List<ICPPElement> subBefore = new ArrayList<>();
					// assume no after elements
					lastLine = getTextContentBeforeElement(lines, lastLine, subElement, subBefore, Collections.emptyList());

//					System.out.println("text content: " + sb2);
					addBefore.put(subElement, subBefore);
				}

				for(Map.Entry<ICPPElement, List<ICPPElement>> entry : addBefore.entrySet())
				{
					List<ICPPElement> elements = cppIf.getTrueBranch();
					for(ICPPElement e : entry.getValue())
					{
						elements.add(elements.indexOf(entry.getKey()), e);
					}
				}

				addBefore.clear();

				List<ICPPElement> lastBranch = cppIf.getTrueBranch();


				for(ICPPElement elseIf : cppIf.getElIfs())
				{
					lastLine = elseIf.getLocation().getLine();

					MacroElIf macroElIf = (MacroElIf) elseIf;

					lastBranch = macroElIf.getBranch();

					// Search line ending the current elif: next #elif, #else or #endif
					refLine = -1;
					if(!macroElIf.getBranch().isEmpty())
					{
						refLine = macroElIf.getBranch().get(0).getLocation().getLine();
					} else if(cppIf.getElIfs().indexOf(elseIf) < (cppIf.getElIfs().size() - 1)) {
						refLine = cppIf.getElIfs().get(cppIf.getElIfs().indexOf(elseIf) + 1).getLocation().getLine();
					} else if(cppIf.getElseLocation() != null)
					{
						refLine = cppIf.getElseLocation().getLine();
					} else {
						refLine = cppIf.getEndIfLocation().getLine();
					}
					// Add lines
					textLines = new ArrayList<>();
					for(int i = lastLine + 1; i < refLine; i++)
					{
						textLines.add(lines[i - 1]);
					}
					if(!textLines.isEmpty()) {
						macroElIf.getBranch().add(new TextContent(null, textLines));
					}

					addBefore = new HashMap<>();
					for(ICPPElement subElement : ((MacroElIf) elseIf).getBranch())
					{
						List<ICPPElement> subBefore = new ArrayList<>();
						// assume no after elements
						lastLine = getTextContentBeforeElement(lines, lastLine, subElement, before, Collections.emptyList());
//						System.out.println("text content: " + sb2);
						addBefore.put(subElement, subBefore);
					}
				}

				// @ TODO
				if(!addBefore.isEmpty() && !lastBranch.containsAll(addBefore.values()))
				{
					// Check if elements are already in last branch
					boolean allFound = true;
					for(List<ICPPElement> list : addBefore.values())
					{
						if(!lastBranch.containsAll(list))
						{
							allFound = false;
						}
					}
					if(!allFound) {
						throw new IllegalStateException("TODO implement");
					}
				}

				addBefore = new HashMap<>();



				if(cppIf.getElseLocation() != null)
				{
					// Add lines between lastLine and #else

					textLines = new ArrayList<>();
					for(int i = lastLine + 1; i < cppIf.getElseLocation().getLine(); i++)
					{
						textLines.add(lines[i - 1]);
					}

					if(!textLines.isEmpty()) {
						TextContent textContent = new TextContent(null, textLines);
						lastBranch.add(textContent);
					}

					lastLine = cppIf.getElseLocation().getLine();
					lastBranch = cppIf.getElseBranch();
				}

				for(ICPPElement subElement : cppIf.getElseBranch())
				{
					List<ICPPElement> subBefore = new ArrayList<>();
					// assume no after elements
					lastLine = getTextContentBeforeElement(lines, lastLine, subElement, subBefore, Collections.emptyList());
//					System.out.println("text content: " + sb2);
					addBefore.put(subElement, subBefore);
				}
				for(Map.Entry<ICPPElement, List<ICPPElement>> entry : addBefore.entrySet())
				{
					List<ICPPElement> elements = cppIf.getElseBranch();
					for(ICPPElement e : entry.getValue())
					{
						if(!elements.contains(e)) {
							elements.add(elements.indexOf(entry.getKey()), e);
						}
					}
				}

				textLines = new ArrayList<>();
				for(int i = lastLine + 1; i < cppIf.getEndIfLocation().getLine(); i++)
				{
					textLines.add(lines[i - 1]);
				}

				if(!textLines.isEmpty()) {
					TextContent textContent = new TextContent(null, textLines);
					lastBranch.add(textContent);
				}

//				System.out.println("text content: " + sb);

				lastLine = cppIf.getEndIfLocation().getLine();
			}

		}
		return lastLine;
	}
}
