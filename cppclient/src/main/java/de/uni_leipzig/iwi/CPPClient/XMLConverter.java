/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import de.uni_leipzig.iwi.CPPClient.cpplang.ICPPElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLConverter {

	XPathFactory xPathfactory = XPathFactory.newInstance();
	XPath xpath = xPathfactory.newXPath();

	public void createXML(String name, ResultModel model, File output) throws ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();

		Element root = doc.createElement(name);
		doc.appendChild(root);


		for(ICPPElement element : model.getCppElements()) {
			element.toXML(root);
		}

		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

		transformer.transform(new DOMSource(doc), new StreamResult(output));
		System.out.println("Wrote XML-based AST of " + name + " to: " + output.getAbsolutePath());
	}

	public String createSourceCode(String xmlString) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(xmlString)));

		StringBuilder sb = new StringBuilder();
		NodeList nodes = doc.getDocumentElement().getChildNodes();
		for(int i = 0; i < nodes.getLength(); i++)
		{
			if(nodes.item(i).getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) nodes.item(i);
				visitElement(element, sb);
			}
		}
		return sb.toString();
	}

	private void visitElement(Element element, StringBuilder sb) throws XPathExpressionException
	{
		switch(element.getNodeName())
		{
			case "text":
				String text = element.getTextContent().trim();
				text = text.replace("//#include", "#include");
				sb.append(text + "\n");
				break;
			case "If":
				String condition = element.getAttribute("condition");

				Pattern ifndef = Pattern.compile("^!defined\\((\\w+)\\)$");
				Pattern ifdef = Pattern.compile("^defined\\((\\w+)\\)$");
				Matcher ifndefmatcher = ifndef.matcher(condition);
				Matcher ifdefmatcher = ifdef.matcher(condition);
				if(ifndefmatcher.matches())
				{
					sb.append("#ifndef " + ifndefmatcher.group(1) + "\n");
				}
				else if(ifdefmatcher.matches())
				{
					sb.append("#ifdef " + ifdefmatcher.group(1) + "\n");
				}
				else {
					sb.append("#if " + condition + "\n");
				}

				generateIf(element, sb);
				break;
			case "Ifndef":
				sb.append("#ifndef " + element.getAttribute("condition") + "\n");
				generateIf(element, sb);
				break;
			case "MacroDefined":
				if(element.getAttribute("args").isEmpty()) {
					sb.append("#define " + element.getAttribute("name") + " " + element.getElementsByTagName("content").item(0).getTextContent() + "\n");
				} else {
					sb.append("#define " + element.getAttribute("name") + "(" + element.getAttribute("args") + ") " + element.getElementsByTagName("content").item(0).getTextContent() + "\n");
				}
				break;
			case "true":
				sb.append("// true " + element.toString() + "\n");
				break;
			default:
				throw new IllegalStateException("Unhandled type " + element.getNodeName());
		}
	}

	private void generateIf(Element element, StringBuilder sb) throws XPathExpressionException {

		NodeList trueBranch = (NodeList) xpath.evaluate("true/*", element, XPathConstants.NODESET);
		NodeList elseBranch = (NodeList) xpath.evaluate("else/*", element, XPathConstants.NODESET);

		for(int i = 0; i < trueBranch.getLength(); i++)
		{
			if(trueBranch.item(i).getNodeType() == Node.ELEMENT_NODE)
			{
				Element e = (Element) trueBranch.item(i);
				visitElement(e, sb);
			}
		}


		if(elseBranch.getLength() > 0)
		{
			// #ELSE
			sb.append("#else\n");
			for(int i = 0; i < elseBranch.getLength(); i++)
			{
				if(elseBranch.item(i).getNodeType() == Node.ELEMENT_NODE)
				{
					Element e = (Element) elseBranch.item(i);
					visitElement(e, sb);
				}
			}
		}
		sb.append("#endif /* " + element.getAttribute("condition") + " */\n");
	}

}
