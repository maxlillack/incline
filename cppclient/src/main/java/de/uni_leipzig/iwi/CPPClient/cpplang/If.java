/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient.cpplang;

import de.uni_leipzig.iwi.CPPClient.Location;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;

public class If implements ICPPElement {

	private Location location;
	private String condition;


	public Location conditionEnd;

	private List<ICPPElement> trueBranch = new ArrayList<>();
	private List<ICPPElement> elIfs = new ArrayList<>();
	private List<ICPPElement> elseBranch = new ArrayList<>();

	private Location elseLocation = null;
	private Location endIfLocation = null;

	public If(Location location, String condition)
	{
		this.location = location;
		// We trim() the condition because we've seen tabs at the end of the condition in example ultralcd
		this.condition = condition.trim();

		// Clean (x) to x
		if(this.condition.startsWith("(") && this.condition.endsWith(")") && this.condition.lastIndexOf('(') == 0)
		{
			this.condition = this.condition.substring(1, this.condition.length() - 1);
		}
		if(this.condition.contains(")") && !this.condition.contains("("))
		{
			this.condition = this.condition.replace(")", "");
		}

	}

	@Override
	public Location getLocation() {
		return location;
	}

	public String getCondition()
	{
		return condition;
	}

	public List<ICPPElement> getTrueBranch() {
		return trueBranch;
	}

	public List<ICPPElement> getElseBranch() {
		return elseBranch;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("If [location=" + location + "]\n");

		for(ICPPElement e : getTrueBranch())
		{
			for(String line : e.toString().split("\\r?\\n"))
			{
				// Add indent
				sb.append("  " + line + "\n");
			}
		}

		for(ICPPElement elIf : getElIfs())
		{
			sb.append(elIf.toString() + "\n");
		}

		if(elseLocation != null)
		{
			sb.append("Else [location=" + getElseLocation() + "]\n");
			for(ICPPElement e : elseBranch)
			{
				sb.append("  " + e + "\n");
			}
		}
		sb.append("Endif [location=" + getEndIfLocation() + "]");

		return sb.toString();
	}

	public Location getElseLocation() {
		return elseLocation;
	}

	public void setElseLocation(Location elseLocation) {
		this.elseLocation = elseLocation;
	}

	public Location getEndIfLocation() {
		return endIfLocation;
	}

	public void setEndIfLocation(Location endIfLocation) {
		this.endIfLocation = endIfLocation;
	}

	public void addElIf(MacroElIf elIf) {
		if(!elIfs.contains(elIf)) {
			elIfs.add(elIf);
		}
	}

	public List<ICPPElement> getElIfs() {
		return elIfs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		If other = (If) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}

	@Override
	public void toXML(Element parent) {

		Document doc = parent.getOwnerDocument();
		Element element = doc.createElement("If");

		element.setAttribute("condition", condition);

		Element trueBranch = doc.createElement("true");
		for(ICPPElement trueElement : getTrueBranch())
		{
			trueElement.toXML(trueBranch);
		}
		element.appendChild(trueBranch);

		for(ICPPElement elseIfElement : getElIfs())
		{
			elseIfElement.toXML(element);
		}

		if(elseLocation != null)
		{
			Element elseBranch = doc.createElement("else");
			for(ICPPElement elseElement : getElseBranch())
			{
				elseElement.toXML(elseBranch);
			}
			element.appendChild(elseBranch);
		}

		parent.appendChild(element);
	}

	public boolean hasElse() {
		return elseLocation != null;
	}

}
