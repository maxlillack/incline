/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient.cpplang;

import de.uni_leipzig.iwi.CPPClient.Location;
import org.w3c.dom.Element;

public class InclusionDirective implements ICPPElement {
	private Location location;
	private String fileName;
	private boolean isAngled;

	public InclusionDirective(Location location, String fileName, boolean isAngled) {
		this.location = location;
		this.fileName = fileName;
		this.isAngled = isAngled;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	public String getFileName() {
		return fileName;
	}

	public boolean isAngled() {
		return isAngled;
	}

	@Override
	public void toXML(Element parent) {
		parent.appendChild(parent.getOwnerDocument().createElement("include"));
	}

}
