#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#ifdef abs
#undef abs
#endif
#define abs(x) ((x)>0?(x):-(x))
#define MB(board) (MOTHERBOARD==BOARD_board)

// Macros to support option testing
#define _CAT(a, ...) a ## __VA_ARGS__
#define SWITCH_ENABLED_false 0
#define SWITCH_ENABLED_true  1
#define SWITCH_ENABLED_0     0
#define SWITCH_ENABLED_1     1
#define SWITCH_ENABLED_      1
#define ENABLED(b) _CAT(SWITCH_ENABLED_, b)
#define DISABLED(b) (!_CAT(SWITCH_ENABLED_, b))

#define PIN_EXISTS(PN) (defined(PN ##_PIN) && PN ##_PIN >= 0)

#define BUTTON_EXISTS(BN) (defined(BTN_## BN) && BTN_## BN >= 0)

#define KERNEL_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))