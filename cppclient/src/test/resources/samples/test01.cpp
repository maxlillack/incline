#include <iostream>

#define YOLO int

void foo(YOLO i)
{
    std::cout << "Foo\n";
}

int main()
{
    std::cout << "Hello, world!\n";
#if y
    int a = 0;
#else
    #define BAR barfoo
    #if x
      int yolo = 0;
    #endif
    int b = 0;
#endif
}