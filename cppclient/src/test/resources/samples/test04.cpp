void suicide()
{
#ifndef FORK
#ifdef SUICIDE_PIN
#if SUICIDE_PIN> -1
      SET_OUTPUT(SUICIDE_PIN);
      WRITE(SUICIDE_PIN, LOW);
#endif /* SUICIDE_PIN> -1 */
#endif /* defined(SUICIDE_PIN) */
#else
#if defined(SUICIDE_PIN) && SUICIDE_PIN > -1
    SET_OUTPUT(SUICIDE_PIN);
    WRITE(SUICIDE_PIN, LOW);
#endif /* defined(SUICIDE_PIN) && SUICIDE_PIN > -1 */
}

void servo_init()
{
#if (NUM_SERVOS >= 1) && defined(SERVO0_PIN) && (SERVO0_PIN > -1)
    servos[0].attach(SERVO0_PIN);
#endif /* (NUM_SERVOS >= 1) && defined(SERVO0_PIN) && (SERVO0_PIN > -1) */
#if (NUM_SERVOS >= 2) && defined(SERVO1_PIN) && (SERVO1_PIN > -1)
    servos[1].attach(SERVO1_PIN);
#endif /* (NUM_SERVOS >= 2) && defined(SERVO1_PIN) && (SERVO1_PIN > -1) */
#if (NUM_SERVOS >= 3) && defined(SERVO2_PIN) && (SERVO2_PIN > -1)
    servos[2].attach(SERVO2_PIN);
#endif /* (NUM_SERVOS >= 3) && defined(SERVO2_PIN) && (SERVO2_PIN > -1) */
#if (NUM_SERVOS >= 4) && defined(SERVO3_PIN) && (SERVO3_PIN > -1)
    servos[3].attach(SERVO3_PIN);
#endif /* (NUM_SERVOS >= 4) && defined(SERVO3_PIN) && (SERVO3_PIN > -1) */
#if NUM_SERVOS >= 5
    //#error "TODO: enter initalisation code for more servos"
#endif /* NUM_SERVOS >= 5 */

  // Set position of Servo Endstops that are defined
#ifdef SERVO_ENDSTOPS
  for(int8_t i = 0; i < 3; i++)
  {
    if(servo_endstops[i] > -1) {
      servos[servo_endstops[i]].write(servo_endstop_angles[i * 2 + 1]);
    }
  }
#endif /* defined(SERVO_ENDSTOPS) */
#endif /* !defined(FORK) */
}