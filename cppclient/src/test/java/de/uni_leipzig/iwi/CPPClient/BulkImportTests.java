/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class BulkImportTests {
    private Set<File> blacklist = new HashSet<File>() {{
        add(new File("samples/bulk/e63e224/result_Configuration.h")); // error: unterminated conditional directive #ifndef __CONFIGURATION_H (very strange)
//        add(new File("samples/bulk/2daa859/remote_Marlin_main.cpp")); // #ifdef SERVO_ENDSTOPS[axis] > -1
//        add(new File("samples/bulk/2daa859/result_Marlin_main.cpp")); // #ifdef SERVO_ENDSTOPS[axis] > -1
//        add(new File("samples/bulk/2309c9f/remote_language.h")); // TODO: import kicks in too late
//        add(new File("samples/bulk/2309c9f/base_language.h")); // TODO: import kicks in too late
//        add(new File("samples/bulk/2309c9f/result_language.h")); // TODO: import kicks in too late
//        add(new File("samples/bulk/47c1ea7/remote_temperature.cpp")); // function-like macro ARRAY_BY_EXTRUDERS invoked with wrong parameter count
//        add(new File("samples/bulk/bd96d22/remote_Marlin_main.cpp")); // #ifdef SERVO_ENDSTOPS[axis] > -1
//        add(new File("samples/bulk/bd96d22/result_Marlin_main.cpp")); // #ifdef SERVO_ENDSTOPS[axis] > -1

    }};

    @Test
    public void asdf() throws Exception {
        CPPClient cppClient = new CPPClient();
        cppClient.start(new File("/home/wilhelm/develop/cpponmps/PPMerge/examples/output/Marlin_main_cleaned_373f3ec_integrated.cpp"));
        cppClient.start(new File("/home/wilhelm/develop/cpponmps/PPMerge/examples/output/ultralcd_46f80e8_integrated.h"));
        cppClient.start(new File("/home/wilhelm/develop/cpponmps/PPMerge/examples/output/temperature_47c1ea7_integrated.cpp"));
        cppClient.start(new File("/home/wilhelm/develop/cpponmps/PPMerge/examples/output/Marlin_main_cleaned_jcrocholl_integrated.cpp"));
    }

    @Test
    public void runBulkTests() {
        File bulkDirectory = new File("samples/bulk");
        for (File directory : bulkDirectory.listFiles()) {
            System.out.println("Directory: " + directory.toString());
            for (File importFile : directory.listFiles()) {
                if (!blacklist.contains(importFile)) {
                    System.out.println("  File: " + importFile.toString());
                    try {
                        CPPClient cppClient = new CPPClient();
                        cppClient.start(importFile);
                    } catch (ParserConfigurationException | IOException | XPathExpressionException | SAXException e) {
                        throw new RuntimeException(importFile.toString(), e);
                    }
                    System.out.println("  Finished.");
                } else {
                    System.out.println("--Blacklist: skipped: " + importFile.toString());
                }
            }
        }
    }
}
