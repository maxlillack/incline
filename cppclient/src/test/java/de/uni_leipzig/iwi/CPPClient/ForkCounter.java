/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import de.uni_leipzig.iwi.CPPClient.cpplang.ICPPElement;
import de.uni_leipzig.iwi.CPPClient.cpplang.If;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForkCounter {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        calculate(MarlinFiles.ESE1);
        calculate(MarlinFiles.ESE2);
        calculate(MarlinFiles.ESE3);
    }

    private static void calculate(MarlinFiles mf) throws ParserConfigurationException, IOException, XPathExpressionException, SAXException {
        CPPClient cppClient = new CPPClient();
        ResultModel output = cppClient.start(mf.getContents());
        List<If> allForkIfdefs = findIfdefForks(output.getCppElements(), true);
        int chunks = allForkIfdefs.size();
        List<If> topLevelForkIfdefs = findIfdefForks(output.getCppElements(), false);
        int vloc = 0;
        for (If ifCond: topLevelForkIfdefs) {
            for (ICPPElement trueElem: ifCond.getTrueBranch()) {
                vloc += newlineCounter(trueElem.toString());
            }
            if (ifCond.hasElse()) {
                for (ICPPElement falseElem: ifCond.getElseBranch()) {
                    vloc += newlineCounter(falseElem.toString());
                }
            }
        }
        int loc = newlineCounter(mf.getContents());

        System.out.println("NAME: " + mf.name());
        System.out.println("LOC: " + loc);
        System.out.println("CHUNKS: " + chunks);
        System.out.println("VLOC: " + vloc);
        System.out.println(loc + " & " + chunks + " & " + vloc);
    }

    private static List<If> findIfdefForks(List<ICPPElement> elements, boolean recursive) {
        List<If> allForkIfdefs = new LinkedList<>();
        for (ICPPElement element: elements) {
            if (element instanceof If) {
                If ifElement = (If) element;
                if (ifElement.getCondition().contains("FORK")) {
                    allForkIfdefs.add(ifElement);
                    if (recursive) {
                        allForkIfdefs.addAll(findIfdefForks(ifElement.getTrueBranch(), true));
                        if (ifElement.hasElse()) {
                            allForkIfdefs.addAll(findIfdefForks(ifElement.getElseBranch(), true));
                        }
                    }
                }
                else {
                    allForkIfdefs.addAll(findIfdefForks(ifElement.getTrueBranch(), true));
                    if (ifElement.hasElse()) {
                        allForkIfdefs.addAll(findIfdefForks(ifElement.getElseBranch(), true));
                    }
                }
            }
        }
        return allForkIfdefs;
    }

    private static int newlineCounter(String input) {
        Matcher m = Pattern.compile("\r\n|\r|\n").matcher(input);
        int lines = 1;
        while (m.find()) {
            lines++;
        }
        return lines;
    }
}
