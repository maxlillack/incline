/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;

enum ExternalFiles implements SampleFile {
    INPUT7_A("ppmerge/astmerge/src/main/resources/examples/marlin", "input7-a.cpp"),
    CHANGE_01("Samples/Change01", "integrated.pde"),
    CHANGE_02("Samples/Change02", "integrated_pins.h"),
    CHANGE_05("Samples/Change05", "integrated_dogm_lcd_implementation.h"),
    MARLIN_MAIN_MERGED("Samples", "Marlin_main_merged.cpp"),
    PLANNER_MERGED("Samples", "planner_merged.cpp"),
    ULTRALCD_MERGED("Samples", "ultralcd_merged.cpp"),
    SLIDE_CASE("Samples", "SlideCase01.cpp"),
    ULTRALCD_MAINLINE("ppmerge/astmerge/src/main/resources/examples", "ultralcd_mainline.cpp"),
    ULTRALCD_TEST4_MAINLINE("ppmerge/astmerge/src/main/resources/examples", "ultralcd_test4_mainline.cpp"),
    ULTRALCD_TEST9_MAINLINE("ppmerge/astmerge/src/main/resources/examples", "ultralcd_test9_mainline.cpp"),
    HELLO("CPPXMLPlugin/example", "hello.cpp");

    private String path;
    private String filename;

    ExternalFiles(String path, String filename) {
        this.path = path;
        this.filename = filename;
    }

    public String getContents() throws IOException {
        File projectRoot = new File(new File("").getAbsolutePath()).getParentFile();
        File fileLocation = new File(path, filename);
        File fullLocation = new File(projectRoot, fileLocation.getPath());
        return IOUtils.toString(fullLocation.toURI());
    }

    public String getExpectedContent() throws IOException {
        File expectedFileLocation = new File("samples/expected", filename + "_expected");
        return IOHelper.getFileContents(expectedFileLocation, getClass().getClassLoader());
    }
}