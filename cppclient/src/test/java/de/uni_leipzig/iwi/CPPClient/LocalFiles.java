/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;


import java.io.File;
import java.io.IOException;

enum LocalFiles implements SampleFile {
    CONDITIONAL_DEFINE("ConditionalDefine.cpp"),
    CONDITIONAL_DEFINE_2("ConditionalDefine2.cpp"),
    MACRODEF("macrodef.cpp"),
    ORIG_THINKYHEAD_PLANNER("orig-thinkyhead-planner.cpp"),
    ORIG_THINKYHEAD_PLANNER_BOOL("orig-thinkyhead-planner-bool.cpp"),
    PART_OF_DOGM_LCD_IMPLEMENTATION("part_of_dogm_lcd_implementation.h"),
    STEPPER("stepper.h"),
    STEPPER_ORIG_PLUS_CLONE_VERSION2("stepper_orig_plus_clone_version2.cpp"),
    TEST_01("test01.cpp"),
    TEST_01_EDITED("test01Edited.cpp"),
    TEST_02("test02.cpp"),
    TEST_03("test03.h"),
    TEST_04("test04.cpp"),
    TEST_ElIf("TestElIf.cpp"),
    IFCONDITION_COMMENT("ifconditioncomment.cpp"),
    REALISTIC_MARLIN_MAIN("realistic_Marlin_main.cpp"),
    ERROR_STATEMENT("error_statement.cpp"),
    LIBBB("libbb.h");

    private static final String path = "samples";
    private String filename;

    LocalFiles(String filename) {
        this.filename = filename;
    }

    public String getContents() throws IOException {
        File location = new File(path, filename);
        return IOHelper.getFileContents(location, getClass().getClassLoader());
    }

    public String getExpectedContent() throws IOException {
        File expectedFileLocation = new File(path + "/expected", filename + "_expected");
        return IOHelper.getFileContents(expectedFileLocation, getClass().getClassLoader());
    }

}
