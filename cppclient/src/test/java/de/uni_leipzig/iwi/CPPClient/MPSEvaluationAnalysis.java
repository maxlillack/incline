/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MPSEvaluationAnalysis {

	private List<String> lines;
	private String task;
	private String filename;
	private static final String CSVNAME = "/home/wilhelm/develop/marlin-mining/controlled-experiment/mps-analysis.csv";
	private static final String PATH_PREFIX = "/home/wilhelm/develop/marlin-mining/controlled-experiment/";

	private int intentionKeep = 0;
	private int intentionKeepAsFeature = 0;
	private int intentionRemove = 0;
	private int nextChunk = 0;
	private int useOfAntiIntention = 0;
	private int undoIntention = 0;
	private int changePC = 0;
	private int exclusiveIntention = 0;

	public static void main(String[] args) throws IOException {
		doIT("group2/D2-10_idea.log", "D2-10", "vim.c");

	}

	private static void doIT(String path_suffix, String task, String filename) {
		try {
			MPSEvaluationAnalysis eval = new MPSEvaluationAnalysis(path_suffix, task, filename);
			eval.dumpStatistics();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public MPSEvaluationAnalysis(String path_suffix, String task, String filename) throws IOException {
		this.lines = FileUtils.readLines(new File(PATH_PREFIX + path_suffix), "UTF-8");
		this.task = task;
		this.filename = filename;
	}

	private void dumpStatistics() throws FileNotFoundException {

		
		Pattern keepNodeCountPattern = Pattern.compile("Intention Keep for (\\d+) nodes");
		Pattern keepView = Pattern.compile("View of Keep Intention is (\\w+)");
		Pattern removeIntention = Pattern.compile("Remove Intention for (\\d+) nodes");
		Pattern keepAsFeatureNodeCount = Pattern.compile("Intention Keep As Feature for (\\d+) nodes");
		
		Map<String, Integer> keepViewUsage = new HashMap<>();
		Map<String, Integer> keepAsFeatureViewUsage = new HashMap<>();
		Map<String, Integer> removeViewUsage = new HashMap<>();
		
		// Failed to log changePC directly. Analyze log when executed after commit
//		boolean inCommit = false;
//
//		for(String line : lines)
//		{
//			if(line.contains("Commit Intentions"))
//			{
//				inCommit = true;
//			}
//			if(inCommit && line.contains("Execute intention ChangePC"))
//			{
//				changePC++;
//			}
//			if(line.contains("Saving model"))
//			{
//				inCommit = false;
//			}
//
//		}
		
		// Process lines
		while(!lines.isEmpty())
		{
			String line = lines.remove(0);
			if(line.contains("[LOG] Intention Keep As Feature"))
			{
				
				Matcher matcher =  keepAsFeatureNodeCount.matcher(line);
				if(!matcher.find())
				{
					System.err.println(line);
				}
//				System.out.println("KeepAsFeature node count " + matcher.group(1));
				
				while(!lines.isEmpty() && !lines.get(0).contains("[LOG]"))
				{
					// Skip non-LOG lines
					lines.remove(0);
				}
				
				// Log with used view
				matcher = keepView.matcher(lines.get(0));
				if(matcher.find())
				{
					intentionKeepAsFeature++;
					String nextLine = lines.remove(0);
					String view = matcher.group(1);
					keepAsFeatureViewUsage.merge(view, 1, (oldValue, newValue) -> oldValue + newValue);
//					System.out.println("KeepAsFeature view " + view);
				} else {
//					System.out.println("Canceled KeepAsFeature");
//					throw new IllegalStateException("Expected log with used view but found " + nextLine);
				}
			}
			else if(line.contains("[LOG] Intention Keep")) {
				intentionKeep++;
				Matcher matcher =  keepNodeCountPattern.matcher(line);
				if(!matcher.find())
				{
					System.err.println(line);
				}
//				System.out.println("Keep node count " + matcher.group(1));
				
				// Log with used view
				while(!lines.isEmpty() && !lines.get(0).contains("[LOG]"))
				{
					// Skip non-LOG lines
					lines.remove(0);
				}
				String nextLine = lines.remove(0);
				
				matcher = keepView.matcher(nextLine);
				if(matcher.find())
				{
					String view = matcher.group(1);
					keepViewUsage.merge(view, 1, (oldValue, newValue) -> oldValue + newValue);
//					System.out.println("Keep view " + view);
				} else {
					throw new IllegalStateException("Expected log with used view but found " + nextLine);
				}
			}		
			else if(line.contains("[LOG] Exclusive Intention"))
			{
				exclusiveIntention++;
			}
			else if(line.contains("[LOG] Cancel Exclusive intention"))
			{
				// Nothing
			} 
			else if(line.contains("[LOG] Arrange Views"))
			{
				// Nothing
			} 
			else if(line.contains("[LOG] Export"))
			{
				System.out.println("Export");
			}
			else if(line.contains("Propose anti Intention"))
			{
				while(!lines.isEmpty() && !lines.get(0).contains("[LOG]"))
				{
					// Skip non-LOG lines
					lines.remove(0);
				}
				
				boolean usedAntiIntention = false;
				while(!lines.isEmpty() && lines.get(0).contains("[LOG] Add anti intention"))
				{
					String next = lines.remove(0);
					usedAntiIntention = true;
				}
				if(!lines.isEmpty() && lines.get(0).contains("[LOG] No anti intention"))
				{
					String next = lines.remove(0);
					usedAntiIntention = false;
				}
				if(usedAntiIntention)
				{
					useOfAntiIntention++;
//					System.out.println("Used Anti Intention");
				}
				
			}
			else if(line.contains("[LOG] Remove Intention"))
			{
				intentionRemove++;
				Matcher matcher =  removeIntention.matcher(line);
				matcher.find();
//				System.out.println("Remove node count " + matcher.group(1));
				
				// Log with used view (note the bug: Log entry references Keep)
				String nextLine = lines.remove(0);
				matcher = keepView.matcher(nextLine);
				if(matcher.find())
				{
					String view = matcher.group(1);
					removeViewUsage.merge(view, 1, (oldValue, newValue) -> oldValue + newValue);
//					System.out.println("Remove view " + view);
				} else {
					throw new IllegalStateException("Expected log with used view but found " + nextLine);
				}
			}
			else if(line.contains("[LOG] Un-declare an intention"))
			{
				while(!lines.isEmpty() && lines.get(0).contains("[LOG] Undeclare"))
				{
					String next = lines.remove(0);
				}
//				System.out.println("Undo intention");
				undoIntention++;
			}
			else if(line.contains("[LOG] Next Chunk"))
			{
//				System.out.println("Next Chunk");
				nextChunk++;
			}
			else if(line.contains("[LOG] Import"))
			{

			}
			else if(line.contains("[LOG] Order Intention for 0 nodes"))
			{
				// No nodes, intention was canceled
			}
			else if(line.contains("[LOG] GenericKeepForkOnly"))
			{
				// deprecated
			}
			else if(line.contains("[LOG] GenericKeepMainlineOnly"))
			{
				// deprecated
			}
			else if (line.contains("[LOG] Intention ChangePC")) {
				changePC++;
			}
			else if(line.contains("[LOG]"))
			{
				throw new IllegalStateException("Unhandled log type in line " + line.trim());
			}
		}

		printSummary(task, filename, intentionKeep, intentionKeepAsFeature, intentionRemove, nextChunk, useOfAntiIntention, undoIntention, changePC, exclusiveIntention, keepViewUsage, keepAsFeatureViewUsage, removeViewUsage);
		writeSummary();
	}

	private void writeSummary() throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new FileOutputStream(new File(CSVNAME), true));
		StringBuilder sb = new StringBuilder();
		writecol(sb, task);
		writecol(sb, filename);
		writecol(sb, intentionKeep);
		writecol(sb, intentionRemove);
		writecol(sb, intentionKeepAsFeature);
		writecol(sb, changePC);
		writecol(sb, useOfAntiIntention);
		sb.append(undoIntention);
		sb.append('\n');

		pw.append(sb.toString());
		pw.close();
		System.out.println("Wrote row");
	}

	private void writecol(StringBuilder sb, int value) {
		writecol(sb, Integer.toString(value));
	}

	private void writecol(StringBuilder sb, String text) {
		sb.append(text);
		sb.append(',');
	}

	private static void printSummary(String task, String filename, int intentionKeep, int intentionKeepAsFeature, int intentionRemove, int nextChunk, int useOfAntiIntention, int undoIntention, int changePC, int exclusiveIntention, Map<String, Integer> keepViewUsage, Map<String, Integer> keepAsFeatureViewUsage, Map<String, Integer> removeViewUsage) {
		System.out.println("=== Summary Task " + task + " File " + filename);
		System.out.println("   Intention Keep " + intentionKeep);
		System.out.println("   Intention Keep As Feature " + intentionKeepAsFeature);
		System.out.println("   Intention Remove " + intentionRemove);
		System.out.println("   Intention Exclusive " + exclusiveIntention);
		System.out.println("   Next Chunk " + nextChunk);
		System.out.println("   useOfAntiIntention " + useOfAntiIntention);
		System.out.println("   changePC " + changePC);
		System.out.println("   Undo intention " + undoIntention);

		System.out.print("   Keep view usage:");
		for(Entry<String, Integer> entry : keepViewUsage.entrySet())
		{
			System.out.print(" View " + entry.getKey() + " = " + entry.getValue() + ",");
		}
		System.out.println("");

		System.out.print("   KeepAsFeature view usage:");
		for(Entry<String, Integer> entry : keepAsFeatureViewUsage.entrySet())
		{
			System.out.print(" View " + entry.getKey() + " = " + entry.getValue() + ",");
		}
		System.out.println("");

		System.out.print("   Remove view usage:");
		for(Entry<String, Integer> entry : removeViewUsage.entrySet())
		{
			System.out.print(" View " + entry.getKey() + " = " + entry.getValue() + ",");
		}
		System.out.println("");
	}
}
