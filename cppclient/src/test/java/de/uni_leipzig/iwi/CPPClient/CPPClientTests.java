/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class CPPClientTests {

    @Test
    public void test01() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.TEST_01);
    }


    @Test
    public void test01xml() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException, TransformerFactoryConfigurationError, TransformerException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(LocalFiles.TEST_01.getContents());
        new XMLConverter().createXML("test1", model, new File("/tmp/", "test1.xml"));
    }

    @Test
    public void test02() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(LocalFiles.TEST_02.getContents());
        System.out.println(model);
    }

    @Test
    public void test03() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(LocalFiles.TEST_03.getContents());
        System.out.println(model);
    }

    @Test
    public void testinput7() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.INPUT7_A);
    }

    @Test
    public void test04() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(LocalFiles.TEST_04.getContents());
        System.out.println(model);
    }

    @Test
    public void ConditionalDefine() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.CONDITIONAL_DEFINE);
    }

    @Test
    public void ConditionalDefine2() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.CONDITIONAL_DEFINE_2);
    }

    @Test
    public void TestElIf() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.TEST_ElIf);
    }

    @Test
    public void part_of_dogm_lcd_implementation() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.PART_OF_DOGM_LCD_IMPLEMENTATION);
    }

    @Test
    public void thinkyheadPlanner() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(LocalFiles.ORIG_THINKYHEAD_PLANNER.getContents());
        System.out.println(model);
    }

    @Test
    public void testStepper() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.STEPPER);
    }

    @Test
    public void test_stepper_orig_plus_clone_version2() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.STEPPER_ORIG_PLUS_CLONE_VERSION2);
    }

    @Test
    public void macrodef() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.MACRODEF);
    }

    @Test
    public void ifconditioncomment() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(LocalFiles.IFCONDITION_COMMENT);
    }

    @Test
    public void Change01() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.CHANGE_01);
    }

    @Test
    public void Change02() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.CHANGE_02);
    }

    @Test
    public void Change05() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.CHANGE_05);
    }

    @Test
    public void Hello() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.HELLO);
    }

    @Test
    public void MarlinMainSample() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(ExternalFiles.MARLIN_MAIN_MERGED.getContents());
        System.out.println(model);
    }

    @Test
    public void PlannerSample() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.PLANNER_MERGED);
    }

    @Test
    public void UltralcdSampleMerged() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(ExternalFiles.ULTRALCD_MERGED.getContents());
        System.out.println(model);
    }

    @Test
    public void UltralcdSampleMainline() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.ULTRALCD_MAINLINE);
    }

    @Test
    public void ultralcd_test4_mainline() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        ResultModel model = client.start(ExternalFiles.ULTRALCD_TEST4_MAINLINE.getContents());
        System.out.println(model);
    }

    @Test
    public void ultralcd_test9_mainline() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.ULTRALCD_TEST9_MAINLINE);
    }

    @Test
    public void SlideCase01() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        assertModelEquality(ExternalFiles.SLIDE_CASE);
    }

    @Test
    public void test_error_statement() throws Exception { assertModelEquality(LocalFiles.ERROR_STATEMENT);
    }

    @Test
    public void exercise_Marlin_main() throws Exception { assertModelEquality(LocalFiles.REALISTIC_MARLIN_MAIN);
    }

    @Test
    public void task_integrated() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        // TODO: Ensure that the required files exist/create them
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "ultralcd_2daa859_integrated.cpp"));
//		}
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "Marlin_main_cleaned_2daa859_integrated.cpp"));
//		}
//		
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "ultralcd_implementation_hitachi_HD44780_08856d9_integrated.h"));
//		}
		{
			CPPClient client = new CPPClient();
			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "ultralcd_esenapaj_integrated.cpp"));
		}
		{
			CPPClient client = new CPPClient();
			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "stepper_esenapaj_integrated.cpp"));
		}
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "Marlin_main_esenapaj_integrated.cpp"));
//		}
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "temperature_3116271_integrated.h"));
//		} 
        {
            CPPClient client = new CPPClient();
            client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "stepper_cleaned_Marlin_STM32_integrated.cpp"));
        }
        {
            CPPClient client = new CPPClient();
            client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "planner_cleaned_Marlin_STM32_integrated.cpp"));
        }
        {
            CPPClient client = new CPPClient();
            client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "Marlin_main_cleaned_Marlin_STM32_integrated.cpp"));
        }
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "cardreader_17de96a_integrated.cpp"));
//		}
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "Marlin_main_jcrocholl_integrated.cpp"));
//		}
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "ultralcd_46f80e8_integrated.h"));
//		}
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "temperature_47c1ea7_integrated.cpp"));
//		}
//		{
//			CPPClient client = new CPPClient();
//			client.start(new File("ppmerge/astmerge/src/main/resources/examples/output", "Marlin_main_373f3ec_integrated.cpp"));
//		}
    }

    @Test
    public void parseBusybox() throws Exception {
        assertModelEquality(LocalFiles.LIBBB);
    }

    private void assertModelEquality(SampleFile file) throws IOException, XPathExpressionException, SAXException, ParserConfigurationException { CPPClient client = new CPPClient();
        ResultModel model = client.start(file.getContents());
        String expectedContent = file.getExpectedContent();
        assertEquals(expectedContent, model.toString());
    }

    @Test
    public void vts() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        CPPClient client = new CPPClient();
        File vts = new File("C:/Users/Max/temp/vts.txt");
        if(vts.exists())
        {
            ResultModel model = client.start(vts);
            System.out.println(model);
        }
    }

    @Test
    public void importTasks() throws Exception {

        List<File> files = new ArrayList<>();
//		files.add(new File("../../marlin-mining/examples/tasks/2daa859/base/Marlin_main.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/2daa859/base/ultralcd.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/2daa859/remote/Marlin_main.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/2daa859/remote/ultralcd.cpp"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/17de96a/base/cardreader.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/17de96a/remote/cardreader.cpp"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/46f80e8/base/ultralcd.h"));
//		files.add(new File("../../marlin-mining/examples/tasks/46f80e8/remote/ultralcd.h"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/47c1ea7/base/temperature.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/47c1ea7/remote/temperature.cpp"));

//		files.add(new File("../../marlin-mining/examples/tasks/373f3ec/base/Marlin_main.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/373f3ec/remote/Marlin_main.cpp"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/08856d9/base/ultralcd_implementation_hitachi_HD44780.h"));
//		files.add(new File("../../marlin-mining/examples/tasks/08856d9/remote/ultralcd_implementation_hitachi_HD44780.h"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/3116271/base/temperature.h"));
//		files.add(new File("../../marlin-mining/examples/tasks/3116271/remote/temperature.h"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/esenapaj/mainline/Marlin_main.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/esenapaj/fork/Marlin_main.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/esenapaj/mainline/stepper.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/esenapaj/fork/stepper.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/esenapaj/mainline/ultralcd.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/esenapaj/fork/ultralcd.cpp"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/jcrocholl/mainline/Marlin_main.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/jcrocholl/fork/Marlin_main.cpp"));
//		
//		files.add(new File("../../marlin-mining/examples/tasks/Marlin_STM32/mainline/Marlin_main_cleaned.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/Marlin_STM32/fork/Marlin_main_cleaned.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/Marlin_STM32/mainline/planner_cleaned.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/Marlin_STM32/fork/planner_cleaned.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/Marlin_STM32/mainline/stepper_cleaned.cpp"));
//		files.add(new File("../../marlin-mining/examples/tasks/Marlin_STM32/fork/stepper_cleaned.cpp"));
//		
        int imported = 0;
        for (File file : files.subList(0, files.size())) {
            if(!file.exists())
            {
                throw new IllegalStateException("File " + file.getAbsolutePath() + " does not exist.");
            }
            try {
                CPPClient client = new CPPClient();
                client.start(file);
            } catch(Exception e)
            {
                System.err.println(imported + " Error at " + file.getAbsolutePath());
                throw e;
            }
            imported++;
        }
        System.out.println("Imported " + imported + " of " + files.size() + " files.");


    }
}

