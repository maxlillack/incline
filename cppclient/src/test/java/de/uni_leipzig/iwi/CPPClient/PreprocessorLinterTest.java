/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PreprocessorLinterTest {
    @Test
    public void commentIncludeStatements() throws Exception {
        String input = "//regular stuff\n" +
                "#include \"Marlin.h\"\n" +
                "# include <sys/sysmacros.h>\n" +
                "#error \"Marlin.h\"\n";

        String expectedOutput = "//regular stuff\n" +
                "//#include \"Marlin.h\"\n" +
                "//#include <sys/sysmacros.h>\n" +
                "//#error \"Marlin.h\"\n";

        String output = PreprocessorLinter.adjustIncludes(input);

        assertEquals(expectedOutput, output);
    }

    @Test
    public void rewritePreprocessorStatementsWithSpaces() throws Exception {
        String input = "# define sigfillset(s)    __sigfillset(s)\n" +
                "# if !ENABLE_USE_BB_SHADOW\n" +
                "# ifdef TASK_COMM_LEN\n" +
                "# elif defined __UCLIBC__\n" +
                "# else\n" +
                "# endif\n";

        String expected = "#define sigfillset(s)    __sigfillset(s)\n" +
                "#if !ENABLE_USE_BB_SHADOW\n" +
                "#ifdef TASK_COMM_LEN\n" +
                "#elif defined __UCLIBC__\n" +
                "#else\n" +
                "#endif\n";

        String output = PreprocessorLinter.rewriteStatementsWithSpaces(input);

        assertEquals(expected, output);
    }

    @Test
    public void removeTrailingSpaces() throws Exception {
        String input = "/*\n" +
                "  planner.c - buffers movement commands and manages the acceleration profile plan         \n" +
                " Part of Grbl\n" +
                " \n" +
                " Copyright (c) 2009-2011 Simen Svale Skogsrud\n" +
                " \n" +
                " Grbl is free software: you can redistribute it and/or modify  \n" +
                " it under the terms of the GNU General Public License as published by\n" +
                " the Free Software Foundation, either version 3 of the License, or \n" +
                " (at your option) any later version.\n" +
                " \n" +
                " Grbl is distributed in the hope that it will be useful,\n" +
                " but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
                " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
                " GNU General Public License for more details.\n" +
                " \n" +
                " You should have received a copy of the GNU General Public License  \n" +
                " along with Grbl.  If not, see <http://www.gnu.org/licenses/>.\n" +
                " */\n";

        String expected = "/*\n" +
                "  planner.c - buffers movement commands and manages the acceleration profile plan\n" +
                " Part of Grbl\n" +
                "\n" +
                " Copyright (c) 2009-2011 Simen Svale Skogsrud\n" +
                "\n" +
                " Grbl is free software: you can redistribute it and/or modify\n" +
                " it under the terms of the GNU General Public License as published by\n" +
                " the Free Software Foundation, either version 3 of the License, or\n" +
                " (at your option) any later version.\n" +
                "\n" +
                " Grbl is distributed in the hope that it will be useful,\n" +
                " but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
                " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
                " GNU General Public License for more details.\n" +
                "\n" +
                " You should have received a copy of the GNU General Public License\n" +
                " along with Grbl.  If not, see <http://www.gnu.org/licenses/>.\n" +
                " */\n";

        String output = PreprocessorLinter.removeTrailingWhitespace(input);

        assertEquals(expected, output);
    }

}
