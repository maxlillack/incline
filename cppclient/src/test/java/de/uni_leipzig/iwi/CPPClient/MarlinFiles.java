/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.CPPClient;


import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;

enum MarlinFiles implements SampleFile {
    VIM("/home/wilhelm/develop/marlin-mining/examples/vim/task/integrated-vim.c"),
    BUSY_BOX("/home/wilhelm/develop/marlin-mining/examples/busybox/task/integrated-busybox.c"),
    ZE("/home/wilhelm/develop/marlin-mining/examples/tasks/08856d9/integrated.h"),
    OZ("/home/wilhelm/develop/marlin-mining/examples/tasks/17de96a/integrated.cpp"),
    TD1("/home/wilhelm/develop/marlin-mining/examples/tasks/2daa859/integrated-Marlin-main.cpp"),
    TD2("/home/wilhelm/develop/marlin-mining/examples/tasks/2daa859/integrated-ultralcd.cpp"),
    TO("/home/wilhelm/develop/marlin-mining/examples/tasks/3116271/integrated.h"),
    TS("/home/wilhelm/develop/marlin-mining/examples/tasks/373f3ec/integrated.cpp"),
    Fs("/home/wilhelm/develop/marlin-mining/examples/tasks/46f80e8/integrated.h"),
    FS("/home/wilhelm/develop/marlin-mining/examples/tasks/47c1ea7/integrated.cpp"),
    STM1("/home/wilhelkm/develop/marlin-mining/examples/tasks/Marlin_STM32/integrated-planner.cpp"),
    STM2("/home/wilhelm/develop/marlin-mining/examples/tasks/Marlin_STM32/integrated-stepper.cpp"),
    STM3("/home/wilhelm/develop/marlin-mining/examples/tasks/Marlin_STM32/Marlin-main-integrated.cpp"),
    ESE1("/home/wilhelm/develop/marlin-mining/examples/tasks/esenapaj/stepper-integrated.cpp"),
    ESE2("/home/wilhelm/develop/marlin-mining/examples/tasks/esenapaj/ultralcd-integrated.cpp"),
    ESE3("/home/wilhelm/develop/marlin-mining/examples/tasks/esenapaj/Marlin_main_esenapaj_integrated.cpp"),
    JCROCHOLL("/home/wilhelm/develop/marlin-mining/examples/tasks/jcrocholl/Marlin-main-integrated.cpp");

    private String filename;

    MarlinFiles(String filename) {
        this.filename = filename;
    }

    public String getContents() throws IOException {
        File location = new File(filename);
        return IOUtils.toString(location.toURI());
    }

    public String getExpectedContent() throws IOException {
        return null;
    }

}
