#ifndef FORK
int actio=99;
#else
int action=100;
#endif /* !defined(FORK) */
#if (defined(FORK) || (EXECUTE)) && (!defined(FORK) || (CONFIG_EXECUTE))
#ifndef FORK
start(action);
#else
start_remotely(action);
#endif /* !defined(FORK) */
switch(action):
#ifndef FORK
case 99:
#else
case 99:
#endif /* !defined(FORK) */
log("execute locally");
break;
#ifdef FORK
case 100:
log("execute remotely");
break;
log('success');
#else
printf('success');
#endif /* defined(FORK) */
#endif /* (defined(FORK) || (EXECUTE)) && (!defined(FORK) || (CONFIG_EXECUTE)) */
