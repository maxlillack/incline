#ifdef ULTIPANEL
uint8_t lastEncoderBits;
#ifdef FORK
int8_t encoderDiff;
#endif /* defined(FORK) */
uint32_t encoderPosition;
#if (defined(FORK) || (PIN_EXISTS(SD_DETECT))) && (!defined(FORK) || (SDCARDDETECT > 0))
uint32_t blocking_enc;
#ifdef FORK
bool lcd_oldcardstatus;
#else
uint8_t lcd_sd_status;
#endif /* defined(FORK) */
#endif /* (defined(FORK) || (PIN_EXISTS(SD_DETECT))) && (!defined(FORK) || (SDCARDDETECT > 0)) */
#endif /* defined(ULTIPANEL) */

menu_t cM = lcd_status_scrn;
#ifdef FORK
bool ignore_click = false;
#else
bool ignor_click = false;
#endif /* defined(FORK) */
