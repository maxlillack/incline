#ifndef FORK
typoo();
#else
typo();
#endif /* !defined(FORK) */
#if (defined(FORK) || (defined(AA))) && (!defined(FORK) || (defined(BB)))
#ifndef FORK
impl();
#else
impl_legacy();
#endif /* !defined(FORK) */
switch 'a':
// aaa
#ifdef FORK
switch 'b':
// bbb
#endif /* defined(FORK) */
#endif /* (defined(FORK) || (defined(AA))) && (!defined(FORK) || (defined(BB))) */
