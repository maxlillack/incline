//===- PrintFunctionNames.cpp ---------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Example clang plugin which simply prints the names of all the top-level decls
// in the input file.
//
//===----------------------------------------------------------------------===//

#include <iostream>
#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Sema/Sema.h"
#include "clang/Lex/PPCallbacks.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Lex/MacroArgs.h"
#include "clang/Lex/MacroInfo.h"
#include "llvm/Support/raw_ostream.h"

#include "tinyxml2.h"


using namespace clang;

namespace {

class MyPPCallback : public PPCallbacks
{

    SourceManager & SM;
    Preprocessor & PP;
    tinyxml2::XMLDocument doc;
    tinyxml2::XMLElement* root;

public:
    MyPPCallback(SourceManager & SM, Preprocessor & PP) : SM(SM), PP(PP)
    {
        //std::cout << "MyPPCallback\n";

        root = doc.NewElement("PPTest");
        doc.InsertFirstChild(root);

        tinyxml2::XMLDeclaration* declaration = doc.NewDeclaration();
        doc.InsertFirstChild(declaration);
    }

    void MacroDefined(const Token & MacroNameTok, const MacroDirective * MD)
    {
        StringRef filename = SM.getFilename(MacroNameTok.getLocation());
        if(isInHelloCpp(MacroNameTok.getLocation())) {
            int tokenNum =  MD->getMacroInfo()->getNumTokens();
            /*std::cout << "MacroDefined callback. Name: " << MacroNameTok.getIdentifierInfo()->getName().str() << "\n"
            << " Source: " << MacroNameTok.getLocation().printToString(SM) << "\n"
            << " isFunctionLike: " << MD->getMacroInfo()->isFunctionLike() << "\n"
            << " isObjectLike: " << MD->getMacroInfo()->isObjectLike() << "\n"
            << " Tokens (" << tokenNum << ")\n";*/

            tinyxml2::XMLElement* macroDefinedElement = doc.NewElement("MacroDefined");
            tinyxml2::XMLElement* nameElement = NewXMLElement("Name",
                                                              MacroNameTok.getIdentifierInfo()->getName(),
                                                              macroDefinedElement);

            tinyxml2::XMLElement* tokensElement = doc.NewElement("Tokens");
            macroDefinedElement->InsertEndChild(tokensElement);

			SourceLocation endLocation = SourceRange(MacroNameTok.getLocation().getLocWithOffset(MacroNameTok.getLength() - 1)).getEnd();

            for(int i = 0; i < MD->getMacroInfo()->getNumArgs(); i++) {
                auto ii = MD->getMacroInfo()->args()[i];
                tinyxml2::XMLElement* tokenElement = NewXMLElement("Arg", ii->getName(), tokensElement);
            }


            for(int i = 0; i < MD->getMacroInfo()->getNumTokens(); i++)
            {
                const Token & token = MD->getMacroInfo()->getReplacementToken(i);
                const char *Start = SM.getCharacterData(token.getLocation());
                //std::cout << StringRef(Start, token.getLength()).str();
                StringRef content(Start, token.getLength());
                tinyxml2::XMLElement* tokenElement = NewXMLElement("Token", content, tokensElement);
				
				// Update end location
                endLocation = SourceRange(token.getLocation().getLocWithOffset(token.getLength() - 1)).getEnd();
            }
            //std::cout << "\n";
            NewXMLElement("Location", MD->getLocation().printToString(SM), macroDefinedElement);
            NewXMLElement("LocationLastToken", MD->getMacroInfo()->getDefinitionEndLoc().printToString(SM), macroDefinedElement);
			NewXMLElement("LocationEnd", endLocation.printToString(SM), macroDefinedElement);

            MacroDirective::DefInfo definition = MD->getDefinition();
            tinyxml2::XMLElement* macroInfoElement = doc.NewElement("MacroInfo");
            auto macroInfo = definition.getMacroInfo();
            NewXMLElement("IsFunctionLike", macroInfo->isFunctionLike() ? "1" : "0", macroInfoElement);
            NewXMLElement("IsObjectLike", macroInfo->isObjectLike() ? "1" : "0", macroInfoElement);
            NewXMLElement("IsVariadic", macroInfo->isVariadic() ? "1" : "0", macroInfoElement);
            NewXMLElement("HasCommaPasting", macroInfo->hasCommaPasting() ? "1" : "0", macroInfoElement);

            if(macroInfo->isFunctionLike())
            {
                //MacroArgs *Args = PP.ReadFunctionLikeMacroArgs
            }

            macroDefinedElement->InsertEndChild(macroInfoElement);

            root->InsertEndChild(macroDefinedElement);
        }
    }

    void Ifdef(SourceLocation Loc, const Token &MacroNameTok, const MacroDefinition &MD)
    {
        if(isInHelloCpp(MacroNameTok.getLocation()))
        {
            //std::cout << "Ifdef: " << MacroNameTok.getIdentifierInfo()->getName().str() << "\n";
            //<< Lexer::getSourceText(CharSourceRange::getCharRange(Loc), SM, LangOptions()).str() << "\n"
            //<< " SM character data " << SM.getCharacterData(Loc) << "\n"
            //<< " Source: " << Loc.printToString(SM) << "\n";
            tinyxml2::XMLElement* ifdefElement = doc.NewElement("Ifdef");
            NewXMLElement("Name", MacroNameTok.getIdentifierInfo()->getName(), ifdefElement);
            NewXMLElement("Location", Loc.printToString(SM), ifdefElement);
            NewXMLElement("Condition", MacroNameTok.getIdentifierInfo()->getName(), ifdefElement);

            NewXMLElement("ConditionRangeBegin", SourceRange(MacroNameTok.getLocation().getLocWithOffset(-1)).getBegin().printToString(SM), ifdefElement);
            NewXMLElement("ConditionRangeEnd", SourceRange(MacroNameTok.getLocation().getLocWithOffset(MacroNameTok.getLength() - 1)).getEnd().printToString(SM), ifdefElement);

	        if(MD) {
                NewXMLElement("ConditionValue", "CVK_True", ifdefElement);
            } else {
                NewXMLElement("ConditionValue", "CVK_False", ifdefElement);
            }
            root->InsertEndChild(ifdefElement);
        }
    }

    void Ifndef(SourceLocation Loc, const Token &MacroNameTok, const MacroDefinition &MD)
    {
        if(isInHelloCpp(MacroNameTok.getLocation()))
        {
            //std::cout << "Ifdef: " << MacroNameTok.getIdentifierInfo()->getName().str() << "\n";
            //<< Lexer::getSourceText(CharSourceRange::getCharRange(Loc), SM, LangOptions()).str() << "\n"
            //<< " SM character data " << SM.getCharacterData(Loc) << "\n"
            //<< " Source: " << Loc.printToString(SM) << "\n";
            tinyxml2::XMLElement* ifdefElement = doc.NewElement("Ifndef");
            NewXMLElement("Name", MacroNameTok.getIdentifierInfo()->getName(), ifdefElement);
            NewXMLElement("Location", Loc.printToString(SM), ifdefElement);
	        NewXMLElement("Condition", MacroNameTok.getIdentifierInfo()->getName(), ifdefElement);

            NewXMLElement("ConditionRangeBegin", SourceRange(MacroNameTok.getLocation().getLocWithOffset(-1)).getBegin().printToString(SM), ifdefElement);
            NewXMLElement("ConditionRangeEnd", SourceRange(MacroNameTok.getLocation().getLocWithOffset(MacroNameTok.getLength() - 1)).getEnd().printToString(SM), ifdefElement);

            if(MD) {
                NewXMLElement("ConditionValue", "CVK_True", ifdefElement);
            } else {
                NewXMLElement("ConditionValue", "CVK_False", ifdefElement);
            }

            root->InsertEndChild(ifdefElement);
        }
    }

    void Endif(SourceLocation Loc, SourceLocation IfLoc)
    {
        if(isInHelloCpp(Loc))
        {
            //std::cout << "Endif: "
            //<< Lexer::getSourceText(CharSourceRange::getCharRange(Loc), SM, LangOptions()).str() << "\n"
            //<< " SM character data " << SM.getCharacterData(Loc) << "\n"
            //<< " Source: " << Loc.printToString(SM) << "\n";
            //std::cout << "Corresponding If at " << IfLoc.printToString(SM) << "\n";
            tinyxml2::XMLElement* endifElement = doc.NewElement("Endif");
            NewXMLElement("LocationEndIf", Loc.printToString(SM), endifElement);
            NewXMLElement("LocationCorrespondingIf", IfLoc.printToString(SM), endifElement);
            root->InsertEndChild(endifElement);
        }
    }

    void Else(SourceLocation Loc, SourceLocation IfLoc)
    {
        if(isInHelloCpp(Loc))
        {
            tinyxml2::XMLElement* elseElement = doc.NewElement("Else");
            NewXMLElement("LocationElse", Loc.printToString(SM), elseElement);
            NewXMLElement("LocationCorrespondingIf", IfLoc.printToString(SM), elseElement);
            root->InsertEndChild(elseElement);
        }
    }

    void If(SourceLocation Loc, SourceRange ConditionRange, ConditionValueKind ConditionValue) {
        if(isInHelloCpp(Loc))
        {
            tinyxml2::XMLElement* ifElement = doc.NewElement("If");
            NewXMLElement("LocationIf", Loc.printToString(SM), ifElement);
            switch(ConditionValue) {
                case ConditionValueKind::CVK_True:
                    NewXMLElement("ConditionValue", "CVK_True", ifElement);
                    break;
                case ConditionValueKind::CVK_False:
                    NewXMLElement("ConditionValue", "CVK_False", ifElement);
                    break;
                case ConditionValueKind::CVK_NotEvaluated:
                    NewXMLElement("ConditionValue", "CVK_NotEvaluated", ifElement);
                    break;
            }
            NewXMLElement("ConditionRangeBegin", ConditionRange.getBegin().printToString(SM), ifElement);
            NewXMLElement("ConditionRangeEnd", ConditionRange.getEnd().printToString(SM), ifElement);
            root->InsertEndChild(ifElement);
        }
    }

    void Elif(SourceLocation Loc, SourceRange ConditionRange, ConditionValueKind ConditionValue, SourceLocation IfLoc) {
        if(isInHelloCpp(Loc))
        {
            tinyxml2::XMLElement* elifElement = doc.NewElement("ElIf");
            NewXMLElement("LocationElIf", Loc.printToString(SM), elifElement);
            switch(ConditionValue) {
                case ConditionValueKind::CVK_True:
                    NewXMLElement("ConditionValue", "CVK_True", elifElement);
                    break;
                case ConditionValueKind::CVK_False:
                    NewXMLElement("ConditionValue", "CVK_False", elifElement);
                    break;
                case ConditionValueKind::CVK_NotEvaluated:
                    NewXMLElement("ConditionValue", "CVK_NotEvaluated", elifElement);
                    break;
            }
            NewXMLElement("ConditionRangeBegin", ConditionRange.getBegin().printToString(SM), elifElement);
            NewXMLElement("ConditionRangeEnd", ConditionRange.getEnd().printToString(SM), elifElement);
            NewXMLElement("LocationCorrespondingIf", IfLoc.printToString(SM), elifElement);
            root->InsertEndChild(elifElement);
        }
    }

    void SourceRangeSkipped(SourceRange Range)
    {
        if(isInHelloCpp(Range.getBegin()))
        {
            /*std::cout << "Skipped from "
                      << Range.getBegin().printToString(SM)
                      << " to "
                      << Range.getEnd().printToString(SM)
                      << "\n";*/
            tinyxml2::XMLElement* sourceRangeSkippedElement = doc.NewElement("SourceRangeSkipped");
            NewXMLElement("RangeBegin", Range.getBegin().printToString(SM), sourceRangeSkippedElement);
            NewXMLElement("RangeEnd", Range.getEnd().printToString(SM), sourceRangeSkippedElement);
            root->InsertEndChild(sourceRangeSkippedElement);

        }
    }

    void InclusionDirective(SourceLocation HashLoc,
                            const Token & IncludeTok,
                            StringRef FileName,
                            bool IsAngled,
                            CharSourceRange FilenameRange,
                            const FileEntry * File,
                            StringRef SearchPath,
                            StringRef RelativePath,
                            const Module * Imported)
    {
        if(isInHelloCpp(HashLoc)) {
            tinyxml2::XMLElement *inclusionDirectiveElement = doc.NewElement("InclusionDirective");
            NewXMLElement("FileName", FileName, inclusionDirectiveElement);
            NewXMLElement("IsAngled", IsAngled ? "1" : "0", inclusionDirectiveElement);
            NewXMLElement("HashLoc", HashLoc.printToString(SM), inclusionDirectiveElement);
            root->InsertEndChild(inclusionDirectiveElement);
        }
    }


    void MacroExpands(const Token &MacroNameTok, const MacroDirective *MD,
                      SourceRange Range, const MacroArgs *Args)
    {
        if(isInHelloCpp(Range.getBegin())) {
            tinyxml2::XMLElement *macroExpandsElement = doc.NewElement("MacroExpands");
            NewXMLElement("MacroNameTok", MacroNameTok.getIdentifierInfo()->getName(), macroExpandsElement);
            NewXMLElement("RangeBegin", Range.getBegin().printToString(SM), macroExpandsElement);
            NewXMLElement("RangeEnd", Range.getEnd().printToString(SM), macroExpandsElement);
            NewXMLElement("MacroDirective", MD->getLocation().printToString(SM), macroExpandsElement);
            //NewXMLElement("Args", "<TODO>", macroExpandsElement);

            tinyxml2::XMLElement* argsElement = doc.NewElement("Args");

            if(Args != nullptr) {
                for (int i = 0; i < Args->getNumArguments(); i++) {
                    const Token * argToken = Args->getUnexpArgument(i);
                    // No idea why there are args from other files
                    if(isInHelloCpp(argToken->getLocation()))
                    {
                        tinyxml2::XMLElement* argElement = doc.NewElement("Arg");

                        const char *Start = SM.getCharacterData(argToken->getLocation());
                        StringRef content(Start, argToken->getLength());
                        NewXMLElement("ArgToken", content, argElement);

                        if(Args->ArgNeedsPreexpansion(argToken,PP))
                        {
                            NewXMLElement("ArgNeedsPreexpansion", "1", argElement);
                            IdentifierInfo *II = argToken->getIdentifierInfo();
                            if(II->hasMacroDefinition())
                            {
                                MacroDirective *  macroDirective = PP.getLocalMacroDirective(II);
                                NewXMLElement("MacroDirective", macroDirective->getLocation().printToString(SM), argElement);
                            }

                        } else {
                            NewXMLElement("ArgNeedsPreexpansion", "0", argElement);
                        }

                        argsElement->InsertEndChild(argElement);
                    }


                }
            }

            macroExpandsElement->InsertEndChild(argsElement);

            root->InsertEndChild(macroExpandsElement);
        }
    }

    void FileChanged(SourceLocation Loc, FileChangeReason Reason, SrcMgr::CharacteristicKind FileType, FileID PrevFID)
    {

    }

    void EndOfMainFile()
    {
        // dump xml
        doc.Print();
    }


private:

    bool isInHelloCpp(SourceLocation Location)
    {
        StringRef filename = SM.getFilename(Location);
        return filename.endswith("hello.cpp");
    }

    tinyxml2::XMLElement* NewXMLElement(std::string name, StringRef text, tinyxml2::XMLElement* parent)
    {
        tinyxml2::XMLElement* newElement = doc.NewElement(name.c_str());
        newElement->SetText(text.str().c_str());
        parent->InsertEndChild(newElement);
        return newElement;
    }

};

class MyPPAction : public PluginASTAction
{

    std::unique_ptr<FrontendAction> delegateAction = llvm::make_unique<PreprocessOnlyAction>();


    std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &, llvm::StringRef) {
        // From documentation: This will not be called if the action has indicated that it only uses the preprocessor.
        //return llvm::make_unique<ASTConsumer>();
        return nullptr;
    }


    virtual bool usesPreprocessorOnly() const {
        return true;
    }

    bool BeginSourceFileAction(CompilerInstance &ci, StringRef)
    {
        //std::cout << "BeginSourceFileAction\n";
        Preprocessor &pp = ci.getPreprocessor();
        pp.addPPCallbacks(llvm::make_unique<MyPPCallback>(ci.getSourceManager(), pp));
        return true;
    }

    bool Execute() {
        //std::cout << "MY EXECUTE\n";
        return delegateAction->Execute();
    }


    bool ParseArgs(const CompilerInstance & ci, const std::vector<std::string>& args) {
        for (unsigned i = 0, e = args.size(); i != e; ++i)
            llvm::outs() << "Received arg: " << args[i] << "\n";
        return true;
    }

protected:
    void ExecuteAction()
    {
        //std::cout << "MY ExecuteAction\n";
        Preprocessor &PP = getCompilerInstance().getPreprocessor();

        // From PreprocessOnlyAction::ExecuteAction()
        // Ignore unknown pragmas.
        PP.IgnorePragmas();

        Token Tok;
        // Start parsing the specified input file.
        PP.EnterMainSourceFile();
        do {
            PP.Lex(Tok);
        } while (Tok.isNot(tok::eof));
        //delegateAction->ExecuteAction();
    }
};

}

//static FrontendPluginRegistry::Add<PrintFunctionNamesAction> X1("print-fns", "print function names");
static FrontendPluginRegistry::Add<MyPPAction> X2("print-pp", "pp listen");
