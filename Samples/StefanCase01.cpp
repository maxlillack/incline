#ifndef CLONE
#ifdef PREVENT_DANGEROUS_EXTRUDE 
  float extrude_min_temp = EXTRUDE_MINTEMP;
#endif
#else /* CLONE */
void serial_echopair_P(const char *s_P, float v)
	{ serialprintPGM(s_P); SERIAL_ECHO(v); }
void serial_echopair_P(const char *s_P, double v)
	{ serialprintPGM(s_P); SERIAL_ECHO(v); }
void serial_echopair_P(const char *s_P, unsigned long v)
	{ serialprintPGM(s_P); SERIAL_ECHO(v); }
#endif /* CLONE */