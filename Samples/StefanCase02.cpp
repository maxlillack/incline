#ifndef CLONE
 int servo_endstops[] = SERVO_ENDSTOPS;
 int servo_endstop_angles[] = SERVO_ENDSTOP_ANGELES;
#else /* CLONE */
 int16_t servo_endstops[] = SERVO_ENDSTOPS;
 int16_t servo_endstop_angles[] = SERVO_ENDSTOP_ANGELES;
#endif /* CLONE */