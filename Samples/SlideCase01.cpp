#ifdef CLONE
#ifdef TANTILLUS
	  if (!allow_cold_extrude_once) {
#endif	
#endif /* CLONE */
      position[E_AXIS]=target[E_AXIS];
      SERIAL_ECHO_START;
      SERIAL_ECHOLNPGM(MSG_ERR_COLD_EXTRUDE_STOP);
#ifdef CLONE
#ifdef TANTILLUS
	  }
	  allow_cold_extrude_once = false;
#endif
#endif /* CLONE */
