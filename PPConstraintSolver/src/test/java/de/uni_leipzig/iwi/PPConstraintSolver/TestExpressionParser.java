/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.PPConstraintSolver;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestExpressionParser {

	@Test
	public void test01()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		ParseResult result = solver.parseToSMTLIB("defined(FORK)");
		assertEquals("defined(FORK)", solver.smtlibToPP(result.getSmtlib()));
	}
	
	@Test
	public void test02()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		ParseResult result = solver.parseToSMTLIB("!defined(FORK) || defined(SDSUPPORT)");
		assertEquals("(or (not defined_OB_FORK_CB_) defined_OB_SDSUPPORT_CB_)", result.getSmtlib());
	}
	
	@Test
	public void test03()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		ParseResult result = solver.parseToSMTLIB("BTN_ENC > 0");
		assertEquals("(> BTN_ENC 0)", result.getSmtlib());
	}
	
	@Test
	public void test04()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		ParseResult result = solver.parseToSMTLIB("(SDCARDDETECT > 0)");
		assertEquals("(> SDCARDDETECT 0)", result.getSmtlib());
	}
	
	@Test
	public void test05()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		ParseResult result = solver.parseToSMTLIB("(defined(FORK) || defined(ULTIPANEL)) && (!defined(FORK) || defined(ULTIPANEL) || defined(BASIC_ENCODER))");
		assertEquals("(and (or defined_OB_FORK_CB_ defined_OB_ULTIPANEL_CB_) (or (not defined_OB_FORK_CB_) (or defined_OB_ULTIPANEL_CB_ defined_OB_BASIC_ENCODER_CB_)))", result.getSmtlib());
	}
	
	@Test
	public void testSMT01()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.smtlibToPP("(not (<= EXTRUDERS 1))");
		assertEquals("EXTRUDERS > 1", result);
	}
	
	@Test
	public void testSMT02()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.smtlibToPP("defDELTA");
		assertEquals("defDELTA", result);
	}
	
}
