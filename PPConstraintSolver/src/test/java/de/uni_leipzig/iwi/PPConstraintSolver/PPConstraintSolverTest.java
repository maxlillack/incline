/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.PPConstraintSolver;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PPConstraintSolverTest {

	@Test
	public void test01()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("FORK && BAR", "FORK");
		assertEquals("BAR", result);
	}
	
	@Test
	public void test02()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("FORK && BAR", "!FORK");
		assertEquals("false", result);
	}

	@Test
	public void test03()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("FORK", "FORK");
		assertEquals("true", result);
	}
	
	@Test
	public void test04()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("defined(FORK) || EXTRUDERS > 1", "!defined(FORK)");
		assertEquals("EXTRUDERS > 1", result);
	}
	
	@Test
	public void test05()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("defined(DELTA)", "!defined(FORK)");
		assertEquals("defined(DELTA)", result);
	}
	
	@Test
	public void test06()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("0", "!defined(FORK)");
		assertEquals("false", result);
	}
	
	@Test
	// TODO: General function macros do not work with the new grammar for PPExpression. Figure something out.
	@Ignore
	public void test07()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("ENABLED(FOO)", "!defined(FORK)");
		assertEquals("ENABLED(FOO)", result);
	}
	
	@Test
	public void test08()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("defined(ULTIPANEL)", "!defined(FORK)");
		assertEquals("defined(ULTIPANEL)", result);
	}
	
	@Test
	public void test09()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("FOO > -1", "!defined(FORK)");
		assertEquals("FOO > -1", result);
	}
	
	@Test
	public void test10()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("(defined(FORK) || (defined(KILL_PIN) && KILL_PIN > -1)) && (!defined(FORK) || (KILL_PIN>-1 ))", "!defined(FORK)");
		assertEquals("defined(KILL_PIN) && KILL_PIN > -1", result);
	}
	

	@Test
	public void test11()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("(NUM_SERVOS >= 1) && defined(SERVO0_PIN) && (SERVO0_PIN > -1)", "FOO");
		assertEquals("NUM_SERVOS >= 1 && defined(SERVO0_PIN) && SERVO0_PIN > -1", result);
	}
	
	@Test
	public void test12()
	{
		PPConstraintSolver solver = new PPConstraintSolver();
		String result = solver.project("(defined(FORK) || (defined(AA))) && (!defined(FORK) || (defined(BB)))", "FORK");
		assertEquals("defined(B)", result);
	}
	
}
