grammar PPExpression;

@header {
    package de.uni_leipzig.iwi.PPConstraintSolver.antlr;
}
prog: expr;
expr : 
	  'defined' '(' ID ')' #define
	 | '!' expr #negation
     | expr operator=('>' | '<' | '>=' | '<=') expr #comparison
     | expr ('||' expr)+ #orexpr
     | expr ('&&' expr)+ #andexpr     
     | ID #literal
     | INT #intliteral
     | '(' expr ')' #group
     ;
     
ID : [a-zA-Z_] [a-zA-Z_0-9]+;
INT : '-'? [0-9]+;
WS : [ \n\u000D] -> skip ;
   