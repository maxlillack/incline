grammar SMTLIB;

@header {
    package de.uni_leipzig.iwi.PPConstraintSolver.antlr;
}
prog: expr;
expr : 
	  '(' 'not' expr ')' #negation
     | '(' operator=('>' | '<' | '>=' | '<=' | '=') expr expr ')' #comparison
     | '(' 'or' expr+ ')' #orexpr
     | '(' 'and' expr+ ')' #andexpr     
     | ID #literal
     | INT #intliteral
     ;
     
ID : [a-zA-Z_] [a-zA-Z_0-9]+;
INT : '-'? [0-9]+;
WS : [ \n\u000D] -> skip ;
   