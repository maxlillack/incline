/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.PPConstraintSolver;

import java.util.Objects;

/*
 * We need to know the variables used in an #ifdef expression and their types.
 * We transform any defined(X) to the Boolean variable defX.
 * We assume Bool types expect for variables used in a comparision (operators > or <), there we use Int
 */
public class SMTVariable {
	String name;
	String type;
	
	public SMTVariable(String name, String type) {
		if(name == null)
		{
			throw new IllegalArgumentException("name must not be null.");
		}
		if(type == null)
		{
			throw new IllegalArgumentException("type must not be null.");
		}
		
		this.name = name;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SMTVariable)
		{
			SMTVariable other = (SMTVariable) obj;
			return other.name.equals(name) && other.type.equals(type);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name, type);
	}
	
	
}
