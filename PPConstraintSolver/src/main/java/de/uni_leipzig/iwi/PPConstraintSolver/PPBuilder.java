/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.PPConstraintSolver;

import de.uni_leipzig.iwi.PPConstraintSolver.antlr.SMTLIBBaseListener;
import de.uni_leipzig.iwi.PPConstraintSolver.antlr.SMTLIBParser.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PPBuilder extends SMTLIBBaseListener {

	Map<ExprContext, String> map = new HashMap<>();

	
	String result;
	
	@Override
	public void exitComparison(ComparisonContext ctx) {
		map.put(ctx,  map.get(ctx.expr(0)) + " " + ctx.operator.getText() + " " + map.get(ctx.expr(1)));
	}

	@Override
	public void exitNegation(NegationContext ctx) {
		
		// We try to simplify negated comparison by using the other operator
		if(ctx.expr() instanceof ComparisonContext)
		{
			ComparisonContext comparison = (ComparisonContext) ctx.expr();
			
			String reversedOperator;
			switch (comparison.operator.getText()) {
			case "<":
				reversedOperator = ">=";
				break;
			case ">":
				reversedOperator = "<=";
				break;
			case "<=":
				reversedOperator = ">";
				break;
			case ">=":
				reversedOperator = "<";
				break;
			default:
				throw new IllegalStateException("Operator " + comparison.operator.getText() + " not handled.");
			}
			map.put(ctx, map.get(comparison.expr(0)) + " " + reversedOperator + " " + map.get(comparison.expr(1)));
			
		} else {
			map.put(ctx, "!" + map.get(ctx.expr()));
		}
		

	}

	@Override
	public void exitAndexpr(AndexprContext ctx) {
		List<String> parts = new ArrayList<>(ctx.expr().size());
		for(ExprContext expr : ctx.expr())
		{
			parts.add(map.get(expr));
		}
		
		map.put(ctx, String.join(" && ", parts));		
	}

	@Override
	public void exitOrexpr(OrexprContext ctx) {
		List<String> parts = new ArrayList<>(ctx.expr().size());
		for(ExprContext expr : ctx.expr())
		{
			parts.add(map.get(expr));
		}
		
		map.put(ctx, String.join(" || ", parts));	
	}

	@Override
	public void exitIntliteral(IntliteralContext ctx) {
		map.put(ctx, ctx.INT().getText());
	}

	@Override
	public void exitLiteral(LiteralContext ctx) {
		String text = ctx.ID().getText();
//		if(text.startsWith("def"))
//		{
//			text = "defined(" + text.substring(3) + ")";
//		}
		
		map.put(ctx, text);
	}
	
	@Override
	public void exitProg(ProgContext ctx) {
		result = map.get(ctx.expr());
	}

	public String getPP()
	{
		return result;
	}
	


}

