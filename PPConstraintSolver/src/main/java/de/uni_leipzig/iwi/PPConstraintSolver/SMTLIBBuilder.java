/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.PPConstraintSolver;

import de.uni_leipzig.iwi.PPConstraintSolver.antlr.PPExpressionBaseListener;
import de.uni_leipzig.iwi.PPConstraintSolver.antlr.PPExpressionParser.*;

import java.util.*;
import java.util.Map.Entry;

public class SMTLIBBuilder extends PPExpressionBaseListener {

	Map<ExprContext, String> map = new HashMap<>();
	Map<String, String> variables = new HashMap<>();
	
	String result;
	
	@Override
	public void exitComparison(ComparisonContext ctx) {
		
		if(ctx.expr(0) instanceof LiteralContext)
		{
			variables.put(map.get(ctx.expr(0)), "Int");
		}
		if(ctx.expr(1) instanceof LiteralContext)
		{
			variables.put(map.get(ctx.expr(1)), "Int");
		}
		
		map.put(ctx, "(" + ctx.operator.getText() + " " + map.get(ctx.expr(0)) + " " + map.get(ctx.expr(1)) + ")");
	}

	@Override
	public void exitDefine(DefineContext ctx) {
		String nodeName = "defined" + "_OB_" + ctx.ID().toString() + "_CB_";
		variables.put(nodeName, "Bool");
		map.put(ctx, nodeName);
	}

	@Override
	public void exitNegation(NegationContext ctx) {
		map.put(ctx, "(not " + map.get(ctx.expr()) + ")");
	}

	@Override
	public void exitAndexpr(AndexprContext ctx) {
		List<String> parts = new ArrayList<>(ctx.expr().size());
		for(ExprContext expr : ctx.expr())
		{
			parts.add(map.get(expr));
		}
		
		map.put(ctx, "(and " + String.join(" ", parts) + ")");		
	}

	@Override
	public void exitOrexpr(OrexprContext ctx) {
		List<String> parts = new ArrayList<>(ctx.expr().size());
		for(ExprContext expr : ctx.expr())
		{
			parts.add(map.get(expr));
		}
		
		map.put(ctx, "(or " + String.join(" ", parts) + ")");	
	}

	@Override
	public void exitIntliteral(IntliteralContext ctx) {
		String text = ctx.INT().getText(); 
		
		// We have to handle Boolean 0 explictly as "false"
		if(!(ctx.getParent() instanceof ComparisonContext))
		{
			text = "false";
		}
		
		map.put(ctx, text);
	}

	@Override
	public void exitLiteral(LiteralContext ctx) {
		variables.put(ctx.ID().getText(), "Bool");
		
		String value = ctx.ID().getText();
		value = value.replace("(", "_OB_");
		value = value.replace(")", "_CB_");

		
		map.put(ctx, value);
	}

	@Override
	public void exitGroup(GroupContext ctx) {
		map.put(ctx, map.get(ctx.expr()));
	}

	@Override
	public void exitProg(ProgContext ctx) {
		result = map.get(ctx.expr());
	}

	public String getSMTLIB()
	{
		return result;
	}
	
	public Set<SMTVariable> getVariables()
	{
		Set<SMTVariable> smtvariables = new HashSet<>();
		for(Entry<String, String> entry : variables.entrySet())
		{
			String name = entry.getKey();
			name = name.replace("(", "_OB_");
			name = name.replace(")", "_CB_");
			smtvariables.add(new SMTVariable(name, entry.getValue()));
		}
		return smtvariables;
	}

}

