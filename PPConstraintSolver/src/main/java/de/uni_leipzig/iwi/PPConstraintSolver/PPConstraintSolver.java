/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.PPConstraintSolver;

import com.microsoft.z3.*;
import de.uni_leipzig.iwi.PPConstraintSolver.antlr.PPExpressionLexer;
import de.uni_leipzig.iwi.PPConstraintSolver.antlr.PPExpressionParser;
import de.uni_leipzig.iwi.PPConstraintSolver.antlr.SMTLIBLexer;
import de.uni_leipzig.iwi.PPConstraintSolver.antlr.SMTLIBParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.HashSet;
import java.util.Set;

public class PPConstraintSolver {
	
	/*
	 * We pass the full constraint and a projection which is used to simplify the constraint
	 * 
	 * For example. Constraint = FORK && F1. Projection is FORK, the result should be F1
	 * for the projection !FORK, the result should be "false"
	 * 
	 * We use this information to construct the views
	 * 
	 */
	public String project(String constraint, String projection)
	{
//		System.out.println("Parse constraint " + constraint);
		ParseResult parsedConstraint = parseToSMTLIB(constraint);
//		System.out.println("Parse projection " + projection);
		ParseResult parsedProjection = parseToSMTLIB(projection);
		
		Context ctx = new Context();

		StringBuilder sb = new StringBuilder();
		
		Set<SMTVariable> variables = new HashSet<>();
		variables.addAll(parsedConstraint.getVariables());
		variables.addAll(parsedProjection.getVariables());
		
		FuncDecl[] decls = {};
		Symbol[] names = {};
		
		for(SMTVariable variable : variables)
		{	
			sb.append("(declare-const " + variable.name + " " + variable.type + ")\n");
		}
		
		sb.append("(declare-const result Bool)\n");
		sb.append("\n");

//		System.out.println("result smtlib");
//		System.out.print(sb.toString() + "(assert (= result " + parsedConstraint.getSmtlib() + "))\n");
//		System.out.println("parsedProjection");
//		System.out.print(sb.toString() + "(assert (= " + parsedProjection.getSmtlib() + " true))\n");
		
		BoolExpr op1;
		BoolExpr op2;
		try {
			op1 = ctx.parseSMTLIB2String(sb.toString() + "(assert (= result " + parsedConstraint.getSmtlib() + "))\n", null, null, names, decls);
			op2 = ctx.parseSMTLIB2String(sb.toString() + "(assert (= " + parsedProjection.getSmtlib() + " true))\n", null, null, names, decls);			
		} catch(Z3Exception z3Exception)
		{
			throw new IllegalStateException("Z3Exception\nresult\n" + sb.toString() + "(assert (= result " + parsedConstraint.getSmtlib() + "))\n" + sb.toString() + "(assert (= " + parsedProjection.getSmtlib() + " true))\n");
		}
		
		Solver solver = ctx.mkSolver();
		
		solver.add(op1);
		solver.add(op2);
		
		if(solver.check() == Status.UNSATISFIABLE)
		{
			return "false";
		}
		
		Tactic tactic = ctx.mkTactic("ctx-solver-simplify");
		
		/*
		 * Somehow, we need to the solver the "combine" the constraints from the original constraint and the contraint from the
		 * projection. I try to use the ctx-solver-simplify tactics and look for a subgoal for "result".
		 * This is not very robust and needs more testing.
		 */
		Goal g = ctx.mkGoal(false, false, false);
		g.add(op1);
		g.add(op2);
		
		ApplyResult applyResult = tactic.apply(g);
		
		for(Goal subGoal : applyResult.getSubgoals()) {
//			BoolExpr boolExpr = null;
//			
//			if(subGoal.getNumExprs() == 0)
//			{
//				boolExpr = ctx.mkTrue();
//			} else {
//				if(subGoal.getFormulas().length > 1) {
//					boolExpr = ctx.mkAnd(subGoal.getFormulas());
//				} else {
//					boolExpr = subGoal.getFormulas()[0];
//				}
//			}
			
			for(BoolExpr formula : subGoal.getFormulas())
			{
				if(formula.toString().equals("(not result)"))
				{
					return "false";
				}
				if(formula.toString().equals("result"))
				{
					return "true";
				}
				if(formula.isEq())
				{
					Expr arg0 = formula.getArgs()[0];
					Expr arg1 = formula.getArgs()[1];
					
					boolean is0Result = arg0.toString().equals("result");
					if(is0Result)
					{
						return smtlibToPP(arg1.toString());
					}
					
					boolean is1Result = arg1.toString().equals("result");
					if(is1Result)
					{
						return smtlibToPP(arg0.toString());
					}
					
				}
			}
		}
		
		throw new IllegalStateException();
	}
	
	public String smtlibToPP(String input)
	{
		
		input = input.replaceAll("\\(- 1\\)", "-1");
		
		ANTLRInputStream inputstream = new ANTLRInputStream(input);
		SMTLIBLexer lexer = new SMTLIBLexer(inputstream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SMTLIBParser parser = new SMTLIBParser(tokens);
        
        PPBuilder listener = new PPBuilder();
		ParseTreeWalker.DEFAULT.walk(listener, parser.prog());
		
		
		String pp = listener.getPP();
		
		pp = pp.replace("_OB_", "(");
		pp = pp.replace("_CB_", ")");
		
		return pp;
        
	}
	
	public ParseResult parseToSMTLIB(String input)
	{
		// May need to build complex result to include variables names (for declaration)
		
		ANTLRInputStream inputstream = new ANTLRInputStream(input);
		PPExpressionLexer lexer = new PPExpressionLexer(inputstream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
		PPExpressionParser parser = new PPExpressionParser(tokens);

		SMTLIBBuilder listener = new SMTLIBBuilder();
		ParseTreeWalker.DEFAULT.walk(listener, parser.prog());
		
		ParseResult result = new ParseResult(listener.getVariables(), listener.getSMTLIB());
		
		return result;
	}

}
