/* MIT License
 * Copyright (c) 2016-2019 Max Lillack and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Generated from SMTLIB.g4 by ANTLR 4.5.3

    package de.uni_leipzig.iwi.PPConstraintSolver.antlr;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SMTLIBParser}.
 */
public interface SMTLIBListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SMTLIBParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(SMTLIBParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SMTLIBParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(SMTLIBParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code negation}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNegation(SMTLIBParser.NegationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code negation}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNegation(SMTLIBParser.NegationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code comparison}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterComparison(SMTLIBParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by the {@code comparison}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitComparison(SMTLIBParser.ComparisonContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orexpr}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterOrexpr(SMTLIBParser.OrexprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orexpr}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitOrexpr(SMTLIBParser.OrexprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andexpr}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAndexpr(SMTLIBParser.AndexprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andexpr}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAndexpr(SMTLIBParser.AndexprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literal}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(SMTLIBParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literal}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(SMTLIBParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intliteral}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIntliteral(SMTLIBParser.IntliteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intliteral}
	 * labeled alternative in {@link SMTLIBParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIntliteral(SMTLIBParser.IntliteralContext ctx);
}