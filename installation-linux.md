Tested on Ubuntu 16.04 and 16.10.

### 1. Install LLVM
From [http://apt.llvm.org/](http://apt.llvm.org/):

`wget -O - http://apt.llvm.org/llvm-snapshot.gpg.key|sudo apt-key add -`

`apt-get install clang-3.9 lldb-3.9`

Verify the installation by running `clang++-3.9 -v`. The output should be something along the lines of:

```
clang version 3.9.0-1ubuntu1 (tags/RELEASE_390/final)
Target: x86_64-pc-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
```
### 2. Clang plugin

* Checkout this repository. It contains the clang plugin _CPPXMLPlugin_ **including pre-built binaries**.

* (Edit the paths in `start.sh` to match your installation)

* Test the plugin by running `start.sh`. The output should look something like this:
```
<?xml version="1.0" encoding="UTF-8"?>
<PPTest>
    <Ifdef>
        <Name>FOO</Name>
        <Location>example/hello.cpp:3:3</Location>
        <Condition>FOO</Condition>
        <ConditionRangeBegin>example/hello.cpp:3:8</ConditionRangeBegin>
        <ConditionRangeEnd>example/hello.cpp:3:11</ConditionRangeEnd>
        <ConditionValue>CVK_False</ConditionValue>
    </Ifdef>
    <Endif>
        <LocationEndIf>example/hello.cpp:5:3</LocationEndIf>
        <LocationCorrespondingIf>example/hello.cpp:3:3</LocationCorrespondingIf>
    </Endif>
    <SourceRangeSkipped>
        <RangeBegin>example/hello.cpp:3:3</RangeBegin>
        <RangeEnd>example/hello.cpp:5:3</RangeEnd>
    </SourceRangeSkipped>
</PPTest>
```

### 3. Setup CPPClient
* Copy the file `cppclient/src/main/resources/application.conf.example` to `cppclient/src/main/resources/application.conf` and adjust the file to your system. TempPath can be any location where you can store temporary files.


### 4. Install Z3
* Download the Z3 4.3.2 sources from [Github](https://github.com/Z3Prover/z3/archive/z3-4.3.2.tar.gz)

* Follow the installation instructions found in `README.md`:

```
python scripts/mk_make.py --java
cd build
make
sudo make install
```

* For some reason (just speculation, I haven't verified this) the java bindings are not installed by this, so we will have to copy them manually:
`sudo cp libz3java.so /usr/lib`


### 5. Setup MPS project

* Install [Jetbrains MPS 3.4.4](https://confluence.jetbrains.com/display/MPS/JetBrains+MPS+3.4+Download+Page)

* Start MPS and open the project `cpponmps/MPS`

* Go to File->Settings->Build, Execution, Deployment->Path Variables and create the following variables:

    * `CPPProjectFolder` and point to the repository folder (`cpponmps`)
    * `TempFolder` pointing to any folder where a temporary file can be written

* Right-click on the solution CPPLanguage and select `Make project`.

