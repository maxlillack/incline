# Warmup/tutorial
This is the running example for the warmup exercise/tutorial.
It is based on the Deltabot feature integration from Marlin.

There are three files:
* marlin-ml.cpp - mainline parent of the merge
* marlin-deltabot.cpp - fork parent of the merge
* marlin-example-integrated.cpp - integrated model of the variants from ppmerge

