get-editor "README.md" | get-text-viewer | hover-text 13 49
get-editor "Compare ('Test/Marlin_STM32/fork/stepper_cleaned.cpp' - 'Test/Marlin_STM32/mainline/stepper_cleaned.cpp')" 
    | click
get-editor "README.md" | click
with [get-editor "Compare ('Test/Marlin_STM32/fork/stepper_cleaned.cpp' - 'Test/Marlin_STM32/mainline/stepper_cleaned.cpp')"] {
    click
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 40 24
    get-button "Copy All Non-Conflicting Changes from Right to Left" | click
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        key-type "M1+z"
        set-caret-pos 46 12
        key-type "M1+z"
    }
    with [get-text-viewer] {
        set-caret-pos 42 1
        key-type "M1+z"
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 11 1
        key-type "M1+z"
    }
    get-text-viewer | set-caret-pos 11 1
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 40 24
    get-button "Copy Current Change from Right to Left" | click
    with [get-button "Next Change"] {
        click
        click
    }
    get-button "Copy Current Change from Right to Left" | click
    get-button "<" | click
    with [get-text-viewer] {
        set-caret-pos 75 38
        key-type "M1+z"
        set-caret-pos 76 28
    }
    get-button "<" | click
    get-canvas | key-type "M1+z"
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 77 7
    with [get-text-viewer] {
        set-caret-pos 76 7
        key-type "M1+z"
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 77 4
        select-range 76 3 76 10
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 76 5
        select-range 76 3 76 6
        key-type "M1+v"
        set-caret-pos 76 34
        select-range 76 33 76 34
        key-type BackSpace -times 5
        type-text N
        set-caret-pos 77 7
        select-range 78 1 77 7
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 96 2
        select-range 96 1 96 2
    }
    get-button "<" | click
    with [get-text-viewer] {
        set-caret-pos 95 33
        key-type Enter
    }
    with [get-button "Next Change"] {
        click
        click
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 169 4
        select-range 169 2 169 7
        key-type "M2+HOME"
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 171 1
        key-type "M1+v"
        key-type "M1+s"
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 179 28
        select-range 179 29 172 1
        key-type "M1+c" -times 2
    }
    with [get-text-viewer] {
        set-caret-pos 176 1
        select-range 175 66 174 1
        key-type "M1+v"
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 273 17
    get-button "<" | click
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 275 11
    get-text-viewer | set-caret-pos 280 62
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 276 6
    with [get-button "<"] {
        click
        click
    }
    get-canvas | key-type "M1+z"
    with [get-text-viewer] {
        set-caret-pos 279 38
        key-type "M1+z"
        set-caret-pos 282 19
        key-type Right
        key-type "NUMPAD_MULTIPLY" "*"
        set-caret-pos 282 52
        type-text " "
        key-type "NUMPAD_MULTIPLY" "*"
        set-caret-pos 289 50
        select-range 288 29 289 50
        type-text " "
        key-type "NUMPAD_MULTIPLY" "*"
        set-caret-pos 289 20
        select-range 289 21 289 20
        key-type Left
        key-type "NUMPAD_MULTIPLY" "*"
        set-caret-pos 293 4
        key-type "M2+CR"
        type-text "#endif //A"
        key-type "M2+BS"
        type-text " "
        key-type "M2+BS"
        type-text "!ARDUINO_ARCH_STM42"
        key-type BackSpace -times 2
        type-text 32
        set-caret-pos 293 4
        key-type "M1+s"
        set-caret-pos 316 29
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 324 13
    get-button "<" | click
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 327 5
    get-button "<" | click
    get-canvas | key-type "M1+z"
    with [get-text-viewer] {
        set-caret-pos 317 28
        key-type "M1+z"
        set-caret-pos 323 29
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 327 5
    with [get-text-viewer] {
        set-caret-pos 323 29
        key-type "M2+CR"
        type-text E
        key-type "M2+BS"
        type-text "#"
        set-caret-pos 324 4
        key-type e
        type-text ndif
        key-type "M1+s"
    }
}
get-editor "README.md" | click
with [get-editor "Compare ('Test/Marlin_STM32/fork/stepper_cleaned.cpp' - 'Test/Marlin_STM32/mainline/stepper_cleaned.cpp')"] {
    click
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 345 6
        select-range 346 6 343 1
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 340 23
        key-type Home
        type-text "\\"
        key-type BackSpace
        key-type Enter
        key-type Up
        key-type "M1+v"
        key-type Down -times 2
        key-type Left
        key-type Enter
        key-type Up
        type-text "#endif"
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 355 8
        select-range 356 8 350 1
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 346 2
        key-type Enter
        key-type "M1+v"
        key-type "M1+s"
        set-caret-pos 358 33
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 387 12
    get-button "<" | click
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 393 12
    with [get-text-viewer] {
        set-caret-pos 382 84
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 394 12
    get-button "<" | click
    with [get-text-viewer] {
        set-caret-pos 388 33
        key-type "M2+HOME"
        key-type Del -times 2
        key-type "M1+s"
        set-caret-pos 588 22
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 599 7
    get-button "<" | click
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 765 19
    get-text-viewer | set-caret-pos 753 34
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 764 1
    with [get-text-viewer] {
        set-caret-pos 753 34
        select-range 754 21 751 41
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 765 8
        select-range 765 7 772 13
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 752 20
        select-range 752 21 752 7
        key-type "M1+v"
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 803 4
    get-button "<" | click
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 808 7
        key-type "M1+s"
        set-caret-pos 808 13
    }
    get-button "<" | click
    get-canvas | key-type "M1+s"
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 821 15
    get-button "<" | click
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 828 11
        key-type "M1+s"
        set-caret-pos 828 10
    }
    get-button "<" | click
    get-canvas | key-type "M1+z"
    with [get-text-viewer] {
        set-caret-pos 806 6
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 839 3
    get-button "<" | click
    with [get-text-viewer] {
        set-caret-pos 827 27
        key-type Home
        key-type "M2+ARROW_LEFT"
        key-type Down
        key-type Home
        key-type "M2+ARROW_DOWN" -times 2
        key-type Del
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 1070 17
    get-button "<" | click
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 1086 18
    get-button "<" | click
    get-canvas | key-type "M1+z"
    with [get-text-viewer] {
        set-caret-pos 1075 42
        key-type "M1+z" -times 2
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 1087 21
        select-range 1070 3 1084 37
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 1055 1
        key-type "M1+v"
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 1086 1
        select-range 1112 8 1086 1
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 1069 37
        key-type Enter -times 2
        key-type "M1+v"
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1] {
        set-caret-pos 1132 30
        select-range 1132 31 1132 1
        key-type "M1+c"
    }
    with [get-text-viewer] {
        set-caret-pos 1116 13
        key-type Enter
        key-type "M1+v"
        key-type "M1+s"
        set-caret-pos 1117 4
        key-type Del
        key-type Left
        key-type Del
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 1130 18
    with [get-text-viewer] {
        set-caret-pos 1055 1
        type-text "  "
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 1074 30
    with [get-text-viewer] {
        set-caret-pos 1347 25
        type-text "16_t"
        key-type Right
        key-type "M1+ARROW_RIGHT" -times 2
        key-type Right -times 3
        type-text 61
        key-type BackSpace -times 2
        type-text "16+"
        key-type "M2+BS"
        type-text "_t"
        key-type "M1+s"
        set-caret-pos 1365 12
        type-text "16_t"
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 1402 16
    get-button "<" | click
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/stepper_cleaned.cpp"] -index 1 | set-caret-pos 1433 12
    with [get-text-viewer] {
        set-caret-pos 1412 10
        type-text "16_t"
        set-caret-pos 1412 61
        key-type "M1+s"
        set-caret-pos 1367 51
        key-type "M1+a"
        key-type "M1+c"
        key-type "" "\\u0000" -times 3
    }
}
get-editor "README.md" | click
get-editor "Compare ('Test/Marlin_STM32/fork/stepper_cleaned.cpp' - 'Test/Marlin_STM32/mainline/stepper_cleaned.cpp')" 
    | close
with [get-view "Project Explorer" | get-tree] {
    select "Test/Marlin_STM32/fork/planner_cleaned.cpp" "Test/Marlin_STM32/mainline/planner_cleaned.cpp"
    get-menu "Compare With/Each Other" | click
}
with [get-editor "Compare ('Test/Marlin_STM32/fork/planner_cleaned.cpp' - 'Test/Marlin_STM32/mainline/planner_cleaned.cpp')"] {
    with [get-button "<"] {
        click
        click
        click
        click
        click
    }
    with [get-text-viewer] {
        set-caret-pos 1075 2
        key-type "M1+s"
        set-caret-pos 483 42
        key-type "M1+a"
        key-type "M1+c"
        key-type "" "\\u0000" -times 2
    }
}
get-editor "README.md" | click
get-editor "Compare ('Test/Marlin_STM32/fork/planner_cleaned.cpp' - 'Test/Marlin_STM32/mainline/planner_cleaned.cpp')" 
    | close
get-editor "README.md" | get-text-viewer | hover-text 13 49
with [get-view "Project Explorer" | get-tree] {
    select "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp" "Test/Marlin_STM32/mainline/Marlin_main_cleaned.cpp"
    get-menu "Compare With/Each Other" | click
}
with [get-editor "README.md"] {
    click
    get-text-viewer | hover-text 16 50
}
with [get-editor "Compare ('Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp' - 'Test/Marlin_STM32/mainline/Marlin_main_cleaned.cpp')"] {
    click
    with [get-button "<"] {
        click
        click
        click
        click
    }
    with [get-text-viewer] {
        set-caret-pos 254 52
        key-type "M1+s"
    }
    with [get-button "<"] {
        click
        click
        click
        click
        click
    }
    get-canvas | key-type "M1+s"
    with [get-button "<"] {
        click
        click
        click
    }
    get-canvas | key-type "M1+s"
    get-button "<" | click
    get-canvas | key-type "M1+s"
    get-button "<" | click
    get-canvas | key-type "M1+s"
    get-button "<" | click
    get-canvas | key-type "M1+s"
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 558 2
    get-button "<" | click
    get-canvas | key-type "M1+s"
    get-text-viewer | set-caret-pos 582 1
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 594 4
    with [get-text-viewer] {
        set-caret-pos 582 1
        key-type Del
        key-type "M1+s"
        set-caret-pos 585 58
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1] {
        set-caret-pos 597 2
        select-range 598 1 595 1
    }
}
get-button Debug | click
get-window "Unable To Launch" | get-button OK | click
with [get-editor "README.md"] {
    click
    with [get-text-viewer] {
        set-caret-pos 16 72
        select-range 16 67 16 85
        key-type "M1+c"
    }
}
with [get-editor "Compare ('Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp' - 'Test/Marlin_STM32/mainline/Marlin_main_cleaned.cpp')"] {
    click
    get-button "<" | click
    with [get-text-viewer] {
        set-caret-pos 583 39
        key-type Enter
        type-text "#ifdef "
        key-type "M1+v"
        key-type Down
        key-type Enter
        type-text "#endif"
        key-type Up -times 2
        key-type Left -times 6
        key-type "M2+END"
        key-type "M1+c"
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 595 2
    get-button "<" | click
    with [get-text-viewer] {
        select-range 585 3 585 28
        key-type Enter
        key-type "M1+v"
        key-type Down
        key-type Left
        key-type Right
        key-type Enter
        type-text "#endif"
        key-type "M3+s" "\\u0013"
    }
}
get-editor "README.md" | click
get-editor "Compare ('Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp' - 'Test/Marlin_STM32/mainline/Marlin_main_cleaned.cpp')" 
    | click
get-editor "README.md" | click
with [get-editor "Compare ('Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp' - 'Test/Marlin_STM32/mainline/Marlin_main_cleaned.cpp')"] {
    click
    with [get-text-viewer] {
        key-type "M1+z" -times 8
        key-type "M1+s"
        set-caret-pos 589 1
        key-type Tab
        key-type "M1+v"
        key-type Enter
        type-text SET
    }
    with [get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1] {
        set-caret-pos 606 8
        select-range 606 9 606 8
    }
    get-button "<" | click
    get-canvas | key-type "M1+z"
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 606 10
    with [get-text-viewer] {
        set-caret-pos 589 30
        key-type "M1+z"
        set-caret-pos 590 8
        type-text "_OUTPUT(13);//for debug"
        key-type Enter
        type-text "#endif"
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 807 7
    with [get-button "<"] {
        click
        click
        click
        click
    }
    with [get-text-viewer] {
        set-caret-pos 874 32
        key-type "M1+s"
    }
    with [get-button "<"] {
        click
        click
    }
    with [get-text-viewer] {
        set-caret-pos 899 67
        key-type "M1+s"
    }
    with [get-button "<"] {
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
    }
    with [get-text-viewer] {
        set-caret-pos 2123 6
        key-type "M1+s"
    }
    get-button "<" | click
    with [get-text-viewer] {
        set-caret-pos 2492 48
        key-type "M1+s"
    }
    with [get-button "<"] {
        click
        click
    }
    get-button "Next Change" | click
    get-text-viewer | set-caret-pos 3036 28
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 3054 8
    get-button "<" | click
    with [get-button "Next Change"] {
        click
        click
    }
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    with [get-button "<"] {
        click
        click
    }
    with [get-button "Next Change"] {
        click
        click
    }
    with [get-button "<"] {
        click
        click
    }
    with [get-button "Next Change"] {
        click
        click
    }
    get-button "<" | click
    get-button "Next Change" | click
    with [get-button "<"] {
        click
        click
    }
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    with [get-button "<"] {
        click
        click
    }
    get-button "Next Change" | click
    get-button "<" | click
    get-button "Next Change" | click
    with [get-button "<"] {
        click
        click
    }
    get-button "Next Change" | click
    get-button "<" | click
    with [get-button "Next Change"] {
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
        click
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 4435 5
    with [get-button "<"] {
        click
        click
    }
    with [get-text-viewer] {
        set-caret-pos 4498 50
        key-type "M1+s"
    }
    get-text-viewer -after [get-label "Test/Marlin_STM32/fork/Marlin_main_cleaned.cpp"] -index 1 | set-caret-pos 4435 24
    with [get-text-viewer] {
        set-caret-pos 590 31
        key-type "M1+s"
        set-caret-pos 589 30
        key-type "M1+a"
        key-type "M1+c"
        key-type "" "\\u0000"
    }
}