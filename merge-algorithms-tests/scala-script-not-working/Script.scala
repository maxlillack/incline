import java.io.File;
import scala.sys.process.{Process, ProcessLogger}

object Script extends App {
	val cwd = System.getProperty("user.dir").replace("\\", "/")
	def run(in: String, folder: String): (List[String], List[String], Int) = {
	    var out = List[String]()
	    var err = List[String]()
	    val process = Process(in, new File(folder))
	    val exit = process ! ProcessLogger((s) => out ::= s, (s) => err ::= s)
	    (out.reverse, err.reverse, exit)
  	}

	def runVTS(file: String, file1: String, output: String) = {
		val s = run("java -cp vts.jar -Xss20m vts.jar " + file + " " + file1 + " " + "vts/" + output, cwd)
		println(s)
	}

	def runFlatteningIfdefs(file: String, file1: String, output: String) = {
		val s = run("java -Xss20m flattening-ifdefs.jar " + file + " " + file1 + " " + "flattening-ifdefs/" + output, cwd)
	}

	val files = new File(".").listFiles.toList.map(_.getName)
	val cppFiles = files.filter(x => x.endsWith("cpp"))

	
	println(cppFiles)

	cppFiles.grouped(2).foreach(x => {
		println("Running VTS on " + x.head)
		val main = x.head
		val fork = x.last
		runVTS(main, fork, "vts-output"+main.split("-")(0).substring(5) + ".cpp")

		println("Running flattening-ifdefs on " + x.head)
		runVTS(main, fork, "flattening-ifdefs-output"+main.split("-")(0).substring(5) + ".cpp")

		println("Running XML-diff on " + x.head)
	})
	
}