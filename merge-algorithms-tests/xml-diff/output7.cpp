#ifndef FORK
#ifdef EXTRUDERS > 1
        retracted_swap[active_extruder]=(code_seen('S') && code_value_long() == 1); // checks for swap retract argument
        retract(true,retracted_swap[active_extruder]);
#else
        retract(true);
#endif /* EXTRUDERS > 1 */
#else
        retract(true);
#endif /* FORK */
      break;
      case 11: // G11 retract_recover
#ifndef FORK
#ifdef EXTRUDERS > 1
        retract(false,retracted_swap[active_extruder]);
#else
        retract(false);
#endif /* EXTRUDERS > 1 */
#else
        retract(false);
#endif /* FORK */
#ifdef FORK

#endif /* FORK */
