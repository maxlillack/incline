### Description
Test files for the three algorithms: Flattening, VTS, XMLDiff. The idea is to be able to test the results of these three algorithms
### Requirements
- Python 2.7.xx
- application.conf file that points to the CPPonMPS project; copy the file in this folder

### How to use it
run python script.py in a command prompt/terminal
### Script Output
The script outputs the result in the Flattening-ifdefs, vts, xml-diff folder. Each file is called output-number.cpp; This allows us to use a diff tool like KDiff3 to see the differences of the outputs between the different algorithms and/or expected output.

Expected output is in folder "expected"