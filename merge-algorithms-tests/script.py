import glob
from itertools import izip_longest
# from subprocess import call
import subprocess

cppfiles = glob.glob('*.cpp')
# print(cppfiles)

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return izip_longest(*args, fillvalue=fillvalue)

inputs = grouper(cppfiles, 2)

def runVTS(main, fork, output):
	print("Running VTS for " + main)
	command = "java -Xss20m -jar vts.jar " + main + " " + fork + " " + "vts/output" + output + ".cpp"
	proc = subprocess.Popen(command, bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = proc.communicate()

def runFlatteningIfdefs(main, fork, output):
	print("Running FlatteningIfdefs for " + main)
	command = "java -Xss20m -jar flattening-ifdefs.jar " + main + " " + fork + " " + "flattening-ifdefs/output" + output + ".cpp"
	proc = subprocess.Popen(command, bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = proc.communicate()

def runXMLDiff(main, fork, output):
	print("Running XMLDiff for " + main)
	command = "java -Xss20m -jar xml-diff.jar " + main + " " + fork + " " + "xml-diff/output" + output + ".cpp"
	proc = subprocess.Popen(command, bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = proc.communicate()

for i in inputs:
	main = i[0]
	fork = i[1]
	output = main[5:6]
	runVTS(main, fork, output)
	runFlatteningIfdefs(main, fork, output)
	runXMLDiff(main, fork, output)