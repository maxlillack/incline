#if defined(FORK)
#if (defined(ENABLE_AUTO_BED_LEVELING) && defined(SERVO_ENDSTOPS))
    case 401:
    {
        engage_z_probe();    // Engage Z Servo endstop if available
    }
    break;

    case 402:
    {
        retract_z_probe();    // Retract Z Servo endstop if enabled
    }
    break;
#endif //(defined(ENABLE_AUTO_BED_LEVELING) && defined(SERVO_ENDSTOPS))
#else
#if (defined(ENABLE_AUTO_BED_LEVELING) && (defined(SERVO_ENDSTOPS) && !defined(Z_PROBE_SLED)))
    case 401:
    {
        engage_z_probe();    // Engage Z Servo endstop if available
    }
    break;

    case 402:
    {
        retract_z_probe();    // Retract Z Servo endstop if enabled
    }
    break;
#endif //(defined(ENABLE_AUTO_BED_LEVELING) && (defined(SERVO_ENDSTOPS) && !defined(Z_PROBE_SLED)))
#endif //defined(FORK)