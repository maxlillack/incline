#ifdef FORK
#ifdef FORK
        retract(true);
#else
#ifdef EXTRUDERS > 1
        retracted_swap[active_extruder]=(code_seen('S') && code_value_long() == 1); // checks for swap retract argument
        retract(true,retracted_swap[active_extruder]);
#else
        retract(true);
#endif
#endif
      break;
      case 11: // G11 retract_recover
        retract(false);
#else
#ifdef FORK
        retract(true);
#else
#ifdef EXTRUDERS > 1
        retracted_swap[active_extruder]=(code_seen('S') && code_value_long() == 1); // checks for swap retract argument
        retract(true,retracted_swap[active_extruder]);
#else
        retract(true);
#endif
#endif
      break;
      case 11: // G11 retract_recover
#ifdef EXTRUDERS > 1
        retract(false,retracted_swap[active_extruder]);
#else
        retract(false);
#endif
#endif
