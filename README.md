# USAGE #

To create an integrated model of two variants, you invoke PPMerge.
Invoke the PPMerge JAR located in `lib/astmerge-1.0-SNAPSHOT.jar` with the following command to create an integrated model for two variant sources (MAINLINE and VARIANT):

```
java -jar astmerge-1.0-SNAPSHOT.jar PATH_TO_MAINLINE_SOURCE PATH_TO_VARIANT_SOURCE PATH_TO_INTEGRATED_OUTPUT
```

The resulting integrated model can be imported into INCLINE in MPS.

We provide already integrated models of some Marlin cases, along with Vim and Busybox examples from the controlled experiment.

* In MPS, you mostly use the plugin actions through the Tools menu. 

* Most intentions work by selecting (with the keyboard) the nodes and then declare the intentions.

* In the integrated view, declared intentions are shown with a colored bar on the left. The positioning is related to the corresponding nodes, but is a bit off. The colors corresponds to the type of intention. You can hover over the bar to get additional information (which view was used)

* When you declare an intention, the preview (future view) should update automatically.

* The Commit action will replace the current model with result model (from applying intentions), clear the declared intentions, and reset the future view

* The Export C++ action serializes the current MPS model.

* The focus is on applying intentions. Normal edit operations will likely be problematic because I never used/tested them.

# DESIGN #

See [wiki page](https://bitbucket.org/maxlillack/incline/wiki/Tool%20Status).

# SETUP #

**Update 2016-10-28:** In the MPS installation, modify the file mps64.exe.vmoptions, change
```-Xss1024k``` to ```-Xss2m```.

This is for Windows. It will work on Linux too but would require different setup steps. (See `installation-linux.md`)

This is only for using the tool, building llvm/clang and the CPPXMLPlugin requires some more tools.

Dependencies:

    * mingw
    * llvm/clang pre-built binaries
    * MPS 3.4.4
    * z3-4.4.1

### 1. Install mingw ###

* Install to any location, use default settings
* Add mingw32/bin to your PATH variable.

### 2. Extract llvm/clang ###

* Extract the llvm/clang binaries from llvm.zip

* Verify the installation by running ```clang++ -v``` (found in bin) in a console window in the folder where you extracted the binaries

The output should look like this:
```
clang version 3.9.0 (tags/RELEASE_390/final)
Target: i686-pc-windows-gnu
Thread model: posix
InstalledDir: C:\Users\Max\Tools\llvm\bin
```

### 3. Clang plugin ###

* Checkout this repository (CPPonMPS), it contains the clang plugin *CPPXMLPlugin* **including  pre-built binaries**

* Edit the paths in ```start.bat``` to match your installation

* Test the plugin by running ```start.bat```. The output should look like this:
```
<?xml version="1.0" encoding="UTF-8"?>
<PPTest>
    <Ifdef>
        <Name>FOO</Name>
        <Location>example/hello.cpp:3:3</Location>
        <Condition>FOO</Condition>
        <ConditionRangeBegin>example/hello.cpp:3:8</ConditionRangeBegin>
        <ConditionRangeEnd>example/hello.cpp:3:11</ConditionRangeEnd>
        <ConditionValue>CVK_False</ConditionValue>
    </Ifdef>
    <Endif>
        <LocationEndIf>example/hello.cpp:5:3</LocationEndIf>
        <LocationCorrespondingIf>example/hello.cpp:3:3</LocationCorrespondingIf>
    </Endif>
    <SourceRangeSkipped>
        <RangeBegin>example/hello.cpp:3:3</RangeBegin>
        <RangeEnd>example/hello.cpp:5:3</RangeEnd>
    </SourceRangeSkipped>
</PPTest>
```

### 4. Setup CPPClient project ###

* Copy the file `cppclient/src/main/resources/application.conf.example` to `cppclient/src/main/resources/application.conf` and adjust the file to your system. TempPath can be any location where you can store temporary files.

### 5. Install z3 ###
* Extract z3-4.4.1-x64-win.zip to a folder you want

* Ensure that `z3-4.4.1-x64-win/bin/libz3java.*` gets picked up by the Java library path.

### 6. Setup MPS project ###

* Install Jetbrains MPS 3.4.4.

* Start MPS and open the project from the folder MPS in the repository folder

* Go to File -> Settings -> Build, Execution, Deployment -> Path Variables and create the following variables:

    * ```CPPProjectFolder``` and point to the repository folder (CPPonMPS)
    * ```TempFolder``` pointing to any folder where a temporary file can be written

* Right-click on the solution CPPLanguage and select ```Make Project```

# Usage #
See also the [wiki page](https://bitbucket.org/maxlillack/cpponmps/wiki/Tool%20Status).

* To import a C++ file, select Tools from the menu bar and selected the desired file. The import may take a moment. After it finished, the imported file is in the solution "Example" in Examples/Imported2/.

* To select a view, open the imported file, right-click and select "Push Editor Hints". Select one of the custom hints:

    * **BoxViewForCloneVariability** will only show the clone/fork variant
    * **SideBySideClones** use a two-column layout at variation points to display the mainline variant on the left and the clone/fork on the right side.
    * **BoxViewNonClone** will only show the mainline variant
    * **BoxViewClone** will only show the clone/fork variant (same as BoxViewForCloneVariability)
    * The action only affect the editors for **#if** whose condition contains "CLONE" -- this is the current definition for a variation point (as opposed to any other #ifdef)

* Using "Window" -> "Editor Tabs" -> "Split ..." in the menu, you can create additional windows for the same model, for each window you can specify the view with the editor hints

* Or use the Arrange views action.