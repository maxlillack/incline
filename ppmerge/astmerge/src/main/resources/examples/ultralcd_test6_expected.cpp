

static void lcd_sd_updir()
{
card.updir();
currentMenuViewOffset = 0;
}

#ifndef FORK

#endif /* !defined(FORK) */
#ifdef NEWPANEL
pinMode(BTN_EN1,INPUT);
pinMode(BTN_EN2,INPUT);
pinMode(SDCARDDETECT,INPUT);
WRITE(BTN_EN1,HIGH);
WRITE(BTN_EN2,HIGH);
#if BTN_ENC > 0
pinMode(BTN_ENC,INPUT);
WRITE(BTN_ENC,HIGH);
#endif /* BTN_ENC > 0 */
#ifdef REPRAPWORLD_KEYPAD
pinMode(SHIFT_CLK,OUTPUT);
pinMode(SHIFT_LD,OUTPUT);
pinMode(SHIFT_OUT,INPUT);
WRITE(SHIFT_OUT,HIGH);
WRITE(SHIFT_LD,HIGH);
#endif /* defined(REPRAPWORLD_KEYPAD) */
#else
#if !defined(FORK) || (!defined(BASIC_LCD))
pinMode(SHIFT_CLK,OUTPUT);
pinMode(SHIFT_LD,OUTPUT);
pinMode(SHIFT_EN,OUTPUT);
pinMode(SHIFT_OUT,INPUT);
WRITE(SHIFT_OUT,HIGH);
WRITE(SHIFT_LD,HIGH);
WRITE(SHIFT_EN,LOW);
#endif /* !defined(FORK) || (!defined(BASIC_LCD)) */
#endif /* defined(NEWPANEL) */
