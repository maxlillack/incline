#if (defined(FORK) || (ENABLE_FEATURE_IPV6)) && (!defined(FORK) || (ENABLE_FEATURE_IPV6))
#ifndef FORK
struct in6_ifreq {
struct in6_addr ifr6_addr;
uint32_t ifr6_prefixlen;
int ifr6_ifindex;
#endif /* !defined(FORK) */
#ifndef __ANDROID__
struct in6_ifreq {
struct in6_addr ifr6_addr;
uint32_t ifr6_prefixlen;
int ifr6_ifindex;
};
#endif /* !defined(__ANDROID__) */
#ifndef FORK
};
#endif /* !defined(FORK) */
#endif /* (defined(FORK) || (ENABLE_FEATURE_IPV6)) && (!defined(FORK) || (ENABLE_FEATURE_IPV6)) */
