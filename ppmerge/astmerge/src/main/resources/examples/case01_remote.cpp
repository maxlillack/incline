#if defined(PS_ON_PIN) && PS_ON_PIN > -1
  case 80: // M80 - ATX Power On
    SET_OUTPUT(PS_ON_PIN); //GND
    WRITE(PS_ON_PIN, PS_ON_AWAKE);
  break;
#endif