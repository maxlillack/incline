#ifdef ULTIPANEL
uint8_t lastEncoderBits;
#ifdef FORK
int8_t encoderDiff;
#endif /* defined(FORK) */
uint32_t encoderPosition;
#ifdef FORK
#if SDCARDDETECT > 0
bool lcd_oldcardstatus;
#endif /* SDCARDDETECT > 0 */
#else
#if PIN_EXISTS(SD_DETECT)
uint8_t lcd_sd_status;
#endif /* PIN_EXISTS(SD_DETECT) */
#endif /* defined(FORK) */
#endif /* defined(ULTIPANEL) */

menu_t cM = lcd_status_scrn;
#ifndef FORK
bool ignore_click = false;
#endif /* !defined(FORK) */
