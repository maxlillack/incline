
static void lcd_prepare_menu()
{
START_MENU();
MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
#ifdef SDSUPPORT
//MENU_ITEM(function, MSG_AUTOSTART, lcd_autostart_sd);
#endif /* defined(SDSUPPORT) */
MENU_ITEM(gcode, MSG_DISABLE_STEPPERS, PSTR("M84"));
#ifndef FORK
MENU_ITEM(gcode, MSG_AUTO_HOME, PSTR("G28"));
//MENU_ITEM(gcode, MSG_SET_ORIGIN, PSTR("G92 X0 Y0 Z0"));
#else
#ifdef LCD_PREVENT_COLD_HOME
if (degHotend(active_extruder) < LCD_MIN_HOME_TEMP) {
MENU_ITEM(function, MSG_AUTO_HOME_DISABLED, lcd_auto_home);
} else {
MENU_ITEM(function, MSG_AUTO_HOME, lcd_auto_home);
}
#else
MENU_ITEM(function, MSG_AUTO_HOME, lcd_auto_home);
#endif /* defined(LCD_PREVENT_COLD_HOME) */
#endif /* !defined(FORK) */
#if !defined(FORK) || (!defined(NO_PREHEAT_PLA_MENUITEM))
MENU_ITEM(function, MSG_PREHEAT_PLA, lcd_preheat_pla);
#endif /* !defined(FORK) || (!defined(NO_PREHEAT_PLA_MENUITEM)) */
#if !defined(FORK) || (!defined(NO_PREHEAT_ABS_MENUITEM))
MENU_ITEM(function, MSG_PREHEAT_ABS, lcd_preheat_abs);
#endif /* !defined(FORK) || (!defined(NO_PREHEAT_ABS_MENUITEM)) */
#ifdef FORK
#ifdef LCD_EASY_LOAD
MENU_ITEM(submenu, MSG_EASY_LOAD, lcd_easy_load_menu);
MENU_ITEM(submenu, MSG_EASY_UNLOAD, lcd_easy_unload_menu);
#endif /* defined(LCD_EASY_LOAD) */
#ifdef LCD_PURGE_RETRACT
MENU_ITEM(function, MSG_PURGE_XMM, lcd_purge);
MENU_ITEM(function, MSG_RETRACT_XMM, lcd_retract);
#endif /* defined(LCD_PURGE_RETRACT) */
#endif /* defined(FORK) */
MENU_ITEM(function, MSG_COOLDOWN, lcd_cooldown);
MENU_ITEM(submenu, MSG_MOVE_AXIS, lcd_move_menu);
END_MENU();
}

#ifdef FORK
a
#endif /* defined(FORK) */

#ifndef FORK

#endif /* !defined(FORK) */
#if (defined(FORK) || (SDCARDDETECT == -1)) && (!defined(FORK) || (SDCARDDETECT == -1))
static void lcd_sd_refresh()
{
#ifndef FORK
card.initsd();
currentMenuViewOffset = 0;
#endif /* !defined(FORK) */
#ifdef SDSUPPORT
card.initsd();
currentMenuViewOffset = 0;
#endif /* defined(SDSUPPORT) */
#ifdef FORK
}
#endif /* defined(FORK) */
#endif /* (defined(FORK) || (SDCARDDETECT == -1)) && (!defined(FORK) || (SDCARDDETECT == -1)) */
static void lcd_sd_updir()
{
#if !defined(FORK) || (defined(SDSUPPORT))
card.updir();
currentMenuViewOffset = 0;
#endif /* !defined(FORK) || (defined(SDSUPPORT)) */
}
