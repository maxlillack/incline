clean_up_after_endstop_move();

#ifdef NONLINEAR_BED_LEVELING
#ifndef FORK
extrapolate_unprobed_bed_level();
print_bed_level();
#endif /* !defined(FORK) */
#else
// solve lsq problem
#ifndef FORK
double *plane_equation_coefficients = qr_solve(ACCURATE_BED_LEVELING_POINTS*ACCURATE_BED_LEVELING_POINTS, 3, eqnAMatrix, eqnBVector);
#endif /* !defined(FORK) */

SERIAL_PROTOCOLPGM("Eqn coefficients: a: ");
SERIAL_PROTOCOL(plane_equation_coefficients[0]);
SERIAL_PROTOCOLPGM(" b: ");
SERIAL_PROTOCOL(plane_equation_coefficients[1]);
SERIAL_PROTOCOLPGM(" d: ");
SERIAL_PROTOCOLLN(plane_equation_coefficients[2]);


set_bed_level_equation_lsq(plane_equation_coefficients);

free(plane_equation_coefficients);
#endif /* defined(NONLINEAR_BED_LEVELING) */
#ifdef FORK
double *plane_equation_coefficients = qr_solve(AUTO_BED_LEVELING_GRID_POINTS*AUTO_BED_LEVELING_GRID_POINTS, 3, eqnAMatrix, eqnBVector);
#endif /* defined(FORK) */
