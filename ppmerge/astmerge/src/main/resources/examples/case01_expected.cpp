#if (defined(FORK) || (PS_ON_PIN > -1)) && (!defined(FORK) || (defined(PS_ON_PIN) && PS_ON_PIN > -1))
#ifndef FORK
case 80: // M80 - Turn on Power Supply
#else
case 80: // M80 - ATX Power On
#endif /* !defined(FORK) */
SET_OUTPUT(PS_ON_PIN); //GND
WRITE(PS_ON_PIN, PS_ON_AWAKE);
break;
#endif /* (defined(FORK) || (PS_ON_PIN > -1)) && (!defined(FORK) || (defined(PS_ON_PIN) && PS_ON_PIN > -1)) */
