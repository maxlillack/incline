#if defined(REPRAPWORLD_KEYPAD)
  #define NEWPANEL
  #define ULTIPANEL
#endif
#if defined(RA_CONTROL_PANEL)
 #define ULTIPANEL
 #define NEWPANEL
 #define LCD_I2C_TYPE_PCA8574
 #define LCD_I2C_ADDRESS 0x27   // I2C Address of the port expander
#endif

#if (defined(FORK) || defined(RA_CONTROL_PANEL)) && (!defined(FORK) || defined(REPRAPWORLD_KEYPAD))
#ifdef FORK
#define NEWPANEL
#endif /* defined(FORK) */
#define ULTIPANEL
#ifndef FORK
#define NEWPANEL
#define LCD_I2C_TYPE_PCA8574
#define LCD_I2C_ADDRESS 0x27
#endif /* !defined(FORK) */
#endif /* (defined(FORK) || defined(RA_CONTROL_PANEL)) && (!defined(FORK) || defined(REPRAPWORLD_KEYPAD)) */
