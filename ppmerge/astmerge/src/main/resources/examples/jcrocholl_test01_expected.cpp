#ifdef DELTA
#ifndef FORK
feedrate = homing_feedrate[axis]/4;
#else
feedrate = homing_feedrate[axis]/10;
#endif /* !defined(FORK) */
#else
feedrate = homing_feedrate[axis]/2 ;
#endif /* defined(DELTA) */
