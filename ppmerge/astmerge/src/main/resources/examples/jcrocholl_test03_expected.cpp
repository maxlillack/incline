#ifndef FORK
extern "C"{
extern unsigned int __bss_end;
extern unsigned int __heap_start;
extern void *__brkval;
#endif /* !defined(FORK) */
#if !defined(FORK) || (defined(SDSUPPORT))
#ifdef FORK
#include "SdFatUtil.h"
int freeMemory() { return SdFatUtil::FreeRam(); }
#endif /* defined(FORK) */
#else
#ifdef FORK
extern "C" {
extern unsigned int __bss_end;
extern unsigned int __heap_start;
extern void *__brkval;
#endif /* defined(FORK) */


#ifdef FORK
}
#endif /* defined(FORK) */
#endif /* !defined(FORK) || (defined(SDSUPPORT)) */
#ifndef FORK
}
#endif /* !defined(FORK) */

//adds an command to the main command buffer
#ifdef FORK
a
#endif /* defined(FORK) */
