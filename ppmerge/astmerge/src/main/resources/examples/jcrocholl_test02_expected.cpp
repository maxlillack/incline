#if (defined(FORK) || (defined (ENABLE_AUTO_BED_LEVELING) && (PROBE_SERVO_DEACTIVATION_DELAY > 0))) && (!defined(FORK) || (defined (ENABLE_AUTO_BED_LEVELING) && (PROBE_SERVO_DEACTIVATION_DELAY > 0)))
#ifndef Z_PROBE_SLED
if (axis==Z_AXIS) retract_z_probe();
#endif /* !defined(Z_PROBE_SLED) */
#ifndef FORK
if (axis==Z_AXIS) retract_z_probe();
#endif /* !defined(FORK) */
#endif /* (defined(FORK) || (defined (ENABLE_AUTO_BED_LEVELING) && (PROBE_SERVO_DEACTIVATION_DELAY > 0))) && (!defined(FORK) || (defined (ENABLE_AUTO_BED_LEVELING) && (PROBE_SERVO_DEACTIVATION_DELAY > 0))) */
