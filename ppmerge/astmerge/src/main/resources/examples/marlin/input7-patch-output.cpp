#ifndef FORK
       #if EXTRUDERS > 1
        retracted_swap[active_extruder]=(code_seen('S') && code_value_long() == 1); // checks for swap retract argument
        retract(true,retracted_swap[active_extruder]);
       #else
#endif /* ! FORK */
        retract(true);
#ifndef FORK
       #endif
#endif /* ! FORK */
      break;
      case 11: // G11 retract_recover
#ifndef FORK
       #if EXTRUDERS > 1
        retract(false,retracted_swap[active_extruder]);
       #else
#endif /* ! FORK */
        retract(false);
#ifndef FORK
       #endif
#endif /* ! FORK */
