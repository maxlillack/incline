
static void lcd_prepare_menu()
{
    START_MENU();
    MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
#ifdef SDSUPPORT
    //MENU_ITEM(function, MSG_AUTOSTART, lcd_autostart_sd);
#endif
    MENU_ITEM(gcode, MSG_DISABLE_STEPPERS, PSTR("M84"));
#if defined(LCD_PREVENT_COLD_HOME)
	if (degHotend(active_extruder) < LCD_MIN_HOME_TEMP) {
		MENU_ITEM(function, MSG_AUTO_HOME_DISABLED, lcd_auto_home);
	} else {
		MENU_ITEM(function, MSG_AUTO_HOME, lcd_auto_home);
	}
#else
    MENU_ITEM(function, MSG_AUTO_HOME, lcd_auto_home);
#endif
#if !defined(NO_PREHEAT_PLA_MENUITEM)
    MENU_ITEM(function, MSG_PREHEAT_PLA, lcd_preheat_pla);
#endif
#if !defined(NO_PREHEAT_ABS_MENUITEM)
    MENU_ITEM(function, MSG_PREHEAT_ABS, lcd_preheat_abs);
#endif
#if defined(LCD_EASY_LOAD)
	MENU_ITEM(submenu, MSG_EASY_LOAD, lcd_easy_load_menu);
	MENU_ITEM(submenu, MSG_EASY_UNLOAD, lcd_easy_unload_menu);
#endif
#if defined(LCD_PURGE_RETRACT)
	MENU_ITEM(function, MSG_PURGE_XMM, lcd_purge);
	MENU_ITEM(function, MSG_RETRACT_XMM, lcd_retract);
#endif
    MENU_ITEM(function, MSG_COOLDOWN, lcd_cooldown);
    MENU_ITEM(submenu, MSG_MOVE_AXIS, lcd_move_menu);
    END_MENU();
}

a

#if SDCARDDETECT == -1
static void lcd_sd_refresh()
{
#ifdef SDSUPPORT
    card.initsd();
    currentMenuViewOffset = 0;
#endif
}
#endif
static void lcd_sd_updir()
{
#ifdef SDSUPPORT
    card.updir();
    currentMenuViewOffset = 0;
#endif
}

