//#include "temperature.h"
//#include "ultralcd.h"
#ifdef ULTRA_LCD
//#include "Marlin.h"
//#include "language.h"
//#include "cardreader.h"
//#include "temperature.h"
//#include "stepper.h"
//#include "ConfigurationStore.h"
#ifdef ULTIPANEL
#ifndef FORK
static void lcd_prepare_menu()
{
    START_MENU();
    MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
#ifdef SDSUPPORT
    //MENU_ITEM(function, MSG_AUTOSTART, lcd_autostart_sd);
#endif /* defined(SDSUPPORT) */
    MENU_ITEM(gcode, MSG_DISABLE_STEPPERS, PSTR("M84"));
}
#endif /* !defined(FORK) */
static void lcd_control_menu()
{
    START_MENU();
    MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
    MENU_ITEM(submenu, MSG_TEMPERATURE, lcd_control_temperature_menu);
    MENU_ITEM(submenu, MSG_MOTION, lcd_control_motion_menu);
#ifdef FWRETRACT
    MENU_ITEM(submenu, MSG_RETRACT, lcd_control_retract_menu);
#endif /* defined(FWRETRACT) */
#ifdef EEPROM_SETTINGS
    MENU_ITEM(function, MSG_STORE_EPROM, Config_StoreSettings);
    MENU_ITEM(function, MSG_LOAD_EPROM, Config_RetrieveSettings);
#endif /* defined(EEPROM_SETTINGS) */
    MENU_ITEM(function, MSG_RESTORE_FAILSAFE, Config_ResetDefault);
    END_MENU();
}




#ifndef FORK


#endif /* !defined(FORK) */
#endif /* defined(ULTIPANEL) */


#endif /* defined(ULTRA_LCD) */
