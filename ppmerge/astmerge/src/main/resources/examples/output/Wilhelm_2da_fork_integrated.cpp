#ifndef FORK
    destination[axis] = 2*home_retract_mm(axis) * home_dir(axis);
#else
    destination[axis] = 2*home_retract_mm(axis) * axis_home_dir;
#endif /* !defined(FORK) */
#if !defined(FORK) && defined(DELTA)
    feedrate = homing_feedrate[axis]/10;
#else
    feedrate = homing_feedrate[axis]/2 ;
#endif /* !defined(FORK) && defined(DELTA) */
