//#include "Marlin.h"
//#include "cardreader.h"
//#include "ultralcd.h"
//#include "stepper.h"
//#include "temperature.h"
//#include "language.h"

#ifdef SDSUPPORT



CardReader::CardReader()
{
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
   sort_count = 0;
#endif /* defined(SDCARD_SORT_ALPHA) */
#endif /* defined(FORK) */
   filesize = 0;
   sdpos = 0;
   sdprinting = false;
   cardOK = false;
   saving = false;
   logging = false;
   autostart_atmillis=0;
   workDirDepth = 0;
   file_subcall_ctr=0;
   memset(workDirParents, 0, sizeof(workDirParents));

   autostart_stilltocheck=true; //the SD start is delayed, because otherwise the serial cannot answer fast enough to make contact with the host software.
   lastnr=0;
  //power to SD reader
#if SDPOWER > -1
    SET_OUTPUT(SDPOWER);
    WRITE(SDPOWER,HIGH);
#endif /* SDPOWER > -1 */

  autostart_atmillis=millis()+5000;
}

#ifndef FORK
char *createFilename(char *buffer,const dir_t &p) //buffer>12characters
#else
char *createFilename(char *buffer, const dir_t &p) //buffer>12characters
#endif /* !defined(FORK) */
{
  char *pos=buffer;
#ifndef FORK
  for (uint8_t i = 0; i < 11; i++)
  {
    if (p.name[i] == ' ')continue;
    if (i == 8)
    {
      *pos++='.';
    }
    *pos++=p.name[i];
#else
  for (uint8_t i = 0; i < 11; i++) {
    if (p.name[i] == ' ') continue;
    if (i == 8) *pos++ = '.';
    *pos++ = p.name[i];
#endif /* !defined(FORK) */
  }
#ifndef FORK
  *pos++=0;
#else
  *pos++ = 0;
#endif /* !defined(FORK) */
  return buffer;
}


#ifndef FORK
void  CardReader::lsDive(const char *prepend,SdFile parent)
#else
void CardReader::lsDive(const char *prepend, SdFile parent, const char * const match/*=NULL*/)
#endif /* !defined(FORK) */
{
  dir_t p;
#ifndef FORK
 uint8_t cnt=0;
#else
  uint8_t cnt=0;
#endif /* !defined(FORK) */

  while (parent.readDir(p, longFilename) > 0)
  {
    if( DIR_IS_SUBDIR(&p) && lsAction!=LS_Count && lsAction!=LS_GetFilename) // hence LS_SerialPrint
    {

#ifndef FORK
      char path[13*2];
      char lfilename[13];
#else
      char path[FILENAME_LENGTH*2];
      char lfilename[FILENAME_LENGTH];
#endif /* !defined(FORK) */
      createFilename(lfilename,p);

      path[0]=0;
      if(strlen(prepend)==0) //avoid leading / if already in prepend
      {
       strcat(path,"/");
      }
      strcat(path,prepend);
      strcat(path,lfilename);
      strcat(path,"/");

      //Serial.print(path);

      SdFile dir;
      if(!dir.open(parent,lfilename, O_READ))
      {
        if(lsAction==LS_SerialPrint)
        {
          SERIAL_ECHO_START;
          SERIAL_ECHOLN(MSG_SD_CANT_OPEN_SUBDIR);
          SERIAL_ECHOLN(lfilename);
        }
      }
      lsDive(path,dir);
      //close done automatically by destructor of SdFile
#ifndef FORK


#endif /* !defined(FORK) */
    }
    else
    {
#ifndef FORK
      if (p.name[0] == DIR_NAME_FREE) break;
      if (p.name[0] == DIR_NAME_DELETED || p.name[0] == '.'|| p.name[0] == '_') continue;
      if (longFilename[0] != '\0' &&
          (longFilename[0] == '.' || longFilename[0] == '_')) continue;
      if ( p.name[0] == '.')
      {
        if ( p.name[1] != '.')
        continue;
      }
#else
      char pn0 = p.name[0];
      if (pn0 == DIR_NAME_FREE) break;
      if (pn0 == DIR_NAME_DELETED || pn0 == '.' || pn0 == '_') continue;
      char lf0 = longFilename[0];
      if (lf0 == '.' || lf0 == '_') continue;
#endif /* !defined(FORK) */

      if (!DIR_IS_FILE_OR_SUBDIR(&p)) continue;
      filenameIsDir=DIR_IS_SUBDIR(&p);
#ifndef FORK

#endif /* !defined(FORK) */

      if(!filenameIsDir)
      {
        if(p.name[8]!='G') continue;
        if(p.name[9]=='~') continue;
      }
      //if(cnt++!=nr) continue;
      createFilename(filename,p);
      if(lsAction==LS_SerialPrint)
      {
        SERIAL_PROTOCOL(prepend);
        SERIAL_PROTOCOLLN(filename);
      }
      else if(lsAction==LS_Count)
      {
        nrFiles++;
      }
      else if(lsAction==LS_GetFilename)
      {
#ifndef FORK
        if(cnt==nrFiles)
          return;
#else
        if (match != NULL) {
          if (strcasecmp(match, filename) == 0) return;
        }
        else if (cnt == nrFiles) return;
#endif /* !defined(FORK) */
        cnt++;
#ifndef FORK

#endif /* !defined(FORK) */
      }
    }
  }
}

void CardReader::ls()
{
  lsAction=LS_SerialPrint;
#ifndef FORK
  if(lsAction==LS_Count)
  nrFiles=0;

#endif /* !defined(FORK) */
  root.rewind();
  lsDive("",root);
}


void CardReader::initsd()
{
  cardOK = false;
  if(root.isOpen())
    root.close();
#ifndef FORK
#ifdef SDSLOW
  if (!card.init(SPI_HALF_SPEED,SDSS))
#else
  if (!card.init(SPI_FULL_SPEED,SDSS))
#endif /* defined(SDSLOW) */
#else
  if (!card.init(SPI_FULL_SPEED,SDSS)
#ifdef SDSLOW
  if (!card.init(SPI_HALF_SPEED,SDSS)
#if defined(LCD_SDSS) && (LCD_SDSS != SDSS)
    && !card.init(SPI_HALF_SPEED,LCD_SDSS)
#endif /* defined(LCD_SDSS) && (LCD_SDSS != SDSS) */
    )
#else
#if defined(LCD_SDSS) && (LCD_SDSS != SDSS)
    && !card.init(SPI_FULL_SPEED,LCD_SDSS)
#endif /* defined(LCD_SDSS) && (LCD_SDSS != SDSS) */
    )
#endif /* defined(SDSLOW) */
#endif /* !defined(FORK) */
  {
    //if (!card.init(SPI_HALF_SPEED,SDSS))
    SERIAL_ECHO_START;
    SERIAL_ECHOLNPGM(MSG_SD_INIT_FAIL);
  }
  else if (!volume.init(&card))
  {
    SERIAL_ERROR_START;
    SERIAL_ERRORLNPGM(MSG_SD_VOL_INIT_FAIL);
  }
  else if (!root.openRoot(&volume))
  {
    SERIAL_ERROR_START;
    SERIAL_ERRORLNPGM(MSG_SD_OPENROOT_FAIL);
  }
  else
  {
    cardOK = true;
    SERIAL_ECHO_START;
    SERIAL_ECHOLNPGM(MSG_SD_CARD_OK);
  }
  workDir=root;
  curDir=&root;
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
    presort();
#endif /* defined(SDCARD_SORT_ALPHA) */
#endif /* defined(FORK) */
  /*
  if(!workDir.openRoot(&volume))
  {
    SERIAL_ECHOLNPGM(MSG_SD_WORKDIR_FAIL);
  }
  */

}

void CardReader::setroot()
{
  /*if(!workDir.openRoot(&volume))
  {
    SERIAL_ECHOLNPGM(MSG_SD_WORKDIR_FAIL);
  }*/
  workDir=root;
#ifndef FORK

#endif /* !defined(FORK) */
  curDir=&workDir;
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
    presort();
#endif /* defined(SDCARD_SORT_ALPHA) */
#endif /* defined(FORK) */
}
void CardReader::release()
{
  sdprinting = false;
  cardOK = false;
}

void CardReader::startFileprint()
{
  if(cardOK)
  {
    sdprinting = true;
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
      flush_presort();
#endif /* defined(SDCARD_SORT_ALPHA) */
#endif /* defined(FORK) */
  }
}

void CardReader::pauseSDPrint()
{
  if(sdprinting)
  {
    sdprinting = false;
  }
}


void CardReader::openLogFile(char* name)
{
  logging = true;
  openFile(name, false);
}

void CardReader::getAbsFilename(char *t)
{
  uint8_t cnt=0;
  *t='/';t++;cnt++;
  for(uint8_t i=0;i<workDirDepth;i++)
  {
    workDirParents[i].getFilename(t); //SDBaseFile.getfilename!
    while(*t!=0 && cnt< MAXPATHNAMELENGTH)
    {t++;cnt++;}  //crawl counter forward.
  }
#ifndef FORK
  if(cnt<MAXPATHNAMELENGTH-13)
#else
  if(cnt<MAXPATHNAMELENGTH-FILENAME_LENGTH)
#endif /* !defined(FORK) */
    file.getFilename(t);
  else
    t[0]=0;
}

void CardReader::openFile(char* name,bool read, bool replace_current/*=true*/)
{
  if(!cardOK)
    return;
  if(file.isOpen())  //replacing current file by new file, or subfile call
  {
    if(!replace_current)
    {
     if((int)file_subcall_ctr>(int)SD_PROCEDURE_DEPTH-1)
     {
       SERIAL_ERROR_START;
       SERIAL_ERRORPGM("trying to call sub-gcode files with too many levels. MAX level is:");
       SERIAL_ERRORLN(SD_PROCEDURE_DEPTH);
       kill();
       return;
     }

     SERIAL_ECHO_START;
     SERIAL_ECHOPGM("SUBROUTINE CALL target:\"");
     SERIAL_ECHO(name);
     SERIAL_ECHOPGM("\" parent:\"");

     //store current filename and position
     getAbsFilename(filenames[file_subcall_ctr]);

     SERIAL_ECHO(filenames[file_subcall_ctr]);
     SERIAL_ECHOPGM("\" pos");
     SERIAL_ECHOLN(sdpos);
     filespos[file_subcall_ctr]=sdpos;
     file_subcall_ctr++;
    }
    else
    {
     SERIAL_ECHO_START;
     SERIAL_ECHOPGM("Now doing file: ");
     SERIAL_ECHOLN(name);
    }
    file.close();
  }
  else //opening fresh file
  {
    file_subcall_ctr=0; //resetting procedure depth in case user cancels print while in procedure
    SERIAL_ECHO_START;
    SERIAL_ECHOPGM("Now fresh file: ");
    SERIAL_ECHOLN(name);
  }
  sdprinting = false;


  SdFile myDir;
  curDir=&root;
  char *fname=name;

  char *dirname_start,*dirname_end;
  if(name[0]=='/')
  {
    dirname_start=strchr(name,'/')+1;
    while(dirname_start>0)
    {
      dirname_end=strchr(dirname_start,'/');
      //SERIAL_ECHO("start:");SERIAL_ECHOLN((int)(dirname_start-name));
      //SERIAL_ECHO("end  :");SERIAL_ECHOLN((int)(dirname_end-name));
      if(dirname_end>0 && dirname_end>dirname_start)
      {
#ifndef FORK
        char subdirname[13];
#else
        char subdirname[FILENAME_LENGTH];
#endif /* !defined(FORK) */
        strncpy(subdirname, dirname_start, dirname_end-dirname_start);
        subdirname[dirname_end-dirname_start]=0;
        SERIAL_ECHOLN(subdirname);
        if(!myDir.open(curDir,subdirname,O_READ))
        {
          SERIAL_PROTOCOLPGM(MSG_SD_OPEN_FILE_FAIL);
          SERIAL_PROTOCOL(subdirname);
          SERIAL_PROTOCOLLNPGM(".");
          return;
        }
        else
        {
          //SERIAL_ECHOLN("dive ok");
        }

        curDir=&myDir;
        dirname_start=dirname_end+1;
      }
      else // the reminder after all /fsa/fdsa/ is the filename
      {
        fname=dirname_start;
        //SERIAL_ECHOLN("remaider");
        //SERIAL_ECHOLN(fname);
        break;
      }

    }
  }
  else //relative path
  {
    curDir=&workDir;
  }
  if(read)
  {
    if (file.open(curDir, fname, O_READ))
    {
      filesize = file.fileSize();
      SERIAL_PROTOCOLPGM(MSG_SD_FILE_OPENED);
      SERIAL_PROTOCOL(fname);
      SERIAL_PROTOCOLPGM(MSG_SD_SIZE);
      SERIAL_PROTOCOLLN(filesize);
      sdpos = 0;

      SERIAL_PROTOCOLLNPGM(MSG_SD_FILE_SELECTED);
#ifndef FORK
      lcd_setstatus(fname);
#else
      getfilename(0, fname);
      lcd_setstatus(longFilename[0] ? longFilename : fname);
#endif /* !defined(FORK) */
    }
    else
    {
      SERIAL_PROTOCOLPGM(MSG_SD_OPEN_FILE_FAIL);
      SERIAL_PROTOCOL(fname);
      SERIAL_PROTOCOLLNPGM(".");
    }
  }
  else
  { //write
    if (!file.open(curDir, fname, O_CREAT | O_APPEND | O_WRITE | O_TRUNC))
    {
      SERIAL_PROTOCOLPGM(MSG_SD_OPEN_FILE_FAIL);
      SERIAL_PROTOCOL(fname);
      SERIAL_PROTOCOLLNPGM(".");
    }
    else
    {
      saving = true;
      SERIAL_PROTOCOLPGM(MSG_SD_WRITE_TO_FILE);
      SERIAL_PROTOCOLLN(name);
      lcd_setstatus(fname);
    }
  }

}

void CardReader::removeFile(char* name)
{
  if(!cardOK)
    return;
  file.close();
  sdprinting = false;


  SdFile myDir;
  curDir=&root;
  char *fname=name;

  char *dirname_start,*dirname_end;
  if(name[0]=='/')
  {
    dirname_start=strchr(name,'/')+1;
    while(dirname_start>0)
    {
      dirname_end=strchr(dirname_start,'/');
      //SERIAL_ECHO("start:");SERIAL_ECHOLN((int)(dirname_start-name));
      //SERIAL_ECHO("end  :");SERIAL_ECHOLN((int)(dirname_end-name));
      if(dirname_end>0 && dirname_end>dirname_start)
      {
#ifndef FORK
        char subdirname[13];
#else
        char subdirname[FILENAME_LENGTH];
#endif /* !defined(FORK) */
        strncpy(subdirname, dirname_start, dirname_end-dirname_start);
        subdirname[dirname_end-dirname_start]=0;
        SERIAL_ECHOLN(subdirname);
        if(!myDir.open(curDir,subdirname,O_READ))
        {
          SERIAL_PROTOCOLPGM("open failed, File: ");
          SERIAL_PROTOCOL(subdirname);
          SERIAL_PROTOCOLLNPGM(".");
          return;
        }
        else
        {
          //SERIAL_ECHOLN("dive ok");
        }

        curDir=&myDir;
        dirname_start=dirname_end+1;
      }
      else // the reminder after all /fsa/fdsa/ is the filename
      {
        fname=dirname_start;
        //SERIAL_ECHOLN("remaider");
        //SERIAL_ECHOLN(fname);
        break;
      }

    }
  }
  else //relative path
  {
    curDir=&workDir;
  }
    if (file.remove(curDir, fname))
    {
      SERIAL_PROTOCOLPGM("File deleted:");
      SERIAL_PROTOCOLLN(fname);
      sdpos = 0;
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
        presort();
#endif /* defined(SDCARD_SORT_ALPHA) */
#endif /* defined(FORK) */
    }
    else
    {
      SERIAL_PROTOCOLPGM("Deletion failed, File: ");
      SERIAL_PROTOCOL(fname);
      SERIAL_PROTOCOLLNPGM(".");
    }

}

void CardReader::getStatus()
{
  if(cardOK){
    SERIAL_PROTOCOLPGM(MSG_SD_PRINTING_BYTE);
    SERIAL_PROTOCOL(sdpos);
    SERIAL_PROTOCOLPGM("/");
    SERIAL_PROTOCOLLN(filesize);
  }
  else{
    SERIAL_PROTOCOLLNPGM(MSG_SD_NOT_PRINTING);
  }
}
void CardReader::write_command(char *buf)
{
  char* begin = buf;
  char* npos = 0;
  char* end = buf + strlen(buf) - 1;

  file.writeError = false;
  if((npos = strchr(buf, 'N')) != NULL)
  {
    begin = strchr(npos, ' ') + 1;
    end = strchr(npos, '*') - 1;
  }
  end[1] = '\r';
  end[2] = '\n';
  end[3] = '\0';
  file.write(begin);
  if (file.writeError)
  {
    SERIAL_ERROR_START;
    SERIAL_ERRORLNPGM(MSG_SD_ERR_WRITE_TO_FILE);
  }
}


void CardReader::checkautostart(bool force)
{
  if(!force)
  {
    if(!autostart_stilltocheck)
      return;
    if(autostart_atmillis<millis())
      return;
  }
  autostart_stilltocheck=false;
  if(!cardOK)
  {
    initsd();
    if(!cardOK) //fail
      return;
  }

  char autoname[30];
  sprintf_P(autoname, PSTR("auto%i.g"), lastnr);
  for(int8_t i=0;i<(int8_t)strlen(autoname);i++)
    autoname[i]=tolower(autoname[i]);
  dir_t p;

  root.rewind();

  bool found=false;
  while (root.readDir(p, NULL) > 0)
  {
    for(int8_t i=0;i<(int8_t)strlen((char*)p.name);i++)
    p.name[i]=tolower(p.name[i]);
    //Serial.print((char*)p.name);
    //Serial.print(" ");
    //Serial.println(autoname);
    if(p.name[9]!='~') //skip safety copies
    if(strncmp((char*)p.name,autoname,5)==0)
    {
      char cmd[30];

      sprintf_P(cmd, PSTR("M23 %s"), autoname);
      enquecommand(cmd);
      enquecommand_P(PSTR("M24"));
      found=true;
    }
  }
  if(!found)
    lastnr=-1;
  else
    lastnr++;
}

void CardReader::closefile(bool store_location)
{
  file.sync();
  file.close();
  saving = false;
  logging = false;

  if(store_location)
  {
    //future: store printer state, filename and position for continuing a stopped print
    // so one can unplug the printer and continue printing the next day.

  }


}

#ifndef FORK
void CardReader::getfilename(const uint8_t nr)
#else
void CardReader::getfilename(uint16_t nr, const char * const match/*=NULL*/)
#endif /* !defined(FORK) */
{
#ifdef FORK
#if defined(SDCARD_SORT_ALPHA) && SORT_USES_RAM && SORT_USES_MORE_RAM
    if (nr < sort_count) {
      strcpy(filename, sortshort[nr]);
      strcpy(longFilename, sortnames[nr]);
      filenameIsDir = isDir[nr];
      return;
    }
#endif /* defined(SDCARD_SORT_ALPHA) && SORT_USES_RAM && SORT_USES_MORE_RAM */
#endif /* defined(FORK) */
  curDir=&workDir;
  lsAction=LS_GetFilename;
  nrFiles=nr;
  curDir->rewind();
#ifndef FORK
  lsDive("",*curDir);
#else
  lsDive("",*curDir,match);
#endif /* !defined(FORK) */

}

uint16_t CardReader::getnrfilenames()
{
  curDir=&workDir;
  lsAction=LS_Count;
  nrFiles=0;
  curDir->rewind();
  lsDive("",*curDir);
  //SERIAL_ECHOLN(nrFiles);
  return nrFiles;
}

void CardReader::chdir(const char * relpath)
{
  SdFile newfile;
  SdFile *parent=&root;

  if(workDir.isOpen())
    parent=&workDir;

  if(!newfile.open(*parent,relpath, O_READ))
  {
   SERIAL_ECHO_START;
   SERIAL_ECHOPGM(MSG_SD_CANT_ENTER_SUBDIR);
   SERIAL_ECHOLN(relpath);
  }
  else
  {
    if (workDirDepth < MAX_DIR_DEPTH) {
      for (int d = ++workDirDepth; d--;)
        workDirParents[d+1] = workDirParents[d];
      workDirParents[0]=*parent;
    }
    workDir=newfile;
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
      presort();
#endif /* defined(SDCARD_SORT_ALPHA) */
#endif /* defined(FORK) */
  }
}

void CardReader::updir()
{
#ifndef FORK
  if(workDirDepth > 0)
#else
  if (workDirDepth > 0)
#endif /* !defined(FORK) */
  {
    --workDirDepth;
    workDir = workDirParents[0];
#ifndef FORK
    int d;
#endif /* !defined(FORK) */
    for (int d = 0; d < workDirDepth; d++)
      workDirParents[d] = workDirParents[d+1];
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
      presort();
#endif /* defined(SDCARD_SORT_ALPHA) */
  }
}

#endif /* defined(FORK) */
#if !defined(FORK) || defined(SDCARD_SORT_ALPHA)
#ifdef FORK

/**
 * Get the name of a file in the current directory by sort-index
 */
void CardReader::getfilename_sorted(const uint16_t nr) {
  getfilename(nr < sort_count ? sort_order[nr] : nr);
}

/**
 * Read all the files and produce a sort key
 *
 * We can do this in 3 ways...
 *  - Minimal RAM: Read two filenames at a time sorting along...
 *  - Some RAM: Buffer the directory and return filenames from RAM
 *  - Some RAM: Buffer the directory just for this sort
 */
void CardReader::presort()
{
  flush_presort();

  uint16_t fileCnt = getnrfilenames();
  if (fileCnt > 0) {

    if (fileCnt > SORT_LIMIT) fileCnt = SORT_LIMIT;

#if SORT_USES_RAM
#if SORT_USES_MORE_RAM
        sortshort = (char**)calloc(fileCnt, sizeof(char*));
        sortnames = (char**)calloc(fileCnt, sizeof(char*));
#else
        char *sortnames[fileCnt];
#endif /* SORT_USES_MORE_RAM */
#else
      char name1[LONG_FILENAME_LENGTH+1];
#endif /* SORT_USES_RAM */

#if FOLDER_SORTING != 0
#if SORT_USES_RAM && SORT_USES_MORE_RAM
        isDir = (uint8_t*)calloc(fileCnt, sizeof(uint8_t));
#else
        uint8_t isDir[fileCnt];
#endif /* SORT_USES_RAM && SORT_USES_MORE_RAM */
#endif /* FOLDER_SORTING != 0 */

    sort_order = new uint8_t[fileCnt];

    if (fileCnt > 1) {

      // Init sort order. If using RAM then read all filenames now.
      for (uint16_t i=0; i<fileCnt; i++) {
        sort_order[i] = i;
#if SORT_USES_RAM
          getfilename(i);
          sortnames[i] = strdup(longFilename[0] ? longFilename : filename);
#if SORT_USES_MORE_RAM
            sortshort[i] = strdup(filename);
#endif /* SORT_USES_MORE_RAM */
          // char out[30];
          // sprintf_P(out, PSTR("---- %i %s %s"), i, filenameIsDir ? "D" : " ", sortnames[i]);
          // SERIAL_ECHOLN(out);
#if FOLDER_SORTING != 0
            isDir[i] = filenameIsDir;
#endif /* FOLDER_SORTING != 0 */
#endif /* SORT_USES_RAM */
      }

      // Bubble Sort
      for (uint16_t i=fileCnt; --i;) {
        bool cmp, didSwap = false;
        for (uint16_t j=0; j<i; ++j) {
          uint16_t s1 = j, s2 = j+1, o1 = sort_order[s1], o2 = sort_order[s2];
            getfilename(o1);
            strcpy(name1, longFilename[0] ? longFilename : filename);
            getfilename(o2);
            char *name2 = longFilename[0] ? longFilename : filename;
#if SORT_USES_RAM
#if FOLDER_SORTING != 0
              cmp = (isDir[o1] == isDir[o2]) ? (strcasecmp(sortnames[o1], sortnames[o2]) > 0) : isDir[FOLDER_SORTING > 0 ? o1 : o2];
#else
              cmp = strcasecmp(sortnames[o1], sortnames[o2]) > 0;
#endif /* FOLDER_SORTING != 0 */
#else
#if FOLDER_SORTING != 0
              bool dir1 = filenameIsDir;
              cmp = (dir1 == filenameIsDir) ? (strcasecmp(name1, name2) > 0) : (FOLDER_SORTING > 0 ? dir1 : !dir1);
#else
              cmp = strcasecmp(name1, name2) > 0;
#endif /* FOLDER_SORTING != 0 */
#endif /* SORT_USES_RAM */
          if (cmp) {
            sort_order[s1] = o2;
            sort_order[s2] = o1;
            didSwap = true;
          }
        }
        if (!didSwap) break;
      }

#if SORT_USES_RAM && !SORT_USES_MORE_RAM
        for (uint16_t i=0; i<fileCnt; ++i) free(sortnames[i]);
#endif /* SORT_USES_RAM && !SORT_USES_MORE_RAM */
    }
    else {
      sort_order[0] = 0;
#if SORT_USES_RAM && SORT_USES_MORE_RAM
        sortnames = (char**)malloc(sizeof(char*));
        sortshort = (char**)malloc(sizeof(char*));
        isDir = (uint8_t*)malloc(sizeof(uint8_t));
        getfilename(0);
        sortnames[0] = strdup(longFilename[0] ? longFilename : filename);
        sortshort[0] = strdup(filename);
        isDir[0] = filenameIsDir;
#endif /* SORT_USES_RAM && SORT_USES_MORE_RAM */
    }

    sort_count = fileCnt;
  }
}

void CardReader::flush_presort() {
  if (sort_count > 0) {
#if SORT_USES_RAM && SORT_USES_MORE_RAM
      for (uint8_t i=0; i<sort_count; ++i) {
        free(sortshort[i]);
        free(sortnames[i]);
      }
      free(sortshort);
      free(sortnames);
#endif /* SORT_USES_RAM && SORT_USES_MORE_RAM */
    delete sort_order;
    sort_count = 0;
#endif /* defined(FORK) */
  }
}

#endif /* !defined(FORK) || defined(SDCARD_SORT_ALPHA) */

void CardReader::printingHasFinished()
{
    st_synchronize();
    if(file_subcall_ctr>0) //heading up to a parent file that called current as a procedure.
    {
      file.close();
      file_subcall_ctr--;
      openFile(filenames[file_subcall_ctr],true,true);
      setIndex(filespos[file_subcall_ctr]);
      startFileprint();
    }
    else
    {
      quickStop();
      file.close();
      sdprinting = false;
      if(SD_FINISHED_STEPPERRELEASE)
      {
          //finishAndDisableSteppers();
          enquecommand_P(PSTR(SD_FINISHED_RELEASECOMMAND));
      }
      autotempShutdown();
#ifdef FORK
#ifdef SDCARD_SORT_ALPHA
        presort();
#endif /* defined(SDCARD_SORT_ALPHA) */
#endif /* defined(FORK) */
    }
}
#endif /* defined(SDSUPPORT) */
