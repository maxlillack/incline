#if !defined(FORK) || !defined(Z_PROBE_SLED)
    // Engage Servo endstop if enabled
#ifdef SERVO_ENDSTOPS
#if defined (ENABLE_AUTO_BED_LEVELING) && (PROBE_SERVO_DEACTIVATION_DELAY > 0)

        if (axis==Z_AXIS) {
          engage_z_probe();
        }
            else
#endif /* defined (ENABLE_AUTO_BED_LEVELING) && (PROBE_SERVO_DEACTIVATION_DELAY > 0) */
      if (servo_endstops[axis] > -1) {
        servos[servo_endstops[axis]].write(servo_endstop_angles[axis * 2]);
      }
#endif /* defined(SERVO_ENDSTOPS) */
#ifdef FORK

    current_position[axis] = 0;
    plan_set_position(current_position[X_AXIS], current_position[Y_AXIS], current_position[Z_AXIS], current_position[E_AXIS]);
#endif /* defined(FORK) */
#endif /* !defined(FORK) || !defined(Z_PROBE_SLED) */
