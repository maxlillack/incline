#if !defined(FORK) && EXTRUDERS > 1
        retracted_swap[active_extruder]=(code_seen('S') && code_value_long() == 1); // checks for swap retract argument
        retract(true,retracted_swap[active_extruder]);
#else
        retract(true);
#endif /* !defined(FORK) && EXTRUDERS > 1 */
      break;
      case 11: // G11 retract_recover
#if !defined(FORK) && EXTRUDERS > 1
        retract(false,retracted_swap[active_extruder]);
#else
        retract(false);
#endif /* !defined(FORK) && EXTRUDERS > 1 */
