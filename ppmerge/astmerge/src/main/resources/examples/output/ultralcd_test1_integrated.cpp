
/* Different menus */
static void lcd_status_screen();
#if (defined(FORK) || (defined(ULTIPANEL))) && (!defined(FORK) || (defined(ULTIPANEL) || defined(BASIC_ENCODER)))
static void lcd_main_menu();
static void lcd_tune_menu();
static void lcd_prepare_menu();
#endif /* (defined(FORK) || (defined(ULTIPANEL))) && (!defined(FORK) || (defined(ULTIPANEL) || defined(BASIC_ENCODER))) */
