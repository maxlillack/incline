/*
  temperature.c - temperature control
  Part of Marlin

 Copyright (C) 2011 Camiel Gubbels / Erik van der Zalm

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 This firmware is a mashup between Sprinter and grbl.
  (https://github.com/kliment/Sprinter)
  (https://github.com/simen/grbl/tree)

 It has preliminary support for Matthew Roberts advance algorithm
    http://reprap.org/pipermail/reprap-dev/2011-May/003323.html

 */
#include "Marlin.h"
#include "ultralcd.h"
#include "temperature.h"
#include "watchdog.h"
#ifndef FORK
#include "language.h"
#endif /* !defined(FORK) */
#include "Sd2PinMap.h"

#ifndef FORK
//===========================================================================
//================================== macros =================================
//===========================================================================

#if EXTRUDERS > 4
//#error Unsupported number of extruders
#else
#define ARRAY_BY_EXTRUDERS(v1, v2, v3, v4) { v1 }

#endif /* EXTRUDERS > 4 */

#define HAS_TEMP_0 ( defined ( TEMP_0_PIN ) && TEMP_0_PIN >= 0 )

#define HAS_TEMP_1 ( defined ( TEMP_1_PIN ) && TEMP_1_PIN >= 0 )

#define HAS_TEMP_2 ( defined ( TEMP_2_PIN ) && TEMP_2_PIN >= 0 )

#define HAS_TEMP_3 ( defined ( TEMP_3_PIN ) && TEMP_3_PIN >= 0 )

#define HAS_TEMP_BED ( defined ( TEMP_BED_PIN ) && TEMP_BED_PIN >= 0 )

#define HAS_FILAMENT_SENSOR ( defined ( FILAMENT_SENSOR ) && defined ( FILWIDTH_PIN ) && FILWIDTH_PIN >= 0 )

#define HAS_HEATER_0 ( defined ( HEATER_0_PIN ) && HEATER_0_PIN >= 0 )

#define HAS_HEATER_1 ( defined ( HEATER_1_PIN ) && HEATER_1_PIN >= 0 )

#define HAS_HEATER_2 ( defined ( HEATER_2_PIN ) && HEATER_2_PIN >= 0 )

#define HAS_HEATER_3 ( defined ( HEATER_3_PIN ) && HEATER_3_PIN >= 0 )

#define HAS_HEATER_BED ( defined ( HEATER_BED_PIN ) && HEATER_BED_PIN >= 0 )

#define HAS_AUTO_FAN_0 ( defined ( EXTRUDER_0_AUTO_FAN_PIN ) && EXTRUDER_0_AUTO_FAN_PIN >= 0 )

#define HAS_AUTO_FAN_1 ( defined ( EXTRUDER_1_AUTO_FAN_PIN ) && EXTRUDER_1_AUTO_FAN_PIN >= 0 )

#define HAS_AUTO_FAN_2 ( defined ( EXTRUDER_2_AUTO_FAN_PIN ) && EXTRUDER_2_AUTO_FAN_PIN >= 0 )

#define HAS_AUTO_FAN_3 ( defined ( EXTRUDER_3_AUTO_FAN_PIN ) && EXTRUDER_3_AUTO_FAN_PIN >= 0 )

#define HAS_AUTO_FAN HAS_AUTO_FAN_0 || HAS_AUTO_FAN_1 || HAS_AUTO_FAN_2 || HAS_AUTO_FAN_3

#define HAS_FAN ( defined ( FAN_PIN ) && FAN_PIN >= 0 )

#endif /* !defined(FORK) */

//===========================================================================
//============================= public variables ============================
//===========================================================================
#ifndef FORK

#endif /* !defined(FORK) */
#if defined(FORK) || defined(K1)
#ifndef FORK
#define K2 ( 1.0 - K1 )

#endif /* !defined(FORK) */
#endif /* defined(FORK) || defined(K1) */

// Sampling period of the temperature routine
#ifdef PID_dT
#undef PID_dT
#endif /* defined(PID_dT) */
#define PID_dT ( ( OVERSAMPLENR * 12.0 ) / ( F_CPU / 64.0 / 256.0 ) )


int target_temperature[EXTRUDERS] = { 0 };
int target_temperature_bed = 0;
int current_temperature_raw[EXTRUDERS] = { 0 };
float current_temperature[EXTRUDERS] = { 0.0 };
int current_temperature_bed_raw = 0;
float current_temperature_bed = 0.0;
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
int redundant_temperature_raw = 0;
float redundant_temperature = 0.0;
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */

#ifdef PIDTEMPBED
float bedKp=DEFAULT_bedKp;
float bedKi=(DEFAULT_bedKi*PID_dT);
float bedKd=(DEFAULT_bedKd/PID_dT);
#endif /* defined(PIDTEMPBED) */

#ifdef FAN_SOFT_PWM
unsigned char fanSpeedSoftPwm;
#endif /* defined(FAN_SOFT_PWM) */

unsigned char soft_pwm_bed;
#ifdef FORK
#ifdef BABYSTEPPING
volatile int babystepsTodo[3]={0,0,0};
#endif /* defined(BABYSTEPPING) */
#endif /* defined(FORK) */

#ifndef FORK
#ifdef BABYSTEPPING
volatile int babystepsTodo[3] = { 0 };
#endif /* defined(BABYSTEPPING) */
#endif /* !defined(FORK) */

#ifdef FILAMENT_SENSOR
int current_raw_filwidth = 0;  //Holds measured filament diameter - one extruder only
#endif /* defined(FILAMENT_SENSOR) */
//===========================================================================
//=============================private variables============================
//===========================================================================
static volatile bool temp_meas_ready = false;

#ifdef PIDTEMP
//static cannot be external:
static float temp_iState[EXTRUDERS] = { 0 };
static float temp_dState[EXTRUDERS] = { 0 };
static float pTerm[EXTRUDERS];
static float iTerm[EXTRUDERS];
static float dTerm[EXTRUDERS];
//int output;
static float pid_error[EXTRUDERS];
static float temp_iState_min[EXTRUDERS];
static float temp_iState_max[EXTRUDERS];
#ifdef FORK
// static float pid_input[EXTRUDERS];
// static float pid_output[EXTRUDERS];
#endif /* defined(FORK) */
static bool pid_reset[EXTRUDERS];
#endif /* defined(PIDTEMP) */
#ifdef PIDTEMPBED
//static cannot be external:
static float temp_iState_bed = { 0 };
static float temp_dState_bed = { 0 };
static float pTerm_bed;
static float iTerm_bed;
static float dTerm_bed;
//int output;
static float pid_error_bed;
static float temp_iState_min_bed;
static float temp_iState_max_bed;
#else
static unsigned long  previous_millis_bed_heater;
#endif /* defined(PIDTEMPBED) */
static unsigned char soft_pwm[EXTRUDERS];

#if HAS_AUTO_FAN
static unsigned long extruder_autofan_last_check;
#endif /* HAS_AUTO_FAN */

#ifndef FORK
#ifdef PIDTEMP
#ifdef PID_PARAMS_PER_EXTRUDER
float Kp[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Kp, DEFAULT_Kp, DEFAULT_Kp, DEFAULT_Kp);
float Ki[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Ki*PID_dT, DEFAULT_Ki*PID_dT, DEFAULT_Ki*PID_dT, DEFAULT_Ki*PID_dT);
float Kd[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Kd / PID_dT, DEFAULT_Kd / PID_dT, DEFAULT_Kd / PID_dT, DEFAULT_Kd / PID_dT);
#ifdef PID_ADD_EXTRUSION_RATE
float Kc[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Kc, DEFAULT_Kc, DEFAULT_Kc, DEFAULT_Kc);
#endif /* defined(PID_ADD_EXTRUSION_RATE) */
#else
float Kp = DEFAULT_Kp;
float Ki = DEFAULT_Ki * PID_dT;
float Kd = DEFAULT_Kd / PID_dT;
#ifdef PID_ADD_EXTRUSION_RATE
float Kc = DEFAULT_Kc;
#endif /* defined(PID_ADD_EXTRUSION_RATE) */
#endif /* defined(PID_PARAMS_PER_EXTRUDER) */
#endif /* defined(PIDTEMP) */
#else
#if EXTRUDERS > 4
//#error Unsupported number of extruders
#else
#define ARRAY_BY_EXTRUDERS(v1, v2, v3, v4) { v1 }

#endif /* EXTRUDERS > 4 */

#ifdef PIDTEMP
#ifdef PID_PARAMS_PER_EXTRUDER
float Kp[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Kp, DEFAULT_Kp, DEFAULT_Kp, DEFAULT_Kp);
float Ki[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Ki*PID_dT, DEFAULT_Ki*PID_dT, DEFAULT_Ki*PID_dT, DEFAULT_Ki*PID_dT);
float Kd[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Kd / PID_dT, DEFAULT_Kd / PID_dT, DEFAULT_Kd / PID_dT, DEFAULT_Kd / PID_dT);
#ifdef PID_ADD_EXTRUSION_RATE
float Kc[EXTRUDERS] = ARRAY_BY_EXTRUDERS(DEFAULT_Kc, DEFAULT_Kc, DEFAULT_Kc, DEFAULT_Kc);
#endif /* defined(PID_ADD_EXTRUSION_RATE) */
#else
float Kp = DEFAULT_Kp;
float Ki = DEFAULT_Ki * PID_dT;
float Kd = DEFAULT_Kd / PID_dT;
#ifdef PID_ADD_EXTRUSION_RATE
float Kc = DEFAULT_Kc;
#endif /* defined(PID_ADD_EXTRUSION_RATE) */
#endif /* defined(PID_PARAMS_PER_EXTRUDER) */
#endif /* defined(PIDTEMP) */
#endif /* !defined(FORK) */

// Init min and max temp with extreme values to prevent false errors during startup
static int minttemp_raw[EXTRUDERS] = ARRAY_BY_EXTRUDERS( HEATER_0_RAW_LO_TEMP , HEATER_1_RAW_LO_TEMP , HEATER_2_RAW_LO_TEMP, HEATER_3_RAW_LO_TEMP);
static int maxttemp_raw[EXTRUDERS] = ARRAY_BY_EXTRUDERS( HEATER_0_RAW_HI_TEMP , HEATER_1_RAW_HI_TEMP , HEATER_2_RAW_HI_TEMP, HEATER_3_RAW_HI_TEMP);
static int minttemp[EXTRUDERS] = ARRAY_BY_EXTRUDERS( 0, 0, 0, 0 );
static int maxttemp[EXTRUDERS] = ARRAY_BY_EXTRUDERS( 16383, 16383, 16383, 16383 );
//static int bed_minttemp_raw = HEATER_BED_RAW_LO_TEMP; /* No bed mintemp error implemented?!? */
#ifndef FORK
#ifdef BED_MAXTEMP
static int bed_maxttemp_raw = HEATER_BED_RAW_HI_TEMP;
#endif /* defined(BED_MAXTEMP) */
#else
#ifdef BED_MAXTEMP
static int bed_maxttemp_raw = HEATER_BED_RAW_HI_TEMP;
#endif /* defined(BED_MAXTEMP) */
#endif /* !defined(FORK) */

#ifdef TEMP_SENSOR_1_AS_REDUNDANT
static void *heater_ttbl_map[2] = {(void *)HEATER_0_TEMPTABLE, (void *)HEATER_1_TEMPTABLE };
static uint8_t heater_ttbllen_map[2] = { HEATER_0_TEMPTABLE_LEN, HEATER_1_TEMPTABLE_LEN };
#else
static void *heater_ttbl_map[EXTRUDERS] = ARRAY_BY_EXTRUDERS( (void *)HEATER_0_TEMPTABLE, (void *)HEATER_1_TEMPTABLE, (void *)HEATER_2_TEMPTABLE, (void *)HEATER_3_TEMPTABLE );
static uint8_t heater_ttbllen_map[EXTRUDERS] = ARRAY_BY_EXTRUDERS( HEATER_0_TEMPTABLE_LEN, HEATER_1_TEMPTABLE_LEN, HEATER_2_TEMPTABLE_LEN, HEATER_3_TEMPTABLE_LEN );
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */

static float analog2temp(int raw, uint8_t e);
static float analog2tempBed(int raw);
static void updateTemperaturesFromRawValues();

#ifndef FORK
#ifdef WATCH_TEMP_PERIOD
int watch_start_temp[EXTRUDERS] = ARRAY_BY_EXTRUDERS(0,0,0,0);
unsigned long watchmillis[EXTRUDERS] = ARRAY_BY_EXTRUDERS(0,0,0,0);
#endif /* defined(WATCH_TEMP_PERIOD) */
#else
#ifdef WATCH_TEMP_PERIOD
int watch_start_temp[EXTRUDERS] = ARRAY_BY_EXTRUDERS(0,0,0,0);
unsigned long watchmillis[EXTRUDERS] = ARRAY_BY_EXTRUDERS(0,0,0,0);
#endif /* defined(WATCH_TEMP_PERIOD) */
#endif /* !defined(FORK) */

#ifndef SOFT_PWM_SCALE
#define SOFT_PWM_SCALE 0

#endif /* !defined(SOFT_PWM_SCALE) */

#ifdef FILAMENT_SENSOR
static int meas_shift_index;  //used to point to a delayed sample in buffer for filament width sensor
#endif /* defined(FILAMENT_SENSOR) */

#ifdef HEATER_0_USES_MAX6675
static int read_max6675();
#endif /* defined(HEATER_0_USES_MAX6675) */

//===========================================================================
//=============================   functions      ============================
//===========================================================================

void PID_autotune(float temp, int extruder, int ncycles)
{
float input = 0.0;
#ifndef FORK
int cycles = 0;
#else
int cycles=0;
#endif /* !defined(FORK) */
bool heating = true;

#ifndef FORK
unsigned long temp_millis = millis(), t1 = temp_millis, t2 = temp_millis;
long t_high = 0, t_low = 0;
#else
unsigned long temp_millis = millis();
unsigned long t1=temp_millis;
unsigned long t2=temp_millis;
long t_high = 0;
long t_low = 0;
#endif /* !defined(FORK) */

long bias, d;
float Ku, Tu;
float Kp, Ki, Kd;
float max = 0, min = 10000;

#ifndef FORK
#if HAS_AUTO_FAN
unsigned long extruder_autofan_last_check = temp_millis;
#endif /* HAS_AUTO_FAN */
#else
#if (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1) ||    (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1) ||    (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1) ||    (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1)
unsigned long extruder_autofan_last_check = millis();
#endif /* (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1) ||    (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1) ||    (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1) ||    (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1) */
#endif /* !defined(FORK) */

#ifndef FORK
if (extruder >= EXTRUDERS
#if !HAS_TEMP_BED
|| extruder < 0
#endif /* !HAS_TEMP_BED */
) {
SERIAL_ECHOLN(MSG_PID_BAD_EXTRUDER_NUM);
return;
}
#else
if ((extruder >= EXTRUDERS)
#if TEMP_BED_PIN <= -1
||(extruder < 0)
#endif /* TEMP_BED_PIN <= -1 */
){
SERIAL_ECHOLN("PID Autotune failed. Bad extruder number.");
return;
}
#endif /* !defined(FORK) */

#ifndef FORK
SERIAL_ECHOLN(MSG_PID_AUTOTUNE_START);
#else
SERIAL_ECHOLN("PID Autotune start");
#endif /* !defined(FORK) */

disable_heater(); // switch off all heaters.

#ifndef FORK
if (extruder < 0)
soft_pwm_bed = bias = d = MAX_BED_POWER / 2;
else
soft_pwm[extruder] = bias = d = PID_MAX / 2;
#else
if (extruder<0)
{
soft_pwm_bed = (MAX_BED_POWER)/2;
bias = d = (MAX_BED_POWER)/2;
}
else
{
soft_pwm[extruder] = (PID_MAX)/2;
bias = d = (PID_MAX)/2;
}
#endif /* !defined(FORK) */

#ifndef FORK
// PID Tuning loop
for(;;) {
#endif /* !defined(FORK) */

#ifndef FORK
unsigned long ms = millis();
#endif /* !defined(FORK) */

#ifndef FORK
if (temp_meas_ready == true) { // temp sample ready
#else

for(;;) {

if(temp_meas_ready == true) { // temp sample ready
#endif /* !defined(FORK) */
updateTemperaturesFromRawValues();

input = (extruder<0)?current_temperature_bed:current_temperature[extruder];

#ifndef FORK
max = max(max, input);
min = min(min, input);
#else
max=max(max,input);
min=min(min,input);
#endif /* !defined(FORK) */

#ifdef FORK
#if (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1) ||          (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1) ||          (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1) ||          (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1)
if(millis() - extruder_autofan_last_check > 2500) {
checkExtruderAutoFans();
extruder_autofan_last_check = millis();
}
#endif /* (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1) ||          (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1) ||          (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1) ||          (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1) */
#endif /* defined(FORK) */
#if defined(FORK) || HAS_AUTO_FAN
#ifndef FORK
if (ms > extruder_autofan_last_check + 2500) {
checkExtruderAutoFans();
extruder_autofan_last_check = ms;
}
#endif /* !defined(FORK) */
#endif /* defined(FORK) || HAS_AUTO_FAN */
#ifdef FORK
if(heating == true && input > temp) {
if(millis() - t2 > 5000) {
heating=false;
if (extruder<0)
#endif /* defined(FORK) */

#ifndef FORK
if (heating == true && input > temp) {
if (ms - t2 > 5000) {
heating = false;
if (extruder < 0)
#endif /* !defined(FORK) */
soft_pwm_bed = (bias - d) >> 1;
else
#ifdef FORK
t1=millis();
t_high=t1 - t2;
max=temp;
#endif /* defined(FORK) */
soft_pwm[extruder] = (bias - d) >> 1;
#ifndef FORK
t1 = ms;
t_high = t1 - t2;
max = temp;
#endif /* !defined(FORK) */
}
#ifdef FORK
if(heating == false && input < temp) {
if(millis() - t1 > 5000) {
heating=true;
t2=millis();
t_low=t2 - t1;
if(cycles > 0) {
#endif /* defined(FORK) */
}
#ifndef FORK
if (heating == false && input < temp) {
if (ms - t1 > 5000) {
heating = true;
t2 = ms;
t_low = t2 - t1;
if (cycles > 0) {
long max_pow = extruder < 0 ? MAX_BED_POWER : PID_MAX;
#else
bias = constrain(bias, 20 ,(extruder<0?(MAX_BED_POWER):(PID_MAX))-20);
if(bias > (extruder<0?(MAX_BED_POWER):(PID_MAX))/2) d = (extruder<0?(MAX_BED_POWER):(PID_MAX)) - 1 - bias;
else d = bias;
#endif /* !defined(FORK) */
bias += (d*(t_high - t_low))/(t_low + t_high);
#ifndef FORK
bias = constrain(bias, 20, max_pow - 20);
d = (bias > max_pow / 2) ? max_pow - 1 - bias : bias;
#else
SERIAL_PROTOCOLPGM(" bias: "); SERIAL_PROTOCOL(bias);
SERIAL_PROTOCOLPGM(" d: "); SERIAL_PROTOCOL(d);
SERIAL_PROTOCOLPGM(" min: "); SERIAL_PROTOCOL(min);
SERIAL_PROTOCOLPGM(" max: "); SERIAL_PROTOCOLLN(max);
if(cycles > 2) {
Ku = (4.0*d)/(3.14159*(max-min)/2.0);
Tu = ((float)(t_low + t_high)/1000.0);
SERIAL_PROTOCOLPGM(" Ku: "); SERIAL_PROTOCOL(Ku);
SERIAL_PROTOCOLPGM(" Tu: "); SERIAL_PROTOCOLLN(Tu);
Kp = 0.6*Ku;
Ki = 2*Kp/Tu;
Kd = Kp*Tu/8;
SERIAL_PROTOCOLLNPGM(" Classic PID ");
SERIAL_PROTOCOLPGM(" Kp: "); SERIAL_PROTOCOLLN(Kp);
SERIAL_PROTOCOLPGM(" Ki: "); SERIAL_PROTOCOLLN(Ki);
SERIAL_PROTOCOLPGM(" Kd: "); SERIAL_PROTOCOLLN(Kd);
#endif /* !defined(FORK) */

#ifndef FORK
SERIAL_PROTOCOLPGM(MSG_BIAS); SERIAL_PROTOCOL(bias);
SERIAL_PROTOCOLPGM(MSG_D);    SERIAL_PROTOCOL(d);
SERIAL_PROTOCOLPGM(MSG_T_MIN);  SERIAL_PROTOCOL(min);
SERIAL_PROTOCOLPGM(MSG_T_MAX);  SERIAL_PROTOCOLLN(max);
if (cycles > 2) {
Ku = (4.0 * d) / (3.14159265 * (max - min) / 2.0);
Tu = ((float)(t_low + t_high) / 1000.0);
SERIAL_PROTOCOLPGM(MSG_KU); SERIAL_PROTOCOL(Ku);
SERIAL_PROTOCOLPGM(MSG_TU); SERIAL_PROTOCOLLN(Tu);
Kp = 0.6 * Ku;
Ki = 2 * Kp / Tu;
Kd = Kp * Tu / 8;
SERIAL_PROTOCOLLNPGM(MSG_CLASSIC_PID);
SERIAL_PROTOCOLPGM(MSG_KP); SERIAL_PROTOCOLLN(Kp);
SERIAL_PROTOCOLPGM(MSG_KI); SERIAL_PROTOCOLLN(Ki);
SERIAL_PROTOCOLPGM(MSG_KD); SERIAL_PROTOCOLLN(Kd);
#endif /* !defined(FORK) */
/*
              Kp = 0.33*Ku;
              Ki = Kp/Tu;
              Kd = Kp*Tu/3;
              SERIAL_PROTOCOLLNPGM(" Some overshoot ");
              SERIAL_PROTOCOLPGM(" Kp: "); SERIAL_PROTOCOLLN(Kp);
              SERIAL_PROTOCOLPGM(" Ki: "); SERIAL_PROTOCOLLN(Ki);
              SERIAL_PROTOCOLPGM(" Kd: "); SERIAL_PROTOCOLLN(Kd);
              Kp = 0.2*Ku;
              Ki = 2*Kp/Tu;
              Kd = Kp*Tu/3;
              SERIAL_PROTOCOLLNPGM(" No overshoot ");
              SERIAL_PROTOCOLPGM(" Kp: "); SERIAL_PROTOCOLLN(Kp);
              SERIAL_PROTOCOLPGM(" Ki: "); SERIAL_PROTOCOLLN(Ki);
              SERIAL_PROTOCOLPGM(" Kd: "); SERIAL_PROTOCOLLN(Kd);
              */
}
#ifdef FORK
if (extruder<0)
#endif /* defined(FORK) */
}
#ifndef FORK
if (extruder < 0)
#endif /* !defined(FORK) */
soft_pwm_bed = (bias + d) >> 1;
else
soft_pwm[extruder] = (bias + d) >> 1;
#ifdef FORK
min=temp;
#endif /* defined(FORK) */
cycles++;
#ifndef FORK
min = temp;
#endif /* !defined(FORK) */
}
}
}
#ifndef FORK
if (input > temp + 20) {
SERIAL_PROTOCOLLNPGM(MSG_PID_TEMP_TOO_HIGH);
#else
if(input > (temp + 20)) {
SERIAL_PROTOCOLLNPGM("PID Autotune failed! Temperature too high");
#endif /* !defined(FORK) */
return;
}
#ifndef FORK
// Every 2 seconds...
if (ms > temp_millis + 2000) {
#else
if(millis() - temp_millis > 2000) {
#endif /* !defined(FORK) */
int p;
#ifndef FORK
if (extruder < 0) {
p = soft_pwm_bed;
SERIAL_PROTOCOLPGM(MSG_OK_B);
}
else {
p = soft_pwm[extruder];
SERIAL_PROTOCOLPGM(MSG_OK_T);
#else
if (extruder<0){
p=soft_pwm_bed;
SERIAL_PROTOCOLPGM("ok B:");
}else{
p=soft_pwm[extruder];
SERIAL_PROTOCOLPGM("ok T:");
#endif /* !defined(FORK) */
}

#ifndef FORK
SERIAL_PROTOCOL(input);
SERIAL_PROTOCOLPGM(MSG_AT);
SERIAL_PROTOCOLLN(p);
#else
SERIAL_PROTOCOL(input);
SERIAL_PROTOCOLPGM(" @:");
SERIAL_PROTOCOLLN(p);
#endif /* !defined(FORK) */

#ifndef FORK
temp_millis = ms;
} // every 2 seconds
// Over 2 minutes?
if (((ms - t1) + (ms - t2)) > (10L*60L*1000L*2L)) {
SERIAL_PROTOCOLLNPGM(MSG_PID_TIMEOUT);
#else
temp_millis = millis();
}
if(((millis() - t1) + (millis() - t2)) > (10L*60L*1000L*2L)) {
SERIAL_PROTOCOLLNPGM("PID Autotune failed! timeout");
#endif /* !defined(FORK) */
return;
}
#ifndef FORK
if (cycles > ncycles) {
SERIAL_PROTOCOLLNPGM(MSG_PID_AUTOTUNE_FINISHED);
#else
if(cycles > ncycles) {
SERIAL_PROTOCOLLNPGM("PID Autotune finished! Put the last Kp, Ki and Kd constants from above into Configuration.h");
#endif /* !defined(FORK) */
return;
}
lcd_update();
}
}

#ifndef FORK
void updatePID() {
#ifdef PIDTEMP
for (int e = 0; e < EXTRUDERS; e++) {
temp_iState_max[e] = PID_INTEGRAL_DRIVE_MAX / PID_PARAM(Ki,e);
}
#endif /* defined(PIDTEMP) */
#else
void updatePID()
{
#ifdef PIDTEMP
for(int e = 0; e < EXTRUDERS; e++) {
temp_iState_max[e] = PID_INTEGRAL_DRIVE_MAX / PID_PARAM(Ki,e);
}
#endif /* defined(PIDTEMP) */
#ifdef PIDTEMPBED
temp_iState_max_bed = PID_INTEGRAL_DRIVE_MAX / bedKi;
#endif /* defined(PIDTEMPBED) */
#endif /* !defined(FORK) */
#if defined(FORK) || defined(PIDTEMPBED)
#ifndef FORK
temp_iState_max_bed = PID_INTEGRAL_DRIVE_MAX / bedKi;
#endif /* !defined(FORK) */
#endif /* defined(FORK) || defined(PIDTEMPBED) */
}

#ifdef FORK
if (heater<0)
return soft_pwm_bed;
return soft_pwm[heater];
#endif /* defined(FORK) */
#ifndef FORK
return heater < 0 ? soft_pwm_bed : soft_pwm[heater];
#endif /* !defined(FORK) */
}

#if HAS_AUTO_FAN

#if (defined(FORK) || (HAS_FAN)) && (!defined(FORK) || (defined(FAN_PIN) && FAN_PIN > -1))
#if EXTRUDER_0_AUTO_FAN_PIN == FAN_PIN
//#error "You cannot set EXTRUDER_0_AUTO_FAN_PIN equal to FAN_PIN"
#endif /* EXTRUDER_0_AUTO_FAN_PIN == FAN_PIN */
#if EXTRUDER_1_AUTO_FAN_PIN == FAN_PIN
//#error "You cannot set EXTRUDER_1_AUTO_FAN_PIN equal to FAN_PIN"
#endif /* EXTRUDER_1_AUTO_FAN_PIN == FAN_PIN */
#if EXTRUDER_2_AUTO_FAN_PIN == FAN_PIN
//#error "You cannot set EXTRUDER_2_AUTO_FAN_PIN equal to FAN_PIN"
#endif /* EXTRUDER_2_AUTO_FAN_PIN == FAN_PIN */
#ifndef FORK
#if EXTRUDER_3_AUTO_FAN_PIN == FAN_PIN
//#error "You cannot set EXTRUDER_3_AUTO_FAN_PIN equal to FAN_PIN"
#endif /* EXTRUDER_3_AUTO_FAN_PIN == FAN_PIN */
#endif /* !defined(FORK) */
#endif /* (defined(FORK) || (HAS_FAN)) && (!defined(FORK) || (defined(FAN_PIN) && FAN_PIN > -1)) */

void setExtruderAutoFanState(int pin, bool state)
{
unsigned char newFanSpeed = (state != 0) ? EXTRUDER_AUTO_FAN_SPEED : 0;
// this idiom allows both digital and PWM fan outputs (see M42 handling).
pinMode(pin, OUTPUT);
digitalWrite(pin, newFanSpeed);
analogWrite(pin, newFanSpeed);
}

void checkExtruderAutoFans()
{
uint8_t fanState = 0;

// which fan pins need to be turned on?
#if (defined(FORK) || (HAS_AUTO_FAN_0)) && (!defined(FORK) || (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1))
if (current_temperature[0] > EXTRUDER_AUTO_FAN_TEMPERATURE)
fanState |= 1;
#endif /* (defined(FORK) || (HAS_AUTO_FAN_0)) && (!defined(FORK) || (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1)) */
#if (defined(FORK) || (HAS_AUTO_FAN_1)) && (!defined(FORK) || (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1))
if (current_temperature[1] > EXTRUDER_AUTO_FAN_TEMPERATURE)
{
#ifndef FORK
if (EXTRUDER_1_AUTO_FAN_PIN == EXTRUDER_0_AUTO_FAN_PIN)
#else
if (EXTRUDER_1_AUTO_FAN_PIN == EXTRUDER_0_AUTO_FAN_PIN)
#endif /* !defined(FORK) */
fanState |= 1;
else
fanState |= 2;
}
#endif /* (defined(FORK) || (HAS_AUTO_FAN_1)) && (!defined(FORK) || (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1)) */
#if (defined(FORK) || (HAS_AUTO_FAN_2)) && (!defined(FORK) || (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1))
if (current_temperature[2] > EXTRUDER_AUTO_FAN_TEMPERATURE)
{
if (EXTRUDER_2_AUTO_FAN_PIN == EXTRUDER_0_AUTO_FAN_PIN)
fanState |= 1;
else if (EXTRUDER_2_AUTO_FAN_PIN == EXTRUDER_1_AUTO_FAN_PIN)
fanState |= 2;
else
fanState |= 4;
}
#endif /* (defined(FORK) || (HAS_AUTO_FAN_2)) && (!defined(FORK) || (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1)) */
#if (defined(FORK) || (HAS_AUTO_FAN_3)) && (!defined(FORK) || (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1))
if (current_temperature[3] > EXTRUDER_AUTO_FAN_TEMPERATURE)
{
if (EXTRUDER_3_AUTO_FAN_PIN == EXTRUDER_0_AUTO_FAN_PIN)
fanState |= 1;
else if (EXTRUDER_3_AUTO_FAN_PIN == EXTRUDER_1_AUTO_FAN_PIN)
fanState |= 2;
else if (EXTRUDER_3_AUTO_FAN_PIN == EXTRUDER_2_AUTO_FAN_PIN)
fanState |= 4;
else
fanState |= 8;
}
#endif /* (defined(FORK) || (HAS_AUTO_FAN_3)) && (!defined(FORK) || (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1)) */

// update extruder auto fan states
#if (defined(FORK) || (HAS_AUTO_FAN_0)) && (!defined(FORK) || (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1))
setExtruderAutoFanState(EXTRUDER_0_AUTO_FAN_PIN, (fanState & 1) != 0);
#endif /* (defined(FORK) || (HAS_AUTO_FAN_0)) && (!defined(FORK) || (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1)) */
#if (defined(FORK) || (HAS_AUTO_FAN_1)) && (!defined(FORK) || (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1))
#ifndef FORK
if (EXTRUDER_1_AUTO_FAN_PIN != EXTRUDER_0_AUTO_FAN_PIN)
#else
if (EXTRUDER_1_AUTO_FAN_PIN != EXTRUDER_0_AUTO_FAN_PIN)
#endif /* !defined(FORK) */
setExtruderAutoFanState(EXTRUDER_1_AUTO_FAN_PIN, (fanState & 2) != 0);
#endif /* (defined(FORK) || (HAS_AUTO_FAN_1)) && (!defined(FORK) || (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1)) */
#if (defined(FORK) || (HAS_AUTO_FAN_2)) && (!defined(FORK) || (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1))
#ifndef FORK
if (EXTRUDER_2_AUTO_FAN_PIN != EXTRUDER_0_AUTO_FAN_PIN
#else
if (EXTRUDER_2_AUTO_FAN_PIN != EXTRUDER_0_AUTO_FAN_PIN
#endif /* !defined(FORK) */
&& EXTRUDER_2_AUTO_FAN_PIN != EXTRUDER_1_AUTO_FAN_PIN)
setExtruderAutoFanState(EXTRUDER_2_AUTO_FAN_PIN, (fanState & 4) != 0);
#endif /* (defined(FORK) || (HAS_AUTO_FAN_2)) && (!defined(FORK) || (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1)) */
#if (defined(FORK) || (HAS_AUTO_FAN_3)) && (!defined(FORK) || (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1))
#ifndef FORK
if (EXTRUDER_3_AUTO_FAN_PIN != EXTRUDER_0_AUTO_FAN_PIN
&& EXTRUDER_3_AUTO_FAN_PIN != EXTRUDER_1_AUTO_FAN_PIN
&& EXTRUDER_3_AUTO_FAN_PIN != EXTRUDER_2_AUTO_FAN_PIN)
#else
if (EXTRUDER_3_AUTO_FAN_PIN != EXTRUDER_0_AUTO_FAN_PIN
&& EXTRUDER_3_AUTO_FAN_PIN != EXTRUDER_1_AUTO_FAN_PIN)
&& EXTRUDER_3_AUTO_FAN_PIN != EXTRUDER_0_AUTO_FAN_PIN)
#endif /* !defined(FORK) */
setExtruderAutoFanState(EXTRUDER_3_AUTO_FAN_PIN, (fanState & 8) != 0);
#endif /* (defined(FORK) || (HAS_AUTO_FAN_3)) && (!defined(FORK) || (defined(EXTRUDER_3_AUTO_FAN_PIN) && EXTRUDER_3_AUTO_FAN_PIN > -1)) */
}

#endif /* HAS_AUTO_FAN */
#ifdef FORK
void manage_heater()
{
float pid_input;
#endif /* defined(FORK) */

#ifndef FORK
//
// Error checking and Write Routines
//
#if !HAS_HEATER_0
//#error HEATER_0_PIN not defined for this board
#endif /* !HAS_HEATER_0 */
#define WRITE_HEATER_0P(v) WRITE ( HEATER_0_PIN , v )

#if EXTRUDERS > 1 || defined(HEATERS_PARALLEL)
#if !HAS_HEATER_1
//#error HEATER_1_PIN not defined for this board
#endif /* !HAS_HEATER_1 */
#define WRITE_HEATER_1(v) WRITE ( HEATER_1_PIN , v )

#if EXTRUDERS > 2
#if !HAS_HEATER_2
//#error HEATER_2_PIN not defined for this board
#endif /* !HAS_HEATER_2 */
#define WRITE_HEATER_2(v) WRITE ( HEATER_2_PIN , v )

#if EXTRUDERS > 3
#if !HAS_HEATER_3
//#error HEATER_3_PIN not defined for this board
#endif /* !HAS_HEATER_3 */
#define WRITE_HEATER_3(v) WRITE ( HEATER_3_PIN , v )

#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 || defined(HEATERS_PARALLEL) */
#ifdef HEATERS_PARALLEL
#define WRITE_HEATER_0(v) { WRITE_HEATER_0P ( v ) ; WRITE_HEATER_1 ( v ) ; }

#else
#define WRITE_HEATER_0(v) WRITE_HEATER_0P ( v )

#endif /* defined(HEATERS_PARALLEL) */
#if HAS_HEATER_BED
#define WRITE_HEATER_BED(v) WRITE ( HEATER_BED_PIN , v )

#endif /* HAS_HEATER_BED */
#if HAS_FAN
#define WRITE_FAN(v) WRITE ( FAN_PIN , v )

#endif /* HAS_FAN */

inline void _temp_error(int e, const char *msg1, const char *msg2) {
if (!IsStopped()) {
SERIAL_ERROR_START;
if (e >= 0) SERIAL_ERRORLN((int)e);
serialprintPGM(msg1);
MYSERIAL.write('\n');
#ifdef ULTRA_LCD
lcd_setalertstatuspgm(msg2);
#endif /* defined(ULTRA_LCD) */
}
#ifndef BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE
Stop();
#endif /* !defined(BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE) */
}

void max_temp_error(uint8_t e) {
disable_heater();
_temp_error(e, PSTR(MSG_MAXTEMP_EXTRUDER_OFF), PSTR(MSG_ERR_MAXTEMP));
}
void min_temp_error(uint8_t e) {
disable_heater();
_temp_error(e, PSTR(MSG_MINTEMP_EXTRUDER_OFF), PSTR(MSG_ERR_MINTEMP));
}
void bed_max_temp_error(void) {
#if HAS_HEATER_BED
WRITE_HEATER_BED(0);
#endif /* HAS_HEATER_BED */
_temp_error(-1, PSTR(MSG_MAXTEMP_BED_OFF), PSTR(MSG_ERR_MAXTEMP_BED));
}

float get_pid_output(int e) {
#endif /* !defined(FORK) */
float pid_output;
#ifdef FORK
if(temp_meas_ready != true)   //better readability
return;
#endif /* defined(FORK) */
#if defined(FORK) || defined(PIDTEMP)
#if defined(FORK) || !defined(PID_OPENLOOP)
#ifndef FORK
pid_error[e] = target_temperature[e] - current_temperature[e];
if (pid_error[e] > PID_FUNCTIONAL_RANGE) {
pid_output = BANG_MAX;
pid_reset[e] = true;
}
else if (pid_error[e] < -PID_FUNCTIONAL_RANGE || target_temperature[e] == 0) {
pid_output = 0;
pid_reset[e] = true;
}
else {
if (pid_reset[e]) {
temp_iState[e] = 0.0;
pid_reset[e] = false;
}
pTerm[e] = PID_PARAM(Kp,e) * pid_error[e];
temp_iState[e] += pid_error[e];
temp_iState[e] = constrain(temp_iState[e], temp_iState_min[e], temp_iState_max[e]);
iTerm[e] = PID_PARAM(Ki,e) * temp_iState[e];
#endif /* !defined(FORK) */

#ifndef FORK
dTerm[e] = K2 * PID_PARAM(Kd,e) * (current_temperature[e] - temp_dState[e]) + K1 * dTerm[e];
pid_output = pTerm[e] + iTerm[e] - dTerm[e];
if (pid_output > PID_MAX) {
if (pid_error[e] > 0) temp_iState[e] -= pid_error[e]; // conditional un-integration
pid_output = PID_MAX;
}
else if (pid_output < 0) {
if (pid_error[e] < 0) temp_iState[e] -= pid_error[e]; // conditional un-integration
pid_output = 0;
}
}
temp_dState[e] = current_temperature[e];
#endif /* !defined(FORK) */
#else
#ifndef FORK
pid_output = constrain(target_temperature[e], 0, PID_MAX);
#endif /* !defined(FORK) */
#endif /* defined(FORK) || !defined(PID_OPENLOOP) */
#ifndef FORK

#ifdef PID_DEBUG
SERIAL_ECHO_START;
SERIAL_ECHO(MSG_PID_DEBUG);
SERIAL_ECHO(e);
SERIAL_ECHO(MSG_PID_DEBUG_INPUT);
SERIAL_ECHO(current_temperature[e]);
SERIAL_ECHO(MSG_PID_DEBUG_OUTPUT);
SERIAL_ECHO(pid_output);
SERIAL_ECHO(MSG_PID_DEBUG_PTERM);
SERIAL_ECHO(pTerm[e]);
SERIAL_ECHO(MSG_PID_DEBUG_ITERM);
SERIAL_ECHO(iTerm[e]);
SERIAL_ECHO(MSG_PID_DEBUG_DTERM);
SERIAL_ECHOLN(dTerm[e]);
#endif /* defined(PID_DEBUG) */

#endif /* !defined(FORK) */
#else
#ifndef FORK
pid_output = (current_temperature[e] < target_temperature[e]) ? PID_MAX : 0;
#endif /* !defined(FORK) */
#endif /* defined(FORK) || defined(PIDTEMP) */
#ifndef FORK

return pid_output;
}

#endif /* !defined(FORK) */
#if defined(FORK) || defined(PIDTEMPBED)
#ifndef FORK
float get_pid_output_bed() {
float pid_output;
#endif /* !defined(FORK) */
#if defined(FORK) || !defined(PID_OPENLOOP)
#ifndef FORK
pid_error_bed = target_temperature_bed - current_temperature_bed;
pTerm_bed = bedKp * pid_error_bed;
temp_iState_bed += pid_error_bed;
temp_iState_bed = constrain(temp_iState_bed, temp_iState_min_bed, temp_iState_max_bed);
iTerm_bed = bedKi * temp_iState_bed;

dTerm_bed = K2 * bedKd * (current_temperature_bed - temp_dState_bed) + K1 * dTerm_bed;
temp_dState_bed = current_temperature_bed;

pid_output = pTerm_bed + iTerm_bed - dTerm_bed;
if (pid_output > MAX_BED_POWER) {
if (pid_error_bed > 0) temp_iState_bed -= pid_error_bed; // conditional un-integration
pid_output = MAX_BED_POWER;
}
else if (pid_output < 0) {
if (pid_error_bed < 0) temp_iState_bed -= pid_error_bed; // conditional un-integration
pid_output = 0;
}
#endif /* !defined(FORK) */
#else
#ifndef FORK
pid_output = constrain(target_temperature_bed, 0, MAX_BED_POWER);
#endif /* !defined(FORK) */
#endif /* defined(FORK) || !defined(PID_OPENLOOP) */
#ifndef FORK

return pid_output;
}
#endif /* !defined(FORK) */
#endif /* defined(FORK) || defined(PIDTEMPBED) */
#ifndef FORK

void manage_heater() {

if (!temp_meas_ready) return;
#endif /* !defined(FORK) */

#ifdef FORK
#ifdef HEATER_0_USES_MAX6675
if (current_temperature[0] > 1023 || current_temperature[0] > HEATER_0_MAXTEMP) {
max_temp_error(0);
}
if (current_temperature[0] == 0  || current_temperature[0] < HEATER_0_MINTEMP) {
min_temp_error(0);
}
#endif /* defined(HEATER_0_USES_MAX6675) */
#endif /* defined(FORK) */
updateTemperaturesFromRawValues();
#ifdef FORK
for(int e = 0; e < EXTRUDERS; e++)
{
#endif /* defined(FORK) */

#ifndef FORK
#ifdef HEATER_0_USES_MAX6675
float ct = current_temperature[0];
if (ct > min(HEATER_0_MAXTEMP, 1023)) max_temp_error(0);
if (ct < max(HEATER_0_MINTEMP, 0.01)) min_temp_error(0);
#endif /* defined(HEATER_0_USES_MAX6675) */
#endif /* !defined(FORK) */
#ifdef PIDTEMP
#ifdef FORK
pid_input = current_temperature[e];
#if defined (THERMAL_RUNAWAY_PROTECTION_PERIOD) && THERMAL_RUNAWAY_PROTECTION_PERIOD > 0
thermal_runaway_protection(&thermal_runaway_state_machine[e], &thermal_runaway_timer[e], current_temperature[e], target_temperature[e], e, THERMAL_RUNAWAY_PROTECTION_PERIOD, THERMAL_RUNAWAY_PROTECTION_HYSTERESIS);
#endif /* defined (THERMAL_RUNAWAY_PROTECTION_PERIOD) && THERMAL_RUNAWAY_PROTECTION_PERIOD > 0 */
#endif /* defined(FORK) */
#if defined(FORK) && !defined(PID_OPENLOOP)
#ifdef FORK
pid_error[e] = target_temperature[e] - pid_input;
if(pid_error[e] > PID_FUNCTIONAL_RANGE) {
pid_output = BANG_MAX;
pid_reset[e] = true;
}
else if(pid_error[e] < -PID_FUNCTIONAL_RANGE || target_temperature[e] == 0) {
pid_output = 0;
pid_reset[e] = true;
}
else {
if(pid_reset[e] == true) {
temp_iState[e] = 0.0;
pid_reset[e] = false;
}
pTerm[e] = PID_PARAM(Kp,e) * pid_error[e];
temp_iState[e] += pid_error[e];
temp_iState[e] = constrain(temp_iState[e], temp_iState_min[e], temp_iState_max[e]);
iTerm[e] = PID_PARAM(Ki,e) * temp_iState[e];

//K1 defined in Configuration.h in the PID settings
#define K2 ( 1.0 - K1 )

dTerm[e] = (PID_PARAM(Kd,e) * (pid_input - temp_dState[e]))*K2 + (K1 * dTerm[e]);
pid_output = pTerm[e] + iTerm[e] - dTerm[e];
if (pid_output > PID_MAX) {
if (pid_error[e] > 0 )  temp_iState[e] -= pid_error[e]; // conditional un-integration
pid_output=PID_MAX;
} else if (pid_output < 0){
if (pid_error[e] < 0 )  temp_iState[e] -= pid_error[e]; // conditional un-integration
pid_output=0;
}
}
temp_dState[e] = pid_input;
#endif /* defined(FORK) */
#else
#ifdef FORK
pid_output = constrain(target_temperature[e], 0, PID_MAX);
#endif /* defined(FORK) */
#endif /* defined(FORK) && !defined(PID_OPENLOOP) */
#ifdef FORK
#ifdef PID_DEBUG
SERIAL_ECHO_START;
SERIAL_ECHO(" PID_DEBUG ");
SERIAL_ECHO(e);
SERIAL_ECHO(": Input ");
SERIAL_ECHO(pid_input);
SERIAL_ECHO(" Output ");
SERIAL_ECHO(pid_output);
SERIAL_ECHO(" pTerm ");
SERIAL_ECHO(pTerm[e]);
SERIAL_ECHO(" iTerm ");
SERIAL_ECHO(iTerm[e]);
SERIAL_ECHO(" dTerm ");
SERIAL_ECHOLN(dTerm[e]);
#endif /* defined(PID_DEBUG) */
#endif /* defined(FORK) */
#else
#ifdef FORK
pid_output = 0;
if(current_temperature[e] < target_temperature[e]) {
pid_output = PID_MAX;
}
#endif /* defined(FORK) */
#endif /* defined(PIDTEMP) */

#ifndef FORK
unsigned long ms = millis();
#endif /* !defined(FORK) */

#ifndef FORK
// Loop through all extruders
for (int e = 0; e < EXTRUDERS; e++) {
#endif /* !defined(FORK) */

#ifndef FORK
#if defined (THERMAL_RUNAWAY_PROTECTION_PERIOD) && THERMAL_RUNAWAY_PROTECTION_PERIOD > 0
thermal_runaway_protection(&thermal_runaway_state_machine[e], &thermal_runaway_timer[e], current_temperature[e], target_temperature[e], e, THERMAL_RUNAWAY_PROTECTION_PERIOD, THERMAL_RUNAWAY_PROTECTION_HYSTERESIS);
#endif /* defined (THERMAL_RUNAWAY_PROTECTION_PERIOD) && THERMAL_RUNAWAY_PROTECTION_PERIOD > 0 */
#endif /* !defined(FORK) */

#ifndef FORK
float pid_output = get_pid_output(e);
#else
if((current_temperature[e] > minttemp[e]) && (current_temperature[e] < maxttemp[e]))
{
soft_pwm[e] = (int)pid_output >> 1;
}
else {
soft_pwm[e] = 0;
}
#endif /* !defined(FORK) */

// Check if temperature is within the correct range
#ifndef FORK
soft_pwm[e] = current_temperature[e] > minttemp[e] && current_temperature[e] < maxttemp[e] ? (int)pid_output >> 1 : 0;
#endif /* !defined(FORK) */

#ifdef FORK
} // End extruder for loop
#endif /* defined(FORK) */
#ifdef WATCH_TEMP_PERIOD
#ifndef FORK
if (watchmillis[e] && ms > watchmillis[e] + WATCH_TEMP_PERIOD) {
if (degHotend(e) < watch_start_temp[e] + WATCH_TEMP_INCREASE) {
setTargetHotend(0, e);
LCD_MESSAGEPGM(MSG_HEATING_FAILED_LCD); // translatable
SERIAL_ECHO_START;
SERIAL_ECHOLNPGM(MSG_HEATING_FAILED);
#else
if(watchmillis[e] && millis() - watchmillis[e] > WATCH_TEMP_PERIOD)
{
if(degHotend(e) < watch_start_temp[e] + WATCH_TEMP_INCREASE)
{
setTargetHotend(0, e);
LCD_MESSAGEPGM("Heating failed");
SERIAL_ECHO_START;
SERIAL_ECHOLN("Heating failed");
}else{
watchmillis[e] = 0;
#endif /* !defined(FORK) */
}
#ifndef FORK
else {
watchmillis[e] = 0;
}
}
#else
}
#endif /* !defined(FORK) */
#endif /* defined(WATCH_TEMP_PERIOD) */
#ifndef FORK

#endif /* !defined(FORK) */
#ifndef PIDTEMPBED
#ifdef FORK
if(millis() - previous_millis_bed_heater < BED_CHECK_INTERVAL)
return;
previous_millis_bed_heater = millis();
#endif /* defined(FORK) */
#endif /* !defined(PIDTEMPBED) */
#ifdef FORK
#if (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1) ||      (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1) ||      (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1)
if(millis() - extruder_autofan_last_check > 2500)  // only need to check fan state very infrequently
{
checkExtruderAutoFans();
extruder_autofan_last_check = millis();
}
#endif /* (defined(EXTRUDER_0_AUTO_FAN_PIN) && EXTRUDER_0_AUTO_FAN_PIN > -1) ||      (defined(EXTRUDER_1_AUTO_FAN_PIN) && EXTRUDER_1_AUTO_FAN_PIN > -1) ||      (defined(EXTRUDER_2_AUTO_FAN_PIN) && EXTRUDER_2_AUTO_FAN_PIN > -1) */
#endif /* defined(FORK) */
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
#ifndef FORK
if (fabs(current_temperature[0] - redundant_temperature) > MAX_REDUNDANT_TEMP_SENSOR_DIFF) {
#else
if(fabs(current_temperature[0] - redundant_temperature) > MAX_REDUNDANT_TEMP_SENSOR_DIFF) {
#endif /* !defined(FORK) */
disable_heater();
#ifndef FORK
_temp_error(-1, MSG_EXTRUDER_SWITCHED_OFF, MSG_ERR_REDUNDANT_TEMP);
#else
if(IsStopped() == false) {
SERIAL_ERROR_START;
SERIAL_ERRORLNPGM("Extruder switched off. Temperature difference between temp sensors is too high !");
LCD_ALERTMESSAGEPGM("Err: REDUNDANT TEMP ERROR");
}
#ifndef BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE
Stop();
#endif /* !defined(BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE) */
#endif /* !defined(FORK) */
}
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */

#ifndef FORK
} // Extruders Loop
#endif /* !defined(FORK) */

#ifndef FORK
#if HAS_AUTO_FAN
if (ms > extruder_autofan_last_check + 2500) { // only need to check fan state very infrequently
checkExtruderAutoFans();
extruder_autofan_last_check = ms;
}
#endif /* HAS_AUTO_FAN */
#endif /* !defined(FORK) */

#ifndef FORK
#ifndef PIDTEMPBED
if (ms < previous_millis_bed_heater + BED_CHECK_INTERVAL) return;
previous_millis_bed_heater = ms;
#endif /* !defined(PIDTEMPBED) */

#else
//code for controlling the extruder rate based on the width sensor
#endif /* !defined(FORK) */
#if TEMP_SENSOR_BED != 0

#if defined(THERMAL_RUNAWAY_PROTECTION_BED_PERIOD) && THERMAL_RUNAWAY_PROTECTION_BED_PERIOD > 0
thermal_runaway_protection(&thermal_runaway_bed_state_machine, &thermal_runaway_bed_timer, current_temperature_bed, target_temperature_bed, 9, THERMAL_RUNAWAY_PROTECTION_BED_PERIOD, THERMAL_RUNAWAY_PROTECTION_BED_HYSTERESIS);
#endif /* defined(THERMAL_RUNAWAY_PROTECTION_BED_PERIOD) && THERMAL_RUNAWAY_PROTECTION_BED_PERIOD > 0 */

#ifdef PIDTEMPBED
#ifndef FORK
float pid_output = get_pid_output_bed();

soft_pwm_bed = current_temperature_bed > BED_MINTEMP && current_temperature_bed < BED_MAXTEMP ? (int)pid_output >> 1 : 0;

#else
pid_input = current_temperature_bed;

#endif /* !defined(FORK) */
#if !defined(FORK) || (!defined(PID_OPENLOOP))
#ifdef FORK
pid_error_bed = target_temperature_bed - pid_input;
pTerm_bed = bedKp * pid_error_bed;
temp_iState_bed += pid_error_bed;
temp_iState_bed = constrain(temp_iState_bed, temp_iState_min_bed, temp_iState_max_bed);
iTerm_bed = bedKi * temp_iState_bed;
#endif /* defined(FORK) */

#ifdef FORK
//K1 defined in Configuration.h in the PID settings
#define K2 ( 1.0 - K1 )

dTerm_bed= (bedKd * (pid_input - temp_dState_bed))*K2 + (K1 * dTerm_bed);
temp_dState_bed = pid_input;

pid_output = pTerm_bed + iTerm_bed - dTerm_bed;
if (pid_output > MAX_BED_POWER) {
if (pid_error_bed > 0 )  temp_iState_bed -= pid_error_bed; // conditional un-integration
pid_output=MAX_BED_POWER;
} else if (pid_output < 0){
if (pid_error_bed < 0 )  temp_iState_bed -= pid_error_bed; // conditional un-integration
pid_output=0;
}

#endif /* defined(FORK) */
#else
#ifdef FORK
pid_output = constrain(target_temperature_bed, 0, MAX_BED_POWER);
#endif /* defined(FORK) */
#endif /* !defined(FORK) || (!defined(PID_OPENLOOP)) */
#ifdef FORK
#ifdef PID_BED_DEBUG
SERIAL_ECHO_START;
SERIAL_ECHO(" PID_BED_DEBUG ");
SERIAL_ECHO(": Input ");
SERIAL_ECHO(pid_input);
SERIAL_ECHO(" Output ");
SERIAL_ECHO(pid_output);
SERIAL_ECHO(" pTerm ");
SERIAL_ECHO(pTerm_bed);
SERIAL_ECHO(" iTerm ");
SERIAL_ECHO(iTerm_bed);
SERIAL_ECHO(" dTerm ");
SERIAL_ECHOLN(dTerm_bed);
#endif /* defined(PID_BED_DEBUG) */
#endif /* defined(FORK) */
#else
// Check if temperature is within the correct band
#ifndef FORK
if (current_temperature_bed > BED_MINTEMP && current_temperature_bed < BED_MAXTEMP) {
if (current_temperature_bed >= target_temperature_bed + BED_HYSTERESIS)
#else
if((current_temperature_bed > BED_MINTEMP) && (current_temperature_bed < BED_MAXTEMP))
{
if(current_temperature_bed > target_temperature_bed + BED_HYSTERESIS)
{
#endif /* !defined(FORK) */
soft_pwm_bed = 0;
#ifndef FORK
else if (current_temperature_bed <= target_temperature_bed - BED_HYSTERESIS)
soft_pwm_bed = MAX_BED_POWER >> 1;
#else
}
else if(current_temperature_bed <= target_temperature_bed - BED_HYSTERESIS)
{
soft_pwm_bed = MAX_BED_POWER>>1;
}
#endif /* !defined(FORK) */
}
#ifndef FORK
else {
#else
else
{
#endif /* !defined(FORK) */
soft_pwm_bed = 0;
#ifndef FORK
WRITE_HEATER_BED(LOW);
#else
WRITE(HEATER_BED_PIN,LOW);
#endif /* !defined(FORK) */
}
#endif /* defined(PIDTEMPBED) */
#endif /* TEMP_SENSOR_BED != 0 */

#ifndef FORK
// Control the extruder rate based on the width sensor
#endif /* !defined(FORK) */
#ifdef FILAMENT_SENSOR
#ifndef FORK
if (filament_sensor) {
meas_shift_index = delay_index1 - meas_delay_cm;
if (meas_shift_index < 0) meas_shift_index += MAX_MEASUREMENT_DELAY + 1;  //loop around buffer if needed
#else
if(filament_sensor)
{
meas_shift_index=delay_index1-meas_delay_cm;
if(meas_shift_index<0)
meas_shift_index = meas_shift_index + (MAX_MEASUREMENT_DELAY+1);  //loop around buffer if needed
#endif /* !defined(FORK) */

#ifndef FORK
// Get the delayed info and add 100 to reconstitute to a percent of
// the nominal filament diameter then square it to get an area
meas_shift_index = constrain(meas_shift_index, 0, MAX_MEASUREMENT_DELAY);
float vm = pow((measurement_delay[meas_shift_index] + 100.0) / 100.0, 2);
if (vm < 0.01) vm = 0.01;
volumetric_multiplier[FILAMENT_SENSOR_EXTRUDER_NUM] = vm;
}
#else
//get the delayed info and add 100 to reconstitute to a percent of the nominal filament diameter
//then square it to get an area

if(meas_shift_index<0)
meas_shift_index=0;
else if (meas_shift_index>MAX_MEASUREMENT_DELAY)
meas_shift_index=MAX_MEASUREMENT_DELAY;

volumetric_multiplier[FILAMENT_SENSOR_EXTRUDER_NUM] = pow((float)(100+measurement_delay[meas_shift_index])/100.0,2);
if (volumetric_multiplier[FILAMENT_SENSOR_EXTRUDER_NUM] <0.01)
volumetric_multiplier[FILAMENT_SENSOR_EXTRUDER_NUM]=0.01;
}
#endif /* !defined(FORK) */
#endif /* defined(FILAMENT_SENSOR) */
}

#define PGM_RD_W(x) ( short ) pgm_read_word ( & x )

// Derived from RepRap FiveD extruder::getTemperature()
#ifdef FORK
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
if(e > EXTRUDERS)
#else
if(e >= EXTRUDERS)
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */
#endif /* defined(FORK) */
// For hot end temperature measurement.
static float analog2temp(int raw, uint8_t e) {
#ifndef FORK
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
if (e > EXTRUDERS)
#else
if (e >= EXTRUDERS)
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */
#endif /* !defined(FORK) */
{
#ifdef FORK
SERIAL_ERRORLNPGM(" - Invalid extruder number !");
#endif /* defined(FORK) */
SERIAL_ERROR_START;
SERIAL_ERROR((int)e);
#ifndef FORK
SERIAL_ERRORLNPGM(MSG_INVALID_EXTRUDER_NUM);
#endif /* !defined(FORK) */
kill();
return 0.0;
}
#ifdef HEATER_0_USES_MAX6675
if (e == 0)
{
return 0.25 * raw;
}
#endif /* defined(HEATER_0_USES_MAX6675) */

if(heater_ttbl_map[e] != NULL)
{
float celsius = 0;
uint8_t i;
short (*tt)[][2] = (short (*)[][2])(heater_ttbl_map[e]);

for (i=1; i<heater_ttbllen_map[e]; i++)
{
if (PGM_RD_W((*tt)[i][0]) > raw)
{
celsius = PGM_RD_W((*tt)[i-1][1]) +
(raw - PGM_RD_W((*tt)[i-1][0])) *
(float)(PGM_RD_W((*tt)[i][1]) - PGM_RD_W((*tt)[i-1][1])) /
(float)(PGM_RD_W((*tt)[i][0]) - PGM_RD_W((*tt)[i-1][0]));
break;
}
}

// Overflow: Set to last value in the table
if (i == heater_ttbllen_map[e]) celsius = PGM_RD_W((*tt)[i-1][1]);

return celsius;
}
return ((raw * ((5.0 * 100.0) / 1024.0) / OVERSAMPLENR) * TEMP_SENSOR_AD595_GAIN) + TEMP_SENSOR_AD595_OFFSET;
}

// Derived from RepRap FiveD extruder::getTemperature()
// For bed temperature measurement.
static float analog2tempBed(int raw) {
#ifdef BED_USES_THERMISTOR
float celsius = 0;
byte i;

for (i=1; i<BEDTEMPTABLE_LEN; i++)
{
if (PGM_RD_W(BEDTEMPTABLE[i][0]) > raw)
{
celsius  = PGM_RD_W(BEDTEMPTABLE[i-1][1]) +
(raw - PGM_RD_W(BEDTEMPTABLE[i-1][0])) *
(float)(PGM_RD_W(BEDTEMPTABLE[i][1]) - PGM_RD_W(BEDTEMPTABLE[i-1][1])) /
(float)(PGM_RD_W(BEDTEMPTABLE[i][0]) - PGM_RD_W(BEDTEMPTABLE[i-1][0]));
break;
}
}

// Overflow: Set to last value in the table
if (i == BEDTEMPTABLE_LEN) celsius = PGM_RD_W(BEDTEMPTABLE[i-1][1]);

return celsius;
#else
return 0;
#endif /* defined(BED_USES_THERMISTOR) */
}
#ifdef FORK
static void updateTemperaturesFromRawValues()
{
#ifdef HEATER_0_USES_MAX6675
current_temperature_raw[0] = read_max6675();
#endif /* defined(HEATER_0_USES_MAX6675) */
for(uint8_t e=0;e<EXTRUDERS;e++)
{
current_temperature[e] = analog2temp(current_temperature_raw[e], e);
}
current_temperature_bed = analog2tempBed(current_temperature_bed_raw);
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
redundant_temperature = analog2temp(redundant_temperature_raw, 1);
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */
#endif /* defined(FORK) */

#ifdef FORK
#if defined (FILAMENT_SENSOR) && (FILWIDTH_PIN > -1)
filament_width_meas = analog2widthFil();
#endif /* defined (FILAMENT_SENSOR) && (FILWIDTH_PIN > -1) */
//Reset the watchdog after we know we have a temperature measurement.
watchdog_reset();
#endif /* defined(FORK) */
/* Called to get the raw values into the the actual temperatures. The raw values are created in interrupt context,
    and this function is called from normal context as it is too slow to run in interrupts and will block the stepper routine otherwise */
#ifndef FORK
static void updateTemperaturesFromRawValues() {
#ifdef HEATER_0_USES_MAX6675
current_temperature_raw[0] = read_max6675();
#endif /* defined(HEATER_0_USES_MAX6675) */
for(uint8_t e = 0; e < EXTRUDERS; e++) {
current_temperature[e] = analog2temp(current_temperature_raw[e], e);
}
current_temperature_bed = analog2tempBed(current_temperature_bed_raw);
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
redundant_temperature = analog2temp(redundant_temperature_raw, 1);
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */
#if HAS_FILAMENT_SENSOR
filament_width_meas = analog2widthFil();
#endif /* HAS_FILAMENT_SENSOR */
//Reset the watchdog after we know we have a temperature measurement.
watchdog_reset();
#else
CRITICAL_SECTION_START;
temp_meas_ready = false;
CRITICAL_SECTION_END;
#endif /* !defined(FORK) */

#ifndef FORK
CRITICAL_SECTION_START;
temp_meas_ready = false;
CRITICAL_SECTION_END;
#endif /* !defined(FORK) */
}

#ifdef FORK
// For converting raw Filament Width to milimeters
#endif /* defined(FORK) */

#ifdef FILAMENT_SENSOR
#ifdef FORK
float analog2widthFil() {
return current_raw_filwidth/16383.0*5.0;
//return current_raw_filwidth;
}
#endif /* defined(FORK) */

#ifndef FORK
// Convert raw Filament Width to millimeters
float analog2widthFil() {
return current_raw_filwidth / 16383.0 * 5.0;
//return current_raw_filwidth;
}
#else
// For converting raw Filament Width to a ratio
int widthFil_to_size_ratio() {
#endif /* !defined(FORK) */

#ifndef FORK
// Convert raw Filament Width to a ratio
int widthFil_to_size_ratio() {
float temp = filament_width_meas;
if (temp < MEASURED_LOWER_LIMIT) temp = filament_width_nominal;  //assume sensor cut out
else if (temp > MEASURED_UPPER_LIMIT) temp = MEASURED_UPPER_LIMIT;
return filament_width_nominal / temp * 100;
}
#else
float temp;
#endif /* !defined(FORK) */

#ifdef FORK
temp=filament_width_meas;
if(filament_width_meas<MEASURED_LOWER_LIMIT)
temp=filament_width_nominal;  //assume sensor cut out
else if (filament_width_meas>MEASURED_UPPER_LIMIT)
temp= MEASURED_UPPER_LIMIT;


return(filament_width_nominal/temp*100);


}
#endif /* defined(FORK) */
#endif /* defined(FILAMENT_SENSOR) */





void tp_init()
#ifdef FORK
#if MB(RUMBA) && ((TEMP_SENSOR_0==-1)||(TEMP_SENSOR_1==-1)||(TEMP_SENSOR_2==-1)||(TEMP_SENSOR_BED==-1))
//disable RUMBA JTAG in case the thermocouple extension is plugged on top of JTAG connector
MCUCR=(1<<JTD);
MCUCR=(1<<JTD);
#endif /* MB(RUMBA) && ((TEMP_SENSOR_0==-1)||(TEMP_SENSOR_1==-1)||(TEMP_SENSOR_2==-1)||(TEMP_SENSOR_BED==-1)) */
#endif /* defined(FORK) */
{
#ifndef FORK
#if MB(RUMBA) && ((TEMP_SENSOR_0==-1)||(TEMP_SENSOR_1==-1)||(TEMP_SENSOR_2==-1)||(TEMP_SENSOR_BED==-1))
//disable RUMBA JTAG in case the thermocouple extension is plugged on top of JTAG connector
MCUCR=BIT(JTD);
MCUCR=BIT(JTD);
#endif /* MB(RUMBA) && ((TEMP_SENSOR_0==-1)||(TEMP_SENSOR_1==-1)||(TEMP_SENSOR_2==-1)||(TEMP_SENSOR_BED==-1)) */
#endif /* !defined(FORK) */

#ifdef PIDTEMPBED
#ifdef FORK
temp_iState_min_bed = 0.0;
temp_iState_max_bed = PID_INTEGRAL_DRIVE_MAX / bedKi;
#endif /* defined(FORK) */
#endif /* defined(PIDTEMPBED) */
#ifdef FORK
for(int e = 0; e < EXTRUDERS; e++) {
#endif /* defined(FORK) */
// Finish init of mult extruder arrays
#ifndef FORK
for (int e = 0; e < EXTRUDERS; e++) {
#endif /* !defined(FORK) */
#ifndef FORK
#ifdef PIDTEMP
temp_iState_min[e] = 0.0;
temp_iState_max[e] = PID_INTEGRAL_DRIVE_MAX / PID_PARAM(Ki,e);
#endif /* defined(PIDTEMP) */
#ifdef PIDTEMPBED
temp_iState_min_bed = 0.0;
temp_iState_max_bed = PID_INTEGRAL_DRIVE_MAX / bedKi;
#endif /* defined(PIDTEMPBED) */
#endif /* !defined(FORK) */
#if (defined(FORK) || (HAS_HEATER_0)) && (!defined(FORK) || (defined(FAN_PIN) && (FAN_PIN > -1)))
SET_OUTPUT(HEATER_0_PIN);
#endif /* (defined(FORK) || (HAS_HEATER_0)) && (!defined(FORK) || (defined(FAN_PIN) && (FAN_PIN > -1))) */
#if HAS_HEATER_1
SET_OUTPUT(HEATER_1_PIN);
#endif /* HAS_HEATER_1 */
#if HAS_HEATER_2
SET_OUTPUT(HEATER_2_PIN);
#endif /* HAS_HEATER_2 */
#if HAS_HEATER_3
SET_OUTPUT(HEATER_3_PIN);
#endif /* HAS_HEATER_3 */
#if HAS_HEATER_BED
SET_OUTPUT(HEATER_BED_PIN);
#endif /* HAS_HEATER_BED */
#if HAS_FAN
SET_OUTPUT(FAN_PIN);
#ifndef FORK
#ifdef FAST_PWM_FAN
setPwmFrequency(FAN_PIN, 1); // No prescaling. Pwm frequency = F_CPU/256/8
#endif /* defined(FAST_PWM_FAN) */
#ifdef FAN_SOFT_PWM
soft_pwm_fan = fanSpeedSoftPwm / 2;
#endif /* defined(FAN_SOFT_PWM) */
#else
#ifdef FAST_PWM_FAN
setPwmFrequency(FAN_PIN, 1); // No prescaling. Pwm frequency = F_CPU/256/8
#endif /* defined(FAST_PWM_FAN) */
#ifdef FAN_SOFT_PWM
soft_pwm_fan = fanSpeedSoftPwm / 2;
#endif /* defined(FAN_SOFT_PWM) */
#endif /* !defined(FORK) */
#endif /* HAS_FAN */

#ifdef HEATER_0_USES_MAX6675

#ifndef SDSUPPORT
#ifndef FORK
OUT_WRITE(SCK_PIN, LOW);
OUT_WRITE(MOSI_PIN, HIGH);
OUT_WRITE(MISO_PIN, HIGH);
#else
SET_OUTPUT(SCK_PIN);
WRITE(SCK_PIN,0);

SET_OUTPUT(MOSI_PIN);
WRITE(MOSI_PIN,1);

SET_INPUT(MISO_PIN);
WRITE(MISO_PIN,1);
#endif /* !defined(FORK) */
#else
pinMode(SS_PIN, OUTPUT);
digitalWrite(SS_PIN, HIGH);
#endif /* !defined(SDSUPPORT) */

#ifndef FORK
OUT_WRITE(MAX6675_SS,HIGH);
#else
SET_OUTPUT(MAX6675_SS);
WRITE(MAX6675_SS,1);
#endif /* !defined(FORK) */

#endif /* defined(HEATER_0_USES_MAX6675) */
#ifndef FORK

#ifdef DIDR2
#define ANALOG_SELECT(pin) do { if ( pin < 8 ) DIDR0 |= BIT ( pin ) ; else DIDR2 |= BIT ( pin - 8 ) ; } while ( 0 )

#else
#define ANALOG_SELECT(pin) do { DIDR0 |= BIT ( pin ) ; } while ( 0 )

#endif /* defined(DIDR2) */
#endif /* !defined(FORK) */

#ifdef FORK
ADCSRA = 1<<ADEN | 1<<ADSC | 1<<ADIF | 0x07;
#endif /* defined(FORK) */
// Set analog inputs
#ifndef FORK
ADCSRA = BIT(ADEN) | BIT(ADSC) | BIT(ADIF) | 0x07;
#endif /* !defined(FORK) */
DIDR0 = 0;
#ifdef FORK
#if defined(TEMP_0_PIN) && (TEMP_0_PIN > -1)
#if TEMP_0_PIN < 8
DIDR0 |= 1 << TEMP_0_PIN;
#else
DIDR2 |= 1<<(TEMP_0_PIN - 8);
#endif /* TEMP_0_PIN < 8 */
#endif /* defined(TEMP_0_PIN) && (TEMP_0_PIN > -1) */
#if defined(TEMP_1_PIN) && (TEMP_1_PIN > -1)
#if TEMP_1_PIN < 8
DIDR0 |= 1<<TEMP_1_PIN;
#else
DIDR2 |= 1<<(TEMP_1_PIN - 8);
#endif /* TEMP_1_PIN < 8 */
#endif /* defined(TEMP_1_PIN) && (TEMP_1_PIN > -1) */
#if defined(TEMP_2_PIN) && (TEMP_2_PIN > -1)
#if TEMP_2_PIN < 8
DIDR0 |= 1 << TEMP_2_PIN;
#else
DIDR2 |= 1<<(TEMP_2_PIN - 8);
#endif /* TEMP_2_PIN < 8 */
#endif /* defined(TEMP_2_PIN) && (TEMP_2_PIN > -1) */
#if defined(TEMP_3_PIN) && (TEMP_3_PIN > -1)
#if TEMP_3_PIN < 8
DIDR0 |= 1 << TEMP_3_PIN;
#else
DIDR2 |= 1<<(TEMP_3_PIN - 8);
#endif /* TEMP_3_PIN < 8 */
#endif /* defined(TEMP_3_PIN) && (TEMP_3_PIN > -1) */
#if defined(TEMP_BED_PIN) && (TEMP_BED_PIN > -1)
#if TEMP_BED_PIN < 8
DIDR0 |= 1<<TEMP_BED_PIN;
#else
DIDR2 |= 1<<(TEMP_BED_PIN - 8);
#endif /* TEMP_BED_PIN < 8 */
#endif /* defined(TEMP_BED_PIN) && (TEMP_BED_PIN > -1) */

//Added for Filament Sensor
#ifdef FILAMENT_SENSOR
#if defined(FILWIDTH_PIN) && (FILWIDTH_PIN > -1)
#if FILWIDTH_PIN < 8
DIDR0 |= 1<<FILWIDTH_PIN;
#else
DIDR2 |= 1<<(FILWIDTH_PIN - 8);
#endif /* FILWIDTH_PIN < 8 */
#endif /* defined(FILWIDTH_PIN) && (FILWIDTH_PIN > -1) */
#endif /* defined(FILAMENT_SENSOR) */
#endif /* defined(FORK) */
#ifdef DIDR2
DIDR2 = 0;
#endif /* defined(DIDR2) */
#ifndef FORK
#if HAS_TEMP_0
ANALOG_SELECT(TEMP_0_PIN);
#endif /* HAS_TEMP_0 */
#if HAS_TEMP_1
ANALOG_SELECT(TEMP_1_PIN);
#endif /* HAS_TEMP_1 */
#if HAS_TEMP_2
ANALOG_SELECT(TEMP_2_PIN);
#endif /* HAS_TEMP_2 */
#if HAS_TEMP_3
ANALOG_SELECT(TEMP_3_PIN);
#endif /* HAS_TEMP_3 */
#if HAS_TEMP_BED
ANALOG_SELECT(TEMP_BED_PIN);
#endif /* HAS_TEMP_BED */
#if HAS_FILAMENT_SENSOR
ANALOG_SELECT(FILWIDTH_PIN);
#endif /* HAS_FILAMENT_SENSOR */
#endif /* !defined(FORK) */

#ifdef FORK
TIMSK0 |= (1<<OCIE0B);
#endif /* defined(FORK) */
// Use timer0 for temperature measurement
// Interleave temperature interrupt with millies interrupt
OCR0B = 128;
#ifndef FORK
TIMSK0 |= BIT(OCIE0B);
#endif /* !defined(FORK) */

#ifdef FORK
#ifdef HEATER_0_MINTEMP
minttemp[0] = HEATER_0_MINTEMP;
while(analog2temp(minttemp_raw[0], 0) < HEATER_0_MINTEMP) {
#if HEATER_0_RAW_LO_TEMP < HEATER_0_RAW_HI_TEMP
minttemp_raw[0] += OVERSAMPLENR;
#else
minttemp_raw[0] -= OVERSAMPLENR;
#endif /* HEATER_0_RAW_LO_TEMP < HEATER_0_RAW_HI_TEMP */
}
#endif /* defined(HEATER_0_MINTEMP) */
#ifdef HEATER_0_MAXTEMP
maxttemp[0] = HEATER_0_MAXTEMP;
while(analog2temp(maxttemp_raw[0], 0) > HEATER_0_MAXTEMP) {
#if HEATER_0_RAW_LO_TEMP < HEATER_0_RAW_HI_TEMP
maxttemp_raw[0] -= OVERSAMPLENR;
#else
maxttemp_raw[0] += OVERSAMPLENR;
#endif /* HEATER_0_RAW_LO_TEMP < HEATER_0_RAW_HI_TEMP */
}
#endif /* defined(HEATER_0_MAXTEMP) */
#endif /* defined(FORK) */
// Wait for temperature measurement to settle
#ifdef FORK
#if (EXTRUDERS > 1) && defined(HEATER_1_MINTEMP)
minttemp[1] = HEATER_1_MINTEMP;
while(analog2temp(minttemp_raw[1], 1) < HEATER_1_MINTEMP) {
#if HEATER_1_RAW_LO_TEMP < HEATER_1_RAW_HI_TEMP
minttemp_raw[1] += OVERSAMPLENR;
#else
minttemp_raw[1] -= OVERSAMPLENR;
#endif /* HEATER_1_RAW_LO_TEMP < HEATER_1_RAW_HI_TEMP */
}
#endif /* (EXTRUDERS > 1) && defined(HEATER_1_MINTEMP) */
#if (EXTRUDERS > 1) && defined(HEATER_1_MAXTEMP)
maxttemp[1] = HEATER_1_MAXTEMP;
while(analog2temp(maxttemp_raw[1], 1) > HEATER_1_MAXTEMP) {
#if HEATER_1_RAW_LO_TEMP < HEATER_1_RAW_HI_TEMP
maxttemp_raw[1] -= OVERSAMPLENR;
#else
maxttemp_raw[1] += OVERSAMPLENR;
#endif /* HEATER_1_RAW_LO_TEMP < HEATER_1_RAW_HI_TEMP */
}
#endif /* (EXTRUDERS > 1) && defined(HEATER_1_MAXTEMP) */
#endif /* defined(FORK) */
delay(250);
#ifdef FORK
#if (EXTRUDERS > 2) && defined(HEATER_2_MINTEMP)
minttemp[2] = HEATER_2_MINTEMP;
while(analog2temp(minttemp_raw[2], 2) < HEATER_2_MINTEMP) {
#if HEATER_2_RAW_LO_TEMP < HEATER_2_RAW_HI_TEMP
minttemp_raw[2] += OVERSAMPLENR;
#else
minttemp_raw[2] -= OVERSAMPLENR;
#endif /* HEATER_2_RAW_LO_TEMP < HEATER_2_RAW_HI_TEMP */
}
#endif /* (EXTRUDERS > 2) && defined(HEATER_2_MINTEMP) */
#if (EXTRUDERS > 2) && defined(HEATER_2_MAXTEMP)
maxttemp[2] = HEATER_2_MAXTEMP;
while(analog2temp(maxttemp_raw[2], 2) > HEATER_2_MAXTEMP) {
#if HEATER_2_RAW_LO_TEMP < HEATER_2_RAW_HI_TEMP
maxttemp_raw[2] -= OVERSAMPLENR;
#else
maxttemp_raw[2] += OVERSAMPLENR;
#endif /* HEATER_2_RAW_LO_TEMP < HEATER_2_RAW_HI_TEMP */
}
#endif /* (EXTRUDERS > 2) && defined(HEATER_2_MAXTEMP) */

#if (EXTRUDERS > 3) && defined(HEATER_3_MINTEMP)
minttemp[3] = HEATER_3_MINTEMP;
while(analog2temp(minttemp_raw[3], 3) < HEATER_3_MINTEMP) {
#if HEATER_3_RAW_LO_TEMP < HEATER_3_RAW_HI_TEMP
minttemp_raw[3] += OVERSAMPLENR;
#else
minttemp_raw[3] -= OVERSAMPLENR;
#endif /* HEATER_3_RAW_LO_TEMP < HEATER_3_RAW_HI_TEMP */
}
#endif /* (EXTRUDERS > 3) && defined(HEATER_3_MINTEMP) */
#if (EXTRUDERS > 3) && defined(HEATER_3_MAXTEMP)
maxttemp[3] = HEATER_3_MAXTEMP;
while(analog2temp(maxttemp_raw[3], 3) > HEATER_3_MAXTEMP) {
#if HEATER_3_RAW_LO_TEMP < HEATER_3_RAW_HI_TEMP
maxttemp_raw[3] -= OVERSAMPLENR;
#else
maxttemp_raw[3] += OVERSAMPLENR;
#endif /* HEATER_3_RAW_LO_TEMP < HEATER_3_RAW_HI_TEMP */
}
#endif /* (EXTRUDERS > 3) && defined(HEATER_3_MAXTEMP) */


#ifdef BED_MINTEMP
/* No bed MINTEMP error implemented?!? */ /*
  while(analog2tempBed(bed_minttemp_raw) < BED_MINTEMP) {
#if HEATER_BED_RAW_LO_TEMP < HEATER_BED_RAW_HI_TEMP
    bed_minttemp_raw += OVERSAMPLENR;
#else
    bed_minttemp_raw -= OVERSAMPLENR;
#endif
  }
  */
#endif /* defined(BED_MINTEMP) */
#ifdef BED_MAXTEMP
while(analog2tempBed(bed_maxttemp_raw) > BED_MAXTEMP) {
#if HEATER_BED_RAW_LO_TEMP < HEATER_BED_RAW_HI_TEMP
bed_maxttemp_raw -= OVERSAMPLENR;
#else
bed_maxttemp_raw += OVERSAMPLENR;
#endif /* HEATER_BED_RAW_LO_TEMP < HEATER_BED_RAW_HI_TEMP */
}
#endif /* defined(BED_MAXTEMP) */
#endif /* defined(FORK) */

#ifndef FORK
#define TEMP_MIN_ROUTINE(NR) minttemp [ NR ] = HEATER_ ## NR ## _MINTEMP ; while ( analog2temp ( minttemp_raw [ NR ] , NR ) < HEATER_ ## NR ## _MINTEMP ) { if ( HEATER_ ## NR ## _RAW_LO_TEMP < HEATER_ ## NR ## _RAW_HI_TEMP ) minttemp_raw [ NR ] += OVERSAMPLENR ; else minttemp_raw [ NR ] -= OVERSAMPLENR ; }

#define TEMP_MAX_ROUTINE(NR) maxttemp [ NR ] = HEATER_ ## NR ## _MAXTEMP ; while ( analog2temp ( maxttemp_raw [ NR ] , NR ) > HEATER_ ## NR ## _MAXTEMP ) { if ( HEATER_ ## NR ## _RAW_LO_TEMP < HEATER_ ## NR ## _RAW_HI_TEMP ) maxttemp_raw [ NR ] -= OVERSAMPLENR ; else maxttemp_raw [ NR ] += OVERSAMPLENR ; }

#endif /* !defined(FORK) */
#ifndef FORK
#ifdef HEATER_0_MINTEMP
TEMP_MIN_ROUTINE(0);
#endif /* defined(HEATER_0_MINTEMP) */
#ifdef HEATER_0_MAXTEMP
TEMP_MAX_ROUTINE(0);
#endif /* defined(HEATER_0_MAXTEMP) */
#if EXTRUDERS > 1
#ifdef HEATER_1_MINTEMP
TEMP_MIN_ROUTINE(1);
#endif /* defined(HEATER_1_MINTEMP) */
#ifdef HEATER_1_MAXTEMP
TEMP_MAX_ROUTINE(1);
#endif /* defined(HEATER_1_MAXTEMP) */
#if EXTRUDERS > 2
#ifdef HEATER_2_MINTEMP
TEMP_MIN_ROUTINE(2);
#endif /* defined(HEATER_2_MINTEMP) */
#ifdef HEATER_2_MAXTEMP
TEMP_MAX_ROUTINE(2);
#endif /* defined(HEATER_2_MAXTEMP) */
#if EXTRUDERS > 3
#ifdef HEATER_3_MINTEMP
TEMP_MIN_ROUTINE(3);
#endif /* defined(HEATER_3_MINTEMP) */
#ifdef HEATER_3_MAXTEMP
TEMP_MAX_ROUTINE(3);
#endif /* defined(HEATER_3_MAXTEMP) */
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 */
#else
void setWatch()
{
#ifdef WATCH_TEMP_PERIOD
for (int e = 0; e < EXTRUDERS; e++)
{
if(degHotend(e) < degTargetHotend(e) - (WATCH_TEMP_INCREASE * 2))
{
watch_start_temp[e] = degHotend(e);
watchmillis[e] = millis();
}
}
#endif /* defined(WATCH_TEMP_PERIOD) */
#endif /* !defined(FORK) */

#ifndef FORK
#ifdef BED_MINTEMP
/* No bed MINTEMP error implemented?!? */ /*
    while(analog2tempBed(bed_minttemp_raw) < BED_MINTEMP) {
      #if HEATER_BED_RAW_LO_TEMP < HEATER_BED_RAW_HI_TEMP
        bed_minttemp_raw += OVERSAMPLENR;
      #else
        bed_minttemp_raw -= OVERSAMPLENR;
      #endif
    }
    */
#endif /* defined(BED_MINTEMP) */
#ifdef BED_MAXTEMP
while(analog2tempBed(bed_maxttemp_raw) > BED_MAXTEMP) {
#if HEATER_BED_RAW_LO_TEMP < HEATER_BED_RAW_HI_TEMP
bed_maxttemp_raw -= OVERSAMPLENR;
#else
bed_maxttemp_raw += OVERSAMPLENR;
#endif /* HEATER_BED_RAW_LO_TEMP < HEATER_BED_RAW_HI_TEMP */
}
#endif /* defined(BED_MAXTEMP) */
#endif /* !defined(FORK) */
}

#ifndef FORK
void setWatch() {
#ifdef WATCH_TEMP_PERIOD
unsigned long ms = millis();
for (int e = 0; e < EXTRUDERS; e++) {
if (degHotend(e) < degTargetHotend(e) - (WATCH_TEMP_INCREASE * 2)) {
watch_start_temp[e] = degHotend(e);
watchmillis[e] = ms;
}
}
#endif /* defined(WATCH_TEMP_PERIOD) */
#endif /* !defined(FORK) */
}
#ifdef FORK
void disable_heater()
{
for(int i=0;i<EXTRUDERS;i++)
setTargetHotend(0,i);
#endif /* defined(FORK) */

#ifdef FORK
#if defined(TEMP_0_PIN) && TEMP_0_PIN > -1
target_temperature[0]=0;
soft_pwm[0]=0;
#if defined(HEATER_0_PIN) && HEATER_0_PIN > -1
WRITE(HEATER_0_PIN,LOW);
#endif /* defined(HEATER_0_PIN) && HEATER_0_PIN > -1 */
#endif /* defined(TEMP_0_PIN) && TEMP_0_PIN > -1 */
#endif /* defined(FORK) */
#if defined(THERMAL_RUNAWAY_PROTECTION_PERIOD) && THERMAL_RUNAWAY_PROTECTION_PERIOD > 0
void thermal_runaway_protection(int *state, unsigned long *timer, float temperature, float target_temperature, int heater_id, int period_seconds, int hysteresis_degc)
{
/*
      SERIAL_ECHO_START;
      SERIAL_ECHO("Thermal Thermal Runaway Running. Heater ID:");
      SERIAL_ECHO(heater_id);
      SERIAL_ECHO(" ;  State:");
      SERIAL_ECHO(*state);
      SERIAL_ECHO(" ;  Timer:");
      SERIAL_ECHO(*timer);
      SERIAL_ECHO(" ;  Temperature:");
      SERIAL_ECHO(temperature);
      SERIAL_ECHO(" ;  Target Temp:");
      SERIAL_ECHO(target_temperature);
      SERIAL_ECHOLN("");
*/
if ((target_temperature == 0) || thermal_runaway)
{
*state = 0;
*timer = 0;
return;
}
switch (*state)
{
case 0: // "Heater Inactive" state
if (target_temperature > 0) *state = 1;
break;
case 1: // "First Heating" state
if (temperature >= target_temperature) *state = 2;
break;
case 2: // "Temperature Stable" state
#ifndef FORK
{
unsigned long ms = millis();
#endif /* !defined(FORK) */
if (temperature >= (target_temperature - hysteresis_degc))
{
#ifndef FORK
*timer = ms;
#else
*timer = millis();
#endif /* !defined(FORK) */
}
#ifndef FORK
else if ( (ms - *timer) > ((unsigned long) period_seconds) * 1000)
#else
else if ( (millis() - *timer) > ((unsigned long) period_seconds) * 1000)
#endif /* !defined(FORK) */
{
SERIAL_ERROR_START;
#ifndef FORK
SERIAL_ERRORLNPGM(MSG_THERMAL_RUNAWAY_STOP);
#else
SERIAL_ERRORLNPGM("Thermal Runaway, system stopped! Heater_ID: ");
#endif /* !defined(FORK) */
SERIAL_ERRORLN((int)heater_id);
#ifndef FORK
LCD_ALERTMESSAGEPGM(MSG_THERMAL_RUNAWAY); // translatable
#else
LCD_ALERTMESSAGEPGM("THERMAL RUNAWAY");
#endif /* !defined(FORK) */
thermal_runaway = true;
while(1)
{
disable_heater();
disable_x();
disable_y();
disable_z();
disable_e0();
disable_e1();
disable_e2();
disable_e3();
manage_heater();
lcd_update();
}
}
#ifndef FORK
} break;
#else
break;
#endif /* !defined(FORK) */
}
}
#endif /* defined(THERMAL_RUNAWAY_PROTECTION_PERIOD) && THERMAL_RUNAWAY_PROTECTION_PERIOD > 0 */
#ifdef FORK
#if defined(TEMP_1_PIN) && TEMP_1_PIN > -1 && EXTRUDERS > 1
target_temperature[1]=0;
soft_pwm[1]=0;
#if defined(HEATER_1_PIN) && HEATER_1_PIN > -1
WRITE(HEATER_1_PIN,LOW);
#endif /* defined(HEATER_1_PIN) && HEATER_1_PIN > -1 */
#endif /* defined(TEMP_1_PIN) && TEMP_1_PIN > -1 && EXTRUDERS > 1 */
#endif /* defined(FORK) */

#ifndef FORK

void disable_heater() {
for (int i=0; i<EXTRUDERS; i++) setTargetHotend(0, i);
#else
#if defined(TEMP_2_PIN) && TEMP_2_PIN > -1 && EXTRUDERS > 2
target_temperature[2]=0;
soft_pwm[2]=0;
#if defined(HEATER_2_PIN) && HEATER_2_PIN > -1
WRITE(HEATER_2_PIN,LOW);
#endif /* defined(HEATER_2_PIN) && HEATER_2_PIN > -1 */
#endif /* defined(TEMP_2_PIN) && TEMP_2_PIN > -1 && EXTRUDERS > 2 */
#endif /* !defined(FORK) */
setTargetBed(0);
#ifdef FORK
#if defined(TEMP_3_PIN) && TEMP_3_PIN > -1 && EXTRUDERS > 3
target_temperature[3]=0;
soft_pwm[3]=0;
#if defined(HEATER_3_PIN) && HEATER_3_PIN > -1
WRITE(HEATER_3_PIN,LOW);
#endif /* defined(HEATER_3_PIN) && HEATER_3_PIN > -1 */
#endif /* defined(TEMP_3_PIN) && TEMP_3_PIN > -1 && EXTRUDERS > 3 */
#endif /* defined(FORK) */

#ifndef FORK
#if HAS_TEMP_0
target_temperature[0] = 0;
soft_pwm[0] = 0;
WRITE_HEATER_0P(LOW); // If HEATERS_PARALLEL should apply, change to WRITE_HEATER_0
#endif /* HAS_TEMP_0 */
#endif /* !defined(FORK) */

#ifndef FORK
#if EXTRUDERS > 1 && HAS_TEMP_1
target_temperature[1] = 0;
soft_pwm[1] = 0;
WRITE_HEATER_1(LOW);
#endif /* EXTRUDERS > 1 && HAS_TEMP_1 */
#else
#if defined(TEMP_BED_PIN) && TEMP_BED_PIN > -1
target_temperature_bed=0;
soft_pwm_bed=0;
#if defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1
WRITE(HEATER_BED_PIN,LOW);
#endif /* defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1 */
#endif /* defined(TEMP_BED_PIN) && TEMP_BED_PIN > -1 */
#endif /* !defined(FORK) */

#ifndef FORK
#if EXTRUDERS > 2 && HAS_TEMP_2
target_temperature[2] = 0;
soft_pwm[2] = 0;
WRITE_HEATER_2(LOW);
#endif /* EXTRUDERS > 2 && HAS_TEMP_2 */
#endif /* !defined(FORK) */

#ifndef FORK
#if EXTRUDERS > 3 && HAS_TEMP_3
target_temperature[3] = 0;
soft_pwm[3] = 0;
WRITE_HEATER_3(LOW);
#endif /* EXTRUDERS > 3 && HAS_TEMP_3 */
#else
void max_temp_error(uint8_t e) {
disable_heater();
if(IsStopped() == false) {
SERIAL_ERROR_START;
SERIAL_ERRORLN((int)e);
SERIAL_ERRORLNPGM(": Extruder switched off. MAXTEMP triggered !");
LCD_ALERTMESSAGEPGM("Err: MAXTEMP");
}
#ifndef BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE
Stop();
#endif /* !defined(BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE) */
}

void min_temp_error(uint8_t e) {
disable_heater();
if(IsStopped() == false) {
SERIAL_ERROR_START;
SERIAL_ERRORLN((int)e);
SERIAL_ERRORLNPGM(": Extruder switched off. MINTEMP triggered !");
LCD_ALERTMESSAGEPGM("Err: MINTEMP");
}
#ifndef BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE
Stop();
#endif /* !defined(BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE) */
}

void bed_max_temp_error(void) {
#if HEATER_BED_PIN > -1
WRITE(HEATER_BED_PIN, 0);
#endif /* HEATER_BED_PIN > -1 */
if(IsStopped() == false) {
SERIAL_ERROR_START;
SERIAL_ERRORLNPGM("Temperature heated bed switched off. MAXTEMP triggered !!");
LCD_ALERTMESSAGEPGM("Err: MAXTEMP BED");
}
#ifndef BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE
Stop();
#endif /* !defined(BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE) */
}

#endif /* !defined(FORK) */

#ifndef FORK
#if HAS_TEMP_BED
target_temperature_bed = 0;
soft_pwm_bed = 0;
#if HAS_HEATER_BED
WRITE_HEATER_BED(LOW);
#endif /* HAS_HEATER_BED */
#endif /* HAS_TEMP_BED */
#endif /* !defined(FORK) */
}
#ifdef FORK

#endif /* defined(FORK) */

#ifdef FORK
ISR(TIMER0_COMPB_vect)
{
#endif /* defined(FORK) */
#ifdef HEATER_0_USES_MAX6675
#define MAX6675_HEAT_INTERVAL 250

#ifndef FORK
long max6675_previous_millis = MAX6675_HEAT_INTERVAL;
int max6675_temp = 2000;
#else
long max6675_previous_millis = MAX6675_HEAT_INTERVAL;
int max6675_temp = 2000;
#endif /* !defined(FORK) */

#ifndef FORK
static int read_max6675() {

unsigned long ms = millis();
if (ms < max6675_previous_millis + MAX6675_HEAT_INTERVAL)
return max6675_temp;

max6675_previous_millis = ms;
max6675_temp = 0;

#ifdef PRR
PRR &= ~BIT(PRSPI);
#endif /* defined(PRR) */

SPCR = BIT(MSTR) | BIT(SPE) | BIT(SPR0);

// enable TT_MAX6675
WRITE(MAX6675_SS, 0);

// ensure 100ns delay - a bit extra is fine
asm("nop");//50ns on 20Mhz, 62.5ns on 16Mhz
asm("nop");//50ns on 20Mhz, 62.5ns on 16Mhz

// read MSB
SPDR = 0;
for (;(SPSR & BIT(SPIF)) == 0;);
max6675_temp = SPDR;
max6675_temp <<= 8;

// read LSB
SPDR = 0;
for (;(SPSR & BIT(SPIF)) == 0;);
max6675_temp |= SPDR;

// disable TT_MAX6675
WRITE(MAX6675_SS, 1);

if (max6675_temp & 4) {
// thermocouple open
max6675_temp = 4000;
}
else {
max6675_temp = max6675_temp >> 3;
}

#else
static int read_max6675()
{
if (millis() - max6675_previous_millis < MAX6675_HEAT_INTERVAL)
#endif /* !defined(FORK) */
return max6675_temp;
#ifdef FORK

max6675_previous_millis = millis();
max6675_temp = 0;

#ifdef PRR
PRR &= ~(1<<PRSPI);
#endif /* defined(PRR) */

SPCR = (1<<MSTR) | (1<<SPE) | (1<<SPR0);

// enable TT_MAX6675
WRITE(MAX6675_SS, 0);

// ensure 100ns delay - a bit extra is fine
asm("nop");//50ns on 20Mhz, 62.5ns on 16Mhz
asm("nop");//50ns on 20Mhz, 62.5ns on 16Mhz

// read MSB
SPDR = 0;
for (;(SPSR & (1<<SPIF)) == 0;);
max6675_temp = SPDR;
max6675_temp <<= 8;

// read LSB
SPDR = 0;
for (;(SPSR & (1<<SPIF)) == 0;);
max6675_temp |= SPDR;

// disable TT_MAX6675
WRITE(MAX6675_SS, 1);

if (max6675_temp & 4)
{
// thermocouple open
max6675_temp = 4000;
}
else
{
max6675_temp = max6675_temp >> 3;
#endif /* defined(FORK) */
}

#ifdef FORK
return max6675_temp;
}

#endif /* defined(FORK) */
#endif /* defined(HEATER_0_USES_MAX6675) */
#ifndef FORK

/**
 * Stages in the ISR loop
 */
enum TempState {
PrepareTemp_0,
MeasureTemp_0,
PrepareTemp_BED,
MeasureTemp_BED,
PrepareTemp_1,
MeasureTemp_1,
PrepareTemp_2,
MeasureTemp_2,
PrepareTemp_3,
MeasureTemp_3,
Prepare_FILWIDTH,
Measure_FILWIDTH,
StartupDelay // Startup, delay initial temp reading a tiny bit so the hardware can settle
};
#endif /* !defined(FORK) */

#ifndef FORK
//
#endif /* !defined(FORK) */
// Timer 0 is shared with millies
#ifndef FORK
//
ISR(TIMER0_COMPB_vect) {
#endif /* !defined(FORK) */
//these variables are only accesible from the ISR, but static, so they don't lose their value
static unsigned char temp_count = 0;
static unsigned long raw_temp_0_value = 0;
static unsigned long raw_temp_1_value = 0;
#ifdef FORK
static unsigned char temp_state = 12;
static unsigned char pwm_count = (1 << SOFT_PWM_SCALE);
static unsigned char soft_pwm_0;
#ifdef SLOW_PWM_HEATERS
static unsigned char slow_pwm_count = 0;
static unsigned char state_heater_0 = 0;
static unsigned char state_timer_heater_0 = 0;
#endif /* defined(SLOW_PWM_HEATERS) */
#endif /* defined(FORK) */
static unsigned long raw_temp_2_value = 0;
#ifdef FORK
#if (EXTRUDERS > 1) || defined(HEATERS_PARALLEL)
static unsigned char soft_pwm_1;
#ifdef SLOW_PWM_HEATERS
static unsigned char state_heater_1 = 0;
static unsigned char state_timer_heater_1 = 0;
#endif /* defined(SLOW_PWM_HEATERS) */
#endif /* (EXTRUDERS > 1) || defined(HEATERS_PARALLEL) */
#if EXTRUDERS > 2
static unsigned char soft_pwm_2;
#ifdef SLOW_PWM_HEATERS
static unsigned char state_heater_2 = 0;
static unsigned char state_timer_heater_2 = 0;
#endif /* defined(SLOW_PWM_HEATERS) */
#endif /* EXTRUDERS > 2 */
#if EXTRUDERS > 3
static unsigned char soft_pwm_3;
#ifdef SLOW_PWM_HEATERS
static unsigned char state_heater_3 = 0;
static unsigned char state_timer_heater_3 = 0;
#endif /* defined(SLOW_PWM_HEATERS) */
#endif /* EXTRUDERS > 3 */
#endif /* defined(FORK) */
static unsigned long raw_temp_3_value = 0;
#ifdef FORK
#if HEATER_BED_PIN > -1
static unsigned char soft_pwm_b;
#ifdef SLOW_PWM_HEATERS
static unsigned char state_heater_b = 0;
static unsigned char state_timer_heater_b = 0;
#endif /* defined(SLOW_PWM_HEATERS) */
#endif /* HEATER_BED_PIN > -1 */
#endif /* defined(FORK) */
static unsigned long raw_temp_bed_value = 0;
#ifndef FORK
static TempState temp_state = StartupDelay;
static unsigned char pwm_count = BIT(SOFT_PWM_SCALE);
#else
#if defined(FILWIDTH_PIN) &&(FILWIDTH_PIN > -1)
static unsigned long raw_filwidth_value = 0;  //added for filament width sensor
#endif /* defined(FILWIDTH_PIN) &&(FILWIDTH_PIN > -1) */
#endif /* !defined(FORK) */

#ifndef FORK
// Static members for each heater
#ifdef SLOW_PWM_HEATERS
static unsigned char slow_pwm_count = 0;
#define ISR_STATICS(n) static unsigned char soft_pwm_ ## n ; static unsigned char state_heater_ ## n = 0 ; static unsigned char state_timer_heater_ ## n = 0

#else
#define ISR_STATICS(n) static unsigned char soft_pwm_ ## n

#endif /* defined(SLOW_PWM_HEATERS) */
#endif /* !defined(FORK) */

#ifndef FORK
// Statics per heater
ISR_STATICS(0);
#if (EXTRUDERS > 1) || defined(HEATERS_PARALLEL)
ISR_STATICS(1);
#if EXTRUDERS > 2
ISR_STATICS(2);
#if EXTRUDERS > 3
ISR_STATICS(3);
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* (EXTRUDERS > 1) || defined(HEATERS_PARALLEL) */
#if HAS_HEATER_BED
ISR_STATICS(BED);
#endif /* HAS_HEATER_BED */
#endif /* !defined(FORK) */

#ifndef FORK
#if HAS_FILAMENT_SENSOR
static unsigned long raw_filwidth_value = 0;
#endif /* HAS_FILAMENT_SENSOR */
#endif /* !defined(FORK) */

#ifdef FORK
#if defined(TEMP_0_PIN) && (TEMP_0_PIN > -1)
#if TEMP_0_PIN > 7
ADCSRB = 1<<MUX5;
#else
ADCSRB = 0;
#endif /* TEMP_0_PIN > 7 */
ADMUX = ((1 << REFS0) | (TEMP_0_PIN & 0x07));
ADCSRA |= 1<<ADSC; // Start conversion
#endif /* defined(TEMP_0_PIN) && (TEMP_0_PIN > -1) */
#endif /* defined(FORK) */
#if defined(FORK) || !defined(SLOW_PWM_HEATERS)
#ifndef FORK
/**
     * standard PWM modulation
     */
if (pwm_count == 0) {
soft_pwm_0 = soft_pwm[0];
if (soft_pwm_0 > 0) {
WRITE_HEATER_0(1);
}
else WRITE_HEATER_0P(0); // If HEATERS_PARALLEL should apply, change to WRITE_HEATER_0

#if EXTRUDERS > 1
soft_pwm_1 = soft_pwm[1];
WRITE_HEATER_1(soft_pwm_1 > 0 ? 1 : 0);
#if EXTRUDERS > 2
soft_pwm_2 = soft_pwm[2];
WRITE_HEATER_2(soft_pwm_2 > 0 ? 1 : 0);
#if EXTRUDERS > 3
soft_pwm_3 = soft_pwm[3];
WRITE_HEATER_3(soft_pwm_3 > 0 ? 1 : 0);
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 */

#if HAS_HEATER_BED
soft_pwm_BED = soft_pwm_bed;
WRITE_HEATER_BED(soft_pwm_BED > 0 ? 1 : 0);
#endif /* HAS_HEATER_BED */
#ifdef FAN_SOFT_PWM
soft_pwm_fan = fanSpeedSoftPwm / 2;
WRITE_FAN(soft_pwm_fan > 0 ? 1 : 0);
#endif /* defined(FAN_SOFT_PWM) */
}

if (soft_pwm_0 < pwm_count) { WRITE_HEATER_0(0); }
#if EXTRUDERS > 1
if (soft_pwm_1 < pwm_count) WRITE_HEATER_1(0);
#if EXTRUDERS > 2
if (soft_pwm_2 < pwm_count) WRITE_HEATER_2(0);
#if EXTRUDERS > 3
if (soft_pwm_3 < pwm_count) WRITE_HEATER_3(0);
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 */

#if HAS_HEATER_BED
if (soft_pwm_BED < pwm_count) WRITE_HEATER_BED(0);
#endif /* HAS_HEATER_BED */

#ifdef FAN_SOFT_PWM
if (soft_pwm_fan < pwm_count) WRITE_FAN(0);
#endif /* defined(FAN_SOFT_PWM) */

pwm_count += BIT(SOFT_PWM_SCALE);
pwm_count &= 0x7f;

#else
/*
   * standard PWM modulation
   */
if(pwm_count == 0){
soft_pwm_0 = soft_pwm[0];
if(soft_pwm_0 > 0) {
WRITE(HEATER_0_PIN,1);
#ifdef HEATERS_PARALLEL
WRITE(HEATER_1_PIN,1);
#endif /* defined(HEATERS_PARALLEL) */
} else WRITE(HEATER_0_PIN,0);

#if EXTRUDERS > 1
soft_pwm_1 = soft_pwm[1];
if(soft_pwm_1 > 0) WRITE(HEATER_1_PIN,1); else WRITE(HEATER_1_PIN,0);
#endif /* EXTRUDERS > 1 */
#if EXTRUDERS > 2
soft_pwm_2 = soft_pwm[2];
if(soft_pwm_2 > 0) WRITE(HEATER_2_PIN,1); else WRITE(HEATER_2_PIN,0);
#endif /* EXTRUDERS > 2 */
#if EXTRUDERS > 3
soft_pwm_3 = soft_pwm[3];
if(soft_pwm_3 > 0) WRITE(HEATER_3_PIN,1); else WRITE(HEATER_3_PIN,0);
#endif /* EXTRUDERS > 3 */


#if defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1
soft_pwm_b = soft_pwm_bed;
if(soft_pwm_b > 0) WRITE(HEATER_BED_PIN,1); else WRITE(HEATER_BED_PIN,0);
#endif /* defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1 */
#ifdef FAN_SOFT_PWM
soft_pwm_fan = fanSpeedSoftPwm / 2;
if(soft_pwm_fan > 0) WRITE(FAN_PIN,1); else WRITE(FAN_PIN,0);
#endif /* defined(FAN_SOFT_PWM) */
}
if(soft_pwm_0 < pwm_count) {
WRITE(HEATER_0_PIN,0);
#ifdef HEATERS_PARALLEL
WRITE(HEATER_1_PIN,0);
#endif /* defined(HEATERS_PARALLEL) */
}

#if EXTRUDERS > 1
if(soft_pwm_1 < pwm_count) WRITE(HEATER_1_PIN,0);
#endif /* EXTRUDERS > 1 */
#if EXTRUDERS > 2
if(soft_pwm_2 < pwm_count) WRITE(HEATER_2_PIN,0);
#endif /* EXTRUDERS > 2 */
#if EXTRUDERS > 3
if(soft_pwm_3 < pwm_count) WRITE(HEATER_3_PIN,0);
#endif /* EXTRUDERS > 3 */

#if defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1
if(soft_pwm_b < pwm_count) WRITE(HEATER_BED_PIN,0);
#endif /* defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1 */
#ifdef FAN_SOFT_PWM
if(soft_pwm_fan < pwm_count) WRITE(FAN_PIN,0);
#endif /* defined(FAN_SOFT_PWM) */

pwm_count += (1 << SOFT_PWM_SCALE);
pwm_count &= 0x7f;

#endif /* !defined(FORK) */
#else
#ifndef FORK
/*
     * SLOW PWM HEATERS
     *
     * for heaters drived by relay
     */
#ifndef MIN_STATE_TIME
#define MIN_STATE_TIME 16

#endif /* !defined(MIN_STATE_TIME) */

// Macros for Slow PWM timer logic - HEATERS_PARALLEL applies
#define _SLOW_PWM_ROUTINE(NR, src) soft_pwm_ ## NR = src ; if ( soft_pwm_ ## NR > 0 ) { if ( state_timer_heater_ ## NR == 0 ) { if ( state_heater_ ## NR == 0 ) state_timer_heater_ ## NR = MIN_STATE_TIME ; state_heater_ ## NR = 1 ; WRITE_HEATER_ ## NR ( 1 ) ; } } else { if ( state_timer_heater_ ## NR == 0 ) { if ( state_heater_ ## NR == 1 ) state_timer_heater_ ## NR = MIN_STATE_TIME ; state_heater_ ## NR = 0 ; WRITE_HEATER_ ## NR ( 0 ) ; } }

#define SLOW_PWM_ROUTINE(n) _SLOW_PWM_ROUTINE ( n , soft_pwm [ n ] )

#define PWM_OFF_ROUTINE(NR) if ( soft_pwm_ ## NR < slow_pwm_count ) { if ( state_timer_heater_ ## NR == 0 ) { if ( state_heater_ ## NR == 1 ) state_timer_heater_ ## NR = MIN_STATE_TIME ; state_heater_ ## NR = 0 ; WRITE_HEATER_ ## NR ( 0 ) ; } }


if (slow_pwm_count == 0) {

SLOW_PWM_ROUTINE(0); // EXTRUDER 0
#if EXTRUDERS > 1
SLOW_PWM_ROUTINE(1); // EXTRUDER 1
#if EXTRUDERS > 2
SLOW_PWM_ROUTINE(2); // EXTRUDER 2
#if EXTRUDERS > 3
SLOW_PWM_ROUTINE(3); // EXTRUDER 3
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 */
#if HAS_HEATER_BED
_SLOW_PWM_ROUTINE(BED, soft_pwm_bed); // BED
#endif /* HAS_HEATER_BED */

} // slow_pwm_count == 0

PWM_OFF_ROUTINE(0); // EXTRUDER 0
#if EXTRUDERS > 1
PWM_OFF_ROUTINE(1); // EXTRUDER 1
#if EXTRUDERS > 2
PWM_OFF_ROUTINE(2); // EXTRUDER 2
#if EXTRUDERS > 3
PWM_OFF_ROUTINE(3); // EXTRUDER 3
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 */
#if HAS_HEATER_BED
PWM_OFF_ROUTINE(BED); // BED
#endif /* HAS_HEATER_BED */

#else
/*
   * SLOW PWM HEATERS
   *
   * for heaters drived by relay
   */
#ifndef MIN_STATE_TIME
#define MIN_STATE_TIME 16

#endif /* !defined(MIN_STATE_TIME) */
if (slow_pwm_count == 0) {
// EXTRUDER 0
soft_pwm_0 = soft_pwm[0];
if (soft_pwm_0 > 0) {
// turn ON heather only if the minimum time is up
if (state_timer_heater_0 == 0) {
// if change state set timer
if (state_heater_0 == 0) {
state_timer_heater_0 = MIN_STATE_TIME;
}
state_heater_0 = 1;
WRITE(HEATER_0_PIN, 1);
#ifdef HEATERS_PARALLEL
WRITE(HEATER_1_PIN, 1);
#endif /* defined(HEATERS_PARALLEL) */
}
} else {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_0 == 0) {
// if change state set timer
if (state_heater_0 == 1) {
state_timer_heater_0 = MIN_STATE_TIME;
}
state_heater_0 = 0;
WRITE(HEATER_0_PIN, 0);
#ifdef HEATERS_PARALLEL
WRITE(HEATER_1_PIN, 0);
#endif /* defined(HEATERS_PARALLEL) */
#endif /* !defined(FORK) */
#if defined(FORK) || defined(FAN_SOFT_PWM)
#ifndef FORK
if (pwm_count == 0) {
soft_pwm_fan = fanSpeedSoftPwm / 2;
WRITE_FAN(soft_pwm_fan > 0 ? 1 : 0);
#endif /* !defined(FORK) */
}
#ifndef FORK
if (soft_pwm_fan < pwm_count) WRITE_FAN(0);
#endif /* !defined(FORK) */
#endif /* defined(FORK) || defined(FAN_SOFT_PWM) */
#ifdef FORK
}
#endif /* defined(FORK) */

#ifndef FORK
pwm_count += BIT(SOFT_PWM_SCALE);
pwm_count &= 0x7f;
#else
#if EXTRUDERS > 1
// EXTRUDER 1
soft_pwm_1 = soft_pwm[1];
if (soft_pwm_1 > 0) {
// turn ON heather only if the minimum time is up
if (state_timer_heater_1 == 0) {
// if change state set timer
if (state_heater_1 == 0) {
state_timer_heater_1 = MIN_STATE_TIME;
}
state_heater_1 = 1;
WRITE(HEATER_1_PIN, 1);
}
} else {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_1 == 0) {
// if change state set timer
if (state_heater_1 == 1) {
state_timer_heater_1 = MIN_STATE_TIME;
}
state_heater_1 = 0;
WRITE(HEATER_1_PIN, 0);
}
}
#endif /* EXTRUDERS > 1 */
#if EXTRUDERS > 2
// EXTRUDER 2
soft_pwm_2 = soft_pwm[2];
if (soft_pwm_2 > 0) {
// turn ON heather only if the minimum time is up
if (state_timer_heater_2 == 0) {
// if change state set timer
if (state_heater_2 == 0) {
state_timer_heater_2 = MIN_STATE_TIME;
}
state_heater_2 = 1;
WRITE(HEATER_2_PIN, 1);
}
} else {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_2 == 0) {
// if change state set timer
if (state_heater_2 == 1) {
state_timer_heater_2 = MIN_STATE_TIME;
}
state_heater_2 = 0;
WRITE(HEATER_2_PIN, 0);
}
}
#endif /* EXTRUDERS > 2 */
#endif /* !defined(FORK) */

#ifndef FORK
// increment slow_pwm_count only every 64 pwm_count circa 65.5ms
if ((pwm_count % 64) == 0) {
slow_pwm_count++;
slow_pwm_count &= 0x7f;
#else
#if EXTRUDERS > 3
// EXTRUDER 3
soft_pwm_3 = soft_pwm[3];
if (soft_pwm_3 > 0) {
// turn ON heather only if the minimum time is up
if (state_timer_heater_3 == 0) {
// if change state set timer
if (state_heater_3 == 0) {
state_timer_heater_3 = MIN_STATE_TIME;
}
state_heater_3 = 1;
WRITE(HEATER_3_PIN, 1);
}
} else {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_3 == 0) {
// if change state set timer
if (state_heater_3 == 1) {
state_timer_heater_3 = MIN_STATE_TIME;
}
state_heater_3 = 0;
WRITE(HEATER_3_PIN, 0);
}
}
#endif /* EXTRUDERS > 3 */
#if defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1
// BED
soft_pwm_b = soft_pwm_bed;
if (soft_pwm_b > 0) {
// turn ON heather only if the minimum time is up
if (state_timer_heater_b == 0) {
// if change state set timer
if (state_heater_b == 0) {
state_timer_heater_b = MIN_STATE_TIME;
}
state_heater_b = 1;
WRITE(HEATER_BED_PIN, 1);
}
} else {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_b == 0) {
// if change state set timer
if (state_heater_b == 1) {
state_timer_heater_b = MIN_STATE_TIME;
}
state_heater_b = 0;
WRITE(HEATER_BED_PIN, 0);
}
}
#endif /* defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1 */
} // if (slow_pwm_count == 0)
#endif /* !defined(FORK) */

#ifndef FORK
// EXTRUDER 0
if (state_timer_heater_0 > 0) state_timer_heater_0--;
#if EXTRUDERS > 1
if (state_timer_heater_1 > 0) state_timer_heater_1--;
#if EXTRUDERS > 2
if (state_timer_heater_2 > 0) state_timer_heater_2--;
#if EXTRUDERS > 3
if (state_timer_heater_3 > 0) state_timer_heater_3--;
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 */
#if HAS_HEATER_BED
if (state_timer_heater_BED > 0) state_timer_heater_BED--;
#endif /* HAS_HEATER_BED */
} // (pwm_count % 64) == 0
#else
// EXTRUDER 0
if (soft_pwm_0 < slow_pwm_count) {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_0 == 0) {
// if change state set timer
if (state_heater_0 == 1) {
state_timer_heater_0 = MIN_STATE_TIME;
}
state_heater_0 = 0;
WRITE(HEATER_0_PIN, 0);
#ifdef HEATERS_PARALLEL
WRITE(HEATER_1_PIN, 0);
#endif /* defined(HEATERS_PARALLEL) */
}
}
#endif /* !defined(FORK) */

#ifdef FORK
#if EXTRUDERS > 1
// EXTRUDER 1
if (soft_pwm_1 < slow_pwm_count) {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_1 == 0) {
// if change state set timer
if (state_heater_1 == 1) {
state_timer_heater_1 = MIN_STATE_TIME;
}
state_heater_1 = 0;
WRITE(HEATER_1_PIN, 0);
}
}
#endif /* EXTRUDERS > 1 */
#if EXTRUDERS > 2
// EXTRUDER 2
if (soft_pwm_2 < slow_pwm_count) {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_2 == 0) {
// if change state set timer
if (state_heater_2 == 1) {
state_timer_heater_2 = MIN_STATE_TIME;
}
state_heater_2 = 0;
WRITE(HEATER_2_PIN, 0);
}
}
#endif /* EXTRUDERS > 2 */
#if EXTRUDERS > 3
// EXTRUDER 3
if (soft_pwm_3 < slow_pwm_count) {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_3 == 0) {
// if change state set timer
if (state_heater_3 == 1) {
state_timer_heater_3 = MIN_STATE_TIME;
}
state_heater_3 = 0;
WRITE(HEATER_3_PIN, 0);
}
}
#endif /* EXTRUDERS > 3 */
#if defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1
// BED
if (soft_pwm_b < slow_pwm_count) {
// turn OFF heather only if the minimum time is up
if (state_timer_heater_b == 0) {
// if change state set timer
if (state_heater_b == 1) {
state_timer_heater_b = MIN_STATE_TIME;
}
state_heater_b = 0;
WRITE(HEATER_BED_PIN, 0);
}
}
#endif /* defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1 */

#ifdef FAN_SOFT_PWM
if (pwm_count == 0){
soft_pwm_fan = fanSpeedSoftPwm / 2;
if (soft_pwm_fan > 0) WRITE(FAN_PIN,1); else WRITE(FAN_PIN,0);
}
if (soft_pwm_fan < pwm_count) WRITE(FAN_PIN,0);
#endif /* defined(FAN_SOFT_PWM) */

pwm_count += (1 << SOFT_PWM_SCALE);
pwm_count &= 0x7f;

// increment slow_pwm_count only every 64 pwm_count circa 65.5ms
if ((pwm_count % 64) == 0) {
slow_pwm_count++;
slow_pwm_count &= 0x7f;

// Extruder 0
if (state_timer_heater_0 > 0) {
state_timer_heater_0--;
}

#if EXTRUDERS > 1
// Extruder 1
if (state_timer_heater_1 > 0)
state_timer_heater_1--;
#endif /* EXTRUDERS > 1 */

#if EXTRUDERS > 2
// Extruder 2
if (state_timer_heater_2 > 0)
state_timer_heater_2--;
#endif /* EXTRUDERS > 2 */
#if EXTRUDERS > 3
// Extruder 3
if (state_timer_heater_3 > 0)
state_timer_heater_3--;
#endif /* EXTRUDERS > 3 */
#if defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1
// Bed
if (state_timer_heater_b > 0)
state_timer_heater_b--;
#endif /* defined(HEATER_BED_PIN) && HEATER_BED_PIN > -1 */
} //if ((pwm_count % 64) == 0) {

#endif /* defined(FORK) */
#endif /* defined(FORK) || !defined(SLOW_PWM_HEATERS) */
#ifndef FORK

#define SET_ADMUX_ADCSRA(pin) ADMUX = BIT ( REFS0 ) | ( pin & 0x07 ) ; ADCSRA |= BIT ( ADSC )

#ifdef MUX5
#define START_ADC(pin) if ( pin > 7 ) ADCSRB = BIT ( MUX5 ) ; else ADCSRB = 0 ; SET_ADMUX_ADCSRA ( pin )

#else
#define START_ADC(pin) ADCSRB = 0 ; SET_ADMUX_ADCSRA ( pin )

#endif /* defined(MUX5) */
#else
temp_state = 1;
#endif /* !defined(FORK) */

#ifdef FORK
case 1: // Measure TEMP_0
#endif /* defined(FORK) */
switch(temp_state) {
#ifndef FORK
case PrepareTemp_0:
#if HAS_TEMP_0
START_ADC(TEMP_0_PIN);
#endif /* HAS_TEMP_0 */
#else
temp_state = 2;
#endif /* !defined(FORK) */
lcd_buttons_update();
#ifndef FORK
temp_state = MeasureTemp_0;
#endif /* !defined(FORK) */
#ifdef FORK
#if defined(TEMP_BED_PIN) && (TEMP_BED_PIN > -1)
#if TEMP_BED_PIN > 7
ADCSRB = 1<<MUX5;
#else
ADCSRB = 0;
#endif /* TEMP_BED_PIN > 7 */
ADMUX = ((1 << REFS0) | (TEMP_BED_PIN & 0x07));
ADCSRA |= 1<<ADSC; // Start conversion
#endif /* defined(TEMP_BED_PIN) && (TEMP_BED_PIN > -1) */
#endif /* defined(FORK) */
break;
#ifndef FORK
case MeasureTemp_0:
#else
temp_state = 3;
#endif /* !defined(FORK) */
#if HAS_TEMP_0
raw_temp_0_value += ADC;
#endif /* HAS_TEMP_0 */
#ifndef FORK
temp_state = PrepareTemp_BED;
#else
case 3: // Measure TEMP_BED
#endif /* !defined(FORK) */
break;
#ifndef FORK
case PrepareTemp_BED:
#if HAS_TEMP_BED
START_ADC(TEMP_BED_PIN);
#endif /* HAS_TEMP_BED */
#else
temp_state = 4;
#endif /* !defined(FORK) */
lcd_buttons_update();
#ifndef FORK
temp_state = MeasureTemp_BED;
#endif /* !defined(FORK) */
#ifdef FORK
#if defined(TEMP_1_PIN) && (TEMP_1_PIN > -1)
#if TEMP_1_PIN > 7
ADCSRB = 1<<MUX5;
#else
ADCSRB = 0;
#endif /* TEMP_1_PIN > 7 */
ADMUX = ((1 << REFS0) | (TEMP_1_PIN & 0x07));
ADCSRA |= 1<<ADSC; // Start conversion
#endif /* defined(TEMP_1_PIN) && (TEMP_1_PIN > -1) */
#endif /* defined(FORK) */
break;
#ifndef FORK
case MeasureTemp_BED:
#else
temp_state = 5;
#endif /* !defined(FORK) */
#if HAS_TEMP_BED
raw_temp_bed_value += ADC;
#endif /* HAS_TEMP_BED */
#ifndef FORK
temp_state = PrepareTemp_1;
#else
case 5: // Measure TEMP_1
#endif /* !defined(FORK) */
break;
#ifndef FORK
case PrepareTemp_1:
#if HAS_TEMP_1
START_ADC(TEMP_1_PIN);
#endif /* HAS_TEMP_1 */
#else
temp_state = 6;
#endif /* !defined(FORK) */
lcd_buttons_update();
#ifndef FORK
temp_state = MeasureTemp_1;
#endif /* !defined(FORK) */
#ifdef FORK
#if defined(TEMP_2_PIN) && (TEMP_2_PIN > -1)
#if TEMP_2_PIN > 7
ADCSRB = 1<<MUX5;
#else
ADCSRB = 0;
#endif /* TEMP_2_PIN > 7 */
ADMUX = ((1 << REFS0) | (TEMP_2_PIN & 0x07));
ADCSRA |= 1<<ADSC; // Start conversion
#endif /* defined(TEMP_2_PIN) && (TEMP_2_PIN > -1) */
#endif /* defined(FORK) */
break;
#ifndef FORK
case MeasureTemp_1:
#else
temp_state = 7;
#endif /* !defined(FORK) */
#if HAS_TEMP_1
raw_temp_1_value += ADC;
#endif /* HAS_TEMP_1 */
#ifndef FORK
temp_state = PrepareTemp_2;
#else
case 7: // Measure TEMP_2
#endif /* !defined(FORK) */
break;
#ifndef FORK
case PrepareTemp_2:
#if HAS_TEMP_2
START_ADC(TEMP_2_PIN);
#endif /* HAS_TEMP_2 */
#else
temp_state = 8;
#endif /* !defined(FORK) */
lcd_buttons_update();
#ifndef FORK
temp_state = MeasureTemp_2;
#endif /* !defined(FORK) */
#ifdef FORK
#if defined(TEMP_3_PIN) && (TEMP_3_PIN > -1)
#if TEMP_3_PIN > 7
ADCSRB = 1<<MUX5;
#else
ADCSRB = 0;
#endif /* TEMP_3_PIN > 7 */
ADMUX = ((1 << REFS0) | (TEMP_3_PIN & 0x07));
ADCSRA |= 1<<ADSC; // Start conversion
#endif /* defined(TEMP_3_PIN) && (TEMP_3_PIN > -1) */
#endif /* defined(FORK) */
break;
#ifndef FORK
case MeasureTemp_2:
#else
temp_state = 9;
#endif /* !defined(FORK) */
#if HAS_TEMP_2
raw_temp_2_value += ADC;
#endif /* HAS_TEMP_2 */
#ifndef FORK
temp_state = PrepareTemp_3;
#else
case 9: // Measure TEMP_3
#endif /* !defined(FORK) */
break;
#ifndef FORK
case PrepareTemp_3:
#if HAS_TEMP_3
START_ADC(TEMP_3_PIN);
#endif /* HAS_TEMP_3 */
#else
temp_state = 10; //change so that Filament Width is also measured
#endif /* !defined(FORK) */
lcd_buttons_update();
#ifndef FORK
temp_state = MeasureTemp_3;
#endif /* !defined(FORK) */
#if defined(FILWIDTH_PIN) &&(FILWIDTH_PIN > -1)
#ifdef FORK
//raw_filwidth_value += ADC;  //remove to use an IIR filter approach
if(ADC>102)  //check that ADC is reading a voltage > 0.5 volts, otherwise don't take in the data.
{
raw_filwidth_value= raw_filwidth_value-(raw_filwidth_value>>7);  //multipliy raw_filwidth_value by 127/128

raw_filwidth_value= raw_filwidth_value + ((unsigned long)ADC<<7);  //add new ADC reading
}
#endif /* defined(FORK) */
#endif /* defined(FILWIDTH_PIN) &&(FILWIDTH_PIN > -1) */
#ifdef FORK
case 10: //Prepare FILWIDTH
#if defined(FILWIDTH_PIN) && (FILWIDTH_PIN> -1)
#if FILWIDTH_PIN>7
ADCSRB = 1<<MUX5;
#else
ADCSRB = 0;
#endif /* FILWIDTH_PIN>7 */
ADMUX = ((1 << REFS0) | (FILWIDTH_PIN & 0x07));
ADCSRA |= 1<<ADSC; // Start conversion
#endif /* defined(FILWIDTH_PIN) && (FILWIDTH_PIN> -1) */
lcd_buttons_update();
temp_state = 11;
break;
case 11:   //Measure FILWIDTH
temp_state = 0;

temp_count++;
break;


case 12: //Startup, delay initial temp reading a tiny bit so the hardware can settle.
temp_state = 0;
#endif /* defined(FORK) */
break;
#ifndef FORK
case MeasureTemp_3:
#else
//    default:
//      SERIAL_ERROR_START;
//      SERIAL_ERRORLNPGM("Temp measurement error!");
//      break;
}
#endif /* !defined(FORK) */
#if HAS_TEMP_3
raw_temp_3_value += ADC;
#endif /* HAS_TEMP_3 */
#ifndef FORK
temp_state = Prepare_FILWIDTH;
#else
if(temp_count >= OVERSAMPLENR) // 10 * 16 * 1/(16000000/64/256)  = 164ms.
{
if (!temp_meas_ready) //Only update the raw values if they have been read. Else we could be updating them during reading.
{
#ifndef HEATER_0_USES_MAX6675
current_temperature_raw[0] = raw_temp_0_value;
#endif /* !defined(HEATER_0_USES_MAX6675) */
#if EXTRUDERS > 1
current_temperature_raw[1] = raw_temp_1_value;
#endif /* EXTRUDERS > 1 */
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
redundant_temperature_raw = raw_temp_1_value;
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */
#if EXTRUDERS > 2
current_temperature_raw[2] = raw_temp_2_value;
#endif /* EXTRUDERS > 2 */
#if EXTRUDERS > 3
current_temperature_raw[3] = raw_temp_3_value;
#endif /* EXTRUDERS > 3 */
#endif /* !defined(FORK) */
break;
#ifndef FORK
case Prepare_FILWIDTH:
#if HAS_FILAMENT_SENSOR
START_ADC(FILWIDTH_PIN);
#endif /* HAS_FILAMENT_SENSOR */
lcd_buttons_update();
temp_state = Measure_FILWIDTH;
#else
}
#endif /* !defined(FORK) */
break;
#ifndef FORK
case Measure_FILWIDTH:
#if HAS_FILAMENT_SENSOR
// raw_filwidth_value += ADC;  //remove to use an IIR filter approach
if (ADC > 102) { //check that ADC is reading a voltage > 0.5 volts, otherwise don't take in the data.
raw_filwidth_value -= (raw_filwidth_value>>7);  //multiply raw_filwidth_value by 127/128
raw_filwidth_value += ((unsigned long)ADC<<7);  //add new ADC reading
}
#endif /* HAS_FILAMENT_SENSOR */
temp_state = PrepareTemp_0;
temp_count++;
break;
case StartupDelay:
temp_state = PrepareTemp_0;
break;
#else
//Add similar code for Filament Sensor - can be read any time since IIR filtering is used
#if defined(FILWIDTH_PIN) &&(FILWIDTH_PIN > -1)
current_raw_filwidth = raw_filwidth_value>>10;  //need to divide to get to 0-16384 range since we used 1/128 IIR filter approach
#endif /* defined(FILWIDTH_PIN) &&(FILWIDTH_PIN > -1) */

#endif /* !defined(FORK) */

#ifndef FORK
// default:
//   SERIAL_ERROR_START;
//   SERIAL_ERRORLNPGM("Temp measurement error!");
//   break;
} // switch(temp_state)

if (temp_count >= OVERSAMPLENR) { // 10 * 16 * 1/(16000000/64/256)  = 164ms.
if (!temp_meas_ready) { //Only update the raw values if they have been read. Else we could be updating them during reading.
#ifndef HEATER_0_USES_MAX6675
current_temperature_raw[0] = raw_temp_0_value;
#endif /* !defined(HEATER_0_USES_MAX6675) */
#if EXTRUDERS > 1
current_temperature_raw[1] = raw_temp_1_value;
#if EXTRUDERS > 2
current_temperature_raw[2] = raw_temp_2_value;
#if EXTRUDERS > 3
current_temperature_raw[3] = raw_temp_3_value;
#endif /* EXTRUDERS > 3 */
#endif /* EXTRUDERS > 2 */
#endif /* EXTRUDERS > 1 */
#ifdef TEMP_SENSOR_1_AS_REDUNDANT
redundant_temperature_raw = raw_temp_1_value;
#endif /* defined(TEMP_SENSOR_1_AS_REDUNDANT) */
#endif /* !defined(FORK) */
current_temperature_bed_raw = raw_temp_bed_value;
#ifndef FORK
} //!temp_meas_ready
#endif /* !defined(FORK) */

#ifndef FORK
// Filament Sensor - can be read any time since IIR filtering is used
#if HAS_FILAMENT_SENSOR
current_raw_filwidth = raw_filwidth_value >> 10;  // Divide to get to 0-16384 range since we used 1/128 IIR filter approach
#endif /* HAS_FILAMENT_SENSOR */
#endif /* !defined(FORK) */

temp_meas_ready = true;
temp_count = 0;
raw_temp_0_value = 0;
raw_temp_1_value = 0;
raw_temp_2_value = 0;
#ifdef FORK
#if HEATER_0_RAW_LO_TEMP > HEATER_0_RAW_HI_TEMP
if(current_temperature_raw[0] <= maxttemp_raw[0]) {
#else
if(current_temperature_raw[0] >= maxttemp_raw[0]) {
#endif /* HEATER_0_RAW_LO_TEMP > HEATER_0_RAW_HI_TEMP */
#ifndef HEATER_0_USES_MAX6675
max_temp_error(0);
#endif /* !defined(HEATER_0_USES_MAX6675) */
#endif /* defined(FORK) */
raw_temp_3_value = 0;
#ifdef FORK
#if HEATER_0_RAW_LO_TEMP > HEATER_0_RAW_HI_TEMP
if(current_temperature_raw[0] >= minttemp_raw[0]) {
#else
if(current_temperature_raw[0] <= minttemp_raw[0]) {
#endif /* HEATER_0_RAW_LO_TEMP > HEATER_0_RAW_HI_TEMP */
#ifndef HEATER_0_USES_MAX6675
min_temp_error(0);
#endif /* !defined(HEATER_0_USES_MAX6675) */
#endif /* defined(FORK) */
raw_temp_bed_value = 0;
#ifdef FORK


#if EXTRUDERS > 1
#if HEATER_1_RAW_LO_TEMP > HEATER_1_RAW_HI_TEMP
if(current_temperature_raw[1] <= maxttemp_raw[1]) {
#else
if(current_temperature_raw[1] >= maxttemp_raw[1]) {
#endif /* HEATER_1_RAW_LO_TEMP > HEATER_1_RAW_HI_TEMP */
max_temp_error(1);
}
#if HEATER_1_RAW_LO_TEMP > HEATER_1_RAW_HI_TEMP
if(current_temperature_raw[1] >= minttemp_raw[1]) {
#else
if(current_temperature_raw[1] <= minttemp_raw[1]) {
#endif /* HEATER_1_RAW_LO_TEMP > HEATER_1_RAW_HI_TEMP */
min_temp_error(1);
}
#endif /* EXTRUDERS > 1 */
#if EXTRUDERS > 2
#if HEATER_2_RAW_LO_TEMP > HEATER_2_RAW_HI_TEMP
if(current_temperature_raw[2] <= maxttemp_raw[2]) {
#else
if(current_temperature_raw[2] >= maxttemp_raw[2]) {
#endif /* HEATER_2_RAW_LO_TEMP > HEATER_2_RAW_HI_TEMP */
max_temp_error(2);
}
#if HEATER_2_RAW_LO_TEMP > HEATER_2_RAW_HI_TEMP
if(current_temperature_raw[2] >= minttemp_raw[2]) {
#else
if(current_temperature_raw[2] <= minttemp_raw[2]) {
#endif /* HEATER_2_RAW_LO_TEMP > HEATER_2_RAW_HI_TEMP */
min_temp_error(2);
}
#endif /* EXTRUDERS > 2 */
#if EXTRUDERS > 3
#if HEATER_3_RAW_LO_TEMP > HEATER_3_RAW_HI_TEMP
if(current_temperature_raw[3] <= maxttemp_raw[3]) {
#else
if(current_temperature_raw[3] >= maxttemp_raw[3]) {
#endif /* HEATER_3_RAW_LO_TEMP > HEATER_3_RAW_HI_TEMP */
max_temp_error(3);
}
#if HEATER_3_RAW_LO_TEMP > HEATER_3_RAW_HI_TEMP
if(current_temperature_raw[3] >= minttemp_raw[3]) {
#else
if(current_temperature_raw[3] <= minttemp_raw[3]) {
#endif /* HEATER_3_RAW_LO_TEMP > HEATER_3_RAW_HI_TEMP */
min_temp_error(3);
}
#endif /* EXTRUDERS > 3 */


/* No bed MINTEMP error? */
#if defined(BED_MAXTEMP) && (TEMP_SENSOR_BED != 0)
#if HEATER_BED_RAW_LO_TEMP > HEATER_BED_RAW_HI_TEMP
if(current_temperature_bed_raw <= bed_maxttemp_raw) {
#else
if(current_temperature_bed_raw >= bed_maxttemp_raw) {
#endif /* HEATER_BED_RAW_LO_TEMP > HEATER_BED_RAW_HI_TEMP */
target_temperature_bed = 0;
bed_max_temp_error();
}
#endif /* defined(BED_MAXTEMP) && (TEMP_SENSOR_BED != 0) */
}

#ifdef BABYSTEPPING
for(uint8_t axis=0;axis<3;axis++)
{
int curTodo=babystepsTodo[axis]; //get rid of volatile for performance

if(curTodo>0)
{
babystep(axis,/*fwd*/true);
babystepsTodo[axis]--; //less to do next time
}
else
if(curTodo<0)
{
babystep(axis,/*fwd*/false);
babystepsTodo[axis]++; //less to do next time
}
}
#endif /* defined(BABYSTEPPING) */
#endif /* defined(FORK) */

#ifndef FORK
#if HEATER_0_RAW_LO_TEMP > HEATER_0_RAW_HI_TEMP
#define MAXTEST <=

#define MINTEST >=

#else
#define MAXTEST >=

#define MINTEST <=

#endif /* HEATER_0_RAW_LO_TEMP > HEATER_0_RAW_HI_TEMP */

for (int i=0; i<EXTRUDERS; i++) {
if (current_temperature_raw[i] MAXTEST maxttemp_raw[i]) max_temp_error(i);
else if (current_temperature_raw[i] MINTEST minttemp_raw[i]) min_temp_error(i);
#endif /* !defined(FORK) */
}
#ifndef FORK
/* No bed MINTEMP error? */
#else
#ifdef PIDTEMP
// Apply the scale factors to the PID values


float scalePID_i(float i)
{
return i*PID_dT;
}

float unscalePID_i(float i)
{
return i/PID_dT;
}

float scalePID_d(float d)
{
return d/PID_dT;
}

float unscalePID_d(float d)
{
return d*PID_dT;
}

#endif /* defined(PIDTEMP) */
#endif /* !defined(FORK) */
#if defined(FORK) || defined(BED_MAXTEMP) && (TEMP_SENSOR_BED != 0)
#ifndef FORK
if (current_temperature_bed_raw MAXTEST bed_maxttemp_raw) {
target_temperature_bed = 0;
bed_max_temp_error();
}
#endif /* !defined(FORK) */
#endif /* defined(FORK) || defined(BED_MAXTEMP) && (TEMP_SENSOR_BED != 0) */
#ifndef FORK
} // temp_count >= OVERSAMPLENR

#endif /* !defined(FORK) */
#if defined(FORK) || defined(BABYSTEPPING)
#ifndef FORK
for (uint8_t axis=X_AXIS; axis<=Z_AXIS; axis++) {
int curTodo=babystepsTodo[axis]; //get rid of volatile for performance

if (curTodo > 0) {
babystep(axis,/*fwd*/true);
babystepsTodo[axis]--; //less to do next time
}
else if(curTodo < 0) {
babystep(axis,/*fwd*/false);
babystepsTodo[axis]++; //less to do next time
}
#endif /* !defined(FORK) */
}
#endif /* defined(FORK) || defined(BABYSTEPPING) */
}

#ifndef FORK
#ifdef PIDTEMP
// Apply the scale factors to the PID values
float scalePID_i(float i)   { return i * PID_dT; }
float unscalePID_i(float i) { return i / PID_dT; }
float scalePID_d(float d)   { return d / PID_dT; }
float unscalePID_d(float d) { return d * PID_dT; }
#endif /* defined(PIDTEMP) */
#endif /* !defined(FORK) */
