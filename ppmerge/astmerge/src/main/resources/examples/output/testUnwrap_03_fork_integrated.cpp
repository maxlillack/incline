int a = 0;
int b = 1;
#ifdef FORK
int add1 = 11;
#endif /* defined(FORK) */
#if defined(FORK) || defined(FOO)
int c = 2;
int d = 3;
#endif /* defined(FORK) || defined(FOO) */
#ifdef FORK
int add2 = 22;
#endif /* defined(FORK) */
int e = 4;
int f = 5;
