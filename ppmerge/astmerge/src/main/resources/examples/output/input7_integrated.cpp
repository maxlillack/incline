#if EXTRUDERS > 1
#ifndef FORK
retracted_swap[active_extruder]=(code_seen('S') && code_value_long() == 1); // checks for swap retract argument
retract(true,retracted_swap[active_extruder]);
#endif /* !defined(FORK) */
#else
retract(true);
#endif /* EXTRUDERS > 1 */
break;
case 11: // G11 retract_recover
#if EXTRUDERS > 1
#ifndef FORK
retract(false,retracted_swap[active_extruder]);
#endif /* !defined(FORK) */
#else
retract(false);
#endif /* EXTRUDERS > 1 */
