#include "temperature.h"
#include "ultralcd.h"
#ifdef ULTRA_LCD





/** forward declerations **/

void copy_and_scalePID_i();
void copy_and_scalePID_d();

/* Different menus */
static void lcd_status_screen();
#ifdef ULTIPANEL




#define ENCODER_FEEDRATE_DEADZONE 10


#ifndef LCD_I2C_VIKI
#define ENCODER_STEPS_PER_MENU_ITEM 5

#else
#define ENCODER_STEPS_PER_MENU_ITEM 2

#endif /* !defined(LCD_I2C_VIKI) */




uint8_t currentMenuViewOffset;              /* scroll offset in the current menu */
uint32_t blocking_enc;
uint8_t lastEncoderBits;
int8_t encoderDiff; /* encoderDiff is updated from interrupt context and added to encoderPosition every LCD update */
uint32_t encoderPosition;
#if SDCARDDETECT > 0
bool lcd_oldcardstatus;
#endif /* SDCARDDETECT > 0 */
#endif /* defined(ULTIPANEL) */




/* Main status screen. It's up to the implementation specific part to show what is needed. As this is very display dependend */
static void lcd_status_screen()
{

#if (defined(FORK) || (defined(ULTIPANEL))) && (!defined(FORK) || (defined(ULTIPANEL) || defined(BASIC_ENCODER)))
if (LCD_CLICKED)
{
currentMenu = lcd_main_menu;
encoderPosition = 0;
lcd_quick_feedback();
}

// Dead zone at 100% feedrate
if (feedmultiply < 100 && (feedmultiply + int(encoderPosition)) > 100 ||
feedmultiply > 100 && (feedmultiply + int(encoderPosition)) < 100)
{
encoderPosition = 0;
feedmultiply = 100;
}

if (feedmultiply == 100 && int(encoderPosition) > ENCODER_FEEDRATE_DEADZONE)
{
feedmultiply += int(encoderPosition) - ENCODER_FEEDRATE_DEADZONE;
encoderPosition = 0;
}
else if (feedmultiply == 100 && int(encoderPosition) < -ENCODER_FEEDRATE_DEADZONE)
{
feedmultiply += int(encoderPosition) + ENCODER_FEEDRATE_DEADZONE;
encoderPosition = 0;
}
else if (feedmultiply != 100)
{
feedmultiply += int(encoderPosition);
encoderPosition = 0;
}

if (feedmultiply < 10)
feedmultiply = 10;
if (feedmultiply > 999)
feedmultiply = 999;
#endif /* (defined(FORK) || (defined(ULTIPANEL))) && (!defined(FORK) || (defined(ULTIPANEL) || defined(BASIC_ENCODER))) */
}

#if (defined(FORK) || (defined(ULTIPANEL))) && (!defined(FORK) || (ULTIPANEL))
#ifdef FORK


#endif /* defined(FORK) */





/* Menu implementation */
static void lcd_main_menu()
{
START_MENU();
MENU_ITEM(back, MSG_WATCH, lcd_status_screen);
#ifndef FORK
if (movesplanned() || IS_SD_PRINTING)
#else
if (movesplanned()>3 || IS_SD_PRINTING)
#endif /* !defined(FORK) */
{
MENU_ITEM(submenu, MSG_TUNE, lcd_tune_menu);
}else{
MENU_ITEM(submenu, MSG_PREPARE, lcd_prepare_menu);
}
MENU_ITEM(submenu, MSG_CONTROL, lcd_control_menu);
#ifdef FORK
if (!(movesplanned()>3 || IS_SD_PRINTING))
{
MENU_ITEM(submenu, MSG_CALIBRATE, lcd_calibrate_menu);
}
#endif /* defined(FORK) */
#ifdef SDSUPPORT
if (card.cardOK)
{
if (card.isFileOpen())
{
if (card.sdprinting)
MENU_ITEM(function, MSG_PAUSE_PRINT, lcd_sdcard_pause);
else
MENU_ITEM(function, MSG_RESUME_PRINT, lcd_sdcard_resume);
MENU_ITEM(function, MSG_STOP_PRINT, lcd_sdcard_stop);
}else{
MENU_ITEM(submenu, MSG_CARD_MENU, lcd_sdcard_menu);
#if SDCARDDETECT < 1
MENU_ITEM(gcode, MSG_CNG_SDCARD, PSTR("M21"));  // SD-card changed by user
#endif /* SDCARDDETECT < 1 */
}
}else{
MENU_ITEM(submenu, MSG_NO_CARD, lcd_sdcard_menu);
#if SDCARDDETECT < 1
MENU_ITEM(gcode, MSG_INIT_SDCARD, PSTR("M21")); // Manually initialize the SD-card via user interface
#endif /* SDCARDDETECT < 1 */
}
#endif /* defined(SDSUPPORT) */
#ifdef FORK
#ifdef LCD_MOVE_BED_DOWN
if (!(movesplanned()>3 || IS_SD_PRINTING)) {
MENU_ITEM(function, MSG_MOVE_BED_DOWN, lcd_move_bed_down);
}
#endif /* defined(LCD_MOVE_BED_DOWN) */
#endif /* defined(FORK) */
END_MENU();
}


#ifdef FORK

#ifdef TANTILLUS
static void lcd_calibrate_menu() {
START_MENU();
MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
MENU_ITEM(submenu, MSG_CALIBRATE_EXTRUDER, lcd_calibrate_extruder_menu);
END_MENU();
}

static void lcd_extrude(float length, float feedrate) {
current_position[E_AXIS] += length;
plan_buffer_line(current_position[X_AXIS], current_position[Y_AXIS], current_position[Z_AXIS], current_position[E_AXIS], feedrate, active_extruder);
}

static void lcd_load_halfway() {
allow_cold_extrude_once = true;
allow_lengthy_extrude_once = true;
lcd_extrude(BOWDEN_LENGTH/2, EASY_LOAD_FEEDRATE/60);
}

static void lcd_unload_halfway() {
allow_lengthy_extrude_once = true;
lcd_extrude(-BOWDEN_LENGTH/2, EASY_UNLOAD_FEEDRATE/60);
}

static void lcd_calibrate_extrude_100mm() {
current_position[E_AXIS] = 0;
plan_set_e_position(current_position[E_AXIS]);
allow_cold_extrude_once = true;
lcd_extrude(100, LCD_EXT_CAL_FEEDRATE/60);
}

static void lcd_calibrate_retract_100mm() {
current_position[E_AXIS] = 0;
plan_set_e_position(current_position[E_AXIS]);
allow_cold_extrude_once = true;
lcd_extrude(-100, LCD_EXT_CAL_FEEDRATE/60);
}

static void lcd_calibrate_extruder_menu() {
START_MENU();
MENU_ITEM(back, MSG_CALIBRATE, lcd_calibrate_menu);
MENU_ITEM_EDIT(float51, MSG_ESTEPS, &axis_steps_per_unit[E_AXIS], 5, 9999);
MENU_ITEM(function, MSG_E_100MM, lcd_calibrate_extrude_100mm);
MENU_ITEM(function, MSG_R_100MM, lcd_calibrate_retract_100mm);
MENU_ITEM(function, MSG_E_HALF_BOWDEN_LENGTH, lcd_load_halfway);
MENU_ITEM(function, MSG_R_HALF_BOWDEN_LENGTH, lcd_unload_halfway);
END_MENU();
}
#endif /* defined(TANTILLUS) */

#ifdef LCD_PURGE_RETRACT
static void lcd_purge()
{
lcd_extrude(LCD_PURGE_LENGTH, LCD_PURGE_FEEDRATE/60);
}

static void lcd_retract()
{
lcd_extrude(-LCD_RETRACT_LENGTH, LCD_RETRACT_FEEDRATE/60);
}
#endif /* defined(LCD_PURGE_RETRACT) */

static void lcd_auto_home()
{
#ifdef LCD_PREVENT_COLD_HOME
if (degHotend(active_extruder) < LCD_MIN_HOME_TEMP) {
lcd_return_to_status();
return;
}
#endif /* defined(LCD_PREVENT_COLD_HOME) */
enquecommand_P((PSTR("G28")));
lcd_return_to_status();
}

#ifdef LCD_EASY_LOAD

static void lcd_easy_load()
{
if (target_temperature[0]<EXTRUDE_MINTEMP) {  // preheat to PLA temp if heater is not on
setTargetHotend0(plaPreheatHotendTemp);
fanSpeed = plaPreheatFanSpeed;
setWatch();
}

allow_cold_extrude_once = true;
allow_lengthy_extrude_once = true;
lcd_extrude(BOWDEN_LENGTH, EASY_LOAD_FEEDRATE/60);

lcd_return_to_status();
}

static void lcd_easy_unload()
{
allow_lengthy_extrude_once = true;
lcd_extrude(-BOWDEN_LENGTH, EASY_UNLOAD_FEEDRATE/60);
lcd_return_to_status();
}

static void lcd_easy_load_menu()
{
START_MENU();
MENU_ITEM(back, MSG_PREPARE, lcd_prepare_menu);
MENU_ITEM(function, MSG_E_BOWDEN_LENGTH, lcd_easy_load);
END_MENU();
}

static void lcd_easy_unload_menu()
{
START_MENU();
MENU_ITEM(back, MSG_PREPARE, lcd_prepare_menu);
MENU_ITEM(function, MSG_R_BOWDEN_LENGTH, lcd_easy_unload);
END_MENU();
}
#endif /* defined(LCD_EASY_LOAD) */
#endif /* defined(FORK) */

static void lcd_tune_menu()
{
START_MENU();
MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
MENU_ITEM_EDIT(int3, MSG_SPEED, &feedmultiply, 10, 999);
MENU_ITEM_EDIT(int3, MSG_NOZZLE, &target_temperature[0], 0, HEATER_0_MAXTEMP - 15);
#if TEMP_SENSOR_1 != 0
MENU_ITEM_EDIT(int3, MSG_NOZZLE1, &target_temperature[1], 0, HEATER_1_MAXTEMP - 15);
#endif /* TEMP_SENSOR_1 != 0 */
#if TEMP_SENSOR_2 != 0
MENU_ITEM_EDIT(int3, MSG_NOZZLE2, &target_temperature[2], 0, HEATER_2_MAXTEMP - 15);
#endif /* TEMP_SENSOR_2 != 0 */
#if TEMP_SENSOR_BED != 0
MENU_ITEM_EDIT(int3, MSG_BED, &target_temperature_bed, 0, BED_MAXTEMP - 15);
#endif /* TEMP_SENSOR_BED != 0 */
MENU_ITEM_EDIT(int3, MSG_FAN_SPEED, &fanSpeed, 0, 255);
MENU_ITEM_EDIT(int3, MSG_FLOW, &extrudemultiply, 10, 999);
#ifdef FILAMENTCHANGEENABLE
MENU_ITEM(gcode, MSG_FILAMENTCHANGE, PSTR("M600"));
#endif /* defined(FILAMENTCHANGEENABLE) */
END_MENU();
}

static void lcd_prepare_menu()
{
START_MENU();
MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
#ifdef SDSUPPORT
//MENU_ITEM(function, MSG_AUTOSTART, lcd_autostart_sd);
#endif /* defined(SDSUPPORT) */
MENU_ITEM(gcode, MSG_DISABLE_STEPPERS, PSTR("M84"));
#ifndef FORK
MENU_ITEM(gcode, MSG_AUTO_HOME, PSTR("G28"));
//MENU_ITEM(gcode, MSG_SET_ORIGIN, PSTR("G92 X0 Y0 Z0"));
#else
#ifdef LCD_PREVENT_COLD_HOME
if (degHotend(active_extruder) < LCD_MIN_HOME_TEMP) {
MENU_ITEM(function, MSG_AUTO_HOME_DISABLED, lcd_auto_home);
} else {
MENU_ITEM(function, MSG_AUTO_HOME, lcd_auto_home);
}
#else
MENU_ITEM(function, MSG_AUTO_HOME, lcd_auto_home);
#endif /* defined(LCD_PREVENT_COLD_HOME) */
#endif /* !defined(FORK) */
#if !defined(FORK) || (!defined(NO_PREHEAT_PLA_MENUITEM))
MENU_ITEM(function, MSG_PREHEAT_PLA, lcd_preheat_pla);
#endif /* !defined(FORK) || (!defined(NO_PREHEAT_PLA_MENUITEM)) */
#if !defined(FORK) || (!defined(NO_PREHEAT_ABS_MENUITEM))
MENU_ITEM(function, MSG_PREHEAT_ABS, lcd_preheat_abs);
#endif /* !defined(FORK) || (!defined(NO_PREHEAT_ABS_MENUITEM)) */
#ifdef FORK
#ifdef LCD_EASY_LOAD
MENU_ITEM(submenu, MSG_EASY_LOAD, lcd_easy_load_menu);
MENU_ITEM(submenu, MSG_EASY_UNLOAD, lcd_easy_unload_menu);
#endif /* defined(LCD_EASY_LOAD) */
#ifdef LCD_PURGE_RETRACT
MENU_ITEM(function, MSG_PURGE_XMM, lcd_purge);
MENU_ITEM(function, MSG_RETRACT_XMM, lcd_retract);
#endif /* defined(LCD_PURGE_RETRACT) */
#endif /* defined(FORK) */
MENU_ITEM(function, MSG_COOLDOWN, lcd_cooldown);
MENU_ITEM(submenu, MSG_MOVE_AXIS, lcd_move_menu);
END_MENU();
}
#ifndef FORK

#endif /* !defined(FORK) */


static void lcd_control_menu()
{
START_MENU();
MENU_ITEM(back, MSG_MAIN, lcd_main_menu);
MENU_ITEM(submenu, MSG_TEMPERATURE, lcd_control_temperature_menu);
MENU_ITEM(submenu, MSG_MOTION, lcd_control_motion_menu);
#ifdef FWRETRACT
MENU_ITEM(submenu, MSG_RETRACT, lcd_control_retract_menu);
#endif /* defined(FWRETRACT) */
#ifdef EEPROM_SETTINGS
MENU_ITEM(function, MSG_STORE_EPROM, Config_StoreSettings);
MENU_ITEM(function, MSG_LOAD_EPROM, Config_RetrieveSettings);
#endif /* defined(EEPROM_SETTINGS) */
MENU_ITEM(function, MSG_RESTORE_FAILSAFE, Config_ResetDefault);
END_MENU();
}


#ifndef FORK

#endif /* !defined(FORK) */
#endif /* (defined(FORK) || (defined(ULTIPANEL))) && (!defined(FORK) || (ULTIPANEL)) */
#ifdef FORK

#endif /* defined(FORK) */










#endif /* defined(ULTRA_LCD) */
