/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge;

import de.uni_leipzig.iwi.CPPClient.CPPClient;
import de.uni_leipzig.iwi.CPPClient.ResultModel;
import de.uni_leipzig.iwi.CPPClient.XMLConverter;
import it.unibo.cs.ndiff.common.vdom.DOMDocument;
import it.unibo.cs.ndiff.core.Nconfig;
import it.unibo.cs.ndiff.core.Ndiff;
import it.unibo.cs.ndiff.exceptions.ComputePhaseException;
import it.unibo.cs.ndiff.exceptions.InputFileException;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;


public class PPMerge {
    private boolean debugPrintXML = false;

    private File baseline;
    private File variant;
    private File outputFile;

    public PPMerge(File baseline, File variant, File outputFile) {
        this.baseline = baseline;
        this.variant = variant;
        this.outputFile = outputFile;
        de.uni_leipzig.iwi.ppmerge.FileUtils.assertExists(baseline);
        de.uni_leipzig.iwi.ppmerge.FileUtils.assertExists(variant);
    }

    public String serializeIntegratedSource(String name, boolean skipXMLASTGeneration) throws ComputePhaseException, InputFileException, TransformerException, SAXException, XPathExpressionException, ParserConfigurationException, IOException {
        String integratedSource = createIntegratedSource(name, skipXMLASTGeneration);

        FileUtils.writeStringToFile(outputFile, integratedSource, Charsets.UTF_8);
        printProgressBox("Integrated AST written to: " + outputFile.getAbsolutePath());
        return integratedSource;
    }

    public String createIntegratedSource(String name, boolean skipXMLASTGeneration) throws TransformerException, ParserConfigurationException, XPathExpressionException, SAXException, IOException, ComputePhaseException, InputFileException {
        String prefix_base = "";
        String prefix_remote = "";
        if(baseline.getName().equals(variant.getName()))
        {
            prefix_base = "base";
            prefix_remote = "remote";
        }

        String tempDir = System.getProperty("java.io.tmpdir");

        File baselineXML = new File(tempDir, prefix_base + FilenameUtils.getBaseName(baseline.getName()) + ".xml");
        File variantXML = new File(tempDir, prefix_remote + FilenameUtils.getBaseName(variant.getName()) + ".xml");
        XMLConverter xmlConverter = new XMLConverter();

        // 1. Parse files
        if(!skipXMLASTGeneration)
        {
            CPPClient cppClient = new CPPClient();
            ResultModel baselineResultModel = cppClient.start(baseline);
            ResultModel variantResultModel = cppClient.start(variant);

            // 2. Create intermediate xml format
            xmlConverter.createXML(name, baselineResultModel, baselineXML);
            xmlConverter.createXML(name, variantResultModel, variantXML);
        } else {
            de.uni_leipzig.iwi.ppmerge.FileUtils.assertExists(baselineXML);
            de.uni_leipzig.iwi.ppmerge.FileUtils.assertExists(variantXML);
        }

        System.out.println("Diffing documents...");

        // 3. XML diff
        Nconfig nconfig = getNConfig();

        // This will invoke the diffing of the two documents with respect to the documentation
        DOMDocument result = Ndiff.getDOMDocument(baselineXML.toURI().toString(), variantXML.toURI().toString(), nconfig);
        System.out.println("Completed AST model differencing.");
        if (debugPrintXML) {
            result.writeToStream(System.out);
            System.out.println("");
        }

        System.out.println("Starting to merge ASTs...");
        // 4. XML merge
        DOMDocument merged = DocumentMerger.getMergedDocument(baselineXML.toURI().toString(), result);

        System.out.println("Finished merging ASTs.");
        if (debugPrintXML) {
            merged.writeToStream(System.out);
            System.out.println("");
        }

        // 5. Simplify
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");

        XSLTMergeSimplifier xsltMergeSimplifier = new XSLTMergeSimplifier();
        String simplifiedSourceCode = xsltMergeSimplifier.runXSLTSimplification(xmlConverter, merged, variant);

        System.out.println("Simplified integrated AST.");

        return simplifiedSourceCode;
    }

    private static void printProgressBox(String message) {
        System.out.println("------------------------------------------------------------------------");
        System.out.println(message);
        System.out.println("------------------------------------------------------------------------");
    }

    public static Nconfig getNConfig() {
        Nconfig nconfig = new Nconfig();

		/*
		 * JNDiff user documentation (italian): http://jndiff.sourceforge.net/docUser.pdf
		 */
        nconfig.addPhaseParam(0, "ltrim", "false");
        nconfig.addPhaseParam(0, "rtrim", "false");
        nconfig.addPhaseParam(0, "collapse", "false");
        nconfig.addPhaseParam(0, "emptynode", "false");
        nconfig.addPhaseParam(0, "commentnode", "true");
        // Update means "changes within an element" the current level of 100
        // basically prevents the detection of any update.
        nconfig.addPhaseParam(Nconfig.FindUpdate, "level", "100");
        // Where to search for potential moves
        nconfig.addPhaseParam(Nconfig.FindMove, "range", "30");
        nconfig.addPhaseParam(Nconfig.FindMove, "minweight", "10");

		/*
		 * Translated:
		 * Sets the level of similarity that must evere twin
		 * nodes in order to be considered equal.
		 * The similarity of two individual nodes is based on attributes and content of the fragments
		 * recognized in the subtree.
		 *
		 */
        nconfig.addPhaseParam(Nconfig.Propagation, "attsimilarity", "100");
		/* Translated:
		If enabled, when the comparison is given more weight
		structure of the document to its content. */
        nconfig.addPhaseParam(Nconfig.Propagation, "forcematch", "true");
        return nconfig;
    }
}
