/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.Vector;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.unibo.cs.ndiff.common.vdom.reconstruction.Rnode;
import it.unibo.cs.ndiff.common.vdom.reconstruction.Rtree;
import it.unibo.cs.ndiff.exceptions.InputFileException;

/*
 * This class is used to construct the "merged" file
 * The normal implementation will just add attributes to indicate inserted/deleted nodes
 * Instead, we create new #ifdef
 * 
 */
public class MyRTree extends Rtree {
	private int nodeDeleted = 0;
	private Integer counter;
	
	public MyRTree(String paramString, boolean ltrim, boolean rtrim,
			boolean collapse, boolean emptynode, boolean commentNode)
			throws InputFileException {
		super(paramString, ltrim, rtrim, collapse, emptynode, commentNode);

	}
	
	private void adoptChild(Integer fnode, Integer node, Integer children) {

		// logger.info("Search Adopt Child fnode:"+fnode+" node:"+node+" children:"+children);
		System.out.println("Search Adopt Child fnode:"+fnode+" node:"+node+" children:"+children);
		if (getNode(fnode).isNew == 1)
		{
			// Was using getNode(fnode).getPosFather()
			
			// Search father
			int posFather = -1;
			Node domFather = getNode(fnode).getRefDomNode().getParentNode();
			for(int i = 0; i < nodeList.size(); i++)
			{
				if(getNode(i).getRefDomNode() == domFather)
				{
					posFather = i;
				}
			}
			if(posFather == -1)
			{
				throw new IllegalStateException("Father not found.");
			}
			
//			if(getNode(fnode).refDomNode.getNextSibling() == null)
//			{
//				throw new IllegalStateException("Father has no siblings.");
//			}
			
			adoptChild(posFather, fnode, children);
		}
		
		// logger.info("Adopt Child fnode:"+fnode+" node:"+node+" children:"+children);

		
		Node child = getNode(node).refDomNode;
		Node nextSibling = child.getNextSibling();
		int i = 0;
		while (i < children) {
			// TODO skipped cloned -- needs generalization
			if(nextSibling.getNodeType() != Node.ELEMENT_NODE || !(((Element)nextSibling).hasAttribute("from") && ((Element)nextSibling).getAttribute("from").contains("cloned")))
			{
				child.appendChild(nextSibling);
				if (!isMarkupHowDelete(nextSibling))
				{
					i++;
				}
				nextSibling = child.getNextSibling();
			} else {
				nextSibling = nextSibling.getNextSibling();
			}

		}
	}
	
	@Override
	public void WRAP(Integer nodenumber, Integer at, Integer pos,
			Integer children, String nodename, Node content) {
		
	    System.out.println(" WRAP nn:" + nodenumber + " at: " + at + " pos: " + pos + " children: " + children + " nodename " + nodename);
		// TODO Auto-generated method stub
//		super.WRAP(nodenumber, at, pos, children, nodename, content);
		
	    
	    String dump = dumpXML(DOM, nodeList);
	    
		Node father = getNode(at).refDomNode;
		
		if(father == null)
		{
			throw new IllegalStateException("Could not found node at " + at);
		}
		
		 // Fix pos w.r.t. extracted
	    NodeList localNodeList = father.getChildNodes();
	    int count = localNodeList.getLength();
	    int offset = 0;
	    for (int i = 0; i < Math.min(pos, count); i++) {
	    	if(localNodeList.item(i).getNodeType() == Node.ELEMENT_NODE)
	    	{
	    		Element element = (Element) localNodeList.item(i);
	    		if(element.hasAttribute("isExtracted")) {
	    			offset += getExtractedChildren(localNodeList.item(i));
	    		}
	    	}
	    }
	    // Offset for "not actually extracted" and "- 1" for the #if in its place
	    if(offset != 0) {
	    	pos -= offset - 1;
	    }
		
		Node refNode = findChildPosition(father, pos);

		if(refNode == null)
		{
			Node toIns = DOM.importNode(content, false);
			
			if(father.getNodeType() != Node.ELEMENT_NODE)
			{
				throw new IllegalStateException("Can only insert into ELEMENT node.");
			}
			
			if(nodename.equals("If"))
			{
				Element ifElement = (Element) toIns;
				ifElement.setAttribute("condition", "defined(FORK) && " + ifElement.getAttribute("condition"));
			}
			
			if(father.getNodeName().equals("text"))
			{
				throw new IllegalStateException("Must not add children to <text>");
			}
			
			father.appendChild(toIns);

			addNodeList(nodenumber, at, toIns);
			
			if (children > 0)
			{
				adoptChild(at, nodenumber, children);
			}
			
		} else if(nodename.equals("If") && content.getNodeName().equals("If") && refNode.getNodeName().equals("If"))
	    {
	    	Element localElement = (Element) refNode;
	    	Element contentElemnet = (Element) content;
	    	
	    	if(localElement.hasAttribute("isExtracted"))
	    	{
		    	//re-wrap
		    	
		    	localElement.setAttribute("condition", "(defined(FORK) || (" + localElement.getAttribute("condition") + ")) && (!defined(FORK) || (" + contentElemnet.getAttribute("condition") + "))");
		    	localElement.removeAttribute("isExtracted");
		    	localElement.setAttribute("isReWrapped", "1");
		    	nonrecursiveAddNodeList(nodenumber, at, localElement);
	    	} else {
		    	Node toIns = DOM.importNode(content, false);
		    	father.insertBefore(toIns, refNode);
		    	addNodeList(nodenumber, at, toIns);
				if (children > 0)
				{
					adoptChild(at, nodenumber, children);
				}
	    	}
	    	

	    } else if(nodename.equals("If") && content.getNodeName().equals("If") && !refNode.getNodeName().equals("If"))
	    {
			Node toIns = DOM.importNode(content, false);
			
			Element localElement = (Element) toIns;
			localElement.setAttribute("condition", "!defined(FORK) || (" + localElement.getAttribute("condition") + ")");
			
			father.insertBefore(toIns, refNode);
//			toIns.appendChild(refNode);
			addNodeList(nodenumber, at, toIns);
			if (children > 0)
			{
				dump = dumpXML(DOM, nodeList);
				getNode(at).isNew = 0;
				adoptChild(at, nodenumber, children);
			}
			addNodeList(nodenumber, at, toIns);
			
			cleanupNodeList();

//			dump = dumpXML(DOM, nodeList);
//			int a = 0;
	    } else if(nodename.equals("true"))
	    {
	    	if(refNode.getNodeName().equals("true")) {
	    		// <true> already exists
//	    		System.err.println("WRAP existing <true> -> What to do here?");
	    		
	    		Node toIns = DOM.importNode(content, false);
		    	((Element) toIns).setAttribute("from", "WRAP");
		    	
		    	if(isMarkupHowDelete(refNode))
				{
		    		// Unncessary, will be deleted?
		    		((Element)refNode).setAttribute("from", "WRAP");
		    		father.insertBefore(toIns, refNode);

					addNodeList(nodenumber, at, toIns);
					
					cleanupNodeList();

					if (children > 0)
					{
						adoptChild(at, nodenumber, children);
					}
					
		    		while(refNode.hasChildNodes())
		    		{
		    			toIns.appendChild(refNode.getFirstChild());
		    		}
		    		father.removeChild(refNode);
				} else {
					father.insertBefore(toIns, refNode);
			    	
			    	//1. append DELETED element in front
			    	while(refNode.getChildNodes().getLength() > 0 && isMarkupHowDelete(refNode.getChildNodes().item(0)))
			    	{
			    		toIns.appendChild(refNode.getChildNodes().item(0));
			    	}
			    	
//			    	//2. append refNode
//			    	toIns.appendChild(refNode);
//

			    	
			    	((Element)refNode).setAttribute("keepNotNew", "1");
					addNodeList(nodenumber, at, toIns);
					dump = dumpXML(DOM, nodeList);
					cleanupNodeList();

					if (children > 0)
					{
						adoptChild(at, nodenumber, children);
					}
					
					// Create copy of remaining child
					if(toIns.getNextSibling() == refNode)
					{
						// <true> is still sibling of new <true> --> move
						
						NodeList refNodeChildNodes = refNode.getChildNodes();
						for(int i = 0; i < refNodeChildNodes.getLength(); i++)
						{
							Node childnode = refNodeChildNodes.item(i);
							
							// TODO - the check on "content" is over specific, we need a check like "is cloned"
							if(!childnode.getNodeName().equals("content") && !isMarkupHowDelete(childnode))
							{
								Node clonedNode = childnode.cloneNode(true);
								// wrap in #ifndef FORK
								
						        Element cloneIf = DOM.createElement("If");
							    cloneIf.setAttribute("condition", "!defined(FORK)");
							    cloneIf.setAttribute("from", "DELETE / WRAP true (cloned)" );
							    Element cloneIfTrue = DOM.createElement("true");
							    cloneIf.appendChild(cloneIfTrue);
							    cloneIfTrue.appendChild(clonedNode);
							    
								toIns.appendChild(cloneIf);
							}
						}
					
						// Move remaining siblings out
						// TODO - We need to make sure parent #if actually was re-wrapped
						if(((Element)toIns.getParentNode()).hasAttribute("isReWrapped")) {
							toIns.getParentNode().getParentNode().insertBefore(refNode, toIns.getParentNode().getNextSibling());
						} else {
							toIns.appendChild(refNode);
						}
					}
					
			    	//3. append DELETED elements at the end
			    	while(refNode.getChildNodes().getLength() > 0 && isMarkupHowDelete(refNode.getChildNodes().item(refNode.getChildNodes().getLength() - 1)))
			    	{
			    		toIns.appendChild(refNode.getChildNodes().item(refNode.getChildNodes().getLength() - 1));
			    	}

					
				}
		    	

//				addNodeList(nodenumber, at, toIns);
				
				//getNode(nodenumber).isNew = 0;
	    	} else {
	    		System.out.println("WRAP: TODO: Can we ignore wrap of <true>?");
		    	Node toIns = DOM.importNode(content, false);
		    	father.insertBefore(toIns, refNode);
		    	addNodeList(nodenumber, at, toIns);
				if (children > 0)
				{
					dump = dumpXML(DOM, nodeList);
					getNode(at).isNew = 0;
					adoptChild(at, nodenumber, children);
				}
	    	}

	    }
	    else if(nodename.equals("else"))
	    {
	    	// skip 
	    	System.out.println("WRAP: skip else");
	    	
	    	addNodeList(nodenumber, at, refNode);
	    	((Element)refNode).removeAttribute("isExtracted");
	    	
	    	// Wrapping existing else -> set node to not new
	    	for(int i = 0; i < nodeList.size(); i++)
	    	{
	    		if(nodeList.get(i).getRefDomNode() == refNode)
	    		{
	    			nodeList.get(i).isNew = 0;
	    			break;
	    		}
	    	}

	    	
	    }
	    else if(nodename.equals("MacroDefined"))
	    {
	    	
	        Element cloneIf = DOM.createElement("If");
		    cloneIf.setAttribute("condition", "defined(FORK)");
		    cloneIf.setAttribute("from", "WRAP " + nodename);
		    Element cloneIfTrue = DOM.createElement("true");
		    cloneIf.appendChild(cloneIfTrue);
		    
	    	Node toIns = DOM.importNode(content, false);
	    	father.insertBefore(toIns, refNode);
	    	
	    	addNodeList(nodenumber, at, toIns);
			if (children > 0)
			{
				dump = dumpXML(DOM, nodeList);
				adoptChild(at, nodenumber, children);
			}
			
			// Wrap in #ifdef FORK
			father.insertBefore(cloneIf, toIns);
	    	cloneIfTrue.appendChild(toIns);
	    	
			
	    }
	    else {
	    	throw new IllegalStateException("Unhandled WRAP of " + nodename);
	    }
		cleanupNodeList();
		dump = dumpXML(DOM, nodeList);
	
		
		if (children > 0 && !nodename.equals("true")) {
			// @TODO
//			adoptChild(at, nodenumber, children);
		}
	}

	private void cleanupNodeList() {
		// clean up nodelist
		int i = 1;
		while(i < nodeList.size())
		{
			final Rnode ref = nodeList.get(i);
			nodeList.subList(i + 1, nodeList.size()).removeIf((Rnode node) -> node.getRefDomNode() == ref.getRefDomNode());
		
			i++;
		}
	}

	@Override
	public void DELETE(Integer nn, Integer nodecount, String paramString) {
	    System.out.println(" DELETE nn:" + nn + "(" + (nn.intValue() - this.nodeDeleted) + ") nodecount:" + nodecount);
	    
	    nn -= nodeDeleted;
		nodeDeleted += nodecount;
		
	    // Do not increase since we are not acuallty removing nodes and this would mess with the nodes list
//	    this.nodeDeleted += nodecount.intValue();
	    Node localNode1 = ((Rnode)getNode(nn.intValue())).getRefDomNode();
	    Node localNode2 = localNode1.getParentNode();
	    
	    String dump = dumpXML(DOM, nodeList);
	    
	    
	    
	    // TODO - Handle changes in non-elements
	    if(localNode1.getNodeType() != Node.ELEMENT_NODE)
	    {
	    	throw new IllegalStateException("DELETE of non-element node not supported.");
	    }
	    if(paramString != null)
	    {
	    	// MOVED
	    	System.out.println("moved to idref " + paramString);
	    }
	    
//	    
//	    System.out.println("DELETE before:");
//	    try {
//			printDocument(DOM, System.out);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    
	    
	    System.out.println("localNode1: " + localNode1.getNodeName() + " " + localNode1.getTextContent());
	    
	    if(localNode1.getNodeName().equals("true"))
	    {
	    	// If we should delete a <true> this means the outer <If> was extracted
	    	// thus, we can ignore it? This will be a problem if the <true> element is not deleted but extracted
	    	if(localNode2.getNodeName().equals("If") && ((Element) localNode2).getAttribute("isExtracted").equals("1") && !localNode1.hasChildNodes())
	    	{
	    		Element parentIf = (Element) localNode2;
	    		parentIf.setAttribute("condition", "!defined(FORK) && " + parentIf.getAttribute("condition"));
 	    	} else {
 	    		// Outer was not extraced. We "delete" the content 	    		
		        Element cloneIf = DOM.createElement("If");
			    cloneIf.setAttribute("condition", "!defined(FORK)");
			    cloneIf.setAttribute("from", "DELETE " + localNode1.getNodeName());
			    ((Element)localNode1).setAttribute("from", "DELETE " + localNode1.getNodeName());
			    Element cloneIfTrue = DOM.createElement("true");
			    
			    while(localNode1.hasChildNodes())
			    {
			    	cloneIfTrue.appendChild(localNode1.getFirstChild());
			    }
			    cloneIf.appendChild(cloneIfTrue);
			    localNode1.appendChild(cloneIf);
 	    	}
	    } else if(localNode1.getNodeName().equals("else")) 
	    {
	    	if(localNode2.getNodeName().equals("If") && ((Element) localNode2).getAttribute("isExtracted").equals("1") && !localNode1.hasChildNodes())
	    	{
	    		throw new IllegalStateException("Delete <else> for extracted #ifdef. Not implemented");
 	    	} else {
 	    		// Outer was not extraced. We "delete" the content 	    		
		        Element cloneIf = DOM.createElement("If");
			    cloneIf.setAttribute("condition", "!defined(FORK)");
			    cloneIf.setAttribute("from", "DELETE " + localNode1.getNodeName());
			    
			    Element cloneIfTrue = DOM.createElement("true");
			    
			    while(localNode1.hasChildNodes())
			    {
			    	cloneIfTrue.appendChild(localNode1.getFirstChild());
			    }
			    cloneIf.appendChild(cloneIfTrue);
			    localNode1.appendChild(cloneIf);
 	    	}
	    }
	    else {
	    	if(true || !localNode1.getTextContent().isEmpty()) {
		        Element cloneIf = DOM.createElement("If");
			    cloneIf.setAttribute("condition", "!defined(FORK)");
			    cloneIf.setAttribute("from", "DELETE " + localNode1.getNodeName());
			    
			    Element cloneIfTrue = DOM.createElement("true");
			    
			    localNode2.insertBefore(cloneIf, localNode1);
			    cloneIf.appendChild(cloneIfTrue);
			    cloneIfTrue.appendChild(localNode1);
	    	}
	    }
	    
	    subNodeList(nn, nodecount);
	    

	    dump = dumpXML(DOM, nodeList);
	    int a = 0;
	    

//	    System.out.println("DELETE after:");
//	    try {
//			printDocument(DOM, System.out);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (TransformerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	    
//	    Object localObject;
//	    if (paramString == null)
//	      {
//	        localObject = this.DOM.createAttributeNS("http://schirinz.web.cs.unibo.it/Ndiff", "status");
//	        ((Attr)localObject).setPrefix("ndiff");
//	        ((Attr)localObject).setValue("deleted");
//	        ((Element)localNode1).setAttributeNode((Attr)localObject);
//	      }
//	      else
//	      {
//	        localObject = this.DOM.createAttributeNS("http://schirinz.web.cs.unibo.it/Ndiff", "status");
//	        ((Attr)localObject).setPrefix("ndiff");
//	        ((Attr)localObject).setValue("movedTo");
//	        ((Element)localNode1).setAttributeNode((Attr)localObject);
//	        localObject = this.DOM.createAttributeNS("http://schirinz.web.cs.unibo.it/Ndiff", "idref");
//	        ((Attr)localObject).setPrefix("ndiff");
//	        ((Attr)localObject).setValue(paramString);
//	        ((Element)localNode1).setAttributeNode((Attr)localObject);
//	      }
//	      subNodeList(paramInteger1.intValue(), paramInteger2.intValue());	  
	    

	    
	}
	  
	private void printDocument(Document doc, OutputStream out) throws IOException, TransformerException {
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	    transformer.transform(new DOMSource(doc), new StreamResult(new OutputStreamWriter(out, "UTF-8")));
	}
	
	  private void subNodeList(int paramInt1, int paramInt2)
	  {
	    for (int i = 0; i < paramInt2; i++) {
	      this.nodeList.removeElementAt(paramInt1);
	    }
	  }
	  
	@Override
	public void UNWRAP(Integer nodenumber, String nodename) {
	    System.out.println(" EXTRACT nn:" + nodenumber + "(" + (nodenumber.intValue() - this.nodeDeleted) + ")");
	    
	    nodenumber -= nodeDeleted;
	    nodeDeleted++;
	    
//	    this.nodeDeleted += 1;
	    Node localNode1 = ((Rnode)getNode(nodenumber.intValue())).getRefDomNode();
	    Node localNode2 = localNode1.getParentNode();
//	    while (localNode1.hasChildNodes()) {
//	      localNode2.insertBefore(localNode1.getFirstChild(), localNode1);
//	    }
//	    localNode2.removeChild(localNode1);
//	    subNodeList(paramInteger.intValue(), 1);
	    
	    {
	    	String dump = dumpXML(DOM, nodeList);
	    	int a = 0;
	    }
	    
	    if(localNode1.getNodeType() == Node.ELEMENT_NODE)
	    {
	    	((Element) localNode1).setAttribute("isExtracted", "1");
	    }
	    
	    System.out.println("EXTRACT Nodename " + localNode1.getNodeName());
	    
	    if(nodename.equals("If"))
	    {
	    	// We add #ifndef FORK, we will have to handle <true> and <else> again
//	    	this.nodeDeleted -= 1;
//		    Element cloneIf = DOM.createElement("If");
//		    cloneIf.setAttribute("condition", "!defined(FORK)");
//		    cloneIf.setAttribute("from", "EXTRACT If");
//		    Element cloneIfTrue = DOM.createElement("true");
//		    cloneIf.appendChild(cloneIfTrue);
//		    Element cloneIfElse = DOM.createElement("else");
//		    cloneIf.appendChild(cloneIfElse);
//		    
//		    localNode1.getParentNode().replaceChild(cloneIf, localNode1);
//		    cloneIfTrue.appendChild(localNode1);
	    	
	    	Element localElement = (Element) localNode1;
//	    	localElement.setAttribute("condition", "defined(FORK) || " + localElement.getAttribute("condition"));
	    	
	    } else if(nodename.equals("else")) {
	    	System.out.println("EXTRACT else parent: " + localNode2.getNodeName());
//	    	// Create new if for nodes in Else
//	    	
//	    	((Element)localNode1).setAttribute("from", "EXTRACT else");
//	    
//	    	// localNode2 = original <If>
//	    	// localNode2.getParent() = <true> from inserted fork #ifdef
//	    	// localNode2.getParent().getParent() = <If> from extract If
//	    	Node extractedParent = localNode2.getParentNode().getParentNode();
//	    	Node extractedIfChild = extractedParent.getFirstChild();
//	    	while(!extractedIfChild.getNextSibling().getNodeName().equals("else")) {
//	    		extractedIfChild = extractedIfChild.getNextSibling();
//	    	}
//	    	
//	    	Node ref = extractedIfChild.getNextSibling();
//	    	
//	    	NodeList childNodes = localNode1.getChildNodes();
//	    	
//	    	for(int i = 0; i < childNodes.getLength(); i++)
//	    	{
//		    	Node n = childNodes.item(i);
////		    	ref.getParentNode().insertBefore(n.cloneNode(true), ref.getNextSibling());
//		    	ref.appendChild(n.cloneNode(true));
//		    	ref = n;
//	    	}
	    	
//		    localNode2.removeChild(localNode1);
	    } 
	    else if(nodename.equals("true"))
	    {
	    	// Do nothing
//	    	subNodeList(nodenumber, 1);
	    	((Element)localNode1).setAttribute("isExtracted", "1");
	    	if(localNode2.getNodeName().equals("If"))
	    	{
		    	Element parentIf = (Element) localNode2; 
		    	parentIf.setAttribute("condition", "defined(FORK) || " + parentIf.getAttribute("condition"));	    		
	    	}
	    }
	    else if(nodename.equals("MacroDefined"))
	    {
	    	// We wrap the old element in #ifdef and copy the content, expecting it to be re-wrapped
	    	((Element)localNode1).removeAttribute("isExtracted");
	    	
	    	
	        Element cloneIf = DOM.createElement("If");
	        cloneIf.setAttribute("from", "DELETE UNWRAP MacroDefined");
		    cloneIf.setAttribute("condition", "!defined(FORK)");
		    Element trueElement = DOM.createElement("true");
		    cloneIf.appendChild(trueElement);
		    
		    localNode1.getParentNode().insertBefore(cloneIf, localNode1);
	    	
		    Node clonedChild = localNode1.getFirstChild().cloneNode(true);
		    localNode1.appendChild(clonedChild);
		    
	    	localNode1.getParentNode().insertBefore(localNode1.getFirstChild(), localNode1.getNextSibling());
	    	trueElement.appendChild(localNode1);
	    }
	    else {
	    	throw new IllegalStateException("Unhandled EXTRACT of " + nodename);
	    }
	    
	    subNodeList(nodenumber, 1);
	    
	    
	    {
	    	String dump = dumpXML(DOM, nodeList);
	    	int a = 0;
	    }
	}

	@Override
	public void INS(Integer arg0, Integer arg1, Integer arg2, Node arg3) {
		// TODO Auto-generated method stub
		super.INS(arg0, arg1, arg2, arg3);
	}
	
	private int getExtractedChildren(Node node)
	{
		int offset = 0;
    	if(node.getNodeType() == Node.ELEMENT_NODE)
    	{
    		Element element = (Element) node;
    		if(element.hasAttribute("isExtracted"))
    		{
    			// recurse
    			NodeList children = element.getChildNodes();
    			for(int j = 0; j < children.getLength(); j++)
    			{
    				offset += getExtractedChildren(children.item(j));
    			}
    		} else {
    			if(!isMarkupHowDelete(element)) {
    				offset++;
    			}
    		}
    	}
    	return offset;
	}

	@Override
	public void INSERT(Integer paramInteger1, Integer paramInteger2, Integer pos, Integer paramInteger4,
			Node paramNode, String paramString) {
		
 		System.out.println(" INSERT nn:" + paramInteger1 + " at:" + paramInteger2 + " pos:" + pos + " nodeCount:" + paramInteger4);
 		
 		String dump = dumpXML(DOM, nodeList);
		
 		
		Node localNode1 = this.DOM.importNode(paramNode, true);	
	    Node localNode2 = ((Rnode)getNode(paramInteger2.intValue())).refDomNode;
	    // TODO - Handle changes in non-elements
	    if(localNode1.getNodeType() != Node.ELEMENT_NODE)
	    {
	    	String xml = dumpXML(DOM, nodeList);
	    	throw new IllegalStateException("INSERT of non-element (Type " + localNode1.getNodeType() + ") node not supported.");
	    }
	     
	    Element cloneIf = DOM.createElement("If");
	    cloneIf.setAttribute("condition", "defined(FORK)");
	    cloneIf.setAttribute("from", "INSERT");
	  
	    Element cloneIfTrue = DOM.createElement("true");
	    cloneIf.appendChild(cloneIfTrue);
	    
		// We do not INSERT <true> but only its children in the new #ifdef. Same for <else>
		if(localNode1.getNodeName().equals("true") || localNode1.getNodeName().equals("else"))
		{
			counter = paramInteger1;
			while(localNode1.hasChildNodes())
			{
				Node child = localNode1.getFirstChild();
				cloneIfTrue.appendChild(child);
				addNodeList(counter, paramInteger2, child);
			}
		}
		else {
			cloneIfTrue.appendChild(localNode1);
		}
	    
	    
	    // Fix pos w.r.t. extracted
	    NodeList localNodeList = localNode2.getChildNodes();
	    
	    if(pos > localNodeList.getLength())
	    {
	    	// Error condition?
	    	int a = 0;
	    }
	    
	    int offset = 0;
	    for (int i = 0; i < pos; i++) {
	    	if(localNodeList.item(i).getNodeType() == Node.ELEMENT_NODE)
	    	{
	    		Element element = (Element) localNodeList.item(i);
	    		if(element.hasAttribute("isExtracted")) {
	    			// Offset for "not actually extracted" and "- 1" for the #if in its place
	    			pos -= getExtractedChildren(localNodeList.item(i)) - 1;
	    		}
	    	}
	    }
//	    if(offset != 0) {
//	    	pos -= offset;
//	    }
	    
	    Node localObject2 = findChildPosition(localNode2, pos);
	    if (localObject2 != null) {	    	
	    	if(localNode1.getNodeName().equals("true") || localNode1.getNodeName().equals("else"))
	    	{
	    		if(localObject2.getNodeName().equals(localNode1.getNodeName()))
	    		{
	    			localObject2.appendChild(cloneIf);
	    			nonrecursiveAddNodeList(paramInteger1, paramInteger2, localObject2);
	    			((Element)localObject2).removeAttribute("isExtracted");
	    		} else {
	    			// Check if there is alredy a (deleted) <true> in the parent #ifdef
	    			
	    			if(localNode2.getFirstChild().getNodeName().equals("true") || localNode2.getFirstChild().getNodeName().equals("else"))
	    			{
	    				// We keep its children and add the newly inserted
	    				Node existingNode = localNode2.getFirstChild();
	    				((Element)existingNode).setAttribute("from", "INSERTED " + localNode1.getNodeName());
	    				
	    				existingNode.appendChild(cloneIf);
	    				
	    				nonrecursiveAddNodeList(paramInteger1, paramInteger2, existingNode);
	    			}
	    			
	    		}
	    	}
	    	else {
	    		localNode2.insertBefore(cloneIf, localObject2);
	    	}
	    	
	    } else {
	    	if(!(localNode2 instanceof Element))
	    	{
	    		String xml = dumpXML(DOM, nodeList);
	    		
	    		throw new IllegalStateException("Can only add children to Element");
	    	}
	    	if(localNode2 == cloneIf)
	    	{
	    		throw new IllegalStateException("Cannot add same node as child");
	    	}
	    	if(localNode2.getParentNode() == cloneIf)
	    	{
	    		throw new IllegalStateException("Cannot add parent as child");
	    	}
	    	
		    if(localNode2.getNodeName().equals("text"))
		    {
		    	String xml = dumpXML(DOM, nodeList);
		    	throw new IllegalStateException("Must not insert into <text>");
		    }
	    	
	    	// If we try to add a <true> and the parent is not already a <true>, we add a <true> wrapping our #ifdef. Same for <else>
	    	if((localNode1.getNodeName().equals("true") && !localNode2.getNodeName().equals("true"))
	    		|| (localNode1.getNodeName().equals("else") && !localNode2.getNodeName().equals("else")))
	    	{
	    		localNode2.appendChild(localNode1);
	    		localNode1.appendChild(cloneIf);
	    		nonrecursiveAddNodeList(paramInteger1, paramInteger2, localNode1);
				String xml = dumpXML(DOM, nodeList);

	    	} else {
	    		localNode2.appendChild(cloneIf);
	    	}
	    }
	    
	    // If we insert a node that was previously deleted, we have to update the nodelist
	    
	    boolean nodeListIsUpdated = false;
	    if(paramInteger1 < nodeList.size())
	    {
	    	Node node = nodeList.get(paramInteger1).getRefDomNode();
	    	Node parent2 = node.getParentNode().getParentNode();
	    	if(parent2.getNodeType() == Node.ELEMENT_NODE)
	    	{
	    		Element parent2Element = (Element) parent2;
	    		if(parent2Element.getAttribute("from").contains("DELETE"))
	    		{
	    			Rnode localRnode = new Rnode();
	    			localRnode.refDomNode = localNode1;
	    			localRnode.isNew = 1;
	    			localRnode.indexKey = paramInteger1;
	    			localRnode.posFather = paramInteger2;
	    			
	    			nodeList.set(paramInteger1, localRnode);
	    			nodeListIsUpdated = true;
	    		}
	    	}
	    	
	    }
	    if(!nodeListIsUpdated) {
	    	// not clone if but inserted element should be paraemter 3!=
	    	
	    	if(!localNode1.getNodeName().equals("true") && !localNode1.getNodeName().equals("else"))
	    	{
	    		addNodeList(paramInteger1, paramInteger2, localNode1);
	    	} else {
	    		// Already did special action for addNodeList for INSERT <true> above
	    	}
	    }
	    
	    dump = dumpXML(DOM, nodeList);
	    int a = 0;
	    
	    // Check for nodes in nodelist where the parent is not in the nodelist
	    for(int i = 1; i < nodeList.size(); i++)
	    {
	    	Node node = nodeList.get(i).getRefDomNode();
	    	if(node.getNodeType() == Node.TEXT_NODE)
	    	{
	    		if(nodeList.get(i-1).getRefDomNode() != node.getParentNode())
	    		{
	    			// Illegal State?
	    			int b = 0;
	    			throw new IllegalStateException("Failed nodeList consistency check.");
	    		}
	    	}
	    }
	}
	
	
	public static String dumpXML(Node node, Vector<Rnode> nodeList)
	{
		
		// Remove old nodenumber attributes
		try {
			XPathFactory xpf = XPathFactory.newInstance();
			XPath xpath = xpf.newXPath();
			XPathExpression expr = xpath.compile("//*[@nodenumber]");
			NodeList nl = (NodeList) expr.evaluate(node, XPathConstants.NODESET);
			for(int i = 0; i < nl.getLength(); i++)
			{
				Element element = (Element) nl.item(i);
				element.removeAttribute("nodenumber");
				element.removeAttribute("pos");
			}
			
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		// Test code to display nodenumbers in the XML dump
		int i = 0;
		
		
	    for (Rnode n : nodeList) {
			if(n.getRefDomNode().getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) n.getRefDomNode();
				int pos = -1;
				if(element.getParentNode() != null)
				{
					for(int j = 0; j < element.getParentNode().getChildNodes().getLength(); j++)
					{
						if(element == element.getParentNode().getChildNodes().item(j))
						{
							pos = j;
							break;
						}
					}
				}
				
				if(element.hasAttribute("nodenumber"))
				{
					System.err.println("nodenumber " + element.getAttribute("nodenumber") + " will be overwritten with number " + i);
//					throw new IllegalStateException("Element is duplicated in nodelist.");
				}
//				if(element.hasAttribute("isExtracted"))
//				{
//					throw new IllegalStateException("Extracted elements must not have a nodenumber");
//				}
				
				element.setAttribute("nodenumber", Integer.toString(i));
				element.setAttribute("pos", Integer.toString(pos));
				element.setAttribute("isNew", Integer.toString(n.isNew));
				element.setAttribute("posFather", Integer.toString(n.getPosFather()));
				
				// Check invariant of correct posFather
				if(n.getPosFather() > -1)
				{
//					Rnode father = nodeList.get(n.getPosFather());
//					if(father == null || n.getRefDomNode().getParentNode() != father.getRefDomNode())
//					{
//						int a = 0;
//					}
				}

			}
			i++;
		}
	    
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			String output = writer.getBuffer().toString()/*.replaceAll("\n|\r", "")*/;
			return output;
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	

	@Override
	public void closeEditing() {
		// TODO Auto-generated method stub
		super.closeEditing();
	}

	@Override
	public boolean isMarkup(Node arg0) {
		// TODO Auto-generated method stub
		return super.isMarkup(arg0);
	}

	@Override
	public boolean isMarkupHowDelete(Node node) {
		
		if(node.getNodeType() == Node.ELEMENT_NODE)
		{
			Element element = (Element) node;
			if(element.getAttribute("from").startsWith("DELETE ")) {
				return true;
			}
		}
		
		// TODO Auto-generated method stub
		return super.isMarkupHowDelete(node);
	}

	@Override
	public Rnode newNode(Object arg0, Node arg1, Integer arg2, Integer arg3,
			Integer arg4) {
		// TODO Auto-generated method stub
		return super.newNode(arg0, arg1, arg2, arg3, arg4);
	}
	
	private Node findChildPosition(Node paramNode, Integer pos) {
		NodeList localNodeList = paramNode.getChildNodes();
		int i = 0;
		Node localNode = null;
		for (int j = 0; j < localNodeList.getLength(); j++) {
			Node n = localNodeList.item(j);
			if (i == pos.intValue()) {
				localNode = localNodeList.item(j);
			}
			if (!isMarkupHowDelete(localNodeList.item(j))) {
				i++;
			}
		}
		return localNode;
	} 
	

	private void addNodeList(Integer paramInteger1, Integer paramInteger2, Node paramNode) {
		counter = paramInteger1;
		recursiveAddNodeList(paramInteger2, paramNode);
	}
	
	private void nonrecursiveAddNodeList(int index, int posFather, Node paramNode) {
		Rnode localRnode = new Rnode();
		localRnode.refDomNode = paramNode;
		if(paramNode.getNodeType() != Node.ELEMENT_NODE || !((Element)paramNode).hasAttribute("keepNotNew"))
		{
			localRnode.isNew = 1;
		}
		localRnode.indexKey = index;
		localRnode.posFather = posFather;
		this.nodeList.insertElementAt(localRnode, index);
	}
	
	private void recursiveAddNodeList(Integer paramInteger, Node paramNode) {
		Rnode localRnode = new Rnode();
		localRnode.refDomNode = paramNode;
		if(paramNode.getNodeType() != Node.ELEMENT_NODE || !((Element)paramNode).hasAttribute("keepNotNew"))
		{
			localRnode.isNew = 1;
		}
		localRnode.indexKey = this.counter;
		localRnode.posFather = paramInteger;
		this.nodeList.insertElementAt(localRnode, this.counter.intValue());
		Integer localInteger1 = this.counter;
		Integer localInteger2 = this.counter = Integer.valueOf(this.counter.intValue() + 1);
		if (paramNode.hasChildNodes()) {
			for (int i = 0; i < paramNode.getChildNodes().getLength(); i++) {
				Node node = paramNode.getChildNodes().item(i);
				if(!isMarkupHowDelete(node) && (node.getNodeType() != Node.ELEMENT_NODE || !((Element)node).hasAttribute("isExtracted")))
				{
					recursiveAddNodeList(localRnode.indexKey, node);
				}
				
			}
		}
	}
}
