/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge.XmlUnitDiff;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;

/*
 * Here, I try the xml diff from xmlunit2
 */
public class TreeTraverser {
	
	private Document doc1;
	private Document doc2;
	private Diff diff;
	
	public TreeTraverser(Document doc1, Document doc2, Diff diff) throws ParserConfigurationException
	{
		this.doc1 = doc1;
		this.doc2 = doc2;
		this.diff = diff;
	}
	
	public void traverse() throws SAXException, IOException
	{	
		StringBuilder sb = new StringBuilder();
		
		Set<Difference> handledDifferences = new HashSet<>();
		
		Element root1 = doc1.getDocumentElement();
		NodeList nodes = root1.getChildNodes();
		for(int i = 0; i < nodes.getLength(); i++) {
			if(nodes.item(i).getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) nodes.item(i);
				switch(element.getNodeName()) {
					case "text":
						
						boolean foundInDiff = false;
						Difference currentDifference = null;
						
						for(Difference difference : diff.getDifferences())
						{
							Node ref = difference.getComparison().getControlDetails().getTarget();
							if(ref.getNodeType() == Node.CDATA_SECTION_NODE)
							{
								ref = ref.getParentNode();
							}
							
							if(ref == element) {
								foundInDiff = true;
								currentDifference = difference;
							}
						}
						
						if(foundInDiff && !handledDifferences.contains(currentDifference))
						{
							sb.append("#ifndef CLONE\n");
							
							sb.append(currentDifference.getComparison().getControlDetails().getTarget().getTextContent());
							sb.append("\n");
							
							sb.append("#else\n");
							
							sb.append(currentDifference.getComparison().getTestDetails().getTarget().getTextContent());
							sb.append("\n");
							
							sb.append("#endif\n");
							handledDifferences.add(currentDifference);
						} else {
							sb.append(element.getTextContent());
							sb.append("\n");
						}
						break;
					case "MacroDefined":
						sb.append("#define " + element.getAttribute("name") + " " + element.getElementsByTagName("content").item(0).getTextContent());
						sb.append("\n");
						break;
					case "If":
						sb.append("#if " + element.getAttribute("condition") + "\n");
						
						sb.append("#endif" + "\n");
						break;
					default:
						throw new IllegalStateException("Unhandled element type " + element.getNodeName());
				}
			}
		}
		
//		System.out.println(sb.toString());
		
	}
	
}
