/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge;

import de.uni_leipzig.iwi.CPPClient.XMLConverter;
import de.uni_leipzig.iwi.ppmerge.xsltfunctions.SimplifyCondition;
import de.uni_leipzig.iwi.ppmerge.xsltfunctions.XSLTHelper;
import it.unibo.cs.ndiff.common.vdom.DOMDocument;
import net.sf.saxon.Configuration;
import net.sf.saxon.TransformerFactoryImpl;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class XSLTMergeSimplifier {
    private static final String xsltFile1 = "xslt/xslt01.xml";
    private static final String xsltFile2 = "xslt/xslt02.xml";
    private static final String xsltFile3 = "xslt/xslt03.xml";
    private static final String xsltFile4 = "xslt/xslt04.xml";


    public String runXSLTSimplification(XMLConverter xmlConverter, DOMDocument merged, File variant) throws TransformerException, IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        String tempDir = System.getProperty("java.io.tmpdir");
        File simplified = new File(tempDir, "xmloutput/" + FilenameUtils.getBaseName(variant.getName()) + "_simplified.xml");

        Transformer transformer;
        TransformerFactory factory = TransformerFactory.newInstance();

        SAXTransformerFactory stf = (SAXTransformerFactory)TransformerFactory.newInstance();

        Configuration config = ((TransformerFactoryImpl)stf).getConfiguration();
        config.registerExtensionFunction(new XSLTHelper());
        config.registerExtensionFunction(new SimplifyCondition());

        ClassLoader classLoader = getClass().getClassLoader();
        Templates templates1 = stf.newTemplates(new StreamSource(classLoader.getResourceAsStream(xsltFile1)));
        Templates templates2 = stf.newTemplates(new StreamSource(classLoader.getResourceAsStream(xsltFile2)));
        Templates templates3 = stf.newTemplates(new StreamSource(classLoader.getResourceAsStream(xsltFile3)));
        Templates templates4 = stf.newTemplates(new StreamSource(classLoader.getResourceAsStream(xsltFile4)));

        TransformerHandler th1 = stf.newTransformerHandler(templates1);
        TransformerHandler th2 = stf.newTransformerHandler(templates2);
        TransformerHandler th3 = stf.newTransformerHandler(templates3);
        TransformerHandler th4a = stf.newTransformerHandler(templates4);
        TransformerHandler th4b = stf.newTransformerHandler(templates4);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(buffer);

        // Template four extract ONE common element from a #ifdef/#else, or the whole #ifdef if both branches are the same, calling it twice allows to extract at least two elements --> need generalization
        // Still need cleanup of empty #ifdef (is elements where extracted one-by-one)

        th1.setResult(new SAXResult(th2));
        th2.setResult(new SAXResult(th3));
        th3.setResult(new SAXResult(th4a));
        th4a.setResult(new SAXResult(th4b));
        th4b.setResult(new StreamResult(printStream));

        transformer = factory.newTransformer();
        transformer.transform(new DOMSource(merged.DOM), new SAXResult(th1));

        th4b.setResult(new StreamResult(simplified));

        String simplifiedString = new String(buffer.toByteArray(), Charsets.UTF_8);
        FileUtils.writeStringToFile(simplified, simplifiedString, Charsets.UTF_8);

        String simplifiedSourceCode = xmlConverter.createSourceCode(simplifiedString);

        simplifiedSourceCode = simplifiedSourceCode.replaceAll("\r\n", "\n");
        return simplifiedSourceCode;
    }
}
