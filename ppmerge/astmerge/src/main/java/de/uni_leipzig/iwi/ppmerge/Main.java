/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge;


import it.unibo.cs.ndiff.exceptions.ComputePhaseException;
import it.unibo.cs.ndiff.exceptions.InputFileException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args){
		if (args.length != 3) {
			System.out.println("Requires exactly 3 arguments: PATH_TO_BASELINE PATH_TO_VARIANT OUTPUT_NAME");
		}
		else {
			File baselinePath = new File(args[0]);
			File variantPath = new File(args[1]);
			File outputFile = new File(args[2]);
			try {
			    PPMerge ppMerge = new PPMerge(baselinePath, variantPath, outputFile);
				ppMerge.serializeIntegratedSource(outputFile.getName(), false);
			} catch (ParserConfigurationException | IOException | XPathExpressionException | SAXException | InputFileException | TransformerException | ComputePhaseException e) {
				e.printStackTrace();
			}
        }
	}
}
