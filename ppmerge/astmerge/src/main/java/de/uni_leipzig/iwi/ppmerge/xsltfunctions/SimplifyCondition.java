/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge.xsltfunctions;

import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.Item;
import net.sf.saxon.om.LazySequence;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.SequenceType;
import net.sf.saxon.value.StringValue;

public class SimplifyCondition extends ExtensionFunctionDefinition {
	
	StructuredQName qname = new StructuredQName("xslthelper", "http://PPMerge.iwi.uni-leipzig.de/saxon-extension", "SimplifyCondition");

	@Override
	public int getMaximumNumberOfArguments() {
		return 1;
	}

	@Override
	public int getMinimumNumberOfArguments() {
		return 1;
	}

	@Override
	public SequenceType[] getArgumentTypes() {
		return new SequenceType[]{SequenceType.SINGLE_STRING};
	}

	@Override
	public StructuredQName getFunctionQName() {
		return qname;
	}

	@Override
	public SequenceType getResultType(SequenceType[] arg0) {
		return SequenceType.SINGLE_STRING;
	}

	@Override
	public ExtensionFunctionCall makeCallExpression() {
		return new ExtensionFunctionCall() {
			
			@Override
			public Sequence call(XPathContext arg0, Sequence[] arg1) throws XPathException {
				String c1 = null;
				if(arg1[0] instanceof LazySequence)
				{
					Item item = ((LazySequence) arg1[0]).head();
					c1 = ((StringValue) item).getStringValue();
				}
				
				/*
				 * TODO: add call to solver
				 */
				
				if(c1.equals("defined(FORK) || SDCARDDETECT == -1 && !defined(FORK) || SDCARDDETECT == -1"))
				{
					return new StringValue("SDCARDDETECT == -1");
				} else {
					return new StringValue(c1);
				}
			}
		};
	}

}
