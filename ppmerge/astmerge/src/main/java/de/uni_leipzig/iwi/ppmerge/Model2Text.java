/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/*
 * This class takes the xml format from the CPPClient's ResultModel
 * If we go along this way, we need a propery serialization/deserialization strategy
 */
public class Model2Text {
	
	private Document doc;
	private Mode activeMode;
	
	public enum Mode
	{
		INSERTED,
		DELETED, NONE, SHOW_DELETED
	}
	
	public Model2Text(String xml, Mode mode) throws SAXException, IOException, ParserConfigurationException
	{
		this.activeMode = mode;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		doc = builder.parse(new InputSource(new StringReader(xml)));
	}
	
	public Model2Text(Document doc, Mode mode) throws SAXException, IOException, ParserConfigurationException
	{
		this.doc = doc;
		this.activeMode = mode;
	}

	public String convert() {
		Element root = doc.getDocumentElement();

		StringBuilder sb = new StringBuilder();
		NodeList nodes = root.getChildNodes();
		for(int i = 0; i < nodes.getLength(); i++) {
			if(nodes.item(i).getNodeType() == Node.ELEMENT_NODE)
			{
				sb.append(traverse(nodes.item(i)));
			}
		}
		
		return sb.toString();
	}
			
			
	private String traverse(Node node)
	{
		StringBuilder sb = new StringBuilder();
		Element element = (Element) node;
		
		boolean inserted = false;
		boolean deleted  = false;
		
		if(element.hasAttribute("ndiff:status"))
		{
			if(element.getAttribute("ndiff:status").equals("inserted")) {
				inserted = true;
			} else if(element.getAttribute("ndiff:status").equals("deleted")) {
				deleted = true;
			}
		}
		
		if(inserted && activeMode != Mode.INSERTED)
		{
			return "";
		}
		if(deleted && activeMode != Mode.SHOW_DELETED)
		{
			return "";
		}
		
		switch(node.getNodeName())
		{
		case "text":
			sb.append(element.getTextContent());
			sb.append("\n");
			break;
		case "MacroDefined":
			sb.append("#define " + element.getAttribute("name") + " " + element.getTextContent() + "\n");
			break;
		case "If":
			sb.append("#if " + element.getAttribute("condition") + "\n");
			// true
			Element trueElement = (Element) element.getElementsByTagName("true").item(0);
			for(int i = 0; i < trueElement.getChildNodes().getLength(); i++) {
				if( trueElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
				{
					sb.append(traverse( trueElement.getChildNodes().item(i)));
				}
			}
			// else
			if(element.getElementsByTagName("else").getLength() > 0)
			{
				sb.append("#else\n");
				Element elseElement = (Element) element.getElementsByTagName("else").item(0);
				for(int i = 0; i < elseElement.getChildNodes().getLength(); i++) {
					if( elseElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
					{
						sb.append(traverse( elseElement.getChildNodes().item(i)));
					}
				}
			}
			
			// endif
			sb.append("#endif" + "\n");
			break;
		default:
			throw new IllegalStateException("Node " + node.getNodeName() + " not supported.");
		}
		

		
		return sb.toString();
	}
			

}
