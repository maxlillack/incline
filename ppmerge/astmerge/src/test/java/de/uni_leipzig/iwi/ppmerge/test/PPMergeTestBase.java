/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge.test;

import de.uni_leipzig.iwi.ppmerge.PPMerge;
import it.unibo.cs.ndiff.exceptions.ComputePhaseException;
import it.unibo.cs.ndiff.exceptions.InputFileException;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PPMergeTestBase {
    public static final String BASE_DIR = "src/main/resources/examples";

    protected void runTest(String name, File input_a, File input_b,
                         String expected) throws ParserConfigurationException, IOException,
            SAXException, XPathExpressionException,
            TransformerFactoryConfigurationError, TransformerException,
            InputFileException, ComputePhaseException {
        runTest(name, input_a, input_b, expected, false, false);
    }

    protected void runTest(String name, File baseline, File variant,
                         String expected, boolean skipXMLGeneration, boolean skipResultCheck) throws ParserConfigurationException, IOException,
            SAXException, XPathExpressionException,
            TransformerFactoryConfigurationError, TransformerException,
            InputFileException, ComputePhaseException {
        assertTrue(baseline.getAbsolutePath() + " does not exist.", baseline.exists());
        assertTrue(variant.getAbsolutePath() + " does not exist.", variant.exists());

        File outputFile = new File(BASE_DIR, "output/" + name + "_integrated." + FilenameUtils.getExtension(variant.getName()));
        PPMerge ppMerge = new PPMerge(baseline, variant, outputFile);
        String simplifiedSourceCode = ppMerge.serializeIntegratedSource(name, skipXMLGeneration);

        expected = expected.replaceAll("\r\n", "\n");
        if(!skipResultCheck) {
            assertEquals(expected, simplifiedSourceCode);
        }
    }


}
