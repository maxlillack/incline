/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge.test;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

import java.io.File;

public class TaskWorkflowTest extends PPMergeTestBase {
    @Test
    public void tasks() throws Exception {
        String task;
        String name;

        String expected = "";

//		{
//			task = "2daa859";
//			name = "Marlin_main_cleaned.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}

//		{
//			task = "2daa859";
//			name = "ultralcd_cleaned.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//		{
//			task = "17de96a";
//			name = "cardreader.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//		{
//			task = "46f80e8";
//			name = "ultralcd.h";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//
//		{
//			task = "47c1ea7";
//			name = "temperature.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}


        {
            task = "373f3ec";
            name = "Marlin_main_cleaned.cpp";
            File input_a = new File("../../../marlin-mining/examples/tasks/" + task + "/base", name);
            File input_b = new File("../../../marlin-mining/examples/tasks/" + task + "/remote", name);
            runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
        }
//
//		{
//			task = "08856d9";
//			name = "ultralcd_implementation_hitachi_HD44780.h";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//
//		{
//			task = "3116271";
//			name = "temperature.h";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//
//		{
//			task = "esenapaj";
//			name = "Marlin_main.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/mainline", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/fork", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//
//		{
//			name = "stepper.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/mainline", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/fork", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//		{
//			name = "ultralcd.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/mainline", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/fork", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//		{
//			task = "Marlin_STM32";
//			name = "Marlin_main_cleaned.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/mainline", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/fork", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//		{
//			task = "Marlin_STM32";
//			name = "planner_cleaned.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/mainline", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/fork", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//		{
//			task = "Marlin_STM32";
//			name = "stepper_cleaned.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/mainline", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/fork", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//
//		{
//			task = "17de96a";
//			name = "cardreader.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//		{
//			task = "jcrocholl";
//			name = "Marlin_main.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/mainline", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/fork", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//		{
//			task = "46f80e8";
//			name = "ultralcd.h";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//		{
//			task = "47c1ea7";
//			name = "temperature.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
//		{
//			task = "373f3ec";
//			name = "Marlin_main_cleaned.cpp";
//			File input_a = new File("../../marlin-mining/examples/tasks/" + task + "/base", name);
//			File input_b = new File("../../marlin-mining/examples/tasks/" + task + "/remote", name);
//			runTest(FilenameUtils.getBaseName(name) + "_" + task, input_a, input_b, expected, false, true);
//		}
    }


}
