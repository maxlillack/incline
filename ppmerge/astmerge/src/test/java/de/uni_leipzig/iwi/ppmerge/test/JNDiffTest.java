/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge.test;

import de.uni_leipzig.iwi.ppmerge.PPMerge;
import it.unibo.cs.ndiff.common.vdom.DOMDocument;
import it.unibo.cs.ndiff.core.Nconfig;
import it.unibo.cs.ndiff.core.Ndiff;
import it.unibo.cs.ndiff.debug.Debug;
import it.unibo.cs.ndiff.exceptions.ComputePhaseException;
import it.unibo.cs.ndiff.exceptions.InputFileException;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;

public class JNDiffTest extends PPMergeTestBase {

	@Test
	public void marlin_test8() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "input8";
		File input_a = new File(BASE_DIR, "marlin/input8-a.cpp");
		File input_b = new File(BASE_DIR, "marlin/input8-b.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "marlin/input8-expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void marlin_test7() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "input7";
		File input_a = new File(BASE_DIR, "marlin/input7-a.cpp");
		File input_b = new File(BASE_DIR, "marlin/input7-b.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "marlin/input7-expected.cpp"), Charsets.UTF_8);

		// @TODO - check comments

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void marlin_test6() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		// Technically correct, but interesting differences
		String name = "input6";
		File input_a = new File(BASE_DIR, "marlin/input6-a.cpp");
		File input_b = new File(BASE_DIR, "marlin/input6-b.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "marlin/input6-expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "input6";
		File input_a = new File(BASE_DIR, "ultralcd_mainline.cpp");
		File input_b = new File(BASE_DIR, "ultralcd_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "ultralcd_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}


	@Test
	public void ultralcd_test01() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test1";
		File input_a = new File(BASE_DIR, "ultralcd_test1_mainline.cpp");
		File input_b = new File(BASE_DIR, "ultralcd_test1_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "ultralcd_test1_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test02() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test2";
		File input_a = new File(BASE_DIR, "ultralcd_test2_mainline.cpp");
		File input_b = new File(BASE_DIR, "ultralcd_test2_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "ultralcd_test2_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test03() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test3";
		File input_a = new File(BASE_DIR, "ultralcd_test3_mainline.cpp");
		File input_b = new File(BASE_DIR, "ultralcd_test3_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "ultralcd_test3_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void testWrap_01() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "testWrap_01";
		File input_a = new File(BASE_DIR, "testWrap_01_mainline.cpp");
		File input_b = new File(BASE_DIR, "testWrap_01_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "testWrap_01_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void testWrap_02() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "testWrap_02";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void testWrap_03() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "testWrap_03";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void testUnwrap_01() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "testUnwrap_01";
		File input_a = new File(BASE_DIR, "testUnwrap_01_mainline.cpp");
		File input_b = new File(BASE_DIR, "testUnwrap_01_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "testUnwrap_01_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void testUnwrap_02() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "testUnwrap_02";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void testUnwrap_03() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "testUnwrap_03";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void testUnwrap_04() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "testUnwrap_04";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test04() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test4";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "ultralcd_test4_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test05() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test5";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, "ultralcd_test5_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test06() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test6";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test07() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		/*
		 * This produces an incorrect result
		 */
		String name = "ultralcd_test7";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test08() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test8";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test09() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		
		/*
		 * Does not find the right children to adopt because it should add a If's children, but this if is marked as "new"
		 * Maybe we need to either handle wrap/unwrap of <else> or we might need to extend for check for isNew == 1 to consider the source of the father node
		 * 
		 */
		String name = "ultralcd_test9";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void ultralcd_test10() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "ultralcd_test10";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void paperexample01() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "paperexample01";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_clone.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);

		runTest(name, input_a, input_b, expected);
	}
	
	@Test
	public void paperexample02() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "paperexample02";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		runTest(name, input_a, input_b, "", false, true);
	}
	
	@Test
	public void paperexample03() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "paperexample03";
		String directory = "C:/Users/Max/workspace/mps-re-engineering/material/2018-02-13 complex example";
		File input_a = new File(directory, "mainline.cpp");
		File input_b = new File(directory, "fork.cpp");
		runTest(name, input_a, input_b, "", false, true);
	}
	
	@Test
	public void paperexample04() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "paperexample04";
		String directory = "C:/Users/Max/workspace/mps-re-engineering/material/2018-08-23 new example";
		File input_a = new File(directory, "mainline.cpp");
		File input_b = new File(directory, "fork.cpp");
		runTest(name, input_a, input_b, "", false, true);
	}
	
	@Test
	public void paperexample() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "paperexample";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");

		runTest(name, input_a, input_b, "", false, true);
	}


	@Test
	public void WilhelmEx4() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		/* Note that this example is desgined so that there are no differences in the whitespaces.
		 * We could ignore whitespace differences but I have to check the implication
		 */
		String name = "WilhelmEx4";
		File input_a = new File(BASE_DIR, name + "_mainline.cpp");
		File input_b = new File(BASE_DIR, name + "_fork.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void case01() throws ParserConfigurationException, XPathExpressionException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException, InputFileException, ComputePhaseException
	{
		String name = "case01";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void jcrocholl() throws Exception {
		// This replays jcrocholl's merge commit with conflicts from https://github.com/jcrocholl/Marlin/commit/43ac59286ea8fb3a6c292a7e52852ac1bd1fd805
		// Jcrocholl is pulling in the mainline (remote) into his own fork (base).

		String name = "jcrocholl";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void jcrocholl_test01() throws Exception {
		String name = "jcrocholl_test01";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void jcrocholl_test02() throws Exception {
		// Technically, the result is fine, but the outer #ifdef condition needs to be simplified
		String name = "jcrocholl_test02";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void jcrocholl_test03() throws Exception {
		String name = "jcrocholl_test03";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void jcrocholl_test04() throws Exception {
		String name = "jcrocholl_test04";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	public void jcrocholl_test05() throws Exception {
		String name = "jcrocholl_test05";
		File input_a = new File(BASE_DIR, name + "_base.cpp");
		File input_b = new File(BASE_DIR, name + "_remote.cpp");
		String expected = FileUtils.readFileToString(new File(BASE_DIR, name + "_expected.cpp"), Charsets.UTF_8);
		runTest(name, input_a, input_b, expected);
	}

	@Test
	@Ignore
	public void marlinMining01() throws Exception {
		String name = "Marlin_main.cpp";
		File input_a = new File("../../marlin-mining/examples/conflicts/2daa859/whole_files", "base_" + name);
		File input_b = new File("../../marlin-mining/examples/conflicts/2daa859/whole_files", "remote_" + name);
		String expected = "";
		runTest(name, input_a, input_b, expected);
	}

	@Test
	@Ignore
	public void marlinMining02() throws Exception {
		String name = "Configuration.h";
		File input_a = new File("../../marlin-mining/examples/conflicts/6a81291/whole_files", "base_" + name);
		File input_b = new File("../../marlin-mining/examples/conflicts/6a81291/whole_files", "remote_" + name);
		String expected = "";
		runTest(name, input_a, input_b, expected);
	}

	@Test
	@Ignore
	public void marlinMining03() throws Exception {
		String name = "Marlin_main.cpp";
		File input_a = new File("../../marlin-mining/examples/conflicts/6ae7f78/whole_files", "base_" + name);
		File input_b = new File("../../marlin-mining/examples/conflicts/6ae7f78/whole_files", "remote_" + name);
		String expected = "";
		runTest(name, input_a, input_b, expected);
	}

	@Test
	@Ignore
	public void marlinMining04() throws Exception {
		String name = "ultralcd.cpp";
		File input_a = new File("../../marlin-mining/examples/conflicts/788d62b/whole_files", "base_" + name);
		File input_b = new File("../../marlin-mining/examples/conflicts/788d62b/whole_files", "remote_" + name);
		String expected = "";
		runTest(name, input_a, input_b, expected);
	}

	@Test
	@Ignore
	public void marlinMining05() throws Exception {
		String name = "Marlin_main.cpp";
		File input_a = new File("../../marlin-mining/examples/conflicts/8a739b6/whole_files", "base_" + name);
		File input_b = new File("../../marlin-mining/examples/conflicts/8a739b6/whole_files", "remote_" + name);
		String expected = "";
		runTest(name, input_a, input_b, expected);
	}
	
	@Test
	public void marlinJcrochollLarge() throws Exception {
		String name = "Marlin_main.cpp";
		File input_a = new File(BASE_DIR, "jcrocholl/mainline/Marlin_main_cleaned.cpp");
		File input_b = new File(BASE_DIR, "jcrocholl/fork/Marlin_main_cleaned.cpp");
		String expected = "";
		runTest(name, input_a, input_b, expected, false, true);
	}


	@Test
	@Ignore
	public void diffTest() throws InputFileException, ComputePhaseException
	{
		Nconfig nconfig = PPMerge.getNConfig();
		File test1a = new File(BASE_DIR, "diff01a.xml");
		File test1b = new File(BASE_DIR, "diff01b.xml");

		File debugFile = new File("../debug/diffing/debug.html");
		System.out.println("debug file " + debugFile.getAbsolutePath());

		Debug.flag = true;
		Debug.start();
		DOMDocument result = Ndiff.getDOMDocument(test1a.toURI().toString(),
				test1b.toURI().toString(),
				nconfig);

		System.out.println("DIFF RESULT");
		result.writeToStream(System.out);
		System.out.println("");

	}
}
