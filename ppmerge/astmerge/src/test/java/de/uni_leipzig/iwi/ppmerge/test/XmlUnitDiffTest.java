/* MIT License
 * Copyright (c) 2016-2019 Max Lillack, Stefan Stanciulescu, and Wilhelm Hedman
 *
 * This file is part of the INCLINE tool. 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.uni_leipzig.iwi.ppmerge.test;

import de.uni_leipzig.iwi.ppmerge.FileUtils;
import de.uni_leipzig.iwi.ppmerge.XmlUnitDiff.TreeTraverser;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class XmlUnitDiffTest {

	public static final String BASE_DIR = "src/main/resources/examples";

	@Test
	public void test01() throws SAXException, IOException, ParserConfigurationException {
		File test1a = new File(BASE_DIR, "test01a.xml");
		File test1b = new File(BASE_DIR, "test01b.xml");
		
		FileUtils.assertExists(test1a);
		FileUtils.assertExists(test1b);
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc1a = builder.parse(test1a);
		Document doc1b = builder.parse(test1b);
		
		Diff mydiff = DiffBuilder.compare(Input.from(doc1a)).withTest(Input.from(doc1b)).build();

		// traverse tree, resolve differences
		TreeTraverser treeTraverser = new TreeTraverser(doc1a, doc1b, mydiff);
		treeTraverser.traverse();

		// TODO: Assert something?
	}
	

	@Test
	public void test02() throws SAXException, IOException, ParserConfigurationException {
		File test1a = new File(BASE_DIR, "test02a.xml");
		File test1b = new File(BASE_DIR, "test02b.xml");

		FileUtils.assertExists(test1a);
		FileUtils.assertExists(test1b);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc1a = builder.parse(test1a);
		Document doc1b = builder.parse(test1b);
		
		Diff mydiff = DiffBuilder.compare(Input.from(doc1a)).withTest(Input.from(doc1b)).build();

		// traverse tree, resolve differences
		TreeTraverser treeTraverser = new TreeTraverser(doc1a, doc1b, mydiff);
		treeTraverser.traverse();

		// TODO: Assert something?
	}

}
