package it.unibo.cs.ndiff.test.exceptions;

public class XMLnotEqualException extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	public XMLnotEqualException(String assertError) {
		super(assertError);
	}

}
