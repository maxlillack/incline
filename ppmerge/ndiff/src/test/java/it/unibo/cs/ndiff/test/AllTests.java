package it.unibo.cs.ndiff.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for it.unibo.cs.ndiff.test");

		suite.addTestSuite(RemergeTest.class);

		return suite;
	}

}
