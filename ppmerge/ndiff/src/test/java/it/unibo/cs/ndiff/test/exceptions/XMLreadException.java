package it.unibo.cs.ndiff.test.exceptions;

public class XMLreadException extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	public XMLreadException(Exception e) {
		super(e);
	}

}
